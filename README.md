# Varac's documentation wiki

Docs are published at [varac.net/docs/](https://www.varac.net/docs/)

## To-do

- Fix `task build`:
  `ERROR - Config value 'theme': Unrecognised theme name: 'material'. The available installed themes are: mkdocs, readthedocs`
- Fix website to display i.e. [GitHub icons](https://www-staging.k.varac.net/docs/network/bittorrent/transmission.html)
- Add a plugin to show latest changes to docs/
  - i.e. [mkdocs-git-latest-changes-plugin](https://tombreit.github.io/mkdocs-git-latest-changes-plugin/)
- Run task as post-commit
- Fix build warnings
- Integrate into [varac.net](https://varac.net/)
  - build from CI
