# aqua

- [Website](https://aquaproj.github.io/)
- [GitHub](https://github.com/aquaproj/aqua)
- Golang, very active
- [Aqua registry](https://github.com/aquaproj/aqua-registry)
- [mise aqua backend](https://mise.jdx.dev/dev-tools/backends/aqua.html)
