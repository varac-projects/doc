# Testing

See also:

- [../coding/python/testing.md](../coding/python/testing.md)
- [kubernetes/helm/testing.md](../kubernetes/helm/testing.md)
- [bats.md](../coding/shell/testing/bats.md)

## Web application testing

- `taiko`: see `./taiko.md`
- selenium

## Infrastructure testing

- [goss](https://github.com/aelsabbahy/goss)
  Quick and Easy server testing/validation (golang)
