# Server testing

## Goss

- [GitHub](https://github.com/goss-org/goss)
- [Docs](https://goss.readthedocs.io/en/stable/)
  - [Quick start](https://goss.readthedocs.io/en/stable/quickstart/)
  - [Platform feature-parity](https://goss.readthedocs.io/en/stable/platforms/)
    - "`goss` works well on Linux, but support on Windows & macOS is alpha"
- Tutorial: [Work with Serverspec alternative tool Goss](https://dev.to/koh_sh/work-with-serverspec-alternative-tool-goss-1pj1)

Install:

```sh
pamac install goss-bin
```

Create test:

```sh
goss add addr google.com:443
cat goss.yaml
goss validate
```

### Templates

- [Docs: templates](https://github.com/goss-org/goss/blob/master/docs/gossfile.md#templates)
  - [Template examples](https://goss.readthedocs.io/en/stable/gossfile/#examples)

Issues:

- [.Env.variable doesn't work in a range](https://github.com/goss-org/goss/issues/965)
- [Validate gossfiles with templates](https://github.com/goss-org/goss/issues/963)

## Testinfra

- [GitHub](https://github.com/pytest-dev/pytest-testinfra)
- [Docs](https://testinfra.readthedocs.io/en/latest/index.html)
- Slow development
- Not fully compatible with [OSX](https://github.com/pytest-dev/pytest-testinfra/issues/21)
  - Testinfra uses system calls to tool like `nc` or `getent`, which work
    differently on Linux an Mac i.e.

## Unmaintained

- [Serverspec](https://github.com/mizzy/serverspec) (Ruby)
