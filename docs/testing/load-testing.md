# Load / performance testing

## k6

* javascript
* [k6 authentication](https://k6.io/docs/examples/http-authentication/)
  * [Test OAuth apps](https://k6.io/blog/how-to-load-test-oauth-secured-apis-with-k6)

## locust

https://locust.io/
* python
