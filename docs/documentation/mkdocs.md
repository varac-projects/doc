# MkDocs

- [Website](https://www.mkdocs.org/)

## Issues

### Nested list items _must_ use 4 spaces

- Based on a bug in [Python-Markdown](https://github.com/Python-Markdown/markdown)

  - [Python-Markdown: Nested lists require 4 spaces of indent](https://github.com/Python-Markdown/markdown/issues/3)

- [mkdocs: Incorrect rendering of nested lists](https://github.com/mkdocs/mkdocs/issues/545)
- [mkdocs-material: Exceptions in markdown nested list](https://github.com/squidfunk/mkdocs-material/issues/6509)

Solutions:

- Python-Markdown suggests using `markdown.markdown(text, tab_length=2)` as Python-Markdown configuration,
  however, MkDocs cannot directly configure Python-Markdown behaviour
- Therefore install the [mdx_truly_sane_lists Python-Markdown extension](https://github.com/radude/mdx_truly_sane_lists)

Install `mdx_truly_sane_lists` pip package:

```sh
pip install mdx_truly_sane_lists
```

`mkdocs.yaml`:

```yaml
markdown_extensions:
  - mdx_truly_sane_lists
```
