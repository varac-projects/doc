# Knowledge base / not taking

- [Awesome Knowledge Management](https://github.com/brettkromkamp/awesome-knowledge-management)
- [Awesome Neovim: Note-taking](https://github.com/rockerBOO/awesome-neovim#note-taking)
- [How to Make Yourself Into a Learning Machine](https://every.to/superorganizers/how-to-build-a-learning-machine-299655)

## Vim / Neovim

### Wiki plugins

- [Kiwi.nvim](https://github.com/serenevoid/kiwi.nvim)
  - lua
  - A stripped down VimWiki for neovim
- [Wiki.vim](https://github.com/lervag/wiki.vim)
  - VimScript
- [VimWiki](http://vimwiki.github.io/)
  - VimScript
  - [Getting started with vimwiki](https://mkaz.blog/working-with-vim/vimwiki/)

### Wiki link plugins

- [mkdnflow](https://github.com/jakewvincent/mkdnflow.nvim)
  - 1k+ commits
  - Lua
- [follow-md-links.nvim](https://github.com/jghauser/follow-md-links.nvim)
  - Lua
  - Few commits, last commit 2024-02
- [mvim-mdlink](https://github.com/Nedra1998/nvim-mdlink)
  - Lua
  - Very few commits, last commit 2024-02

### Other useful plugins

- [nvim-toc](https://github.com/richardbizik/nvim-toc)
  Generate table of contents for markdown files

## Obsidian

- [Website](https://obsidian.md/)
- [GitHub org](https://github.com/obsidianmd)
- Neovim Plugins
  - [epwalsh/obsidian.nvim](https://github.com/epwalsh/obsidian.nvim)
    - Lua, 394 commits (2023-11)
  - [ada0l/obsidian](https://github.com/ada0l/obsidian)
    - Lua
    - 76 commits (2023-11)
  - [obs.nvim](https://github.com/IlyasYOY/obs.nvim)
    - 21 commits (2023-11)

## Other apps

- [Logseq](https://logseq.com/)
  - AUR packages available
- [Joplin](https://joplinapp.org/)
- [Outline](https://www.getoutline.com/)
  - A knowledge base for teams.
- [Dendron](https://wiki.dendron.so/)

## Zettelkasten

- [The Zettelkasten](https://www.soenkeahrens.de/en/takesmartnotes)
- [My Neovim Zettelkasten](https://mischavandenburg.com/zet/neovim-zettelkasten/)
- [A knowledge base with Vim, Fzf and Ripgrep](https://naps62.com/posts/knowledge-base)
- [VimWiki addon for managing notes according to Zettelkasten method](https://github.com/michal-h21/vim-zettel)
- [telekasten](https://github.com/renerocksai/telekasten.nvim)
  working with a text-based, markdown zettelkasten / wiki and
  mixing it with a journal, based on telescope.nvim.
