# Diagrams

- [Community list of comparisons between Text to Diagram tools](https://text-to-diagram.com/)
  - Compares Graphviz, D2, Mermaid and PlantUML

## Kroki

> Unified API with support for BlockDiag (BlockDiag, SeqDiag, ActDiag, NwDiag, PacketDiag, RackDiag),
> BPMN, Bytefield, C4 (with PlantUML), D2, DBML, Diagrams.net (experimental), Ditaa, Erd, Excalidraw,
> GraphViz, Mermaid, Nomnoml, Pikchr, PlantUML, SvgBob, Symbolator, UMLet, Vega, Vega-Lite, WaveDrom
> and WireViz …and more to come!

- [Website](https://kroki.io/)
- [GitHub](https://github.com/yuzutech/kroki)
- [Gitlab integration](https://docs.gitlab.com/ee/administration/integration/kroki.html)
  - [Add DBML and D2 support from Kroki](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109454)
  - With 16.x, no `d2` or `dbml` working

## Ascii diagrams

- [venn.nvim Neovim plugin](https://github.com/jbyuki/venn.nvim)

## likec4

- "Architecture-as-a-code with live diagrams"
- [Website](https://likec4.dev/)
- [Docs](https://likec4.dev/tutorial/)
- [Playground](https://playground.likec4.dev/w/tutorial/)

## D2

- [Website](https://d2lang.com/)
- [D2 Playground](https://play.d2lang.com)

## Mermaid

- [Mermaid vs. UML](https://ruleoftech.com/2018/generating-documentation-as-code-with-mermaid-and-plantuml)
- [Mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor/)
- [Integrated in gitlab](https://docs.gitlab.com/ce/user/markdown.html#mermaid)
- [Mermaid JS, library](https://github.com/knsv/mermaid)
- [Mermaid cli](https://github.com/mermaidjs/mermaid.cli)
- [Scale
  images](https://github.com/gnab/remark/issues/72#issuecomment-419693263)
- see examples in `~/projects/diagrams/mermaid`

### Issues

- [How do I force subgraphs to be top-down ?](https://github.com/mermaid-js/mermaid/issues/287#issuecomment-2445697256)

## Use pandoc to create PDF from markdown + mermaid

- [Gist: Render PDF from Markdonw that using mermaid](https://gist.github.com/letientai299/2c974b4f5e7b05be52d369ff8693c29a)
- [mermaid-filter](https://github.com/raghur/mermaid-filter)

```sh
sudo apt install pandoc
npm i -g mermaid-filter

pandoc  -F mermaid-filter -o freifunk.pdf freifunk.md
```

## UML tools

- [Violet UML Editor](https://github.com/violetumleditor/violetumleditor):
  Stalled development (2018)

- [argo uml](https://github.com/argouml-tigris-org/argouml):
  Stalled development (2018)

- Umbrello: Avaiable as ubuntu deb package

- [PlantUML](http://plantuml.com/de/): text to diagram, Stalled development
  (2018), available as .deb

## DrawIO

- [app.diagrams.net](https://app.diagrams.net)

## Server rack / switch documentation

### Blockdiag

- [Website](http://blockdiag.com/en/)

- Development stalled

- Blockdiag can be rendered from Krok

- [nwdiag](https://github.com/blockdiag/nwdiag)

  - generate network-diagram image file from spec-text file
  - included tools: i.e. [rackdiag](http://blockdiag.com/en/nwdiag/rackdiag-examples.html)
  - Aur package: `nwdiag`
