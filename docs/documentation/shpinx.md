# Sphinx

- [Render mermaid diagrams with sphinx](https://github.com/mgaitan/sphinxcontrib-mermaid)
- [How to generate PDFs](https://stackoverflow.com/questions/39534718/how-to-create-a-pdf-out-of-sphinx-documentation-tool)

## Extenssions

I couldn't find any extenstion which allows collapsing sections with
sub-sections.

### Tabs, Grids etc

#### sphinx-design

- [RTD page](https://sphinx-design.readthedocs.io)
- No fancy formatting, no labels inside cards/tabs

#### Other

- [sphinx-tabs](https://sphinx-tabs.readthedocs.io): uses js
- [Sphinx-Panels](https://sphinx-panels.readthedocs.io)
  - similar to `sphinx-tabs`: only css, no js.
  - [Will get deprecated in favor of sphinx-design](https://github.com/executablebooks/sphinx-panels/issues/67)

### Toggle / collapse

- [sphinx-togglebutton](https://sphinx-togglebutton.readthedocs.io/en/latest)

### Version warnings

- [sphinx-version-warning](https://github.com/humitos/sphinx-version-warning):
  adds a highgly customizable version warning banner. See [display
  banner link to stable docs from latest and old
  docs](https://github.com/readthedocs/readthedocs.org/issues/3481)
  for discussion.
