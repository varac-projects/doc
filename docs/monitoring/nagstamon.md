# Nagstamon

## Install from nagstamon repo

Stable and testing: https://nagstamon.ifw-dresden.de/download/
Incoming queue (automated master builds): https://nagstamon.ifw-dresden.de/files/incoming/

## Build

    cd ~/projects/monitoring/Nagstamon/build
    python3 build.py

## Prometheus nagstamon integration

### Prometheus API key mapping

see also https://github.com/HenriWahl/Nagstamon/blob/master/Nagstamon/Servers/Prometheus.py#L163
and https://github.com/HenriWahl/Nagstamon/issues/623


| Nagstamon field    | Prometheus key                     |
| ------------------ | ---------------------------------- |
| Host               | labels.podname or labels.namespace |
| Service            | labels.alertname                   |
| Status             | labels.severity                    |
| Last Check         | n/a                                |
| Duration           | activeAt                           |
| Attempt            | state                              |
| Status Information | annotations.message                |
