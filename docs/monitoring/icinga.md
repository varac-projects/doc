# Nagios / Icinga logs parsen

Convert epoch timestamp to date:

    cat icinga.log | perl -pe 's/(\d+)/localtime($1)/e'
Icli
====

FILTER EXPRESSIONS
------------------

       Each expression can be negated with an exclamation mark, e.g. "!A" for all non-acknowledged services.

       A   Check state has been acknowledged
       D   The host this service belongs to is Down or Unreachable
       F   Service is flapping between states
       N   Notifications for this service are disabled
       P   Only passive checks are enabled.  Note that !P simply means that active checks are enabled, no matter the status of passive checks
       S   Check state is soft.  For instance, it used to be OK and is now critical, but has not reached its maximum number and caused a notification yet.  Good to find (or
           ignore) service problems which might just be temporary, non-critical glitches.
       o   Host/Service state is OK
       w   Service state is Warning
       c   Service state is Critical
       u   Service state is Unknown
       d   Host state is Down
       x   Host state is Unreachable


* Examples



    alias ncli='icli -c /var/cache/nagios3/objects.cache -f /var/cache/nagios3/status.dat  -F /var/lib/nagios3/rw/nagios.cmd'

    # list only failed, non-acknoleged services/hosts
    ncli -z '!o,!A'

    # recheck check_mk
    ncli -r -s Check_MK
    ncli -r -s Check_MK -h wallaby.bitmask.i

    # show webapp status
    ncli  -s webapp_login
