# Monitoring logfiles

- Comparism: [mtail, grok_exporter or fluentd](https://stackoverflow.com/q/41160883)
- [telegraf](https://github.com/influxdata/telegraf)

## mtail

- [GitHub](https://github.com/google/mtail/)
- [Log Collection, Distribution, and Filtering {: #syslog}](https://github.com/google/mtail/blob/main/docs/Interoperability.md#log-collection-distribution-and-filtering--syslog)

Example usage:

    cd ~/projects/logging/mtail/testing
    mtail --progs ~/projects/logging/mtail/testing/progs --logs /var/log/syslog
    curl -s http://localhost:3903/metrics | grep  '^mtail'

### Issues

- No [support reading from systemd's journal](https://github.com/google/mtail/issues/5)
  - [unable to read from unix socket](https://github.com/google/mtail/issues/276)

> To receive logs directly from systemd-journal, one must be able to read from a unix domain socket.
> Mtail now does this, although I kind of regret it. Socket support for both datagram and stream
> families was tricky and likely has many future bugs.

- Can't push to pushgateway. Possible workaround with [PushProx](https://github.com/RobustPerception/PushProx)

### mtail postfix

- [autistici: postfix.mtail](https://git.autistici.org/ai3/config/-/blob/master/roles/mail/files/mtail/postfix.mtail)
- [autistici: postfix.json](https://git.autistici.org/ai3/config/-/blob/master/roles/ai3-prometheus/files/grafana/dashboards/postfix.json)
