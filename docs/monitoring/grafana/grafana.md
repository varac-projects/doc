# Grafana

<https://grafana.com/docs/grafana/latest/>

## Configuration

<https://grafana.com/docs/grafana/latest/installation/configuration/>

## API keys

Create a service account API token from the UI:

- Create a [service account](https://grafana.k.varac.net/org/serviceaccounts)
- `Add service account token`

- [Authentication API](https://grafana.com/docs/grafana/latest/http_api/auth/)
- [How to generate API keys using the Grafana API](https://grafana.com/docs/grafana/latest/http_api/create-api-tokens-for-org/)

## Python clients

- [Grafana-client](https://github.com/panodata/grafana-client) API client: Active, 338
  commits, >20 contributors
- [Grafana API SDK](https://github.com/ZPascal/grafana_api_sdk) API client: 0
  contributors, 7 commits
  [Why another API package ?](https://github.com/ZPascal/grafana_api_sdk/issues/6)
- [Grafana-dashboard-manager](https://github.com/Beam-Connectivity/grafana-dashboard-manager)
