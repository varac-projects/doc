# Dashboards

- [Grafana Dashboard hub](https://grafana.com/grafana/dashboards)

Example dashboards:

- [anarcat/grafana-dashboards](https://gitlab.com/anarcat/grafana-dashboards)
- [float dashboards](https://git.autistici.org/ai3/float/-/tree/master/roles/float-infra-prometheus/templates/grafana/dashboards)

## Provisioning

[Provisioning docs](https://grafana.com/docs/grafana/latest/administration/provisioning)

### Dashboard provisioning

When provisioning a dashboard, make sure you don't have any `DS_PROMETHEUS`
variables in it, because grafana will fail to load it with `Datasource named
${DS_PROMETHEUS} was not found`. See also:

- [Support dashboard variables in dashboard provisioning](https://github.com/grafana/grafana/issues/10786)

Export the dashboard using the `View JSON` button of the `Share/Export tab`.

## Grafana Dashboards as code

As of 2022-08, the best practice of dashboards in code is very much in flux.
`grafonnet-lib` was the best practice so far, but Grafana points out in
[their roadmap for `everthing in code`](https://github.com/grafana/grafana/discussions/39593)
that they are restructuring their internal schemas and will use [cue](https://cuelang.org/)
in the future.

- [`grafonnet-lib` is deprecated in favor of `grafonnet`](https://github.com/grafana/grafonnet-lib/issues/332#issuecomment-1572790066)
- [A complete guide to managing Grafana as code: tools, tips, and tricks](https://grafana.com/blog/2022/12/06/a-complete-guide-to-managing-grafana-as-code-tools-tips-and-tricks/)
- [Grafana thema](https://github.com/grafana/thema) shows progress.
- [Grafana dashboards — best practices and dashboards-as-code](https://andidog.de/blog/2022-04-21-grafana-dashboards-best-practices-dashboards-as-code)

### Grabana

- [GitHub](https://github.com/K-Phoen/grabana)
- [FR: Dump dashboard as json](https://github.com/K-Phoen/grabana/issues/258)

### Dark

- [Github](https://github.com/K-Phoen/dark)
- [Docs](https://github.com/K-Phoen/dark/blob/master/docs/index.md)
- Uses [Grabana](https://github.com/K-Phoen/grabana)

### jsonnet

see `../../coding/jsonnet.md`

Use by i.e. [kubernetes-mixin](https://github.com/kubernetes-monitoring/kubernetes-mixin)
dashboards. kubernets-mixin uses:

- [Grafana Labs' Jsonnet libraries](https://github.com/grafana/jsonnet-libs)
- grafonnet

### weave grafanalib

[weaveworks/grafanalib](https://github.com/weaveworks/grafanalib)

> Python library for building Grafana dashboards

- [docs](https://grafanalib.readthedocs.io/en/stable/index.html)

➕ Python
➕ Actively maintained

### grafana-dash-gen

- [uber/grafana-dash-gen](https://github.com/uber/grafana-dash-gen)

➖Javascript

### grafyaml

- [opendev/grafyaml](https://opendev.org/opendev/grafyaml)

### Outdated tools

#### Polly

> Polly is a specification for parameterized packages of
> observability-related config objects such as dashboards, alerts and more.

- [Polly website](https://pollypkg.github.io/polly/)
- [Polly Github](https://github.com/pollypkg/polly)
- [Polly cert-manager example](https://github.com/pollypkg/polly/blob/main/examples/cert-manager/cert-manager.cue)

#### Grafonnet-lib

[grafonnet-lib](https://github.com/grafana/grafonnet-lib)
uses [jsonnet](https://github.com/google/jsonnet)

- [Future of this library](https://github.com/grafana/grafonnet-lib/issues/332)

> Attention: We're in the process of introducing generated code that can be
> used instead of the manually maintained Jsonnet code in the grafonnet
> directory. The generated code lives in grafonnet-7.0. It's generated from a
> new project, grafana/dashboard-spec. The generated code is still incomplete,
> however, the components present are useable. We very much appreciate
> contributions in grafana/dashboard-spec for components yet to be added.

- [API docs](https://grafana.github.io/grafonnet-lib/api-docs/)
- [Example Dashboards](https://github.com/grafana/grafonnet-lib/tree/master/examples)
- [Vim jsonnet filetype plugin](https://github.com/google/vim-jsonnet)
- [Grafana as code medium article](https://medium.com/@tarantool/grafana-as-code-b642cac9ae75)
- [Frodem video](https://archive.fosdem.org/2020/schedule/event/grafana_as_code)
- [Example unit names](https://github.com/grafana/grafana-sensu-app/blob/master/tests/__mocks__/app/core/utils/kbn.ts#L438)
- [categories.ts](https://github.com/grafana/grafana/blob/main/packages/grafana-data/src/valueFormats/categories.ts)
- [Tutorial with templates](https://www.novatec-gmbh.de/en/blog/grafana-dashboards-as-code-with-grafonnet/)

#### dashboard-spec

pamac install jsonnet-bundler-bin jsonnet [grafana/dashboard-spec](https://github.com/grafana/dashboard-spec)

Next generation dashboard scripting from Grafana itself.
Still WIP.

- [Available units of mesurements/categories](https://github.com/grafana/grafana/blob/main/packages/grafana-data/src/valueFormats/categories.ts)

#### grafana-dashboard-builder

- [jakubplichta/grafana-dashboard-builder](https://github.com/jakubplichta/grafana-dashboard-builder)

> Generate Grafana dashboards with YAML using Python

➖ Out of date (last commit 2021-01)

## Autograf

Dynamically generate Grafana dashboard based on Prometheus metrics

- [Github](https://github.com/prometheus-community/smartctl_exporter/issues/119)

## Grafana

- [Configure Grafana authentication](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/grafana/)
  - [Anonymous authentication](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/#anonymous-authentication)

> You can make Grafana accessible without any login required
> by enabling anonymous access in the configuration file.
