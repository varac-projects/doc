# Console tools for monitoring

## CPU / RAM

* [bashtop](https://github.com/aristocratos/bashtop)
* [procs](https://github.com/dalance/procs)
* htop
* [gotop](https://github.com/xxxserxxx/gotop)
* [bottom](https://github.com/ClementTsang/bottom)

## Disk usage

* [gdu](https://github.com/dundee/gdu): ncdu alternative in go

## The Stress Terminal UI: s-tui

* [s-tui](https://github.com/amanusk/s-tui)

Graphs CPU metrics (incl. temperature) nicely in the terminal

* [Dash: Terminal dashboard solution inspired by Grafana](https://github.com/ricoberger/dash)
* [WTF: personal information dashboard for your terminal](https://wtfutil.com/)
* [termui: Golang terminal dashboard](https://github.com/gizak/termui)
* [dashing: Terminal dashboards for Python ](https://github.com/FedericoCeratto/dashing)
