# Pushing to prometheus

https://prometheus.io/docs/practices/pushing/

## PushGateway

https://github.com/prometheus/pushgateway

* [official helm chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-pushgateway)
* [ofiicial docker image](https://hub.docker.com/r/prom/pushgateway)

[Example of pushing node-exporter metrics to pushgateway](https://github.com/prometheus/node_exporter/issues/279#issuecomment-397581152):

    curl -s http://localhost:9256/metrics | grep -v "\(\(^\| \)go_\|http_request\|http_requests\|http_response\|process_\)" | curl --data-binary @- http://pushgateway.example.com:9091/metrics/job/node/instance/<INSTANCE_NAME>

### Public instance

    export URL='https://prometheus-pushgateway.example.net'

Delete all metrics in the group identified by `{job="node",instance="sth"}`:

    curl -Lu "admin:$PW" -X DELETE $URL/metrics/job/node/instance/sth

## prometheus-proxy

https://github.com/pambrose/prometheus-proxy

* Well maintained, recent releases
* Up to date, properly tagged images https://hub.docker.com/r/pambrose/prometheus-agent and
  https://hub.docker.com/r/pambrose/prometheus-proxy
* [No helm chart published on artifacthub](https://artifacthub.io/packages/search?ts_query_web=prometheus-proxy&sort=relevance&page=1)

## prom-aggregation-gateway

https://github.com/weaveworks/prom-aggregation-gateway
* [No helm chart published on artifacthub](https://artifacthub.io/packages/search?ts_query_web=+prom-aggregation-gateway&sort=relevance&page=1)
* [Feature Request - HELM Chart](https://github.com/weaveworks/prom-aggregation-gateway/issues/47)
* The prometheus-pushgateway helm chart can get used for deploying prom-aggregation-gateway with little changes:
  https://github.com/weaveworks/prom-aggregation-gateway/pull/49

## Stale / unmaintained

### PushProx

https://github.com/prometheus-community/PushProx

* Last release over 1y old
* [Unversioned, unverbose official image](https://hub.docker.com/r/prometheuscommunity/pushprox/)

#### Authentication

* Use ingress with http auth for server
* Use [basic auth via URL for the client](https://github.com/prometheus-community/PushProx/issues/12#issuecomment-337019573)
* PR for adding authentication to pushprox nativly: [Added basic auth for proxy and /clients endpoint](https://github.com/prometheus-community/PushProx/pull/94)

#### Docker images

* [camptocamp/pushprox](https://hub.docker.com/r/camptocamp/pushprox)
  docker image, last updated 2019-10 (as of 2020-02)
* https://hub.docker.com/r/rancher/pushprox-client (2020)
* https://hub.docker.com/r/rancher/pushprox-proxy  (2020)
* https://hub.docker.com/r/prometheuscommunity/pushprox  (up to date)

#### PushProx helm charts

* No [official helm chart](https://github.com/prometheus-community/helm-charts)
* https://github.com/camptocamp/charts/tree/master/pushprox (2019)
  uses `camptocamp/pushprox` image (see above)
