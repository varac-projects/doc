# kube-prometheus-stack

- [kube-prometheus-stack helm chart](https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack)

## Configuration

Secret `prometheus-prometheus-stack-kube-prom-prometheus`: Gzipped main prometheus config `prometheus.yaml.gz`

```sh
kubectl-view_secret -n oas prometheus-prometheus-stack-kube-prom-prometheus | gunzip |less
```

This `prometheus.yaml` ends up in the Prometheus pod under `/etc/prometheus/config_out/prometheus.env.yaml`, see it with:

```sh
kc -n oas exec -it prometheus-prometheus-stack-kube-prom-prometheus-0 -c prometheus -- cat /etc/prometheus/config_out/prometheus.env.yaml
```

- Configmap `prometheus-prometheus-stack-kube-prom-prometheus-rulefiles-0`:
  Rules and alerts

## Runbooks

- [runbooks for alerts shipped with kube-prometheus project](https://runbooks.prometheus-operator.dev/)

## Scrape target manually

i.e. for cert-manager:

```sh
kubectl -n cert-manager port-forward cert-manager-5d9d756cf8-qg64h 9402
curl localhost:9402/metrics
```
