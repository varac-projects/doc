# blackbox exporter

https://github.com/prometheus/blackbox_exporter

Best blackbox-exporter dashboard so far: [Prometheus Blackbox Exporterby sparanoid](https://grafana.com/grafana/dashboards/7587)

Show config:

    curl "localhost:9115/config"

On kubernetes:

    curl blackbox-exporter-prometheus-blackbox-exporter.monitoring.svc.cluster.local:9115/config

Query blackbox-exporter process metrics:

    curl "localhost:9115/metrics"

Query blackbox-exporter with curl:

    curl "localhost:9115/probe?target=example.org:22&module=ssh_banner"
    wget "localhost:9115/probe?target=google.com&module=http_2xx" -O -

Enable debugging:

https://www.robustperception.io/debugging-blackbox-exporter-failures

    curl "localhost:9115/probe?target=example.org:22&module=ssh_banner&debug=true"

Query blackbox-exporter on kubernetes with curl:

    curl "blackbox-exporter-prometheus-blackbox-exporter.monitoring.svc.cluster.local:9115/probe?target=example.org:22&module=ssh_banner"
    curl "blackbox-exporter-prometheus-blackbox-exporter.monitoring.svc.cluster.local:9115/probe?target=185.161.129.131:2194&module=tcp_connect"


Issues:

* [Invalid Scrape Target in 'kubernetes-pods' for Pods with Multiple Containers](https://github.com/prometheus/prometheus/issues/3530)

### Prometheus scape config examples

* https://github.com/prometheus/blackbox_exporter#prometheus-configuration
* https://www.robustperception.io/checking-if-ssh-is-responding-with-prometheus

### Query blackbox exporter metrics from prometheus

Show all failed probes for access points:

    probe_success{instance=~"ap-.*"}==0

Show all failed probes execept for access points:

    probe_success{instance!~"ap-.*"}==0
