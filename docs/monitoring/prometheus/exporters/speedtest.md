# Seedtest_exporter

see also `~/Howtos/network/performance/speedtest.md`

## librespeed

- [Librespeed Prometheus Exporter blogpost](https://brendonmatheson.com/2021/11/28/librespeed-prometheus-exporter.html)
- [brendonmatheson/prometheus-librespeed-exporter](https://github.com/brendonmatheson/prometheus-librespeed-exporter)
  - [brendonmatheson/prometheus-librespeed-exporter image](https://hub.docker.com/r/brendonmatheson/prometheus-librespeed-exporter)
  - Repo and container image: last update 2021

### openwrt

On laptop:

```bash
cd ~/projects/network/performance/prometheus-librespeed-exporter
scp librespeed-exporter.sh root@_gateway:/tmp
```

On router:

```bash
opkg install bash jq prometheus-node-exporter-lua-textfile
/etc/init.d/prometheus-node-exporter-lua restart
mkdir /var/prometheus
ln -s /tmp/librespeed-exporter.sh /usr/bin/librespeed-exporter.sh
/tmp/librespeed-exporter.sh > /var/prometheus/librespeed.prom
```

## speetest.net

### billimek/prometheus-speedtest-exporter

- [GitHub](https://github.com/billimek/prometheus-speedtest-exporter)
- Based on official speedtest.net cli tool and
  [ricoberger/script_exporter](https://github.com/ricoberger/script_exporter)
- Multi-arch container image (amd64, arm6, arm7, and arm64): [billimek/prometheus-speedtest-exporter](https://hub.docker.com/r/billimek/prometheus-speedtest-exporter/tags)
- Up to date repo + properly tagged, small container image

Start exporter:

```sh
podman run --rm --publish 9469:9469 billimek/prometheus-speedtest-exporter:sha-243c8eb
```

Run speedtest:

```sh
curl 'localhost:9469/probe?script=speedtest'

```

### Other images

- [hferreira23/prometheus-speedtest-exporter](https://github.com/hferreira23/prometheus-speedtest-exporter)
  - Based on the official speedtest cli app
  - [container image](https://hub.docker.com/r/hferreira/prometheus-speedtest-exporter/tags)
  - Up to date repo + container image, but only `latest` tag

### Deprecated / stale / old

- [MiguelNdeCarvalho/speedtest-exporter](https://github.com/MiguelNdeCarvalho/speedtest-exporter)
  - Last update 2023-06
- [whi-tw/speedtest_exporter](https://github.com/whi-tw/speedtest_exporter)
  - fork of nlamirault/speedtest_exporter, last update 2022
- [wdstorer/speedtest-exporter](https://github.com/wdstorer/speedtest-exporter)
  - last update Feb 2019
- [stefanwalther/speedtest-exporter](https://github.com/stefanwalther/speedtest-exporter)
  - nodejs, last update 2018
- [cattanisimone/speedtest-exporter](https://github.com/cattanisimone/speedtest-exporter)
  - nodejs, fork of stefanwalther/speedtest-exporter, last update 9/2019
- [p1gmale0n/speedtest_exporter](https://github.com/p1gmale0n/speedtest_exporter)
  - Stale, last commit 2020
  - Fork of nlamirault/speedtest_exporter, with added helm chart and
    [stale docker image](https://hub.docker.com/r/p1gmale0n/speedtest_exporter)
- [varac/speedtest-exporter-cli](https://0xacab.org/varac/speedtest-exporter-cli)
  - [Helmchart](https://0xacab.org/varac/helmcharts/-/tree/master/speedtest-exporter)
  - Based on [sivel/speedtest-cli](https://github.com/sivel/speedtest-cli), a python
    speedtest implementation (last updated Aug 2019).

#### jeanralphaviles/prometheus_speedtest

- [jeanralphaviles/speedtest](https://github.com/jeanralphaviles/prometheus_speedtest)
  - [docker image](https://hub.docker.com/r/jraviles/prometheus_speedtest)
    - Last update 2022-12
  - Related [Python package](https://pypi.org/project/prometheus-speedtest/)
  - [grafana dashboard](https://grafana.com/grafana/dashboards/11229)

Based on [sivel/speedtest-cli](https://github.com/sivel/speedtest-cli), a python
speedtest implementation (last updated 2021).

```sh
 docker run --rm -d --name prometheus_speedtest -p 9516:9516/tcp jraviles/prometheus_speedtest:latest
curl -m 180 localhost:9516/probe
```

#### nlamirault/speedtest_exporter

- [nlamirault/speedtest_exporter](https://github.com/nlamirault/speedtest_exporter)

  - Stale [docker hub image](https://hub.docker.com/r/nlamirault/speedtest_exporter)
    - Last update 2020

- Based on [zpeters/speedtest](https://github.com/zpeters/speedtest), a

- deprecated go speedtest library.

- No activity since Aug 2019.

  docker run -p 9112:9112 nlamirault/speedtest_exporter
  curl --max-time 180 localhost:9112/metrics

Issues:

- Breaks after client timeout (when using curl without increasing the timeout)
