# Gitlab exporters

* [mvisonneau/gitlab-ci-pipelines-exporter](https://github.com/mvisonneau/gitlab-ci-pipelines-exporter)
* [owentl/gitlab-prometheus](https://github.com/owentl/gitlab-prometheus)
  Generate prometheus metrics from gitlab issues
