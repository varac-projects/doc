# Libvirt exporters

- [Tinkoff/libvirt-exporter](https://github.com/Tinkoff/libvirt-exporter)
  - Last commit 2022
  - [prometheus-libvirt-exporter snap package](https://snapcraft.io/prometheus-libvirt-exporter)
  - Forked from:
    - [kumina/libvirt_exporter](https://github.com/kumina/libvirt_exporter)
      - Archived (Last commit 2021)
      - [Debian package](https://tracker.debian.org/pkg/prometheus-libvirt-exporter)
    - [rumanzo/libvirt_exporter_improved](https://github.com/rumanzo/libvirt_exporter_improved)
      (Last commit 2020)
- [zhangjianweibj/prometheus-libvirt-exporter](https://github.com/zhangjianweibj/prometheus-libvirt-exporter)
  - Last commit 2021

## Grafana dashboards

- [19639-libvirt-dashboard](https://grafana.com/grafana/dashboards/19639-libvirt-dashboard/)
  - Uses the Debian package / `kumina/libvirt_exporter`
- [13633-libvirt](https://grafana.com/grafana/dashboards/13633-libvirt/)
  - Last update 2020
  - Uses [bykvaadm/libvirt_exporter_improved](https://github.com/bykvaadm/libvirt_exporter_improved)
    (Last commit 2020)
- [15682-libvirt](https://grafana.com/grafana/dashboards/15682-libvirt/)
  - Last updated 2022
  - Uses `zhangjianweibj/prometheus-libvirt-exporter`
