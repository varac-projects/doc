# node-exporter

https://github.com/prometheus/node_exporter

* [node exporter alerts](https://lyz-code.github.io/blue-book/devops/prometheus/node_exporter/)

available in buster as .deb

Install smartmontools without recommnded packages, otherwise it will install
the `mailutils` (default) or `bsd-mailx` package.

    sudo apt install --no-install-recommends  smartmontools
    sudo apt install  prometheus-node-exporter

## TLS auth

* https://medium.com/@dominicafuwape/tls-client-authentication-for-node-exporter-3a0b029f4d6d
* https://www.robustperception.io/using-letsencrypt-with-the-node-exporter

## Textfile collector

https://github.com/prometheus/node_exporter#textfile-collector

    cat /var/lib/prometheus/node-exporter/README.textfile

Default path set by node-exporter installed as debian package:

    /var/lib/prometheus/node-exporter

### Push to prometheus

A custom, ansible managed systemd service `push-prometheus-metrics.service` is triggered by
`push-prometheus-metrics.timer` on a regular basis and pushes all node-exporter
metrics to `https://prometheus-pushgateway.oas.varac.net`,
using `/usr/local/bin/push-prometheus-metrics`.

### Community collector scripts

https://github.com/prometheus-community/node-exporter-textfile-collector-scripts
checked out in `~/project/monitoring/prometheus/exporters/node-exporter-textfile-collector-scripts`

## Issues

### Missing temp metrics on raspberry

> Node exporter apparently doesn't track this metric and this was super easy to do by myself.

https://github.com/fgrosse/pi-temp

## Metrics

### CPU count

https://github.com/prometheus/node_exporter/blob/master/example-rules.yml#L4:

    count(node_cpu{mode="idle"}) without (cpu,mode)
