# Prometheus

<https://prometheus.io/docs/introduction/overview/>

## Server config

```sh
export URL=http://localhost:9090
```

Show config:

```sh
curl ${URL}/config
```

server cli options

i.e. `--log.level=debug`

## Query Examples

Query alerts:

```sh
curl ${URL}/api/v1/alerts | jq .data.alerts
```

Query examples:

```sh
curl -g "${URL}/api/v1/query?query={'up'}"
curl -g "${URL}/api/v1/query?query={'up'}" | jq '.data.result[0]'
curl -g "${URL}/api/v1/query?query={job='brix-node-exporter'}[7d]"
```

## promtool

- Only installable via [GitHub release](https://github.com/prometheus/prometheus/releases)
- <https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/#syntax-checking-rules>

Install:

```sh
eget --file=promtool prometheus/prometheus
```

Setup env var:

```sh
export URL=https://...
promtool query instant ${URL} up
```

### Use promtool from prometheus container

Set alias:

```sh
alias promtool='kubectl -n monitoring exec -it -c prometheus \
  statefulset/prometheus-kube-prometheus-stack-prometheus -- promtool'
alias prom_query='kubectl -n monitoring exec -it -c prometheus \
  statefulset/prometheus-kube-prometheus-stack-prometheus -- promtool query instant ${URL}:9090'

promtool query instant ${URL} node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate
```

### Check rules

- [Syntax-checking rules](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/#syntax-checking-rules)

## cli clients

### Stale / unmaintained

- <https://github.com/mtulio/prometheus-cli>
- <https://github.com/ryotarai/prometheus-query>
- <https://github.com/prometheus-junkyard/prometheus_cli>

## Helm chart

Issues:

- [Invalid Scrape Target in 'kubernetes-pods' for Pods with Multiple Containers](https://github.com/prometheus/prometheus/issues/3530)

## Queries

<https://prometheus.io/docs/prometheus/latest/querying/basics/>
<https://prometheus.io/docs/prometheus/latest/querying/examples/>
Examples:

```sh
avg_over_time(node_memory_MemAvailable_bytes[1m])/1024/1024
```

Show failed blackbox exporter queries:

```sh
probe_success==0
```

## Data maintenance

- [Analyzing Prometheus data with external tools](https://medium.com/@valyala/analyzing-prometheus-data-with-external-tools-5f3e5e147639)

- [Dropping metrics at scrape time with Prometheus](https://www.robustperception.io/dropping-metrics-at-scrape-time-with-prometheus)

List all metric names:

```sh
curl "${URL}/api/v1/label/__name__/values" | jq .
```

List all metric names starting with `stackdriver`:

```sh
curl -G "${URL}/api/v1/label/__name__/values" --data-urlencode "match[]={__name__=~'stackdriver.+'}" | jq .
```

List all metric names matching a label:

```sh
curl -G "${URL}/api/v1/label/__name__/values" --data-urlencode \
  "match[]={__name__=~'.+', pod='prometheus-stackdriver-exporter-77c9767477-dz64s'}" | jq .
```

### Admin API

For some tasks (i.e. metric deletion) the admin API needs to get activated.

Docker compose:

Add the flag to the `command` list:

```sh
command:
  - '--web.enable-admin-api'
  ...
```

Kubernetes:

Set `enableAdminAPI: true` in `prometheus/kube-prometheus-stack-prometheus`:

```sh
kubectl -n monitoring edit prometheus kube-prometheus-stack-prometheus
```

### Delete metrics

[Prometheus: Delete Time Series Metrics](https://www.shellhacks.com/prometheus-delete-time-series-metrics/)

Note: The admin APIs needs to be enabled, see above how to do this.

```sh
export QUERY="{instance=~'mediaplayer.cas.*'}"
export QUERY="{__name__=~'stackdriver.*'}"
```

Search for old time series:

```sh
curl --netrc -g "${URL}/api/v1/query?query=${QUERY}[7d]"
```

Mark metrics matching a regex for deletion:

```sh
curl --netrc -X POST -g "${URL}/api/v1/admin/tsdb/delete_series?match[]=${QUERY}"
```

Delete all metrics until a unix timestamp:

```sh
curl --netrc -X POST -g "${URL}/api/v1/admin/tsdb/delete_series?match[]={__name__=~\".+\"}&end=1725208202"
```

Actually delete them:

```sh
curl --netrc -XPOST -g "${URL}/api/v1/admin/tsdb/clean_tombstones"
```

## Mixins

<https://github.com/monitoring-mixins/docs>

> A mixin is a set of Grafana dashboards and Prometheus rules and alerts,
> packaged together in a reuseable and extensible bundle. Mixins are written
> in jsonnet, and are typically installed and updated with jsonnet-bundler.

You can either [generate mixins by hand](https://github.com/monitoring-mixins/docs#generate-config-files)
or use the [pre-generates alerts](https://github.com/monitoring-mixins/website/tree/master/assets).

```sh
cd ~/projects/monitoring/prometheus/mixins/monitoring-mixins-website
gup
```

## Prometheus-operator helm chart

### Uninstall

<https://github.com/helm/charts/tree/master/stable/prometheus-operator#uninstalling-the-chart>

```sh
helm delete --purge oas-test-prometheus
kubectl delete crd prometheuses.monitoring.coreos.com \
  prometheusrules.monitoring.coreos.com \
  servicemonitors.monitoring.coreos.com \
  podmonitors.monitoring.coreos.com \
  alertmanagers.monitoring.coreos.com
```

evtl.

```sh
kubectl delete -n oas persistentvolumeclaims \
  prometheus-prometheus-oas-test-prometheus-promet-prometheus-0 \
  alertmanager-alertmanager-oas-test-prometheus-promet-alertmanager-0
```

## Recording rules

- [Prometheus docs: Defining recording rules](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/)
- [Prometheus docs: Best practices for recording rules](https://prometheus.io/docs/practices/rules/#recording-rules)
