# Prometheus alert rules

## Public alert rule collections

### Mixins

https://monitoring.mixins.dev/

### Awesome Prometheus alerts

https://awesome-prometheus-alerts.grep.to
https://github.com/samber/awesome-prometheus-alerts/blob/master/_data/rules.yml

    cd ~/projects/monitoring/prometheus/awesome-prometheus-alerts/_data

Show categories

    ❯ dasel select -f rules.yml -m "groups.[*].name" | head
    Basic resource monitoring
    Databases and brokers
    Reverse proxies and load balancers
    Runtimes
    Orchestrators
    Network and storage
    Other

Sub-categories:

    ❯ dasel select -f rules.yml -m "groups.(name=Basic resource monitoring).services.[*].name"
    Prometheus self-monitoring
    Host and hardware
    Docker containers
    Blackbox
    Windows Server
    VMware
    Netdata

Rules from `Basic resource monitoring` category:

    dasel select -f rules.yml -m "groups.(name=Other).services.[*].exporters"

Rules from the `Basic resource monitoring`/`Prometheus self-monitoring` sub-category:

    dasel select -f rules.yml -m "groups.(name=Basic resource monitoring).services.(name=Prometheus self-monitoring).exporters"
