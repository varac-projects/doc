# OpenMetrics

> OpenMetrics a specification built upon and carefully extending Prometheus exposition format in almost 100% backwards-compatible ways.

* [github repo](https://github.com/OpenObservability/OpenMetrics)
* [specs](https://github.com/OpenObservability/OpenMetrics/blob/main/specification/OpenMetrics.md)
*
