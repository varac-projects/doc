# Alertmanager

- [Github](https://github.com/prometheus/alertmanager)
- [Docs](https://prometheus.io/docs/alerting/alertmanager/)

## Config

Enable debug logging:

- Add `--log.level=debug` as cli parameter

### Routes

- [Routing tree editor](https://prometheus.io/webtools/alerting/routing-tree-editor/)

## Alternative Web-Interfaces / Dashboards

- [karma](https://github.com/prymitive/karma)

## Community managed alerts

- [Awesome prometheus alerts](https://awesome-prometheus-alerts.grep.to/)

## amtool

- [amtool Github](https://github.com/prometheus/alertmanager#amtool)

> amtool is a cli tool for interacting with the Alertmanager API.
> It is bundled with all releases of Alertmanager

Aliases based on context:
Docker:

```sh
alias amtool='docker exec alertmanager amtool --alertmanager.url http://localhost:9093'
```

Kubernetes prometheus:

```sh
alias amtool='kubectl -n oas exec -it -c prometheus-alertmanager \
  deployment/prometheus-alertmanager -- amtool --alertmanager.url http://localhost:9093'
```

Kubernetes kube-prometheus-stack:

```sh
alias amtool='kubectl -n monitoring exec -it -c alertmanager \
  statefulset/alertmanager-kube-prometheus-stack-alertmanager -- \
  amtool --alertmanager.url http://localhost:9093'
```

Inside pod:

```sh
alias amtool='amtool --alertmanager.url http://localhost:9093'
```

Show alerts:

```sh
amtool alert query
amtool alert query -o json | json_pp
amtool alert query instance='https://example.org'
```

Show silences: `amtool silence query`
Silence alert (expiry defaults to 1h):

```sh
amtool silence add instance='https://example.org' -c 'Upstream issue'
amtool silence add 'instance=~.*example.org' -c 'Upstream issue' -d 3d
```

Expire (delete) silence:

```sh
amtool silence expire 82d7b097-b21d-40b0-ad77-4397afe09531
```

Add a test alert

```sh
amtool alert add alertname=foo node=bar
```

## API

- [API spec source](https://github.com/prometheus/alertmanager/blob/main/api/v2/openapi.yaml)
- [APT spec in koumoul](https://koumoul.com/openapi-viewer/?url=https://raw.githubusercontent.com/prometheus/alertmanager/master/api/v2/openapi.yaml&proxy=false)

Query status

```sh
curl --netrc https://alertmanager.oas.varac.net/api/v2/status | jq .
```

All alerts:

```sh
curl --netrc https://alertmanager.oas.varac.net/api/v2/alerts | jq .
```

Filter out silenced alerts:

```sh
curl --netrc 'https://alertmanager.oas.varac.net/api/v2/alerts?silenced=false' | jq .
```

Filter out silenced and watchdog alerts:

```sh
curl -Ssl --netrc 'https://aldertmanager.oas.varac.net/api/v2/alerts?silenced=false&receiver=email' | jq length
```

Sending out a test alert mail:

```sh
curl -H "Content-Type: application/json" \
  -d '[{"labels":{"alertname":"TestAlert1"}}]' localhost:9093/api/v1/alerts
```

## Alertmanager bots

Alertmanager by itself only supports a handful of 3rd party services/protocols
directly. Unfortunatly matrix, signal etc are not supported.
The [webhook-reciever](https://prometheus.io/docs/operating/integrations/#alertmanager-webhook-receiver)
can be used to integrate middle-bots to reach more services.

### Multi-messenger bots

- [alertmanager bot](https://github.com/metalmatze/alertmanager-bot)
  - Generic, interactive chat-bot.
  - [Only supports telegram for now](https://github.com/metalmatze/alertmanager-bot#missing)

### Matrix

- [matrix-alertmanager](https://github.com/jaywink/matrix-alertmanager)

Test matrix-alertmanager:

```sh
curl -H "Content-Type: application/json" -XPOST \
  -d @~/projects/monitoring/prometheus/alertmanager/webhook-test.json \
   'alertmanager.example.org:3001/alerts?secret=...'
```

- The [official matrix go-neb bot](https://github.com/matrix-org/go-neb/) has
  alertmanager support
- [matrix-alertmanager-receiver](https://git.sr.ht/~fnux/matrix-alertmanager-receiver)
  - no docker image
- Stale, non-maintained:
  - [alertmanager-matrix-receiver](https://gitlab.com/e271/alertmanager-matrix-receiver)

### SMS

- [sachet](https://github.com/messagebird/sachet)

### Signal

- [altermanager-webhook-signald](https://github.com/dgl/alertmanager-webhook-signald)

### Mattermost

- [alertmanager-plugin](https://integrations.mattermost.com/alertmanager-plugin/)

### Alertmanager libraries

- [PylertAlertManager](https://github.com/ABORGT/PylertAlertManager)
  Doesn't support basic auth
