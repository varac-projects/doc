# Customizing prometheus-operator

Use the [prometheus.prometheusSpec.additionalScrapeConfigs](https://github.com/helm/charts/tree/master/stable/prometheus-operator#prometheus)
helm chart value.

See also the prometheus-operator [example/additional-scrape-configs](https://github.com/coreos/prometheus-operator/tree/master/example/additional-scrape-configs)
