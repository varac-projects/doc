# Healthchecks

* Self-hosted https://github.com/healthchecks/healthchecks
* https://healthchecks.io/docs/configuring_prometheus/
* https://hub.docker.com/r/linuxserver/healthchecks
