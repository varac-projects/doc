# Rust

## Learn

- [Rust book](https://doc.rust-lang.org/book/)
- [Rustlings](https://github.com/rust-lang/rustlings/)
- [Rust by Example](https://doc.rust-lang.org/rust-by-example/)

## Install

### Rustup

> Rustup is the Rust Language's installer and primary front-end.

- Rustup metadata and toolchains will be installed into the Rustup
  home directory, located at: `~/.rustup`
- The Cargo home directory located at: `~/.cargo`
- The `cargo`, `rustc`, `rustup` and other commands will be added to
  Cargo's bin directory, located at: `~/.cargo/bin`

### Install rustup from <https://rustup.rs>

Install latest/nightly from [rustup.rs](https://rustup.rs/):

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

#### Install rustup as snap

> NOTA BENE: This is an experimental, unofficial, snap and should not be relied on.

```sh
sudo snap install rustup
```

## Install rustc + cargo from from system package repo

```sh
sudo apt install rustc
```

## Usage
