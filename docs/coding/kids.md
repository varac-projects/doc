# Coding for kids

## Courses

- [Hacker School](https://hacker-school.de/kurse/)
  - German based classes

## Fellowship programs

- [Youth Hacking 4 Freedom](https://fsfe.org/activities/yh4f/)
