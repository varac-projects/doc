# Mega-linter

- [Pre-commit hook](https://megalinter.io/latest/mega-linter-runner/#pre-commit-hook)
  - [.pre-commit-hooks.yaml](https://github.com/oxsecurity/megalinter/blob/main/.pre-commit-hooks.yaml)

## Run mega-linter locally

- [Run mega-linter-runner locally](https://megalinter.io/latest/mega-linter-runner/)

### npx

- No installation needed
- Starts `docker run …`, and fails with Podman with
  `Error: statfs /var/run/docker.sock: permission denied`
  - With Podman you'll need to run the container manually (see below)

```sh
$ npx mega-linter-runner --flavor ci_light \
  -e "'ENABLE=MARKDOWN,YAML'" -e 'SHOW_ELAPSED_TIME=true'
…
Command: docker run --platform linux/amd64 --rm \
  -v /var/run/docker.sock:/var/run/docker.sock:rw \
  -v $(pwd):/tmp/lint:rw \
  -e ENABLE=MARKDOWN,YAML \
  -e SHOW_ELAPSED_TIME=true \
  oxsecurity/megalinter-ci_light:v8
```

### Global install

```sh
npm install mega-linter-runner -g
```

### Run mega-linter with podman

- [Allow to run with podman instead of docker](https://github.com/oxsecurity/megalinter/issues/1160)

Usage:

```sh
docker run --rm -v $(pwd):/tmp/lint:rw oxsecurity/megalinter:v7
```

- [Very few linters require the podman socket (i.e. swiftlint)](https://github.com/oxsecurity/megalinter/issues/1160#issuecomment-1660079841):

```sh
systemctl --user start podman.socket
docker run --rm \
  -v /run/user/1000/podman/podman.sock:/var/run/docker.sock:rw \
  -v $(pwd):/tmp/lint:rw \
  -e ENABLE=YAML \
  oxsecurity/megalinter-ci_light:latest
```

## Disable linters and checks

Use env vars, i.e.:

Disable linters:

```sh
export DISABLE_LINTERS="SPELL_CSPELL,SPELL_LYCHEE,PYTHON_PYRIGHT,PYTHON_MYPY,PYTHON_PYLINT,PYTHON_FLAKE8"
```

Disable checks:

tba..

## Embedded linters

### ci_light image

- [ci_light image](https://megalinter.io/8.3.0/flavors/ci_light/)

#### jscpd

- [GitHub](https://github.com/kucherenko/jscpd)
- [cli docs](https://github.com/kucherenko/jscpd/tree/master/apps/jscpd)
  - [Config options](https://github.com/kucherenko/jscpd/tree/master/apps/jscpd#options)
- [Megalinter docs: jscpd](https://megalinter.io/8.3.0/descriptors/copypaste_jscpd/)
- [Megalinter .jscpd.json](https://github.com/oxsecurity/megalinter/blob/main/TEMPLATES/.jscpd.json)
- Config file example (`.jscpd.json`):

```json
{
  "threshold": 5,
  "ignore": ["charts/dark/crds/*.yaml", "**/templates/_helpers.tpl"]
}
```

Local usage:

```sh
npx jscpd --threshold 50 --exitCode 1 .
```

Issues:

- Megalinter fails when the number of clones is higher than 0, regardless of
  the configured `threshold`
  - [Ensure jscpd respects configured threshold](https://github.com/oxsecurity/megalinter/pull/2451)
  - [Consider a thresholdAbsolute Option](https://github.com/kucherenko/jscpd/issues/468)
