# Linting

## Umbrella linters

- [Githubs super-linter](https://github.com/github/super-linter). Shell/Python
- [mega-linter](https://nvuillam.github.io/mega-linter) is a hard-fork of
  GitHub Super-Linter, rewritten in python to add lots of additional features

## Code linters

### Prettier

- [Website](https://prettier.io/)
- [Plugins](https://prettier.io/docs/en/plugins)

## Repository linters

### repolinter

- [Website](https://todogroup.github.io/repolinter/)
  - [Github](https://github.com/todogroup/repolinter)
- Javascript
- [Container image](https://github.com/todogroup/repolinter/pkgs/container/repolinter)
- No arch package
- No release since 2021: [Cut new version](https://github.com/todogroup/repolinter/issues/299)

#### Install

First, install [github-linguist](https://github.com/todogroup/repolinter/blob/main/docs/axioms.md#linguist):

```sh
$ pamac install icu73
$ export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
$ export PATH="/home/varac/.local/share/gem/ruby/3.0.0/bin:${PATH}"
$ gem install github-linguist
$ github-linguist
92.72%  3567       Shell
7.28%   280        Ruby
```

Then install repolinter itself:

```sh
    npm install -g repolinter
```

#### Issues / PRs

- [feat: add any-file-contents rule](https://github.com/todogroup/repolinter/pull/290)
- Running `repolinter` locally opens a
  [QT linguist](https://doc.qt.io/qt-6/qtlinguist-index.html) GUI
  Solution: `sudo pacman -R qt5-tools`
- Uses `node-fetch` library which doesn't honor proxy configuration through env vars:
  [node-fetch does not work behind a proxy](https://github.com/node-fetch/node-fetch/issues/1770)
- Regex support is pretty limited

### ls-lint

- [Website](https://ls-lint.org/)
  - [GitHub](https://github.com/loeffel-io/ls-lint)
  - Golang
  - AUR package `ls-lint`

## semgrep

see [semgrep.md](../../software/semgrep.md)

## Languages

## Python

- see [python/linting.md](../python/linting.md)

### Formats / Markup languages

#### reStructuredText

##### rstcheck

- [github](https://github.com/myint/rstcheck)
- Last commit 2020-05. Lint only, cannot format/fix.
- But it's the only rst linter ALE supports.

##### Others

- [rstfmt](https://github.com/dzhu/rstfmt): Maintained, python
  - [Add pre-commit support](https://github.com/dzhu/rstfmt/pull/17)
  - [Broken due to Sphinx 4.0 release](https://github.com/dzhu/rstfmt/issues/12)
- [rst-lint](https://github.com/twolfson/restructuredtext-lint): Last commit
  2020-11. Python. Lint only, cannot format/fix.
