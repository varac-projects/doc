# FOSS dev

## Changelog

See coding/changelog.md

## Contributing guides

- https://github.com/nayafia/contributing-template

## Codes of conduct

https://www.contributor-covenant.org/

## Git linters

see `./git/linting.md`
