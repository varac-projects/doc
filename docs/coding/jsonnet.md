# jsonnet

* [Website](https://jsonnet.org)
* [jsonnet-bundler](https://github.com/jsonnet-bundler/jsonnet-bundler)

## Installation

Install `jsonnet-bundler`:

    brew install jsonnet-bundler

or

    go get github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb

Install `jsonnet`:

    apt install jsonnet

or

    pamac install jsonnet

## Usage

[Tutorial](https://jsonnet.org/learning/tutorial.html)



## Issues

* [jsonnet doesn't preserve order of object fields](https://github.com/google/jsonnet/issues/903)
