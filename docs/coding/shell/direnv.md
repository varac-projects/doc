# Direnv

- [Website](https://direnv.net/)

## Python venv support

Create and load python venv: `layout python`

## Dotenv support

    dotenv .env

Requires direnv>=2.26.0 (No packaged in Ubuntu/Debian so far):

    dotenv_if_exists .env

## Nvm integration

- [wiki: Node/Using nvm](https://github.com/direnv/direnv/wiki/Node#using-nvm)

Add `.config/direnv/direnvrc` with code from above documentation,
and add i.e. `use nvm 10.15.3` to your `envrc` file.
