# Nushell

- [Website](https://www.nushell.sh/)
- [Github](https://github.com/nushell/nushell)
- Discord channels
  - [Cool oneliners](https://discord.com/channels/601130461678272522/615253963645911060)
- Docs
  - [Book](https://www.nushell.sh/book/)
  - [Commands](https://www.nushell.sh/commands/)
  - [Dookbook](https://www.nushell.sh/cookbook/)
- [nu_scripts](https://github.com/nushell/nu_scripts)
  A place to share Nushell scripts with each other

## Config

- [direnv](https://www.nushell.sh/cookbook/direnv.html)

## Issues

- [http command don't seem to respect system HTTP/S proxy settings](https://github.com/nushell/nushell/issues/8847)
- [Tab completion only partially works](https://github.com/nushell/nushell/issues/10168),
  which makes it hard to learn
- [Chained echo cmds doesn't work](https://github.com/nushell/nushell/issues/740)
  Use `print` instead
