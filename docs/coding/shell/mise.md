# Mise

- [Website](https://mise.jdx.dev/)
- [Getting started](https://mise.jdx.dev/getting-started.html)
- [direnv suport](https://mise.jdx.dev/direnv.html)
- [vaults/secrets management](https://github.com/jdx/mise/issues/1359)
- State files: `~/.local/state/mise`

## Backends

- [Mise docs: Backends](https://mise.jdx.dev/dev-tools/backends/)
- [registry.toml](https://github.com/jdx/mise/blob/main/registry.toml)
  - See which backends are available for a tool
- [Alternate backends (brew, nix, npm, cargo, etc)](https://github.com/jdx/mise/discussions/1250)

> Because of the extra complexity of asdf tools and
> security concerns we are actively moving tools in the registry away
> from asdf where possible to backends like aqua and ubi
> which don't require plugins

## Install

Arch / Manjaro:

```sh
pamac install mise-bin
```

[Debian / Ubuntu](https://mise.jdx.dev/getting-started.html#apt)

Install `usage` package for tab-completion:

```sh
mise use -g usage
```

Configure [bash integration](https://mise.jdx.dev/getting-started.html#_2a-activate-mise):
Be sure to add it to your `.bash_profile` so non-interactive scripts find
the binaries paths.

## Shims

- [Shims](https://mise.jdx.dev/dev-tools/shims.html) dir: `~/.local/share/mise/shims`
- [Shims: How they work in mise-en-place](https://jdx.dev/posts/2024-04-13-shims-how-they-work-in-mise-en-place/)
- [Shims: Build a version manager](https://jdx.dev/posts/2024-04-13-shims-build-a-version-manager/)

## Plugins

- [Registry](https://mise.jdx.dev/registry.html)
  - [GitHub: Plugin list](https://github.com/mise-plugins/registry?tab=readme-ov-file#plugin-list)

List all available plugins:

```sh
mise plugin ls-remote
```

Install plugins:

```sh
mise plugin install node
mise plugin install python https://github.com/asdf-community/asdf-python
mise plugins install node https://github.com/mise-plugins/rtx-nodejs.git#v1.0.0
mise plugin install opentofu https://github.com/NorddeutscherRundfunk/asdf-opentofu#parse-legacy-file
```

### Terraform

Mise is able to read the Terraform version from a [legacy version file](https://mise.jdx.dev/configuration.html#legacy-version-files),
in this case `main.tf` file (must be there,
no other filename is parsed), and automatically install it on next Terraform
call. This way, no extra `.mise.toml` is needed.

**Attention**: If the `main.tf` is using [legacy provider version contraints](https://github.com/terraform-linters/tflint-ruleset-terraform/blob/v0.7.0/docs/rules/terraform_required_providers.md)
(i.e. `google = "5.35.0"`), mise will refuse to parse the `required_version`
silently !

Otherwise, you can of course use

```sh
mise use terraform@1.4.7
```

which will create/update a `.mise.toml` file though.

Install Terraform gloablly:

```sh
mise use --global terraform@1.4.7
```

#### Opentofu

```sh
mise use opentofu@1.4.5
```

## Environments

- [Docs: Environments](https://mise.jdx.dev/environments/)

## Templates

- [Templates](https://mise.jdx.dev/templates.html#exec-options)

### Exec function

- [Templates: Exec options](https://mise.jdx.dev/templates.html#exec-options)

Example how to source secrets (use cache options to speed up promts so the exec
is not evaluated each time):

```toml
[env]
ETC_PASSWORD = "{{exec(command='rbw get etc/password', cache_key='secret', cache_duration='1d')}}"
```

Mise Terraform issues:

- [Support changing the version file for terraform and opentofu](https://github.com/jdx/mise/issues/2602)

## Issues

- [`mise use` altering global config if local config does not exist](https://github.com/jdx/mise/issues/2259)
- [Add support for optional env files](https://github.com/jdx/mise/issues/1777)
