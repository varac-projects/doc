# Bash

Here Bash specifics only,
general shell scripting howto at `./shell-scripting.md`

## Safe/strict mode for scripts

http://redsymbol.net/articles/unofficial-bash-strict-mode/

    set -euo pipefail

## Check for bashism

- use `checkbashism`

## String Manipulation

- http://tldp.org/LDP/abs/html/string-manipulation.html

## Cut variable

    var='abc1:fgh'

    # cut from left
    echo ${a%:*}
    abc1

    # cut from right
    echo ${a#*:}
    fgh
