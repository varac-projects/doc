# Shell snippet managers

## pet

- [GitHub](https://github.com/knqyf263/pet)
- Config: `~/.config/pet/config.toml`
- Snippet file: `/home/varac/.config/pet/snippet.toml`


Install:

    pamac install pet-bin

Issues:

- [feat: support multiple default values](https://github.com/knqyf263/pet/pull/203)

## Other

- [keep](https://github.com/OrkoHunter/keep)
  - Python, last commit 2023-05
