# Shell overview

- [Working with JSON in Various Shells](https://blog.kellybrazil.com/2022/05/19/working-with-json-in-various-shells/)
  Good overview about next-gen shells (Stars as of 2023-09):
  - [Nushell](https://www.nushell.sh/): [26.2k stars](https://github.com/nushell/nushell)
  - [Fish](https://fishshell.com/): [22k stars](https://github.com/fish-shell/fish-shell)
  - [Elvish](https://elv.sh/): [5.1k stars](https://github.com/elves/elvish)
  - [Oil](https://www.oilshell.org/): [2.6k stars](https://github.com/oilshell/oil)
  - [Murex](https://murex.rocks/): [1.3k stars](https://github.com/lmorg/murex)
  - [Next Generation Shell/NGS](https://ngs-lang.org/): [1.2k stars](https://github.com/ngs-lang/ngs)

## Startup files

- [Shell startup scripts overview](https://blog.flowblok.id.au/2013-02/shell-startup-scripts.html)

## Linting / code style

- for general shell linting, use `shellcheck`
- for bash, use `checkbashism`
  - see [bash.md](./bash.md) for more details on `bashism`
- Also: `shfmt`

### Linting in Vim

null-ls uses `shellcheck`

#### Ignoring

- [Ignore](https://github.com/koalaman/shellcheck/wiki/Ignore)
- [ShellCheck directives](https://github.com/koalaman/shellcheck/wiki/Directive)

Ignore a line:

```sh
# shellcheck disable=SC2086
```

### Shellcheck

- [Website](https://www.shellcheck.net/)
  - [Wiki](https://www.shellcheck.net/wiki/Home)
  - [Wiki Sitemap](https://www.shellcheck.net/wiki/)
- [Github](https://github.com/koalaman/shellcheck)
- [shellcheck precommit hooks](https://github.com/koalaman/shellcheck-precommit)

#### Shellcheck fixing support

Still no native [automitic fixing](https://github.com/koalaman/shellcheck/issues/1220)
but as a [workaround](https://github.com/koalaman/shellcheck/issues/1220#issuecomment-614794078):

```sh
shellcheck -f diff test.sh |git apply
```

For [multiple files](https://github.com/koalaman/shellcheck/issues/1220#issuecomment-1372717048):

```sh
find . -type f -iname '*.sh' -printf '%P\0' | xargs -0 shellcheck -f diff | git apply
```

There's also an example to use [parallel jobs](https://github.com/koalaman/shellcheck/issues/1220#issuecomment-937763190).

### beautysh

[Github](https://github.com/lovesegfault/beautysh)

## Quoting variables

<https://github.com/koalaman/shellcheck/wiki/SC2086>

Use array to pass i.e. cmd-line options:

```sh
opts=(-v --debug)
leap "${opts[@]}"
```

## AWK

### Felder herausschneiden

```sh
awk -F'<td>' '{ print $7 }'
```

## Parse File

```sh
while read line; do
  DATE=`echo $line|cut -d " " -f 6-7`
  FILE=`echo $line|cut -d " " -f 8-`
  echo "$DATE, $FILE"
done < $TMPFILE
```

## Sonderzeichen eleminieren

```sh
sed -e "s/[^[:digit:][:alpha:][:space:].,]//g"
```

## Write to file

```sh
  cat > /home/billy/test << EOF
  Hello World!
  This is my stupid text file.
  EOF
```

## Variables

### Default Werte

```sh
arch=${4:-x86_64}
mem=${5:-512}
```

## Usage

```sh
Usage() {
  cat <<EOF
  Usage: ${0##*/} [ options ] output user-data [meta-data]
}
```

## Parse commandline arguments

- see /usr/share/doc/util-linux/examples or `man getopt`

## Short opts

```sh
short_opts="hi:d:f:m:o:v"
long_opts="disk-format:,dsmode:,filesystem:,help,interfaces:,output:,verbose"
getopt_out=$(getopt --name "${0##*/}" \
       --options "${short_opts}" --long "${long_opts}" -- "$@") &&
       eval set -- "${getopt_out}" ||
       bad_Usage
while [ $# -ne 0 ]; do
       cur=${1}; next=${2};
       case "$cur" in
               -h|--help) Usage ; exit 0;;
               -v|--verbose) VERBOSITY=$((${VERBOSITY}+1));;
               -d|--disk-format) diskformat=$next; shift;;
               -f|--filesystem) filesystem=$next; shift;;
               -m|--dsmode) dsmode=$next; shift;;
               -i|--interfaces) interfaces=$next; shift;;
               --) shift; break;;
       esac
       shift;
done
#
## check arguments here
## how many args do you expect?
[ $# -ge 2 ] || bad_Usage "must provide output, userdata"
[ $# -le 3 ] || bad_Usage "confused by additional args"
#
output=$1
userdata=$2
metadata=$3
```

## Error handling

```sh
error() { echo "$@" 1>&2; }
errorp() { printf "$@" 1>&2; }
fail() { [ $# -eq 0 ] || error "$@"; exit 1; }
failp() { [ $# -eq 0 ] || errorp "$@"; exit 1; }
```

### check for exit code

```sh
if $(echo 'hi'|grep -q h); then; echo true; fi
if $(echo 'hi'|grep -q o); then; echo true; fi
```

## Add timestamps to output

If the -i or -s switch is passed, ts timestamps incrementally instead.
In case of -i, every timestamp will be the time elapsed since the last timestamp.
In case of -s, the time elapsed since start of the program is used.

```sh
unbuffer leap --yes  --force deploy donkey | ts -i
```
