# Bash testing frameworks

- [Wikipedia: List of unit testing frameworks / Bash](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#Bash)
- [bats-core](./bats.md)
  - Most active, most contributors
- [bash_unit](https://github.com/pgrange/bash_unit)

## Stale

- [bach](https://github.com/bach-sh/bach)
  - Last release 2022-11
- [shunit2](https://github.com/kward/shunit2/)
  - Last release 2020
- [shellspec](https://github.com/shellspec/shellspec)
  - Last commit 2022
- [shtk](https://github.com/jmmv/shtk)
  - Last release 2017
