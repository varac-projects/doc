# Bats

- [Github](https://github.com/bats-core/bats-core)
- [Docs](https://bats-core.readthedocs.io/en/stable/)
- [How to combine bats and pytest](https://mysqlaz.wordpress.com/2017/09/19/how-to-combine-bats-pytest-and-x-plugin-for-tests/)
- [Unit testing BASH](https://ggdx.co.uk/tooling/unit-testing-bash/)
  - Good examples

## Install

```sh
sudo pacman -S bash-bats bash-bats-assert
```

### Helper libraries

- [How can I run helper libraries like assert](https://bats-core.readthedocs.io/en/stable/faq.html#how-can-i-use-helper-libraries-like-bats-assert)
- [Assert](https://github.com/bats-core/bats-assert)
- [File](https://github.com/bats-core/bats-file)
  Common filesystem assertions for Bats
- [bats-detik](https://github.com/bats-core/bats-detik)
  A library to ease e2e tests of applications in K8s environments
