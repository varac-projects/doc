# Dotenv files

See also [direnv.md](./direnv.md) how to auto-load `.env` files

## Source .env from bash

Best solution so far:

```sh
set -a && source .env && set +a
```

[Using export](https://stackoverflow.com/a/20909045)
(is able to ignores comments):

```sh
export $(grep -v '^#' .env | xargs)
```

Also handles **values with spaces**, but beware:
`-d` is not supported in busybox (i.e. some Gitlab CI containers):

```sh
export $(grep -v '^#' .env | xargs -d '\n')
```

## Source dotenv file in Gitlab CI

Add this command to the beginning of the job script:

```yaml
script:
  - |
    [ -f .env ] && export $(grep -v '^#' .env | xargs)
```

## Python

- [python-dotenv](https://pypi.org/project/python-dotenv/)
