# ZSH

- [Zsh Configuration Files](https://www.baeldung.com/linux/zsh-configuration-files)

## Plugins

- [awesome-zsh-plugins](https://github.com/unixorn/awesome-zsh-plugins)

### zsh plugin managers

- [archlinux wiki zsh plugin managers](https://wiki.archlinux.org/title/zsh#Plugin_managers)
- [Yet another "which plugin manager" question](https://www.reddit.com/r/zsh/comments/u1gbkl/yet_another_which_plugin_manager_question/)
- [zsh-plugin-manager-benchmark](https://github.com/rossmacarthur/zsh-plugin-manager-benchmark)

#### zinit

- Abandonned, rescued, reanmed to [zi](https://github.com/z-shell/zi)
- [zdharma-continuum/zinit](https://github.com/zdharma-continuum/zinit)

Install:

<https://github.com/zdharma/zinit#option-2---manual-installation>

```sh
git clone https://github.com/zdharma/zinit.git ~/.zinit/bin
```

Usage:

#### Other options

- [antidote](https://github.com/mattmc3/antidote)
- [sheldon](https://github.com/rossmacarthur/sheldon): Rust

#### Deprecated

- [antibody](https://github.com/getantibody/antibody)
- [antigen](https://github.com/zsh-users/antigen)
- [zgen](https://github.com/tarjoilija/zgen) Abandoned

## Alternative config dir

```console
$ cat .zshenv
export ZDOTDIR=~/.config/zsh
```

Start zsh with new config:

```sh
export ZDOTDIR=$(mktemp -d) && zsh
```

## ZSH completion

- [zsh-completions-howto.org](https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org)
- [Arch wiki: Zsh/command completion](https://wiki.archlinux.org/title/Zsh#Command_completion)
- [Zshc ompletions howto](https://github.com/zsh-users/zsh-completions/blob/master/zsh-completions-howto.org)
- Easier than the [Official zsh completion docs](http://zsh.sourceforge.io/Doc/Release/Completion-System.html)
- [Zsh guido: Completion, old and new](http://zsh.sourceforge.net/Guide/zshguide06.html)

Default completion locations:

- `/usr/share/zsh/functions/Completion/`
-

Custom completion location:

- `~/.config/shell/zsh/completion` (added in `~/.config/shell/zsh/conf.d/05_completion.sh`)

Show current completion locations:

```sh
echo $fpath | tr ' ' '\n' | grep -i completion
```

Create/update completions in `$ZDOTDIR/completion`:

```sh
~/bin/custom/zsh_create_completions.sh
```

```sh
man zshcompsys
```

For active system-wide completions see

- `/usr/share/zsh/functions/Completion/`
- `/usr/share/zsh/site-functions/`
- and custom ones at `~/.zsh/completion/`

<https://github.com/zsh-users/zsh-completions>

## Bash completetion compatibility

<https://stackoverflow.com/a/8492043>

## Profiling zsh startup time

- <https://kevin.burke.dev/kevin/profiling-zsh-startup-time/>
- <https://github.com/ohmyzsh/ohmyzsh/issues/5327>
- <https://blog.jonlu.ca/posts/speeding-up-zsh>

Measure time:

```console
$ time  zsh -i -c exit
Before: 2,05s user 0,46s system 95% cpu 2,624 total
```

Start verbose:

```sh
zsh -xv
```

Show zprof profile stats (needs to get enabled on top of `.zshrc`)

```sh
zprof
```

## Colors

[Zsh color codes](https://wiki.archlinux.org/title/zsh#Colors)

```sh
echo "$(tput setaf 1)~~> Red $(tput sgr0)"
```

## Nvim zsh support

- Still open: [shellcheck: Support zsh](https://github.com/koalaman/shellcheck/issues/809)
- Still open: [bash-language-server: Handle zsh syntax](https://github.com/bash-lsp/bash-language-server/issues/252)
