# Java

- [Arch wiki: Java](https://wiki.archlinux.org/title/java)
- [jenv](https://github.com/jenv/jenv)

## Run .jar

```sh
java -jar yed-3.16/yed.jar
```
