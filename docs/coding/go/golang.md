# Golang

## Install

```sh
apt install golang
```

Export GOPATH env var:

```sh
echo 'export GOPATH="$HOME/golang"' >> ~/.bashrc
```

Add

```sh
echo 'export PATH="$PATH:${GOPATH}/bin/"' >> ~/.bashrc
```

## Usage

### Using external modules

```sh
go mod init grabana
go mod tidy
go run ./main.go
```

### Build an existing project

```sh
go build
```

### Install package system-wide

```sh
go get github.com/prometheus/prom2json/cmd/prom2json
```

### Install latest git master

Install all deps:

```sh
go get -d ./...
```

### Run single file

```sh
go run test_hello_world.go
```

## Handle multiple go versions

### Natively

Install go:

```sh
go install golang.org/dl/go1.19.1@latest
rehash
go1.19.1 download
alias go=go1.19.1
go version
```

### goenv

- [Install](https://github.com/syndbg/goenv/blob/master/INSTALL.md)

Issues:

- [goenv-commands: command not found](https://github.com/syndbg/goenv/issues/246)

### Stale goenv alternatives

- [gvm](https://github.com/moovweb/gvm)
- [crsmithdev/goenv](https://github.com/crsmithdev/goenv): Stalled 2015
