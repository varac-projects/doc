# Gnu make

## Makefile alternatives

### Task / Taskfile

Makefile alternative

- [Website](https://taskfile.dev)
- [Notable projects using taskfile](https://github.com/go-task/task/discussions/751)
- Recent commits and releases
- Uses go templates and the [sprig](http://masterminds.github.io/sprig/)
  template functions
- [Taskfile.nvim](https://github.com/mistweaverco/Taskfile.nvim)
  A Vim/Neovim plugin to run Taskfile tasks, VimScript

### Install

- [Instalation](https://taskfile.dev/installation/)
- [No Binaries for armv7 / Raspberry Pi](https://github.com/go-task/task/issues/842)
- [Not packaged in Debian/Ubuntu](https://github.com/go-task/task/issues/1698)

Arch:

```sh
pamac install go-task-bin
```

With mise:

```sh
mise use --global task
```

### Completion

- [Setup completions](https://taskfile.dev/installation/#setup-completions)

### Usage

[How to properly escape variable substitution](https://github.com/go-task/task/issues/966#issuecomment-1363701654)

```sh
{{`{{.State.Status}}`}}
```

### Issues

#### Variables

- [enforce & check required variables](https://github.com/go-task/task/issues/1203)
  - [related PR](https://github.com/go-task/task/pull/1204)

### Other alternatives

- [tusk](https://github.com/rliebz/tusk)
  - Go
  - Recent commits, last release 2022
  - No arch/AUR package
- [just](https://github.com/casey/just)

  - Rust
  - Recent commits and releases
  - [Neovim/Treesitter support](https://github.com/casey/just#vim-and-neovim)
  - Available as Arch package as `community/just`

- [Mise tasks](https://mise.jdx.dev/tasks/)

#### Stale

- [orbit](https://github.com/gulien/orbit)
