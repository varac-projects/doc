# Python logging

## Colored logging

## structlog

- [GitHub](https://github.com/hynek/structlog)
- [Docs](https://www.structlog.org/en/stable)
  - [Getting Started](https://www.structlog.org/en/stable/getting-started.html)

Install:

```sh
sudo pacman -S python-structlog python-rich
```

Set default log level:

```python
structlog.configure(
  wrapper_class=structlog.make_filtering_bound_logger(logging.DEBUG)
)
```

Change default from `stdout` to `stderr`:

```python
structlog.configure(logger_factory=structlog.PrintLoggerFactory(sys.stderr))
```

Hello World example:

```python
import structlog
log = structlog.get_logger()
log.info("hello, %s!", "world", key="value!", more_than_strings=[1, 2, 3])
```

### Other options

- [borntyping/python-colorlog](https://github.com/borntyping/python-colorlog)
  - Last update 2024-01
- [xolox/python-coloredlogs](https://github.com/xolox/python-coloredlogs)
  - Last update 2021

## Libraries using logging

see `python3-urllib3`:

```sh
grep logg -ir /usr/lib/python3/dist-packages/urllib3
```

Initialised in `/usr/lib/python3/dist-packages/urllib3/__init__.py`:

Modules used this:

```python
import logging
log = logging.getLogger(__name__)
```
