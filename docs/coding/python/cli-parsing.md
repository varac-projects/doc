# Python cli option parsers

## argparse

- [Python docs: argparse](https://docs.python.org/3/library/argparse.html)
- Built-in
- Replaces old `optparse`
- [Stackoverflow tutorial](https://stackoverflow.com/a/30493366)

## Plac

> plac is a Python package that can generate command line parameters from
> function signatures.

- [GitHub](https://github.com/ialbert/plac)
- External dependency
