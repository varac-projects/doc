# Python linters

## Ruff

- [Ruff](https://github.com/astral-sh/ruff)
- [Docs](https://docs.astral.sh/ruff/)
  - [Configuration](https://docs.astral.sh/ruff/configuration/)
  - [All settings](https://docs.astral.sh/ruff/settings/)
  - [Rules](https://docs.astral.sh/ruff/rules/)
- Included in [LazyVim python lang extra](https://www.lazyvim.org/extras/lang/python)
- Container image i.e.: `registry.gitlab.com/pipeline-components/ruff:latest`
- [pre-commit hook](https://github.com/astral-sh/ruff-pre-commit)

### Issues

- [FR: Upstream/preset/profile to preconfigure options](https://github.com/astral-sh/ruff/issues/809)
- [Support line filter / diff input](https://github.com/astral-sh/ruff/issues/1931)
  - [Support baselines](https://github.com/astral-sh/ruff/issues/1149)

## Pyright

- Included in [LazyVim python lang extra](https://www.lazyvim.org/extras/lang/python)
