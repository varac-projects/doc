# pdm

- [Docs](https://pdm.fming.dev/latest/)
  - [Working with Virtual Environments](https://pdm-project.org/latest/usage/venv/)
- [Support for PEP 621](https://github.com/python-poetry/roadmap/issues/3)
  ([PEP 621 – Storing project metadata in pyproject.toml](https://peps.python.org/pep-0621/)),
  which Poetry still haven't
- [Pip docs: pyproject.toml](https://pip.pypa.io/en/stable/reference/build-system/pyproject-toml/)

## Install

```sh
sudo pacman -S python-pdm
```

## Setup in new project

```sh
pdm init
```

## Usage

Show details about pdm setup:

```sh
pdm info
pdm info --env
```

Install/add packages:

```sh
pdm add mkdocs-material
```

Update (and install) dependencies in lock file:

```sh
pdm update
```

Run dependency binaries:

```sh
pdm run esphome
.venv/bin/esphome
```
