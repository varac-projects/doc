# uv

- [Github](https://github.com/astral-sh/uv)
- [Docs](https://docs.astral.sh/uv/)

## Install

- [Docs: installation](https://docs.astral.sh/uv/getting-started/installation/#installation-methods)

Debian (not packaged for Debian yet) / generic:

```sh
curl -LsSf https://astral.sh/uv/install.sh | sh
```

Arch:

```sh
sudo pacman -S uv
```

## Usage

Initialize in existing project (creates `.python-version`, `pyproject.toml`. `hello.py`):

```sh
uv init --no-workspace .
```

Add a dependency (creates virtualenv at: `.venv`):

```sh
uv add "ansible<10"
uv add "httpx @ git+https://github.com/encode/httpx"

```

Add requirements from a `requirements.txt` file:

```sh
uv add -r requirements.txt
```

Update lockfile when i.e. adding dependencies manually to `pyproject.toml`:

```sh
uv lock
```

Install dependencies:

```sh
uv sync
```

## venv

[Docs: Project environments](https://docs.astral.sh/uv/concepts/projects/#project-environments)

To run a cmd inside the venv:

```sh
uv run ansible
```
