# Poetry package manager

- [Website](https://python-poetry.org/)
- [Why Is Poetry Essential to the Modern Python Stack?](https://andrewbrookins.com/python/why-poetry/)
- [Support for PEP 621](https://github.com/python-poetry/roadmap/issues/3)
  ([PEP 621 – Storing project metadata in pyproject.toml](https://peps.python.org/pep-0621/))

## Installation

`python3-poetry` deb package is only available in ubuntu jammy and upwards.
Until then:

```sh
brew install poetry
```

or

```sh
pip install poetry
```

## Usage

- [Basic usage](https://python-poetry.org/docs/basic-usage/)

### Setup

Either create a new project (creates a new directory structure and files)

```sh
poetry new
```

or
[initialise an existing project](https://python-poetry.org/docs/basic-usage/#initialising-a-pre-existing-project)
(Creates a `pyproject.toml` file)

```sh
poetry init -n
```

### Install packages

Add package:

```sh
poetry add pendulum
```

[Install deps including the current project](https://python-poetry.org/docs/basic-usage/#installing-dependencies)

```sh
poetry install
```

or only install dependencies from lock file

```sh
poetry install --no-root
```

### Virtual envs

- [Managing environments](https://python-poetry.org/docs/managing-environments/)

Show venv details:

```sh
poetry env info
```

Only show venv path:

```sh
poetry env info --path
```

Enable venv:

```sh
poetry env use python
```

[Disable virtualenv](https://github.com/python-poetry/poetry/issues/243#issuecomment-401131764):

```sh
poetry config virtualenvs.create false
```

Update `poetry.lock`:

```sh
poetry update
```

## Container usage

- [Blazing fast Python Docker builds with Poetry 🏃](https://medium.com/@albertazzir/blazing-fast-python-docker-builds-with-poetry-a78a66f5aed0)
