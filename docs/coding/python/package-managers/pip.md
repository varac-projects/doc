# Python package managers

## External package managers

- ./pdm.md
- ./pipx.md
- ./poetry.md
- [pixi.sh](https://pixi.sh/)

## pip

Install version:

    pip3 install --no-cache-dir midea_inventor_lib==1.0.4

Use git master:

    pip3 install --user git+https://github.com/mnubo/kubernetes-py.git

Use a branch:

    pip3 install --user git+https://github.com/tangentlabs/django-oscar-paypal.git@develop

[Install a non-merged PR](https://stackoverflow.com/questions/13561618/pip-how-to-install-a-git-pull-request)

    pip install git+https://github.com/user/repo.git@refs/pull/123/head

## pip tools

    pip install pip-tools
    pip-compile --upgrade
