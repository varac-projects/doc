# Python yaml

https://hackersandslackers.com/simplify-your-python-projects-configuration/

## yaml libs

### Builtin

a_yaml_file = open("example.yaml")

parsed_yaml_file = yaml.load(a_yaml_file, Loader=yaml.FullLoader)

### External libs

https://github.com/beetbox/confuse
