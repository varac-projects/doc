# Tox

## Run single test suite

    tox -e benchmark -- -x tests/benchmarks/test_sqlcipher.py

Beware that this leads to errors not sourcing the fixtures. Use mappings for that:

## Run specific test group/mapping

    tox -v -e benchmark -- -m benchmark_test_crypto_encrypt_doc

## Run single test

http://blog.jasonmeridth.com/posts/how-to-run-a-single-test-with-tox/

## Recreate venv

    tox --recreate
