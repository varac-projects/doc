# Pandas for data analysis

- [Website](https://pandas.pydata.org/)
- [Pandas Dataframe: Plot Examples with Matplotlib and Pyplot](https://queirozf.com/entries/pandas-dataframe-plot-examples-with-matplotlib-pyplot)
- [How do I create plots in pandas?](https://pandas.pydata.org/docs/getting_started/intro_tutorials/04_plotting.html)
- [Chart visualization](https://pandas.pydata.org/pandas-docs/stable/user_guide/visualization.html)
- [pola.rs](https://pola.rs/): Rust based alternative to pandas
