# Virtualenv

Install python3 venv:

```sh
pip3 install --user venv
```

or

```sh
apt install python3-venv
```

Then activate it:

```sh
python3 -m venv .venv
. .venv/bin/activate
```

## Activating a python3 venv though direnv

<https://github.com/direnv/direnv/wiki/Python#venv-stdlib-module>

Put this in `.envrc`:

```sh
echo 'layout python-venv' >> .envrc
direnv allow
```

Creates and activates a venv in `.direnv/python-venv-3.6.7` i.e.

see `/home/varac/projects/audio/spotify/.envrc`

```sh
pip install -e .
```

will install packages to `.direnv/python-venv-3.6.7/lib/python3.6/site-packages`
and binaries are located in `.direnv/python-venv-3.6.7/bin/`

Remove venv from old python versions (except current python version i.e. `3.9.5`):

First see which dirs are located:

```sh
for i in $(locate .direnv/python- | grep -E 'python-([0-9]{1,}\.)+[0-9]{1,}$' | grep -v '3\.9\.5'); do echo $i; done
```

then remove:

```sh
for i in $(locate .direnv/python- | grep -E 'python-([0-9]{1,}\.)+[0-9]{1,}$' | grep -v '3\.9\.5'); do rm -rf $i; done
```

## Issues

- `error: invalid command 'bdist_wheel'`: Install wheel, i.e. `pip3 install wheel`

## pipx

<https://github.com/cs01/pipx>
<https://pipxproject.github.io/pipx/docs/>

- python 3.6+ is required to install pipx

Install pipx:

```sh
apt install pipx
```

or the latest release:

```sh
curl https://raw.githubusercontent.com/cs01/pipx/master/get-pipx.py | python3
```

Install a dedicated release version:

```sh
pipx install git+https://github.com/pimutils/khal.git@v0.9.9
pipx install git+https://github.com/Flexget/Flexget.git@2.15.1

pipx upgrade git+https://github.com/pimutils/khal.git@v0.9.9
```

Install a non-merged PR (<https://github.com/TailorDev/Watson/pull/432>):

```sh
pipx install git+https://github.com/DavidOliver/Watson/@click-8
```

`pipx` install venvs to `~/.local/pipx/venvs`.

Install a particular version:

```sh
pipx install --spec spotdl==1.0.0 spotdl
```

## Pipsi

<https://github.com/mitsuhiko/pipsi>

Deprecated in favor of pipx.
