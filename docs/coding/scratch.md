# Scratch

## Desktop clients / Offline editors

- No [official Linux desktop client](https://scratch.mit.edu/download)
  - [scratux is dead](https://github.com/scratux/scratux/issues/24)

## Online editor

- [Online editor](https://scratch.mit.edu/projects/editor/?tutorial=getStarted)
