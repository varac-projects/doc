# ruby version manager

https://rvm.io/rvm


Cons:

* No debian package

## Installation of rvm

[Installation](https://rvm.io/rvm/install)
[On ubuntu using a ppa](https://github.com/rvm/ubuntu_rvm)
[Chain of trust](https://rvm.io/rvm/security)

Import gpg-keys:

    sudo apt install dirmngr gpg2
    gpg2 --keyserver hkps://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB

or

		curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
    curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import

Install stable rvm (adds sourcing of rvm script to `~/.profile`):

    curl -sSL https://get.rvm.io | bash -s stable

Install rvm system-wide:

> If the install script is run prefixed with sudo, RVM will automatically
> install into /usr/local/rvm


# Using rvm

## Install ruby version

		rvm list known
		rvm install 2.6

# Use default rvm ruby version

    rvm list
    rvm default

# Gemsets

    rvm gemset create puppet2.7
    rvm gemset list
    rvm gemset use puppet2.7
    rvm use default@puppet2.7

# Which rvm am i in ?

    ~/.rvm/bin/rvm-prompt
