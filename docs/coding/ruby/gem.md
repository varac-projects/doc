# rubygems

## use https

    gem sources # are you still using http or yet https ?
    gem sources -r http://rubygems.org/ # remove http
    gem sources -a https://rubygems.org/ # add https

sources cache is at `.gem/specs/`

## Show all versions of a remote gem

    gem list --remote ^puppet$ --all

## Install minor version of gem

    gem install puppet -v "~>3.8"

## Install gem from source

    gem install specific_install
    gem specific_install https://github.com/githubsvnclone/rdoc.git

## bundler

    bundle config set --local path 'vendor/bundle'
    bundle
