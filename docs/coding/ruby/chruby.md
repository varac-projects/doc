# chruby

Pros:

* support for [system-wide installations](https://github.com/postmodern/chruby#system-wide)

Cons:

* No debian package

### Usage

    echo "ruby-2.6" > .ruby-version

#### Etc

    /usr/local/bin/chruby-exec 2.6.6 -- bundle
    /usr/local/bin/chruby-exec system -- bundle

## Install/Setup chruby

    git clone https://github.com/postmodern/chruby ~/projects/coding/ruby/chruby
    cd ~/projects/coding/ruby/chruby
    sudo scripts/setup.sh

* Installs chruby binaries and related files into `/usr/local/*`
* Installs `/etc/profile.d/chruby.sh`

Auto-switching ruby version to the one specified in `.ruby-version`:
https://github.com/postmodern/chruby#auto-switching

Add `source /usr/local/share/chruby/auto.sh` to shell startup file


# ruby-install

    git clone https://github.com/postmodern/ruby-install ~/projects/coding/ruby/ruby-install
    cd ~/projects/coding/ruby/ruby-install
    sudo make install

## Usage


### Install ruby version

Install ruby version system-wide (`/opt/rubies`):

    sudo ruby-install ruby 2.6

## Issues

Ruby 2.3:

> Ich kann es lokal nicht mehr bauen (openssl), und auch auf den stretch servern lässt es sich zwar bauen, aber funktioniert nicht richtig (https://github.com/postmodern/ruby-install/issues/194).
