# Running tests verbose

    rake test TESTOPTS="-v"

# Running single test file

    be rake test TEST='./test/remote/nicknym_source_test.rb' TESTOPTS="-v"

# Running single test

    be rake test TEST='./test/remote/nicknym_source_test.rb' TESTOPTS="-v --name=test_successful_query"
