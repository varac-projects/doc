# rbenv

https://github.com/sstephenson/rbenv

Cons:

* [Doesn't support system-wide installs yet](https://github.com/rbenv/rbenv/pull/1155)

## Installtion

https://github.com/rbenv/rbenv#installation

    sudo apt install rbenv ruby-build

## Usage

    rbenv init


## Pin ruby version in project

    rbenv local 2.3.3

will create `.ruby-version` with content `2.3.3`

    gem install bundler

# ruby-build

https://github.com/rbenv/ruby-build

**Warning**: debian/ubuntu pakages are outdated, only debian testing ships and up to
             date package (as of 2020-04) !

That's why the recommended installation is by git repe, installed in `~/.rbenv/plugins/ruby-build`:

    $ mkdir -p "$(rbenv root)"/plugins
    $ git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

## OLD: as .deb


    sudo apt install ruby-build

## Get new ruby version

    rbenv install x.y.z

### Workaround for ruby < 2.4

From https://github.com/rbenv/ruby-build/wiki#openssl-usrincludeopensslasn1_mach102-error-error-this-file-is-obsolete-please-update-your-software:

> The openssl extension of Ruby version before 2.4 is not compatible with OpenSSL 1.1.x.

    rbenv install 2.3.3

# rbenv-bundler

https://github.com/carsomyr/rbenv-bundler


# Run programs as sudo

https://github.com/dcarley/rbenv-sudo

    rbenv sudo puppet agent -t

# rbenv gemset

https://github.com/jf/rbenv-gemset

    git clone git://github.com/jf/rbenv-gemset.git $HOME/.rbenv/plugins/rbenv-gemset

# systemd service using rbenv

https://github.com/AutogrowSystems/RedisAlerting/wiki/systemd-service-using-rbenv
