# Node.js

- [Arch wiki: Node.js](https://wiki.archlinux.org/title/node.js_)

## nvm

- [Github](https://github.com/nvm-sh/nvm)
- [Arch wiki: nodejs/ternate installations](https://wiki.archlinux.org/title/node.js#Alternate_installations)

Install:

```sh
sudo pacman -S nvm
```

Add this to shell startup file (i.e. my custom `~/.config/shell/common/interactive`):

```sh
export NVM_DIR="$HOME/.nvm"
[ -s "/usr/share/nvm/nvm.sh" ] && \. "/usr/share/nvm/nvm.sh"
```

### Usage

List available remote versions:

```sh
nvm ls-remote
```

Install a version:

```sh
nvm install 18
nvm install 18.13.0
```

The first version installed becomes the default.
New shells will start with the default version of node (e.g., nvm alias default).

Use a dedicated node version for current terminal session:

```sh
nvm use 18.13.0
```

### Use nvm together with direnv

See [direnv.md](shell/direnv.md)

## Task runners

### xrun CLI / @xarc/run-cli

- [npm package](https://www.npmjs.com/package/@xarc/run-cli)
- _must_ get installed globally

Install:

```sh
npm install -g @xarc/run-cli
```
