# Asahi Linux

* [Website](https://asahilinux.org/)
* [Github](https://github.com/AsahiLinux)
* [Wiki](https://github.com/AsahiLinux/docs/wiki)
* [Broken Software](https://github.com/AsahiLinux/docs/wiki/Broken-Software)
* [Alternative Distros](https://github.com/AsahiLinux/docs/wiki/SW%3AAlternative-Distros)
* [Asahi Linux reddit](https://www.reddit.com/r/AsahiLinux/)

## Install

* [Installing the Asahi Linux Alpha on my M1 Mac mini](https://www.jeffgeerling.com/blog/2022/installing-asahi-linux-alpha-on-my-m1-mac-mini)
* [The first Asahi Linux Alpha Release is here](https://asahilinux.org/2022/03/asahi-linux-alpha-release/)

Install one-liner:

    curl https://alx.sh | sh

### Issues

* After all, it was much too early to try (2023-12)
  * Post-install boot-loop when still attached to external display via USB-C
  * console login with plugged power adapter (WTF?)
  * speaker
  * Webrtc echo test
  * bluetooth

### Post-install setup

* Follow `./arch.md` installation instructions

* Remove alarm user (so UID 1000 gets freed)
Setup new user:

    useradd -m varac
    usermod -aG seat,sudo varac

* Change root pw

#### Post-install Issues

* Cannot install `sddm-git`: `The following packages are not compatible with your architecture`

## Install sway

* [Does SwayWM works?](https://www.reddit.com/r/AsahiLinux/comments/x4wuz1/does_swaywm_works/)
