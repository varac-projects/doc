# PopOS

## Wayland

* [Enable wayland](https://unix.stackexchange.com/a/701229)
* Enable wayland in `/etc/gdm3/custom.conf`
* Edit `/usr/lib/udev/rules.d/61-gdm.rules` and comment out both `RUN+`
  directives under LABELS `gdm_prefer_xorg` and `gdm_disable_wayland`
* Reboot

## Issues

* [Lemp9 freezes, requires hard reboot](https://github.com/pop-os/linux/issues/45#issuecomment-947392613)
  [related Reddit thread](https://www.reddit.com/r/System76/comments/nfbpoh/lemur_pro_and_kernel_811_lockup/)
  Quick fix: `intel_idle.max_cstate=4` kernel parameter
  Although [it still breaks live-cds](https://github.com/pop-os/linux/issues/45#issuecomment-1328961582)
