# Debian upgrading

## To bookworm

* [Upgrades from Debian 11 (bullseye)](https://www.debian.org/releases/stable/armhf/release-notes/ch-upgrading.en.html)

Start tmux session in case it's a remote session:

    tmux

First upgrade all packages in the old release:

    apt update
    apt upgrade -y

Revisit `/etc/apt/sources.list` and:

* remove unneeded lines
* Change all URLs to use `http://deb.debian.org/debian/`

Prepare update:

    sed -i 's/bullseye/bookworm/' /etc/apt/sources.list /etc/apt/sources.list.d/*
    sed -i "s/non-free/non-free-firmware/g" /etc/apt/sources.list
    rm /etc/apt/preferences.d/bookworm
    apt update

    apt full-upgrade -y
    apt full-upgrade -y
    apt autoremove --purge
    reboot

### Post-upgrade

#### apt

##### Deprecated apt key storage

    W: https://download.docker.com/linux/debian/dists/bookworm/InRelease:
       Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg),
       see the DEPRECATION section in apt-key(8) for details.

i.e. for docker repo:

    W: https://download.docker.com/linux/debian/dists/bookworm/InRelease: Key is stored in legacy trusted.gpg keyring

Fix:

    apt-key del 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo tee /etc/apt/trusted.gpg.d/docker.asc
    apt update

#### node-exporter openipmi alerts

Somehow some ipmi packages get installed during upgrade, so on non-IPMI servers
remove all IPMI packages:

    apt purge *ipmi*

### Issues

#### systemd-resolved

* No DNS resolution (see ../../systemd/resolved.md)
  * [systemd-resolved has been split into a separate package](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#systemd-resolved)

Fix:

    rm /etc/resolv.conf
    echo 'nameserver 1.1.1.1' > /etc/resolv.conf
    apt install systemd-resolved

#### non-free-firmware

[Apt keeps reminding about the move of non-free firmware to a different location](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split)
To silence this warning:

    echo 'APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";' > /etc/apt/apt.conf.d/no-bookworm-firmware.conf
