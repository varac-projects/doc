# Debian packaging

## Ad-hoc packaging from git repo without debian/ dir

    DEB_BUILD_OPTIONS=nocheck gbp buildpackage --git-ignore-branch --git-no-pristine-tar --git-upstream-tree=debian --git-ignore-new --git-builder="dpkg-buildpackage -rfakeroot -us -uc"

### Script to automatical produce origin tarball, based on debuild

    /home/varac/bin/build-debian-package-from-git.sh

## Packaging with existing debian/ dir

    debuild -uc -us

### dpkg buildpackage config options

Use `DEBUILD_DPKG_BUILDPACKAGE_OPTS` in `~/.devscripts` so you can call debuild without additional cli paramters:

    debuild

### Disable tests while building

    export DEB_BUILD_OPTIONS=nocheck

## Edit changelog file

    dch
    dch -v 1.0beta2 'Tag 1.0beta2'

## Build empty fake package

    sudo apt-get install equivs

    export NAME='pnp4nagios-web-config-nagios3'
    echo "
    Package: $NAME
    Priority: optional
    Section: admin
    Installed-Size: 728
    Maintainer: varac <varac@leap.se>
    Architecture: all
    Source: $NAME
    Version: 0.1-fake
    Depends: bash,
    Description: fake package until jessie-backports is available
    " >> /tmp/equivs

    # builds fake package
    equivs-build /tmp/equis
    # installs fake package
    sudo dpkg -i .deb

    #rm tmp jockey-common_0.9.7-0ubuntu7_all.deb

## Request for Package (RFP)

https://wiki.debian.org/RFP
