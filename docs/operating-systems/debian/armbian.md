# Armbian

* [Website](https://www.armbian.com)
* [Docs](https://docs.armbian.com/)

## SSH

> On a normal armbian-image SSH and DHCP is enabled.
> User: root
> Initial password: 1234

## How to toggle verbose boot

https://docs.armbian.com/User-Guide_Fine-Tuning/#how-to-toggle-verbose-boot

> alter the verbosity= line in /boot/armbianEnv.txt (defaults to 1 which means less verbose, maximum value is 7).

## Dist upgrades

<https://docs.armbian.com/User-Guide_FAQ/#how-do-i-upgrade-from-armbian-buster-to-bullseye>

> Armbian does not offer a standardized way nor do you encourage users to
> upgrade their userspace like Bionic to Focal, Stretch to Buster or mentioned
> above Buster to Bullseye. We would love to do that but the reason why we
> cannot is simply the lack of ressources in time and devices to test such
> upgrades in random scenarios.
> You can try to upgrade your userspace by following official ways from
> Debian/Ubuntu but make sure to freeze your firmware packages via
> armbian-config beforehand. Also you will not receive any help from Armbian
> if something goes wrong or other issues with an upgraded system.

* However, [it might just work so it's worth trying at least](https://forum.armbian.com/topic/19237-armbian-upgrade-from-buster-to-bullseye/#comment-131217)
