# Bug Report

- <http://www.debian.org/Bugs/Reporting>
- <https://help.ubuntu.com/community/reportbug>

reportbug reads `~/.reportbugrc`

## Options

```sh
reportbug PACKAGE
reportbug --no-query-bts PACKAGE
```

## WNPP

`Work-Needing and Prospective Packages`, including `RFP` (`request for package`)

- [Debian Wiki: WNPP](https://wiki.debian.org/WNPP)
- [Work-Needing and Prospective Packages](https://www.debian.org/devel/wnpp/)
- [Debian Wiki: RFP](https://wiki.debian.org/RFP)
- [Debian bugtracker: All WNPP packages](https://bugs.debian.org/cgi-bin/pkgreport.cgi?package=wnpp)

For a RFP, simply report a bug against the pseudo-package `wnpp`:

```sh
reportbug wnpp
```

If you already checked that there are no already reported requests for this package:

```sh
reportbug wnpp --no-query-bts
```

## Query BTS

```sh
querybts
querybts wnpp --latest-first
```
