# Debian alternatives

https://wiki.debian.org/DebianAlternatives

Install custom alternative

    sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/bin/alacritty 50

Choose alternative:

    sudo update-alternatives --config x-terminal-emulator
