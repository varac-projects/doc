# Arch linux

* [Bugs](https://bugs.archlinux.org/)

## Issues

* Kernel 6.4: NetworkManager can't claim wifi interface
* [Why does the installer add a paswordless key to LUKS-encrypted partition?](https://forum.manjaro.org/t/why-does-the-installer-add-a-paswordless-key-to-luks-encrypted-partition/76551)

## Package issues

* `element-desktop-1.11.26-1` from Arch repo: `segmentation fault`
  * `element-desktop-stable-bin-deb-package` AUR package works

## Install

* [Installation guide](https://wiki.archlinux.org/title/installation_guide)
* [Download](https://archlinux.org/download/) iso, dd on usb stick and boot
* for network setup see `../network/iwctl.md`
* Run [archinstall](https://wiki.archlinux.org/title/Archinstall)
  and follow [these steps from the nwg-shell instalation instructions](https://github.com/nwg-piotr/nwg-shell/wiki)

## Post-install setup

[install etckeeper](https://wiki.archlinux.org/title/Etckeeper)

    pacman -Sy
    pacman -Qqen > /etc/pkglist.txt
    pacman -Qqem > /etc/pkglist-foreign.txt
    pacman -S etckeeper
    etckeeper init
    git config --global user.email varac@varac.net
    git config --global user.name varac

    etckeeper commit "first commit"

Tmux:

[Enable UTF-8 locale for tmux](https://unix.stackexchange.com/a/278401):

    echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
    locale-gen
    pacman -S tmux

### Add custom ca certificate

[Arch wiki: TLS trust management](https://wiki.archlinux.org/title/Transport_Layer_Security#Trust_management)

    trust anchor /tmp/custom_certs/*

## Package management

see `../package-managers/pacman.md`

## Sway

[sway](https://wiki.archlinux.org/title/sway)

    sudo pacman -S sway

## Arch distributions

### Manjaro

* [Using Manjaro for Beginners](https://wiki.manjaro.org/index.php/Using_Manjaro_for_Beginners)
* [Reporting Bugs](https://docs.manjaro.org/reporting-bugs/)
* [Manjaro Gilab groups](https://gitlab.manjaro.org/explore/groups)

### Manjaro sway

see `../wayland/sway/manjaro-sway.md`
