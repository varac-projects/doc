# Android ad blocking

- [AdAway](https://adaway.org/)
  - [Kuketz-blog:
    AdAway](https://www.kuketz-blog.de/adaway-werbe-und-trackingfrei-im-android-universum/)
    (german article)
- [Blokada](https://blokada.org/)

  - [GitHub](https://github.com/blokadaorg/blokada)
  - [Kuketz-blog: Blokada](https://www.kuketz-blog.de/blokada-tracking-und-werbung-unter-android-unterbinden/) (german article)
  - [Blokada anti-features: Paid features and trackers](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/8536)
