# Find my device

- [r/CalyxOS: Android trackers support](https://www.reddit.com/r/CalyxOS/comments/1e7hw81/android_trackers_support/)
- [r/CalyxOS: Pixel 8 find my device offline](https://www.reddit.com/r/CalyxOS/comments/1cslq9y/pixel_8_find_my_device_offline/)
- [GrapheneOS: Find My Device Network Support](https://github.com/GrapheneOS/os-issue-tracker/issues/4079)
- [Google Find My Device tag recommendation?](https://discuss.grapheneos.org/d/13333-google-find-my-device-tag-recommendation)

## How it works

- [r/Chipolo: Good summary how FMD network works](https://www.reddit.com/r/Chipolo/comments/1d8umi0/comment/l8ssxul/)
- Unlike apple, where only one phone will publish the tag location,
  Google FMD service needs multiple phones to publish the tag location

## Tags

- Pebblebbe: Not available in Europe anymore

### Chipolo One Point

- [Product page](https://chipolo.net/de/products/chipolo-one-4-pack)
- ø 37.9 mm, 6.4 mm thick (Airtag: ø 31.9 mm, 8mm thick)
- [r/Chipolo: Can you see on Find My Device App + Web, or just Find My Device App (and not Web)?](https://www.reddit.com/r/Chipolo/comments/1d8umi0/chipolo_google_find_my_device_trackers_can_you/)
- [Chipolo one vs one point location accuracy](https://www.reddit.com/r/Chipolo/comments/13vjj54/comment/jmb3j3z/)
  - tldr; `Chipolo One` only works with the Chipolo app (and only phones that have it installed
    will update the location), `Chipolo One Point` only works with the Google FMD app/network,
    and therefore has much better location accuracy

Setup/pairing:

> i have goten myself LG headphones and paired them first on a
> goggled android Phone and then wiped that phone to put a
> custom rom onto it and now does it still show up
> in the app with that account

## Smartphone

### Alternatives to Google find-my-device

- [FindMyDevice](https://gitlab.com/Nulide/findmydevice) apps
  - Locate and control your device remotely
  - [Verlorenes Smartphone orten mit Find My Device ohne Google](https://yourdevice.ch/verlorenes-smartphone-orten-mit-find-my-device-ohne-google/)
  - [F-Droid app](https://f-droid.org/packages/de.nulide.findmydevice/)
- [Nextcloud phonetrack app](https://apps.nextcloud.com/apps/phonetrack)

## Software

### Google find my device

- Registration needs to happen on a device with the Find-my-device app
  installed, and a connected google account
- Device location can be shared with another google account,
  but the sharing link needs to get openend on a device with also both
  connected google account and Find-my-device app installed.
- [Google: Find my device web service](https://www.google.com/android/find/?login)
