# CalyxOS

- [Website](https://calyxos.org/)
  - Big [Team](https://calyxos.org/docs/about/team/)
- [Community resources](https://calyxos.org/community/)
- [Device-support](https://calyxos.org/docs/guide/device-support/#support-length)
  - Low-tech/appordable models available
- Activism/journalist community oriented
- Middleground between security and usability
- MicroG pre-installed
  - Games, Google play credit for commercial apps working
- [CalyxOS: De-Googled geht anders – Custom-ROMs Teil2 🇩🇪](https://www.kuketz-blog.de/calyxos-de-googled-geht-anders-custom-roms-teil2/)

> CalyxOS utilizes Verified Boot (including bootloader re-locking)
> to keep the Android security model intact.

## Camera

- [Is the camera as good with CalyxOS ?](https://www.reddit.com/r/CalyxOS/comments/j30rtt/is_the_camera_as_good_with_calyxos/)
- [Graphene docs: Camera](https://grapheneos.org/usage#camera)
- [Pixel camera](https://play.google.com/store/apps/details/Google_Camera?id=com.google.android.GoogleCamera&hl=gsw)
  can be used to take advantage of the Pixel hardware
- Otherwise, [Open Camera](https://opencamera.org.uk/) can be used
  - [Play store](https://play.google.com/store/apps/details?id=net.sourceforge.opencamera)

## Install host prerequisites

```sh
sudo pacman -S android-udev
```

- Download and verify the CalyxOS device flasher
  - The [calyxos-flasher-git AUR package](https://aur.archlinux.org/packages/calyxos-flasher-git)
    is too outdated

## Flash device

**Note**: Follow [device specifc installation docs](https://calyxos.org/install/) !

But in general:

- Remove SIM card
- Enable Developer Options
  - Settings → About Phone → tap Build number 7 times
- Enable USB debugging
  - Settings → System → Advanced → Developer Options → USB Debugging
- Enable OEM Unlocking
  - Settings → System → Advanced → Developer Options → OEM unlocking
- Download and verify the device factory image

Ensure `device-flasher.linux` and the device factory image are located in the
same directory, then:

```sh
./device-flasher.linux
```

- Unlock the bootloader at the beginning of the installation
- Lock the bootloader again at the end of the installation

## Back to stock Android

- [Back to stock Android](https://calyxos.org/install/stock/)
