# Android apps

## Kids

- [Tommi kindersoftwarepreis](https://tommi.kids/kindersoftwarepreis/)

### Apps

- [Wo ist Goldi ?](https://www.stmd.bayern.de/themen/wo-ist-goldi/)

## Digital detox

- [Digital Detox apps](https://stressbehandlung.info/digital-detox-apps/)
