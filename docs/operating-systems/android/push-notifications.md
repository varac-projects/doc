# Android push notifications

- `ntfy`, `gotify`: see `./notifications/notifications.md`

## LineageOS for microG

- [migrog](https://microg.org/)
- [LineageOS for microG](https://lineage.microg.org/)

### Migrate from LOS to LOS for migroG

See the `Migration from LineageOS` section in the
[installation instructions](https://lineage.microg.org/#instructions).

- Install twrp recovery (refer to device install docs)
  OTA updates fail when using LOS recovery (`Signature verification failed`)
- Install [migration keys ZIP](https://download.lineage.microg.org/extra)

### Install LOS for microG rom

- Folllow instructions from the official [lineageos wiki page for the S5](https://wiki.lineageos.org/devices/klte)
- [Download LineageOS with microg](https://download.lineage.microg.org/).

Verify signature:

```sh
cd ~/projects/android/lineage/microg/update_verifier/
python3 update_verifier.py lineageos4microg_pubkey ~/projects/android/devices/galaxy-s5-fatiya/lineage-16.0-20200928-microG-klte.zip
```
