# F-Droid

- [Website](https://f-droid.org/en/)

## Builds

- Package metadata repo: [fdroiddata](https://gitlab.com/fdroid/fdroiddata)
- [Docs: Can I see the current build status?](https://gitlab.com/fdroid/wiki/-/wikis/FAQ#can-i-see-the-current-build-status)
- [Docs: Build cycle](https://gitlab.com/fdroid/wiki/-/wikis/FAQ#build-cycle)
- [Docs: How long does it take for my app to show up on website and client?](https://gitlab.com/fdroid/wiki/-/wikis/FAQ#how-long-does-it-take-for-my-app-to-show-up-on-website-and-client)
- [Cycle explanation](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/16080#note_2172901080)
- [F-Droid Monitor](https://monitor.f-droid.org)
  - [Current build cycle (running)](https://monitor.f-droid.org/builds/running)
    - See also the `fdroiddata version` for the commit which is currently
      processed

## F-Droid repository management tool

- [fdroid/fdroidserver](https://gitlab.com/fdroid/fdroidserver)

Install:

```sh
pamac install fdroidserver
```
