# LinageOS

## Parse wiki

```sh
cd ~/projects/android/lineage/lineage_wiki/_data/devices
gup
./tablets.py
```

or

```sh
grep 'type:.*tablet' \
  ~/projects/android/lineage/lineage_wiki/_data/devices/* \
  | cut -d':' -f1 | xargs grep -l '19\.1'
```

## adb

Enable usb debugging on device

```sh
adb devices
```

If you can't connect, change the USB mode from MTD to MIDI and try again:

```sh
adb kill-server
adb devices
```

Accept connection on mobile

reboot into recovery (flash a new rom)

```sh
 adb reboot recovery
```

reboot into fastboot (flash new recovery)

```sh
adb reboot fastboot
```

## Fastboot mode / Flash Recovery image

```sh
adb reboot fastboot

sudo fastboot devices
sudo fastboot flash recovery cm-12.1-20160606-NIGHTLY-z3c-recovery.img
sudo fastboot reboot
```

## Upgrade to a new major version

- Refer to your device upgrade instructions, i.e.
  [wiki: pioneer upgrade](https://wiki.lineageos.org/devices/pioneer/upgrade/)

```sh
adb -d reboot sideload
adb -d sideload img.zip
```

## Recovery Mode

> Once the phone is off hold volume down button and press the power button.
> Continue to hold it untill you see a white screen with HBOOT menu. Once in the
> HBOOT menu select recovery. Use the volume up or down to scroll through menu
> selections and power button to select enter.

## Factory reset

With TWRP:

> Just go Wipe and swipe the Factory Reset bar. That will wipe the data, cache
> and Dalvik cache.

## Upgrade recovery

Done automatically if LOS recovery is used !

Old:

- `adb reboot bootloader`
- `fastboot flash boot lineage-17.1-20201029-recovery-pioneer.img`
