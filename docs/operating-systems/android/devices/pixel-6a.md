# Pixel 6a

- [LineageOS device page](https://wiki.lineageos.org/devices/bluejay/)
  - Released July 2022
  - SoC: Google Tensor GS101
  - RAM: 6 GB
  - Storage: 128 GB
  - Network: 5G
  - Wi-Fi: `802.11 a/b/g/n/ac/ax`

## CalyxOS

### Install

[Install CalyxOS on Pixel 6a](https://calyxos.org/install/devices/bluejay/linux/)
