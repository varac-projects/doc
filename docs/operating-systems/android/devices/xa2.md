# Sony Xperia XA2

- [LineageOS device page](https://wiki.lineageos.org/devices/pioneer/)
- Released: February 2018
- RAM: 3 GB
- Storage: 32 GB (+ up to 256 GB SDcard)
- Network: up to 4G LTE
- Wifi: `802.11 a/b/g/n`
