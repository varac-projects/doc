# OpenAndroidInstaller

- [Website](https://openandroidinstaller.org/)

## Install

### AUR package

[openandroidinstaller-git](https://aur.archlinux.org/packages/openandroidinstaller-git)

```sh
pamac install openandroidinstaller-git
```

Issues:

- [openandroidinstaller-git will remove go-yq](https://aur.archlinux.org/packages/openandroidinstaller-git#comment-968985)
