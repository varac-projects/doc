# Verify apk cert

[How to verify SHA256 fingerprint of APK](https://security.stackexchange.com/a/220562)

i.e. the [downloaded signal apk](https://signal.org/android/apk/)

```sh
apksigner verify --verbose --print-certs \
  Signal-website-universal-release-4.58.5.apk  | grep SHA-256
```
