# Custom android ROMs

- [Kuketz blog: Android: GrapheneOS, CalyxOS und Co. unter der Lupe](https://www.kuketz-blog.de/android-grapheneos-calyxos-und-co-unter-der-lupe-custom-roms-teil1/)
- [Comparison of Android ROMs](https://eylenburg.github.io/android_comparison.htm)

## CalyxOS

see [./calyxos.md](./calyxos.md)

## GrapheneOS

- [Device-support](https://grapheneos.org/faq#device-support)
  - Older models not supported as much as with CalyxOS
- Also uses Verified Boot (including bootloader re-locking)
- Probably the best security hardening option
- Very fast security updates (~1-2 days after release by Google)
- Lots of drama about lead maintainer, who stepped down as tech lead 2024-04
- Google services sandbox not always reliable

## LineageOS

See [lineage.md](./lineage.md)
