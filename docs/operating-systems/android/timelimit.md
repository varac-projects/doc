# TimeLimit

- [Website](https://timelimit.io/en/)
- [Test blog post (german)](https://mobilsicher.de/ratgeber/jolo-kindersicherung-empfehlenswert)
  [Docs](https://timelimit.io/de/dokumentation/)
- [TimeLimit Configuration Modes](https://timelimit.io/en/configsuggestions/)
- [Repos on Codeberg](https://codeberg.org/timelimit)
  - [Feature requests](https://codeberg.org/timelimit/timelimit-feature-requests)

## App usage metrics

- Timelimit doesn't track app usage, it only limits it.
- [usageDirect](https://f-droid.org/en/packages/godau.fynn.usagedirect/) can do
  that instead

## Self-hosting

- [timelimit-server](https://codeberg.org/timelimit/timelimit-server)
- [timelimit-server-ui](https://codeberg.org/timelimit/timelimit-server-ui)
- [Docs](https://codeberg.org/timelimit/timelimit-server/src/branch/master/docs/usage)

  - [Docker](https://codeberg.org/timelimit/timelimit-server/src/branch/master/docs/usage/docker.md)

- [No support for server-side components](https://codeberg.org/timelimit/timelimit-server-ui/issues/2#issuecomment-2369246)
  and not the developer is not welcoming contributors / users

## Network mode

- [Not possible with Open TimeLimit (F-Droid)](https://timelimit.io/de/dokumentation/#download)
  but it is possible with [TimeLimit (F-Droid)](https://f-droid.org/en/packages/io.timelimit.android.aosp.direct/)
