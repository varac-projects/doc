# Android Backup

- Old, doesn't include Seedvault: [What options do I have to backup my Android device?](https://android.izzysoft.de/articles/named/android-backup-options)

## Seedvault

- [GitHub](https://github.com/seedvault-app/seedvault)
- [Docs: App Backup Repository Documentation](https://github.com/seedvault-app/seedvault/blob/android15/doc/README.md)
- [FAQ](https://github.com/seedvault-app/seedvault/wiki/FAQ)
  - [Why do some apps not allow to get backed up?](https://github.com/seedvault-app/seedvault/wiki/FAQ#why-do-some-apps-not-allow-to-get-backed-up)
- [CalyxOS SeedVault Guide](https://calyxos.org/docs/guide/getting-started/#seedvault-guide)
- Backup is saved in the `.SeedVaultAndroidBackup` directory in your chosen backup location

> This application is compiled with the operating system and
> does not require a rooted device for use. It uses the same
> internal APIs as adb backup which is deprecated and thus needs a replacement.

### Seedvault backup backends

- WebDAV (via DAVx5)
- Nextcloud ([not recommended](https://calyxos.org/docs/guide/getting-started/#seedvault-guide))
  - Nextclkoud target error-prone (Quota, uploads fail randomly etc.)
    Solution: Backup local and sync the backup (rclone, syncthing ?)
- USB drive ([recommended](https://calyxos.org/docs/guide/getting-started/#seedvault-guide))

Not yet implemented:

- [FR: S3 backup target](https://github.com/seedvault-app/seedvault/issues/524)
- [FR: rclone backup support](https://github.com/seedvault-app/seedvault/issues/196)

### 3rd-party tools

- [Seedvault backup extractor](https://github.com/jackwilsdon/seedvault-extractor)

## Rclone

- [F-Droid: rcx](https://f-droid.org/de/packages/io.github.x0b.rcx/)
  - Last update 2021

## Backup sd card

Insert sdcard in laptop, mount it, then:

```sh
rsync -av --delete --exclude-from=…/backup_sdcard.exclude /media/varac/sdhc_32gb/ /var/backup/manuell/desire_z/sdcard
```
