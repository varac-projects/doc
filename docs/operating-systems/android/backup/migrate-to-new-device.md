# Android migrate to new device

- [Backup phone apps and data with Seedvault](https://kb.above.im/backup-seedvault/)

## Backup on old device

### Seedvault

- Remove unwanted apps
- Configure Seedvault and backup
- Verify backup status and unselect app that don't need a backup
- Backup again
- Browse last backup status list and note down apps that
  - failed during backup
  - couldn't get backed up by Seedvault

### Apps that don't allow Seedvault backup

- Signal: [Backup and Restore Messages](https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages#android_transfer)
  (plaintext file written to root dir of internal storage)

### Backup files

- Browse internal (and external) sdcards and
  - delete unneeded files
  - backup important files to usbstick
- Internal sdcard
- Pictures / Gallery (DCIM)
- Audio recordings
- `Android/Data` is not accessible via `mtp:/`

## Restore on new device

- Install `fdroid basic` and `aurora store` if not installed already

### Apps with block Seedvault backup

- Install Signal and restore from backup

### Apps which sync data from cloud

Manually install and configure:

- Davx5
  - [FR: Export settings](https://github.com/bitfireAT/davx5-ose/discussions/53)
  - see [nextcloud/clients](../../../software/nextcloud/clients.md) how
    to start accunt config from cli
- Nextcloud app
  - Configure `nextcloud` app before `davdroid`, since [davx5 is integrated in
    the nextcloud app](https://www.davx5.com/tested-with/nextcloud).
- Nextcloud News
- [Telegram](https://blog.invitemember.com/transferring-telegram-to-a-new-device/)

## Apps with dedicated backup & restore mechanism

First, make a backup from inside each app:

- Signal (Token needed for restore: `pass show signal`)
- OsmAnd
  - Sync directory to new device (`/storage/.../Android/data/net.osmand.plus/files`)

## Verify restore of most important app

- Go through all all migrated to new device and check
  if restore was successful

## Additional setting

- sip account
  siehe `~/Howtos/sip.md`

## Final migration

For those apps which don't allow mutliple instances across devices
migrate them right before
the factory reset of the old device.
These are i.e. :

- Signal
- Telegram (?)
- …
