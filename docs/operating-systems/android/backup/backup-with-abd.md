# Android backup with adb

- [izzysoft wiki: ADB for end-users / Backup & Restore](https://android.izzysoft.de/articles/named/adb-for-end-users#backup)

## Prepare

Export installed (3rd-party) packages:

```sh
adb shell 'pm list packages -3' | sed 's/.*://g' | sort > ~/tmp/packages.md
```

Sort packages into these categories:

1. Seedvault: Include in Seedvault backup
2. Native: Packages that block being backed up (i.e. Signal ?)
3. Cloud: Packages that sync data to the cloud (i.e. Nextcloud / DavX)
4. No-Backup: don't persist any important data
5. Uninstall: Don't imstall app on new device

Get a list of packages you want to restore, excluding those in `exclude.list`:

```sh
sort packages.list | sed 's/.*=//' | grep -Fvxf exclude.list | tr '\n' ' '
```

Warning: Restore process stalls at some apps, i.e. Bitmask client, so remove it before backup up!

Dump all apps into a single archive:

```sh
adb backup -apk -nosystem -noshared -all -f all-userapps.ab
```

Unpack/decrypt archive with android-backup-extractor:

```sh
abe unpack all-userapps.ab all-userapps.tar
tar -xf all-userapps.tar
```

Install all apks:

```sh
find . -name \*.apk -exec adb install {} \;
```

Then restore the backup:

```sh
adb restore all-apps.ab
```

Tested on migration from `S5-G900T` to `S5-G900F`.

## Issues

These apps couldn't successfully restore their settings:

- davdroid doesn't have an export fucntion
  see `~/.vdirsyncer/config` for URLs and config
  options
- nextcloud (check automatic upload of pictures)

These apps couldn't get backed up:

- signal

  - Use export/import function: [Backup and Restore Messages](https://support.signal.org/hc/en-us/articles/360007059752-Backup-and-Restore-Messages)

  ```sh
  adb pull /mnt/sdcard/Signal/Backups/signal-2018-09-14-18-46-08.backup
  adb push signal-2018-09-14-18-46-08.backup /mnt/sdcard/Signal/Backups
  wipe -q signal-2018-09-14-18-46-08.backup
  ```

- antennapod: `gopass www/gpodder.net`, downloads are at `/sdcard/Android/data/de.danoeh.antennapod/files/media/`
  (simply coping over podcasts from other device doesn't work)

- audinaut

- ultrasonic

- cythara

- k9 import/export settings

  ```sh
  adb pull /sdcard/k9_settings_export_2018-09-14.k9s
  adb push k9_settings_export_2018-09-14.k9s /sdcard/
  rm k9_settings_export_2018-09-14.k9s
  ```

  pw: `gopass server/bitrigger.de/shelluser/varac`, `gopass www/webmail.immerda.ch/varac@varac.net`

- keepass

  ```sh
  adb pull /storage/emulated/0/keepass/mobile.kdbxi
  adb push keepass/mobile.kdbxi /sdcard/

  ```

- pixart messenger `gopass im/xmpp/varac@jabber.systemli.org`

- osmand + contour
