# Home-assistant blueprints

- [Awesome HA Blueprints](https://epmatt.github.io/awesome-ha-blueprints/)
  "A curated collection of blueprints for Home Assistant.
  Reliable, customizable, fully tested by the community."

## Blueprint import

- Settings -> Automations and Scenes
- Blueprints ("Vorlagen")
- `Get more blueprints`: Brings you to [Blueprints Exchange](https://community.home-assistant.io/c/blueprints-exchange/53)
- Search for device, and then click on `My | Import Blueprint`
- `Open link`
- Check branch URL (maybe the main branch switched from `master` to `main` ?)

## Create groups

- `Devices + Services` -> `Helper`
- `Add helper` -> `Groups`
- `Light group`

## Create automation

- Select 3 dot menue of blueprint -> `Create automation`

## Blueprints Exchange

- [Blueprints Exchange](https://community.home-assistant.io/c/blueprints-exchange/53)
- [Awesome HA Blueprints](https://epmatt.github.io/awesome-ha-blueprints/)
