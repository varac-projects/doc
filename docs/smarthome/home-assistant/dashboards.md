# Dashboards

- [HA Dashoboards](https://www.home-assistant.io/dashboards/)
- [HA Frontend repo](https://github.com/home-assistant/frontend/)

## Cards

- [Swiss Army Knife Card for Home Assistant](https://swiss-army-knife-card-manual.amoebelabs.com/)

### Sensor

- Only supports one entity
  - [Adding multiple sensors values into one!](https://community.home-assistant.io/t/adding-multiple-sensors-values-into-one/329677)
  - [Multiple temperature sensors in one](https://community.home-assistant.io/t/multiple-temperature-sensors-in-one/59300)
