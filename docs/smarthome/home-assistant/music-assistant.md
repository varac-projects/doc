# Music assistant

- [Website / Docs](https://music-assistant.io/)
- [GitHub](https://github.com/music-assistant/hass-music-assistant)
  - [FAQ](https://music-assistant.io/faq/how-to/)
  - [Player Provider Poll #1](https://github.com/orgs/music-assistant/discussions/1200)
  - [Companion app as media player](https://github.com/orgs/music-assistant/discussions/396)
- [Player providers](https://music-assistant.io/player-support/)
  - [Slimproto (a.k.a Squeezebox)](https://music-assistant.io/player-support/slimproto/)
    Music Assistant (partly) emulates a Logitech Media Server and
    has a full implementation of the Slim protocol (aka slimproto).
- [Music providers](https://music-assistant.io/music-providers/)

## Local playback

The music-assistant web interface doesn't allow local playback.
There are open feature requests / discussion:

- [Add Web Browser as a Player entity](https://github.com/orgs/music-assistant/discussions/436)
- [Play Music on "current" Device](https://github.com/orgs/music-assistant/discussions/2347)

Until this feature is implemented, the recommended way for
local browser playback is using [snapcast-web](https://github.com/badaix/snapweb)
and the MA [snapcast player provider](https://music-assistant.io/player-support/snapcast/)
The MA desktop client supports local playback though, see below.

## Home-assistant integration

### MA icon in sidebar

From [FAQ](https://music-assistant.io/faq/how-to/):

> If you are running the addon within the HA host go to SETTINGS>>ADDONS>>MUSIC ASSISTANT
> and select "Show in sidebar".
> If you are using docker then you can use an iframe panel

## Desktop client

- [Music Assistant Companion App](https://music-assistant.io/companion-app/)
- [GitHub](https://github.com/music-assistant/companion/)
- [AUR packages](https://aur.archlinux.org/packages?K=music-assistant)
- "Squeezelite comes embedded in the application. This allows playback of music to your computer"

## Issues

### Device screen display

- [Squeezelite Devices (Muse Luxe) Display](https://github.com/orgs/music-assistant/discussions/1305)
