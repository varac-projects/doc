# Dehumidifier

## Comfee

- Modell `Comfee MDDF-16DEN7-WF`
- Supported as shown in [this blog post](https://www.techlover.de/2020/11/01/lufentfeuchter-comfee-in-home-assistant-und-homekit/)
- [Custom image with pip module needed](https://0xacab.org/varac/varac-home-assistant)
- id: `midea_dehumidifier_35184372111610`
- Custom config changes in configuration.yaml

### Issues

- [Device offline should not be an error](https://github.com/barban-dev/midea_inventor_dehumidifier/issues/10)
- [Needed to change any occurrence of 0xA1 to 0xAC](https://github.com/barban-dev/homeassistant-midea-dehumidifier/issues/46#issuecomment-1021671718)
  Done in branch `comfee-mddf-16den7-wf` of `git@github.com:varac/homeassistant-midea-dehumidifier.git`
- [Device state is never "unavailable"](https://github.com/barban-dev/homeassistant-midea-dehumidifier/issues/49)

### Logs

Offline:

```bash
 DEBUG (MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'mode': None, 'tank_show': False, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
 DEBUG (MainThread) [custom_components.midea_dehumidifier.sensor] sensor.midea_dehumidifier: cannot retrieve the state of midea_dehumidifier device
 DEBUG (MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'mode': None, 'tank_show': False, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
 DEBUG (MainThread) [custom_components.midea_dehumidifier.sensor] sensor.midea_dehumidifier: cannot retrieve the state of midea_dehumidifier device
 INFO (MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: async_update called.
 DEBUG (MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: sending get_device_status via Web API...
 INFO (SyncWorker_1) [root] MideaClient::send_api_request: response_status=200, response_reason=OK
 ERROR (SyncWorker_1) [root] MideaClient::send_api_request: errorCode=3123, errorMessage="The appliance is offline"
 ERROR (MainThread) [homeassistant.helpers.entity] Update for humidifier.midea_dehumidifier_35184372111610 fails
Traceback (most recent call last):
  File "/usr/src/homeassistant/homeassistant/helpers/entity.py", line 487, in async_update_ha_state
    await self.async_device_update()
  File "/usr/src/homeassistant/homeassistant/helpers/entity.py", line 691, in async_device_update
    raise exc
  File "/config/custom_components/midea_dehumidifier/humidifier.py", line 401, in async_update
    res = await self.hass.async_add_executor_job(self._client.get_device_status, self._device['id'])
  File "/usr/local/lib/python3.9/concurrent/futures/thread.py", line 52, in run
    result = self.fn(*self.args, **self.kwargs)
  File "/config/deps/lib/python3.9/site-packages/midea_inventor_lib/midea_client.py", line 203, in get_device_status
    decodedReplyStr = self.appliance_transparent_send(deviceId, dataStr)
  File "/config/deps/lib/python3.9/site-packages/midea_inventor_lib/midea_client.py", line 553, in appliance_transparent_send
    if not "reply" in result:
TypeError: argument of type 'NoneType' is not iterable
```

On:

```bash
 (MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'humidity': 55, 'mode': 'Continuos', 'ion': 0, 'fan_speed_mode': 'Silent', 'fan_speed': 40, 'current_humidity': 64, 'tank_show': False, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
 (MainThread) [custom_components.midea_dehumidifier.sensor] sensor.midea_dehumidifier: current humidity = 64
(MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: async_update called.
 (MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: sending get_device_status via Web API...
(SyncWorker_8) [root] MideaClient::get_device_status (cached): DeHumidification [powerMode=1, mode=2, Filter=False, Water tank=False, Current humidity=64, Current humidity (decimal)=0, Wind speed=40, Set humidity=55, Set humidity (decimal)=0, ionSetSwitch=0, isDisplay=True, dryClothesSetSwitch=0, Up&Down Swing=0]
(MainThread) [custom_components.midea_dehumidifier.humidifier] DeHumidification [powerMode=1, mode=2, Filter=False, Water tank=False, Current humidity=64, Current humidity (decimal)=0, Wind speed=40, Set humidity=55, Set humidity (decimal)=0, ionSetSwitch=0, isDisplay=True, dryClothesSetSwitch=0, Up&Down Swing=0]
(MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'humidity': 55, 'mode': 'Continuos', 'ion': 0, 'fan_speed_mode': 'Silent', 'fan_speed': 40, 'current_humidity': 64, 'tank_show': False, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
```

On and tank out / full (`P2` in display):

```bash
(MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'humidity': 55, 'mode': 'Continuos', 'ion': 0, 'fan_speed_mode': 'Silent', 'fan_speed': 40, 'current_humidity': 56, 'tank_show': True, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
(MainThread) [custom_components.midea_dehumidifier.sensor] sensor.midea_dehumidifier: current humidity = 56
(MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: async_update called.
 (MainThread) [custom_components.midea_dehumidifier.humidifier] midea-dehumidifier: sending get_device_status via Web API...
(SyncWorker_1) [root] MideaClient::get_device_status (cached): DeHumidification [powerMode=1, mode=2, Filter=False, Water tank=True, Current humidity=56, Current humidity (decimal)=0, Wind speed=40, Set humidity=55, Set humidity (decimal)=0, ionSetSwitch=0, isDisplay=True, dryClothesSetSwitch=0, Up&Down Swing=0]
(MainThread) [custom_components.midea_dehumidifier.humidifier] DeHumidification [powerMode=1, mode=2, Filter=False, Water tank=True, Current humidity=56, Current humidity (decimal)=0, Wind speed=40, Set humidity=55, Set humidity (decimal)=0, ionSetSwitch=0, isDisplay=True, dryClothesSetSwitch=0, Up&Down Swing=0]
(MainThread) [custom_components.midea_dehumidifier.sensor] state.attributes = {'min_humidity': 40, 'max_humidity': 85, 'available_modes': ['Target_humidity', 'Continuos', 'Smart', 'Dryer'], 'humidity': 55, 'mode': 'Continuos', 'ion': 0, 'fan_speed_mode': 'Silent', 'fan_speed': 40, 'current_humidity': 56, 'tank_show': True, 'device_class': 'dehumidifier', 'friendly_name': 'midea_dehumidifier_35184372111610', 'supported_features': 1}
(MainThread) [custom_components.midea_dehumidifier.sensor] sensor.midea_dehumidifier: current humidity = 56
```
