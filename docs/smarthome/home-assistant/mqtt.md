# MQTT

## Message broker

### Moquitto

- [Website](https://mosquitto.org/)
- [Mosquitto docs](https://mosquitto.org/documentation/)
- Recommende by zigbee2mqtt
- Server address: `mqtt://casita.varac.ts:1883`

## Bridges

### Zigbee2MQTT

- [Website](https://www.zigbee2mqtt.io/)
