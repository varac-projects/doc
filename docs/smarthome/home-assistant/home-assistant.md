# Home Assistant

- [Docker installation](https://www.home-assistant.io/docs/installation/docker)
- [Awesome HA](https://www.awesome-ha.com/)
- [Developer Docs](https://developers.home-assistant.io/)
  - [Sensors](https://developers.home-assistant.io/docs/core/entity/sensor/)
  - Available [Units of measurements](https://github.com/home-assistant/core/blob/dev/homeassistant/const.py#L633)
  - Available [Device classes](https://www.home-assistant.io/integrations/sensor#device-class)
  - Available [State classes](https://developers.home-assistant.io/docs/core/entity/sensor/#available-state-classes)

## Configuration

- [Docs: Configuration.yaml](https://www.home-assistant.io/docs/configuration/)
  - [YAML syntax](https://www.home-assistant.io/docs/configuration/yaml/)
  - [Splitting up the configuration](https://www.home-assistant.io/docs/configuration/splitting_configuration/)

## API

- [Rest API docs](https://developers.home-assistant.io/docs/api/rest)

Query prometheus integration:

```shell
BEARER=$(gopass show --password token/home-assistant.oas2.casita.local.moewe-altonah.de/prometheus)
alias HA_CURL="curl -H \"Authorization: Bearer $BEARER\""
HA_CURL http://home-assistant.varac.net/api/prometheus
```

## Integrations

- [DIY integrations](https://www.home-assistant.io/integrations/#search/diy)
- [Remote RPi integration](https://www.home-assistant.io/integrations/remote_rpi_gpio)

### Prometheus

#### Query prometheus metrics

- [mweinelt/ha-prometheus-sensor](https://github.com/mweinelt/ha-prometheus-sensor)
  - Recent activity
  - Failed [Upstream integration](https://github.com/mweinelt/ha-prometheus-sensor#upstream-integration)
  - Issue: Can't find it in [HACS](https://github.com/mweinelt/ha-prometheus-sensor/issues/2)
- [lfasci/homeassistant-prometheus-query](https://github.com/lfasci/homeassistant-prometheus-query)
  - Last commit 2022-10

#### Expose prometheus metrics

> The prometheus integration exposes metrics in a format which Prometheus can read

- [Prometheus integration](https://www.home-assistant.io/integrations/prometheus/)

Issues:

- [Add support to set custom labels for Prometheus](https://community.home-assistant.io/t/add-support-to-set-custom-labels-for-prometheus/163966)

## Addons

- Manual installation in `~/.config/custom_components`
