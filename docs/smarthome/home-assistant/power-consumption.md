# Power consumption in Home-assistant

See also [../smart-meter.md](../smart-meter.md) for reading smart meters.

## Appliance detection

- [Detect and monitor the state of an appliance based on its power consumption](https://community.home-assistant.io/t/detect-and-monitor-the-state-of-an-appliance-based-on-its-power-consumption-v2-1-1-updated/421670)
- [leofabri/hassio_appliance-status-monitor](https://github.com/leofabri/hassio_appliance-status-monitor)
