# HACS

- [Website](https://hacs.xyz/)

## Install

[In container](https://hacs.xyz/docs/setup/download):

```sh
wget -O - https://get.hacs.xyz | bash -
```

[Initial Configuration](https://hacs.xyz/docs/configuration/basic)
