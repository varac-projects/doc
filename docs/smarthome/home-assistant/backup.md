# Home Assistant backup

- [Backup](https://www.home-assistant.io/integrations/backup/)
- [3…2…1… Backup](https://www.home-assistant.io/blog/2025/01/03/3-2-1-backup/)
- [2025.1: Backing Up into 2025!](https://www.home-assistant.io/blog/2025/01/03/release-20251/)
- [2025.2 Blog post: Iterating on backups](https://www.home-assistant.io/blog/2025/02/05/release-20252/#iterating-on-backups)

Obsolete:

- [Home Assistant Backups automatisch erstellen und löschen](https://smarterkram.de/911/)

- [hass-auto-backup](https://github.com/jcwillox/hass-auto-backup)

  > Improved Backup Service for Home Assistant that can Automatically Remove
  > Backups and Supports Generational Backup Schemes

- [Restore](https://www.home-assistant.io/common-tasks/os#restoring-a-backup-on-a-new-install)

  > You can make use of backup which you have copied off of a previous install to
  > restore to a new installation during the onboarding process.
  > Follow the link at the bottom of the account creation page to
  > upload your backup from the previous installation.
