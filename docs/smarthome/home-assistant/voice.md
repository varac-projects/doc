# Home assistant voice

- [Voice preview edition product page](https://www.home-assistant.io/voice-pe/)
- [Community blog: The era of open voice assistants has arrived](https://community.home-assistant.io/t/the-era-of-open-voice-assistants-has-arrived/813601)
- [Voice Chapter 8 - Assist in the home today](https://www.home-assistant.io/blog/2024/12/19/voice-chapter-8-assist-in-the-home/)
