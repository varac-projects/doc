# Alarm clock

- [hassalarm](https://github.com/Johboh/hassalarm)

## Hardware

- [Alarm clock for bedroom?](https://community.home-assistant.io/t/alarm-clock-for-bedroom/585529)

### Sandman doppler

- [Website](https://www.sandmanclocks.com/products/doppler)
- [ha-doppler](https://github.com/pa-innovation/ha-doppler)
- [Sandman Doppler Alarm Clock Blueprint ](https://community.home-assistant.io/t/sandman-doppler-alarm-clock-actions-active-snoozed-turned-off-pre-alarm/523473)
