# Home-/Music-assistant RFID Jukebox

- Blog post: [Building an RFID jukebox](https://www.home-assistant.io/integrations/tag/#building-an-rfid-jukebox)
- [home-assistant tags blogpost](https://www.home-assistant.io/blog/2020/09/15/home-assistant-tags/)
- [Docs: Tags](https://www.home-assistant.io/integrations/tag/)

## Setup

- Setup [music-assistant](./music-assistant.md)
- Setup [tag reader](../esp/esphome/tagreader.md)
- Find tag scanner id:

> To find your scanner’s device ID, open
> `Developer tools -> Events -> Listen` to events and subscribe to `tag_scanned`.
> Then scan a tag on the reader and note down the device_id from the data section.

i.e. `223e7b4b2a009f595d52107c3455b45f` (Adonno tag reader)

- Find player id:

i.e. `098759dc44d75b6c8ca866a40fed2ee0` (ESP Muse)

- Configure automation as shown in [Building an RFID jukebox](https://www.home-assistant.io/integrations/tag/#building-an-rfid-jukebox)
  and name it `RFID Jukebox: Play Music`

### Add tags

- Goto `Settings/Tags`
  - Scan tag and rename it
  - Copy the tag ID
- Goto `Settings/Automations & Scenes`
  - Click on `RFID Jukebox: Play Music`
  - Select `Edit in YAML` from `󰇙` menu
  - Copy & paste existing entry, replace ID and album URI
