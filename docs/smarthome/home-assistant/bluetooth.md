# Home assistant bluetooth

- [Home Assistant docs: Bluetooth](https://www.home-assistant.io/integrations/bluetooth)
- [Home Assistant Container Part 11: Bluetooth support](https://sequr.be/blog/2022/11/home-assistant-container-part-11-bluetooth-support/)

## Devices

- [Xiaomi Mijia BLE Sensors](https://esphome.io/components/sensor/xiaomi_ble.html)
