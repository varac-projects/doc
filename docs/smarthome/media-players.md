# Home assistant media player integration

## Esphome

- [Esphome 2022.6.0 changelog, featuring Media player](https://esphome.io/changelog/2022.6.0.html#media-players)
- [Esphome media players](https://esphome.github.io/media-players)

## Hardware

- [raspiaudio](../hardware/raspiaudio.md)

## HA Music assistant

[./home-assistant/music-assistant.md](./home-assistant/music-assistant.md)

## Other integrations

- [snapcast](https://github.com/badaix/snapcast)
