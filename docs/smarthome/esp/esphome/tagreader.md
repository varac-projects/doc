# Home-assistant RFID / NFC tags

## Adonno tag reader

- [GitHub](https://github.com/adonno/tagreader)
- [Shop](https://adonno.com/tagreader/)

- [Setup](https://adonno.com/tagreader/setup/)

- Simple to build/use NFC tag reader, specially created for Home Assistant
- Based on ESP8266 D1 mini
- Integrates well with the [ESPHome ha addon](https://esphome.io/guides/getting_started_hassio.html)

### RFID jukebox

See [home-assistant/jukebox.md](../../home-assistant/jukebox.md)
