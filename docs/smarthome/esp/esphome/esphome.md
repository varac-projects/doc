# EspHome

- [Power Sensor-Kalibrierung](https://techbotch.org/blog/esphome-sensor-calibration/index.html)

## Hardware

### POE

- [ESP32 with wired LAN and PoE](https://github.com/arendst/Tasmota/issues/8503)
- [Wesp32: esp32 with POE](https://wesp32.com/)
- [olimex ESP32-POE-ISO](https://www.olimex.com/Products/IoT/ESP32/ESP32-POE-ISO/open-source-hardware)

## Issues

- Find good way to merge device specific with common sensors
  - [No way to merge values to key](https://github.com/esphome/feature-requests/issues/123)
- [Regular device reboots](https://github.com/esphome/issues/issues/2896)
- [Don't use dashes in device names (and underscores are not allowed)](https://github.com/esphome/issues/issues/2168)

## Configs

- [esphome substitutions](https://esphome.io/guides/configuration-types.html#config-substitutions)
- [Splitting up the configuration](https://www.home-assistant.io/docs/configuration/splitting_configuration/)

## Usage

```sh
esphome compile temphum-laube.yaml
```

Compiled firmware is at `.esphome/build/temphum-laube/.pioenvs/temphum-laube/firmware.bin`

### Migrate from tasmota

- Flash tasmota-minimal
- Flash compiled esphome firmware

## Prometheus support

- [Add Prometheus /metrics-Endpoint PR](https://github.com/esphome/esphome/pull/1032)

## Device support

### Gosund P1

- 3-plug switch
- [esphome-devices gosund-p1](https://www.esphome-devices.com/devices/Gosund-P1)
- [gosund-p1 forum thread](https://community.home-assistant.io/t/gosund-p1-power-strip-with-power-monitoring/184820/28)

### Gosund SP1

- Single plug switch

Original (Tasmota) metrics:

```txt
Spannung        0 V
Strom   0.000 A
Leistung        0 W
Scheinleistung  0 VA
Blindleistung   0 VAr
Leistungsfaktor 0.00
Energie heute   0.000 kWh
Energie gestern 0.000 kWh
Energie insgesamt       0.000 kWh
```

### Calibration

- [Calibrate Sensors in ESPHome](https://www.danielmartingonzalez.com/en/calibrate-sensors-esphome)
- [Esphome: Sensor filters](https://esphome.io/components/sensor/index.html#sensor-filters)
