# DF-Player mini

* [Esphome](https://esphome.io/components/dfplayer.html)
* [Datasheet](https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299)
* [DIY Video Doorbell with Voice Response | ESPHome / ESP32 Camera / DFPlayer Mini](https://community.home-assistant.io/t/diy-video-doorbell-with-voice-response-esphome-esp32-camera-dfplayer-mini/208254)
