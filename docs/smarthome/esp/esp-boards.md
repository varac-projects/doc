# ESP8266 boards

- [List of all officially released ESP32 development boards](https://www.espboards.dev/esp32/)
  - Incl. pictures

## AI Thinker ESP-12F

- [Datasheet](https://docs.ai-thinker.com/_media/esp8266/docs/esp-12f_product_specification_en.pdf)
- Single blue LED is connected to D4 and turns on when
  closed.

## Esp32

- [ESP32 Platform](https://esphome.io/components/esp32.html)
  - List of boards: [platformio/espressif32](https://registry.platformio.org/platforms/platformio/espressif32/boards?version=5.3.0&p=1)

### ESP32 NodeMCU dev board

- [Berrybase: ESP32 NodeMCU Development Board](https://www.berrybase.de/esp32-nodemcu-development-board)
- [Berrybase Blog: ESP32 NodeMCU Module Guide](https://blog.berrybase.de/esp32-node-mcu-module-anfaenger-guide/)
- EAN: `4251266700609`
- 240 MHz, 512 Kilobyte SRAM, 4MB flash
- Esphome Board id: `esp32dev`
- WLAN and Bluetooth (incl. LE)
- 38 Pins incl. GPIO, ADC, PWM, SPI, I2C, UART
- Chipsatz: `ESP-WROOM-32`

Issues:

[Ch340 keeps disconnecting](https://askubuntu.com/a/1528114)
Solution: Add the usb id to `USB_DENYLIST` in i.e. `/etc/tlp.d/10-esp32.conf` to prevent it from autosuspending

### with POE

- [ESP32-POE](https://www.olimex.com/Products/IoT/ESP32/ESP32-POE/open-source-hardware)
  - [Esphome Ethernet component](https://esphome.io/components/ethernet.html)
