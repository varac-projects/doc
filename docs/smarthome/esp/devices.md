# ESP devices

- [ESPHome Remote](https://github.com/landonr/esphome-remote)
  iPod style wifi smart home remote. Uses ESPHome and Home Assistant to
  integrate with Sonos, Roku and run custom scenes and scripts.

## Water meter sensor

[Auslesen des Hauswasserzählers mit ESPHome und Home Assistant](https://www.bujarra.com/leyendo-el-contador-de-agua-de-casa-con-esphome-y-home-assistant/?lang=de)
