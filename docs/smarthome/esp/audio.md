# ESP audio

* [ESPHome & Playing WAV files with a speaker](https://community.home-assistant.io/t/example-esphome-playing-wav-files-with-a-speaker/150302/14)
* [esphome i2s audio](https://esphome.io/components/media_player/i2s_audio.html)
  [Media Player Components](https://esphome.io/components/media_player/index.html)

## Hardware

* [Adafruit I2S 3W Class D Amplifier Breakout](https://www.adafruit.com/product/3006?gclid=EAIaIQobChMI6qbJ8vvK5wIVxZyzCh0NwwRIEAQYASABEgJjWvD_BwE)

### DF-Player mini

* [DF-Player mini](https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299)
  * [esphome DF mini support](https://esphome.io/components/dfplayer.html)
* [ESP8266 MP3 Sound Machine v2](https://selfhostedhome.com/esp8266-mp3-sound-machine-v2/)
* [Doorbell with DF-Player mini](https://www.reddit.com/r/homeassistant/comments/wuyia7/diy_esphome_doorbell_chime/)

### Esphome media players

* [ESPHome Media Players](https://esphome.github.io/media-players/)
  * [ATOM Echo Smart Speaker Development Kit](https://shop.m5stack.com/collections/m5-controllers/products/atom-echo-smart-speaker-dev-kit?ref=NabuCasa)
    seems to be very quite!
