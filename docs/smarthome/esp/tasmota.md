# Tasmota devices

<https://www.tasmota.info/>

## Prometheus

<https://www.markhansen.co.nz/monitoring-electricity-with-tasmota/>

By default, Tasmota is build without prometheus integration.
Since [there is no official prometheus build](https://github.com/arendst/Tasmota/issues/9206),
you need to re-build it:

### Building

<https://github.com/tasmota/docker-tasmota>

```sh
cd ~/projects/smart-home/tasmota/Tasmota.git
git fetch
```

Checkout latest tag:

```sh
git tag -l | tail
git co -b 9.5.0 v9.5.0
```

Add `#define USE_PROMETHEUS` to `tasmota/user_config_override.h`, then:

```sh
docker pull blakadder/docker-tasmota
docker run -ti --rm -v $(pwd):/tasmota -u $UID:$GID blakadder/docker-tasmota -e tasmota
```

Build binary is at `build_output/firmware/`

### Usage

Get prometheus metrics:

```sh
curl --netrc -Li tasmota_222d0d-3341.lan/metrics
```

## MQTT

- [Docs: MQTT](https://tasmota.github.io/docs/MQTT/)

[Docs: Configure MQTT using Backlog](https://tasmota.github.io/docs/MQTT/#configure-mqtt-using-backlog):

```sh
Backlog mqtthost <mqtt_broker_address>; mqttport <mqtt_broker_port>; mqttuser <username>;
  mqttpassword <password>; topic <device_topic>
```

## Upgrading tasmota to newer version or add features

<https://tasmota.github.io/docs/Upgrading>

Example hardware: `Gosund P1 3-fach AC+USB WiFi Smart Steckdosenleiste`,
bought with tasmota 9.5 (2021-07)

First usage:

- Connect to Wi-fi network `tasmota_XXX`
- Browse <http://192.168.4.1/>
- First, strip down installed firmware by installing `tasmota-minimal.bin`,
  using OTA update with following URL: <https://ota.tasmota.com/tasmota/tasmota-minimal.bin.gz>
  See <https://tasmota.github.io/docs/Upgrading/#upload-buffer-miscompare-error>
  for the rationale
- Then, install the custom firmware build using the `Upload file` option.
- After this, configuring Wi-fi again is neccessary (<http://192.168.4.1>)

## Recovery

<https://tasmota.github.io/docs/Device-Recovery/#fast-power-cycle-device-recovery>

When using tasmota: Press down power button for 40s does a factory reset and
restart.

You can [recover with](https://groups.google.com/g/sonoffusers/c/I5w60R-oNJU/m/NXAz_b7KAgAJ):

```sh
curl -F "u2=@/tmp/tasmota.bin" http://192.168.4.1/u2
```

## Devices

### Gosund P1

- <https://www.loxwiki.eu/display/LOX/Steckdosenleiste+Gosund+P1+mit+Tasmota>
- [Gosund P1 template](https://templates.blakadder.com/gosund_P1.html)

### Temperature/humidity sensor DHT22

- [Ebay-Kauf](https://www.ebay.de/itm/284103582664?var=585595341204)
  <https://tasmota.github.io/docs/AM2301/>
- see `~/projects/smart-home/tasmota/temperature_humidity_sensor_dht22`
- Firmware: `~/projects/smart-home/tasmota/Tasmota.git/build_output/firmware/tasmota-10.0.0-generic.bin.gz`
- WLAN: ESP-01S
- Sensor: DHT22

Before:

Module: `Sonoff Basic (1)`
GPIO: All `none`

After configuration:

Module: `Generic`
GPIO2: `AM2301` für das DHT22 Modul

- Avahi hostname: `http://tasmota-2379c9-6601.local/`

#### Calibration

- [No hardware calibration possible](https://github.com/arendst/Tasmota/issues/2872)
  In this issue there's also sensor recommendations for accuracy (BMP280 or BME280 (Bosch), see <https://tasmota.github.io/docs/BME280>)

Use Tasmota Console:

i.e. Sensor reports 24.5⁰, external kalibrated thermometer reports 22.7⁰, offset
is -1.8⁰:

`TempOffset -1.8..24.5`, press Enter.

### ESP8266 based Wi-fi boards

<https://en.wikipedia.org/wiki/ESP8266>

#### Wemos D1 mini Pro

- <https://tasmota.github.io/docs/devices/Wemos-D1-Mini/>

- ESP Chip ID: 2889749 (ESP8266EX)

- Flash Size 16mb

- Program Flash Size 1mb

#### ESP8266MOD (by AI-Thinker)

- <https://www.tinyosshop.com/index.php?route=product/product&product_id=908>
- ESP Chip ID: 13840473 (ESP8266EX)
- Flash Size 4mb
- Program Flash Size 1mb

Erasing flash and backing up firmware is not possible:

```sh
A fatal error occurred: ESP8266 ROM does not support function erase_flash.
A fatal error occurred: ESP8266 ROM does not support function erase_flash.
```

Flashing:

<https://tasmota.github.io/docs/Getting-Started/#flashing>

```sh
esptool --port /dev/ttyUSB0 write_flash -fs 1MB -fm dout 0x0 ~/projects/smart-home/tasmota/Tasmota.git/build_output/firmware/tasmota-10.0.0-generic.bin
```

- <http://tasmota-d33059-4185.local/>

### Mini D1 mini Pro sensors

[DHT22](https://wiki.politick.ca/pages/viewpage.action?pageId=8454628),
including useful tips about Mini D1 Pro GPIO usage

#### HTU21D

Enable build flag:

```sh
#USE_HTU
```

Config:

- Generic (18)
- D1 GPIO5: I2C SDA
- D2 GPIO4: I2C SCL

Wiring:

## Console

```sh
i2cscan
```
