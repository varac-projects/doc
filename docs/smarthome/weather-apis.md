# Weather APIs

see private wiki `~/smarthome/weather.md)` for locations

## Open-Meteo

> Open-Meteo is an open-source weather API and offers free access for
> non-commercial use. No API key is required. You can use it immediately!

- [Website](https://open-meteo.com)
- [Docs and API builder](https://open-meteo.com/en/docs)
- [Opensprinklershop open-meteo.com provider](https://opensprinklershop.de/2022/05/18/noch-mehr-wetterdienste/?v=3a52f3c22ed6):
  `http://opensprinklershop.de:3336`
- [home-assistant open-meteo integration](https://www.home-assistant.io/integrations/open_meteo/)
- Website: historical data until 3 month ago
- Soil moisture, evapotranspiration, etc.

## Weather underground

- No official home-assistant integration, but a [3rd party one](https://github.com/cytech/home-assistant-wundergroundpws)

### API

- [How to use API](https://knx-user-forum.de/forum/supportforen/smarthome-py/1519025-wunderground-hat-wieder-gratis-api-key)

## OpenWeatherMap API

### One-call API

- [One-call API](https://openweathermap.org/api/one-call-3)
- Only for paid subscriptions
- [Home-assistant OpenWeatherMap integration](https://www.home-assistant.io/integrations/openweathermap/)

## agromonitoring.com

### Agro API

- [How to start](https://agromonitoring.com/api/get)

## DWD

- No official integration yet

Discussions:

- [DWD HTTP API (rain radar for Germany](https://community.home-assistant.io/t/dwd-http-api-rain-radar-for-germany/334713/5)
  Hacky workaround
- [Current Homeassistant Weather forecast integrations not accurate enough for Germany. Can you integrate wetter.com?](https://community.home-assistant.io/t/current-homeassistant-weather-forecast-integrations-not-accurate-enough-for-germany-can-you-integrate-wetter-com/213829)

### 3rd party integrations

- [Deutscher Wetterdienst integration for Home-Assistant](https://github.com/FL550/dwd_weather)
- [Custom component for Home Assistant](https://github.com/hg1337/homeassistant-dwd)
- [DWD Warnungen für HomeAssistant ](https://github.com/Dielee/DWDAlerts)

## Home-assistant weather integration

- [Weather Entity](https://developers.home-assistant.io/docs/core/entity/weather/)

## sensor.community

- [Website](https://sensor.community/en/)
- [Map](https://maps.sensor.community)
- [Unofficial sensor.community API documentation](https://api-sensor-community.bessarabov.com/#/data/get_airrohr_v1_sensor__SensorID__)

## Noise map

- [Website](https://noise-map.com)

From [Noise Map](https://noise-map.com/home/):

> Noise Map uses modern big-data tools to anaylse and simulate
> noise around the world. We looked at noise propagation
> literature and understood how sounds travels.
> Using the available data (mainly ADS-B data) about plane
> locations around the world, we model and visualise the
> expected noise levels on the ground near airports around
> the world. We also visualise other types of noise on our
> dashboard (road, rail, …) based on the work of
> https://github.com/lukasmartinelli/osm-noise-pollution
