# Wifi displays

## Lenovo ThinkSmart View Display

- [Website](https://www.lenovo.com/de/de/p/smart-devices/smart-office/thinksmart/thinksmart-view/len109e0005)
- Teams or Zoom firware
- Android 10, Qualcomm QCS8250, 8GB RAM, 128 GB Flash
- 1,25 cm x 26,3 cm x 14,2 cm
- [The Lenovo ThinkSmart View, Rebooted](https://taoofmac.com/space/blog/2023/04/22/1330)
- [XDA Forum thread](https://xdaforums.com/t/cd-18781y-lenovo-thinksmart-view-bootloader-firmware-zoom-teams-conversion-normal-android.4426029/)
- [Is this the perfect standalone tablet for HA?](https://community.home-assistant.io/t/is-this-the-perfect-standalone-tablet-for-ha/658422?page=10)

## Lenovo Smart Clock 2

- [Specs](https://www.lenovo.com/de/de/departsales/c/Smart-Clock-Gen-2/p/WMD00000485)
  - 4" LCD-IPS-Touchscreen
  - MediaTek MT8167S, 1 GB RAM + 8 GB Flash
- Rooted devices sold on Ebay

## E-Ink

Way to go:

- [Use ESPHome with e-ink Displays to blend in with your home decor](https://community.home-assistant.io/t/use-esphome-with-e-ink-displays-to-blend-in-with-your-home-decor/435428)
- [Github](https://github.com/Madelena/esphome-weatherman-dashboard)

Others:

- [E-Ink Display mit ESPHome und Home Assistant steuern (ESP32)](https://smarterkram.de/1407/)
- [Is there are any ready solutions for simple battery powered display, for simple information](https://community.home-assistant.io/t/is-there-are-any-ready-solutions-for-simple-battery-powered-display-for-simple-information/261043/8)
E-Ink Display:

- [7.5" 800×480 ePaper Display HAT für Raspberry Pi v2](https://www.berrybase.de/7.5-800-480-epaper-display-hat-fuer-raspberry-pi-v2)
- [7,5 Zoll E-Paper Display Modul für Raspberry Pi Pico, 800×480, schwarz/weiß](https://www.berrybase.de/7-5-zoll-e-paper-display-modul-fuer-raspberry-pi-pico-800-480-schwarz/weiss)
