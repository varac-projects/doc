# Privacy preserving voice assistants

- [Wikipedia: List of speech recognition software](https://en.wikipedia.org/wiki/List_of_speech_recognition_software)
- [Wikipedia: Speech recognition software for Linux](https://en.wikipedia.org/wiki/Speech_recognition_software_for_Linux)

## Hardware

- [Google AIY VoiceKit 2 with Raspberry Pi 4 B](https://community.mycroft.ai/t/google-aiy-voicekit-2-with-raspberry-pi-4-b/9920)
- [Chatterbox](https://hellochatterbox.com/)
  - "Chatterbox is a build-it-yourself and code-it-yourself
    smart speaker designed to give children a fun and safe
    playground to learn about technology."

### Mycroft Mark II

- Discontinued
- [Wikipedia: Mycroft Hardware](<https://en.wikipedia.org/wiki/Mycroft_(software)#Hardware>)
- [MycroftAI/hardware-mycroft-mark-II](https://github.com/MycroftAI/hardware-mycroft-mark-II)
- [Docs](https://mycroft-ai.gitbook.io/docs)
  - [German voices](https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/customizations/languages/german)
- [Raspberry Pi upgrade](https://github.com/MycroftAI/hardware-mycroft-mark-II/blob/master/mark-II-production/BOM/SJ-201-R10-2022-07-29-08-44-49.csv)
- [Neon AI OS for the Mark II](https://neon.ai/neonaiformycroftmarkii/)
- [Reddit: r/Mycroftai](https://www.reddit.com/r/Mycroftai/)

## openconversational.ai

Forum for the Neon AI platform and applications.
Supporting the Mycroft AI legacy, Mark II users, the OpenVoice Operating System, and more.

- [Open Source Conversational AI Community](https://community.openconversational.ai/)

## OpenVoiceOS (OVOS)

- [Website](https://openvoiceos.github.io/ovos-landing-page/)
- [Status page](https://openvoiceos.github.io/status/)
- [Docs: Technical Manual](https://openvoiceos.github.io/ovos-technical-manual/)
- [Community docs](https://openvoiceos.github.io/community-docs/)
- [GitHub](https://github.com/openVoiceOS/)
- [FAQ: OVOS, Neon, and the Future of the Mycroft Voice Assistant](https://community.openconversational.ai/t/faq-ovos-neon-and-the-future-of-the-mycroft-voice-assistant/13496/7)
- Matrix room: `#openvoiceos:matrix.org`
- [OpenVoiceOS/raspOVOS](https://github.com/OpenVoiceOS/raspOVOS)

## Neon AI OS

- [Website](https://neon.ai)
  - [Neon AI OS for the Mark II](https://neon.ai/neonaiformycroftmarkii/)
- [GitHub](https://github.com/NeonGeckoCom)
- [Neon.AI Docs](https://neongeckocom.github.io/neon-docs/)
  - [Neon OS on the Mark II](https://neongeckocom.github.io/neon-docs/neon_os/On_the_MarkII/)
- Matrix room: `#NeonMycroft:matrix.org`

## Old, stale

- [Jasper](https://jasperproject.github.io/)
- [MycroftAI/mimic3](https://github.com/MycroftAI/mimic3)
  A fast local neural text to speech engine for Mycroft
- [rhasspy](https://github.com/rhasspy/rhasspy)
  Offline private voice assistant for many human languages
