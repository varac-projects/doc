# Conbee2 / DeConz

* [deconz-community/deconz-docker Container image](https://github.com/deconz-community/deconz-docker)
  * [Docker hub](https://hub.docker.com/r/deconzcommunity/deconz)
* [Compatible devices](https://phoscon.de/en/compatible)

## Conbee2 firmware update

* [Docker: Updating Conbee/RaspBee Firmware](https://github.com/deconz-community/deconz-docker#updating-conbeeraspbee-firmware)

```bash
ssh mediaplayer.local
sudo -i
cd /etc/docker/compose/deconz
docker-compose logs | grep GCF

  deconz  | 20:55:06:090 GW update firmware found: /usr/share/deCONZ/firmware/deCONZ_ConBeeII_0x26660700.bin.GCF

docker-compose down
docker run -it --rm --entrypoint "/firmware-update.sh" --privileged --cap-add=ALL -v /dev:/dev -v /lib/modules:/lib/modules -v /sys:/sys deconzcommunity/deconz
docker-compose up
```
