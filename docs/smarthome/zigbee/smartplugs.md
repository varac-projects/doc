# Zigbee smartplugs

- [ZigBee Steckdosen Test-Check: Das sind die 5 besten Modelle](https://www.homeandsmart.de/zigbee-steckdosen-test)
- [HA forum: Zigbee Smart Plug with Power Monitoring?](https://community.home-assistant.io/t/zigbee-smart-plug-with-power-monitoring/263162)

## Smartplugs with power measurement

### Nous A1Z

- [Product site](https://nous.technology/product/a1z-1/de.html)
- Reset: Press button 5-7 sec

### Other

- Xiaomi Mi Smart Plug XM500008: Single plug ~45€
- [BlitzWolf BW-SHP13 Single Plug](https://www.blitzwolf.at/BlitzWolf-BW-SHP13-ZigBee-WIFI-Smart-Socket)
