# Zigbee remote controls

- See also [home-assistant/blueprints.md](../home-assistant/blueprints.md)

## Ikea Tradfri remote Control E810 (round)

- [Blakadder: Tradfri Remote Control E810](https://zigbee.blakadder.com/Ikea_E1810.html)
  - Reset remote controls: Press button next to battery 4 times

### Ikea Tradfri home-assistant blueprint

- [ZHA - IKEA TRADFRI - 5 Button Remote - Color Lights](https://community.home-assistant.io/t/zha-ikea-tradfri-5-button-remote-color-lights/276816)
  - [Github: niro1987 ZHA Tradfri color lights blueprint](https://github.com/niro1987/homeassistant-config/blob/main/blueprints/automation/niro1987/zha_ikea_tradfri_5button_remote_custom.yaml)

## Ikea Styrbar (old, square)

- [Blakadder: Styrbar Remote Control (Stainless)](https://zigbee.blakadder.com/Ikea_E2001.html)

### Ikea Styrbar home-assistant blueprint

- [Github: niro1987 ZHA Styrbar color lights blueprint](https://github.com/niro1987/homeassistant-config/blob/main/blueprints/automation/niro1987/zha_ikea_tradfri_styrbar_color.yaml)
  - [blueprint docs](https://epmatt.github.io/awesome-ha-blueprints/docs/blueprints/controllers/ikea_e2001_e2002/#additional-notes)
  - Can only modify devices, not entities (i.e. light groups)
  - [Bug - IKEA long press actions not working because of oversized string values](https://github.com/EPMatt/awesome-ha-blueprints/issues/313)
    - [Open PR to fix it](https://github.com/EPMatt/awesome-ha-blueprints/pull/314)
    - [Use ikea_e2001_e2002.yaml from open PR](https://github.com/kanflo/awesome-ha-blueprints/blob/ikea_e1742_dimmer_fix/blueprints/controllers/ikea_e2001_e2002/ikea_e2001_e2002.yaml)
    - Doesn't work, use [LewisSpring/ikea_e2001_e2002_modified.yaml](https://gist.github.com/LewisSpring/b0e625f91971dc3df4aa5c270662715b#file-ikea_e2001_e2002_modified-yaml)
      instead !
