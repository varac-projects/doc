# Zigbee coordinators

- [ZHA known working modules](https://www.home-assistant.io/integrations/zha/#known-working-zigbee-radio-modules)
  - [zigpy compatible zigbee coordinator hardware](https://github.com/zigpy/zigpy#compatible-zigbee-coordinator-hardware)
- [zigbee2mqtt supported adapters](https://www.zigbee2mqtt.io/guide/adapters/#recommended)
- [Best Zigbee Coordinators for Home Assistant 2024](https://smarthomescene.com/blog/best-zigbee-dongles-for-home-assistant-2023/)
- [Conbee II technical details](https://phoscon.de/de/conbee2/techspec)
  - Chipset: Older, deprecated [ATSAMR21B18](https://www.microchip.com/en-us/product/atsamr21b18)
- Old Zigbee Chipsets [Texas instruments CC2531](https://www.ti.com/product/de-de/CC2531)
- Newer Chipsets based on `CC2652`, i.e.
  - [CC2652P](https://www.ti.com/product/CC2652P?keyMatch=CC2652&tisearch=universal_search&usecase=partmatches)
- [CC2652 ZigBee USB-Sticks im Vergleich](https://www.elcombri.de/cc2652-zigbee-usb-sticks-im-vergleich/)

[Zigbee + Matter (Thread)? Welcher Stick kann was?](https://haus-automatisierung.com/hardware/2023/10/09/zigbee-matter-thread-welcher-stick.html)

## SLZB-06

- [Smlight Website](https://smlight.tech/)
- [Docs: SLZB-06 \* Series Manual](https://smlight.tech/manual/slzb-06/)
- Poe enabled Zigbee, Matter-over-thread, Bluetooth coordinator
- EPS32 based
- Zigbee SoC: [CC2652P](https://www.ti.com/product/CC2652P?keyMatch=CC2652&tisearch=universal_search&usecase=partmatches)
  - Bluetooth 5.2 Low Energy, Thread, Zigbee 3.0
- [SMLight SLZB-06 Zigbee POE Coordinator Review](https://smarthomescene.com/reviews/smlight-slzb-06-zigbee-poe-coordinator-review/#web-dashboard-overview)

### Setup

- Plug in USB-C power cable
- Wait until wifi SSID `SLB-06_…` is available, connect to it
- Browse to `http://192.168.1.1/`

## Home-assistant SkyConnect

- [Website](https://www.home-assistant.io/skyconnect/)
- A future firmware update will bring Thread support; allowing SkyConnect to
  power your Matter and Zigbee networks at the same time.
- Shows up as USB device `10c4:ea60 Silicon Labs CP210x UART Bridge`

## Conbee2

- [Conbee2 Setup](https://smart-home-assistant.de/deconz-conbee-ii-einrichtung)
