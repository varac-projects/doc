# Zigbee temperature sensors

- [Xiaomi Aqara Temperatur-, Luftfeuchtigkeits- und Luftdrucksensor T1](https://www.berrybase.de/aqara-temperatur-und-luftfeuchtigkeitssensor-t1-unterstuetzt-matter-zigbee-3.0)
  - Matter, Zigbee, 18€
- [Sonoff SNZB-02](https://sonoff.tech/product/gateway-and-sensors/snzb-02/)
  - 7,50 €

## With display

- [Sonoff SNZB-02D](https://sonoff.tech/product-document/gateway-and-sensors-doc/snzb-02d-doc/)
  - [User manual](https://sonoff.tech/wp-content/uploads/2023/02/%E8%AF%B4%E6%98%8E%E4%B9%A6-SNZB-02D-V1.0.pdf)
  - Reset: Press button 5 secs
- [Heimann HS3HT](https://zigbee.blakadder.com/Heiman_HS3HT.html)
- [Ihorn LH-331ZB](https://zigbee.blakadder.com/iHORN_LH-331ZB.html)
- [MOES Zigbee Smart Brightness Thermometer/Humidity](https://zigbee.blakadder.com/Moes_ZSS-ZK-THL.html)
  - ["piece of junk"](https://community.home-assistant.io/t/moes-zigbee-smart-brightness-thermometer-real-time-light-sensitive-temperature-and-humidity-detector-values-jumping-to-zero/444860)

## With TVOC (Total Volatile Organic Compounds)

- [Comparism of different air quality sensors: The Xiaomi Particle Counter Is So Inaccurate It Should Not Control the Purifier](https://smartairfilters.com/en/blog/xiaomi-particle-counter-inaccurate-not-control-purifier/)

- [Aqara TVOC Air Quality Monitor](https://www.aqara.com/en/product/tvoc-air-quality-monitor)

  - E Ink screen
  - 1y battery life
  - [home-assistant forum thread](https://community.home-assistant.io/t/aqara-tvoc-air-quality-monitor/330014)

- Xiaomi Mi2
  - Inaccurate, see the above article
