# Zigbee devices

See also `../home-assistant/devices/`

## Light bulbs

- Ikea tradfri
  - Reset bulbs: Switch bulb off+on again 6 times

## Remote controls

- i.e. [Blakadder: Tradfri Remote Control E810](https://zigbee.blakadder.com/Ikea_E1810.html)
  - Reset remote controls: Press button next to battery 4 times
- See also [home-assistant/blueprints.md](../home-assistant/blueprints.md)
