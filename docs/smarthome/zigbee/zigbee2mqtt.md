# Zigbee2mqtt

- [Website](https://www.zigbee2mqtt.io/)
- [GitHub main project](https://github.com/Koenkk/zigbee2mqtt)
- [GitHub zigbee2mqtt-frontend](https://github.com/nurikk/zigbee2mqtt-frontend)

## Migrate to other zibgee controler

- [How do I migrate from one adapter to another?](https://www.zigbee2mqtt.io/guide/faq/#how-do-i-migrate-from-one-adapter-to-another)
- [What does and does not require repairing of all devices](https://www.zigbee2mqtt.io/guide/faq/#what-does-and-does-not-require-repairing-of-all-devices)

## Issues

- [OTA check failed](https://github.com/Koenkk/zigbee2mqtt/issues/6729)

### Frontend

- [Too many popups. Must turn them off somehow](https://github.com/nurikk/zigbee2mqtt-frontend/issues/1974)
