# Zigbee

## Device support

- [Blakadder list](https://zigbee.blakadder.com/)
  Recommended by Home Assistant
- [Zigbee2mqtt supported devices](https://www.zigbee2mqtt.io/supported-devices)
- [ZHA device support](https://www.home-assistant.io/integrations/zha/#knowing-which-devices-are-supported)
- [CSA (Connectivity Standards Alliance, formerly the Zigbee Alliance)](https://csa-iot.org/all-solutions/zigbee/)

## Firmware Updates

[Zigbee Home Automation OTA](https://www.home-assistant.io/integrations/zha/#ota-firmware-updates):

> Currently, OTA providers for firmware updates are only available for IKEA and
> LEDVANCE devices

## Libraries

- [home-assistant ZHA (Zigbee Home Automation)](https://www.home-assistant.io/integrations/zha/)
  - Based on [zigpy](https://github.com/zigpy/zigpy)
    - Currently no [Zigbee Green Power specification support](https://github.com/zigpy/zigpy/issues/341)
