# Zigbee smoke detectors

- [Heimann HS1SA](https://www.heimantech.com/product/index.php?type=detail&id=33)
  - [Review](https://smarthomescene.com/reviews/heiman-zigbee-smoke-sensor-and-alarm-review/)
    Beware of the different models ! Only `HS1SA-E` supports Zigbee 3.0
  - [Smoke Sensor Heiman HS1SA Zigbee Control/Configuration settings](https://community.home-assistant.io/t/smoke-sensor-heiman-hs1sa-zigbee-control-configuration-settings/414822/13)
  - [zigpy issue](https://github.com/zigpy/zha-device-handlers/issues/518)
  - [Smoke Sensor Heiman HS1SA Zigbee Control/Configuration settings](https://community.home-assistant.io/t/smoke-sensor-heiman-hs1sa-zigbee-control-configuration-settings/414822/13)
  - [Review](https://smarthomescene.com/reviews/heiman-zigbee-smoke-sensor-and-alarm-review/)
