# Zigbee door sensors

## Sonoff SNZB-04

- [zigbee2mqtt device entry](https://www.zigbee2mqtt.io/devices/SNZB-04.html)
- [Blakadder device entry](https://zigbee.blakadder.com/Sonoff_SNZB-04.html)
- Pairing: Long press reset button for 5s until the LED indicator flashes
  three times, which means the device has entered pairing mode
