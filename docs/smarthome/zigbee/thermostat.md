# Zigbee thermostat devices

- TRV = Thermostatic Radiator Valves

## Sonoff TRVZB

- [Product site](https://sonoff.tech/product/home-appliances/trvzb/)
  - [Docs](https://sonoff.tech/wp-content/uploads/2023/09/%E8%AF%B4%E6%98%8E%E4%B9%A6-TRVZB-V1.1.pdf)
- Very quiet!
- [HA forum: Less Z2M support for Sonoff TRVZB](https://community.home-assistant.io/t/less-z2m-support-for-sonoff-trvzb/638787)
- [Zigbee2MQTT: SONOFF TRVZB](https://www.zigbee2mqtt.io/devices/TRVZB.html)
  - Supports `local_temperature_calibration`
  - [Sonoff TRVZB is missing attributes that can be exposed, yet configurable using a SEND, SET command](https://github.com/Koenkk/zigbee2mqtt/issues/19269)

Reset / enter pairing mode:

- In manual mode, turn the knob counterclockwise. When the screen
  displays `OF`, press and hold the middle button for 3 seconds
  and the screen iconi `` flashes. Now the device enters Pairing mode.
- If the device fails to pair with the gateway within 3 minutes
  after it enters Pairing mode, the device will exit the pairing status.
- If you want to pair again, please press and hold the middle button
  for 3 seconds until the icon `OF` flashes when the screen
  shows ``

Factory reset:

- Press “Middle button” for a longer time and
  at the same time put batteries in,
  then the screen displays `FR` and blinks for 3 seconds.

## home-assistant integration

### Better Thermostat for HA

- [Website / Docs](https://better-thermostat.org/)
  - [GitHub](https://github.com/KartoffelToby/better_thermostat)
- [Better thermostat UI card](https://github.com/KartoffelToby/better-thermostat-ui-card)
- [Night time schedule blueprint](https://better-thermostat.org/schedule)
  - Before creating an nighttime automatation with this blueprint,
    manually create a custom nighttime [schedule](https://www.home-assistant.io/integrations/schedule/)
    that can be used when creating the automation.

### Lovelace Thermostat Cards

- [Top 8 Home Assistant Thermostat Cards](https://smarthomescene.com/blog/top-8-home-assistant-thermostat-cards/)
