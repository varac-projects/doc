# Openspinkler Pi

- [Openspinkler Pi product page](https://opensprinkler.com/product/opensprinkler-pi/)
- OSPI v1.52 hardware board
- [OSPi v1.5 User manual](https://openthings.freshdesk.com/support/solutions/articles/5000631606-user-manual-opensprinkler-pi-ospi-v1-4-)
- [Web Interface](http://opensprinkler.lan:8080)

## Firmware

- [GitHub](https://github.com/OpenSprinkler/OpenSprinkler-Firmware)
- [OpenSprinkler-Weather server component](https://github.com/OpenSprinkler/OpenSprinkler-Weather)
- [Installation instructions](https://openthings.freshdesk.com/support/solutions/articles/5000631599)

### Usage

Weather Adjustments

- [Using Weather Adjustments](https://openthings.freshdesk.com/support/solutions/articles/5000823370-using-weather-adjustments)
- Forecast / weather metrics are fetched from DarkSky (`https://darksky.net`
  currently broken)

Weather providers:

- [GitHub OpenSprinkler-Weather weather provider service code](https://github.com/OpenSprinkler/OpenSprinkler-Weather)
- Default provider: `http://weather.opensprinkler.com`
- [Opensprinklershop.de open-meteo.com provider](https://opensprinklershop.de/2022/05/18/noch-mehr-wetterdienste/?v=3a52f3c22ed6):
  `http://opensprinklershop.de:3336`
  Current configured weather provider
- [opensprinklershop.de DWD provider](https://opensprinklershop.de/2022/05/07/wetterdienst-tester-gesucht/?v=3a52f3c22ed6):
  `http://opensprinklershop.de:3333`
  Also: Instructions how to change weather provider for Opensprinkler

Example request:

```console
$ export LOC='12.3456, 98.1234,9.244'
$ curl "http://weather.opensprinkler.com/weather1.py/?loc=$LOC"
&scale=0&tz=56&sunrise=405&sunset=1190&eip=2728279759&rawData={"wp":"DS","h":88.62,"p":0.21,"t":60.5,"raining":1}&errCode=0%

$ curl "http://opensprinklershop.de:3336/weather1.py/?loc=$LOC"
&scale=0&tz=56&sunrise=405&sunset=1190&eip=779266636&rawData={"wp":"OM","h":93.09,"p":0.38,"t":60.6,"raining":1}&errCode=0%

$ curl "http://opensprinklershop.de:3333/weather1.py/?loc=$LOC"
&scale=0&tz=56&sunrise=405&sunset=1190&eip=779266636&rawData={"wp":"DWD","h":85.96,"p":0.27,"t":60.5,"raining":1}&errCode=0%
```

#### Opensprinkler API

- [OS API docs](https://openthings.freshdesk.com/support/solutions/articles/5000716363-os-api-documents)
- API password is the md5-hashed system-pw

i.e.:

```sh
export API_PW=$(gopass show --password token/opensprinkler.lan/api-pw)
```

Get controller values:

```sh
curl "opensprinkler.lan:8080/jc?pw=$API_PW"
```

Get all values:

```sh
curl "opensprinkler.lan:8080/ja?pw=$API_PW"
```

Get logs of last 30 days:

```sh
curl "opensprinkler.lan:8080/jl?pw=$API_PW&hist=30" | jq .
```

### Upgrading

[B. Upgrade a previously installed Unified Firmware](https://openthings.freshdesk.com/support/solutions/articles/5000631599-installing-and-updating-the-unified-firmware)

> Warning: The firmware update process will set OS back
> to factory defaults. This includes controller settings,
> program settings, and device password (which will be
> set back to the factory default of “opendoor”).
> Please ensure you back up your current configurations
> (e.g. Export Configurations) before proceeding with firmware update.

```sh
sudo systemctl stop OpenSprinkler.service
cd ~/projects/smart-home/opensprinkler/OpenSprinkler-Firmware
git fetch
git checkout -b '220(2)' '220(2)'
sudo ./build.sh ospi
sudo systemctl start OpenSprinkler.service
```

- Login with default pw (`opendoor`)
- Re-import backup
- Reset password

#### GPIO issues

- [Recent kernels no longer support sysfs interface for GPIO](https://github.com/OpenSprinkler/OpenSprinkler-Firmware/issues/250)
  - [Workaround](https://github.com/OpenSprinkler/OpenSprinkler-Firmware/issues/250#issuecomment-2063549593)

### Container

- [README_Docker.md](https://github.com/OpenSprinkler/OpenSprinkler-Firmware/blob/master/README_Docker.md)

## Android app

- [GitHub](https://github.com/OpenSprinkler/OpenSprinkler-App)
- [Play store](https://play.google.com/store/apps/details?id=com.albahra.sprinklers)

## Sensors

### Soil moisture sensors

- [Hacking a Capacitive Soil Moisture Sensor (v1.2) for Frequency Output](https://thecavepearlproject.org/2020/10/27/hacking-a-capacitive-soil-moisture-sensor-for-frequency-output/)
