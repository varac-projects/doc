# Nanoleaf

* [Yeelight](https://www.home-assistant.io/integrations/yeelight/):
  Cheap Nanoleaf alternative, scores platinum on our quality scale.

https://github.com/rowak/nanoleaf-desktop
https://github.com/MylesMor/nanoleafapi

## Integrations

https://eva-ics.readthedocs.io/en/3.3.2/integrations/nanoleaf.html
https://www.openhab.org/addons/bindings/nanoleaf/
https://www.home-assistant.io/integrations/nanoleaf
