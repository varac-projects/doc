# Air quality sensors

- https://home-assistant-guide.com/2020/10/05/the-best-temperature-and-humidity-sensor-for-esphome/
- https://home-assistant-guide.com/2021/11/10/the-best-particulate-pm2-5-sensor-for-esphome/
- https://github.com/nkitanov/iaq_board
- https://community.home-assistant.io/t/air-quality-sensors-e-ink-display-using-esphome/201776

* https://www.techstage.de/ratgeber/bessere-luft-smarte-raumluft-sensoren-fuer-co2-radon-ozon-feinstaub-und-co/w1wf5s0
* https://www.jeffgeerling.com/blog/2021/airgradient-diy-air-quality-monitor-co2-pm25

## Zigbee

see `./zigbee.md`

## WLAN

### DIY with Esphome

see `./esphome.md`

### Fertige Geräte

- [AZ-Envy Wlan ESP8266-12F Umwelt Entwicklungsboard mit Feuchtigkeits- und Luftqualitätssensor (MQ-2 und SHT30)](https://www.az-delivery.de/products/az-envy?variant=32759608344672&utm_source=google&utm_medium=cpc&utm_campaign=azd_de_google_performance-max_labelled-products&utm_content=&utm_term=&gclid=CjwKCAiAvK2bBhB8EiwAZUbP1AP6Jol69SmC5P2vsU_PbGxXYx2tmzalYyIu07y1lV5LjdhPDFag8xoC7Y8QAvD_BwE)
  10€
- [Amazon Smart Air Quality Monitor](https://www.amazon.de/dp/B08X2V3T2B?tag=cascblog00-21&th=1): 80€, kein CO2, [keine HA Integration](https://community.home-assistant.io/t/integrate-amazon-smart-air-quality-monitor/435717)
- z.b. https://www.ebay.de/itm/133896498880 inkl. bewegungs-Sensor
- [JQ-300/200/100 Indoor Air Quality Meter](https://community.home-assistant.io/t/jq-300-200-100-indoor-air-quality-meter/189098): Billig , cloud-only

### Temp, Hum + VOC

- [Laser egg](https://www.amazon.de/Kaiterra-Laser-Egg-Luftqualit%C3%A4t-Luftfeuchtigkeit/dp/B07GDR4N6P)
