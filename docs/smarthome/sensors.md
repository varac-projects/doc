# Sensors

## Soil moisture sensors

- [b-parasite](https://github.com/rbaron/b-parasite)
  An open source soil moisture sensor

## Air quality sensor

- [IKEA VINDSTYRKA](https://zigbee.blakadder.com/Ikea_E2112.html)
  - [HA forum thread](https://community.home-assistant.io/t/ikea-vindstyrka-zigbee-air-quality-sensor/549747)
  - Zigbee
  - Display
  - Fan inside (!)
- [HoLu-sensorkit](https://hollandseluchten.waag.org/holu-sensorkit/)
  - esp8266, a BME280 temperature/moisture/atmospheric pressure sensor
    and a PM2.5 + PM10 sensor
  - arduino (but should work with a raspberry pi as well)

## Temperature / humidity

### Zigbee

- [Which Zigbee temperature sensor?](https://community.home-assistant.io/t/which-zigbee-temperature-sensor/346703/3)
- [Sonoff SNZB-02 Temperature & Humidity Sensor](https://sonoff.tech/product/gateway-and-sensors/snzb-02/)
  - Works fine
  - 3V Battery (CR2450)
  - [User manual](https://sonoff.tech/wp-content/uploads/2021/03/%E8%AF%B4%E6%98%8E%E4%B9%A6-SNZB-02-V1.0-20210305.pdf)
- [Aqara Temperatur Sensor Zigbee](https://de.aliexpress.com/item/4000990800194.html?gatewayAdapt=glo2deu)
- [Nous E6](https://meine-digitale-welt.de/nous-e6/)
  - Only with custom code possible

### ESP HTU21d temperature/humidity sensor

- [HTU21d](https://www.home-assistant.io/integrations/htu21d)
- Wiring:
  - HTU21D ESP8266 AI Thinker
  - 3.3V
  - G
  - CL D#
  - DA D4

### 433 Mhz

- [jckuester/weather-station](https://github.com/jckuester/weather-station)
  - Raspberry Pi, Arduino Nano, RXB6 433Mhz receiver,
    and as many GT-WT-01 temperature/humidity sensors as you like.

## Gas sensors

- [Raspberry Pi Gas Sensor (MQ-X) konfigurieren und auslesen](https://tutorials-raspberrypi.de/raspberry-pi-gas-sensor-mq2-konfigurieren-und-auslesen/)

## CO2

- [How does an NDIR CO2 Sensor Work?](https://www.co2meter.com/blogs/news/6010192-how-does-an-ndir-co2-sensor-work)
- [FHem: Luftqualität messen mit dem MH-Z14 / MH-Z19 CO2 sensor und ESPEasy WeMos](https://blog.moneybag.de/fhem-luftqualitaet-messen-mit-dem-mh-z14-mh-z19-co2-sensor-und-espeasy-wemos/)

### Prometheus co2exporter

- [gebi/co2exporter](https://github.com/gebi/co2exporter)

- [TFA-Dostmann AIRCO2NTROL Coach CO2 Monitor](https://www.amazon.de/dp/B07R4XM9Z6)
  - connected via usb
  - no battery
