# Smart meter

## Smart Meter Interface

- [Tasmota: Smart meter interface](https://tasmota.github.io/docs/Smart-Meter-Interface)
  - [Scripts for different smart meters](https://tasmota.github.io/docs/Smart-Meter-Interface/#smart-meter-descriptors)
- [Smart Message Language](https://de.wikipedia.org/wiki/Smart_Message_Language)

### Hichi Lesekopf

ESP01s-based Wi-fi Intfrared Reader

- [Website](https://sites.google.com/view/hichi-lesekopf/startseite)
- [v2, esp32-based](https://sites.google.com/view/hichi-lesekopf/wifi-v2)
- [KNX support](https://sites.google.com/view/hichi-lesekopf/knx)
- [Video: Getting started](https://www.youtube.com/watch?v=VuXpzKetOhc)
  - Upgrade Firmware: Files are linked in the video description
- [Tasmota: MQTT](https://tasmota.github.io/docs/MQTT/)

#### Usage

- [Enable INFO mode, disable PIN](https://www.youtube.com/watch?v=HY6KSwvsZjY)

Enable raw/debug mode:

- Main menu → Consoles → Console
- Enter command: `sensor53 d1`

**Beware**: [Until raw mode is disabled again, metrics won't update !](https://sites.google.com/view/hichi-lesekopf/faq)

Disable raw mode:

- Enter command: `sensor53 d0`

## M-Bus

- [Esphome forum: Hacky integration for M-Bus](https://community.home-assistant.io/t/hacky-integration-for-m-bus/127288/3)
  - [M-Bus Slave Click](https://www.mikroe.com/m-bus-slave-click)
- [esphome-dlms-meter](https://github.com/DomiStyle/esphome-dlms-meter)
- [esphome FR: M-Bus support](https://github.com/esphome/feature-requests/issues/1646)
- [Relay WebLog 250](https://www.relay.de/produkte/m-bus-master/weblog-250): Can
  read up to 250 devices, but no standard protocol to hook into home-assistant
- [M-BUS HAT für Raspberry Pi](https://www.az-delivery.de/products/m-bus-hat-fur-raspberry-pi?variant=43547012563211)
  "maximal 6 Lasteinheiten können versorgt werden"
- [ZTSHBK USB-zu-MBUS-Slave-Modul](https://www.amazon.de/ZTSHBK-USB-zu-MBUS-Slave-Modul-Master-Slave-Kommunikation-Debugging-Bus%C3%BCberwachung/dp/B09F5FGYVS/ref=asc_df_B09F5FGYVS/?tag=googshopde-21&linkCode=df0&hvadid=579542346646&hvpos=&hvnetw=g&hvrand=14377343804261577272&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9061054&hvtargid=pla-1666112830794&psc=1&th=1&psc=1)
