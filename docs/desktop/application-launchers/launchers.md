# Desktop app launchers

- [Arch wiki: Application launchers](https://wiki.archlinux.org/title/List_of_applications/Other#Application_launchers)
- [What are the best app launchers for UNIX-like systems?](https://www.slant.co/topics/3945/~best-linux-app-launchers)
- [Awesome wayland: Launchers](https://github.com/rcalixte/awesome-wayland?tab=readme-ov-file#launchers)

## Walker

- [GitHub](https://github.com/abenz1267/walker)
- Active, >800 commits, 10 contributors
- Go
- AUR package: `walker-bin`
- Issues:
  - Erratic and non-deterministic behaviour: Using the same input yields different matching results with i.e.:
    `/home/varac/bin/custom/recently_used_files.sh | tail -200 | walker -d`

### Walker plugins

- Easy to extend: simple stdin/stdout (external or via configuration, see wiki)
- [Wiki: Plugins](https://github.com/abenz1267/walker/wiki/Plugins)

### Wofi

Rofi-like, text-based alternative for wlroots-based wayland compositors

- [Website](https://cloudninja.pw/docs/wofi.html)
- [Repo](https://hg.sr.ht/~scoopta/wofi)
- C
- Last commit 2024-08

Still [clunky Fuzzy matching](https://todo.sr.ht/~scoopta/wofi/48), i.e.

```sh
gopass ls --flat | wofi -d -i -M fuzzy
```

### bemenu

Dynamic menu library and client program inspired by dmenu

- [GitHub](https://github.com/Cloudef/bemenu)
- C
- 60+ contributors
- Bemenu works good together with [j4-dmenu-desktop](https://github.com/enkore/j4-dmenu-desktop)
- Works with wayland
- Very good fuzzy matching (better than wofi)

## Various other launchers

- [Albert](./albert.md)
- [ulauncher](./ulauncher.md)
- [kickoff](https://github.com/j0ru/kickoff)
  rust, application launcher only ?
- [yofi](https://github.com/l4l/yofi): Rust
- [fuzzel](https://codeberg.org/dnkl/fuzzel): Ugly, f
- [tofi](https://github.com/philj56/tofi)
  - C
  - Last release 2023
  - Stylish overlay
- [dmenu-wayland](https://github.com/nyyManni/dmenu-wayland)

### Outdated

- [Cerebro](https://www.cerebroapp.com/)
  - [GitHub](https://github.com/cerebroapp/cerebro)
  - Last release 2023-02
- [Synapse](https://github.com/MichaelAquilina/synapse-project): Last commit 2018
- [Zazu App](http://zazuapp.org/)
  - Official [GitHub repo archived](https://github.com/tinytacoteam/zazu)
  - [sirula](https://github.com/DorianRudolph/sirula): `.desktop` launcher only
  - [lavalauncher](https://git.sr.ht/~leon_plickat/lavalauncher)
  - [gmenu](https://code.rocketnine.space/tslocum/gmenu)
  - [mauncher](https://github.com/mortie/mauncher)
  - [term-dmenu](https://github.com/Seirdy/term-dmenu)
