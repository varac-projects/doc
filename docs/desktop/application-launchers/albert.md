# Albert

- [Website](https://albertlauncher.github.io/)
- [GitHub](https://github.com/albertlauncher/albert)
  - Active, >2k commits, 29 contributors
  - 2024: only commits from 1 author
- [albert AUR package](https://aur.archlinux.org/packages/albert)

## Plugins

- [Docs: Extensions](https://albertlauncher.github.io/gettingstarted/extension/)
  - C++ or Python
- [Official Albert Python plugins](https://github.com/albertlauncher/python)
  - Active, 440 commit, 46 contributors
  - i.e.
    - bitwarden, aur, arch_wiki, emoji, ...
  - [Bitwarden plugin example](https://github.com/albertlauncher/python/blob/main/bitwarden/__init__.py) (139 loc)
- [Official Albert C++ plugins](https://github.com/albertlauncher/plugins)
  - Active, 893 commits, 16 contributors
- No plugins packaged in Arch/AUR

## Issues

- ~/bin/ not searched
- [Terminal programs launch N windows when launched for the Nth time](https://github.com/albertlauncher/albert/pull/802)

### Terminal

When invoking a command from albert (i.e. `ls -la`), it uses this cmd to start the binary in a terminal:

```sh
sh -ic "/bin/zsh -ic ''/bin/ls' '-al''; exec /bin/zsh"
```

Together with the configurable terminal prefix (i.e. `xterm -e`):

```sh
xterm -e sh -ic "/bin/zsh -ic ''/bin/ls' '-al''; exec /bin/zsh"
```

Terminals to try out:

- <https://github.com/jwilm/alacritty>

Terminals which won't work:

- Termite
  - [Can't start termite](https://github.com/albertlauncher/albert/issues/817)
  - [support termite cmds with spaces](https://github.com/thestinger/termite/issues/679)
- Sakura
- Terminator

Terminal which work:

- xterm
- tilix
- Kitty
