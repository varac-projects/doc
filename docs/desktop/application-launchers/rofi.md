# Rofi

A window switcher, application launcher and dmenu replacement

- [Github](https://github.com/DaveDavenport/rofi)
- C
- [Arch wiki: Rofi](https://wiki.archlinux.org/index.php/Rofi)
- Original rofi [has no Wayland support](https://github.com/davatorium/rofi/issues/446)
  - [Wayland-fork](https://github.com/lbonn/rofi)
  - [Arch package](https://archlinux.org/packages/extra/x86_64/rofi-wayland/)
- Default in manjaro-sway-edition and hyprland mlinuxforwork
- C, recent releases
- Bundled themes: `/usr/share/rofi/themes`
- [rofi-themes-collection](https://github.com/newmanls/rofi-themes-collection)
- [A huge collection of Rofi based custom Applets, Launchers & Powermenus](https://github.com/adi1090x/rofi)

Config: `~/.config/rofi/config.rasi`

## Usage

Select file from recently used files:

```sh
recently_used_files.sh | tail -200 | rofi -dmenu | wl-copy
```

## Plugins / scripts

- [Rofi wiki: User scripts](https://github.com/davatorium/rofi/wiki/User-scripts)
- [miroslavvidovic/rofi-scripts](https://github.com/miroslavvidovic/rofi-scripts)
- [davatorium/rofi-scripts](https://github.com/davatorium/rofi-scripts)
  - Outdated

### (Web) search

- [pdonadeo/rofi-web-search](https://github.com/pdonadeo/rofi-web-search)
  - Last commit 2019

### Passwords

- [rofi-pass](https://github.com/carnager/rofi-pass)
- Keepass
  - [keepmenu](https://github.com/firecat53/keepmenu)
  - [Passhole](https://github.com/Evidlo/passhole/)
    - [Support for multiple stores](https://github.com/Evidlo/passhole/issues/15#issuecomment-458833776)

### rofimoji

- [GitHub](https://github.com/fdw/rofimoji)
  - "Emoji, unicode and general character picker for rofi and rofi-likes"
  - `pamac install rofimoji`

### Pass/Gopass

- [tessen](https://github.com/ayushnix/tessen): Works fine
  - Supports tofi, rofi, yofi, wofi, fuzzel, bemenu
  - [AUR package](https://aur.archlinux.org/packages/tessen)
- [rofi-pass](https://github.com/carnager/rofi-pass)
  - [doesn't support gopass yet](https://github.com/carnager/rofi-pass/issues/175)
  - [Requires xdotool](https://github.com/carnager/rofi-pass/issues/165) which
    doesn't work on Wayland
