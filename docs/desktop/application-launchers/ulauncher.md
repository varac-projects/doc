# ULauncher

- [Website](https://ulauncher.io/)
- [Docs: extensions and color themes](http://docs.ulauncher.io/en/stable/)
- [GitHub](https://github.com/albertlauncher/albert)
  - > 2k commits, 54 contributors
  - Slow development
  - But in last 6 month (until 2024-11) only commits from
    the maintainer
- Python
- [AUR package: ulauncher](https://aur.archlinux.org/packages/ulauncher)
- config in `~/.config/ulauncher`
- Ulauncher forks:
  - [catapult](https://github.com/otsaloma/catapult)
    - <300 commits
  - [dlauncher](https://github.com/diced/dlauncher) (dead)

## Usage

- Start ulauncher as a daemon
- Then use `ulauncher-toggle` to show/hide ulauncher.

## Ulauncher extensions

- [Extension docs](http://docs.ulauncher.io/en/latest/)
- [Many good extensions](https://ext.ulauncher.io/)
- Plugin examples
  - [Bitwarden plugin](https://github.com/kbialek/ulauncher-bitwarden)
  - [pass-ulauncher](https://github.com/yannishuber/pass-ulauncher)
- Install an extension:
  Ulauncher preferences -> extensions -> add extension
  and paste the extension URL

Develop an extension:

```sh
git clone https://github.com/yannishuber/pass-ulauncher.git
~/projects/app-launchers/ulauncher/pass-ulauncher/
ln -s ~/projects/app-launchers/ulauncher/pass-ulauncher/ ~/.local/share/ulauncher/extensions/
```

In order to see the new extension you have to restart Ulauncher.

### Try out

- <https://ext.ulauncher.io/-/github-the-lay-ulauncher-spotify-api>
- <https://ext.ulauncher.io/-/github-nesivmi-ulauncher-translate>
- <https://ext.ulauncher.io/-/github-pbkhrv-ulauncher-keepassxc>
- <https://ext.ulauncher.io/-/github-dcervenkov-ulauncher-z-search>
  <https://github.com/rupa/z>
  <https://github.com/andrewferrier/fzf-z>
- <https://ext.ulauncher.io/-/github-compilelife-ulauncher-searchfile>

### Password extensions

#### Bitwarden

- [ulauncher-rbw](https://0xacab.org/varac-projects/ulauncher-rbw)

Setup for development:

```sh
mkdir -p ~/projects/app-launchers/ulauncher/extensions/
git clone https://0xacab.org/varac-projects/ulauncher-rbw ~/projects/app-launchers/ulauncher/extensions/rbw
ln -s ~/projects/app-launchers/ulauncher/extensions/rbw ~/.local/share/ulauncher/extensions/rbw
```

#### pass

- [ibot3/pass-ulauncher](https://github.com/ibot3/pass-ulauncher), a fork of
  - [yannishuber/pass-ulauncher](https://github.com/yannishuber/pass-ulauncher)
  - which supports gopass
  - No fuzzy find :(
- <https://github.com/m3y/ulauncher-pass-paster>

### Stale / archived

- [yannishuber/pass-ulauncher](https://github.com/yannishuber/pass-ulauncher)

  - Works, but [only supports one pwstore](https://github.com/yannishuber/pass-ulauncher/issues/5)
  - No releases, last commit 2019-05

- [seqizz/ulauncher-password-store](https://github.com/seqizz/ulauncher-password-store)
  Doesn't support API v2

### keepassxc

- [No support for multiple databases](https://github.com/pbkhrv/ulauncher-keepassxc/issues/22)

### System (reboot, hibernate etc)

- <https://ext.ulauncher.io/-/github-leinardi-ulauncher-systemctl> works
- <https://github.com/iboyperson/ulauncher-system> doesn't work:

  `The extension exited instantly. Please check the logs.`

### Exec-terminal

<https://github.com/henriqueutsch/exec-terminal/>

Doesn't install

### ip-lookup

<https://github.com/munim/ulauncher-ip-lookup>

Doesn't work

## Issues

- [v6 Release](https://github.com/Ulauncher/Ulauncher/issues/869)
  is beeing worked on since 2021. v6 brings a new breaking
  API version v3 but the [extension registry](https://ext.ulauncher.io/)
  still doesn't allow v3 extensions to be published
- Not very welcoming for contributors, [comments](https://github.com/Ulauncher/Ulauncher/issues/869#issuecomment-2461564968)
  are beeing marked as off-topic
- All extensions are loaded in background at startup
  [FR: Don't run extensions in the background](https://github.com/Ulauncher/Ulauncher/issues/1063)

### Pre-v6

- Ulauncher crashes on big lists:
  - [Unable to create Cairo image surface: invalid value](https://github.com/Ulauncher/Ulauncher/issues/1420)
  - Fixed in v6
- [Sway: Disable boarders](https://github.com/Ulauncher/Ulauncher/issues/230#issuecomment-546727596)
  - Solution: Run ulauncher with
    [--hide-window](https://github.com/Ulauncher/Ulauncher/issues/230#issuecomment-570736422) and/or
    [--no-window-shadow](https://github.com/Ulauncher/Ulauncher/issues/230#issuecomment-570807951)

## Install ulauncher

```sh
pamac install ulauncher
```

## Development

Build and run v6 from source:

```sh
mkdir -p ~/projects/app-launchers/ulauncher/
cd ~/projects/app-launchers/ulauncher/
git clone https://github.com/Ulauncher/Ulauncher Ulauncher.git
sudo pacman -Syu --needed git bash make sed yarn mypy \
  ruff typos python-{build,black,cairo,pytest,pytest-mock,setuptools}
```

Pull and make:

```sh
cd ~/projects/app-launchers/ulauncher/Ulauncher.git/
git pull
make run
```
