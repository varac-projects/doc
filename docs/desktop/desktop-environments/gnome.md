# Gnome

- [Website](https://www.gnome.org/)
- [Arch wiki](https://wiki.archlinux.org/title/GNOME)

## Install

### Manjaro / Arch

Refer to [pacman.md#Package groups](../../package-managers/pacman.md) for
how to easily select packages from this package group

```sh
sudo pacman -S gnome gnome-control-center
```

### Configure

- See [login-managers.md#GDM](../wayland/login-managers.md)
  how to configure keyboard layout for GDM
- (Permanently) [disabling Desktop Screen Lock in GNOME](https://www.baeldung.com/linux/gnome-disable-screen-lock)

## Extensions

- [Arch wiki: Gnome/Extensions](https://wiki.archlinux.org/title/GNOME#Extensions)
- In Arch the `gnome-shell-extensions` package is already installed
  as a dependency of `gnome-shell`

### cli tool

(in Arch/Manjaro)

List extensions:

```sh
gnome-extensions list
gnome-extensions list --details
```

Ubuntu / Debian:

```sh
sudo apt-get install gnome-shell-extension-prefs
```

## gsettings / dconf

[Gconf, Dconf, Gsettings and the relationship between them](https://askubuntu.com/questions/249887/gconf-dconf-gsettings-and-the-relationship-between-them)

Backup gsettings:

```sh
gsettings list-recursively | sort > ~/backups/gsettings/gsettings.bak
cd ~/backups/gsettings/; gsettings list-recursively | sort > gsettings.bak
git add gsettings.bak; git commit -m'Checkin'
```

See available keybord shortcuts:

```sh
grep keybindings ~/backups/gsettings/gsettings.bak
```

## Indicators

## Guillotine

[Github](https://gitlab.com/ente76/guillotine)

> Guillotine is a gnome extension designed for efficiently carrying out
> executions of commands from a customizable menu. Simply speaking:
> it is a highly customizable menu that enables you
> to launch commands and toggle services.

### Argos

[Github](https://github.com/p-e-w/argos)

> Create GNOME Shell extensions in seconds

- Unfortunatly [unclear future of project](https://github.com/p-e-w/argos/issues/108)

### Old i3 hacks

```sh
gsettings set org.gnome.desktop.background show-desktop-icons false
```

Remove bottom panel for i3

```sh
gsettings get org.gnome.gnome-panel.layout toplevel-id-list
gsettings set org.gnome.gnome-panel.layout toplevel-id-list "['top-panel']"
```

Allow gnome-control center settings in gnome-i3 session

```sh
sudo sed 's/^OnlyShowIn=GNOME;Unity;$/OnlyShowIn=GNOME;i3;/' -i /usr/share/applications/*.desktop
sudo sed 's/^OnlyShowIn=GNOME;$/OnlyShowIn=GNOME;i3;/' -i /usr/share/applications/*.desktop
```
