# Tiling window managers

## Tiling in gnome

### Pop Shell

- [Pop Shell](https://github.com/pop-os/shell/)
  - [How To Install Pop Shell Window Tiling Extension](https://www.linuxuprising.com/2020/05/how-to-install-pop-shell-tiling.html)

`~/projects/gnome/popos-shell`

Available web searches: see `~/projects/gnome/popos-shell/src/plugins/web/main.js`
for web search shortcuts see `~/projects/gnome/popos-shell/src/plugins/web/meta.json`

#### API workarounds

- [Is there a command to go a specific workspace?](https://askubuntu.com/questions/41093/is-there-a-command-to-go-a-specific-workspace)
- [Use custom key bindings to switch to workspace](https://github.com/pop-os/shell/issues/142#issuecomment-703276661)

Go to workspace 1:

```sh
wmctrl -s 1
```

#### Issues/Feature requests

- [FR: Auto-positioning defaults for windows](https://github.com/pop-os/shell/issues/413)
  - Related: [Option to remember window layout from last session](https://github.com/pop-os/shell/issues/735)
  - <https://developer.gnome.org/SaveWindowState/>
  - [FR: control windows position by cli](https://github.com/pop-os/shell/issues/468)
- [Multi-head mode, or independent workspaces per monitor](https://github.com/pop-os/shell/issues/823)
- [Send window to numbered workspace](https://github.com/pop-os/shell/issues/689)
- [Change workspaces with Super + number](https://github.com/pop-os/shell/issues/142)
- [FR: Show workspace names](https://github.com/pop-os/shell/issues/835)
- Lacking option of setting the default orientation mode

### Other Gnome tiling option

- [Material Shell](https://material-shell.com)

- [Forge](https://extensions.gnome.org/extension/4481/forge/)

- [Tiling-Assistant](https://github.com/Leleat/Tiling-Assistant)

- [gTile](https://github.com/gTile/gTile)

- [Tactile](https://gitlab.com/lundal/tactile)
  Con: Configurable, but static window layout

- [PaperWM](https://github.com/paperwm/PaperWM)
  Con: Last release 2020-09
