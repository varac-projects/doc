# localsearch (formerly Tracker) desktop indexer

- [Docs](https://gnome.pages.gitlab.gnome.org/localsearch/)

## Config

```sh
gsettings list-recursively | grep -i org.freedesktop.Tracker | sort | uniq
```

- database: `~/.cache/tracker3/meta.db`

## Usage

```sh
localsearch status
localsearch search test
```
