# Wayland screencasting

- https://github.com/emersion/xdg-desktop-portal-wlr/wiki/Screencast-Compatibility#obs-xdg-portal
- https://www.reddit.com/r/swaywm/comments/ev3ty6/does_anyone_know_a_workaround_to_share_screen_in/
- https://www.reddit.com/r/swaywm/comments/fq1qoe/how_to_use_xdgdesktopportalwlr_to_share_the_screen/
- [Arch wiki: PipeWire/WebRTC screen sharing](https://wiki.archlinux.org/index.php/PipeWire#WebRTC_screen_sharing)
- [Mozilla webrtc test page](https://mozilla.github.io/webrtc-landing/gum_test.html)
- [webrtc.github.io](https://webrtc.github.io/samples/)
  - [Device input/output](https://webrtc.github.io/samples/src/content/devices/input-output/)

## Tools

- [Arch wiki: Screen capture](https://wiki.archlinux.org/title/Screen_capture)

### wf-recorder

- https://github.com/ammen99/wf-recorder
- [Experimental fork which adds ffmpeg filters and other stuff](https://github.com/schauveau/wf-recorder-x)

Usage:

    wf-recorder --muxer=v4l2 --codec=rawvideo --file=/dev/video2 -x yuv420p

- Can select and display stream in chromium
- Can't select stream in firefox
- Can select and display stream on mozilla webrtc gum page

but it only shows external monitor

### wlstreamer

https://github.com/b12f/wlstreamer

Issues:

- [Autofocus across screens doesn't work properly, only shows external monitor](https://github.com/b12f/wlstreamer/issues/2)

Install:

     cd ~/projects/wayland/wlstreamer
     cargo install --path . --root ~/.local

Load `v4l2loopback` kernel module (now auto loaded during boot):

    sudo modprobe v4l2loopback exclusive_caps=1 card_label=WfRecorder

See which video device the loopback module claimes:

    v4l2-ctl --list-devices

Share particular screen:

    wlstreamer --not-screen DP-3 -o /dev/video0

With autofocus (beware of above bug!):

    wlstreamer -o /dev/video2

### Mon2Cam

https://github.com/ShayBox/Mon2Cam

- Typescript (requires deno runtime)

### Other video loopback solutions

- https://gist.github.com/progandy/bff675311aa2c3b777a37abe81aa4b4d
- https://gitlab.com/jamedjo/gnome-dbus-emulation-wlr

#### Fake screensharing using video loopback

https://wiki.archlinux.org/index.php/Screen_capture#Via_a_virtual_webcam_video_feed

Best solution so far!

    screenshare.sh

And then use chromium instead of firefox (see more details below)

### Native Screensharing

- [Arch wiki: WebRTC screen sharing](https://wiki.archlinux.org/title/PipeWire#WebRTC_screen_sharing)
- [Make screen sharing on wayland (sway) work!](https://soyuka.me/make-screen-sharing-wayland-sway-work/)
- Chromium: Use `enable-webrtc-pipewire-capturer` flag
- [Arch wiki: Zoom Meetings](https://wiki.archlinux.org/title/Zoom_Meetings)

#### xdg-desktop-portal-wlr

- [xdg-desktop-portal-wlr FAQ](https://github.com/emersion/xdg-desktop-portal-wlr/wiki/FAQ)
- FR: [Allow sharing of individual windows](https://github.com/emersion/xdg-desktop-portal-wlr/issues/107)

##### Issues

- [Screen share broken in the latest chromium-based browsers (v112)](https://www.reddit.com/r/swaywm/comments/12ioanb/screen_share_broken_in_the_latest_chromiumbased/)
  - Happened 2023-04 with `xdg-desktop-portal-wlr` `0.6.0-1` and `chromium` `112.0.5615.165-1`
  - Workaround until a new release of `xdg-desktop-portal-wlr` is shipped in
    Arch: Install `xdg-desktop-portal-wlr-git` (`v0.3.0.r15.gefcbcb6-1` fixed it!)

## Browser notes

https://github.com/emersion/xdg-desktop-portal-wlr/wiki/Screencast-Compatibility

### Firefox

https://developer.mozilla.org/de/docs/Mozilla/Developer_guide/Build_Anweisungen/Simple_Firefox_build
https://firefox-source-docs.mozilla.org/setup/linux_build.html#lets-build-firefox

- Mozilla webrtc gum page cuts the borders of the fake screencast video stream -
  but the same stream displays fine in chromium (88).
- In jitsi the image is also cropped, and has a very bad resolution → Use
  chromium for now

## Chromium

Ubuntu snap uses X11, not wayland

https://bugs.chromium.org/p/chromium/issues/detail?id=682122
https://fhackts.wordpress.com/2019/07/08/enabling-webrtc-desktop-sharing-under-wayland/

The only solution is to use wf-recorder (see below).

run chrome in wayland mode:

    chrome --enable-features=UseOzonePlatform --ozone-platform=wayland

## Screencast tools

### Jitsi

Issues with fake loopback video screencast:

- [Local image of screencast is mirrored, but all other can see it fine](https://github.com/jitsi/jitsi-meet/issues/6863)

### Server tools

#### Screego

screen sharing for developers

- [Website](https://screego.net)

Not sure if it works with wayland .. ?

## Build xdg-desktop-portal and xdg-desktop-portal-wlr

### xdg-desktop-portal

- [Ubuntu xdg-desktop-portal deb](https://github.com/flatpak/xdg-desktop-portal/issues/446)
  is built without pipewire support ! So we need to build informatation

Build:

    cd ~/project/wayland/xdg-desktop-portal
    sudo apt install libjson-glib-dev libportal-dev libgeoclue-2-dev libfuse-dev
    ./autogen.sh
    make
    sudo make install

    /usr/local/libexec/xdg-desktop-portal

### xdg-desktop-portal-wlr

Build:

    cd ~/project/wayland/xdg-desktop-portal-wlr
    meson build
    ninja -C build
    sudo ninja -C build install

    /usr/local/libexec/xdg-desktop-portal-wlr

Test (run all cmds in different terminals to watch the output):

    systemctl --user stop xdg-desktop-portal-wlr.service
    systemctl --user stop xdg-desktop-portal.service
    systemctl --user stop xdg-desktop-portal-gtk.service
    ps aux | grep xdg-des

    /usr/local/libexec/xdg-desktop-portal-wlr -l DEBUG
    /usr/local/libexec/xdg-desktop-portal-gtk --verbose -r
    /usr/local/libexec/xdg-desktop-portal --verbose -r
    journalctl -f
