# Wayland bars

- [Bars & panels](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway#bars--panels)

## Bars

### Waybar

Much better than swaybar !

- [GitHub](https://github.com/Alexays/Waybar)
- [Examples](https://github.com/Alexays/Waybar/wiki/Examples)

#### Usage

Restart waybar:

```sh
systemctl --user restart waybar.service
```

#### Modules

Overview:

- [Waybar wiki: Modules](https://github.com/Alexays/Waybar/wiki)
- [Waybar wiki: 3rd-party modules](https://github.com/Alexays/Waybar/wiki/Module:-Custom:-Third-party)

Modules to try:

- [waybar-issues](https://github.com/andresterba/waybar-issues)
  - Get your open github/gitlab issues in your waybar
  - [AUR package](https://aur.archlinux.org/packages/waybar-issues)
- [waybar-eyes](https://github.com/cyrinux/waybar-eyes)
  - help you blinking eyes and move, take a break
  - [AUR package](https://aur.archlinux.org/packages/waybar-eyes)

#### Issues

- [Warning and Gtk-Critical messages from waybar](https://github.com/Alexays/Waybar/issues/1373)
- [Waybar/GTK apps take a long time to start](https://github.com/swaywm/sway/wiki#gtk-applications-take-20-seconds-to-start)

### Swaybar

#### Swaybar Issues

I tried again with sway 1.1rc1 (2019-05), and swaybar is still not working for
any of the tray icons except for nextcloud. Waybar is the way to go!

#### Tray protocols

There are 3 different tray icons protocols:

- Xembed (i.e. nm-applet): not supported
  [Support for legacy Xembed will come later](https://github.com/swaywm/sway/issues/1357)
- StatusNotifierItem / SNI: supported
- DBus: Still not supported [Tray D-Bus Menu #6249](https://github.com/swaywm/sway/pull/6249)
- Assigned workspace doesn't indicate in red when a new app creates it

[swaybar tray context menu is not shown](https://github.com/swaywm/sway/issues/3939)
