# Wayland screen overlay apps

## eww

- [eww](https://elkowar.github.io/eww)
- [Introduction to eww](https://dharmx.is-a.dev/eww-powermenu/)

Issues:

- eww windows also hide behind fullscreen mode
- [Not possible to show window on all monitors](https://github.com/elkowar/eww/issues/648)

## Other overlay apps

- [nwg-wrapper](https://github.com/nwg-piotr/nwg-wrapper)
  - Can display text on (empty !) wayland screens
  - not under active development
- [r/swaywm: Searching for (popup) terminal emulator with overlay wayland shell support](https://www.reddit.com/r/swaywm/comments/sqwkki/searching_for_popup_terminal_emulator_with/)
  - Floating windows (i.e. scratchpad) [doesn't work in full screen mode](https://github.com/swaywm/sway/issues/6799)
- [waytext](https://github.com/jeffa5/waytext)
  - last commit 2023

## Non-overlay options

- [zenity](https://gitlab.gnome.org/GNOME/zenity)
- dialog program
