# Lock screen

## hyprland tools

### hypridle

- [GitHub](https://github.com/hyprwm/hypridle)
- [Hyprland docs: hypridle](https://wiki.hyprland.org/Hypr-Ecosystem/hypridle/)

```sh
pamac install hypridle
```

### hyprlock

- [GitHub](https://github.com/hyprwm/hyprlock)
- [Hyprland docs: hyprlock](https://wiki.hyprland.org/Hypr-Ecosystem/hyprlock/)

```sh
pamac install hyprlock
```

## Other tools

- [idlehack](https://github.com/loops/idlehack)
  - Monitor dbus and inhibit swayidle when Firefox or Chromium request it
    (typically because the user is watching video)
  - Included in manjaro sway edition

## sway tools

**Warning:** All tools below are not actively developed anymore,
use hypridle + hyprlock instead !

### systemd-lock-handler

- [Sourcehut](https://git.sr.ht/%7Ewhynothugo/systemd-lock-handler)
- [Last tag 2023-05](https://git.sr.ht/~whynothugo/systemd-lock-handler/refs)

[ItsDrike/dotfiles: Move to systemd-lock-handler instead of swayidle for locking](https://github.com/ItsDrike/dotfiles/commit/1f8bebd13b6db7988e1d4d0679902a2ceee541e9):

> Swayidle will likely end up dropping logind support, and they themselves
> describe it as unstable. For this reason, move to the suggested
> alternative: systemd-lock-handler, and just have swayidle run the
> command to trigger the logind session lock event, picked up by
> systemd-lock-handler.

#### Install

Import key from `Hugo Osvaldo Barrera <hugo@whynothugo.nl>` and install:

```sh
gpg --keyserver keyserver.ubuntu.com --search-keys 7880733B9D062837
pamac install systemd-lock-handler
```

#### Setup

Enable systemd user service:

```sh
systemctl --user enable --now systemd-lock-handler.service
```

Then add `~/.config/systemd/user/lockscreen.service`
(see [dotfiles](https://0xacab.org/varac-projects/dotfiles/-/blob/main/home/dot_config/systemd/user/lockscreen.service?ref_type=heads))
and enable it:

```sh
systemctl --user enable lockscreen.service
```

Enter `lock.target` with `loginctl lock-session`

### swayidle

- [GitHub](https://github.com/swaywm/swayidle)
- [Man page](https://github.com/swaywm/swayidle/blob/master/swayidle.1.scd)
- Handles logind inhibitors

### swaylock

- [swaylock](https://github.com/swaywm/swaylock)
- Last release 2022-12
- Comes pre-installed with Manjaro sway edition

Issues:

- Locking the screen before sleep is unreliable, and
  swayidle / swaylock don't handle locking on lid close events
  - [Drop logind integration](https://github.com/swaywm/swayidle/issues/117)
    - [PR: Drop logind integration](https://github.com/swaywm/swayidle/pull/133)
- Consecutive failed login attempts prevents from unlocking
  (`/etc/security/faillock.conf` has `deny = 3` as default)
  - [Arch wiki: Lock out user after three failed login attempts](https://wiki.archlinux.org/title/Security#Lock_out_user_after_three_failed_login_attempts)

### swaylock-effects

- [GitHub](https://github.com/mortie/swaylock-effects)
- Swaylock, with fancy effects
- Comes pre-installed with Manjaro sway edition
- Last release 2023-01

Replace `swaylock-effects` with `swaylock`:

```sh
sudo pacman -S swayloc
```
