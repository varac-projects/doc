# Apps that don't start in native wayland mode by default

## Electron apps

* [archlinux wiki Electron](https://wiki.archlinux.org/title/wayland#Electron)

There's no better way of running electron apps natively in wayland
than to modify their desktop files and add the chromium flags there, see
<https://github.com/electron/electron/issues/30897#issuecomment-1155635309>

Work:

* Chromium

Issues

* [Screensharing doesn't work in jitsi-meet](https://github.com/jitsi/jitsi-meet-electron/issues/608),
  related upstream [electron issue](https://github.com/electron/electron/issues/30652)
