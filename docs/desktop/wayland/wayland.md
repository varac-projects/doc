# Wayland

- [Arch wiki: Wayland](https://wiki.archlinux.org/title/Wayland)
  - [Compositors](https://wiki.archlinux.org/title/Wayland#Compositors)
    - [Dynamic](https://wiki.archlinux.org/title/Wayland#Dynamic)

## Wayland compatible apps

- [Are we Wayland yet?](https://arewewaylandyet.com/)
- [Anarcat's wiki](https://anarc.at/software/desktop/wayland/)

## Etc

How to tell if Wayland is running: `

```sh
echo $XDG_SESSION_TYPE
```
