# Wayland display management

## Multi-display setup

- [swayws](https://github.com/hw0lff/swayws): easy moving of workspaces to and from outputs
- [Two Alternative Multi-Monitor Setups](https://github.com/swaywm/sway/issues/4771)

## way-displays

- [way-displays](https://github.com/alex-courtis/way-displays)
- Multiple contributors
- Support for way-displays in `/usr/share/sway/scripts/scale.sh`
  (deployed by `Manjaro Sway edition`)

### Install

```sh
sudo pacman -S way-displays
```

### Config

```sh
mkdir -p ~/.config/way-displays
cp /etc/way-displays/cfg.yaml ~/.config/way-displays/cfg.yaml
```

Add yourself to the input group to monitor events:

```sh
sudo usermod -a -G input "${USER}"
```

Remove any output commands from your sway config file and add the following:

```sh
exec way-displays > /tmp/way-displays.${XDG_VTNR}.${USER}.log 2>&1
```

Restart the compositor

### Usage

```sh
systemctl --user enable way-displays.service
```

Show current config:

```sh
way-displays -g
```

Logs:

```sh
cat /tmp/way-displays*
```

Tweak `cfg.yaml` to your liking and save it. Changes will be immediately applied.

## Kanshi

- [Kanshi](https://sr.ht/~emersion/kanshi/): define output profiles that are
  automatically enabled and disabled on hotplug
- No other contributors
- Wildcard support missing [Feature request: Use regex in output name](https://todo.sr.ht/~emersion/kanshi/49)

## Shikane

- [GitHub](https://github.com/hw0lff/shikane)
- No other contributors
- Doesn't work with `/usr/share/sway/scripts/scale.sh`

## srandr

Ncurses randr UI

- [Gitlab](https://gitlab.com/ragon000/srandr/)

- [Installation fails](https://gitlab.com/ragon000/srandr/issues/1)

## wl-mirror

- [wl-mirror](https://github.com/Ferdi265/wl-mirror)
  simple Wayland output mirror client

## Fonts / Display setup

- [Archwiki HiDPI](https://wiki.archlinux.org/title/HiDPI)
- [Font Scaling](https://www.reddit.com/r/swaywm/comments/vplczr/font_scaling/)
- [Wayland Protocol Finally Ready For Fractional Scaling](https://www.phoronix.com/news/Wayland-Fractional-Scale-Ready)

## Brightness controll

### wluma

- [GitHub](https://github.com/maximbaz/wluma)
- Config at `~/.config/wluma/config.toml`
  - [Example config](https://github.com/maximbaz/wluma/blob/main/config.toml)

Install:

```sh
sudo pacman -S wluma ddcutil
```

[Grant udev permissions](https://github.com/maximbaz/wluma?tab=readme-ov-file#permissions):

```sh
sudo cp /usr/lib/udev/rules.d/90-wluma-backlight.rules /etc/udev/rules.d/
reboot
```

Configure:

```sh
mkdir ~/.config/wluma/
cp /usr/share/wluma/examples/config.toml ~/.config/wluma/
```

Usage:

```sh
systemctl --user enable wluma
systemctl --user start wluma
```

Debug:

```sh
RUST_LOG=debug wluma
```

### Issues

- [FR: Support custom external sources for luminance value](https://github.com/maximbaz/wluma/issues/98)
  - [feat: add supprt for als.cmd to read brightness from external script](https://github.com/maximbaz/wluma/pull/99)

## Color temperature management

### Gammastep

Alternative to Redshift

- [Gitlab](https://gitlab.com/chinstrap/gammastep)

Install:

```sh
sudo apt install gammastep
```

Or redshift-wayland (but there's no debian package):

- [Wayland support issue](https://github.com/jonls/redshift/issues/55)
  [Wayland support PR](https://github.com/jonls/redshift/pull/663)
- [Wayland support PR](https://github.com/swaywm/sway/issues/2700)
- [wayland compatible redshift fork](https://github.com/minus7/redshift)
