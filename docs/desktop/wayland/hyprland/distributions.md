# Hyprland distributions

- [Hyprland wiki: Preconfigured setups](https://wiki.hyprland.org/Getting-Started/Preconfigured-setups/)

## ml4w / mylinuxforwork

- [mylinuxforwork/dotfiles](https://github.com/mylinuxforwork/dotfiles)
- ~2k stars, 26 contributors
- Last release 2025-02
- Recent activity, recent releases, ~1.7k commits
- local at `~/projects/hyprland/distributions/mylinuxforwork/dotfiles`
- AUR packages: `ml4w-hyprland`, `ml4w-hyprland-git`
  from the ml4w author
- Cons: Installation needed, some dotfiles are just templates

## HyDE

- [GitHub](https://github.com/Hyde-project/hyde)
- "Portable and extensible re-work of prasanthrangan/hyprdots"
- 500 stars, 17 contributors
- Recent activity, recent releases, ~2k commits
- AUR package: `hyde-cli-git` (outdated)

### Key bindings

- Show keybindings dialog in Hyprland: `Mod Ctrk K`
- default keybindings: `share/dotfiles/.config/hypr/conf/keybindings/default.conf`

- Reload hyprland config: `Mod Shift R`
- App launcher (Rofi): `Mod Crtl Return`

## JaKooLit

- [KooL's Hyprland Dotfiles](https://github.com/JaKooLit/Hyprland-Dots)
- 1.4k stars, 35 contributors
- Recent activity, recent releases, ~1.7k commits

## end_4

- [GitHub](https://github.com/end-4/dots-hyprland)
- 5.2k stars, 56 contributors
- Recent activity, ~2k commits
- Last release 2023
- Cons:
  - [Seems to compile hyprland](https://end-4.github.io/dots-hyprland-wiki/en/i-i/01setup/#before-you-start)

## Unmaintained

### prasanthrangan

- [GitHub](https://github.com/prasanthrangan/hyprdots)
- 8.6k stars, 95 contributors
