# hyprland

- [Awesome Hyprland](https://github.com/hyprland-community/awesome-hyprland)

## Install

- [Master tutorial](https://wiki.hyprland.org/Getting-Started/Master-Tutorial/)
- [Useful Utilities: Must have](https://wiki.hyprland.org/Useful-Utilities/Must-have/)

```sh
pamac install hypridle hyprlock hyprland xdg-desktop-portal-hyprland \
  hyprpolkitagent-bin qt5-wayland qt6-wayland waypaper hyprpaper wlogout \
  xdg-desktop-portal-hyprland noto-fonts ags-hyprpanel-git
```

## Setup

### Displays

[Docs: Monitors](https://wiki.hyprland.org/Configuring/Monitors/)

Show all displays/monitors:

```sh
hyprctl monitors all
```
