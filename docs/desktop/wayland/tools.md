# Wayland tools

See also [Useful add ons for sway](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway)

## Clipboards

- [Keeping secrets secret with keepassxc, clipman and swaywm](https://www.reddit.com/r/swaywm/comments/ljl0dh/keeping_secrets_secret_with_keepassxc_clipman_and/)
- [cliphist](https://github.com/sentriz/cliphist)
- [clipman](https://github.com/chmouel/clipman)

### wayland-clipboard

- [Github](https://github.com/bugaevc/wl-clipboard.git)

[Ubuntu eoan packaged wl-clipboard 1.0 won't work with sway 1.2](https://github.com/bugaevc/wl-clipboard/issues/52)

Build and install:

```sh
cd ~/projects/wayland/wl-clipboard
gup
rm -rf build; meson build && ninja -C build && sudo ninja -C build install
```

## Libappindicator tray

### Electron apps

from [Libappindicator tray #21](https://github.com/Alexays/Waybar/issues/21):

> You need libappindicator-gtk3 (added to AUR package)
> and launch apps with env XDG_CURRENT_DESKTOP=Unity slack

## Notifications

### Sway notification center

- [GitHub](https://github.com/ErikReider/SwayNotificationCenter)
- [feature req: Persistence across restart](https://github.com/ErikReider/SwayNotificationCenter/issues/163)

### Mako

Notification daemon

- [Debian ITP](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=912865)

### Battery warning notifications

- [Reddit thread](https://www.reddit.com/r/swaywm/comments/rhrj9e/low_battery_alerts/)
- [poweralertd](https://sr.ht/~kennylevinsen/poweralertd/): No config possible
- [swaynag-battery](https://github.com/m00qek/swaynag-battery): Configurable
- [batsignal](https://github.com/electrickite/batsignal)

## Screenshots with grim + slurp

[grim](https://github.com/emersion/grim)

```sh
apt install grim slurp

grim -g "$(slurp)" /tmp/s.png
```

## Screencast / screen recording

[Arch wiki: Screen recording](https://wiki.archlinux.org/index.php/Wayland#Screen_recording)

### wf-recorder

> wf-recorder is a utility program for screen recording of wlroots-based
> compositors

- [wf-recorder](https://github.com/ammen99/wf-recorder)

Install:

```sh
 git clone https://github.com/ammen99/wf-recorder.git && cd wf-recorder
 sudo apt install ocl-icd-opencl-dev libavdevice-dev
 meson build --prefix=/usr --buildtype=release
 ninja -C build
```

### green-recorder

- [green-recorder](https://github.com/hhlp/green-recorder)
- Forked from [green-recorder](https://github.com/mhsabbagh/green-recorder)
  unclear who's maintaining this project
- [Installable from ppa](https://github.com/hhlp/green-recorder#download)
- Python2 :/

## Debug input events

```sh
sudo libinput debug-events
```
