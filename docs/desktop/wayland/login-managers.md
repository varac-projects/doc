# Wayland login managers

- [Arch wiki: Display managers](https://wiki.archlinux.org/title/Wayland#Display_managers)
- [Sway wiki: Login managers](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway#login-managers)

## Greetd

- [Sourcehut](https://git.sr.ht/~kennylevinsen/greetd)
- [Wiki](https://man.sr.ht/~kennylevinsen/greetd/#customization)
- [Arch wiki: greetd](https://wiki.archlinux.org/title/Greetd)
- [Default login manager in Manjaro Sway Edition](https://github.com/manjaro-sway/manjaro-sway/blob/main/SUPPORT.md?plain=1)

Install:

```sh
paru greetd
```

Setup:

- [Setting up greetd](https://man.sr.ht/~kennylevinsen/greetd/#setting-up-greetd)

Steps:

```sh
sudo systemctl -f enable greetd.service
sudo systemctl -f start greetd.service
```

Login flow:

- Greetd starts, using `/etc/greetd/config.toml`
- Greetd starts greeter using `default_session.command` (i.e. `tuigreet`)
- Greeter offers sessions configured in `/usr/share/wayland-sessions/`

### Auto login

[Arch wiki: Gretd/Auto-login](https://wiki.archlinux.org/title/Greetd#Enabling_autologin)
Add to `/etc/greetd/config.toml`:

```toml
[initial_session]
command = "sway-user-service"
user = "varac"
```

Exit session and get back to greetd login:

```sh
sudo systemctl restart greetd.service
```

### Greeters

- [Arch wiki: Greetd/Greeters](https://wiki.archlinux.org/title/Greetd#Greeters)

#### ReGreet

- [GitHub](https://github.com/rharish101/ReGreet)
- Rust
- Recent releases
- Recommended by [hyprland](https://wiki.hyprland.org/Getting-Started/Master-Tutorial/#launching-hyprland)
- Needs a compositor (sway, hyprland) to work
- [Arch wiki: Greetd/ReGreet](https://wiki.archlinux.org/title/Greetd#ReGreet)
- [AUR package greetd-regreet-git](https://aur.archlinux.org/packages/greetd-regreet-git) up to date
- [Arch package greetd-regreet](https://archlinux.org/packages/extra/x86_64/greetd-regreet/) lacks behind

#### QtGreet

- [GitHub](https://github.com/marcusbritanicus/QtGreet)
- Recent commits
- Supports Shut down, reboot
- No Xorg deps

#### tuigreet

- [Github](https://github.com/apognu/tuigreet)
- Default greeter in Manjaro-sway
- Stale, last commit 2024-06

##### Keyboard layout

`tuigreet` uses `/etc/vconsole.conf` for keyboard layout. For german keyboard:

```sh
~ $ cat /etc/vconsole.conf
# https://man.archlinux.org/man/vconsole.conf.5.en
KEYMAP=de-latin1
FONT=eurlatgr
FONT_MAP=
```

#### wlgreet

- [wlgreet](https://git.sr.ht/~kennylevinsen/wlgreet)
  Works, but last tag 2024-04

Install:

```sh
paru greetd-wlgreet
```

Setup:

- [Setting up greetd with wlgreet](https://man.sr.ht/~kennylevinsen/greetd/#setting-up-greetd-with-wlgreet)

Issues:

- No options to reboot, shutdown etc.
- I can't find any option to set the keyboard layout

## GDM

- [Configure keyboard layout](https://wiki.archlinux.org/title/GDM#Keyboard_layout)

## SDDM

- [sddm](https://wiki.archlinux.org/title/SDDM)

### SDDM wayland support

- For proper Wayland support [sddm-git](https://aur.archlinux.org/packages/sddm-git) is needed
  until a new [release](https://github.com/sddm/sddm/releases) is made `>0.19.0`
- See also:
  - [0.20.0 milestone](https://github.com/sddm/sddm/milestone/13)
  - [Wayland SDDM](https://github.com/LeoMeinel/arch-install/issues/34)
- Beware: Compiling `sddm-git` won't work on Asahi Linux
- Also: By default SDDM still uses a Xorg session for the greeter, even when
  it starts a wayland session afterwards.
  - I didn't want to configure a german keyboard using Xorg files.
  - Starting sddm in native wayland mode didn't work for me, so I decided to
    switch to `greetd`

Install:

```sh
paru -S sddm-git
```
