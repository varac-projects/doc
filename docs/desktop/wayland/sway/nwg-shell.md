# nwg-shell

* [Website](https://nwg-piotr.github.io/nwg-shell/)
* [Github](https://github.com/nwg-piotr/nwg-shell)
* [Wiki](https://github.com/nwg-piotr/nwg-shell/wiki)

## Install nwg-shell on Arch

* [How to set up nwg-shell on minimal Arch Linux install in several simple steps](https://github.com/nwg-piotr/nwg-shell/wiki)

Add these packages (Choose `pipewire-jack` and `wireplumber` as answers):

    sudo pacman -S nautilus neovim firefox network-manager-applet \
      xdg-user-dirs git man-db

## Issues

### Asahi

Most packages are not available for `aarch64`. They need to get installed manually.

Ignore warnings about incompability:

    paru -S wdisplays wlsunset swaync python-dasbus azote autotiling gopsuinfo \
      nwg-bar nwg-dock nwg-menu nwg-drawer nwg-wrapper nwg-shell-config \
      nwg-panel
    paru -S nwg-shell

### Etc

* Reboot: Sddm job hangs for 1,5m during shut down --> Install `sddm-git`
* Suspend/hibernate only via CLI, not in GUI

## Installation on PopOS 22.04

Currently not possible without great pain..
