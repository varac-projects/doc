# Manjaro-sway

- [Github](https://github.com/manjaro-sway/manjaro-sway)
- [FAQ/Support](https://github.com/manjaro-sway/manjaro-sway/blob/main/SUPPORT.md)

## Install

- [Download](https://manjaro-sway.download/) and flash to usb stick
- Boot

## Upgrade

[How do I upgrade the ~/.config after an update?](https://github.com/manjaro-sway/manjaro-sway/blob/main/SUPPORT.md#how-do-i-upgrade-the-config-after-an-update)

## Customize

- Use swaync instead of mako: See [Disable/Remove mako](https://github.com/manjaro-sway/manjaro-sway/issues/330)
