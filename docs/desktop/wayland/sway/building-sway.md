# Building Sway

## Deps

### meson

    sudo apt install meson

### wlroots

https://github.com/swaywm/wlroots.git

    sudo apt install libxcb-xinput-dev libavutil-dev libavcodec-dev libavformat-dev
    apt install wayland-protocols

Build & Install:

    cd ~/projects/wayland/sway/wlroots
    gup
    meson --reconfigure build && ninja -C build && sudo ninja -C build install

## Build sway

### Install deps:


    sudo apt install libcap-dev libpam0g-dev scdoc libjson-c-dev

Build and install:

    cd ~/projects/wayland/sway/sway
    gup
    meson --reconfigure build && ninja -C build && sudo ninja -C build install

Run:

      LD_PRELOAD=/usr/local/lib/x86_64-linux-gnu/libwlroots.so sway
