# Sway input config

- [sway-input man page](https://man.archlinux.org/man/sway-input.5)
- [Configure Keyboard Layout Switching for Sway](https://lebenplusplus.de/2023/02/18/configure-keyboard-layout-switching-for-sway/)

## Key remapping

- [Arch wiki: Input remap utilities](https://wiki.archlinux.org/title/Input_remap_utilities)

Problem: When using the Apple magic keyboard with the `kbd_options
altwin:swap_alt_win` to swap *both* alt and super keys, the right alt is not
mapped to `alt_gr` anymore. `kbd_options` are pretty limited in such a scanario,
therefore more sophisticated tools like `keyd` can be used.

### keyd

- [Github](https://github.com/rvaiya/keyd)
- Config files at `/etc/keyd/*.conf`
- [keyd man page](https://github.com/rvaiya/keyd/blob/master/docs/keyd.scdoc)

Install:

    pamac install keyd
    sudo systemctl enable keyd
    sudo systemctl start keyd

#### keyd issues

- [Can't properly setup compose key](https://github.com/rvaiya/keyd/issues/614)

## Other input config tools

- [sway-input-config](https://github.com/Sunderland93/sway-input-config)
  GUI Input device configurator for SwayWM
