# Sway keybindings

Showing current keybindings using swaymsg is not possible at the moment

* [ipc: add query for mode bindings](https://github.com/swaywm/sway/pull/5561)
* [Get list of key bindings via swaymsg](https://github.com/swaywm/sway/issues/7179)
* [List all keybindings in running session](https://github.com/swaywm/sway/issues/6774)
* [Manjaro-sway: Parse bindsym and add help to launcher](https://github.com/manjaro-sway/manjaro-sway/issues/14)

Show current keybindings using custom alias as a Markdown table:

    sway_keybindings

Show current keybindings using custom alias, raw:

    sway_keybindings_raw

## remontoire

A graphical keybinding viewer for i3 and other programs.

* [Github](https://github.com/regolith-linux/remontoire)
* Last commit 2022-06
* [How to use Remontoire as a Sway keyboard shortcut viewer](https://www.reddit.com/r/swaywm/comments/jvvt16/how_to_use_remontoire_as_a_sway_keyboard_shortcut/)

Install:

    pacman -S core/automake core/autoconf
    pamac install remontoire-regolith

Usage:

    remontoire -c ~/.config/sway/config.d/40-bindsym.conf

or [Using Remontoire to view keybindings for arbitrary config files](https://github.com/regolith-linux/remontoire#using-remontoire-to-view-keybindings-for-arbitrary-config-files):

    cat ~/.config/sway/config.d/40-bindsym.conf | remontoire -i -p '"'

## i3keys

Lists available bindings for i3 or Sway with a graphical or text keyboard

* [Github](https://github.com/RasmusLindroth/i3keys)
* Last commit 2022-10
