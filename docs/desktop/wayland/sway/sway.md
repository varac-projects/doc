# Sway

## Docs / resources

* [Arch wiki](https://wiki.archlinux.org/title/sway)

(Awesome) lists:

* [oofdog/awesome-wayland](https://github.com/oofdog/awesome-wayland)
* [natpen/awesome-wayland](https://github.com/natpen/awesome-wayland): Archived

Other resources:

* [Useful add ons for sway](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway)
* [Archlinux wiki](https://wiki.archlinux.org/title/sway)
* [reddit](https://www.reddit.com/r/swaywm/)

## Installation guides

* [Installation guide for the latest sway version on Ubuntu 20.04](https://llandy3d.github.io/sway-on-ubuntu/)
  * Author's Sway [dotfiles](https://github.com/Llandy3d/dotfiles)
* [mavjs/sway-gnome](https://github.com/mavjs/sway-gnome) (Stale)
  * Based on [Drakulix/sway-gnome](https://github.com/Drakulix/sway-gnome) (Stale)

## Distributions

* [manjaro-sway](https://github.com/manjaro-sway/manjaro-sway)
* [nwg-shell](https://nwg-piotr.github.io/nwg-shell/)
* [SwayOS](https://swayos.github.io/)
  * Not mentioned on [Useful add ons for sway](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway)
* [Ubuntu Sway Remix](https://ubuntusway.com/)
  * Videos and chat in russian
  * Installation only via fresh install from iso
  * [ubuntusway-default-settings](https://github.com/Ubuntu-Sway/ubuntusway-default-settings)
    * Useful config for copy&paste
* [Sway desktop environment](https://github.com/Madic-/Sway-DE)

## Issues

* [Still no Displaylink support](https://gitlab.freedesktop.org/wlroots/wlroots/-/issues/1823)
* [Screen won't turn on after resume from sleep](https://github.com/swaywm/sway/issues/7228)
  * [power management with systemd](https://wiki.archlinux.org/title/Power_management#Power_management_with_systemd)
  * [Reddit: Screen won't turn on after turning off](https://www.reddit.com/r/swaywm/comments/tts4t5/screen_wont_turn_on_after_turning_off/)
  * [resume from suspend = black screen](https://forum.endeavouros.com/t/bit-of-an-odd-one-resume-from-suspend-black-screen/31712)
  * [Opening the laptop lid doesn't trigger swaylock](https://github.com/swaywm/sway/issues/3198)
* QT icons not showing up in panel (i.e. nextcloud)
  * [nextloud](https://github.com/nwg-piotr/nwg-panel/issues/165)
* Boot: [External display not recognized unless re-plugged in](https://www.reddit.com/r/archlinux/comments/kphx7q/usb_typec_monitor_not_reliably_detected/)
* Screensharing: Can't select single windows, only entire screens.
  [Screensharing trobleshooting](https://github.com/emersion/xdg-desktop-portal-wlr/wiki/%22It-doesn't-work%22-Troubleshooting-Checklist)

## Installation

    sudo apt install sway

[Login managers](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway#login-managers)

Setup GDM:

    sudo cp /home/varac/.config/sway/gdm/sway.desktop /usr/share/wayland-sessions/

## Configuration


### Environment variables


From [What's the current most effective way of setting env variables in Sway?](https://www.reddit.com/r/swaywm/comments/trjtdn/whats_the_current_most_effective_way_of_setting/):

#### greetd + tuigreet

* Default in Manjaro-sway

> If you're using greetd, use wrapper scripts to set the environment variables
  in `/etc/greetd/`

From [tuigreet: desktop-environments](https://github.com/apognu/tuigreet#desktop-environments):

> greetd only accepts environment-less commands to be used to start a session.
> Therefore, if your desktop environment requires either arguments or environment
> variables, you will need to create a wrapper script and refer to it in an
> appropriate desktop file.

#### Without greetd

> If you're not using greetd or any other display manager,
> set the environment variables in `~/.bash_profile`, `~/.profile`,
> or `~/.config/fish/` depending on your login shell.

#### With sway as systemd user service

> If you're running sway as a systemd service or if you're running systemd user
> services that are built for Wayland, you'll have to set specific environment
> variables in `~/.config/environment.d/`.

* [Arch wiki: sytemd/user/Environment variables](https://wiki.archlinux.org/title/Systemd/User#Environment_variables)

## Etc

How to tell if I'm on wayland ?

    echo $XDG_SESSION_TYPE

## swaymsg

* [swaymsg man page](https://github.com/swaywm/sway/blob/48d6eda3cb0f79d2190756af44f01d028696bf0e/swaymsg/swaymsg.1.scd)

Get all outputs (swaymsg pretty-prints to stdout but when piped, it prints json):

    swaymsg -t get_outputs

Get currently focused output:

    swaymsg -t get_outputs| jq -r '.[] | select(.focused ).name

Set output resolution:

    sway output HDMI-A-1 mode 1024x768

Show all active windows/clients:

    swaymsg -t get_tree | jq '.nodes[]'

Show all windows/clients on output `LVDS-1`:

    swaymsg -t get_tree | jq '.nodes[] | select(.name=="LVDS-1")'

Show all windows/clients on workspace `10`:

    swaymsg -t get_tree | jq '.nodes[].nodes[] | \
      select(.name | test("10")).nodes[] | {name,id,pid}'

Show all floating windows:

    swaymsg -t get_tree | jq '.nodes[].nodes[].floating_nodes[]'

Show current config *before* includes,
see [feature request: alternative to swaymsg -t get_config that is a parsed JSON structure](https://github.com/swaywm/sway/issues/5671);

    swaymsg -t get_config

Show current config *after* includes (custom alias):

    sway_config

## Xwayland

Let non-wayland-native apps (i.e. chromium) run under wayland.

* [How to debug Wayland problems](https://fedoraproject.org/wiki/How_to_debug_Wayland_problems)

Use `xprop` or `xwininfo` in a terminal and try clicking on the window (or xeyes).
`xlsclients` shows the currently running apps which use xwayland.

Or use `swaymsg -t get_tree | grep class`

I.e., current non-native-wayland apps:

* signal-desktop

## Run application as root in Wayland

    xhost +SI:localuser:root
    gksudo -k gparted

## Screen sharing

* [Arch wiki: PipeWire WebRTC screen sharing](https://wiki.archlinux.org/title/PipeWire#WebRTC_screen_sharing)
* [DBus tools for emulating gnome-screenshot on Wayland's wlroots](https://gitlab.com/jamedjo/gnome-dbus-emulation-wlr)

## Autostart in sway

[Sway doesn't support the XDG Desktop Application Autostart Specification](https://github.com/swaywm/sway/issues/1423)
(`~/.config/autostart/`) out of the box.
[dex](https://github.com/jceb/dex) comes to the rescue, but it's
stalled (last commit 2022-02). It can be used as shown in
[this example](https://github.com/manjaro-sway/manjaro-sway/issues/268).
The preferred way for autostarting applications in sway is to `exec`
them from sway config.
The [Systemd integration](https://github.com/swaywm/sway/wiki/Systemd-integration)
can also be used to run apps as systemd service.

## Projects to try out

* [swaysome](https://gitlab.com/hyask/swaysome)
  This binary helps you configure sway to work a bit more like Awesome.
  This currently means workspaces that are name-spaced on a per-screen basis.
* [sworkstyle](https://lib.rs/crates/sworkstyle)
  Workspaces with the swayest style! This program will dynamically rename your
  workspaces to indicate which programs are running in each workspace
  (Seems similar to what nwg-panel already does)
* [persway](https://github.com/johnae/persway)
