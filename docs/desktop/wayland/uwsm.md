# Universal Wayland Session Manager

> Wraps standalone Wayland compositors into a set of Systemd units
> on the fly. This provides robust session management including
> environment, XDG autostart support, bi-directional binding with
> login session, and clean shutdown.

- [GitHub](https://github.com/Vladimir-csp/uwsm)
- [Recommended way to start Hyprland](https://wiki.hyprland.org/Useful-Utilities/Systemd-start/)
- [Applications must be started with `uwsm app …`](https://www.reddit.com/r/hyprland/comments/1hh19m8/uwsm_and_wofi_question/)
