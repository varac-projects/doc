# Dark mode

- [Arch wiki: Dark mode](https://wiki.archlinux.org/title/Dark_mode_switching)
- [Dark mode in Sway](https://major.io/p/sway-dark-mode/)
- [Gnome dark style preference](https://blogs.gnome.org/alicem/2021/10/04/dark-style-preference/)
- Freedesktop appearance color-scheme key: `org.freedesktop.appearance.color-scheme`
- System-wide available themes are stored under `/usr/share/themes`

## Tools

- [color-scheme-simulator](https://gitlab.gnome.org/alicem/color-scheme-simulator)
  - Last update 2022

### darkman

- [Gitlab](https://gitlab.com/WhyNotHugo/darkman)
- Implements the FreeDesktop dark mode standard
- Last [tag](https://gitlab.com/WhyNotHugo/darkman/-/tags) 2023-01
- [Arch package](https://archlinux.org/packages/extra/x86_64/darkman/)
  - Last updated 2023-10
- [Docs: Man page](https://darkman.whynothugo.nl/)
- [Contributed example scripts](https://gitlab.com/WhyNotHugo/darkman/-/tree/main/examples?ref_type=heads)
- Works fine for:
  - Waybar
  - Neovim
  - Kitty
  - [Firefox](https://wiki.archlinux.org/title/Dark_mode_switching)
  - Thunderbird
- To-do:
  - Chromium
- Config: `~/.config/darkman/`
- State dir: `~/.cache/darkman/`

#### Install

```sh
sudo pacman -S darkman xdg-desktop-portal-gtk gnome-themes-extra
pamac install neovim-remote
```

#### Configure

```sh
mkdir ~/.local/share/dark-mode.d ~/.local/share/light-mode.d

cd ~/.local/share/dark-mode.d
wget https://gitlab.com/WhyNotHugo/darkman/-/raw/main/examples/dark-mode.d/desktop-notification.sh
wget https://gitlab.com/WhyNotHugo/darkman/-/raw/main/examples/dark-mode.d/gtk3-theme.sh
chmod +x ~/.local/share/dark-mode.d/*.sh

cd ~/.local/share/light-mode.d
wget https://gitlab.com/WhyNotHugo/darkman/-/raw/main/examples/light-mode.d/desktop-notification.sh
wget https://gitlab.com/WhyNotHugo/darkman/-/raw/main/examples/light-mode.d/gtk3-theme.sh
wget https://gitlab.com/WhyNotHugo/darkman/-/raw/main/examples/light-mode.d/neovim-background.sh
chmod +x ~/.local/share/light-mode.d/*.sh

systemctl --user enable --now darkman.service
```

[Disable automatic transition](https://gitlab.com/WhyNotHugo/darkman/-/issues/30):

- set `usegeoclue: false` in `~/.config/darkman/config.yaml`
- `rm ~/.cache/darkman/location.json`
- `systemctl --user restart darkman.service`

#### Usage

```sh
darkman toggle
```

### yin-yang

- [GitHub](https://github.com/oskarsh/Yin-Yang)
- [Documentation in the wiki](https://github.com/oskarsh/Yin-Yang/wiki)
- [AUR package](https://aur.archlinux.org/packages/yin-yang)
  - Recently updated
- Plugin support
- No way to add simple user scripts ?
- [Support for Neovim](https://github.com/oskarsh/Yin-Yang/issues/34)
- [Firefox addon](https://addons.mozilla.org/de/firefox/addon/yin-yang-linux/)
  - Last updated 2021-11

## Application support

### Auto-switching applications

- Waybar
- Firefox
  - Addons / Themes: `System theme - auto`
  - Settings / Language and Appearance / Website appearance: `Automatic`
  - Settings / Language and Appearance / Colors
    - \[x\] Use system colors
    - Override the colors specified by the page with your selections above:
      `Only with high contrast themes`
- Thunderbird
  - Addons / Themes: `System theme - auto`

### Application that need manual intervention

- [Kitty](https://github.com/kovidgoyal/kitty/issues/1792)
- Neovim: Using [neovim-remote](https://github.com/mhinz/neovim-remote)

### Websites

#### Already implemented auto-switching

- GitHub
- [mastodon.social](https://mastodon.social/explore)

#### Still no auto-switching

- [Gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/223205)
