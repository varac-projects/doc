# Desktop entries

- [Arch wiki: Desktop entries](https://wiki.archlinux.org/title/desktop_entries)
- [Debug desktop entries](https://askubuntu.com/questions/961745/unable-to-create-custom-uri-scheme-on-ubuntu)
- Add custom desktop entries by creating a .desktop file in `.local/share/applications/`
- See also [mimetype.md](../etc/mimetype.md)
- [Desktop entry location](https://wiki.archlinux.org/title/Desktop_entries#Application_entry):
  - `/usr/share/applications/`
  - `/usr/local/share/applications/`
  - `~/.local/share/applications/`

Run desktop entry:

```sh
gtk-launch chromium.desktop
gtk-launch ~/.local/share/applications/org.jitsi.jitsi-meet.desktop
```

Validate:

```sh
desktop-file-validate <your desktop file>
```
