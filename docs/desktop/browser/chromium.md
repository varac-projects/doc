# Chromium

## Command line switches

- [List of Chromium Command Line Switches](https://peter.sh/experiments/chromium-command-line-switches/)
- Configured in `~/.config/chrome-flags.conf`

## Proxy settings

- [Configure Proxy for Chromium and Google Chrome From Command Line on Linux](https://www.linuxbabe.com/desktop-linux/configure-proxy-chromium-google-chrome-command-line)

## Add certs to cert store

- [Arch wiki: Adding CAcert certificates for self-signed certificates](https://wiki.archlinux.org/title/Chromium#Adding_CAcert_certificates_for_self-signed_certificates)
- [Support self-signed certs for SSO](https://open.greenhost.net/stackspin/single-sign-on/-/issues/62#note_25209)

Cert store locations:

For non-snap installations:

```sh
export CERTSTORE=~/.pki
```

For snap installations:

```sh
export CERTSTORE=~/snap/chromium/current/.pki
```

Add LE staging CA cert:

```sh
certutil -d sql:$CERTSTORE -A -t TC -n "LE staging" -i ~/oas/openappstack/test/pytest/le-staging-bundle.pem
```

## Wayland

- [Arch wiki: Native Wayland support](https://wiki.archlinux.org/title/chromium#Native_Wayland_support)
- Enable `WebRTC PipeWire support` and set `Preferred Ozone platform` to `Wayland`
  in `chrome://flags`
