# External editors

<https://en.wikipedia.org/wiki/Help:Text_editor_support>

Requirements:

- recent Firefox (>v57)
- Windows support

## withExEditor

- [withExEditor](https://github.com/asamuzaK/withExEditor): Active development
- [Generic Host](https://github.com/asamuzaK/withExEditorHost) which is able
  to launch any editor

[Setup](https://github.com/asamuzaK/withExEditorHost#host-setup):

- `mkdir -p ~/.config/withexeditorhost/config/firefox`
- `cd ~/.config/withexeditorhost/conf/firefox`
- Download und unzip [latest release](https://github.com/asamuzaK/withExEditorHost/releases)
- `chmod a+x index`
- `./index setup`, choose browser and path to editor
- Installer creates `editorconfig.json` and `withexeditorhost.sh`
- Due to some issue the `index.js` is missing, use
  `wget https://raw.githubusercontent.com/asamuzaK/withExEditorHost/master/index.js` to install

After setting up the host install the [browser addon](https://github.com/asamuzaK/withExEditor#download)

- [Keyboard shortcut can get configured in the firefox add-on
  menu](https://github.com/asamuzaK/withExEditor/issues/114#issuecomment-481880686)

## GhostText

- [GhostText](https://github.com/GhostText/GhostText)
- Firefox + Chrome, Last commit 2019-09. Configurable Shortcut for opening the editor.
- Dedicated Hosts for several editors

## Other

Out of scope:

- [textern](https://addons.mozilla.org/en-US/firefox/addon/textern):
  (Only works on Linux)

Deprecated, no longer maintained:

- [itsalltext](https://github.com/docwhat/itsalltext): `With the Firefox 57,
It's All Text! has stopped working`
- [firefox-viewsourcewith](https://github.com/dafizilla/firefox-viewsourcewith):
  Last commit 2015
- [TextArea Sputnik](http://lib.custis.ru/TextArea_Sputnik/en): No github link,
  unclear what version the download link points to, looks stale
