# Firefox

- [Arch wiki: Firefox](https://wiki.archlinux.org/title/firefox)
- Config:

## Addons

### Firevim (broken)

- [GitHub](https://github.com/glacambre/firenvim/)
- Currently [broken in thunderbird (again...) #1263](https://github.com/glacambre/firenvim/issues/1263)
- [Troubleshooting](https://github.com/glacambre/firenvim/blob/master/TROUBLESHOOTING.md)
- Try with <http://txti.es/>

### Addon / extension development

- [Add-on signing in Firefox](https://support.mozilla.org/en-US/kb/add-on-signing-in-firefox?as=u&utm_source=inproduct#)

> What are my options if I want to use an unsigned add-on? (advanced users)
> Firefox Extended Support Release (ESR), Firefox Developer Edition and Nightly
> versions of Firefox will allow you to override the setting to enforce the
> extension signing requirement, by changing the preference
> xpinstall.signatures.required to false in the Firefox Configuration Editor
> (about:config page). …
> There are also special unbranded versions of Firefox that allow this override.

## Release channels

<https://wiki.mozilla.org/Release_Management/Release_Process>
<https://wiki.mozilla.org/Release_Management/Calendar>

## build

<https://github.com/glandium/git-cinnabar/wiki/Mozilla:-A-git-workflow-for-Gecko-development>

Beware: Initial cloning takes a couple of hours !

```sh
cd ~/projects/browsers/firefox
git clone hg::https://hg.mozilla.org/mozilla-unified
cd mozilla-unified
git config fetch.prune true
```

## Remove site from HSTS cache

- History -> Manage history
- Search for site
  `Forget this site`

## Wayland

- Add `MOZ_ENABLE_WAYLAND=1` to `.config/environment.d/envvars.conf`

## Cookie banner auto-reject

- [Block cookie banners on Firefox](https://support.mozilla.org/en-US/kb/cookie-banner-reduction)
- [mozilla/cookie-banner-rules-list](https://github.com/mozilla/cookie-banner-rules-list)

> Since the feature is still in development, please use the latest version of
> Firefox Nightly for testing. You need to set the following prefs to enable the
> feature:

```sh
cookiebanners.service.mode = 1 (reject all) or 2 (reject all or fall back to accept all).
cookiebanners.bannerClicking.enabled = true - Enables the clicking feature.
cookiebanners.cookieInjector.enabled = true - Enables the cookie injection feature.
```
