# Desktop notifications

- [Arch wiki: Desktop Notification](https://wiki.archlinux.org/title/Desktop_notifications)
- [Ubuntu wiki: NotificationDevelopmentGuidelines](https://wiki.ubuntu.com/NotificationDevelopmentGuidelines)

## Server

- [Arch wiki: Notification servers](https://wiki.archlinux.org/title/Desktop_notifications#Notification_servers)
  [Sway compatible notification servers](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway#notification)

- [Sway Notification Center](https://github.com/ErikReider/SwayNotificationCenter)
- [Mako](https://github.com/emersion/mako)
  - Default server in [Manjaro sway edition](https://github.com/manjaro-sway/manjaro-sway)
  - Doesn't show icons in default Manjaro sway setup
  - Cannot [close notifications on click](https://github.com/emersion/mako/commit/f5690773c6cf3fc05e82de35bc0be7241eb6bd47)
    when notification contains a URL (opens in browser)
- [Dunst](https://github.com/dunst-project/dunst)
- [fnott](https://codeberg.org/dnkl/fnott)
- Stale: Salut, Wayherb

## Client

- [Icon Naming Specification](https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html)
- Icon locations: `/usr/share/icons/`

Ussage:

```sh
notify-send 'Hello world!' 'This is an example notification.' --icon=dialog-information
```
