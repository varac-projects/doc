# Openstack cli

https://opendev.org/openstack/python-openstackclient
https://docs.openstack.org/python-openstackclient/latest/

    apt install python3-openstackclient

## Completion

    wget https://gist.githubusercontent.com/philipsd6/aea968e80342973fd8d3aeeda343dae5/raw/aadf29fd9e499070860dd8fbf05dc6a100562511/_openstack -O ~/.zsh/completion/_openstack

## Configuration

Done in:

    ~/.config/openstack/clouds.yaml
    ~/.config/openstack/clouds-public.yaml

## Usage


    openstack --os-cloud=otc_admin project list

### Remove all resources from project

https://opendev.org/x/ospurge

Leaves some resources behind / doesn't work at all ? :

    openstack project purge --keep-project --project eu-de_terratest

## OpenTelecomCloud extensions

https://python-otcextensions.readthedocs.io/en/latest/

    pip3 install otcextensions python-openstackclient --user

### Usage

    openstack cce cluster list
