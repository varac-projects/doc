# Cloud images

- [Debian cloud images wiki page](https://wiki.debian.org/ThomasChung/CloudImage)
- [Debian clood images](https://cloud.debian.org/images/cloud/)

## KVM Images

### mount LVM Images

```sh
kpartx -av /dev/vg_crypt/aroma_server
fdisk -l /dev/mapper/vg_crypt-aroma_server
mount /dev/mapper/vg_crypt-aroma_server1 /mnt
...
umount /mnt
kpartx -dv /dev/vg_crypt/aroma_server
```

### mount raw images

```sh
sudo kpartx -av leap_baseimage.img
sudo mount /dev/mapper/loop1p1 /mnt/
…
sudo umount /mnt
kpartx -dv leap_baseimage.img
```

?

```sh
sudo kpartx -av leap_baseimage.img
sudo partx -a /dev/loop1
sudo fdisk -l /dev/loop1
sudo mount /dev/mapper/loop1p1 /mnt/
…
sudo umount /mnt
sudo partx -d /dev/loop1
kpartx -dv leap_baseimage.img
sudo losetup -a
sudo losetup -d /dev/loop0
```

### shrink raw image partition

```sh
sudo kpartx -av leap_baseimage.img
sudo partx -a /dev/loop0
sudo fsck.ext3 -f /dev/mapper/loop0p1
# sudo tune2fs -O ^has_journal /dev/mapper/loop1p1   # really neccessary ?
sudo resize2fs /dev/mapper/loop0p1 1G
# > Die Grösse des Dateisystems auf /dev/mapper/loop0p1 wird auf 262144 (4k) Blöcke geändert.
# 262144 + 2 Blöcke Sichereit-> 262146 Blöcke
# Block: 1024B, Sector: 512B
# 262146 Blöcke * 2 = 131073 Sectors
#
sudo fdisk /dev/loop1
  d
  n
  > p
  >> begin: 0
  >> end: +1080033K
  w
  #
sudo partx -d /dev/loop0
```

### Clone Partition

```sh
kpartx -av /dev/vg01/leap-baseimage-wheezy2
dd if=/dev/mapper/vg01-leap--baseimage--wheezy2p1 of=vg01-leap--baseimage--wheezy2p2
kpartx -dv /dev/vg01/leap-baseimage-wheezy2
```

## qcow2 images

### create qcow2 images

- <https://kashyapc.wordpress.com/2011/09/24/creating-a-qcow2-virtual-machine/>

preallocation=metadata : pre-allocates the whole size
without this parameter, the image file is as big as the current size

```sh
/usr/bin/qemu-img create -f qcow2 -o preallocation=metadata /export/vmimgs/glacier.qcow2 8G
```

### mount qcow2 images

```sh
sudo modprobe nbd max_part=63
sudo qemu-nbd -c /dev/nbd0 leap-baseimage-wheezy.qcow2
sudo fdisk -l /dev/nbd0
mount /dev/nbd0p1 /mnt/image
...
umount /mnt/image
sudo qemu-nbd -d /dev/nbd0
... oder:
sudo pkill -9 qemu-nbd
sudo kpartx -d /dev/loop0
sudo losetup -d /dev/loop0
```

### mount qcow image with lvm inside

```sh
sudo modprobe nbd max_part=63
sudo qemu-nbd -c /dev/nbd0 leap-baseimage-wheezy.qcow2
sudo fdisk -l /dev/nbd0
sudo lvscan
sudo vgchange -ay leap-debian
sudo lvs
sudo mount /dev/leap-debian/root mnt

sudo umount mnt
sudo vgchange -an leap-debian
sudo qemu-nbd -d /dev/nbd0
```

### Convert images

```sh
qemu-img convert -O raw leap-baseimage-wheezy.qcow2 leap-baseimage-wheezy.img
qemu-img convert -f raw -O qcow2 guest-disk.img guest-disk-copy.qcow2
```

### Remove VM and Image

```sh
virsh destroy $VM
virsh undefine --remove-all-storage $VM

# if above --remove-all-storage doesn't work:
virsh vol-delete $VM.img --pool default
```

### Grow qcow2 image

- [Resize KVM Images](http://carl.schelin.org/?p=1799)

#### Resize qcow2 image file

Resize with relative additional size:

```sh
virsh shutdown ubuntu-server
qemu-img info ubuntu-server.qcow2
file ubuntu-server.qcow2
qemu-img resize ubuntu-server.qcow2 +5GB
virsh start ubuntu-server
```

Resize with absolute size:

```sh
qemu-img resize ubuntu-server.qcow2 50GB
```

#### Resize OS partition

In case of a Debian bookworm libvirt installation image, the OS partition gets
automatically resized during boot from an resized qcow2 image.
