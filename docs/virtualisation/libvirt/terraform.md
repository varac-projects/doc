# Libvirt Terraform provider

- [Run Debian 11 (Bullseye) on KVM using Qcow2 Cloud Image](https://techviewleo.com/run-debian-11-bullseye-on-kvm-using-qcow2-cloud-image/)
- Outdated, but still interesting: [camptocamp/terraform-libvirt-k3s](https://github.com/camptocamp/terraform-libvirt-k3s)
  Although [k3os is dead](https://github.com/rancher/k3os/issues/846)
- [dmacvicar/libvirt provider](https://registry.terraform.io/providers/dmacvicar/libvirt/latest)

## Server setup

- Follow [debian wiki:installation](https://wiki.debian.org/KVM#Installation)
- Unfortunatly, [LVM storage pools can't be created yet with
  dmacvicar/libvirt](https://github.com/dmacvicar/terraform-provider-libvirt/pull/814)

Create libvirt directpry storage pool (LVM doesn't work, see below):

```sh
virsh pool-create-as default  --type dir --target /var/lib/libvirt/images
virsh pool-autostart default
```

Issues with LVM storage pool:

- Using a `backingStore` as baseimage and creating a bigger root LVM volume
  doesn't work, therefore sticking with directory storage pools for now.
  Since [LVM storage pool support](https://github.com/dmacvicar/terraform-provider-libvirt/pull/814)
  is still not merged, I refrained from opening an issue for this.
  Related issues:
  - [Support Volume Resizing from Base Volume](https://github.com/dmacvicar/terraform-provider-libvirt/issues/369)
  - [Unable to set disk size when deploying from cloud image](https://github.com/dmacvicar/terraform-provider-libvirt/issues/357)

## Terraform setup

Install on laptop (for cloud-init disk):

```sh
sudo pacman -S cdrtools
```

### Cloud-init

- [cloud-init modules](https://cloudinit.readthedocs.io/en/latest/reference/examples.html)

### Issues

- [Images are created with root user and I get access denied #978](https://github.com/dmacvicar/terraform-provider-libvirt/issues/978#issuecomment-1276244924)
  Solution: Disable apparmor in `/etc/ibvirt/qemu.conf`
- Domain cannot get installed because of apparmor: See [security/apparmor.md](../../security/apparmor.md)
