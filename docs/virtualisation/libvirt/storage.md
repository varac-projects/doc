# Libvirt storage

## Create an lvm storage pool

- [Storage Management](https://libvirt.org/storage.html#StorageBackendLogical)

Pool XML:

```xml
<pool type='logical'>
  <name>ubuntu-vg</name>
</pool>
```

Create:

```sh
virsh pool-define /tmp/lvm.xml
virsh pool-start lvm
virsh pool-autostart lvm
```

## Grow guest disk

[How To extend or increase KVM VM disk size](https://computingforgeeks.com/how-to-extend-increase-kvm-virtual-machine-disk-size/)

```sh
virsh shutdown dapple-agent2
sudo virsh domblklist dapple-agent2
qemu-img info /var/local/lib/libvirt/pools/directory-pool/images/root-dapple-agent2
qemu-img resize /var/local/lib/libvirt/pools/directory-pool/images/root-dapple-agent2 +30G
virsh start dapple-agent2
```
