# Libvirt

- [Docs](https://libvirt.org/docs.html)

- Resource monitoring tool: `virt-top`

- dom-configs liegen in /etc/libvirt/qemu/

- logs etc. in /var/lib/libvirt/qemu/

## libvirt/kvm install

- [debian wiki](https://wiki.debian.org/KVM#Installation)
- /etc/qemu
- /etc/libvirt

Steps:

```sh
sudo apt install --no-install-recommends qemu-system libvirt-clients libvirt-daemon-system
```

### Configure libvirt

### Issues

#### Autostart after boot fails

[Ubuntu bug: Cannot start VMs without routable IPv4 address](https://bugs.launchpad.net/ubuntu/+source/qemu/+bug/1492621)

If autostart during boot doesn't work and libvirtd logs these errors:

```log
libvirtd[1157]: internal error: Failed to autostart VM 'dapple-agent2':
  internal error: process exited while connecting to monitor:
  2023-07-13T15:22:04.228127Z qemu-system-x86_64: warning:
  Spice: ../server/reds.cpp:2551:reds_init_socket: getaddrinfo(127.0.0.1,5900):#
  Address family for hostname not supported
```

Add a systemd drop-in `/etc/systemd/system/libvirtd.service.d/autostart.conf` with this config:

```ini
[Unit]
Require=network-online.target
[Service]
# Still wait a bit to prevent libvirt autostarting VMs too early
# https://bugs.launchpad.net/ubuntu/+source/qemu/+bug/1492621
ExecStartPre=/bin/sleep 10
```

#### Run virsh and access libvirt as a regular user

```sh
adduser <youruser>  libvirt
reboot
```

### Create storagepool

Create LVM pool:

```sh
virsh pool-define-as libvirt logical --source-name dapple-vg --target /dev/mapper/system_crypt
virsh pool-autostart libvirt
```

## Boot Live-CD

```sh
kvm -cdrom /home/tostado/Software/ubuntu-12.04-beta2-desktop-amd64.iso --network=bridge:virbr0
```

## Libvirt Console Access

- [virsh: VM console does not show any output](http://serverfault.com/a/705062)

in guest VM run the following

```sh
systemctl enable serial-getty@ttyS0.service
systemctl start serial-getty@ttyS0.service
```

in guest VM in /etc/default/grub set

```sh
GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 console=ttyS0"
GRUB_TERMINAL="serial console"
```

in guest VM run the following

```sh
guest# update-grub
```

## Edit files inside a shutdown VM from the host

```sh
virt-edit baseimage.dfi.local /etc/default/grub
```

### rename machine

```sh
virsh dumpxml name_of_vm > /tmp/name_of_vm.xml
```

Undefine the old vm to prevent an error because of an duplicate UUID.

```sh
virsh undefine name-of-vm
```

Edit the xml file then import it.

```sh
virsh define /tmp/name_of_vm.xml
```

## virsh

### remote uris

```sh
virsh -c qemu+ssh://dapple/system list --all
```

### List / dominfo

```sh
virsh list --all
virsh dominfo debian-vpn
```

Show all domains memory:

```sh
virsh domstats --balloon
virsh domstats --balloon |grep -E '(Domain|balloon.max)'
```

### Modify VMs

#### Add memory

```sh
export DOM=dapple-media-controlplane

virsh dominfo $DOM | grep memory
virsh shutdown $DOM
virsh setmaxmem $DOM 12G --config
virsh setmem $DOM 12G --config
virsh start $DOM
```

#### Add a vCPU

See [Modifying the number of virtual CPUs](https://www.ibm.com/docs/en/linux-on-systems?topic=cpus-modifying-number-virtual)

```sh
virsh vcpucount $DOM
virsh setvcpus $DOM 3 --maximum --config
virsh setvcpus $DOM 3 --config
virsh reboot $DOM
```

#### Modify description

```sh
virsh desc --config --new-desc "K3s agent: Gitlab agent and runner, tym-flow CI and prod deployment" $DOM
virsh desc --current --new-desc "K3s agent: Gitlab agent and runner, tym-flow CI and prod deployment" $DOM
```

#### Modify auto-start behaviour

Enable autostart:

```sh
$ virsh autostart $DOM
Domain '$DOM' marked as autostarted
```

Disable autostart:

```sh
$ virsh autostart --disable jump
Domain 'jump' unmarked as autostarted
```

List domains marked as autostart:

```sh
virsh list --autostart --all
```

#### Add a data disk to existing domain

```sh
virsh vol-create-as <POOL>  <NAME> 12G --format qcow2 --allocation 30G
```

qcow2:

```sh
virsh vol-create-as virtimages try.pixelated-project.org.data.img 14G --format qcow2 --allocation 14G
virsh attach-disk try.pixelated-project.org \
  --source /var/lib/libvirt/images/try.pixelated-project.org.data.img \
  --target vdb --persistent
```

lvm:

```sh
virsh vol-create-as lvm try.pixelated-project.org.data.img 20G
virsh attach-disk try.pixelated-project.org --source /dev/n097h049/try.pixelated-project.org.data.img --target vdb --persistent

virsh detach-disk try.pixelated-project.org --target vdb
virsh vol-delete --pool virtimages try.pixelated-project.org.data.img
```

### Find IP of VM

Oneliner:

```sh
 for mac in `virsh domiflist platform-test.dfi.local ¸
   | grep -o -E "([0-9a-f]{2}:){5}([0-9a-f]{2})"` ; do
     arp -e |grep $mac  |grep -o -P "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}" ; done
```

as function:

```sh
 virt_ip () {
   for mac in `virsh domiflist $1 |grep -o -E "([0-9a-f]{2}:){5}([0-9a-f]{2})"`
   do
     arp -e |grep $mac  |grep -o -P "^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
   done
 }
```

## Virt-Manager

- [Website](https://virt-manager.org/)

### German keyboard config

- [Linux KVM : how to specify keybord layout of a VM](http://lost-and-found-narihiro.blogspot.de/2012/02/linux-kvm-how-to-specify-keybord-layout.html)

Change the libvirt xml config for that host to

```xml
<graphics type='vnc' port='-1' autoport='yes' keymap='de'/>
```

On the VM, change the [keyboard config](https://wiki.debian.org/Keyboard#Keyboard_configuration)

```sh
dpkg-reconfigure console-data
dpkg-reconfigure keyboard-configuration
service keyboard-setup restart
```

and reboot it.

## Virt-viewer

[Website](https://gitlab.com/virt-viewer/virt-viewer)

```sh
virt-viewer -c qemu:///system  leap-baseimage
```

## Snapshots

- [Virtual Machine Lifecycle](http://wiki.libvirt.org/page/VM_lifecycle#Taking_a_Snapshot_of_a_guest_domain)

Take snapshot:

```sh
virsh snapshot-create-as develop fresh1
virsh snapshot-list develop
virsh snapshot-revert develop fresh1
virsh snapshot-delete develop fresh1
```

### Cloning of VMs

```sh
virt-clone -o artful -n artful_fresh_install --auto-clone
```

## Static DHCP leases for VMs

[KVM/libvirt: How to configure static guest IP addresses on the virtualisation host](https://serverfault.com/questions/627238/kvm-libvirt-how-to-configure-static-guest-ip-addresses-on-the-virtualisation-ho)

libvirt dnsmasq config file: /var/lib/libvirt/dnsmasq/default.conf

First, find out the MAC addresses of the VMs you want to assign static IP addresses:

```sh
virsh  dumpxml  $VM_NAME | grep 'mac address'
```

Remove possible old lease from lease spool file

```sh
/var/lib/libvirt/dnsmasq/default.leases
```

Add host entries for your VMs

```sh
virsh net-update default add ip-dhcp-host --xml "<host mac='52:54:00:73:e9:6c' \
  name='staging.pixelated-project.org' ip='192.168.136.24'/>" --live --config
```

Reload dhnsmasq to pick up changes

```sh
kill -HUP $(cat /var/run/libvirt/network/default.pid)
```

New static IP should show up here:

```sh
cat /var/lib/libvirt/dnsmasq/default.hostsfile
```

Then, reboot your VM (or restart its DHCP client, e.g. ipdown eth0; ifup eth0)

Issues:

- What's the IPs inside /var/lib/libvirt/dnsmasq/virbr0.status ?
- why does virsh net-dhcp-leases default show different values than virsh net-dumpxml default ?
