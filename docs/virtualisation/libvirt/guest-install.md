# Libvirt guest installation

## Installation with virt-install

### qcow2 / wheezy

    qemu-img create -f qcow2 -o preallocation=off /home/varac/kvm/leap_baseimage3.qcow2 8G
    virt-install --name=debian-wheezy --ram 512 --os-type linux --os-variant debianwheezy --virt-type kvm --disk path=/home/varac/kvm/debian-wheezy.qcow2,bus=virtio,format=qcow2 --location=http://ftp.us.debian.org/debian/dists/wheezy/main/installer-amd64/ --nographics -x"DEBIAN_FRONTEND=text console=ttyS0"


    virt-install --connect=qemu:///system \
      --network=bridge:br0 \
      --name=debian-wheezy \
      --disk path=/home/varac/debian-wheezy.qcow2,format=qcow2 \
      --ram 512 \
      --vcpus=1 \
      --check-cpu \
      --hvm \
      --location=http://download.fedora.redhat.com/pub/fedora/linux/releases/15/Fedora/x86_64/os/ \
      --nographics

### with virt-install-custom

    lvcreate –n aroma_server –L 12G vg_crypt
    virt-install-custom --name aroma-server --ram 1024 --disk path=/dev/vg_crypt/aroma_server --cdrom ~/Software/debian-6.0.4-amd64-CD-1.iso

### Installation with 2 Disks in LVM

    virt-install --name opensuse --ram 1024 --vcpus=1 --disk path=/dev/vg_crypt/susesda2 --disk path=/dev/vg_crypt/susesdb2  \
      --graphics vnc,keymap=de --cdrom ~/Software/Suse/openSUSE-11.1-DVD-x86_64-iso/openSUSE-11.1-DVD-x86_64.iso --os-type linux \
      --network=bridge:virbr0,model=virtio --mac `openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`

### Debian-Installation with Imagefile

    dd if=/dev/zero of=workshop.img bs=1024 count=8388608 # 8gb
    virt-install --name debian --ram 1024 --vcpus=1 --disk path=/home/tostado/kvm/workshop.img  \
    --graphics vnc,keymap=de --cdrom ~/Software/debian-6.0.3-i386-CD-1.iso --os-type linux \
    --network=bridge:virbr0,model=virtio --mac `openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`

### Windows 7

    virsh vol-create-as default Win7pro_64bit 25G

    virt-install --name Win7pro_64bit --ram 2048 --vcpus=2 \
      --os-type windows --os-variant win7 \
      --disk vol=xubuntu-vg/Win7pro_64bit,device=disk,bus=virtio \
      --cdrom /home/varac/Software/Windows/windows7-professional-amd64-original-verified.iso \
      --disk path=/home/varac/Software/Windows/virtio-win-0.1-81.iso,device=cdrom,perms=ro \
      --boot cdrom,menu=on \
      --network network=default,model=virtio \
      --hvm --vnc --noautoconsole

### install debian, automatically create image in storage pool

#### graphical

    virt-install -d --hvm --vnc --name=vm01 --ram 512 --disk pool=default,size=4,bus=virtio,cache=none --network network=default,model=virtio --location=http://ftp.debian.org/debian/dists/wheezy/main/installer-amd64/ --os-type=linux --os-variant=debianwheezy

#### text console

    virt-install --name=cougar --ram 2048 --os-type linux --os-variant debiansqueeze --virt-type kvm --disk path=/dev/vg01/cougar,bus=virtio,size=30 --location=http://ftp.us.debian.org/debian/dists/squeeze/main/installer-amd64/ --nographics -x"DEBIAN_FRONTEND=text console=ttyS0" -w bridge:br0

## Issues

### Bookworm upgrade: Cannot start libvirt domain

See [apparmor.md#Issues](../../security/apparmor.md)
