# Libvirt networking

## default net

```sh
 virsh net-start default
 virsh net-autostart default
```

## which vnetX belongs to which guest

```sh
$ virsh dumpxml starfish|grep vnet
      <target dev='vnet18'/>
      <target dev='vnet19'/>
```

```sh
VNET=vnet13
for vm in $(virsh list | grep running | awk '{print $2}'); do
  virsh dumpxml $vm|grep -q "$VNET" && echo $vm
done
```
