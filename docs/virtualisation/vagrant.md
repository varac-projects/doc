# Installation

    apt install vagrant nfs-server

# Storagepool

    `/var/lib/libvirt/images` -> `/home/varac/var/lib/libvirt/images`

Box images are stored in `~/.vagrant.d/boxes`.
On `vagrant up`, the box image will be copied to the `default` storage pool,
as well as the dedicated running box image.

    virsh vol-list --pool default

# Vagrant init

# Re-install plugins

First try:

    vagrant plugin repair

If this doesn't help:

    vagrant plugin expunge

If this _still_ doesn't help:

    mv ~/.vagrant.d ~/.vagrant.d.2.X.Y.bak


# Global config

see ~/.vagrant.d/Vagrantfile

# Port Forwarding

    config.vm.network :forwarded_port, guest: 80, host: 4567

# use local caching via squid-deb-proxy

    VAGRANT_CACHING=true vagrant up

# increase logging

global config file: ~/.vagrant.d/Vagrantfile

    VAGRANT_LOG=INFO vagrant up

## add libvirt box from vbox image

https://github.com/sciurus/vagrant-mutate

    vagrant plugin install vagrant-mutate

First, add

    vagrant box add ubuntu/artful64

Now, mutate it

    vagrant mutate ubuntu/artful64 libvirt

Last, remove the virtualbox one

    vagrant box remove --provider=virtualbox ubuntu/artful64

Issues:

* Doesn't work with ubunut artful (doesn't get IP, virt-mananger shows
  hanging boot process). See https://github.com/sciurus/vagrant-mutate/issues/102

## Plugins

### vagrant-libvirt

https://leap.se/en/docs/platform/tutorials/vagrant#support-for-libvirt

    sudo apt install vagrant-libvirt


#### Issues

##### Box can't get a dhcp lease

    sudo systemctl restart libvirtd

### vagrant-lxc

https://github.com/fgrehm/vagrant-lxc

    vagrant plugin install vagrant-lxc

Quick start:

    vagrant init fgrehm/precise64-lxc
    vagrant up --provider=lxc

or:

    cd /home/varac/vagrant/lxc
    vagrant up --provider=lxc

### vagrant sshfs

https://github.com/dustymabe/vagrant-sshfs

    apt install vagrant-sshfs

### Sandbox

https://github.com/ryuzee/sahara.git

### vagrant-proxyconf

https://github.com/tmatilai/vagrant-proxyconf

    VAGRANT_HTTP_PROXY="http://10.92.21.140:8000" vagrant up
    VAGRANT_HTTP_PROXY="http://10.92.21.140:8000" vagrant provision

## Vagrant on Windows

- Babun
- virtualbox
- vagrant
- rsync

## Shared folders

* `9p` not working:

    `Call to virDomainCreateWithFlags failed: internal error: qemu unexpectedly closed the monitor: 2019-11-26T12:28:32.672982Z qemu-system-x86_64: -device virtio-9p-pci,id=fs0,fsdev=fsdev-fs0,mount_tag=8e3f2500ab92c49d0e811084d285c40,bus=pci.0,addr=0x5: cannot initialize fsdev 'fsdev-fs0': failed to open '/home/varac/projects/fux/netz-ag/fux-ansible': Permission denied`

* `nfs` not working (see Troubleshooting below)


* `sshfs` not working:

    ```
    Mounting SSHFS shared folder via slave SSHFS mount failed. Please
    look at the below STDERR output from the processes that were run.

    SSH command:

    Warning: Permanently added '192.168.122.114' (ECDSA) to the list of known hosts.
    Received disconnect from 192.168.122.114 port 22:2: Too many authentication failures
    Disconnected from 192.168.122.114 port 22
    ```


## Troubleshooting

### NFS mounts

https://www.vagrantup.com/docs/synced-folders/nfs.html

    mount -o vers=4 192.168.122.1:/home/varac/projects/ansible /vagrant

    mount.nfs: access denied by server while mounting 192.168.122.1:/home/varac/projects/ansible
