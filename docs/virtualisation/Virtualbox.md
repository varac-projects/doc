# DOku

* http://wiki.debian.org/VirtualBox

# Powerdown V;

    vboxmanage controlvm "test_1360848573" poweroff

# Powerdown all VMs

    vboxmanage list runningvms \| cut -d" " -f 1\| xargs -n 1 vboxmanage
    controlvm '{}' poweroff

# Delete all VMs

    pkill VBoxHeadless\
    vboxmanage list vms \| cut -d" " -f 1\| xargs -n 1 vboxmanage
    unregistervm ---delete

# Enable VNC

    VBoxHeadless ---vnc ---vncport 6666 ---vncpass geheim ---startvm
    vagrant-test_1367696854

# Enable RDP

## Install VirtualBox Extensions Pack

> To get RDP working you need to install the VirtualBox ExtensionsPack
  Pack from the vendors website VirtualBox Downloads.

    wget http://download.virtualbox.org/virtualbox/4.2.0/Oracle_VM_VirtualBox_Extension_Pack-4.2.0.vbox-extpack
    VBoxManage extpack install
    Oracle_VM_VirtualBox_Extension_Pack-4.2.0.vbox-extpack

    VBoxManage modifyvm vagrant-test_1367696854 ---vrde on

# Start VM

    VBoxManage startvm vagrant-test_1367696854 ---type headless

# Setup headless LiveCD

https://code.google.com/p/dlt/wiki/RemoteDLTVM

    VBoxManage createvm ---name "dsl" ---register\
    VBoxManage storagectl dsl ---name "IDE Controller" ---add ide\
    VBoxManage storageattach dsl ---storagectl "IDE Controller" ---port 0
    ---device 0 ---type dvddrive ---medium
    /root/virtualbox-test/kali-linux-1.0.1-amd64-mini.iso\
    VBoxManage modifyvm "dsl" ---memory 512 ---acpi on ---boot1 dvd ---nic1
    nat\
    VBoxHeadless ---vnc ---vncport 6666 ---vncpass geheim ---startvm dsl
