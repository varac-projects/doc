QEMU
====

.

    qemu-img create ubuntu-feisty-rc1.img 10GB qemu -hda ubuntu-feisty-rc1.img -cdrom install-isos/ubuntu-7.04-beta-desktop-i386.iso -boot d
    qemu -hda winxptiny.img

iso boot
--------

    qemu -cdrom ubuntu-7.04-beta-desktop-i386.iso -boot d

Installation von iso auf usbdisk
--------------------------------

    qemu -hda /dev/sdb -boot d -cdrom lubuntu-11.10-alternate-i386.iso

USB Stick Boot
--------------

    qemu -usb /dev/sdb

Boot raspberry pi image
-----------------------
