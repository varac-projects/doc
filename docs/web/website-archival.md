# Website archival / crawling

Good camparism article: [11 best open-source web crawlers and scrapers in 2024](https://blog.apify.com/top-11-open-source-web-crawlers-and-one-powerful-web-scraper/)

## Tools

Sorted by potential cadidates for scraping with authentication:

- [Katana](https://github.com/projectdiscovery/katana)
  - Golang, > 11k GH ⭐
  - ✅ Can crawl sites that require authentication, not only
    basic auth
- [MechanicalSoup](https://github.com/MechanicalSoup/MechanicalSoup)
  - Python
  - ✅ Can crawl sites that require authentication, not only
    basic auth, through submitting forms
- [heritrix3](https://github.com/internetarchive/heritrix3)
  - Java
  - ✅ Can crawl sites that require authentication, not only
    basic auth
    - [Authentication and Cookies](https://github.com/internetarchive/heritrix3/blob/master/docs/configuring-jobs.rst#authentication-and-cookies)
- [Apache Nutch](https://nutch.apache.org/)
  - Java
  - ✅ Can crawl sites that require authentication, not only
    basic auth
    - [httpclient-auth.xml.template](https://github.com/apache/nutch/blob/master/conf/httpclient-auth.xml.template)
  - ❌ "Overly complicated for simple or small-scale crawling tasks,
    whereas lighter, more straightforward tools could be more effective"
- [gospider](https://github.com/jaeles-project/gospider)
  - ❌ Doesn't handle authentication
- [spidy](https://github.com/rivermont/spidy)
  - ❌ Doesn't handle authentication
- [scrapy](https://scrapy.org/)
  - ❌ Can only handle [basic/http auth](https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#module-scrapy.downloadermiddlewares.httpauth)
- [crawlee](https://crawlee.dev/)
  - 🤷 Authentication: [Scraping auth data using JSDOM](https://crawlee.dev/blog/scrape-using-jsdom)
- [node-crawler](https://github.com/bda-research/node-crawler)
  - ❌ [Doesn't handle authentication](https://github.com/bda-research/node-crawler/issues/281)
- [Selenium](https://www.selenium.dev/)
  - ❌ Heavy, runs a full headless browser
  - Doesn't specialize in crawling
  - ✅ Can crawl sites that require authentication, not only
    basic auth
- [Webmagic](https://github.com/code4craft/webmagic)
  - Java
  - 🤷 Authentication ?
- [Nokigiri](https://nokogiri.org/)
  - Ruby
  - 🤷 Authentication ?

### ArchiveBox

- [ArchiveBox](https://github.com/ArchiveBox/ArchiveBox)
  Open-source self-hosted web archiving

### Outdated

- [crawler4j](https://github.com/yasserg/crawler4j)
  - Last commit 2020
