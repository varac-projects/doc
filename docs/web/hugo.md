# Hugo

<https://gohugo.io/>
<https://themes.gohugo.io/>

<https://gohugo.io/getting-started/quick-start/>

## Create new site

```sh
hugo new site openappstech-website
git submodule add https://github.com/themefisher/vex-hugo.git themes/vex-hugo
echo 'theme = "vex-hugo"' >> config.toml

hugo server
```
