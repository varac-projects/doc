# Website feeds

## Selfhosted feed readers

- [Awesome selfhosted: Feed readers](https://github.com/awesome-selfhosted/awesome-selfhosted#feed-readers)
  Long extensive list
- [The Best Self-Hosted RSS Feed Readers](https://lukesingham.com/rss-feed-reader/)
  Curated overview

### Out of the game

- Nextcloud News
  - Nextcloud instance, and some installations struggle with
    proper cronjob setup
- [Tiny Tiny RSS: don’t support Nazi sympathisers](https://flameeyes.blog/2017/09/03/tiny-tiny-rss-dont-support-nazi-sympathisers/)
- No helm charts:
  - Winds
  - Stringer
  - Selfoss
  - Feedbin
  - Commafeed
    - One helm chart found
    - Java

### Feed readers with helm charts

#### Miniflux

- [Website](https://miniflux.app/)
  - [Features](https://miniflux.app/features.html)
    - Regex filter to allow or block articles
  - DB: Podtgres only
- [GitHub](https://github.com/miniflux/v2)
  - Golang
  - 254 contributors
- 2 [miniflux charts](https://artifacthub.io/packages/search?ts_query_web=miniflux&sort=relevance&page=1)
  - [gabe565/miniflux](https://artifacthub.io/packages/helm/gabe565/miniflux):
    Most recent updated helm chart
- [Official container image: ghcr.io/miniflux/miniflux](https://github.com/miniflux/v2/pkgs/container/miniflux)
  - [Artifacthub security-report: F !](https://artifacthub.io/packages/helm/gabe565/miniflux?modal=security-report)

#### FreshRSS

- [FreshRSS](https://freshrss.org/)
  - DB: PostgreSQL, SQLite, MariaDB, MySQL
- [GitHub](https://github.com/FreshRSS/FreshRSS)
  - Php
- [Demo](https://demo.freshrss.org/)
- 5 [FreshRSS helm charts](https://artifacthub.io/packages/search?ts_query_web=freshrss&sort=relevance&page=1)
  - Only one recently updated
- [FreshRSS Extensions repo](https://github.com/FreshRSS/Extensions)
  - [FilterTitle](https://github.com/cn-tools/cntools_FreshRssExtensions/tree/master/xExtension-FilterTitle)
    - A FreshRSS extension to filter feed entries by title.

## Clients

### Android

- [F-Droid: Capy Reader](https://f-droid.org/en/packages/com.capyreader.app/)
  - Recent releases
- [F-Droid: Readrops client](https://f-droid.org/en/packages/com.readrops.app/)
  - Recent releases
- [F-Droid: Read You](https://f-droid.org/en/packages/me.ash.reader/)
  - Recent releases
- [GPlay: FeedMe](https://play.google.com/store/apps/details?id=com.seazon.feedme)
  - Recent releases
- [FocusReader](https://play.google.com/store/apps/details?id=allen.town.focus.reader)
  - Recent releases
- [Fluent Reader Lite](https://github.com/yang991178/fluent-reader-lite)
  - Non-free on Google PLay
  - Free from GitHub releases

#### Outdated

- [F-Droid: News](https://f-droid.org/en/packages/co.appreactor.news/): Recent releases
  - Last release 2023
- [F-Droid: EasyRSS client](https://f-droid.org/en/packages/org.freshrss.easyrss/) last release 2020

## Feed generators

- [r/rss: Free RSS generator ?](https://www.reddit.com/r/rss/comments/15gxref/free_rss_generator/)
- Test URL: <https://www.hbgr.org/ueber-uns/pressemitteilungen>

Options:

- morse.it
  - [Generated URL](https://morss.it/:items=||*[class=mod-articles-category-title]/https://www.hbgr.org/ueber-uns/pressemitteilungen)
    - doesn't work with curl, but with NC News

Doesn't work properly:

- rss.app: Latest post missing

Outdated:

- [hfeeds](https://github.com/shafihuzaib/hfeeds)
  - Last commit 2019
