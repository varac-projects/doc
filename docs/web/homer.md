# Homer

- [Github](https://github.com/bastienwirtz/homer)
- [Demo site](https://homer-demo.netlify.app/)
- [Helm charts](https://artifacthub.io/packages/search?ts_query_web=homer&sort=relevance&page=1)
- [Docs](https://github.com/bastienwirtz/homer/tree/main/docs)

## Alternatives

- [homepage](https://github.com/gethomepage/homepage)
  A highly customizable homepage (or startpage / application dashboard) with Docker and service API integrations.

## Install

    pamac install homer-web

## Usage
