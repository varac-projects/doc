# Playwright

## Playwright for Python

[Website](https://playwright.dev/python/)

[Install](https://playwright.dev/python/docs/intro):

```sh
pamac install python-playwright
```

Install system dependencies (works only for Debian-based OS, [not for Arch](https://github.com/microsoft/playwright/issues/8100))

```sh
playwright install-deps
```

Install [system dependencies for Manjaro](https://github.com/microsoft/playwright/issues/2621#issuecomment-931530175):

```sh
pamac install icu66 libwebp052 libffi7
sudo pacman -S flite
```

Install the required browsers:

```sh
playwright install firefox
```

### Usage

#### Authentication

- [Http basic auth](https://playwright.dev/python/docs/network#http-authentication)

##### Basic auth

```sh
playwright codegen -b chromium "https://USER:PASSWORD@example.org"
```

#### Debug mode

Run playwright in [debug mode](https://playwright.dev/python/docs/debug):

```sh
export PWDEBUG=1
```

#### Docker

- [Docker Python](https://playwright.dev/python/docs/docker)
- [M$ image registry](https://mcr.microsoft.com/en-us/product/playwright/python/about)

### Codegen UI

[Test generator](https://playwright.dev/docs/codegen-intro)

```sh
playwright codegen -b firefox ix.de
```

Copy & paste resulting code to file, and run with:

```sh
python ix.de.playwright.py
```

## Docker images

- [Official Playwright for Python images](https://mcr.microsoft.com/en-us/product/playwright/python/tags)
  are ~2Gb big

- [Question: How to reduce size of Docker image for Playwright (focal)](https://github.com/microsoft/playwright/issues/10168)
  Alpine 3.19 (-3.15):

  $ pip install playwright
  ERROR: Could not find a version that satisfies the requirement playwright
  ERROR: No matching distribution found for playwright
