# Cypress

## Install

* [Installing Cypress](https://docs.cypress.io/guides/getting-started/installing-cypress#npm-install)

Using npm

    npm install cypress --save-dev

## Docker

* https://www.cypress.io/blog/2019/05/02/run-cypress-with-a-single-docker-command/
* <https://mtlynch.io/painless-web-app-testing/>
* [Official docker images](https://hub.docker.com/u/cypress)

## Usage

* [Example playground](https://example.cypress.io)

Get started:

    ./node_modules/.bin/cypress open

Then start adding tests in ./

## Issues

* [Cypress in docker fails to run with read-only project dir](https://github.com/cypress-io/cypress/issues/22502)

## Logging

There's still no native support for [Capture + Display all logs for everything that happens in Cypress](https://github.com/cypress-io/cypress/issues/448)
There are work-around plugins though:

* [cypress-debugger](https://github.com/currents-dev/cypress-debugger)
* [cypress-terminal-report](https://github.com/archfz/cypress-terminal-report)
* [cypress-log-to-output](https://github.com/flotwig/cypress-log-to-output)
  * [only works on Chrome. But chrome headless does not work and does not support video recording](https://github.com/cypress-io/cypress/issues/448#issuecomment-517235698)
  * Can't make it work with electron: [Warning: An unsupported browser family was used, output will not be logged to console: chromium](https://github.com/flotwig/cypress-log-to-output/issues/16)

### Use DEBUG env var

* [See Troubleshooting:Print DEBUG logs](https://docs.cypress.io/guides/references/troubleshooting#Print-DEBUG-logs)
* For electron browser `DEBUG=cypress:electron`
