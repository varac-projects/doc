# Taiko

## Installation

<https://github.com/getgauge/taiko>

* [How Taiko compares to other browser automation tools](https://gauge.org/2019/08/21/how-taiko-compares-to-other-browser-automation-tools/)

Install / upgrade:

    npm install -g taiko

## Use docker image

    docker run -it open.greenhost.net:4567/stackspin/stackspin/stackspin-ci:main bash
    taiko
      openBrowser({headless: true, args: ['--disable-gpu', '--disable-dev-shm-usage', '--disable-setuid-sandbox', '--no-first-run', '--no-sandbox', '--no-zygote']})

## Usage

Start REPL:

    taiko

Example:

    openBrowser()
    goto("google.com")
    await button('Ich stimme zu').exists()
    click(button('Ich stimme zu'))
    focus(textBox(below(image('Google'))))
    press('Enter')
    close

## Use taiko scrpts

    cd ~/projects/testing/taiko/ptf-demo
    taiko --observe ptf-demo.js

## Element attributes

<https://github.com/getgauge/taiko/issues/548>

i.e. onlyoffice

    let italicButtonId = '#id-toolbar-btn-italic'
    italicButton = $(italicButtonId)
    italicButton._description
    buttonState = evaluate($(italicButtonId), (elem) => {return elem.getAttribute('class')})

## Taiko plugins

### Taiko Screencast Plugin

<https://github.com/getgauge-contrib/taiko-screencast>

* records GIF using [gifencoder](https://www.npmjs.com/package/gifencoder)
* Slow capture
* GIF is played back in a decent speed with default config

Install:

    npm install -g taiko-screencast

Record:

    taiko --observe ~/projects/testing/taiko/screencast.js --plugin screencast

* Screencast break at Onlyoffice with [MaxListenersExceededWarning](https://github.com/getgauge-contrib/taiko-screencast/issues/19)

### taiko-video

<https://github.com/hkang1/taiko-video/>

* Records mp4 using ffmpeg, much smaller than the gif output from `taiko-screencast`
* Much faster than `taiko-screencast`
* Adjustable frames per second. [Default FPS is much too fast](https://github.com/hkang1/taiko-video/issues/4#issuecomment-894109792),
  but 0.6-0.8 FPS are ok (`await video.startRecording('taiko-video.mp4', 0.6);`

### taiko-screeny

<https://github.com/saikrishna321/taiko-screeny>

> A tool to capture screenshot on every taiko action
