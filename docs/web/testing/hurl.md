# Hurl

- [Website](https://hurl.dev/)
- [hurl.nvim](https://github.com/jellydn/hurl.nvim)
  - run HTTP requests directly from `.hurl` files
- [Template#Functions](https://hurl.dev/docs/templates.html#functions)
