# Web APIs

- [Mockbin.io](https://mockbin.io/)
  - Open-source and fully-free tool that allows you to quickly mock an API endpoint,
    configure a fixed response and track requests to your endpoint.
