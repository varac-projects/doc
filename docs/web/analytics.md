# Website analytics

* [Analytics ohne Cookie-Hinweis](https://moritzorendt.com/analytics-ohne-cookie-hinweis-datenschutz)
* [](https://www.baden-wuerttemberg.datenschutz.de/faq-zu-cookies-und-tracking-2/)
* [Good overview of open source analytics software](https://loganmarchione.com/2021/03/analytics-with-hugo/)
  * Helm charts on artifacthub.io as of 2022-03:
    * Matomo: Multiple charts, some recently updated
    * Umami: One chart, recently updated.
      Supports MySQL and Postgres, chart uses postgres :/
    * Plausible: One stale chart (updated >1y ago)
    * Ackee: One stale chart (updated >5m ago)
    * For all other apps: No charts :/

* [LWN: Lightweight alternatives to Google Analytics](https://lwn.net/Articles/822568/)

## Privacy friendly cloud services

* [Fathom data collection insights](https://usefathom.com/data)

## Plausible

* Uses [clickhouse](https://clickhouse.yandex) + postgres
* [Geolocation docs](https://plausible.io/docs/self-hosting-configuration#ip-geolocation)

### Issues

#### Backup/Restore

* [Docs: Export your website stats](https://plausible.io/docs/export-stats)
  -> No import options !
* [Docs: Stats API reference](https://plausible.io/docs/stats-api)
  "It's a read-only interface to present historical and real-time stats only"
* No clear instructions for [Clickhouse backups](https://github.com/plausible/analytics/discussions/1226)
* [Backup Docker Installation](https://github.com/plausible/analytics/discussions/1132)

#### Geolocation

* [Self-hosted city-level geolocation not working](https://github.com/plausible/analytics/issues/1809)
* [Default db-ip.com geolocation DB is inaccurate](https://github.com/plausible/analytics/discussions/102#discussioncomment-2484907)
* [Feedback for cities and regions](https://github.com/plausible/analytics/issues/1582)
* [Accurate location - city level data](https://github.com/plausible/analytics/discussions/102)
* [Setting up city level data, fresh install only shows countries](https://github.com/plausible/analytics/discussions/1601)
* [Alternative geolocation database](https://github.com/plausible/analytics/discussions/219)

## Matomo

* <https://matomo.oas.varac.net>

### GDPR and legal advisories

* [Tracking ohne Cookies: Wie du Matomo cookiefrei nutzt und ob das eine gute Idee ist](https://www.content-iq.com/tracking-mit-matomo-ohne-cookies/#cookie-banner-ohne-cookies)
* [What data does Matomo track?](https://matomo.org/faq/general/faq_18254/)

### Privacy settings

* [Configure Privacy Settings](https://matomo.org/faq/general/configure-privacy-settings-in-matomo/#step-3-include-a-web-analytics-opt-out-feature-on-your-site-using-an-iframe)

### JS snippet

Customize js snippet in Settings -> Websites -> Tracking code

or manually:

[Disable cookies in client-side js](https://www.content-iq.com/tracking-mit-matomo-ohne-cookies/#cookie-banner-ohne-cookies)

Insert `_paq.push(['disableCookies']);` *before* `paq.push(['trackPageView']);`

### Kubernetes setup

* See `~/kubernetes/flux-config/bitrigger.varac.net/apps/varac/matomo-hr.yaml` for
  helmRelease config
* Add `proxy_client_headers[] = HTTP_X_FORWARDED_FOR` to the [General] section
  to [get the real visitor IP](https://matomo.org/faq/how-to-install/faq_98)
* [Disable cookies in Matomo](https://anarc.at/services/analytics/):
  To disable all cookies for every site in Matomo, go to
  `Administration` → `Privacy` → `Anonymize data` and enable the checkbox `Force tracking without cookies`
* [Geolocation](https://matomo.org/faq/how-to/setting-up-accurate-visitors-geolocation/)
  See `~/Howtos/geolocation.md` how to get the db
  * [How do I get the DB-IP databases](https://matomo.org/faq/how-to/#faq_163)

  ```sh
  scp /var/lib/GeoIP/GeoLite2-City.mmdb bitrigger:/var/lib/Stackspin/local-storage/pvc-20f288b4-3e42-4422-b204-26c5c8b6c1da_varac_matomo-data/matomo/misc/DBIP-City.mmdb
  kubectl -n varac exec deploy/matomo -c matomo -- ln -s /bitnami/matomo/misc/DBIP-City.mmdb /opt/bitnami/matomo/misc/DBIP-City.mmdb
  ```

* [Circumvent Ublock origin which blocks any `matamo.js|php` requests by
  default](https://nicco.io/blog/matomo-vs-ublock-origin)

Issues:

* `/opt/bitnami/` is not persitant, while `/bitnami` is. So the symlink
      will vanish after a pod restart

## Goatcounter

<https://www.goatcounter.com/>

* [No GDPR consent needed](https://www.goatcounter.com/help/gdpr)
* [Anarcat uses Goatcounter](https://anarc.at/services/analytics/)
* No chart is available
* No official container image, [only 3rd-party ones](https://hub.docker.com/search?q=goatcounter)

## Loki 2.0

* <https://github.com/grafana/loki/blob/master/docs/sources/upgrading/_index.md#200>
* <https://github.com/grafana/loki/blob/master/CHANGELOG.md>

### Nginx log format

* auf json umstellen

### Dashboard

* <https://grafana.com/grafana/dashboards/12559>
