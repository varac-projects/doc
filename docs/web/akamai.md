# Akamai

## cli client

- [akamai/cli](https://github.com/akamai/cli)
- [Docs: Work with CLIs](https://techdocs.akamai.com/developer/docs/about-clis)
- [Credentials file](https://techdocs.akamai.com/developer/docs/set-up-authentication-credentials#add-credential-to-edgerc-file):
  `~/.edgerc`

Install:

```sh
pamac install akamai-bin
```

Configure:

- [Create authentication credentials](https://techdocs.akamai.com/developer/docs/set-up-authentication-credentials)
- [Add credential to .edgerc file](https://techdocs.akamai.com/developer/docs/set-up-authentication-credentials#add-credential-to-edgerc-file)

## Purge cache

- [UI: Fast Purge](https://control.akamai.com/apps/fast-purge/#/)
- [akamai/cli-purge](https://github.com/akamai/cli-purge)

Akamai CLI for Purge allows you to purge cached content
from the Edge using FastPurge (CCUv3):

Install purge plugin:

```sh
akamai install purge
```

Run in one terminal:

```sh
watch 'http -h https://dev.maps.ndr.de/data/2024-zensus/index.html \
  "Pragma: akamai-x-cache-on, akamai-x-cache-remote-on, akamai-x-check-cacheable, \
   akamai-x-get-cache-key, akamai-x-get-ssl-client-session-id, \
   akamai-x-get-true-cache-key, akamai-x-get-request-id" | grep -E "(Cache-Control|X-Cache:)"'
```

Purge in another terminal and verify in first terminal if the
`Cache-Control: max-age=...` value is reset.

```sh
akamai purge invalidate https://example.org
```
