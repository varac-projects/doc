# Link checker

- [Link checker comparism table](https://github.com/lycheeverse/lychee?tab=readme-ov-file#features)

- [Lychee](https://github.com/lycheeverse/lychee)
  - [Docs]
  - [Configuration File](https://lychee.cli.rs/usage/config/)
  - Rust, fast
  - Included in [Megalinter](https://megalinter.io/latest/descriptors/spell_lychee/)
  - [pre-commit hook](https://github.com/lycheeverse/lychee/?tab=readme-ov-file#pre-commit-usage)
  - [Feature request: add FlareSolverr solver to bypass CloudFlare protection](https://github.com/lycheeverse/lychee/issues/1439)
    - [Don't treat sites with 403 status codes as broken links?](https://github.com/lycheeverse/lychee/issues/1157)
  - [Don't tag nightly pre-releases](https://github.com/lycheeverse/lychee/issues/1601)
- [Markdown-link-check](https://github.com/tcort/markdown-link-check)
  - Included in [Megalinter](https://megalinter.io/latest/descriptors/markdown_markdown_link_check/)
  - [Links to sites that have DDoS protections are marked as failed](https://github.com/tcort/markdown-link-check/issues/109)
  - pre-commit hook included (see this repos [.pre-commit-config.yaml](https://0xacab.org/varac-projects/doc/-/raw/main/.pre-commit-config.yaml?ref_type=heads)
- [Linkspector](https://github.com/UmbrellaDocs/linkspector)
  - Uses Puppeteer to check links in Chrome's headless mode,
    reducing the number of false positives.
