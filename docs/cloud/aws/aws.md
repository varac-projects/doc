# General

- [AWS console](https://console.aws.amazon.com/console/home)

## Setup API access

Upload ssh-key:  IAM -> Users -> varac -> Upload SSH key
Create api access key: IAM -> Users -> varac -? Create access key
Save access key in `~/.aws/credentials`

Test access key:

```sh
aws --profile rewire iam get-user
```

## Billing

- Billing only works in us2-west

[The Startup Quick Guide to Managing Budgets, Credits, and AWS Resources](https://aws.amazon.com/blogs/startups/how-to-set-aws-budget-when-paying-with-aws-credits/)

## Install CLI

```sh
sudo apt install awscli
```

## Configure

[Configure the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

See `~/.aws/config` and `~/.aws/credentials`

```sh
 aws configure
```

## Env vars

[Environment variables to configure the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

```sh
export AWS_ACCESS_KEY_ID=... # gitleaks:allow
export AWS_SECRET_ACCESS_KEY=... # gitleaks:allow
export AWS_DEFAULT_REGION=us-west-2
```
