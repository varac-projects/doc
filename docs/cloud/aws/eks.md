# EKS



## Install eks cluster with terraform

* [TF tutorial: Provision an EKS Cluster](https://learn.hashicorp.com/tutorials/terraform/eks)
* [TF eks_cluster resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster)
* [TF Kubernetes provider: Manage Kubernetes Resources via Terraform](https://learn.hashicorp.com/tutorials/terraform/kubernetes-provider)

## eksctl

* [eksctl website](https://eksctl.io)
* [github](https://github.com/weaveworks/eksctl)
* [AWS docs: Getting started with eksctl](https://docs.aws.amazon.com/eks/latest/userguide/getting-started-eksctl.html)
* [AWS docs: Create a cluster using eksctl](https://docs.aws.amazon.com/eks/latest/userguide/create-cluster.html)


Install:

    brew install eksctl

Usage:

    eksctl create cluster \
     --name <my-cluster> \
     --version <1.21> \
     --without-nodegroup

* [Using config files](https://eksctl.io/usage/creating-and-managing-clusters/#using-config-files)
