# AWS IAM

List groups a user is member of:

    aws iam list-groups-for-user --user-name varac

List policies for group:

    aws iam list-attached-group-policies --group-name eks_admins
