# AWS EC2

[Debian aws cli](https://wiki.debian.org/Amazon/EC2/HowTo/awscli)
[Debian buster image](https://aws.amazon.com/marketplace/pp/B0859NK4HC)

Debian marketplace owner ID: `136693071363`

## Images

<https://wiki.debian.org/Cloud/AmazonEC2Image/Bullseye>

Get all debian images published by the official debian account:

    aws ec2 describe-images --region eu-north-1 --owners 136693071363 \
      --query 'sort_by(Images, &CreationDate)[].[CreationDate,Name,ImageId]' \
      --output table

Latest bullseye:

    aws ec2 describe-images --region eu-north-1 --owners 136693071363 \
      --filters 'Name=architecture,Values=x86_64' 'Name=name,Values=debian-11*' \
      --query 'sort_by(Images, &CreationDate)[-1].[ImageId]' --output text

## Regions

<https://aws.amazon.com/about-aws/global-infrastructure/regions_az/?p=ngi&loc=2>

## List keypairs

    aws ec2 run-instances --image-id ami-xxxxxxxx --count 1 \
      --instance-type t1.micro --key-name MyKeyPair --security-groups my-sg

## List security-groups

    aws ec2 describe-security-groups | grep GroupName
    aws ec2 describe-security-groups

## Instances

* [Available instance types](https://aws.amazon.com/ec2/instance-types/)
* [Pricing](https://aws.amazon.com/ec2/pricing/on-demand/)

### Launch instance

<https://docs.aws.amazon.com/cli/latest/userguide/cli-ec2-launch.html#launching-instances>

    aws ec2 run-instances --image-id ami-2a34e94a --count 1 \
      --instance-type t2.micro --key-name leap_varac --security-groups leap

Stop instance: `aws ec2 stop-instances --instance-ids i-0d15424fb9449a6a4`

## Set status of instances

    aws ec2 describe-instance-status
    aws ec2 describe-instance-status | jq '.InstanceStatuses[] | { InstanceId }'

## List Instances

Output instance_id, ip_address, name, state as table:

    aws ec2 describe-instances --output table
      --query "Reservations[*].Instances[*].{name: Tags[?Key=='Name'] | \
        [0].Value, instance_id: InstanceId, \
        pub_ip_address: PublicIpAddress, state: State.Name}"

Etc:

    aws ec2 describe-instances --query 'Reservations[*].Instances[*].LaunchTime'
    aws ec2 describe-instances \
      --query 'Reservations[*].Instances[*].[InstanceId,LaunchTime]' --output text

    aws ec2 describe-instances \
    --filters "Name=instance-state-name,Values=pending,running,stopped,stopping"\
    --query "Reservations[].Instances[].[InstanceId]" --output text | tr '\n' ' '

## Get console output

    aws ec2 get-console-output --output text --instance-id i-a50e3e7

## Add second public IP to instance

<http://blog.ls20.com/get-two-public-ips-on-an-amazon-ec2-instance-for-free/>
<https://leap.se/code/issues/8532#note-1>

Note down primary elastic network interface (ENI) from response of instance
creation, or get it with:

    aws ec2 describe-instances --instance-ids i-2107fdfd \
    --query 'Reservations[0].Instances[0].NetworkInterfaces[0].NetworkInterfaceId'

Add sec. private IP

    aws ec2 assign-private-ip-addresses --network-interface-id eni-45bd1014 \
      --secondary-private-ip-address-count  1

Note down sec. private IP from above cmd or get it with:

    aws ec2 describe-instances --instance-ids i-2107fdfd --query 'Reservations[0].Instances[0].NetworkInterfaces[0].PrivateIpAddresses[?Primary==`false`][0].PrivateIpAddress'

Allocate public elastic IP address (EIP)

    aws ec2 allocate-address

Note down IP from above cmd

Associate new public EIP with secondary private IP

    aws ec2 associate-address --instance-id i-2107fdfd --public-ip 35.162.83.69 \
      --private-ip-address 172.31.13.197

The second IP now only needs to get added to the network IF on the Vm itself
with i.e.

    ip addr add dev eth0 192.168.x.y/24

But this is what the platform will do for us.

Todo:

* Implement watchdog to remove unused EIPs

## Terminate Instance

    aws ec2 terminate-instances --instance-ids 'i-....'

### Terminate ALL instances

DANGER !

    aws ec2 terminate-instances --instance-ids $(aws ec2 describe-instances \
    --filters "Name=instance-state-name,Values=pending,running,stopped,stopping"\
    --query "Reservations[].Instances[].[InstanceId]" --output text | tr '\n' ' ')
