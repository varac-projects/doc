# Minio

- [Docs](https://min.io/docs)
- [Arch wiki](https://wiki.archlinux.org/title/MinIO)

## Server

- [Container Installation](https://min.io/docs/minio/container/index.html)
- [MinIO Object Storage for Kubernetes](https://docs.min.io/docs/minio-client-quickstart-guide)

## Client

- [Github](https://github.com/minio/mc)
- Golang
- [MinIO Client docs](https://min.io/docs/minio/linux/reference/minio-mc.html)
- Comes with pre-configured sandbox play store (`mc ls play`)

### Install

Install with brew: `brew install minio/stable/mc`

Arch:

```sh
sudo pacman -S minio-client
alias mc=/usr/bin/mcli
```

Binary download:

```sh
wget https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
mv mc /usr/local/bin/
```

#### Autocompletion

Doesn't work well.

Setup autocompletion (updates i.e. `~/.zshrc`):

```sh
mc --autocompletion
```

### Config

Setup new store with credentials:

Config incl. configured aliases: `~/.mc/config.json`

Varac's store:

```sh
mc alias set pinenas-pub-varac $URL $ACCESSKEY $SECRETKEY
```

Show all configured aliases:

```sh
mc alias ls
```

### Usage

Create bucket:

```sh
mc mb varac/test
```

List buckets:

```sh
mc ls varac/test
```

Copy a file:

```sh
mc cp minicom.log varac/test
```

Find all files in bucket/prefix:

```sh
mc find varac/test
```

### Stats / disk usage

Overall stats:

```sh
mc stat varac/
```

Bucket stats:

```sh
mc stat varac/test
```

Disk usage per dir:

```sh
mc du varac/test
```

### Admin client usage

- [Minio admin client docs](https://min.io/docs/minio/linux/reference/minio-mc-admin.html)
  - [User managment](https://min.io/docs/minio/linux/administration/identity-access-management/minio-user-management.html)

### Permissions

- Login to admin console
- Create access key
- Grant access key `admin:XYZ` privileges, see [Admin Policy Action Keys](https://min.io/docs/minio/linux/administration/identity-access-management/policy-based-access-control.html#mc-admin-policy-action-keys)
  or `admin:*` (Beware !)

Show info (needs admin credentials configured):

```sh
mc admin info pinenas-pub-admin
```

Create a user:

```sh
mc admin user add pinenas-pub-admin newuser newusersecret
```

Add serviceaccount to varac's account:

```sh
mc admin user svcacct add pinenas-pub-varac varac \
  --name restic-zancas \
  --description "Restic backup on zancas"
```

Show information about accesskey including attached [policy](https://min.io/docs/minio/linux/administration/identity-access-management/policy-based-access-control.html#id4):

```sh
mc admin user svcacct info restic-zancas ACCESSKEY --policy
```

List all Serviceaccounts associated to an account:

```sh
mc admin user svcacct list pinenas-pub-admin varac
```

Dump `readwrite` (default) policy and write to file:

```sh
mc admin policy info pinenas-pub-admin readwrite | \
  jq .Policy > ~/projects/cloud/storage/minio/policy.readwrite.json
```

Restrict bucket access on Serviceaccount:

```sh
jq '.Statement[0].Resource = ["arn:aws:s3:::restic.zancas/*"]' \
  ~/projects/cloud/storage/minio/policy.readwrite.json > /tmp/policy-new.json
mc admin user svcacct edit --policy /tmp/policy-new.json \
  pinenas-pub-admin ACCESSKEY
```

## Minio directpv

- [Minio's CSI driver](https://github.com/minio/directpv)

### Install directpv krew plugin

```sh
kubectl krew install directpv
```

### Install CSI driver with helm chart

- Official [helm chart](https://github.com/minio/directpv/tree/master/operator/helm-charts/directpv-chart)
  - Last chart version update 2023-12, but recent commits to `templates/`
  - No value templating, plain manifests
  - No [Helm chart repository](https://github.com/minio/directpv/issues/956)
  - No way to [Configure replicas](https://github.com/minio/directpv/pull/957)
  - [Hardcoded Namespace](https://github.com/minio/directpv/pull/957)
- [nineinfra-charts/minio-directpv](https://artifacthub.io/packages/helm/nineinfra-charts/minio-directpv)
  - [Chart source](https://github.com/nineinfra/minio-charts)
  - 4 commits, one relase, last update 2023-11

### Install CSI driver manually

Only use this method for testing, the helm chart method should be
preferred.

Install DirectPV in all Kubernetes nodes:

```sh
kubectl directpv install
```

This will install the following resources:

```text
┌──────────────────────────────────────┬──────────────────────────┐
│ NAME                                 │ KIND                     │
├──────────────────────────────────────┼──────────────────────────┤
│ directpv                             │ Namespace                │
│ directpv-min-io                      │ ServiceAccount           │
│ directpv-min-io                      │ ClusterRole              │
│ directpv-min-io                      │ ClusterRoleBinding       │
│ directpv-min-io                      │ Role                     │
│ directpv-min-io                      │ RoleBinding              │
│ directpvdrives.directpv.min.io       │ CustomResourceDefinition │
│ directpvvolumes.directpv.min.io      │ CustomResourceDefinition │
│ directpvnodes.directpv.min.io        │ CustomResourceDefinition │
│ directpvinitrequests.directpv.min.io │ CustomResourceDefinition │
│ directpv-min-io                      │ CSIDriver                │
│ directpv-min-io                      │ StorageClass             │
│ node-server                          │ Daemonset                │
│ controller                           │ Deployment               │
└──────────────────────────────────────┴──────────────────────────┘
```
