# Cloud providers

## European

Alternatives to the big 3:

- [European cloud computing platforms](https://european-alternatives.eu/category/cloud-computing-platforms)
- [Stackit](https://www.stackit.de/en/)
  - Based in Germany
- [Open Telekom Cloud](https://www.open-telekom-cloud.com/)
  - Based in Germany
  - Based on [OpenStack](https://de.wikipedia.org/wiki/OpenStack)
