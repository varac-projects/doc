# Gcloud compute

* [Google Compute Engine Machine Type Comparison](https://gcloud-compute.com/)
  * i.e. [t2d-standard-8 vs. n2d-standard-8](https://gcloud-compute.com/comparison/t2d-standard-8/vs/n2d-standard-8.html)
* [Google Cloud Pricing Calculator](https://cloudpricingcalculator.appspot.com/)
