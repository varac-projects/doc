# glcoud

- [Google Cloud CLI documentation](https://cloud.google.com/sdk/docs)
  - [Command reference](https://cloud.google.com/sdk/gcloud/reference)

## Config

- [gcloud config reference](https://cloud.google.com/sdk/gcloud/reference/config)

Configuration files are stored under `~/.config/gcloud/configurations`

> In addition to setting properties in a configuration, and the use of flags, it
> is possible to override the value of a property with an environment variable.
> The matching environment variable for a property is of the form
> 'CLOUDSDK_CATEGORY_PROPERTY'.
> For example, to demonstrate overriding the project property in the core
> category to my-project, use a command like:
> CLOUDSDK_CORE_PROJECT=my-project gcloud config get core/project

## General usage

### Filters

- [gcloud topic filters](https://cloud.google.com/sdk/gcloud/reference/topic/filters)
- [Filter example](https://gist.github.com/rezamt/6ee102ee653e37280e35d649e7c221b5)

List domains without `pendingContactSettings`:

    gcloud domains registrations list \
      --filter "pendingContactSettings.adminContact=.*" --format "table[no-heading](DOMAIN)"

### Auth login

    gcloud auth login

### Proxy / custom CA cert

- [Gcloud docs: Proxy settings](https://cloud.google.com/sdk/docs/proxy-settings)

Use the ca cert bundle (including custom certs):

    gcloud config set core/custom_ca_certs_file /etc/ssl/cert.pem

## Cloud SQL

    gcloud sql backups list --instance=mastodon
    gcloud sql backups create --instance=mastodon \
      --description="Manual before instance upgrade"

## Projects

List all projects belonging to an organization:

    gcloud beta asset search-all-resources \
      --asset-types=cloudresourcemanager.googleapis.com/Project \
      --scope=organizations/1234
