# Secrets

Create secret:

    echo -n "$APIKEY" | gcloud secrets create
      dark-grafana-api-key --data-file=- \
      --replication-policy=user-managed \
      --locations=europe-west1,europe-west2,europe-west3,europe-west4,europe-west6,europe-north1

List secrets:

    gcloud secrets list

Show secret metadata:

    gcloud secrets describe dark-grafana-api-key

[Show secret content](https://cloud.google.com/secret-manager/docs/access-secret-version):

    gcloud secrets versions access latest --secret="dark-grafana-api-key"

[Update secret](https://cloud.google.com/secret-manager/docs/add-secret-version#secretmanager-add-secret-version-gcloud):

    echo -n "$APIKEY" | gcloud secrets versions add dark-grafana-api-key --data-file=-
