# GCP cloud SQL

Usage:

```sh
gcloud storage ls
gcloud storage ls gs://mastodon--dev/accounts
gcloud storage cp /tmp/index.html gs://versatiles-prod/index.html
```

## Backup

[Bucket backup options](https://serverfault.com/a/899332):

- [Storage Transfer Service](https://cloud.google.com/storage-transfer-service)
- `gsutil cp` or `gsutil rsync` to another bucket
- [Object versioning](https://cloud.google.com/storage/docs/object-versioning)
  together with [Object Lifecycle Management](https://cloud.google.com/storage/docs/lifecycle)
