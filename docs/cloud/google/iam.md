# Gcloud IAM

- [Gcloud IAM: Roles and Permissions](https://gcloud-iam.nkn-it.de/)

## Service accounts

List/describe service accounts:

```sh
gcloud iam service-accounts list
gcloud iam service-accounts describe

```

Get the IAM policy for a service account:

```sh
gcloud iam service-accounts get-iam-policy …@….iam.gserviceaccount.com
```

Impersonate a serviceAccount:

```sh
export GOOGLE_IMPERSONATE_SERVICE_ACCOUNT="…@….iam.gserviceaccount.com"
gcloud …

```
