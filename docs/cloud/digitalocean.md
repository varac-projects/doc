# Digital Ocean

## doctl

<https://github.com/digitalocean/doctl>
<https://docs.digitalocean.com/reference/doctl/>

    sudo snap install doctl

### doctl and greenhost API

    export DIGITALOCEAN_ACCESS_TOKEN="$COSMOS_API_TOKEN"
    export DIGITALOCEAN_API_URL=https://service.greenhost.net/api/v2

More or less useless with GH API because of i.e.
[doctl compute droplet list fails](https://code.greenhost.net/greenhost/cosmos2/-/issues/1483)

## API

<https://developers.digitalocean.com/documentation/v2/>
<https://service.greenhost.net/apidocs#/default>

    cd ~/projects/config-management/terraform/greenhost-test
    export API_TOKEN="$(gopass show --password www/service.greenhost.net/ci-api-key)"

SSH keys (`/v2/account/keys`):

    curl -H "Authorization: Bearer $API_TOKEN" "https://service.greenhost.net/api/v2/account/keys"

Regions:

    curl -H "Authorization: Bearer $API_TOKEN" 'https://service.greenhost.net/api/v2/regions'

droplets :

## DNS records

Get:

    curl -H "Authorization: Bearer $API_TOKEN" "https://service.greenhost.net/api/v2/domains/openappstack.net/records"

Create a reccord:

ttl is not requirerd

    record='{"name": "varac-cli-test", "data": "1.2.3.4", "type": "A", "ttl": 60}'


    curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $API_TOKEN" -d "$record" https://service.greenhost.net/api/v2/domains/openappstack.net/records
