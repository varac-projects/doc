# Matrix

* [Matrix bots](https://github.com/matrix-org/go-neb)

## API

<https://matrix.org/docs/guides/client-server-api>

### Get API token

* In the UI: `Settings/Help`
* With curl:

    `curl -XPOST -d '{"type":"m.login.password", "user":"alertmanager-moewe", "password":".."}' "https://matrix.org/_matrix/client/r0/login"`
