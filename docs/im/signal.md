# signal-desktop

## Install

- [Download](https://signal.org/de/download)

Install on Ubuntu/Debian:

```sh
wget -O- https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" |
  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update && sudo apt install signal-desktop
```

## Issues

### No UI

- [signal-desktop starts minimized on wayland](https://www.reddit.com/r/signal/comments/1c319dz/signaldesktop_starts_minimized_on_wayland/)
  - Solution: [Start Signal twice](https://www.reddit.com/r/signal/comments/1c319dz/comment/l65sbe4/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)
