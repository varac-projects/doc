# IRC

## IRC networks

* [libera](https://libera.chat/guides/connect)
  * Server: irc.eu.libera.chat
  * Port: 6697
  * Server password: -

* indymedia
  * Server: irc.indymedia.org
  * Port: 6697
  * Server password: -

## Public web clients

* [irc.puscii.nl](https://irc.puscii.nl) (TheLounge)

## Generic IRC cmds

Get operators, channel ops etc

    /msg chanserv access #channel list

## Add Server

    /server add libera.chat irc.eu.libera.chat/6669 -ssl
    /connect

## Register

    /query nickserv register YourUeberPassOnlyForIrc my-spam-catching@emailaddress

Enforce (protect username againgst non-Identified users):

    /msg nickserv set enforce on

Request cloak (oftc):

    /msg nickserv set cloak on

## Identify + Autojoin

    /set irc.server.libera.chat.command \
      "/msg NickServ identify xxxxx;wait 2000;/msg ChanServ invite #rtc"

## rejoin chat protected with password

. using weechat autojoin config

    /disconnect libera.chat
    /connect libera.chat

. manual

    /connect libera.chat
    /msg -server libera.chat nickserv identify xxxxx
    /msg -server libera.chat ChanServ invite #CHAT
    /join -server libera.chat #CHAT

## Channel modes

[channelmodes list](https://freenode.net/kb/answer/channelmodes)

Make channel secret (prevent spammers):

    /mode #cad +s

## Long Urls

[Bare display](https://weechat.org/blog/post/2014/02/16/Bare-display)

Use `bare display` (ALT+L) to open multi-line URLs.

## Standalone IRC Web interfaces

* [Convos.chat](https://convos.chat/)
* Stale: [dispatch](https://github.com/khlieng/dispatch)

### The Lounge

* [Website](https://thelounge.chat)
* [github](https://github.com/thelounge/thelounge)
* [Docs](https://thelounge.chat/docs)
* Up to date [official docker image](https://hub.docker.com/r/thelounge/thelounge)
* Up to date [truecharts helm chart](https://artifacthub.io/packages/helm/truecharts/thelounge)
* [Solarized theme](https://github.com/thelounge/thelounge-theme-solarized)

Exec into container:

    kubectl -n thelounge exec -it deployments/thelounge -- sh

List users:

    thelounge list

Add user:

    thelounge add varac

Install theme:

    thelounge install thelounge-theme-solarized

Issues with TheLounge:

* [Plugins WIP](https://github.com/thelounge/thelounge/projects/4)

## Client / Server options

* [artifact hub irc search](https://artifacthub.io/packages/search?page=1&ts_query_web=irc)
* [quassel](https://quassel-irc.org/)
  * [Last release 2019](https://github.com/quassel/quassel/releases)

### Weechat

* [docker-image](https://hub.docker.com/r/jkaberg/weechat)

### smuxi

* [Website](https://smuxi.im/)
* [Last release 2018](https://github.com/meebey/smuxi/releases) (as of 2020-10)
* Stale [official docker image](https://hub.docker.com/r/smuxi/smuxi/tags)
* Stale [redmine issue tracker](https://smuxi.im/projects/smuxi/issues) linked
  from [website](https://smuxi.im/contribute)
* Gnome integration
* No helm chart
