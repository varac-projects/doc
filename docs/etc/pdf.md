# PDF

[wiki.ubuntuusers.de: PDF](http://wiki.ubuntuusers.de/PDF)

## Viewers

[Formulare ausfüllen](https://wiki.ubuntuusers.de/PDF/#PDF-Dateien-anzeigen-kommentieren-und-Formulare-ausfuellen)

## Evince

- [Cannot check a form checkbox element](https://gitlab.gnome.org/GNOME/evince/-/issues/1114)

By default, [Evince has a very annoying zoom limit](https://fransdejonge.com/2017/10/fix-minuscule-zoom-limit-in-evince/)
Fix it with:

```sh
$ gsettings get org.gnome.Evince page-cache-size
  uint32 512
$ gsettings set org.gnome.Evince page-cache-size 1024
```

Warning - may use a lot of RAM!

## Merge/Rearrange multiple PDFs

### GUI

[pdfarranger](https://github.com/pdfarranger/pdfarranger)
Recently updated

```sh
sudo apt install pdfarranger
```

### cli

```sh
pdfunite in-1.pdf in-2.pdf in-n.pdf out.pdf
```

## Reduce PDF size

- [ps2pdf](https://web.mit.edu/ghostscript/www/Ps2pdf.htm)
- [Reduce PDF File Size in Linux](https://www.digitalocean.com/community/tutorials/reduce-pdf-file-size-in-linux)

Usage:

```sh
ps2pdf input.pdf output.pdf
```

## Convert images to PDF

### With img2pdf

```sh
sudo apt install img2pdf
```

DINA4 portrait:

```sh
img2pdf --pagesize A4 --border 2cm:2.5cm a.png > a.pdf
```

DINA4 landscape:

```sh
img2pdf --pagesize A4^T --border 2cm:2.5cm a.png > a.pdf
```

Issues:

- Can't combine multiple images into one pdf page, each image creates a new
  PDF page

### With imagemagick

Convert and scale jpg to DINA4 pdf:

```sh
convert a.jpg -compress jpeg -resize 1240x1753 -extent 1240x1753 \
  -gravity center  -units PixelsPerInch -density 150x150 a.pdf
convert b* -compress jpeg -resize 1240x1753 -extent 1240x1753 \
  -gravity center  -units PixelsPerInch -density 150x150 b.pdf
```

Combine multiple images in one PDF page:

```sh
montage src* -mode concatenate -tile 1x -resize 500x \
  -page a4 -gravity center dst.pdf
```

## Extract images from PDF

```sh
pdfimages -png document.pdf .
```

## Convert PDF to high quality image

[Convert PDF to image with high resolution](https://stackoverflow.com/questions/6605006/convert-pdf-to-image-with-high-resolution)

```sh
convert -verbose -density 150 -trim -quality 100 -flatten -sharpen 0x1.0 \
   image.png
```

## PDFTK

[pdftk](https://gitlab.com/pdftk-java/pdftk)

```sh
apt install pdftk-java
```

## Remove password from pdf

[How to remove the password from a PDF](https://www.baeldung.com/linux/pdf-remove-password)

```sh
qpdf --password=YOURPASSWORD-HERE --decrypt input.pdf output.pdf
```

Or: Open the protected file and use ctrl+p or use print optiont
to print the file, now save the file as pdf.

## PFD diff tools

### Diffoscope

Best PDF diff tool so far !
[Website](https://diffoscope.org/)

Install:

```sh
pamac install python-pypdf
sudo pacman -S diffoscope tinyxxd python-pdfminer
```

Usage:

```sh
diffoscope resume.pdf /tmp/resume.pdf
```

### diff-pfd

[diff-pdf](https://vslavik.github.io/diff-pdf)

Can't get a useful diff at all :/

### Other diff viewers

- Online: [diffchecker](https://www.diffchecker.com/pdf-compare)
- [pdfdiff](https://github.com/cascremers/pdfdiff)
  Last commit 2022-03
- [pdf-diff](https://github.com/JoshData/pdf-diff)
  Last commit 2019

## Generate PDF

### Markdown to PDF

see [markdown.md](../markup/markdown/markdown.md) "Generate PDF from markdown"

### HTML to PDF

- [How to Automate HTML-to-PDF Conversions](https://www.baeldung.com/linux/html-pdf-conversions)
- For conversion examples using different tools see `~/projects/pdf/html-to-pdf-conversion-examples`

#### Weasyprint

HTML -> PDF generator

- [Website](https://weasyprint.org/)
- [Docs](https://doc.courtbouillon.org/weasyprint/stable/)
- [Test suite with examples](http://test.weasyprint.org/)

Issues:

- [Presentational hints like img size are ignored by default](https://github.com/Kozea/WeasyPrint/issues/872)
- jsonresume PDF looks quite different to what the HTML shows in the browser
- [Resulting PDF only has one page](https://github.com/Kozea/WeasyPrint/issues/9)
  i.e. for jsonresume
- [Reproducible PDF generation possible again](https://github.com/Kozea/WeasyPrint/issues/1553)

#### Various tools

- Pandoc
  PDFs generated from jsonresume look like shite
- [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf)
  jsonresume PDF shows blurry subtitle and other text styles
- [node-html-pdf](https://github.com/marcbachmann/node-html-pdf/)
  - Can't generate PDF from jsonresume:
    [Auto Configuration Failed - libproviders.so: No such file or directory](https://github.com/marcbachmann/node-html-pdf/issues/680)
  - Outdated (last commit March 2021)
- `ebook-convert` from `calibre` package
  jsonresume PDF look absolutely horrible
- `unoconv`: Standalone tool which uses the LibrOffice library to convert
  documents from/to different formats
  jsonresume PDF look absolutely horrible
- [node-puppeteer-pdf-wrapper](https://gitlab.datacollaborative.com/open-source/node-puppeteer-pdf-wrapper)
  Silently finishes without PDF generation...

Headless chromium:

```sh
chromium --headless --disable-gpu --print-to-pdf='varac.pdf' --hide-scrollbars
  --run-all-compositor-stages-before-draw  --no-sandbox --virtual-time-budget=60000 varac.html`
```

- Most similar to html in browser, but with printing footer/header
- [chrome-headless-render-pdf](https://github.com/Szpadel/chrome-headless-render-pdf)
  - Doesn't work: endless [Waiting for chrome to became available](https://github.com/Szpadel/chrome-headless-render-pdf/issues/11)

### Other PDF generators

- [reportlab](https://www.reportlab.com/opensource/):
  source code only mercurial : `apt install python3-reportlab`
- [python-pdfkit](https://github.com/JazzCore/python-pdfkit):
  Wkhtmltopdf python wrapper to convert html to pdf.
  Only converts files/URLs to PDFs.
- [rst2pdf](?)
- [PyPDF4](https://github.com/claird/PyPDF4): python, last commit 2020
- [pyFPDF](https://github.com/reingart/pyfpdf): Python, last commit 2018
- [pdfrw](https://github.com/pmaupin/pdfrw): Python, last commit 2018
