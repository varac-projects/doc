# Printing

- [Arch wiki: CUPS](https://wiki.archlinux.org/title/CUPS)

## Cups administration

- [Arch wiki: CUPS/Allowing admin authentication through PolicyKit](https://wiki.archlinux.org/title/CUPS#Allowing_admin_authentication_through_PolicyKit)

Install:

    pamaxc install system-config-printer cups-pk-helper

Configure `/etc/polkit-1/rules.d/49-allow-passwordless-printer-admin.rules`
as decribed in the docs above. The user needs to be in group `wheel`.
Then use [system-config-printer](https://archlinux.org/packages/extra/x86_64/system-config-printer/)
for printer administration, the web interface still asks for passwords.

## Command line printing

- [Command-Line Printing and Options](https://www.cups.org/doc/options.html)

### simple print

    lp -d Canon_MG6450 FILE

or even, when `Canon_MG6450` is the default printer

    lp FILE

### Duplex

Default settings:

    lpoptions -p Canon_MG6450

Available Settings:

    lpoptions -p Canon_MG6450 -l
    lpoptions -p Canon_MG6450 -l | grep -i duplex

Print parts of document:

    lp -d Canon_MG6450 -o "page-ranges=1 CNGrayscale=True Duplex=None" A.pdf

Greyscale, no Duplex:

    lp -d Canon_MG6450 -o "CNGrayscale=True Duplex=None" A.pdf

Print Duplex:

    lp -d Canon_MG6450 -o Duplex=DuplexNoTumble A.pdf

Duplex Grayscale:

    lp -d Canon_MG6450 -o "Duplex=DuplexNoTumble ColorModel=Gray" A.pdf

### Set margins

    lp -o fit-to-page -o page-left=18 -o page-right=18 -o page-top=18 -o page-bottom=18 FILE

### Query available printers

    lpstat -p -d

### Set default printer

    lpoptions -d Canon_MG6400_series_

### Show default printer/destination

    lpstat -d

### Use different printer

    lp -d EPSON_XP-342_343_345_Series …


### etc

General stats:

     lpc status Canon_MG6450

Show queue:

     lpq -P Canon_MG6450
