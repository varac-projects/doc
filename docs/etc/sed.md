# Sed

## Greedy Regexp in sed

<http://www.skybert.net/unix/non-greedy-matching-in-sed/>

> What you need to do, is to match against everything
> but the closing delimiter, in our case, the '@'.
> This is done with the character group notation
> and the negator ^ in front of the closing delimiter:

```sh
echo 'The @HashMap@ and @ArrayList@ classes are great!' | sed 's|@\([^@]*\)@|<code>\1</code>|g'
```

## Multiple sed cmds in cli

```sh
sed "s/:/\//g; s/git@/https:\/\//"
```

## Change date format

```sh
sed -i -E "s|([0-9]{4})/([0-9]{2})/([0-9]{2})|\1-\2-\3|" transactions-*
```

## Remove empty lines and comments

<https://stackoverflow.com/a/23808815>

```sh
sed -e '/^\s*#.*$/d' -e '/^\s*$/d' inputFile
```

## Replace after matching line

<https://stackoverflow.com/a/18620241>

i.e. Replace third line after match (`foo`):

```sh
sed -i '/foo/{n;n;n;s/bar/baz/}' test.txt
```
