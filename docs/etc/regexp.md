# Regular expressions

## Extract Ip-Numbers

    grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' file.txt

## Greedy wildcards

- http://stackoverflow.com/questions/2503413/regular-expression-to-stop-at-first-match
- incase your using VIM, this regex needs to be a little different: instead of .\*? it's .\{-} for a non-greedy match.

## Insert Line before regexp

    sed '/<\/wpt>/{x;s/.*/  <category>Camping Frankr<\/category>/;G;}' /tmp/camping-test.gpx

- http://www.cyberciti.biz/faq/grep-regular-expressions/
- `[[:digit:]]`

## Obfuscate Email Adr

    echo "tostado@dev.zeromail.org"| sed 's/@/ {at} /'|sed 's/\(.*\)\./\1{dot}/'

## Grep inside .tgz

https://stackoverflow.com/questions/13983365/grep-from-tar-gz-without-extracting-faster-one

    tar xf jessie_packaging.tgz --to-command \
      "awk '/httpredir/ { print ENVIRON[\"TAR_FILENAME\"]; exit }'"
