# Dotfiles

## Overview

- [project-awesome dotfiles](https://project-awesome.org/webpro/awesome-dotfiles)
  - [GitHub repo](https://github.com/webpro/awesome-dotfiles)
- [dotfiles.github.io](https://dotfiles.github.io/)
  - "Your unofficial guide to doing dotfiles on GitHub"
  - [General-purpose dotfiles utilities](https://dotfiles.github.io/utilities/)

## Dotfile managers

- [chezmoi](./chezmoi.md)
- [chezmoi vs. dotbot, rcm, vcsh, yadm, bare git](https://www.chezmoi.io/comparison-table/)
- [Dotfile manager tool list, sorted by GitHub stars](https://dotfiles.github.io/utilities/)

### git bare repository

- [Simplest Way to Sync dotfiles and config Using Bare Git repo](https://medium.com/@augusteo/simplest-way-to-sync-dotfiles-and-config-using-git-14051af8703a)
- [Manage Dotfiles With a Bare Git Repository](https://harfangk.github.io/2016/09/18/manage-dotfiles-with-a-git-bare-repository.html)

Initialize:

```sh
git init --bare $HOME/.dotfiles
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.sh_aliases
. ./.sh_aliases
dotfiles config --local status.showUntrackedFiles no
dotfiles config --local user.name Varac
dotfiles config --local user.email varac@varac.net
dotfiles remote add origin git@0xacab.org:varac-projects/dotfiles.git
```

Usage:

```sh
dotfiles add ~/.config/sway
dotfiles status
dotfiles commit -m'Initial commit'
dotfiles push --set-upstream origin main
```

Pull to another device:

First, add an ssh key of the new device to [0xacab profile: keys](https://0xacab.org/-/profile/keys)
Then:

```sh
git clone --bare git@0xacab.org:varac-projects/dotfiles.git $HOME/.dotfiles
echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.sh_aliases
dotfiles checkout
dotfiles config --local status.showUntrackedFiles no
```

Fetch all remote branches:

Somehow, `dotfiles fetch` doesn't fetch any other remote branches, but this will
do:

```sh
dotfiles fetch  origin '*:*'
```

### Manage dotfiles with plain git repo

- [Managing my dotfiles as a git repository](https://drewdevault.com/2019/12/30/dotfiles.html)
  Using a standard git repo, not a bare one

### Manage dotfiles with Ansible

- [Link files task](https://github.com/elnappo/dotfiles/blob/master/ansible/tasks/link_files.yml)
- [geerlingguy/ansible-role-dotfiles](https://github.com/geerlingguy/ansible-role-dotfiles)

### Other tools

Stale:

- [dot](https://github.com/ubnt-intrepid/dot)

## Example dotfile config repos

- [sloria/dotfiles](https://github.com/sloria/dotfiles)
- [dotphiles](https://github.com/dotphiles/dotphiles)
  A community driven framework of dotfiles
- [andschwa/dotfiles](https://github.com/andschwa/dotfiles)

## pre-commit

- [pre-commit for bare repos with separate working tree and git dir](https://github.com/pre-commit/pre-commit/issues/1657)

Solution:

```sh
alias dotfiles="GIT_DIR=/home/varac/.dotfiles/ GIT_WORK_TREE=/home/varac"
dotfiles pre-commit install
dotfiles pre-commit run --all
```
