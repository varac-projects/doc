# chezmoi

- [Chezmoi](https://www.chezmoi.io/)
  - "Manage your dotfiles across multiple diverse machines, securely."
  - Go
  - Solves the problem of different contents of config files for different
    devices, and supports encryption for sensitive data (like shikane display
    serials, snippets)
- [Author's dotfiles](https://github.com/twpayne/dotfiles)
- State/git dir: `~/.local/share/chezmoi`

## Install

- [Install](https://www.chezmoi.io/install/)

Arch / Manjaro:

```sh
sudo pacman -S chezmoi
```

With mise:

```sh
mise use -g chezmoi
```

## Config

- [Configuration file](https://www.chezmoi.io/reference/configuration-file/):
  `~/.config/chezmoi/chezmoi.yaml`

## Usage

### New machine

Clone dotfiles repo:

```sh
chezmoi init https://0xacab.org/varac-projects/dotfiles.git
# or: chezmoi init git@0xacab.org:varac-projects/dotfiles.git
```

Chezmoi needs to get configured with
[template user data](https://www.chezmoi.io/user-guide/templating/#template-data).
Edit `~/.config/chezmoi/chezmoi.yaml` and add all templata data variables which
chezmoi needs. Copy and paste from extisting machine.

For secrets templating, [rbw](../../security/passwords/bitwarden/cli-client.md)
needs to be configured first:

```console
chezmoi execute-template < ~/.local/share/chezmoi/home/dot_config/rbw/config.json.tmpl > ~/.config/rbw/config.json
rbw sync
rbw list
```

Now chezmoi is ready to populate yout dotfiles:

```sh
chezmoi status
chezmoi diff
chezmoi apply
```

## Templates

- [Docs: Templating](https://www.chezmoi.io/user-guide/templating/)

```sh
chezmoi add --template ~/.zshrc
```

### Secrets

- [Bitwarden functions](https://www.chezmoi.io/reference/templates/bitwarden-functions/)

Use rbw:

```ini
oauthClientId = {{ (rbw "git-credential-oauth-id").data.password }}
```
