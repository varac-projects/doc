# Linux power management

## Power adapter status

    sensors | grep -iA5 bat

## Suspend and Hibernate

* [Debugging hibernation and suspend (2007)](https://www.kernel.org/doc/Documentation/power/basic-pm-debugging.txt)

### Arch suspend and hibernate

* [Arch wiki: Power management/Suspend and hibernate](https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate)

#### Blank screen after suspend



### Hibernate on ubuntu

#### Using a swapfile on encrypted rootfs

Problem: On a fresh ubuntu 20.04 install, the swap partition is too small (~1Gb)
for a hibernate image.
So we need to remove it and use a swapfile.

* [Hibernate and resume from swap file in Ubuntu 20.04 using full disk encryption](https://rephlex.de/blog/2019/12/27/how-to-hibernate-and-resume-from-swap-file-in-ubuntu-20-04-using-full-disk-encryption/)
* [Askubuntu: Hibernate and resume from a swap file](https://askubuntu.com/a/1132154)

See `~/disk/swap.md` on how to properly size swap.

    sudo findmnt -no SOURCE,UUID -T /var/lib/swapfile

    sudo apt -y install uswsusp
    sudo dpkg-reconfigure -pmedium uswsusp
    # Answer "Yes" to continue without swap space
    # Select "/dev/disk/by-uuid/20562a02-cfa6-42e0-bb9f-5e936ea763d0" replace the UUID with the result from the previous findmnt command
    # Encrypt: "No"

...

### Handle lid close behaviour

http://ubuntuhandbook.org/index.php/2020/05/lid-close-behavior-ubuntu-20-04/
