# File managers

## Nautilus

Install:

```sh
sudo pacman -R nautilus gvfs-smb
```

## yazi

- [Website](https://yazi-rs.github.io/)
- Rust, fast !
- Neovim plugin: [yazi.nvim](https://github.com/mikavilpas/yazi.nvim?tab=readme-ov-file)

## ranger

- [GitHub](https://github.com/ranger/ranger)
- Python
- [Neovim plugin](https://github.com/kelly-lin/ranger.nvim?tab=readme-ov-file)

## nnn

- [GitHub](https://github.com/jarun/nnn/)
- [Wiki: configuration](https://github.com/jarun/nnn/wiki/Usage#configuration)
- C
- Cons:
  - In order to uses [file icons](https://github.com/jarun/nnn/wiki/Advanced-use-cases#file-icons),
    nnn needs to get compiled with a special build flag.
  - Only one contributor (=maintainer)

Install:

```sh
cd ~/projects/file-managers/nnn
gup
sudo apt-get install pkg-config libncursesw5-dev libreadline-dev
make O_NERD=1
sudo make strip install
```

### Plugins

- [nnn plugins](https://github.com/jarun/nnn/tree/master/plugins)

Plugins are at `~/.config/nnn/plugins`

### preview-tui

- [preview-tui](https://github.com/jarun/nnn/blob/master/plugins/preview-tui)

has native kitty support (in contrast to `preview-tabbed`)

- Sometimes lines are not correctly wrapped
- No video preview

Install helpers:

```sh
sudo apt install imagemagick bat ffmpegthumbnailer libmagic-dev
env GO111MODULE=on go get -u github.com/doronbehar/pistol/cmd/pistol
```
