# Digital wallets

## Android

### fwallet

- [F-Droid](https://f-droid.org/packages/business.braid.f_wallet/)
- [Gitlab](https://gitlab.com/TheOneWithTheBraid/f_wallet)
- Supports the [Deutschlandticket](https://de.wikipedia.org/wiki/Deutschlandticket)
  via importing a [`.pkpass`](https://en.wikipedia.org/wiki/PKPASS) file
