# Mimetypes

- [Arch wiki](https:/wiki.archlinux.org/title/XDG_MIME_Applications)
- [Ubuntuusers wiki Mimetypes](https:/wiki.ubuntuusers.de/MIME-Typ/)
- [Mimetype list by IANA](https:/www.iana.org/assignments/media-types/media-types.xhtml)

Mimetypes are not only determined by the global and custom mimetype mapping
files but also by the `Mimetype=` entries of all available `.desktop` files !

Query mimetype of given file:

```sh
$ mimetype ~/Howtos/accessibility.md
/home/varac/Howtos/accessibility.md: text/markdown
```

or:

```sh
$ file --mime-type image.png
image/png
```

or:

```sh
gio mime text/html
```

Query default app to open given filetype:

```console
❯ XDG_UTILS_DEBUG_LEVEL=9 xdg-mime query default image/jpeg
Checking /home/varac/.config/mimeapps.list
Checking /home/varac/.local/share/applications/defaults.list and /home/varac/.local/share/applications/mimeinfo.cache
darktable.desktop
```

Global Mimetypes: `/etc/mime.types`

User mimetype mapping files:

- `~/.config/mimeapps.list`
- `~/.local/share/applications/defaults.list`
- `~/.local/share/applications/mimeinfo.cache`

Update default app for mimetype:

Set default app in `~/.config/mimeapps.list`, then:

```sh
rm ~/.local/share/applications/mimeinfo.cache
update-desktop-database
```

## Config file precedence

- `~/.config/mimeapps.list`
- `/usr/share/applications/mimeapps.list`
- `~/.local/share/applications/defaults.list and ~/.local/share/applications/mimeinfo.cache`
- `~/.local/share/applications/defaults.list and ~/.local/share/applications/mimeinfo.cache`
- `~/.local/share/applications/defaults.list and ~/.local/share/applications/mimeinfo.cache`
- `~/.local/share/applications/defaults.list and ~/.local/share/applications/mimeinfo.cache`
- `~/.local/share/flatpak/exports/share/applications/defaults.list and ~/.local/share/flatpak/exports/share/applications/mimeinfo.cache`
- `~/.local/share/flatpak/exports/share/applications/defaults.list and ~/.local/share/flatpak/exports/share/applications/mimeinfo.cache`
- `/var/lib/flatpak/exports/share/applications/defaults.list and /var/lib/flatpak/exports/share/applications/mimeinfo.cache`
- `/var/lib/flatpak/exports/share/applications/defaults.list and /var/lib/flatpak/exports/share/applications/mimeinfo.cache`
- `/usr/local/share/applications/defaults.list and /usr/local/share/applications/mimeinfo.cache`
- `/usr/local/share/applications/defaults.list and /usr/local/share/applications/mimeinfo.cache`
- `/usr/share/applications/defaults.list and /usr/share/applications/mimeinfo.cache`
