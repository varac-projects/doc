# fzf

> fzf is a general-purpose command-line fuzzy finder.

- [GitHub](https://github.com/junegunn/fzf)

## Usage

i.e.:

```sh
cat ~/.config/notmuch/addresses | fzf
```
