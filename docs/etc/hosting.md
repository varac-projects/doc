# Hosting

## Hosting with DynDNS

Complicated. I.e., when using DynDNS, you effectivly can't host anything
on your root domain, because:

* You need to use CNAME records to point to the dynamic DNS entry
* You want a SPF TXT root record but you
  [can't have both TXT and CNAME records for the same (sub)domain](https://serverfault.com/a/834403)
  as of [RFC1034, section 3.6.2](https://www.rfc-editor.org/rfc/rfc1034)

So you need to serve services in subdomain, i.e. `www`, or matrix (see
`~/Howtos/im/matrix/federation.md`
