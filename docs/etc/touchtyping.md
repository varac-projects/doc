# Touch typing

## Apps

- [typiskt](https://github.com/budlabs/typiskt)
  touchtyping training in the terminal
- [ktouch](https://apps.kde.org/ktouch/): touch typing tutor for KDE
- [tipp10](https://gitlab.com/tipp10/tipp10): free open source touch typing software
- `klavaro`: qt lession based learning app
- `gtypist`: curses learning
- [tuxtype](https://www.tux4kids.com/tuxtyping.html): low-res game, also learning lessons
- [typespeed](https://typespeed.sourceforge.net/): curses game

### nlkt

> Nlkt is a lightweight keyboard trainer (touch-typing tutor).
> Non-linearness means that the program use dynamic, not static exercises, which
> are based on the current user's progress and mistakes.
> Exercises are built from user's mistakes and fortunes.
> Features: multiple accounts for single user, support for several layouts,
> visual keyboard state, keyboard hints.

### Unmaintained

- [speedpad](https://github.com/feurix/speedpad): ncurses tool to test, train, and increase typing speed

## Online service

### Games

- [TypeRush](https://www.typerush.com)
- [ZType](https://zty.pe/)
  - Space invaders like
  - No layout level, only English
- [FreeTypingGame.Net](https://www.freetypinggame.net/play.asp)
  - Choose rows to train
  - Only english and spanish
