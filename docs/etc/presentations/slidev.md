# Slidev

- [Website](https://sli.dev/)
- [GitHub](https://github.com/slidevjs/slidev)
- [AUR package "slidev" outdated](https://aur.archlinux.org/packages/slidev-cli)
- [Showcases](https://sli.dev/resources/showcases)
- Very active, >30k GH stars, ~250 contributors
- Mermaid support
- [Slidev prettier plugin](https://sli.dev/features/prettier-plugin)

Install globally in user homedir:

```sh
mkdir ~/.npm/packages
npm config set prefix ~/.npm/packages
npm i -g @slidev/cli
```

## Usage

- Create new presentation following the [Syntax guide](https://sli.dev/guide/syntax).
- Edit `slides.md`

Then start presentation:

```sh
slidev
```

## Themes

- [Theme Gallery](https://sli.dev/resources/theme-gallery)
