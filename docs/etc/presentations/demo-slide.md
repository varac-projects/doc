<!-- markdownlint-disable MD033 MD041 MD045 -->

# Markdown demo slide

- Showcase about special use cases

---

## Image positioning

[How can one display images side by side in a GitHub README.md?](https://stackoverflow.com/q/24319505)

---

## Images side by side with markdown syntax

![Gitlab](https://gitlab.com/uploads/-/system/group/avatar/6543/logo-extra-whitespace.png?width=96) ![Opentofu](https://www.bigdatawire.com/wp-content/uploads/2024/01/opentofu-logo.png)

## Images side by side with HTML syntax

<img src="https://gitlab.com/uploads/-/system/group/avatar/6543/logo-extra-whitespace.png?width=96" /> <img src="https://www.bigdatawire.com/wp-content/uploads/2024/01/opentofu-logo.png" /> <!-- markdownlint-disable-line MD013 -->

### Images in tables

|                                                                                                     |                                                                                       |
| --------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| ![Gitlab](https://gitlab.com/uploads/-/system/group/avatar/6543/logo-extra-whitespace.png?width=96) | ![Opentofu](https://www.bigdatawire.com/wp-content/uploads/2024/01/opentofu-logo.png) |

- Downside: Displays a visible table with Headers around the images

---

### Images in floating HTML

<p float="left">
  <img src="https://gitlab.com/uploads/-/system/group/avatar/6543/logo-extra-whitespace.png?width=96" />
  <img src="https://www.bigdatawire.com/wp-content/uploads/2024/01/opentofu-logo.png" />
</p>

---

### Images in div container

<div class="mx-auto mt-3">
<img src="https://gitlab.com/uploads/-/system/group/avatar/6543/logo-extra-whitespace.png?width=96" class="float-left mr-5" width="20%"> <!-- markdownlint-disable-line MD013 -->

<img src="https://www.bigdatawire.com/wp-content/uploads/2024/01/opentofu-logo.png" class="" width="20%">
</div>
