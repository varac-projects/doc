# Presentations

Existing presentations: `~/projects/presentations`

## How to hold a presentation

- Ask someone to assist, i.e.
  - Track time
  - Write down issues, questions, etc
- Use wireless presenter
- Disable applications the do notifications (gnome-calendar, thunderbird etc)
- Disable custom starship prompt
- Start chromium in incognito mode
- Enable slide presentation mode / slide notes
- Move presentation to beamer, leave notes window on desktop

## Tools

- [Anarcat 2020 blog post: Presentation tools](https://anarc.at/blog/2020-09-30-presentation-tools/)
- [presentation frameworks supported by decktape](https://github.com/astefanutti/decktape#decktape)

### Slidev

- [./slidev.md](./slidev.md)

### Hedgedoc

- [Website](https://hedgedoc.org/)
- Integrates [reveal.js](https://github.com/hakimel/reveal.js/)
- [Docs](https://docs.hedgedoc.org/)
  - [Slide options](https://docs.hedgedoc.org/references/slide-options/)
- [Slide example](https://demo.hedgedoc.org/slide-example?both)
- [Hedgedoc Markup features example](https://demo.hedgedoc.org/features?both)
- [Demo instance](https://hedgedoc.dev/)
  **Warning**: All slides will get purged after 24h!

Issues:

- [Slide seperator is not recognized with a space at the end](https://github.com/hedgedoc/hedgedoc/issues/5586)
  - Fixed in unreleased v2

### Marp

- [Website](https://marp.app/)
- [GitHub](https://github.com/marp-team/marp)
- Not so active, 7.5K GH stars, 12 contributors
- Typescript
- [Docs](https://marpit.marp.app/)
- [Online-Editor](https://web.marp.app/)
- Not supported by Decktape, but has built-in PDF exporter
- [markdown-preview-nvim FR: Adding Marp for slide editing](https://github.com/iamcco/markdown-preview.nvim/issues/245)

Example: `~/projects/presentations/marp`

Install [marp-cli](https://github.com/marp-team/marp-cli):

```sh
npm install -g @marp-team/marp-cli
```

Generate html and watch for changes:

```sh
 marp -w marp-example.md
```

#### Marp issues

- watch mode doesn't detect file changes after a while
- [inline images resize by percentage](https://github.com/marp-team/marpit/issues/193)
  not possible
- No [mermaid support](https://github.com/marp-team/marp-core/issues/139)

### reveal-md

- [GitHub](https://github.com/webpro/reveal-md)
- Not so active, 3.7K GH stars, 75 contributors
- [markdown-preview.nvim FR: add support for reveal.md](https://github.com/iamcco/markdown-preview.nvim/issues/29)

## HTML based tools

- [reveal.js](https://github.com/hakimel/reveal.js/)
  - [markdown-preview.nvim FR: add support for revealjs markdown presentations](https://github.com/iamcco/markdown-preview.nvim/issues/467)
- [inpress.js](https://github.com/impress/impress.js/)
- [inspire.js](https://inspirejs.org/)

## Console based presentation tools

- [nerd-slides-vim-plugin](https://github.com/roymanigley/nerd-slides-vim-plugin)
  "vim plugin to keep your presentations/slides nerdy"

## Unmaintained presentation frameworks

### Markdown

- [Backslide](https://github.com/sinedied/backslide)
- [Bespoke](https://github.com/bespokejs/bespoke)
- [Deck.js](https://github.com/imakewebthings/deck.js)
- [Dzslides](https://github.com/paulrouget/dzslides)
- [Fusuma](https://hiroppy.github.io/fusuma/)
- [Landslide](https://github.com/adamzap/landslide) (rendering command needed)
- [Marckdeck](https://github.com/divshot/markdeck)
- [Mdx-deck](https://github.com/jxnblk/mdx-deck)
- [Nuedeck](https://github.com/twitwi/nuedeck/)
- [P_slides](https://github.com/munen/p_slides)
- [Reveal-ck](https://github.com/jedcn/reveal-ck)
- [Reveal-jekyll](https://github.com/tasmo/reveal-jekyll)
- [Slideshow](https://github.com/slideshow-s9/slideshow)
- [Slidy2](https://www.w3.org/Talks/Tools/Slidy2/)
- [WebSlides](https://github.com/webslides/WebSlides)

#### Remark

- [Website](https://github.com/gnab/remark)
- One Html file, no generate command needed.
- Cons: Still a nasty mix of Html and inline-markdown
- Example: `~/thoughtworks/cdp/presentation`

##### Issues

Scale images to fit screen:

Put the following snippet in the `<syle>` section of the slide:

```css
img {
  max-width: 100%;
}
.remark-slide-content {
  background-size: contain;
}
```

###### export to html

```sh
cd ~/thoughtworks/cdp/presentation
sed -n '/^class:/,/<\/textarea/p' cdp2015.html | head -n -1 | tail -n +3 > cdp.md
pandoc --self-contained  -t revealjs -s cdp.md -o cdp2015-thorsten-hinrichsmeyer-exported.html
```

### html

- [Flowtime](https://github.com/marcolago/flowtime.js/blob/master/documentation.md)
- [Shower](https://github.com/shower/shower)
- [WebSlides](https://github.com/webslides/webslides/)

### etc

- [bespoke.js](https://github.com/bespokejs/bespoke)

## Decktape

- [Github](https://github.com/astefanutti/decktape)
  - PDF exporter for HTML presentation frameworks
  - Supports Bespoke.js, deck.js, DZSlides, Flowtime.js,
    impress.js, Inspire.js, NueDeck, remark, reveal.js,
    RISE, Shower, Slidy, WebSlides
