# Fonts

- [Ubuntuusers wiki: Schriften](https://wiki.ubuntuusers.de/Schriften/)
  - includes font locations

Default user location: `~/.local/share/fonts`

## List installed Fonts

    fc-list

## Update fonts cache

    sudo fc-cache

## Install new font

## External font sources

<https://wiki.ubuntuusers.de/Schriften/#Extern>

### Nerdfonts

- [getnf](https://github.com/ronniedroid/getnf)
  A better way to install NerdFonts
- Important: [Starship required Fira Code Nerd Font](https://starship.rs/guide/#%F0%9F%9A%80-installation)
  which **needs** to get installed manually or using `getnf`, the debian package
  `fonts-firacode` doesn't include the right font.
