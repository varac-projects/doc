# Linux time and date

## Convert epoch to date

    date -d @'1475584168'

## traditional syslog time

    date -d @'1475584168' +'%b %e %T'

## Whole (traditinal) syslog header

    echo "$(date -d @'1475584168' +'%b %e %T') $(hostname) programname:"

## Fix date format

i.e. `2021/01/20` -» `2021-01-20`

    sed -i -E 's,([0-9]{4})/([0-9]{2})/([0-9]{2}),\1-\2-\3,g' journals/*

## libfaketime

- [GitHub](https://github.com/wolfcw/libfaketime)
- [FR: freeze Browser time](https://github.com/wolfcw/libfaketime/issues/443)
- Beware: Wall clock continues running from fake starting point

Examples:

    LD_PRELOAD=/usr/lib/faketime/libfaketime.so.1 FAKETIME="-15d" chromium
    FAKETIME_DONT_RESET=1 faketime '2024-03-01 00:00:00' node generate-pdf.js


### libfaketime in container


    $ docker run --rm -it -e FAKETIME="2023-12-31 23:59:00" docker.io/skto/faketime:v90 date
    Wed Nov 29 10:03:08 UTC 2023
