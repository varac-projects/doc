# QR codes

- [QR code standards for different formats](https://github.com/zxing/zxing/wiki/Barcode-Contents)

## Scan / decode QR codes

- [cobang](https://github.com/hongquan/CoBang)

  - [cobang AUR package](https://aur.archlinux.org/packages/cobang)

- [zbar](https://github.com/mchehab/zbar)
  - `zbarcam` or `zbarcam-gtk` from the [zbar arch package](https://archlinux.org/packages/extra/x86_64/zbar/)
  - Not a nice UI: Decoded QR codes will only show up in the console
- [decoder](https://gitlab.gnome.org/World/decoder/)
  - Gnome app to scan and Generate QR Codes
  - Activly maintained
  - Orphaned [decoder-git AUR package](https://aur.archlinux.org/packages/decoder-git)

## Produce ascii/UTF8 qr codes

- [List of QR code generators on Linux](https://linuxconfig.org/list-of-qr-code-generators-on-linux)

### zint

- [Website](https://www.zint.org.uk/)
- [GitHub](https://github.com/zint/zint)
- [Arch package](https://archlinux.org/packages/extra/x86_64/zint/)
- Both cli and GUI tool
- Online demo at [barcode-generator.org](https://www.barcode-generator.org/)

### qrencode cli tool

- [GitHub](https://github.com/fukuchi/libqrencode)

Generate a terminal based text QR code:

```sh
qrencode -t utf8 password
echo "password" | qrencode -t utf8
```

### decoder

See above

### Outdated

- [qr-code-generator-desktop](https://github.com/studioLaCosaNostra/qr-code-generator-desktop)
  - Last release 2020

## Produce WLAN qr codes

- [de-facto Wifi QR code standard](https://github.com/zxing/zxing/wiki/Barcode-Contents#wi-fi-network-config-android-ios-11)
- [Online QR-Code generator](https://www.qrcode-generator.de/solutions/wifi-qr-code/)

### with qrencode

```sh
qrencode "WIFI:T:WPA;S:My_Network;P:My_very_secure_Password;;" -o wifi_login.png
qrencode "WIFI:T:WPA;S:casita;P:$(rbw get 'wlan|whg1.3|casita');;" -t utf8
```

Script:

```sh
~/bin/wifi-to-qr-pdf.sh 90er
```

### wifi-qr

<https://github.com/kokoye2007/wifi-qr>

```sh
sudo apt install wifi-qr
```

Produce terminal QR code from current connected wifi: `wifi-qr`

Unfortunatly not scriptable.
