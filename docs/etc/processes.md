# Find which app processes are running on a remote server

    ps aux | grep -Ev '(\]$| sshd: |sshd -D$|systemd-journald$|systemd-udevd|systemd-logind$| /lib/systemd/systemd --user$| /sbin/init| /usr/bin/dbus-daemon |cron -f$|rsyslogd -n$| /sbin/agetty | /usr/sbin/ntpd |/usr/lib/postfix/sbin/master|qmgr -l -t unix -u$|pickup -l -t unix -u -c$|tlsmgr -l -t unix -u -c$| /usr/sbin/dnsmasq | ps aux$| /usr/lib/policykit-1/polkitd --no-debug$| /usr/lib/packagekit/packagekitd*| -bash$|\(sd-pam\)$|/usr/sbin/irqbalance --foreground$)'
