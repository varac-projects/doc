# Word cloud generators

## Python Wordcloud

- [Docs](http://amueller.github.io/word_cloud/)
- [Github](https://github.com/amueller/word_cloud)

### Install

```sh
sudo apt install python3-wordcloud
```

or

```sh
pipx install wordcloud
```

### Usage

Add `-` to default regex to include words like `high-availability`

```sh
wordcloud_cli
  --imagefile IT_Kenntnisse_Hinrichsmeyer.png
  --text kenntnisse-cloud.txt
  --mask thorsten-outline.white.png
  --background white
  --contour_width 1
  --height 800
  --width 600
  --regex "(?:\w[\w'-]+)"
```

- [PIL / Pillow color reference](https://pillow.readthedocs.io/en/latest/reference/ImageColor.html)
  for `--background` option

- [mathplotlib colorcode reference](https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html)
  for `--colormap` option

### Issues

- [SVG images don't have any contour lines](https://github.com/amueller/word_cloud/issues/694)
- [Add option to create SVG image with wordcloud_cli](https://github.com/amueller/word_cloud/issues/693)

## Online generators

- [wortwolken.com](https://www.wortwolken.com/)
- Now requires a CSV file
