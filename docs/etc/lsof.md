List procs that hog resource usage by having too many open files:

    lsof -l | awk '{print $2} {print $1} {print $11}' | sort | uniq -c | sort -n
