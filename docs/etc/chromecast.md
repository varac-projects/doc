# Chromecast

* [How to Cast Video from Ubuntu to Chromecast](https://vitux.com/how-to-cast-video-from-ubuntu-to-chromecast/)

## Clients

* [catt](https://github.com/skorokithakis/catt/)
  Cast All The Things allows you to send videos from many, many online sources to your Chromecast.

### mkchromecast

* [Github](https://github.com/muammar/mkchromecast)
* [mkchromecast-git AUR package](https://aur.archlinux.org/packages/mkchromecast-git)

Issues:

### VLC

### Chromium

### Firefox fx_cast extenstion

* [Website](https://hensm.github.io/fx_cast/)
  * [fx_cast-bin AUR package](https://aur.archlinux.org/packages/fx_cast-bin)
  * Stale [fx_cast AUR package](https://aur.archlinux.org/packages/fx_cast)

[Install](https://github.com/hensm/fx_cast#installing):

* Configure [local hostname resolution](https://wiki.archlinux.org/index.php/avahi#Hostname_resolution)

## Chromecast alternatives

* [17 Best Chromecast Alternatives in 2023](https://www.purevpn.com/blog/best-chromecast-alternatives/)
* [picast](https://codeberg.org/miurahr/picast/)
  * Last release 2021
* [RTSP](https://en.wikipedia.org/wiki/Real_Time_Streaming_Protocol)
* [desktopcast](https://github.com/seijikun/desktopcast)
  Casts to any UPNP/DLNA device
* [Raspicast](https://www.instructables.com/Raspberry-Pi-As-Chromecast-Alternative-Raspicast/)

### Miracast

* [Arch wiki](https://wiki.archlinux.org/title/list_of_applications#Miracast)
* [gnome-network-displays](https://gitlab.gnome.org/GNOME/gnome-network-displays)
  * [AUR package](https://aur.archlinux.org/packages/gnome-network-displays)
* [miraclecast](https://github.com/albfan/miraclecast)
  * [AUR package](https://aur.archlinux.org/packages/miraclecast-git)
* [LG Miracast docs](https://eguide.lgappstv.com/manual/gb/12005_3.html)
