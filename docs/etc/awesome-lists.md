# Awesome lists

- [Selfhosting](https://github.com/awesome-selfhosted/awesome-selfhosted)
- [Containers/Kubernetes](../container/awesome-container-lists.md)
- [libresh/awesome-librehosters](https://github.com/libresh/awesome-librehosters)
  - A list of nice hosting providers
- [Awesome-GitOps](https://github.com/weaveworks/awesome-gitops)
