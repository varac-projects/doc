# UMTS


## SMS

### Recieve

    $ gammu getallsms

    Location 100001, folder "Inbox", phone memory, Inbox folder
    SMS message
    SMSC number          : "+49..."
    Sent                 : Mon 28 Oct 2013 12:58:26 AM  +0100
    Coding               : Default GSM alphabet (no compression)
    Remote number        : "22011"
    Status               : UnRead

    Dear...

    1 SMS parts in 1 SMS sequences


### Recieve

    echo 'Hello' | gammu --sendsms TEXT +49...
