# cli table tools

## From csv

### miller

> like awk, sed, cut, join, and sort for name-indexed data such as CSV, TSV, and tabular JSON

- [Docs](https://miller.readthedocs.io)
- [GitHub](https://github.com/johnkerl/miller)
  - Actively maintained, >8k commits
- Can output Markdown tables from CSV
- [Arch package](https://archlinux.org/packages/extra/x86_64/miller/)
- Example: `cat long.csv | mlr --csv -o md cat`

### CsvToMarkdownTable

- [GitHub](https://github.com/donatj/CsvToMarkdownTable)
- NodeJS
- Actively maintained, ~250 commits
- Not packages in Arch
- Very few cli options
- No [Option to remove quotes from CSV](https://github.com/donatj/CsvToMarkdownTable/issues/118)
- Properly escapes pipe symbols in csv
- Example: `cat simple.csv| npx csv-to-markdown-table --delim ',' --headers`

### cvs2md

- [GitHub](https://github.com/lzakharov/csv2md)
- [AUR package](https://aur.archlinux.org/packages/csv2md-git)
- converts CSV files into Markdown tables
- Python, last commit 2024-03
- Packaged as AUR package `csv2md-git`
- No option to specify the max width of columns
- Properly removes quotes from csv
- Example: `cat simple.csv | csv2md`

#### Issues

- [Escape pipe symbol in items](https://github.com/lzakharov/csv2md/issues/21)
  - Fixed in v1.4.0

### other

- [table](https://git.sr.ht/~strahinja/table)
  - Packages as AUR package `table`
  - Can't produce markdown
- [csvtotable](https://github.com/vividvilla/csvtotable)
  Last commit 2021
- [csvtomd](https://github.com/mplewis/csvtomd)
  - Last commit 2018

## From Markdown

- [tablemark](https://github.com/haltcase/tablemark) NodeJS module
  Last commit 2021-10
  - [tablemark-cli](https://github.com/haltcase/tablemark-cli) NodeJS cli
    Last commit 2022-04
