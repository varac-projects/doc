# Maps

## Map formats

- [mbtiles](https://wiki.openstreetmap.org/wiki/MBTiles)
- [versatiles](https://github.com/versatiles-org/versatiles-spec)
- [pmtiles](https://github.com/protomaps/PMTiles)

## Viewers

- [mbview-rs](https://github.com/Akylas/mbview-rs)
  No installable binary for download, no Arch package
- [maptiler](https://www.maptiler.com/engine/download/)
  Proprietary, sign-up before download
- [mapbox/mbview](https://github.com/mapbox/mbview)
  Stale, last commit 2021
- [MapLibre GL JS](https://maplibre.org/maplibre-gl-js/docs/)
  TypeScript library that uses WebGL to render interactive maps from vector tiles in a browser
- [martin](https://github.com/maplibre/martin)
  By the co-founder of [MapLibre](https://maplibre.org/)

## Versatiles

- [Docs](https://github.com/versatiles-org/versatiles-documentation)
- [versatiles-rs](https://github.com/versatiles-org/versatiles-rs)

### Download map tiles

- [download.versatiles.org](https://download.versatiles.org/)
  Only hosts whole planet files
- Public versatiles bucket also hosts Europe, Germany, BBW etc.: `gcloud storage ls gs://versatiles/`
- [Partial download](https://github.com/versatiles-org/versatiles-documentation/blob/main/guides/download_tiles.md#partial-download)

Example partial download (downloads only a small part of Hamburg):

    versatiles convert --bbox-border 3 \
      --bbox "53.56308,9.93112,53.54667,9.96193" \
      https://download.versatiles.org/planet-latest.versatiles ~/projects/versatiles/data/altona-$(date +"%F").versatiles

### Install

    eget versatiles-org/versatiles-rs

### Usage

    mkdir ~/projects/versatiles
    cd ~/projects/versatiles
    git clone https://github.com/versatiles-org/versatiles-frontend
    cd versatiles-frontend
    npm install
    npm run dist

    versatiles server -s ~/projects/versatiles/versatiles-frontend/dist/frontend.br.tar ~/projects/versatiles/data/germany-20230226.versatiles

### Issues

- When using a smaller map section you need to find the region to zoom in, see
  [Show bbox and min/max zoom level in preview](https://github.com/versatiles-org/versatiles-frontend/issues/16)
  Solution: Add the bbox coordinated and zoom factor to the URL, i.e. for
  - [Germany](http://localhost:8080/map.html?url=/tiles/germany-20230604/#4.62/51.35/19.22)
  - [Hamburg](http://localhost:8080/map.html?url=/tiles/germany-20230604/#10.79/53.5604/10.0034)
