# Caldav / carddav

- [Comparison of CalDAV and CardDAV implementations](https://en.wikipedia.org/wiki/Comparison_of_CalDAV_and_CardDAV_implementations)
  `/home/varac/projects/caldav`

## GUI Calendar clients

### gnome-calendar

Issues:

- [No timezone support](https://gitlab.gnome.org/GNOME/gnome-calendar/issues/2)
  for creating events
- [No native caldav/carddav support](https://gitlab.gnome.org/GNOME/gnome-online-accounts/-/issues/1)

### Direct caldav support only

- `evolution`: Big, monolithic application.
  - [Not modular](https://gitlab.gnome.org/GNOME/evolution/issues/231)
    still the best choice for timezone support and complex recurring rules.
- [Nextcloud calendar](https://apps.nextcloud.com/apps/calendar)
  - [Support webcal http auth & store credentials](https://github.com/nextcloud/calendar/issues/70)
  - [good recurrency support for event](https://github.com/nextcloud/calendar/issues/10)
  - No offline support, obviously

### Unmaintained

- [california](https://wiki.gnome.org/Apps/California): [Last tag 0.4.0 from 2014](https://gitlab.gnome.org/Archive/california/tags)
- `Orage`: Not usable, horrible UI, was taken out of
  [xfce 19.04](https://bluesabre.org/2019/04/18/xubuntu-19-04-the-exhaustive-update/)

#### cadaver

- [Website](http://www.webdav.org/cadaver/)
- Not updated since 2009 :/

- [Backup your caldav calendar with cadaver](https://uriesk.wordpress.com/2015/02/13/backup-your-caldav-calendar-with-cadaver)

```sh
export URL='https://cloud.moewe-altonah.de/remote.php/webdav'
mkdir /tmp/calendar
cd /tmp/calendar
cadaver $URL
ls
…
```

## Command line tools

### vdirsyncer

- [Website](https://vdirsyncer.pimutils.org/en/stable/tutorials/index.html#client-applications)
- Config and data dir: `~/.vdirsyncer/`

### Khal

- Nice ncurses UI, but [lacking timezone support for creating events](https://github.com/pimutils/khal/issues/635)
  and [complex recurring rules](https://github.com/pimutils/khal/issues/663).

## Task clients

- [Endeavour](https://gitlab.gnome.org/World/Endeavour): ex gnome-tasks
- [Nextcloud tasks](https://apps.nextcloud.com/apps/tasks)

## Contacts / carddav

- [Nextcloud contacts](https://apps.nextcloud.com/apps/contacts)
- [GNOME Contacts](https://gitlab.gnome.org/GNOME/gnome-contacts)
