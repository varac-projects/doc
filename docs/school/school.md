# School tools / apps

- [Die digitale Bildung unter der Lupe: Eine Analyse von Schul- und Lern-Apps](https://www.kuketz-blog.de/die-digitale-bildung-unter-der-lupe-eine-analyse-von-schul-und-lern-apps/)

## Units

- [Play store: Untis mobile](https://play.google.com/store/apps/details?id=com.grupet.web.app)
- [BetterUnits](https://github.com/SapuSeven/BetterUntis)
  - An alternative mobile client for the Untis timetable system.
  - [F-Droid](https://f-droid.org/de/packages/com.sapuseven.untis/)
  - Last release 2023-11
- [Stundenplan](https://github.com/Lasslos/your_schedule)
  - An alternative mobile client for Untis Timetable,
    adding features such as filtering out certain classes
  - [IzzySoft F-Droid repo](https://apt.izzysoft.de/fdroid/index/apk/eu.laslo_hauschild.your_schedule)
