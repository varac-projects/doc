reStructuredText
================

-  `Official RST website with
   examples <https://docutils.sourceforge.io/rst.html>`__

   -  `reStructuredText Markup
      Specification <https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html>`__

-  `writethedocs rst
   guide <https://www.writethedocs.org/guide/writing/reStructuredText/>`__
-  `cheatsheet
   PDF <https://raw.githubusercontent.com/ralsina/rst-cheatsheet/master/rst-cheatsheet.pdf>`__

Vim integrarions
----------------

-  `vim-reSTfold <https://github.com/landonb/vim-reSTfold>`__: RST
   section folder for Vim
-  `riv.vim <https://github.com/gu-fan/riv.vim>`__: Notes and Wiki with
   rst.
