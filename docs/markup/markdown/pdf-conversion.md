# Generate PDF from markdown

- [convert markdown to pdf in commandline](https://unix.stackexchange.com/questions/710817/convert-markdown-to-pdf-in-commandline)
- [How Can I Convert Github-Flavored Markdown To A PDF](https://superuser.com/questions/689056/how-can-i-convert-github-flavored-markdown-to-a-pdf)

## md-to-pdf

Best solution so far:

- [md-to-pdf](https://github.com/simonhaenisch/md-to-pdf)
- Nodejs/Typescript, uses [Marked](https://github.com/markedjs/marked) and [Puppeteer](https://github.com/puppeteer/puppeteer)
- Last commit 2023
- No arch package, install: `npm i -g md-to-pdf`
- Works well !

Usage:

```sh
md-to-pdf README.md --pdf-options '{ "format": "A4", "margin": "20mm" }'
```

## Custom md2pdf.sh script

~/bin/md2pdf.sh

> Custom script which generates a PDF from a/mulitple markdown files
> using pandoc

```sh
md2pdf.sh list.md -o list.pdf
```

## Easypdf

- [GitHub](https://github.com/blackfly19/Easypdf)
- Last commit 2023, only 5 commits

## Using pandoc

- [Customizing pandoc](https://learnbyexample.github.io/tutorial/ebook-generation/customizing-pandoc)
- [markdown2pdf_pandoc](https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc)
- [pandoc Manual: Creating a PDF](https://pandoc.org/MANUAL.html#creating-a-pdf)

### weasyprint engine

- Lightweight
- Works well

### pdfroff

- Doesn't display checkboxes
  (`troff: <standard input>:135: warning: can't find special character 'u2610'`)

### wkhtmltopdf engine

- Ruby, pulls in ~500MB of dependencies
- Deprecated [Github repo](https://github.com/wkhtmltopdf/wkhtmltopdf) is
  archived

#### xelatex pdf-engine

- Latex, pulls in tons of dependencies

Recommended, has more options than `texlive`.
Setup:

```sh
sudo apt install pandoc texlive-xetex texlive-fonts-recommended
```

Config (default fonts, formatting etc):

```sh
~/projects/markup/markdown/pandoc/pdf-defaults.tex
```

Generate:

```sh
pandoc -H ~/projects/markup/markdown/pandoc/pdf-defaults.tex \
  --pdf-engine=xelatex -f gfm R.md -o R.pdf
```

include highlighting:

```sh
pandoc -H ~/projects/markup/markdown/pandoc/pdf-defaults.tex \
  --pdf-engine=xelatex --highlight-style python test.md -o test.pdf
```

#### Using texlive

```sh
sudo apt-get install pandoc texlive-latex-extra

pandoc -f gfm ubuntu_16.10.md -o ubuntu_16.10.pdf
```

#### Syntax highlighting

- [pandoc highlighting style examples](https://stackoverflow.com/a/47876166)
- [Pandoc Syntax Highlighting Examples](https://www.garrickadenbuie.com/blog/pandoc-syntax-highlighting-examples)

List available styles:

```sh
pandoc --list-highlight-styles

pandoc --highlight-style=zenburn R.md -o R.pdf
```

Using a template together with .tex defaults:

```sh
pandoc ~/projects/markup/markdown/highlighting.md  -o /tmp/highlighting.pdf \
  --from markdown --template eisvogel --listings \
  -H ~/projects/markup/markdown/pandoc/pdf-bulletpoints.tex
```

##### Syntax highlighting issues

- [Syntax highlight style ignored when no language provided instead of using default](https://github.com/jgm/pandoc/issues/6104)

### luamark

- [Github](https://github.com/jgm/lunamark)
- From the `pandoc` author
- Not packaged in Arch

> Currently HTML, dzslides (HTML5 slides), Docbook, ConTeXt,
> LaTeX, and Groff man are the supported output formats

## Unmaintained tools

- [md2pdf](https://github.com/walwe/md2pdf)
  - Python, based on `wkhtmltopdf`
  - Last commit 2022-01
- [markdown-pdf](https://github.com/alanshaw/markdown-pdf):
  Last commit 2019-08 (as of 2020-04)
- [gimli](https://github.com/walle/gimli) - Development stalled since 2017
