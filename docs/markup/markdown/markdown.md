# Markdown

- [Google Markdown style guide](https://google.github.io/styleguide/docguide/style.html)

## Markdown syntax

- [Markdownguide](https://www.markdownguide.org)
  - [Tools](https://www.markdownguide.org/tools)
  - [Extended syntax](https://www.markdownguide.org/extended-syntax/#overview)
- [Github Flavored Markdown](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

### Images

Basic usage (no scaling options):

```markdown
![Opentofu](./assets/opentofu.png)
```

Use html:

```html
<img src="./assets/gitlab.png" width="300" height="200" />
```

#### Image positioning

[How can one display images side by side in a GitHub README.md?](https://stackoverflow.com/q/24319505)

see [presentations/demo-slide.md](../../etc/presentations/demo-slide.md)

## Prettyfi markdown

- [glow](https://github.com/charmbracelet/glow)
  - Render markdown on the CLI, with pizzazz! 💅🏻
  - Go
  - [Doesn't handle wide tables well](https://github.com/charmbracelet/glow/issues/531)
- [mdcat](https://github.com/swsnr/mdcat)
  - Rust
  - > 1k commits
  - [Renders images in terminal](https://github.com/linebender/resvg#svg-support) using [resvg](https://github.com/linebender/resvg)
    - [AUR package](https://aur.archlinux.org/packages/resvg)
      has a lot of KDE dependencies,
      so `eget linebender/resvg` is preferred
  - Markdown table support added in [v2.4.0](https://github.com/swsnr/mdcat/releases/tag/mdcat-2.4.0)
- [bat](https://github.com/sharkdp/bat)
  - Beware: [Markdown Tables aren't highlighted](https://github.com/sharkdp/bat/issues/1752)

## Collaborative markdown editing

Best workflow for collaborative markdown pages:

- Use [Hedgedoc demo](https://demo.hedgedoc.org), edit online, upload images

- Download markdown file

- Download and locally reference all images using
  [Markdown articles tool](https://github.com/artiomn/markdown_articles_tool)

  markdown_tool.py Openappstack\\ demoweek\\ blogpost.md

## Hedgedoc (formerly codiMD / hackMD)

- [hedgedoc docs](https://docs.hedgedoc.org/)
  - [HedgeDoc Flavored Markdown](https://docs.hedgedoc.org/references/hfm/)
  - [Slide options](https://docs.hedgedoc.org/references/slide-options/)
- [hedgedoc API](https://docs.hedgedoc.org/dev/api/)
- [hedgedoc Greenhost instance](https://hedgedoc.greenhost.net)

### Heddoc cli tool

- [GitHub](https://github.com/hedgedoc/cli)

Usage:

```sh
export HEDGEDOC_SERVER='https://demo.hedgedoc.org'
hedgedoc export --md MXlAkGw4RgeCd6hlPWcRIQ /tmp/Openappstack.md
```

### markdown-articles-tool

[markdown_articles_tool](https://github.com/artiomn/markdown_articles_tool)

> Parse markdown article, download images and replace image URL's to local paths

```sh
markdown_tool.py --images-publicpath '/images' Openappstack.md
```

## Styles

- [CommonMark](https://commonmark.org/)

see `~/projects/markup/markdown/syntax.md` as example

## Syntax

- [Nest code in lists](https://meta.stackexchange.com/a/3793)
- [Nest code block in code blocks](https://weblog.west-wind.com/posts/2022/Feb/16/Escaping-Markdown-Code-Snippets-and-Inline-Code-as-Markdown)

### Code block syntax highlighting

- [github-linguist: Supported languages](https://github.com/github-linguist/linguist/blob/master/lib/linguist/languages.yml)
- [highlight.js: Supported languages](https://github.com/highlightjs/highlight.js/blob/main/SUPPORTED_LANGUAGES.md)
- [Treesitter supported languages](https://github.com/nvim-treesitter/nvim-treesitter?tab=readme-ov-file#supported-languages)

Show available `github-linguist` languages and files extensions:

```sh
curl --no-progress-meter \
  https://raw.githubusercontent.com/github-linguist/linguist/refs/heads/main/lib/linguist/languages.yml\
  | yq 'to_entries | .[] | .key + ": " + (.value.extensions | @csv)'
```

## Markdown server

### Madness

- [madness](http://madness.dannyb.co/) - Instant Markdown Server
- [madness container image](https://hub.docker.com/r/dannyben/madness)

Start madness serving current directory:

```sh
docker run --rm -it -v $PWD:/docs -p 3000:3000 dannyben/madness
```

or alias

```sh
madness
```

## Other tools

- [glow](https://github.com/charmbracelet/glow):
  Render markdown on the CLI
- For neovim markdown plugins see [neovim markdown plugins](../../editors/neovim/plugins/markdown.md)
  - [glow.nvim](https://github.com/ellisonleao/glow.nvim)
    A (text-based) markdown preview directly in your neovim
  - [glow-hover](https://github.com/JASONews/glow-hover.nvim)
    Leverage glow to display LSP hover responses.

## Collapsilbe section

```markdown
<details>
<summary>Click me</summary>

### Heading

1. Foo
2. Bar
   - Baz
   - Qux

### Some Code

    function logSomething(something) {
      console.log("Something", something);
    }

</details>
```
