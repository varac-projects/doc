# Markdown linters and formatters

## Prettier

- [coding/quality/linting.md](../../coding/quality/linting.md)
- [Default Lazyvim markdown formatter](https://www.lazyvim.org/extras/lang/markdown#conformnvim-optional)
- also formats markdown code
- Plugins
  - [Slidev prettier plugin](https://sli.dev/features/prettier-plugin)
  - [markdown-code-fences](https://github.com/rstacruz/prettier-plugin-markdown-code-fences)
    - Doesn't work:
      [TypeError: prettierMarkdown.parsers.markdown.preprocess is not a function](https://github.com/rstacruz/prettier-plugin-markdown-code-fences/issues/1)

## mdformat

- [GitHub](https://github.com/executablebooks/mdformat)
  - Development stalled, last commit/tag 2023-08
- Supports [plugins](https://github.com/executablebooks/mdformat?tab=readme-ov-file#installing)
  - Without the [mdformat-gfm plugin](https://github.com/hukkin/mdformat-gfm),
    [Markdown checkboxes will get re-formatted wrongly](https://github.com/executablebooks/mdformat/issues/430)
- [nvim/mason support](https://github.com/williamboman/mason.nvim/issues/1152)
  - For proper GFM support, don't use Mason until [mdformat-gfm gets bundled in Mason](https://github.com/williamboman/mason.nvim/issues/1152#issuecomment-2067596772)
    and install the `mdformat` and `mdformat-gfm-git` system-wise

Install:

```sh
  sudo pacman -S mdformat
  pamac install mdformat-gfm-git
```

Issues:

- No [Options to disable/enable rules](https://github.com/executablebooks/mdformat/issues/431)
- [FR: Configure default language for fenced code blocks](https://github.com/executablebooks/mdformat/issues/438)
- [mdformat reformats --- to \_\_\_\_\_\_\_, destroying slide seperators](https://github.com/executablebooks/mdformat/issues/431#issuecomment-2037054102)

  - Installng [mdformat-frontmatter](https://github.com/butler54/mdformat-frontmatter)
    will only fix this for the frontmatter, but not for slide seperators

## DavidAnson/markdownlint-cli2

- [GitHub](https://github.com/DavidAnson/markdownlint-cli2)
- NodeJS, based on the NodeJS [DavidAnson/markdownlint](https://github.com/DavidAnson/markdownlint)
  library
- [AUR package: markdownlint-cli2](https://archlinux.org/packages/extra/any/markdownlint-cli2/)
- [David Ansons Blog: Why markdownlint-cli2 ?](https://dlaa.me/blog/post/markdownlintcli2)
- Default linter installed by [LayzVim markdown extra](https://www.lazyvim.org/extras/lang/markdown)
- [Configuration](https://github.com/DavidAnson/markdownlint-cli2?tab=readme-ov-file#configuration)
  - Doesn't walk up the directory tree to find a config file like
    `markdownlint-cli`
    - (No) [Support configuration file in parent directory of CWD](https://github.com/DavidAnson/markdownlint-cli2/issues/53)
- [pre-commit support](https://github.com/DavidAnson/markdownlint-cli2?tab=readme-ov-file#pre-commit)

## Igorshubovych/markdownlint-cli

- [GitHub](https://github.com/igorshubovych/markdownlint-cli)
- NodeJS, based on the NodeJS [DavidAnson/markdownlint](https://github.com/DavidAnson/markdownlint)
  library
- [AUR package: markdownlint-cli](https://archlinux.org/packages/extra/any/markdownlint-cli/)
- [Configuration](https://github.com/igorshubovych/markdownlint-cli?tab=readme-ov-file#configuration)
  - Config files: markdownlint-cli looks for these files in the
    current directory:
    - `.markdownlint.jsonc`
    - `.markdownlint.json`
    - `.markdownlint.yaml`
    - `.markdownlint.yml`
    - or for the file `.markdownlintrc` in the current or all parent folders
  - [config format](https://github.com/DavidAnson/markdownlint#optionsconfig)
  - [Rules](https://github.com/DavidAnson/markdownlint/blob/main/doc/Rules.md)
- [pre-commit support](https://github.com/igorshubovych/markdownlint-cli?tab=readme-ov-file#use-with-pre-commit)

## Other linters / formatters

- [markdownlint/markdownlint](https://github.com/markdownlint/markdownlint)
  - Ruby, last git tag 2023-10
- [markdown-toc](https://github.com/jonschlinkert/markdown-toc)
  API and CLI for generating a markdown TOC (table of contents) for a README or any markdown files
