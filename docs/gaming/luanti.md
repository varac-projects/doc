# Luanti (formerly Minetest)

- [Website](https://www.luanti.org/)
- [GitHub](https://github.com/minetest/minetest)
- [Minetest helm chart](https://artifacthub.io/packages/helm/fermosit/minetest)
  - last updated 2022
