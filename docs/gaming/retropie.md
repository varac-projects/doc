# Retro gaming distributions

## Batocera

- [Website](https://batocera.org/)
- [Docs/wiki](https://wiki.batocera.org/)
  - [Add games/BIOS files to Batocera](https://wiki.batocera.org/add_games_bios)
- Not based on any distribution, but build from scratch with [buildroot](https://buildroot.org/)
- Better out-of-the-box user experience than RetroPie
- Uses Sway/Wayland !

Sync roms:

```sh
rsync -av /home/varac/Software/batocera/roms/ batocera:/media/SHARE/roms
```

### Updates

- [Upgrading/downgrading Batocera](https://wiki.batocera.org/upgrade_manually#upgrading_downgrading_batocera)

Start upgrade:

```sh
batocera-upgrade https://batocera.org/upgrades/x86_64/stable/last
```

### Controller

- [Hotkey shortcuts](https://wiki.batocera.org/basic_commands)
- [Map a controller](https://wiki.batocera.org/configure_a_controller)

#### Keyboard

[Keyboard shortcuts](https://defkey.com/batocera-shortcuts)

#### Bluetooth pairing

- [bluetooth controllers - manual setup](https://wiki.batocera.org/bluetooth_controllers_-_manual_setup)

### HDMI Audio

- Settings / Sound / HDMI

### Terminal

Switch to TTY with `ctrl+alt+f3` and back with `ctrl+alt+f1`

### Keyboard layout

[Switching the Keyboard layout is not working](https://github.com/batocera-linux/batocera.linux/issues/7382#issuecomment-1301912470)

Edit `~/userdata/systemsettings/batocera.conf` and set:

```sh
system.kblayout
system.kbvariant
```

(Note: Using `de`/`nodeadkeys` doesn't fix the layout...)

## Retropie

- [Retropie docs](https://retropie.org.uk/docs)
- Builds upon Raspbian, [EmulationStation](https://emulationstation.org/), [RetroArch](https://www.retroarch.com/)

Logs:

- `./configs/all/emulationstation/es_log.txt`

Config: `/opt/retropie/configs/all/retroarch.cfg`

Sync roms:

```sh
rsync -av /home/varac/Software/RetroPie/roms/ retropie:RetroPie/roms
```

## ROMs

- [Where to Download Retropie ROMs?](https://raspberrytips.com/download-retropie-roms/)
