# Linux gaming

- [bazzite](https://bazzite.gg/)
  - Cloud native image built upon Fedora Atomic Desktops that
    brings the best of Linux gaming to all of your devices -
    including your favorite handheld.
- [./retropie.md](./retropie.md)
- comes bundled with [Lutris](https://lutris.net/) and [Steam](https://store.steampowered.com/)
