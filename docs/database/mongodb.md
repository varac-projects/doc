# MongoDB

- [Docs](https://docs.mongodb.com/)

## Install

### Cli client

#### In docker container

```sh
$ docker exec -it unifi_mongodb bash
  apt update
  apt install mongodb-mongosh
  mongosh
```

## Usage

Start mongo REPL:

```sh
mongo
```

or

```sh
mongosh
```

Then:

```txt
> show dbs
> show users
>
```

### Connect to a db

Connect as root

```sh
mongo -u root -p ${MONGODB_ROOT_PASSWORD} --authenticationDatabase admin
mongo mongodb://wekan:PASSWORD@localhost:27017/wekan
```

## Administration

### Upgrade

#### 5 to 6

- [Upgrade from MongoDB 5.0 to MongoDB 6.0](https://www.mongodb.com/docs/upcoming/release-notes/6.0-upgrade-standalone/#std-label-6.0-upgrade-standalone)

Install mongosh in docker container as shown above.
Then:

```sh
$ mongosh
  test> db.adminCommand( { getParameter: 1, featureCompatibilityVersion: 1 } )
  { featureCompatibilityVersion: { version: '5.0' }, ok: 1 }

  test> db.adminCommand( { setFeatureCompatibilityVersion: "6.0" } )
  { ok: 1 }
```

#### 6 to 7

- [Upgrade from MongoDB 6.0 to MongoDB 7.0](https://www.mongodb.com/docs/upcoming/release-notes/7.0-upgrade-standalone/)

Install mongosh in docker container as shown above.
Then:

```sh
$ mongosh
  test> db.adminCommand( { getParameter: 1, featureCompatibilityVersion: 1 } )
  { featureCompatibilityVersion: { version: '6.0' }, ok: 1 }

  test> db.adminCommand( { setFeatureCompatibilityVersion: "7.0" } )
  { ok: 1 }
```

### Change the storage engine

Change the storage engine of a standalone MongoDB instance from MMAPv1 to WiredTiger

[Change Standalone to WiredTiger](https://docs.mongodb.com/manual/tutorial/change-standalone-wiredtiger/)

In old container:

```sh
mongodump --out=/tmp/dump/
```

In new container:

```sh
mongorestore <exportDataDestination>
```
