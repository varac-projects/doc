# Mariadb

## Install on debian

    apt install mariadb-server

or

    apt install default-mysql-server

> On new installs no root password is set and no debian-sys-maint user is created anymore.

As user root:

    mysql

## Etc

    mysqlcheck -u root -p --auto-repair --check --optimize --all-databases

## SQL syntax examples

    SHOW databases;
    show tables;
    use DB;
    describe users;

    delete from user where User="nobody";
    UPDATE products SET id='17' WHERE identifier='something';

    CREATE DATABASE DB;
    GRANT ALL PRIVILEGES ON DB.* TO USER@localhost IDENTIFIED BY "PW";
    GRANT RELOAD ON *.* TO USER@localhost;
    SHOW GRANTS FOR  USER@localhost;
    FLUSH PRIVILEGES;

## Mariadb docker

https://hub.docker.com/_/mariadb

Start server:

    docker run --name phpbb-mariadb -e MYSQL_ROOT_PASSWORD=... -d mariadb:10.3

Connect with mysql client container:

    docker run -it --link some-mariadb:mysql --rm mariadb sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD"'

### Connect with local mariadb client

Get IP of running server:

    docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' phpbb-mariadb

    mysql -h 172.17.0.2 -u root -p...

Create DB:

    create database DB1924737;
    GRANT ALL PRIVILEGES ON DB1924737.* TO 'U1924737'@'%' IDENTIFIED BY '...';

Restore dump:

    mysql -h 172.17.0.2 -u root -p... DB1924737 < DB1924737.sql

Connect as phpbb user:

    mysql -h 172.17.0.2 -u U1924737 -p... DB1924737

### (Hotfix) Install php mysqli module in php-fpm container

    docker exec -it forum-migration_phpbb-php-fpm_1 bash
    docker-php-ext-install mysqli

## Mysqldump

    mysqldump --single-transaction -h [server] -u [username] -p[password] [db_name] > nextcloud-mysqldump-$(date --iso-8601=seconds).dump
