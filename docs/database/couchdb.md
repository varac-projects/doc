# Couchdb

## Run couchdb as docker service

- [Docker hub: klaemo/couchdb](https://hub.docker.com/r/klaemo/couchdb/)

  docker run -d -p 5984:5984 --name couchdb couchdb

Purge old couchdb container:

    docker rm $(docker stop $(docker ps -a -q --filter ancestor=couchdb --format="{{.ID}}"))

Show docker containers started from image name:

    docker ps -a -q  --filter ancestor=couchdb

## user credentials

    export COUCH_PW="..."
    export COUCHDB_HOST="admin:${COUCH_PW}@127.0.0.1:5984"

Curl options:

    export CURL_OPTS="--cacert /home/varac/Leap/git/bitmask/provider/files/ca/ca.crt"

Use Netrc:

    echo "machine 127.0.0.1 login admin password ${COUCH_PW}" > /etc/couchdb/couchdb.netrc

List all DBs:

    /usr/bin/curl --netrc-file /etc/couchdb/couchdb.netrc -X GET "127.0.0.1:5984/_all_dbs"

List all users:

    curl -X GET "$COUCHDB_HOST/_users/_all_docs?include_docs=true"
    curl $CURL_OPTS -X GET "${COUCHDB_HOST}/_users/_all_docs?include_docs=true"

List a user:

    curl  -X GET "$COUCHDB_HOST/_users/org.couchdb.user:test1"

Delete a user:

    curl  -X DELETE "$COUCHDB_HOST/_users/org.couchdb.user:test1?rev=1-49a131dc3862c5acc4feda736ac886f2"

Parse entry:

    curl  -X GET "$COUCHDB_HOST/_users/org.couchdb.user:admin" | grep -Po '"_rev":.*?[^\\]",'|cut -d':' -f 2|sed 's/[",]//g'

Better pw handling:

    /usr/bin/curl -u "admin:${COUCH_PW}" -X PUT 127.0.0.1:5984/leap_web --data ''

Create Users:

    curl -X PUT http://localhost:5986/_users/org.couchdb.user:jan2 -d '{"name": "jan2", "password": "apple", "roles": [], "type": "user"}'

Remove Users:

    couch-doc-update --host 127.0.0.1:5984 --db _users --id org.couchdb.user:jan --delete

## Bigcouch

Show nodes

    /usr/bin/curl --netrc-file /etc/couchdb/couchdb.netrc -X GET 'http://127.0.0.1:5986/nodes/_all_docs'

Join Node

    /usr/bin/curl --netrc-file /etc/couchdb/couchdb.netrc -X PUT 'http:// 127.0.0.1:5986/nodes/bigcouch@mole.dev.bitmask.net' -d {}

create db

    /usr/bin/curl --netrc-file /etc/couchdb/couchdb.netrc -X PUT 'http://127.0.0.1:5984/testdb' -d {}

## Install couchdb from source

see https://we.riseup.net/leap/couchdb-manual#installation

## Nuke all dbs

    systemctl stop couchdb.service
    rm -rf /var/lib/couchdb
    mkdir /var/lib/couchdb
    chown couchdb:couchdb /var/lib/couchdb
    systemctl start couchdb.service
