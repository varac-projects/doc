# Postgres operators

## Zalando postgres-operator

- [GitHub](https://github.com/zalando/postgres-operator)
  - 4.4k stars, >200 contributors
- [Docs](https://postgres-operator.readthedocs.io/en/latest/)
  - [In-place major version upgrade](https://postgres-operator.readthedocs.io/en/latest/administrator/#in-place-major-version-upgrade)
    - Fewer manual steps involved than with crunchy postgres-operator
- [Helm chart](https://github.com/zalando/postgres-operator/tree/master/charts/postgres-operator)

## Crunchy postgres-operator

- [GitHub](https://github.com/CrunchyData/postgres-operator)
  - 4k stars, 90 contributors
- [Docs](https://access.crunchydata.com/documentation/postgres-operator/latest)
  - [Postgres Major Version Upgrade](https://access.crunchydata.com/documentation/postgres-operator/latest/guides/major-postgres-version-upgrade)
    - Still multiple manual steps involved
