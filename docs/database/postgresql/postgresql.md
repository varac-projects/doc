# Postgresql

<https://wiki.ubuntuusers.de/PostgreSQL/>

## Install

### Install issues

If no cluster gets created automatically, kick it with:

```sh
pg_createcluster 9.4 main --start
```

## Authentication

### Connect as postgres (admin) user

```sh
sudo -i -u postgres
psql
```

### Bitnami K8S image

```sh
psql -U postgres
```

> You will be prompted to enter the postgres user password.
> This is the same as the application password.

### Connect as different user

```sh
psql user_name  -h 127.0.0.1 -d db_name
```

### Change

## Usage

Show DB:

```text
\l
```

Use db:

```text
\c openproject;
```

Show tables:

```text
\dt
```

Query data:

```sql
SELECT * FROM users;
```

List postgres users:

```text
\du
```

Show hashed passwords:

```sql
select * from pg_shadow;
```

## Upgrade postgresql cluster

### Manual

<https://scottlinux.com/2015/11/14/upgrade-postgresql-on-debian-jessie-with-pg_upgradecluster/>

```sh
apt-get install postgresql-9.4
systemctl stop postgresql@9.4-main.service apache2 foreman
sudo -i -u postgres
  pg_dropcluster --stop 9.4 main
  pg_ctlcluster 9.4 main stop
  exit
systemctl start postgresql@9.4-main.service apache2 foreman
```

when everything is ok:

```sh
apt remove postgresql-9.1 postgresql-client-9.1  --purge
```

### Docker upgrade

- [docker-postgres-upgrade](https://github.com/tianon/docker-postgres-upgrade)

### Upgrading in Kubernetes

#### Bitnami chart

- [Postgres upgrade to 13 or 14](https://github.com/bitnami/charts/issues/7999)

## Backup & Restore

### Kubernetes

- [Backup and restore PostgreSQL deployments](https://docs.bitnami.com/general/apps/gitlab/administration/backup-restore-postgresql/)
- [Migrate Data Between Kubernetes Clusters with Bitnami and Velero](https://docs.bitnami.com/tutorials/migrate-data-bitnami-velero/)

### Use custom format (recommended)

With this, you can use pg_restore with more options (like clean all DB objects
before restoring).

Backup:

```sh
sudo -u postgres pg_dump --format=custom dbname > filename
```

Restore:

```sh
DROP DATABASE openproject;
CREATE DATABASE openproject;
pg_restore -d openproject /var/backups/postgres/openproject.pg_dump
```

Unfortunately, this didn't work:

```sh
pg_restore --create --clean -d openproject /var/backups/postgres/openproject.pg_dump
```

Convert custom dump format to sql

```sh
pg_restore openproject-2017-02-09-13\:17\:25.dump > openproject-2017-02-09-13:17:25.sql
```

### Using plain sql dump format

<https://www.postgresql.org/docs/9.4/static/backup-dump.html>

Backup:

```sh
pg_dump dbname > outfile
```

Restore:

```sh
psql dbname < infile
psql --set ON_ERROR_STOP=on dbname < infile
```

### Storing dumps in version control

- [akaihola/pgtricks](https://github.com/akaihola/pgtricks)
  Handy helpers for PostgreSQL users

Usage:

```sh
git clone https://github.com/akaihola/pgtricks.git
cd path/to/git/repo
pg_dump --clean openproject > openproject.sql
../pgtricks/pgtricks/pg_dump_splitsort.py openproject.sql
```

## Etc

### Rewriting string in all tables using dump/restore

```sh
systemctl stop openproject-puma.service nginx.service

su - postgres
pg_dump --clean openproject > openproject.before_path_rewrite.sql
```

Change and review:

```sh
sed 's/dokuwiki-alt/it\/dokuwiki-media/g' \
  openproject.after_path_rewrite.sql > openproject.after_path_rewrite.sql
git diff --word-diff=color --word-diff-regex=. \
  openproject.before_path_rewrite.sql openproject.after_path_rewrite.sql
```

Restore:

```sh
psql --set ON_ERROR_STOP=on openproject < openproject.after_path_rewrite.sql
```

## Upgrade postgres deployed by Bitnami helm chart

- Still no official documentation on Bitnami chart:
  [Postgres upgrade to 13 or 14](https://github.com/bitnami/charts/issues/7999)
- [Upgrade bitnami postgresql image k8s](https://stackoverflow.com/a/69899094)
- [Stackspin: Upgrade postgres chart to v11](https://open.greenhost.net/stackspin/stackspin/-/issues/1134)

Steps:

Create new helm release of Postgres while the old one running
Migrate:

```sh
 kubectl exec -it new-helm-db-postgresql-0 -- bash \
   -c 'export PGPASSWORD=${POSTGRES_PASSWORD}; \
       time pg_dump -h old-postgresql -U postgres | psql -U postgres'
```

## pgloader

> Migrate to PostgreSQL in a single command!

- [GitHub](https://github.com/dimitri/pgloader)

### Install pgloader

- Alpine: `apk add pgloader`
