# semgrep

- [Github](https://github.com/semgrep/semgrep)
- [Docs](https://semgrep.dev/docs/)
  - [Getting started](https://semgrep.dev/docs/getting-started/)
- [Interactive tutorial](https://semgrep.dev/learn/)
- [Megalinter integrtion](https://megalinter.io/latest/descriptors/repository_semgrep/)
- [Semgrep registry](https://semgrep.dev/r)

## Install

    pamac install semgrep-bin

## Usage

    semgrep scan --error -c semgrep.yaml

## Issues

- Semgrep doesn't support asserting if files (don't) exist:
  [Add support for semgrep rules that check for specific file path names](https://github.com/semgrep/semgrep/issues/2438)
  - Suggested solution: Use tools like `ls-lint` or `repolinter`
