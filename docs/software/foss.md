# Free and open source software

- [switching.software](https://switching.software)
  - Ethical, easy-to-use and privacy-conscious alternatives to well-known software
  - [Recommended Similar Projects](https://switching.software/replace/switching.software/)
