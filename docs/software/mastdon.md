# Mastodon

- [Website](https://joinmastodon.org/)
- [Docs](https://docs.joinmastodon.org/)
- [Github](https://github.com/mastodon/mastodon)
- [Support at Github discussions](https://github.com/mastodon/mastodon/discussions)
- [awesome-fediadmin](https://codeberg.org/nev/awesome-fediadmin/)
  A collection of awesome resources for running your own federated social media website.
- [gotosocial](https://github.com/superseriousbusiness/gotosocial)
  A Mastodon server written in go

## Install

### Helm chart

The values for these secrets must be set:

```sh
helm template \
  --set mastodon.secrets.secret_key_base='foo' \
  --set mastodon.secrets.otp_secret='foo' \
  --set mastodon.secrets.vapid.private_key='foo' \
  --set mastodon.secrets.vapid.public_key='foo' \
  .
```

## Setup

### Reset admin passwort

See also [tootctl docs](https://docs.joinmastodon.org/admin/tootctl/)

```sh
alias tootctl='kubectl -n mastodon exec -it deployment/mastodon-web -- tootctl'
tootctl accounts modify admin --reset-password
```

### Manual Elasticsearch re-index

```sh
tootctl search deploy
```

or seperate `INDEXes` at once

```sh
tootctl search deploy --only tags
```

When this fails with [You can't set the item's total value to less than the current progress. (ProgressBar::InvalidProgressError)](https://github.com/mastodon/mastodon/issues/18625)
this workaround worked at least

```sh
sed -E 's/indices.sum.+/2000/g' -i /opt/mastodon/lib/mastodon/search_cli.rb
tootctl search deploy
```

### Moderator

```sh
tootctl accounts modify --role=Moderator <username>
```

## Blocklists

- [Federation Safety Enhancement Project?](https://nivenly.org/blog/2023/08/10/federation-safety-enhancement-project-announcement/)

## Issues

### Full text search

From [Search docs](https://docs.joinmastodon.org/user/network/#search):

> Mastodon’s full-text search allows logged-in users to find results from their
> own posts, their favourites, their bookmarks and their mentions.
> It deliberately does not allow searching for arbitrary strings in the entire
> database, in order to reduce the risk of abuse by people searching for
> controversial terms to find people to dogpile.

Related issues / PRs:

- [Search doesn't find public posts, only you own](https://github.com/mastodon/mastodon/issues/594)

## Tools

- [Strubbl/mastodon-toots-exporter](https://github.com/Strubbl/mastodon-toots-exporter)
  Exports toots of your Mastodon account
- [madonctl](https://github.com/McKael/madonctl)
  Go-CLI client for the Mastodon social network API
- [toot - Mastodon CLI & TUI](https://toot.bezdomni.net/)
- [Mastodeck](https://mastodeck.com/)
  - Tweetdeck alternative
  - Closed source SAAS

## Server administration

### Migrate/restore server

- [How to fully re-install Mastodon and keep database](https://github.com/mastodon/mastodon/discussions/17292)
- [Decommission server instance](https://docs.joinmastodon.org/admin/tootctl/#self-destruct)
  - Remove remote users/posts with `tootctl`, otherwise these will be cached on
    other servers as zombies

### Resource Tuning

#### Sidekiq

- [Optimizing Mastodon Performance with Sidekiq and Redis Enterprise](https://www.reddit.com/r/Mastodon/comments/13ldw9o/optimizing_mastodon_performance_with_sidekiq_and/)

### Monitoring

- [FR: Prometheus exporter](https://github.com/mastodon/mastodon/issues/8640)
  - [It seems streaming API gots prometheus metrics](https://github.com/mastodon/mastodon/pull/26299/files)

#### Metrics exporters

- [Mastodon prometheus exporter FR](https://github.com/mastodon/mastodon/issues/8640)

##### Standalone exporters

So far, there's no helm chart for most of these exporters, unless explicitly
mentioned.

- [marians/mastodon-metrics-exporter](https://codeberg.org/marians/mastodon-metrics-exporter)
  Metrics: Mastodon, Postgres, Redis, search
- [systemli/prometheus-mastodon-exporter](https://github.com/systemli/prometheus-mastodon-exporter)
  - Only few metrics: Domains, statuses, users, weekly_logins, weekly_registrations, weekly_statuses
- [sidekiq-prometheus-exporter](https://github.com/Strech/sidekiq-prometheus-exporter)
  - [Helm chart](https://github.com/Strech/sidekiq-prometheus-exporter#helm)

Stale:

- [dadall/mastodon_exporter](https://github.com/dadall/mastodon_exporter)
- [b2d-prometheus/mastodon exporter](https://gitlab.com/b2d-prometheus/mastodon)

##### Exporters using statsd

- [Mastodon - Part 3 - statsd and Prometheus](https://ipng.ch/s/articles/2022/11/27/mastodon-3.html)
  - Matching [Mastodon Stats Grafana dashboard](https://grafana.com/grafana/dashboards/17492-mastodon-stats/)
- [Mastodon monitoring and metrics setup](https://github.com/ofalvai/mastodon-monitoring)
  - Uses statsd-exporter ([statsd_mapping](https://github.com/ofalvai/mastodon-monitoring/blob/main/statsd-exporter/statsd_mapping.yml))
- [prometheus-statsd-exporter](https://github.com/prometheus/statsd_exporter/)
  - [helm chart](https://artifacthub.io/packages/helm/prometheus-community/prometheus-statsd-exporter)
