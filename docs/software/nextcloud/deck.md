# Deck

## Server app

### Issues

- Docs: [I have experienced an error](https://github.com/stefan-niedermann/nextcloud-deck/blob/master/FAQ.md#i-have-experienced-an-error)

Feature requests:

- [Feature request - Integrate Nextcloud Tags](https://github.com/nextcloud/deck/issues/201)
  Use NC Tags as global tags across all boards
- [Feature request: open and edit cards in upcoming cards view](https://github.com/nextcloud/deck/issues/2745)

### Backup / import / export cards

[3rd party apps for export](https://github.com/nextcloud/deck/issues/87#issuecomment-889957645)

    https://github.com/newroco/DeckImportExport
    https://github.com/keitalbame/nextcloud-deck-export-import
    https://github.com/filipizydorczyk/next-cloud-deck-backup
    https://github.com/mclang/nextcloud-deck-import-trello-json
    https://github.com/jonasof/kb-to-nd

## Android client

### IllegalStateException onSaveInstanceState

- [IllegalStateException: Can not perform this action after onSaveInstanceState](https://github.com/stefan-niedermann/nextcloud-deck/issues/1507)
- [App has failed to run initial sync due to 403 (issue started occurring long ago)](https://github.com/stefan-niedermann/nextcloud-deck/issues/1560)
- Server app: [onSaveInstanceState](https://github.com/nextcloud/deck/issues/3617)
