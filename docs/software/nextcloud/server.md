# Nextcloud server

[Get server capabilities](https://github.com/stefan-niedermann/nextcloud-deck/issues/621#issuecomment-664159566):

```sh
 curl --request GET --url \
   'https://cloud.moewe-altonah.de/ocs/v2.php/cloud/capabilities?format=json' \
   --header 'content-type: application/json' \
   --header 'ocs-apirequest: true' --netrc | jq .
```

## NC server Administration

- [Nextcloud Updates automatisieren](https://sysadms.de/2020/08/nextcloud-updates-automatisieren/)

### occ cli

```sh
cd /var/www/owncloud/
sudo -u openproject php occ --help
```

Show available commands:

```sh
sudo -u openproject php occ list
```

Status:

```sh
sudo -u openproject php occ status
sudo -u openproject php occ files:scan --path='files/'
```

Update app:

```sh
sudo -u www-data php ./occ app:update news --allow-unstable
```

[Update app to non-stable alpha/beta version](https://github.com/nextcloud/news/issues/2585#issuecomment-1937028091):

```sh
sudo -u www-data php ./occ app:update news --allow-unstable
```

### Backup

#### mariadb

<https://doc.owncloud.org/server/10.0/admin_manual/maintenance/backup.html>

- `/var/lib/owncloud-data`
- `/var/www/owncloud`
- DB: `mysqldump --single-transaction DB2485828 > /var/backups/mysql-owncloud.9.1.4._$(date +"%Y%m%d").bak`
