# Nextcloud desktop client

Config files: `~/.config/Nextcloud`

## Debug / Troubleshooting

```sh
mkdir /tmp/nc
/usr/bin/nextcloud --logdebug --logfile /tmp/nc.log
```
