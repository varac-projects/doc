# Nextcloud clients

- [Managing devices](https://docs.nextcloud.com/server/19/user_manual/session_management.html)
- [Systemli gets a unified login (single sign-on)](https://www.systemli.org/en/2024/06/15/systemli-gets-a-unified-login-single-sign-on/)

## Desktop

- [Desktop client](./desktop-client.md)

### App the only support App tokens

- [vdirsyncer](https://github.com/pimutils/vdirsyncer)
- [Thunderbird lightning](<https://en.wikipedia.org/wiki/Lightning_(software)>)

## Mobile

Currently all used mobile apps support the login flow via browser or
piggyback on the NC mobile client, so no app tokens are needed !

### App that use the nexcloud mobile client for authentication

- [Deck](./deck.md)

### Apps that support the NC login flow via browser

- [News android app](https://apps.nextcloud.com/apps/nextcloud_news_android_app)
- Davx5

## Davx5

- [Website](https://www.davx5.com/)
- [Davx5 with Nextloud](https://www.davx5.com/tested-with/nextcloud)
  - Contact group method: groups are per-contact categories

Start [account configuration from cli](https://github.com/bitfireAT/davx5-ose/discussions/53#discussioncomment-10356744),
i.e.:

```sh
user=varac
token=**********
domain=systemli.org
adb shell 'am start -a android.intent.action.VIEW -d "davx5://'$user':'$token'@'$domain'"'
```

### Issues

- [HttpException: HTTP 415 - How to find calendar entry which causes it](https://github.com/bitfireAT/davx5-ose/discussions/949)
