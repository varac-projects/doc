# CAD Programs

- [UbuntuUsers wiki: CAD](https://wiki.ubuntuusers.de/CAD/)
- [CAD software linux](https://itsfoss.com/cad-software-linux/)

## FreeCAD

- [Website](https://www.freecadweb.org/)
- [FreeCAD](https://github.com/FreeCAD/FreeCAD)
- [FreeCAD wiki: Getting started](https://wiki.freecadweb.org/Getting_started)
- Debian packages

### Workbenches

#### Arch workbench

- [Arch module](https://wiki.freecadweb.org/Arch_Module)
- [Arch tutorial](https://wiki.freecadweb.org/index.php?title=Arch_tutorial/en)
- Can be combined with the [TechDraw_Module](https://wiki.freecadweb.org/TechDraw_Module)

> The BIM functionality of FreeCAD is now progressively split
> into this Arch Workbench, which holds basic architectural tools,
> and the Workbench BIM.svg BIM Workbench, which is available from
> the Std AddonMgr.svg Addon Manager

#### BIM Workbench

- [FreeCAD wiki: BIM Workbench](https://wiki.freecadweb.org/BIM_Workbench)
- [FreeCAD BIM migration guide](https://yorik.uncreated.net/blog/2020-010-freecad-bim-guide)
- Installeable via AddOn-Manager

## OpenSCAD

- [Website](https://www.openscad.org/)
- [GitHub](https://github.com/openscad/openscad)
- Actively maintained
- Descibed by DSL

## librecad

- Unmaintained, [no stable release since 2016](https://github.com/LibreCAD/LibreCAD/releases)
- No new Ubuntu package verssoins since 18.10
- Can open .dwg files

## Sweethome3D: seit Ubuntu 18.04 keine neue Version mehr

- java

Issues:

- [3D preview on external display only works when inititally started on laptop display](https://sourceforge.net/p/sweethome3d/bugs/339/)
