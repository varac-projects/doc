# Desktop publishing software

## Scribus

- [Website](https://www.scribus.net/)
- [Wiki / Docs](https://wiki.scribus.net/canvas/Scribus)
- Config: `~/.config/scribus/`
- Tempates / Scrapbooks: `~/Templates/Scribus`

### Scripts

Script directories:

- `/usr/share/scribus/scripts`
- `~/.local/share/scribus/scripts`

- [PhotoBookTools-for-Scribus](https://github.com/RaffertyR/PhotoBookTools-for-Scribus)
  - See details in [../media/images/photobooks.md](../media/images/photobooks.md)
