# Pacman

- [pacman](https://wiki.archlinux.org/title/pacman)
- [pacman/Tips and tricks](https://wiki.archlinux.org/title/pacman/Tips_and_tricks)

## List / Search

- List packages installed by pacman natively: `pacman -Qe`
- List packages installed via wrapper: `pacman -Qm`

Search for packages:

Search for package providing a filename:

```sh
pacman -F eslint
```

List files in a package: `pacman -Ql kanshi`

List of installed packages (without AUR):

```sh
pacman -Qqen > /etc/pkglist.txt
```

List of non-native packages (i.e. from AUR):

```sh
pacman -Qqem > /etc/pkglist-aur.txt
```

## Install

```sh
pacman -R python-jsmin
```

## Uninstall

```sh
pacman -R python-jsmin
```

## Downgrade

[Downgrade packages](https://wiki.archlinux.org/title/downgrading_packages)

### From local cache, if available

```sh
pacman -U file:///var/cache/pacman/pkg/curl-8.0.1-1-x86_64.pkg.tar.zst  file:///var/cache/pacman/pkg/lib32-curl-8.0.1-1-x86_64.pkg.tar.zst
```

### Using the downgrade script

[Automation](https://wiki.archlinux.org/title/downgrading_packages#Automation)
Install [downgrade](https://github.com/archlinux-downgrade/downgrade) script:

```sh
pamac install downgrade
```

Downgrade packages:

```sh
export DOWNGRADE_FROM_ALA=1
sudo -E downgrade icu=72.1-2
```

## Setup mirrorlist

- [Manjaro wiki: Pacman-mirrors](https://wiki.manjaro.org/index.php/Pacman-mirrors)

Search for the 5 fastest mirrors and update `/etc/pacman.d/mirrorlist`
accordingly:

```sh
sudo pacman-mirrors -f 5
```

Restrict to german mitrrors:

```sh
sudo pacman-mirrors -g -c Germany
```

## Upgrade

- Upgrade packages: `pacman -Syu`

## Dependency management

- Show unneeded dependencies: `pacman -Qdtq`
- Remove unneeded dependencies: `pacman -Rs $ (pacman -Qdtq)`

## pacman + Proxy

- [Getting pacman to work through proxy with Wget](https://www.puppychau.com/archives/667)
- [Pacman does not honor proxy settings](https://wiki.archlinux.org/title/pacman#Pacman_does_not_honor_proxy_settings)

## Arch signing keys

- [pacman/Package signing](https://wiki.archlinux.org/title/Pacman/Package_signing)

Refresh all keys (takes a while):

```sh
pacman-key --refresh-keys
```

## Arch Linux Archive (ALA)

- [Arch wiki: ALA](https://wiki.archlinux.org/title/Arch_Linux_Archive)
- i.e. [all chromium packages](https://archive.archlinux.org/packages/c/chromium/)

## Package groups

- [Arch wiki: Meta package and package group](https://wiki.archlinux.org/title/Meta_package_and_package_group)
  - [Installing package groups](https://wiki.archlinux.org/title/Pacman#Installing_package_groups)
- [The Easy Way To Install And Remove A Package Group In Arch Linux](https://ostechnix.com/the-easy-way-to-install-and-remove-a-package-group-in-arch-linux/)

## Trobleshooting

### Warning: local is newer than community

[How To Fix “Warning: local is newer than community” Error In Arch Linux](https://ostechnix.com/how-to-fix-warning-local-is-newer-than-community-error-in-arch-linux/)

```sh
Warning: seatd: local (0.7.0-3) is newer than community (0.7.0-2)
```

Solution: `sudo pacman -Suu` will downgrade packages that are too new

### Invalid gpg key

[Manjaro wiki: Pacman trobleshooting#Errors about Keys](https://wiki.manjaro.org/index.php/Pacman_troubleshooting)
[Arch wiki: Invalid signature errors](https://wiki.archlinux.org/title/Pacman/Package_signing#Invalid_signature_errors)
multilib.db: manjaro-sway:
[signature from "Jonas Strassel <info@jonas-strassel.de>" is invalid](https://github.com/manjaro-sway/manjaro-sway/issues/463)

Solution A: [Fetch and lsign failing gpg key](https://github.com/manjaro-sway/manjaro-sway/issues/463#issuecomment-1658178692):

```sh
sudo pacman-key --recv-keys A44C644D792767CED7941AFEABB2075D5F310CF8
sudo pacman-key --lsign-key A44C644D792767CED7941AFEABB2075D5F310CF8
```

Manual solution C:

```sh
sudo rm -rf /var/lib/pacman/sync/* /var/cache/pacman/pkg/* /etc/pacman.d/gnupg
sudo pacman-key --init
sudo pacman-key --populate
sudo pacman-key --refresh-keys
sudo pacman -Sc
sudo pacman -Syy
sudo pacman -Sy manjaro-keyring archlinux-keyring
```

#### Build package with PKGBUILD

- [arch wiki: PKGBUILD](https://wiki.archlinux.org/title/PKGBUILD)

i.e.

```sh
cd ~/projects/timetracking/activitywatch/aw-watcher-window-wayland-git
makepkg
makepkg --install
```

## AUR helpers

- [Arch wiki](https://wiki.archlinux.org/title/AUR_helpers)
- [Enable AUR support](https://wiki.manjaro.org/index.php/Arch_User_Repository)
