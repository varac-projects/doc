# npm

- [npm docs](https://docs.npmjs.com)
- [Arch wiki: Node.js/npm](https://wiki.archlinux.org/title/Node.js#Node_Packaged_Modules)

## Install

- [Npm docs: Install](https://docs.npmjs.com/cli/v9/configuring-npm/install)
- [Arch npm package](https://archlinux.org/packages/extra/any/npm/)
  - [Manjaro packages: npm](https://packages.manjaro.org/?query=npm)
  - Issue: `npm` `10.1.0` package ignores the `https_proxy` etc env vars.
    `/usr/lib/node_modules/corepack/shims/npm` from the `nodejs` package does
    honor them. Waiting for [npm 10.2.0](https://packages.manjaro.org/?query=npm) to be available
    from the Manjaro `Stable` repo.

## Global Config / setup

```sh
npm config list
```

## Global installation in homdir

```sh
mkdir ~/.npm/packages
npm config set prefix ~/.npm/packages
```

Show root dirs:

```sh
npm root -g
npm root
```

Show prefix dirs:

```sh
npm prefix
npm prefix -g
```

## Install / Upgrade npm

```sh
npm install npm@latest -g
```

Installation paths:

- local: `~/node_modules/.bin/`
- global: `~/.npm-global/bin/`

[npm global or local packages](https://flaviocopes.com/npm-packages-local-global/)

## Install npm modules locally, not as root

[Resolving EACCES permissions errors when installing packages globally](https://docs.npmjs.com/getting-started/fixing-npm-permissions)

```sh
npm install mermaid.cli
~/node_modules/.bin/mmdc -h
```

## Install from Github

```sh
npm install LinuxBozo/jsonresume-theme-kendall#main
```

## Upgrade module

```sh
npm update turndown --depth 3
```

## Update package-lock.json

Update `package-lock.json` after updating versions in `package.json`:

```sh
npm update --save
```

## Audit security vulnerabilities

```sh
npm audit
npm audit fix
```

### npx

- [npm docs: npx](https://docs.npmjs.com/cli/v8/commands/npx)

[Introducing npx: an npm package runner](https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b)

## Yarn

[Website](https://yarnpkg.com/lang/en/)

Until this [Debian ITP](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=843021) got resolved:

```sh
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo tee /etc/apt/trusted.gpg.d/yarn.asc
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
```

Config at `~/.yarnrc`
