# dnf package manager

- [Fedora docs: Using the DNF software package manager](https://docs.fedoraproject.org/en-US/quick-docs/dnf/)

## Usage

```sh
dnf search git
dnf install git
```
