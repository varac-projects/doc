# Debian packages

## Security

- [Security tracker](https://security-tracker.debian.org/tracker/)
- [Vulnerable source packages among backports for stable](https://security-tracker.debian.org/tracker/status/release/stable-backports)
- i.e. [Information on source package curl](https://security-tracker.debian.org/tracker/source-package/curl)

From the [Backports FAQ](https://backports.debian.org/FAQ/):

> Q: Is there security support for packages from backports.debian.org?
> A: Unfortunately not. This is done on a best effort basis by the people who track the package, usually the ones who originally did upload the package into backports. When security related bugs are fixed in Debian unstable the backporter is permitted to upload the package from directly there instead of having to wait until the fix hits testing. You can see the open issues for bullseye-backports in the security tracker (though there may be false positives too, the version compare isn't perfect yet)
