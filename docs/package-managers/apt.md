# Apt package manager

## Full upgrade

```sh
sudo apt clean
sudo apt --fix-broken install
sudo apt update -m
sudo dpkg --configure -a
sudo apt install -f
sudo apt full-upgrade
```

## Purge

```sh
apt purge $(dpkg -l | grep ^rc | awk '{print $2}')
```

## Add apt repo in secure manner

<https://wiki.debian.org/DebianRepository/UseThirdParty>

## Don't install recommended packages

```sh
apt install --no-install-recommends ...
```

## Add next release to apt sources

```sh
export next_release="bookworm"
sudo cat << EOF > /etc/apt/sources.list.d/${next_release}.list
deb http://deb.debian.org/debian ${next_release} main
deb http://deb.debian.org/debian ${next_release}-updates main
EOF

sudo cat << EOF > /etc/apt/preferences.d/${next_release}
Explanation: Debian testing
Package: *
Pin: release o=Debian,n=${next_release}
Pin-Priority: 2
EOF

sudo apt update
```

## Add debian testing to apt sources

**Warning**: `testing` doesn't provide security updates, so better use codenames
like `buster` !!!

```sh
sudo cat << EOF > /etc/apt/sources.list.d/buster.list
deb http://deb.debian.org/debian/ testing main
deb http://deb.debian.org/debian/ testing-updates main
EOF

sudo cat << EOF > /etc/apt/preferences.d/testing
Explanation: Debian testing
Package: *
Pin: release o=Debian,a=testing
Pin-Priority: 2
EOF

sudo apt update
```

## Extract a .deb file without opening it on Debian or Ubuntu Linux

```sh
dpkg-deb -xv htop_2.0.1-1ubuntu1_amd64.deb $(mktemp)
```

## Backports

<https://backports.debian.org/Instructions/>

- there is the NEW queue: <http://ftp-master.debian.org/backports-new.html>
- there is the IRC channel where you might see discussion,
  and a bot announcing uploads/acceptance by NEW
- there is a mailing list where you can see uploads
  and acceptances announced automatically: <http://backports.debian.org/Mailinglists/>

Setup backports:

```sh
echo "deb http://deb.debian.org/debian $(lsb_release -cs)-backports main" > /etc/apt/sources.list.d/backports.list
apt update
```

## List manually installed packages

```sh
~/bin/apt-manually-installed-packages.py
```

## List installed packages with their source

[apt-forktracer Website](https://owsiany.pl/apt-forktracer-page)

> apt-forktracer a utility for managing Debian package versions
> It lets you find out which packages in the system are installed in non-official versions

```sh
apt install apt-forktracer
apt-forktracer
```

## List non-free packages

```sh
dpkg-query -W -f='${Section}\t${Package}\n' | grep ^non-free
```

## Add 3rd party repo signing key

[How To Handle apt-key and add-apt-repository Deprecation Using gpg to Add External Repositories on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-handle-apt-key-and-add-apt-repository-deprecation-using-gpg-to-add-external-repositories-on-ubuntu-22-04)

```sh
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/microsoft.gpg
```

## List installed debs

```sh
apt-mark showmanual > ~/home/varac/ubuntu-migration.20121205.txt
apt-mark showmanual > ~/ubuntu-migration.now.txt
diff ~/ubuntu-migration.20121205.txt ~/ubuntu-migration.now.txt
```

## Purge residual packages (config leftovers from uninstalled packages)

```sh
apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')
```

## dpkg

```sh
dpkg --compare-versions 0.6.24-1 gt 0.6b3 && echo yes
```

## apt-key

from `man apt-key`:

```txt
DEPRECATION

Except for using apt-key del in maintainer scripts, the use of apt-key is deprecated. This
section shows how to replace existing use of apt-key.

If your existing use of apt-key add looks like this:

wget -qO- https://myrepo.example/myrepo.asc | sudo apt-key add -

Then you can directly replace this with (though note the recommendation below):

wget -qO- https://myrepo.example/myrepo.asc | sudo tee /etc/apt/trusted.gpg.d/myrepo.asc

Make sure to use the "asc" extension for ASCII armored keys and the "gpg" extension for the
binary OpenPGP format (also known as "GPG key public ring"). The binary OpenPGP format works
for all apt versions, while the ASCII armored format works for apt version >= 1.4.
```

i.e.

```console
 W: http://raspbian.raspberrypi.org/raspbian/dists/bookworm/InRelease:
 Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg),
 see the DEPRECATION section in apt-key(8) for details.
```

Solution:

- Remove keys from `/etc/apt/trusted.gpg`
- Add `[signed-by=/usr/share/keyrings/raspbian-archive-keyring.gpg]` to `/etc/apt/sources.list`

```sh
apt-key list
gpg --import /etc/apt/trusted.gpg
gpg --armor --export A0DA38D0D76E8B5D638872819165938D90FDDD2E > /etc/apt/trusted.gpg.d/A0DA38D0D76E8B5D638872819165938D90FDDD2E.asc
...
rm /etc/apt/trusted.gpg
gpg --delete-keys A0DA38D0D76E8B5D638872819165938D90FDDD2E
apt-key list

```

## Pinning

- [Apt-Pinning](https://wiki.ubuntuusers.de/Apt-Pinning/)

Pin version:

```sh
Pin: version 2.3.3-20180415.190957~artful1
```

Add [Debian testing](https://wiki.debian.org/DebianTesting) repo:

```console
$ cat <<EOF > /etc/apt/preferences.d/testing.pref
Package: *
Pin: release a=testing
Pin-Priority: 1
EOF

echo "deb http://deb.debian.org/debian testing main" > /etc/apt/sources.list.d/testing.list
apt update
```

## Remove unneded packages

```sh
apt autoremove --purge
deborphan | dpkg --purge
dpkg -l | grep ^rc | awk '{print $2}' | dpkg --purge
apt-forktracer
while deborphan -n | grep -q . ; do apt purge $(deborphan -n); done
```

## Apt frontends

### Nala

- [Gitlab](https://gitlab.com/volian/nala)
- [Nala – A Feature-rich Commandline Frontend For APT Package Manager](https://ostechnix.com/nala-commandline-frontend-for-apt/)

Install:

```sh
sudo apt install nala
```

Issues:

- Using nala `0.11.1~bpo22.04.1` from `jammy-backports` gives
  [ImportError: cannot import name 'get_terminal_size' from 'click.termui'](https://github.com/tiangolo/typer/pull/375)
  - This is fixed in `python3-click` `8.1`, but there's no debian package so far
  - Same for `0.11.1` from \`kinetic\`\`
  - Same for nala `0.11.0` from the Nala deb repo

### UnattendedUprades

```sh
// Lines below have the format format is "keyword=value,...".  A
// package will be upgraded only if the values in its metadata match
// all the supplied keywords in a line.  (In other words, omitted
// keywords are wild cards.) The keywords originate from the Release
// file, but several aliases are accepted.  The accepted keywords are:
//   a,archive,suite (eg, "stable")
//   c,component     (eg, "main", "contrib", "non-free")
//   l,label         (eg, "Debian", "Debian-Security")
//   o,origin        (eg, "Debian", "Unofficial Multimedia Packages")
//   n,codename      (eg, "jessie", "jessie-updates")
//     site          (eg, "http.debian.net")
// The available values on the system are printed by the command
// "apt-cache policy", and can be debugged by running
// "unattended-upgrades -d" and looking at the log file.
```

Debug

```sh
unattended-upgrades --dry-run --debug
```
