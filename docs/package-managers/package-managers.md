# Etc package managers

* [topgrade](https://github.com/topgrade-rs/topgrade)
  Topgrade detects which tools you use and runs the appropriate commands to update them.
