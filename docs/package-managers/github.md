# Github package managers / release downloaders

## gh (official Gitlab cli client)

- [Website](https://cli.github.com/)

Usage:

```sh
gh release download -D /tmp --pattern '*.gz' -R Ramilito/kubesess
```

## eget

- [Github](https://github.com/zyedidia/eget)

Install:

```sh
curl https://zyedidia.github.io/eget.sh | sh
```

Usage:

```sh
export EGET_BIN=/home/varac/bin/download
eget neovim/neovim
```

Get dedicated release version:

```sh
eget -t v2.2.3 fluxcd/flux2
```

## Huber, Package Manager for GitHub repos

- [github](https://github.com/innobead/huber)
- Rust
- No Arch package
- Only installs packages from a curated [managed package list](https://github.com/innobead/huber/blob/master/doc/packages.md)

### Install

```sh
curl -sfSL https://raw.githubusercontent.com/innobead/huber/master/hack/install.sh | bash
```

## Alternatives (all not packaged in Arch)

- [gh-release-install](https://github.com/jooola/gh-release-install)
  Python
- [grm](https://github.com/jsnjack/grm)
  - Go
- Stale: [gpm](https://github.com/aerys/gpm)
  - Rust
  - Last commit 1 year ago
