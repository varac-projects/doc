# Pacman wrappers

## cli

### pamac

- [Manjaro Wiki](https://wiki.manjaro.org/index.php/Pamac)
- config dir: `~/.config/pamac`
- tmp dir: `/var/tmp/pamac`
- DBs: `/var/tmp/pamac/dbs/sync`
- Temp package files: `/var/tmp/pamac/aur-varac`

Usage:

List available updates:

#### Pamac issues

##### Can't install for aur packages on corporate proxy

- [Gitlab issue](https://gitlab.manjaro.org/applications/pamac/-/issues/1325)
- Solution: Use `yay` for installing AUR packages

##### unable to lock database, Failed to synchronize databases

- [Pamac update: unable to lock database, Failed to synchronize databases](https://forum.manjaro.org/t/pamac-update-unable-to-lock-database-failed-to-synchronize-databases/114762/1)
- Solution: `pamac update --force-refresh`

##### invalid or corrupted database

- `Error: Failed to prepare transaction: invalid or corrupted database`

[Try this solution](https://forum.manjaro.org/t/pamac-error-failed-to-prepare-transaction-invalid-or-corrupted-database/128016/2):

```sh
pamac update --force-refresh
```

If this doesn't solve the issue, try this solution from a [Manjaro fourum post](https://forum.manjaro.org/t/i-am-getting-an-error-when-i-try-to-install-updates/46362/9):

```sh
sudo pacman-mirrors -g -c Germany
sudo rm /var/lib/pacman/sync/*
sudo pacman-key --init
sudo pacman-key --refresh-keys
sudo pacman-key --populate archlinux manjaro
sudo pacman -Syyu
```

### yay

- [Github](https://github.com/Jguer/yay)
- Not packaged in Arch, but pre-packaged in Manjaro-sway

Behind proxy:

- [Support for proxy variables](https://github.com/Jguer/yay/issues/1911)

Use `--sudoflags=-E`:

Update package lists:

```sh
yay -Syu --sudoflags=-E
```

Install package:

```sh
yay -S google-cloud-sdk --sudoflags=-E
```

### Paru

- [Github](https://github.com/Morganamilo/paru)
- Not packaged in Arch

Install:

```sh
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

Files:

- Cache/clone dir: `~/.cache/paru/clone/`

#### Usage

Interactive search and install:

```sh
paru inxi
```

Install package:

```sh
paru -S tofi
```

### baph

- [Github](https://github.com/PandaFoss/baph)
- Not packaged in Arch
- Stale !

Full system upgrade:

```sh
baph -u
```

## TUI

### Parui

[parui](https://github.com/Vonr/parui) is a TUI frontend to paru
