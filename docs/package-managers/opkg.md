# opkg

* [opkg docs](https://openwrt.org/docs/guide-user/additional-software/opkg)
* [Useful opkg commands](https://wiki.emacinc.com/wiki/Opkg:installedPkgs)

## Usage

Update package lists:

    opkg update

## Upgrade packages

> To upgrade a group of packages, run opkg upgrade packagename1 packagename2.
> A list of upgradeable packages can be obtained with the opkg list-upgradable command.
> Since OpenWrt firmware stores the base system in a compressed read-only
> partition, any update to base system packages will be written in the
> read-write partition and therefore use more space than it would if it was just
> overwriting the older version in the compressed base system partition. It's
> recommended to check the available space in internal flash memory and the
> space requirements for updates of base system packages. Upgrading packages you
> installed should not have this issue as they are already in the read-write
> partition so the new one will overwrite the older one, although checking
> before upgrading never hurts.
> As a general rule of thumb, devices that have 8 MiB or more total flash size
> and no user-installed packages should not have space issues when updating base
> packages, and of course devices set up for Extroot won't have any space issue.

    opkg upgrade <package>

## Upgrade all installed packages

> Mass upgrade of all packages is strongly discouraged. If you still want to proceed,
> set up Opkg extras or use the following one-liner:

    opkg list-upgradable | cut -f 1 -d ' ' | xargs opkg upgrade

## List All Installed Packages Sorted by Size

    cd /tmp
    wget https://openwrt.org/_export/code/user/vvkjndl/opkg-list-packages-sorted?codeblock=0
    sh /tmp/opkg-list-packages-sorted\?codeblock=0

## Out of disk while installing

* [How do I free up some space?](https://openwrt.org/faq/no_space_left_on_device#how_do_i_free_up_some_space)
  * [Remove partly installed packages and their dependencies if opkg runs out of space during a transaction](https://openwrt.org/docs/guide-user/additional-software/opkg#out_of_space)
