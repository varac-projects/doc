# Ergonimics

- [Ianny](https://github.com/zefr0x/ianny)
  - Simple, light-weight, easy to use, and effective
    Linux Wayland desktop utility that helps with preventing
    repetitive strain injuries by keeping track of usage patterns
    and periodically informing the user to take breaks.
