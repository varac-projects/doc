# Testing in Neovim

## Neotest

- [GitHub](https://github.com/nvim-neotest/neotest)

### Issues

- Python: `No tests found`
  - Solution: [Create a `pyproject.toml` file](https://github.com/nvim-neotest/neotest-python/issues/75#issuecomment-2188820822)
