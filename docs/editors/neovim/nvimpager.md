# nvimpager

<https://github.com/lucc/nvimpager>

Install:

<https://github.com/lucc/nvimpager#installation>

```sh
git clone https://github.com/lucc/nvimpager ~/projects/neovim/nvimpager/
cd ~/projects/neovim/nvimpager/
gup
sudo apt install pandoc make scdoc
make PREFIX=$HOME/.local install
```
