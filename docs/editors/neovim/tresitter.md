# Neovim treesitter

- [LazyVim treesitter defaults](https://www.lazyvim.org/plugins/treesitter)

Check which language parsers are installed: `:TSInstallInfo`
Add language parsers to be installed in `~/.config/nvim/lua/plugins/treesitter.lua`

## Additional language support

- [dnicolodi/tree-sitter-beancount](https://github.com/dnicolodi/tree-sitter-beancount)
