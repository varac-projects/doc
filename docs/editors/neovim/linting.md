# Neovim linting

## nvim-lint

- [nvim-lint](https://github.com/mfussenegger/nvim-lint)
- Lua
- Default linter in [Lazyvim linting plugin](https://www.lazyvim.org/plugins/linting#nvim-lint)
- Only linting, [no fixer support](https://github.com/mfussenegger/nvim-lint/issues/5)
- [Supported linters](https://github.com/mfussenegger/nvim-lint?tab=readme-ov-file#available-linters)
- Config: `~/.config/nvim/lua/plugins/lint.lua`

## Other linters

- [diagnostic-languageserver](https://github.com/iamcco/diagnostic-languageserver)
  - Typescript
- [efm-langserver](https://github.com/mattn/efm-langserver)

  - go, 359 commits
  - Super long config example, not out of the box

- [diagnosticls-configs-nvim](https://github.com/creativenull/diagnosticls-configs-nvim)
  - Linting and formatting
