# Neovim IDE

Best integrated IDE: [LazyVim](https://www.lazyvim.org/)

Blog posts about Neovim IDE setups:

- [How to set up Neovim 0.5 + Modern plugins (LSP, Treesitter, Fuzzy finder, etc)](https://blog.inkdrop.app/how-to-set-up-neovim-0-5-modern-plugins-lsp-treesitter-etc-542c3d9c9887)
- [Basic stable starting point config for a Neovim IDE in
  lua](https://github.com/LunarVim/nvim-basic-ide)
- [nv-ide](https://github.com/crivotz/nv-ide)
  - Neovim custom configuration, oriented for full stack developers
    (Ruby on Rails, ruby, php, html, css, SCSS, javascript)

## Components

- [Formatting](./formatting.md)
- [Git](./git.md)
- [LSP](./lsp.md)
- [Linting](./linting.md)
- [Treesitter](./tresitter.md) for code syntax (i.e. highlighting)

### Diagnostics

- [Trouble](https://github.com/folke/trouble.nvim)
- [Usage](https://github.com/folke/trouble.nvim#-usage)
- Config in `~/.config/nvim/lua/plugins.lua`
- Just a UI for LSP diagnostic issues

Default key bindings:

- `<leader>xx`: TroubleToggle

Issues:

- [Expose providers/modes name](https://github.com/folke/trouble.nvim/issues/213)

### Hover over plugins

- [boo](https://github.com/LukasPietzschmann/boo.nvim)
  - Quickly pop-up lsp-powered infos of the thing your cursor is on
- [eagle.nvim](https://github.com/soulis-1256/eagle.nvim)
  - Show diagnostics and lsp info inside a custom window, following the mouse position
