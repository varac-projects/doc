# AI

- [r/neovim: AI-Assisted Coding in Neovim](https://www.reddit.com/r/neovim/comments/1fm5rvh/aiassisted_coding_in_neovim/)

## Overview

| Neovim PLugin                                                         | Supported models                                                                   |
| --------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| [avante.nvim](https://github.com/yetone/avante.nvim)                  | claude, openai, azure, gemini, cohere, copilot                                     |
| [codecompanion.nvim](https://github.com/olimorris/codecompanion.nvim) | Anthropic, Copilot, Gemini, Ollama, OpenAI, Azure OpenAI, HuggingFace and xAI LLMs |
