# Spellcheck

* Arch: `sudo pacman -S hunspell-de hunspell-en_us hunspell`

## Install languages

* English spell check files are bundled with the `neovim-runtime` deb package
  and installed in `/usr/share/vim/vim82/spell/`
* Custom language files can be downloaded interactively by using i.e.
  `:set spelllang=es`, `:set spell`, and are placed into `~/.config/nvim/spell`
* If the automatic download doesn't work, spell files can be downloaded
  manually:

Manual download:

    mkdir -p ~/.config/nvim/spell/
    wget https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.spl -O ~/.config/nvim/spell/de.utf-8.spl
    wget https://ftp.nluug.nl/pub/vim/runtime/spell/de.utf-8.sug -O ~/.config/nvim/spell/de.utf-8.sug
    wget https://ftp.nluug.nl/pub/vim/runtime/spell/es.utf-8.spl -O ~/.config/nvim/spell/es.utf-8.spl
    wget https://ftp.nluug.nl/pub/vim/runtime/spell/es.utf-8.sug -O ~/.config/nvim/spell/es.utf-8.sug

## Set spellcheck language

    set spell
    set spelllang=de

or

    :setlocal spell spelllang=en_us

Modeline:

    # % vim: spelllang=es

## Usage

Show possible corrections, with cursor over wrongly spelled word
in normal mode:

    z=

Add word to custom dictionary (i.e. `~/.config/nvim/spell/de.utf-8.add`):

    zg

## Auto-detect language

[vim-DetectSpellLang](https://github.com/Konfekt/vim-DetectSpellLang)

* Doesn't work on spanish texts ... ?
