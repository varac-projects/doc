# Grammar check

Ich will was, Du auch ?

- [Arch wiki: Grammar check](https://wiki.archlinux.org/title/Language_checking#Grammar_checkers)

## Languagetool

- [Website](https://languagetool.org/de)
- [Arch package](https://archlinux.org/packages/extra/any/languagetool/)

### Vim plugins

- [valentjn/ltex-ls language server](https://github.com/valentjn/ltex-ls)
  - Uses Languagetool
  - Last commit & release 2023-03
  - [Docs](https://valentjn.github.io/ltex/)
  - [Write Better in Neovim With Languagetool](https://www.rockyourcode.com/write-better-in-neovim-with-languagetool/)
  - [Use a custom dictionary](https://miikanissi.com/blog/grammar-and-spell-checker-in-nvim/)
  - [nvim-lspconfig config options](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#ltex)
  - Local config at `~/.config/nvim/lua/plugins/lsp.lua`
- [vigoux/ltex-ls.nvim](https://github.com/vigoux/ltex-ls.nvim)
  - Enhanced integration of ltex-ls for neovim
  - Last commit 2022-08
  - Couldn't make it work

Stale:

- [dpelle/vim-LanguageTool](https://github.com/dpelle/vim-LanguageTool)
  - Last commit 2021
  - "It is not asynchronous, so it will hang until the results are produced."
- [languagetool-language-server/languagetool-languageserver](https://github.com/languagetool-language-server/languagetool-languageserver)
  Last commit 2021
- Archived: [vigoux/LanguageTool.nvim](https://github.com/vigoux/LanguageTool.nvim)
- [Konfekt/vim-langtool](https://github.com/Konfekt/vim-langtool)
  Last commit 2021
- [rhysd/vim-grammarous](https://github.com/rhysd/vim-grammarous)
  Last commit 2020
