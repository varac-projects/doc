# LazyVim

- [LazyVim](https://www.lazyvim.org/)
  - "LazyVim is a Neovim setup powered by 💤 lazy.nvim
    to make it easy to customize and extend your config."
- [Github](https://github.com/LazyVim/LazyVim)
- [folke/dot](https://github.com/folke/dot/tree/master/nvim)
  Folkes dotfiles, incl. lazyvim config
- [Varacs dotfiles nvim config](https://0xacab.org/varac-projects/dotfiles/-/tree/main/.config/nvim)
  - based on LazyVim

## Getting started

- [Requirements](https://www.lazyvim.org/#%EF%B8%8F-requirements)

Start a fresh LazyVim install:

```sh
export NVIM_APPNAME=nvim-lazyvim-clean
git clone https://github.com/LazyVim/starter ~/.config/${NVIM_APPNAME}
rm -rf ~/.config/${NVIM_APPNAME}/.git
nvim
```

## Config

- [How do you deal with project-specific settings (e.g. formatting)](https://github.com/LazyVim/LazyVim/discussions/1878#discussioncomment-7667560)
- [folke/neoconf.nvim feature: Allow conform config](https://github.com/folke/neoconf.nvim/issues/111)

## Key bindings

- [Keymaps](https://www.lazyvim.org/keymaps)

Commonly used key bindings:

Neotest (enable [Neotest test extra](https://www.lazyvim.org/extras/test/core)):

- [Neotest](https://www.lazyvim.org/keymaps#neotest)

## Projects

- [LazyVim util/project extra](https://www.lazyvim.org/extras/util/project)
  uses [project.nvim](https://github.com/ahmedkhalf/project.nvim) for project
  management
- Last commit 2023-04
- `project.nvim` is configured with `manual_mode = true`, so
  [no projects are automatically added](https://github.com/ahmedkhalf/project.nvim/issues/126)
- When in a projectRoot, save project with `:ProjectRoot`
