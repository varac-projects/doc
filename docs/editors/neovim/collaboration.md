# Collaborative editing

## Ethersync

- [GitHub](https://github.com/ethersync/ethersync)
- [Docs](https://ethersync.github.io/)
- Limitation: P2P, no easy way of connecting different networks

### Install

Install daemon:

```sh
pamac install ethersync-bin
```

[Install the Neovim plugin](https://ethersync.github.io/ethersync/neovim.html)

## live-share

- [GitHub](https://github.com/azratul/live-share.nvim)
  - Few commits, not very active
  - Based on [instant](https://github.com/jbyuki/instant.nvim) (last commit 2022)
    and reverse tunneling services like `serveo.net` (abandoned)
    and [localhost.run](https://localhost.run/)
