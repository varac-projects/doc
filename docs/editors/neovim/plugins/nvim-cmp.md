# nvim-cmp

- [GitHub](https://github.com/hrsh7th/nvim-cmp)
- [Docs](https://github.com/hrsh7th/nvim-cmp/blob/main/doc/cmp.txt)
  - [Creating a custom source](https://github.com/hrsh7th/nvim-cmp/blob/main/doc/cmp.txt#L837)
- Default comletion engine in [LazyVim](https://www.lazyvim.org/plugins/coding)
- [List of sources](https://github.com/hrsh7th/nvim-cmp/wiki/List-of-sources)
- [custom source: get_keyword_pattern vs get_trigger_characters](https://github.com/hrsh7th/nvim-cmp/discussions/971)
