# Conform

- [conform.nvim](https://github.com/stevearc/conform.nvim)
- Log file: `~/.local/state/nvim/conform.log`
- [mason-conform](https://github.com/zapling/mason-conform.nvim)
  Automatically install formatters registred with conform.nvim via mason
