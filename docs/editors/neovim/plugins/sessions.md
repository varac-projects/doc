# Session plugins

- [How to get Nvim to remember last open buffers, splits and cursor position ?](https://www.reddit.com/r/neovim/comments/10r2ej3/how_to_get_nvim_to_remember_last_open_buffers/)

## Folke/persistence.nvim

- Default session plgin for LazyVim: [Lazy UI plugins](https://www.lazyvim.org/plugins/util)
- LazyVim uses [folke/persistence.nvim](https://github.com/folke/persistence.nvim)
  for session management
- "automatically saves the active session under `~/.local/state/nvim/sessions` on exit"
- "simple API to restore the current or last session"

## nvim-possession

- [GitHub](https://github.com/gennaro-tedesco/nvim-possession)

### Issues

Using `autoload = true`:

```sh
Failed to run `config` for indent-blankline.nvim

...local/share/nvim/lazy/LazyVim/lua/lazyvim/plugins/ui.lua:193: attempt to index global 'Snacks' (a nil value)

# stacktrace:
  - /LazyVim/lua/lazyvim/plugins/ui.lua:193 _in_ **values**
  - ~/.config/nvim/lua/plugins/possession.lua:13 _in_ **init**
  - ~/.config/nvim/lua/config/lazy.lua:9
  - ~/.config/nvim/init.lua:8
Executing DiagnosticChanged Autocommands for "*"
autocommand <Lua 66: ~/.local/share/nvim/lazy/trouble.nvim/lua/trouble/sources/diagnostics.lua:70>
```

Might be fixed in [recent (>= 0.10.1) Neovim
versions](https://github.com/LazyVim/LazyVim/issues/4688#issuecomment-2453341605).

## auto-session

- [GitHub](https://github.com/rmagatti/auto-session)

## Other

- [suave.lua](https://github.com/nyngwang/suave.lua)
  - Last commit 2023
