# Vim plugins

- [neovimcarft](https://neovimcraft.com/)
  Search through our curated list of neovim plugins
  - [nvim.sh](https://nvim.sh/)
    neovim plugin search from the terminal

## Help

- [which-key](https://github.com/folke/which-key.nvim)
  - How to add [missing keys ?](https://github.com/folke/which-key.nvim/issues/446)

## Plugins to check out

- [nerdy.nvim](https://github.com/2KAbhishek/nerdy.nvim)
  Find Nerd Glyphs Easily 🤓🔭
- [sontungexpt/url-open](https://github.com/sontungexpt/url-open)
  - Open url under cursor in neovim without netrw with default browser of your
    system and highlight url
  - Open the GitHub page for the Neovim plugin mentioned under the cursor

## Organising / knowledge base

- See also [knowlege-base-apps.md](../../../documentation/knowledge-base-apps.md)
- [neorg](https://github.com/nvim-neorg/neorg)
  Neorg (Neo - new, org - organization) is a Neovim plugin designed to reimagine
  organization as you know it.
  - [Nvim-orgmode vs Neorg, which do you use and why?](https://www.reddit.com/r/neovim/comments/15liww4/nvimorgmode_vs_neorg_which_do_you_use_and_why/)

## Tables

<https://github.com/dhruvasagar/vim-table-mode>

Create tables from whitespace delimited text:

```sh
:Tableize/\s\+
```

(uses vim regex).

## Firevim

<https://github.com/glacambre/firenvim>

Issues:

- Doesn't work (well) with Nextcloud Deck

## Icon / emoji pickers

- [icon-picker.nvim](https://github.com/ziontee113/icon-picker.nvim)
  - actively maintained
  - works ✅ !
- [telescope-symbols.nvim](https://github.com/nvim-telescope/telescope-symbols.nvim)
  - last commit 2023-02
- [telescope-emoji.nvim](https://github.com/xiyaowong/telescope-emoji.nvim)
  - couldn't get default paste functionaliy properly working in Lazyvim
  - last commit 20212-12
- [emoji_picker-nvim](https://github.com/WilsonOh/emoji_picker-nvim)
  - last commit 2022-08
