# Tabby ML

- [Website](https://tabby.tabbyml.com/)
- [Docs: Lazyvim](https://tabby.tabbyml.com/docs/extensions/installation/vim#-lazynvim)
- Logs at `~/.tabby-client/agent/logs/current.log`

## Running Tabby as a Language Server

- [Blog article](https://tabby.tabbyml.com/blog/running-tabby-as-a-language-server)

Install:

    npm install --global tabby-agent

Configure config file `~/.tabby-client/agent/config.toml`
