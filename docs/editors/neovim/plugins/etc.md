# Etc plugins

## Learning / training

- [Weyaaron/nvim-training](https://github.com/Weyaaron/nvim-training)
  - A plugin to practice neovim movements

## Etc

- [cheatsheet.nvim](https://github.com/doctorfree/cheatsheet.nvim)
  A cheatsheet plugin for neovim with bundled cheatsheets
  for the editor, multiple vim plugins, nerd-fonts, regex,
  etc. with a Telescope fuzzy finder interface !Weyaaron/nvim-training
