# Neovim terminal / command runners/window

## Terminal / command window

[Run code in Nvim](https://www.reddit.com/r/neovim/comments/u1v677/run_code_in_nvim/)

- [toggleterm](https://github.com/akinsho/toggleterm.nvim)
  - Lua, recently updated
  - manage multiple terminal windows
- [Neoterm](https://github.com/kassio/neoterm)
  - Last commit 2023-03

## Command/task runners

### Overseer

- [GitHub](https://github.com/stevearc/overseer.nvim)
- Built-in support for many task frameworks
  (make, npm, cargo, `.vscode/tasks.json`, etc)
- Supports [Taskfile](https://github.com/stevearc/overseer.nvim/commit/db9a9c482174849eb44a5c05661c21bd42ffbdb2)
- Supports toggleterm
- Default task runner in LazyVim, enabled by [overseer extra](https://www.lazyvim.org/extras/editor/overseer)

Issues:

- [How to use WatchRun example with toggleterm strategy](https://github.com/stevearc/overseer.nvim/discussions/369)

### Other

- [Overseer alternatives](https://github.com/stevearc/overseer.nvim/blob/master/doc/explanation.md#alternatives)

- [runner.nvim](https://github.com/MarcHamamji/runner.nvim)

  - Lua
  - Uses Telescope

- [asyncrun.vim](https://github.com/skywind3000/asyncrun.vim)

  - VimScript/Lua
  - [Different runners](https://github.com/skywind3000/asyncrun.vim#extra-runners)
    like `toggleterm`
  - Issue: [Terminal columns/width reported to be 80 columns](https://github.com/skywind3000/asyncrun.vim/issues/235)
    - No response after 3/4 year :/

- [asynctasks](https://github.com/skywind3000/asynctasks.vim)

  - VimScript/Python
  - Uses asyncrun to introduce `VScode`'s task system to vim
  - Building/running/testing/deploying tasks
  - Project and global config support

- [sniprun](https://github.com/michaelb/sniprun)

  - Lua + Rust
  - Partial selection runner

- [VStasks](https://github.com/EthanJWright/vs-tasks.nvim)
  A telescope plugin that runs tasks similar to VS Code's task implementation.

- [neovim-tasks](https://github.com/Shatur/neovim-tasks)
  A statefull task manager focused on integration with build systems.

### Potentially stale

- [code_runner.nvim](https://github.com/CRAG666/code_runner.nvim)
  - lua
  - Last commit 2023-05
- [projectlaunch](https://github.com/sheodox/projectlaunch.nvim)
  - Last commit 2023-04
- [toggletasks](https://github.com/jedrzejboczar/toggletasks.nvim)
  - Lua
  - Own task syntax :/
  - Uses telescope and toggleterm
  - Last commit 2023-03

### Stale

- [jaq-nvim](https://github.com/is0n/jaq-nvim)
  - Last commit 2022-10
- [vim-dispatch](https://github.com/tpope/vim-dispatch) by tpope
  - vim-script
  - Last commit 2022-01
- [vim-terminator](https://github.com/erietz/vim-terminator)
  - Last commit 2022-04
- [vim-quickrun](https://github.com/thinca/vim-quickrun)
  - vimscript
  - No docs, only vim help
  - Last commit 2022-07
- [mg979/tasks.vim](https://github.com/mg979/tasks.vim)
  - Last commit 2022-08
- [GustavoKatel/tasks.nvim](https://github.com/GustavoKatel/tasks.nvim)
  - Lua
  - package.json, tasksjson (vscode) support
  - Telescope integration
  - Last commit 2022-05
- [yabs.nvim](https://github.com/pianocomposer321/yabs.nvim):
  - rewrite pending
  - Last commit 2022-05
- [neomake](https://github.com/neomake/neomake)
  - vimscript
  - Last commit 2022-02
- [AsyncCommand](https://github.com/idbrii/AsyncCommand)
  - Last commit 2020
- [vim-hooks](https://github.com/ahw/vim-hooks)
  - Last commit 2021
- [Taskfile.nvim](https://github.com/mistweaverco/Taskfile.nvim)
  - Last commit 2018
- [vim-shell-executor](https://github.com/JarrodCTaylor/vim-shell-executor)
  - Last commit 2016
- [AutoRun](https://github.com/pallavagarwal07/AutoRun)
  - Last commit 2015
