# Codeium

- [Official vim script plugin](https://codeium.com/vim_tutorial)
- [Exafunction/codeium.vim](https://github.com/Exafunction/codeium.vim)
- [LazyVim codeium extra](https://www.lazyvim.org/extras/coding/codeium)
