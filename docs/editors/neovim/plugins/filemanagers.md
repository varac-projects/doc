# Neovim file file manager plugins

- [A Curated List of File Explorers for Nvim](https://jdhao.github.io/2022/02/27/nvim_file_tree_explorer/)

## Telescope

<https://github.com/nvim-telescope/telescope.nvim#usage>

```vim
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
```

- [easypick](https://github.com/axkirillov/easypick.nvim)
  create Telescope pickers from arbitrary console commands

### Telescope extensions

[Community extensions](https://github.com/nvim-telescope/telescope.nvim/wiki/Extensions)

- [intelligent prioritization when selecting files from your editing history](https://github.com/nvim-telescope/telescope-frecency.nvim)

Write custom extensions:

- Very small example extension: [telescope-changed-files](https://github.com/axkirillov/telescope-changed-files)

### nvim-tree

<https://github.com/kyazdani42/nvim-tree.lua>

Downside: Only for nvim

### nnn

First install [nnn](https://github.com/jarun/nnn) as documented in [file-managers.md](../../../etc/file-managers.md)

Then use the [lua nnn.vim](https://github.com/luukvbaal/nnn.nvim) plugin

Downside: Icon support must be patched in !
