# Neovim markdown plugins

<!-- toc -->

- [Neovim Markdown extra](#neovim-markdown-extra)
- [Telescope markdown plugins](#telescope-markdown-plugins)
- [Table of contents](#table-of-contents)
  - [jonschlinkert/markdown-toc](#jonschlinkertmarkdown-toc)
  - [Marksman language server](#marksman-language-server)
  - [Other TOC generators](#other-toc-generators)
- [General markdown plugins](#general-markdown-plugins)
- [Other markdown preview plugins](#other-markdown-preview-plugins)
  - [peek](#peek)
  - [Tables](#tables)

<!-- tocstop -->

Issues:

- Still no keymap for increasing or decreasing Markdown headers.
  `vim-markdown` plugin does that but it's VimScript and probably
  mess too much with existing plugins

## Neovim Markdown extra

- [Neovim Markdown extra](https://www.lazyvim.org/extras/lang/markdown)

Included plugins:

- [Headlines.nvim](https://github.com/lukas-reineke/headlines.nvim)
  - Included in LazyVim Markdown extra
  - This plugin adds highlights for text filetypes, like markdown, orgmode, and neorg.
    - Background highlighting for headlines
    - Background highlighting for code blocks
    - Whole window separator for horizontal line
    - Bar for Quotes
- [markdown-preview.nvim](https://github.com/iamcco/markdown-preview.nvim)
  - NodeJS
- Installs [igorshubovych/markdownlint-cli](https://github.com/igorshubovych/markdownlint-cli)
  from the [Mason registry](https://github.com/mason-org/mason-registry/blob/main/packages/markdownlint/package.yaml),
  configured by [nvim-lint](https://www.lazyvim.org/extras/lang/markdown#nvim-lint-optional)

## Telescope markdown plugins

- [crispgm/telescope-heading.nvim](https://github.com/crispgm/telescope-heading.nvim)
  - Allows you to switch between headings

## Table of contents

### jonschlinkert/markdown-toc

- [GitHub](https://github.com/jonschlinkert/markdown-toc)
- API and CLI for generating a markdown TOC (table of contents) for a README or any markdown files
- Default TOC generator bundled in the [Lazyvim markdown extra](https://www.lazyvim.org/extras/lang/markdown)
- Insert `<!-- toc -->` into the head of a Markdown document,
  it's updated automatically on save

### Marksman language server

- [Github](https://github.com/artempyanykh/marksman)
- Automatically installed via Mason with LazyVim Markdown extra
- [Marksman features](https://github.com/artempyanykh/marksman/blob/main/docs/features.md)
- [LunarVim: Improve Markdown Editing with Marksman](https://medium.com/@chrisatmachine/lunarvim-improve-markdown-editing-with-marksman-739d06c73a26)
- Marksman includes a code action to produce a [Table of Contents](https://github.com/artempyanykh/marksman/blob/main/docs/features.md)
  - Usage: `<leader>ca`

### Other TOC generators

- [mzlogin/vim-markdown-toc](https://github.com/mzlogin/vim-markdown-toc)
  - VimScript
  - Existing table of contents will auto update on save by default
  - Last commit 2022-08
- [richardbizik/nvim-toc](https://github.com/richardbizik/nvim-toc)
  - Lua
  - Last commit 2023-07
- [ajorgensen/vim-markdown-toc](https://github.com/ajorgensen/vim-markdown-toc)
  - VimScript
  - Stale: last commit 2020

## General markdown plugins

- [tadmccorkle/markdown](https://github.com/tadmccorkle/markdown.nvim#links)

  - Lua

- [preservim/vim-markdown](https://github.com/preservim/vim-markdown)
  - VimScript
  - Syntax highlighting, matching rules and
    mappings for the original Markdown and extensions
  - Useful for decreasing or increasing Markdown headers
  - Generate TOC
- [nvim-FeMaco.lua](https://github.com/AckslD/nvim-FeMaco.lua)
  - "A small plugin allowing to edit injected language trees with correct
    filetype in a floating window. This allows you to use all of your config
    for your favorite language"

## Other markdown preview plugins

- [glow.nvim](https://github.com/ellisonleao/glow.nvim)
  - A (text-based) markdown preview directly in your neovim
  - [glow-hover](https://github.com/JASONews/glow-hover.nvim)
    Leverage glow to display LSP hover responses.

### [peek](https://github.com/toppair/peek.nvim)

- [Github](https://github.com/toppair/peek.nvim)
- Last commit 2023-04
- Dependency: Deno javascript runtime needed

  live update
  synchronized scrolling
  github-style look
  TeX math
  Mermaid diagrams

### Tables

- [markdown-table-mode.nvim](https://github.com/Kicamon/markdown-table-mode.nvim)
