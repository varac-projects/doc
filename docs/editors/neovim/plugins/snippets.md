# Vim snippets

## Snippet engines

- [nvim-snippets](#nvim-snippets)
- [Luasnip](https://github.com/L3MON4D3/LuaSnip)
  - Lua
  - Available as [Lazyim extra](https://www.lazyvim.org/extras/coding/luasnip)
  - Supports: VScode, Snipmate, Lua snippets
- [nvim-cmp supported snippet engines](https://github.com/hrsh7th/nvim-cmp/wiki/List-of-sources#snippets)
- [nvim-snippy](https://github.com/dcampos/nvim-snippy)
  - SnipMate compatibles
  - Supports `honza/vim-snippets`
- [vsnip](https://github.com/hrsh7th/vim-vsnip)
  - VimScript
- [neosnippet](https://github.com/Shougo/neosnippet.vim)
  - Vimscript
  - `Note: Active development on neosnippet.vim has stopped.`
    `The only future changes will be bug fixes.`
- Ultisnips, see below

### nvim-snippets

- [GitHub](https://github.com/garymjr/nvim-snippets)
- "Snippet support using native neovim snippets"
- Uses vim.snippet under the hood for snippet expansion
- Default snippet engine in [LazyVim](https://www.lazyvim.org/plugins/coding#nvim-snippets)
- Supports vscode style snippets, i.e. [friendly-snippets](https://github.com/rafamadriz/friendly-snippets)
- [Example snippet](https://github.com/garymjr/nvim-snippets?tab=readme-ov-file#example-snippet)
- Snippet search path: `~/.config/nvim/snippets`

### UltiSnips

- [Github](https://github.com/SirVer/ultisnips),
- Python
- [Bad docs](https://github.com/SirVer/ultisnips/blob/master/doc/UltiSnips.txt)

Show list of snippets available for filetype: `<F2>`
Otherwise `<tab>` will complete snippets.

## Snippet syntax

### Vscode / LSP syntax

- [LSP snippetSyntax docs](https://github.com/microsoft/language-server-protocol/blob/main/snippetSyntax.md)
- Snippet collections: [friendly-snippets](https://github.com/rafamadriz/friendly-snippets)
  Set of preconfigured snippets for different languages
- Disadvantage: JSON :/
- [Example shell snippets](https://github.com/rafamadriz/friendly-snippets/blob/main/snippets/shell/shell.json)

### Snipmate snippets

- Snipmate snippet syntax is easy to read and maintain
- Can't find a syntax documentation source of truth
- [LuaSnip docs for Snipmate syntax](https://github.com/L3MON4D3/LuaSnip#add-snippets)
- Original [vim-snipmate](https://github.com/garbas/vim-snipmate) plugin
- [honza/vim-snippets](https://github.com/honza/vim-snippets)
- [Example shell snippets](https://github.com/honza/vim-snippets/blob/master/snippets/sh.snippets)

### Lua snippets

- Available as [Lazyim extra](https://www.lazyvim.org/extras/coding/luasnip)
- Horrible [Lua snippet syntax](https://github.com/L3MON4D3/LuaSnip/blob/master/Examples/snippets.lua#L191)
- [LuaSnip-snippets.nvim](https://github.com/molleweide/LuaSnip-snippets.nvim)
  Neovim plugin that provides snippets for the LuaSnip plugin
  - [LuaSnip-snippets Python snippet example](https://github.com/molleweide/LuaSnip-snippets.nvim/blob/main/lua/luasnip_snippets/snippets/python.lua#L88)

### Ultisnip syntax

- Extensible
- [Example snippets from honza/vim-snippets](https://github.com/honza/vim-snippets/blob/master/UltiSnips/all.snippets)
