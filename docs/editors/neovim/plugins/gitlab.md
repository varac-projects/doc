# Neovim Gitlab plugins

## gitlab.nvim (harrisoncramer)

Review Gitlab MRs from within the editor

- [Blogpost](https://harrisoncramer.me/building-a-gitlab-client-for-neovim/)
- [Github](https://github.com/harrisoncramer/gitlab.nvim)

## Official GitLab plugin for Neovim - gitlab.vim

Supports [Code suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html)

- [Website ](https://docs.gitlab.com/ee/editor_extensions/neovim/)
- [Docs](https://gitlab.com/gitlab-org/editor-extensions/gitlab.vim/-/blob/main/README.md)
