# Neovim plugin managers

## Lazy

- [GitHub](https://github.com/folke/lazy.nvim)
- [Structuring Your Plugins](https://github.com/folke/lazy.nvim#-structuring-your-plugins)

## packer

- [packer quick start](https://github.com/wbthomason/packer.nvim#quickstart)
  - Packs are installed to `~/.local/share/nvim/site/pack/packer/start`

## Mason

- [GitHub](https://github.com/williamboman/mason.nvim)
- Install path: `~/.local/share/nvim/mason/bin`
- [Configuration](https://github.com/williamboman/mason.nvim?tab=readme-ov-file#configuration)
- Log: `:MasonLog` or `~/.local/state/nvim/mason.log`

### Mason registry

- [GitHub](https://github.com/mason-org/mason-registry/tree/main/packages)
- [CONTRIBUTING.md](https://github.com/mason-org/mason-registry/blob/main/CONTRIBUTING.md)
  - i.e. [testing](https://github.com/mason-org/mason-registry/blob/main/CONTRIBUTING.md#testing)
- i.e. [markdownlint](https://github.com/mason-org/mason-registry/blob/main/packages/markdownlint/package.yaml)

### Issues

- [Lockfile or ability to freeze a certain package version](https://github.com/williamboman/mason.nvim/issues/1701)
  - [You can install a specific LSP version](https://github.com/williamboman/mason.nvim/issues/425#issuecomment-1248738710)
    using `:MasonInstall typescript-language-server@0.11.2`, but it still will
    be included in the next update
  - [mason-lock.nvim](https://github.com/zapling/mason-lock.nvim)
    - Plugin: Lockfile suppport for mason.nvim
