# Remove development

[Awesome Neovim: Remote development](https://www.trackawesomelist.com/rockerBOO/awesome-neovim/readme/#remote-development)

- [netman.nvim](https://github.com/miversen33/netman.nvim)
  - Integrates into [neo-tree](https://github.com/nvim-neo-tree/neo-tree.nvim/)
  - multiple protocols
  - SSH provider can't deal with `Include` statement
    - [FR: Include Syntax Support](https://github.com/miversen33/netman.nvim/issues/208)
- [remote-sshfs.nvim](https://github.com/nosduco/remote-sshfs.nvim)
  - mounts files with sshfs
  - dependency: telescope
- [remote.nvim](https://github.com/niuiic/remote.nvim)
  - only ssh
  - requires a config file with remote file locations
- [distant.nvim](https://github.com/chipsenkbeil/distant.nvim)
  - Based on [distant](https://github.com/chipsenkbeil/distant)
    - Last commit 2023-11
