# Vim distribuitions

- [Exploring the Top Neovim Distributions: LazyVim, LunarVim, AstroVim, and NVChad](https://medium.com/@adaml.poniatowski/exploring-the-top-neovim-distributions-lazyvim-lunarvim-astrovim-and-nvchad-which-one-reigns-3adcdbfa478d)
- [Awesome Neovim Preconfigured Configuration list](https://github.com/rockerBOO/awesome-neovim#preconfigured-configuration)
- Below are distributions listed with >1k GitHub stars

## Neovim only

- [LazyVim](./lazyvim.md)
- [NvChad](https://github.com/NvChad/NvChad)
  - 25k stars
  - Focussed on speed and performance
- [LunarVIM](https://www.lunarvim.org/)
  - [GitHub](https://github.com/LunarVim/LunarVim)
  - "An IDE layer for Neovim with sane defaults. Completely free and community driven."
  - 18k stars
  - Configuration abstraction
- [AstroVIM](https://astronvim.com/)
  - [GitHub](https://github.com/AstroNvim/AstroNvim)
    - 12k stars
  - "AstroNvim is an aesthetic and feature-rich neovim config
    that is extensible and easy to use with a great set of plugins"
  - [AstroVIM vs Neovim](https://www.reddit.com/r/neovim/comments/12aqs6w/comment/jevngs3/?utm_source=reddit&utm_medium=web2x&context=3)
- [kickstart.nvim](https://github.com/nvim-lua/kickstart.nvim)
  A launch point for your personal nvim configuration

## Vim + Neovim

- [SpaceVim](https://github.com/SpaceVim/SpaceVim)
  - 19.3k stars
