# Neovim

- [awesome-neovim](https://www.trackawesomelist.com/rockerBOO/awesome-neovim/readme/)
- [Arch wiki: Neovim](https://wiki.archlinux.org/title/Neovim)

## Install

### Arch

See also `./spellcheck.md` for dictionary deps

## Config

Default config locations:

- `~/.config/nvim`
- `~/.local/share/nvim`
- `~/.local/state/nvim`
- `~/.cache/nvim`

These can be changed by using the [NVIM_APPNAME](https://neovim.io/doc/user/starting.html#%24NVIM_APPNAME)
env variable.
This obsoletes config managers like [cheovim](https://github.com/NTBBloodbath/cheovim).

i.E. to start with a minimal/clean config:

```sh
NVIM_APPNAME=nvim_clean nvim
```

### Lazyvim

See [lazyvim.md](./lazyvim.md)

## Usage

### Getting help

#### Cheatsheet

Using [cheatsheet.nvim](https://github.com/sudormrfbin/cheatsheet.nvim):

Type `<leader>?` (which means `\` + `?`) to navigate through cheatsheet, or
use `:Cheatsheet`

Custom Cheatsheet is at `~/.config/nvim/cheatsheet.txt` and can get edited
with `:CheatsheetEdit`, and search with Telescope typing `<leader>?custom`.

#### Neovim.md

Open with `<F2>`

## Debug

```sh
:checkhealth provider
```

Debug / verbose startup:

```sh
nvim -V/tmp/nvim.log ...
```

## VIM in general

- [Vi -Tips](https://www.youtube.com/watch?v=wlR5gYd6um0&list=PL8tzorAO7s0jy7DQ3Q0FwF3BnXGQnDirs)
- [Vim Adventures: Learn VIM while playing a game](https://vim-adventures.com/)

## Help

`<F1>` or `:help`

## Shortcuts

Show default shortcuts: `:help index`
Show custom shortcuts: `:map`

## modeline

<http://vim.wikia.com/wiki/Modeline_magic>
<http://vimdoc.sourceforge.net/htmldoc/options.html#modeline>

i.e. add to json file;

```vim
// vim:ts=4:sw=4:et
```

or in a config file:

```sh
# vim:filetype=sway:foldmethod=marker:foldlevel=0
```

## (Re-)wrap existing text

[Wrap Existing Text at 80 Characters in Vim](https://thoughtbot.com/blog/wrap-existing-text-at-80-characters-in-vim):

Select text and use `gq` to rewrap.
