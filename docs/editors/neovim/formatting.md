# Neovim formatting

## conform

- [GitHub](https://github.com/stevearc/conform.nvim)
- Default [Lazyvim formatter](https://www.lazyvim.org/plugins/formatting#conformnvim)
- [Supported formatters](https://github.com/stevearc/conform.nvim?tab=readme-ov-file#formatters)
- Config: `~/.config/nvim/lua/plugins/conform.lua`

## Archived: Null-ls

> Use Neovim as a language server to inject
> LSP diagnostics, code actions, and more via Lua.

- [null-ls](https://github.com/jose-elias-alvarez/null-ls.nvim)
- lua, ~1000 commits
- configured in `~/.config/nvim/conf.d/linting.vim`
- log at `/home/varac/.cache/nvim/null-ls.log`
- [Debug log](https://github.com/jose-elias-alvarez/null-ls.nvim#how-do-i-enable-debug-mode-and-get-debug-output)

Issues:

- [How to disable formatting on save per session ?](https://github.com/jose-elias-alvarez/null-ls.nvim/discussions/894)

## Other

- [neoformat](https://github.com/sbdchd/neoformat) (vimscript)
- [formatter](https://github.com/mhartington/formatter.nvim) (lua)
- [format.nvim](https://github.com/lukas-reineke/format.nvim) (lua)
