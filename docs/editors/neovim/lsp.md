# Neovim LSP

- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
  - [All supported languages](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md)
  - Config in `~/.config/nvim/lua/plugins.lua`
  - Logfile at `~/.local/state/nvim/lsp.log`
  - See `:LspInfo`

## LSP plugins

- [nvim-lightbulb](https://github.com/kosayoda/nvim-lightbulb)

## LSP issues

- [vscode-yaml](https://github.com/redhat-developer/vscode-yaml)
- [Up to date yaml schemas](https://github.com/yannh/kubernetes-json-schema)

### yaml-language-server

- [yamlls config for nvim-lspconfig](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#yamlls)
- Still missing: [Support for .tpf (Helm chart Yaml-based template files)](https://github.com/redhat-developer/yaml-language-server/issues/220)
- Kubernetes support:
  - [Set specific Kubernetes Schema Version](https://github.com/redhat-developer/yaml-language-server/issues/211)
  - [Remove built in kubernetes support](https://github.com/redhat-developer/yaml-language-server/issues/307)
  - [yaml-language-server settting yaml.schemaStore.url not working](https://github.com/neovim/nvim-lspconfig/issues/1207)
  - [yamlls is active when filetype is "helm"](https://github.com/neovim/nvim-lspconfig/issues/2252)

Enable yaml-language-server autocompletion by setting the right json schema,
either by a comment at the top of the document like:

```sh
# yaml-language-server: $schema=https://json.schemastore.org/helmfile.json
```

or by configuring [dotfiles: nvim-lspconfig](https://0xacab.org/varac-projects/dotfiles/-/blob/main/.config/nvim/lua/plugins/lsp.lua?ref_type=heads#L189)
