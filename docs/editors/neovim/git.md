# Neovim git integration

- [Best Git Integration for Neovim?](https://www.reddit.com/r/neovim/comments/v31ft4/best_git_integration_for_neovim/)
- [neogit](https://github.com/NeogitOrg/neogit)
- [git-blame.nvim](https://github.com/f-person/git-blame.nvim)
- Stale: [nvim-blame-line](https://github.com/tveskag/nvim-blame-line)

## gitui

- see [git/tools.md](../../git/tools.md)
- [Availabe as LazyExtra](https://www.lazyvim.org/extras/util/gitui)

## Lazygit

- Integrated into Lazyvim (see [Requirements](https://www.lazyvim.org/#%EF%B8%8F-requirements))
- Use [toggleterm + lazygit](https://github.com/akinsho/toggleterm.nvim#custom-terminals)
