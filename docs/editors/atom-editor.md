# Atom editor

https://github.com/atom/atom/

## Plugins

### Markdown preview

* Currently installed/using:
  [Markdown Preview Enhanced](https://atom.io/packages/markdown-preview-enhanced)
* https://shd101wyy.github.io/markdown-preview-enhanced/#/
* `Ctrl+Shift+m` to open preview
* Can also render
  [diagrams (mermaid, graphviz etc)](https://shd101wyy.github.io/markdown-preview-enhanced/#/diagrams)
* Can render [fontawesome](https://fontawesome.com/icons).

Currently installed packages:

* markdown-preview-enhanced@0.17.3
* revealjs-preview@0.3.1
* slides-preview@0.3.1
* teletype@0.13.4
* script

## Collaborative editing

https://blog.atom.io/2017/11/15/code-together-in-real-time-with-teletype-for-atom.html

## Vim mode

https://medium.com/team40/how-i-made-atom-into-vim-3717b6d50822
