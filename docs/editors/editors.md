# Editors

## Helix

- [Website](https://helix-editor.com/)
- [GitHub](https://github.com/helix-editor/helix)
- 33k stars
- Rust based vim re-write
- More integrated
- [AUR package: helix](https://archlinux.org/packages/extra/x86_64/helix/)
- [helix-vim](https://github.com/LGUG2Z/helix-vim)
  A Vim-like configuration for Helix (key bindings etc.)
- [Docs](https://docs.helix-editor.com/title-page.html)
  - [Language Server Configurations](https://github.com/helix-editor/helix/wiki/Language-Server-Configurations)
- [Log file](https://github.com/helix-editor/helix/wiki/FAQ#access-the-log-file)
  `~/.cache/helix/helix.log`

## Compared to Neovim / Leazvim

- [Migrating from Vim](https://github.com/helix-editor/helix/wiki/Migrating-from-Vim)
- Features missing
  - [Plugin system](https://github.com/helix-editor/helix/wiki/FAQ#how-to-write-plugins-is-there-a-plugin-system-in-place-yet)
  - [Natively testrunner](https://github.com/helix-editor/helix/issues/5168#)

### Config

- [Configuration](https://docs.helix-editor.com/configuration.html)
- Config file: `~/.config/helix/config.toml`

### Setup

Fetch and install LSP grammars:

```sh
hx --grammar fetch
hx --grammar build
```

Show health information:

```sh
hx --health
```

### Usage

```sh
hx --tutor
```

## Other editors

- [Kakoune](http://kakoune.org/)
  - [GitHub](https://github.com/mawww/kakoune)
  - 10k stars
  - "Modal editor · Faster as in fewer keystrokes · Multiple selections ·
    Orthogonal design"

## Neovim curated configs

see [neovim/distributions.md](neovim/distributions.md)
