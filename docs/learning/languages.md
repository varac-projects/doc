# Natural language learning

- [Anki](https://apps.ankiweb.net/)
  - "Powerful, intelligent flash cards.
    Remembering things just became much easier."
  - [Ankidroid](https://ankidroid.org/docs/help.html)
    - [GitHub](https://github.com/ankidroid/Anki-Android)
    - [F-Droid](https://f-droid.org/en/packages/com.ichi2.anki/)
