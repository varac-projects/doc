# Gotify

* [Docs](https://gotify.net/docs)
* [FDroid: Gotify](https://f-droid.org/en/packages/com.github.gotify/)
* [Gotify truecharts chart](https://artifacthub.io/packages/helm/truecharts/gotify)

## Clients

* [Python cli client](https://github.com/schwma/gotify-push)
* [gotify-indicator](https://ubuntuhandbook.org/index.php/2020/02/gotify-indicator-send-receive-messages-in-ubuntu-desktop/)

### Matrix

* [gotify-matrix-bot](https://github.com/Ondolin/gotify-matrix-bot)
* [matrix-gotify](https://gitlab.com/Sorunome/matrix-gotify) (stale)

## Usage

    curl -F "title=my title" -F "message=my message" -F "priority=5" \
      "https://gotify.oas.varac.net/message?token=$(gopass show --password token/gotify.oas.varac.net/apps/cli-test)"
