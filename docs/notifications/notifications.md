# Notification services

## Generic

- [gotify](./gotify.md)
- [shoutrrr](https://containrrr.dev/shoutrrr/v0.8/)
  - Golang
  - [Multiple backends](https://containrrr.dev/shoutrrr/v0.8/services/overview/)
  - No helm chart so far
- [notifiarr](https://github.com/Notifiarr/notifiarr)
  - [notifiarr truecharts chart](https://artifacthub.io/packages/helm/truecharts/notifiarr)

### ntfy

- [ntfy docs](https://docs.ntfy.sh/)
- [FDroid: nfty](https://f-droid.org/en/packages/io.heckel.ntfy/)
  Last release 2022-12
- [ntfy truecharts chart](https://artifacthub.io/packages/helm/truecharts/ntfy)
- [ntfy authentication](https://github.com/binwiederhier/ntfy/issues/19)
- [Fedilab Push notifications](https://fedilab.app/wiki/features/push-notifications/)

Usage:

    curl -d "Backup successful 😀" ntfy.sh/mytopic
