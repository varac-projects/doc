# Arr

- [awesome-arr](https://github.com/Ravencentric/awesome-arr)
- [Servarr Wiki](https://wiki.servarr.com/)
- [TRaSH Guides](https://trash-guides.info/)
  Here you will find guides mainly for Sonarr/Radarr/Bazarr
  and everything related to it.

## Components

- [exportarr](https://github.com/onedr0p/exportarr)
  AIO Prometheus Exporter for Bazarr, Prowlarr, Lidarr, Readarr, Radarr, and Sonarr
- [lidarr](./audio/lidarr.md)
- [arr-scripts](https://github.com/RandomNinjaAtk/arr-scripts/tree/main)
  - Extended Container Scripts,
    Designed to be easily implemented/added to
    [Linuxserver.io](https://www.linuxserver.io/) containers.
- [Recyclarr](https://recyclarr.dev/wiki/)
  - "command-line application that will automatically synchronize recommended settings
    from the TRaSH guides to your Sonarr/Radarr instances."

### Unpacking

- [auto-unrar](https://github.com/Nicxx2/auto-unrar)
- [rar2fs](https://github.com/hasse69/rar2fs)
  FUSE file system for reading RAR archives

## Etc

- [deemix](https://wiki.evoseedbox.com/)
  Discontinued

## Containers

- [hotio.dev](https://hotio.dev/containers/base/)
