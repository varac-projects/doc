# Music servers

## Snapcast

- [GitHub](https://github.com/badaix/snapcast)
- Multiroom client-server audio player
- Comparism: [Why not Logitech Media Server / slimserver](https://github.com/badaix/snapcast/issues/600#issuecomment-625809831)
- [snapdroid android client](https://github.com/badaix/snapdroid)
- [Home Assistant integration](https://www.home-assistant.io/integrations/snapcast/)

### ESP32 clients

- [CarlosDerSeher/snapclient](https://github.com/CarlosDerSeher/snapclient?tab=readme-ov-file)
  - Fork of original, stale [jorgenkraghjakobsen/snapclient](https://github.com/jorgenkraghjakobsen/snapclient)
- [Blog article](https://www.crowdsupply.com/sonocotta/esparagus-media-center/updates/esparagus-and-snapcast)
- [Raspiaudio forum: Snacast firmware](https://forum.raspiaudio.com/t/snacast-firmware/917/4)
- [Install on Muse Luxe](https://forum.raspiaudio.com/t/snacast-firmware/917/10)

## Lyrion (formerly Logitech Media Server)

- see [slimproto.md](./slimproto.md)

## \*sonic compatible

- [airsonic-advanced](https://github.com/airsonic-advanced/airsonic-advanced)
  - Java
- [Good comparism](https://dustri.org/b/airsonic-a-self-hosted-jukebox-that-sucks-less.html)
- [Apps/clients for \*sonic servers](https://airsonic.github.io/docs/apps/)
- [gonic](https://github.com/sentriz/gonic): go implementation
- [navidrome](https://github.com/deluan/navidrome): go implementation
- [airsonic-advanced](https://github.com/airsonic-advanced/airsonic-advanced)
- [anarcat/debian-airsonic](https://github.com/anarcat/debian-airsonic/)
- [supysonic](https://github.com/spl0k/supysonic)
  - [packaged in debian](https://packages.debian.org/bullseye/supysonic)

Helm charts:

- [airsonic](https://artifacthub.io/packages/helm/k8s-at-home/airsonic)
- [gonic](https://artifacthub.io/packages/helm/k8s-at-home/gonic)

Unmaintained:

- [airsonic](https://github.com/airsonic/airsonic)

## Funkwhale

- [Website](https://funkwhale.audio/)
- [Funkwhale](https://dev.funkwhale.audio/funkwhale/funkwhale)
- Python, federation
- Recent [releases](https://dev.funkwhale.audio/funkwhale/funkwhale/-/releases)
