# Slimproto (a.k.a Squeezebox)

## Server

### Music-assistant

- See [music-assistant.md](../../smarthome/home-assistant/music-assistant.md)
  for slimproto integration

### Lyrion Music Server

- [Lyrion Music Server](https://lyrion.org/)
  - formerly `Logitech Media Server`
- [GitHub: lms-community/slimserver](https://github.com/lms-community/slimserver)
- [lmscommunity/logitechmediaserver container image](https://hub.docker.com/r/lmscommunity/logitechmediaserver)
- [Logitech Media Server in squeezebox wiki](https://wiki.slimdevices.com/index.php/Logitech_Media_Server)

Plugin:

- [Spotty-Plugin](https://github.com/michaelherger/Spotty-Plugin)

#### Helm chart

- [LMS truecharts chart](https://artifacthub.io/packages/helm/truecharts/logitech-media-server)
  - [values.yaml](https://github.com/truecharts/charts/blob/master/charts/stable/logitech-media-server/values.yaml)

## Clients

### Squeezelite

- [squeezelite Github](https://github.com/ralph-irving/squeezelite)
  - [squeezebox forum: 3rd Party Software](https://forums.slimdevices.com/forumdisplay.php?4-3rd-Party-Software)
- [squeezelite-esp32 Github](https://github.com/sle118/squeezelite-esp32)
  Tried to update any any other newer firmware than the `squeezeliteML.bin (v0.4)` from [RASPIAUDIO/squeezelite-MuseLuxe](https://github.com/RASPIAUDIO/squeezelite-MuseLuxe)
- Squeezelite Hardware
  - For `MuseLuxe` and `MuseRadio` see [hardware/raspiaudio.md](../../hardware/raspiaudio.md)

### PiCorePlayer

- [Website](https://www.picoreplayer.org/)
