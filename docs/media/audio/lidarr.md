# Lidarr

- [Website](https://lidarr.audio/)
- [Github](https://github.com/Lidarr/Lidarr/)
- [Wiki / docs](https://wiki.servarr.com/lidarr)
- [linuxserver lidarr image](https://docs.linuxserver.io/images/docker-lidarr/)

## Usage

- [Importing existing library or files](https://wiki.servarr.com/lidarr/quick-start-guide#importing-existing-library-or-files)

## Extensions

- [arr-scripts](https://github.com/RandomNinjaAtk/arr-scripts)
  - Successor of `docker-lidarr-extended` and `AMD`

Discontinued and archived:

- [docker-lidarr-extended](https://github.com/RandomNinjaAtk/docker-lidarr-extended)
- [AMD - Automated Music Downloader](https://github.com/RandomNinjaAtk/docker-amd)

## Helm chart

- [truecharts/lidarr](https://artifacthub.io/packages/helm/truecharts/lidarr)
  - Uses
    [onedr0p/lidarr-develop](https://github.com/onedr0p/containers/tree/main/apps/lidarr) image

## Issues

Missing functionality:

- [Various artists/compilations](https://github.com/lidarr/Lidarr/issues/207)
- [Allow multiple roots for artists](https://github.com/Lidarr/Lidarr/issues/1968)
- [Ability to add sampler series](https://github.com/lidarr/Lidarr/issues/582)
