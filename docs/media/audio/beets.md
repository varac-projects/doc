# Beets

> Beets is the media library management system for obsessive-compulsive music geeks.

- [Website](http://beets.io/)
- [GitHub](https://github.com/beetbox/beets)
- [Docs](https://beets.readthedocs.io/en/latest/guides/main.html)
- Config: `~/.config/beets/config.yaml`
- DB: `~/Media/Audio/beets`
- Music directory managed by beets: `~/Music`
- beets log: `~/var/log/beet.log`

## Tagging workflow

Incoming, unttagged music/audiobooks: `~/Audio/incoming`

Only test what beets would do:

```sh
beet import --pretend Album01
```

Import a "singleton" (single track):

```sh
beet import -s
```

## Manual tagging

Auto-detection doesn't work, ugh :/

[Using the Auto-Tagger: Choices](https://beets.readthedocs.io/en/stable/guides/tagger.html#choices)

> --> "Use as is" and edit tags later.

Doesn't work, it doesn't automatically apply the genre:

```sh
beet import --set genre="Electro" Album01
```

So simply import it to the general library and retag it later with the genre:

```sh
beet import Artist/Album01
beet modify Album01 genre="Electro"
```

Deal with non-matchings titles:

```sh
cd ~/Media/incoming
down_on_spot 'https://open.spotify.com/album/...'
beet import -s Album01/Track01
```

## Edit tags

I.e. add `Children's Music` tag:

```sh
export TAG="Children's Music"
```

Find album:

```sh
beet ls -a Album01
```

Modify tags:

```sh
beet modify Album01 $TAG
```

Or manually, edit album tag, and add `genre: Children's Music`:

```sh
beet edit -a Album01
```

Album gets moved to location based on tag configured in config file, verify with:

```sh
beet ls -a -p Album01
```

Re-import album, i.e. when special characters let Phonybox fail to play a folder:

```sh
beet -vv import -L Album01
beet modify Artist genre=$TAG
```

or even better:

```sh
beet edit -a Album01
```

## Manual cover download

Only needed if automatic embedding during import from `FetchArt` and `EmbedArt`
plugins fail, otherwise the cover will get placed in the resulting folder after
import.

```sh
glyrc cover --artist Artist --album Album01
```

Or:

```sh
~/bin/glyrc-cover-download.sh
```

Embed cover image:

```sh
beet embedart -f /tmp/album.jpg album::Album01
```

## Sync to Phonybox

```sh
rsync_phoniebox.sh
```

## Print CD-Cover

```sh
glabels-3 cdcover.glabels
```
