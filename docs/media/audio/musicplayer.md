# Linux music players

## clementine

- [Website](https://www.clementine-player.org/)
- [GitHub](https://github.com/clementine-player/Clementine)
- [last release 2016](https://github.com/clementine-player/Clementine/issues/5958)
- Debian package without Soundcloud support

## Lollypop

- [Website](https://wiki.gnome.org/Apps/Lollypop)
- [Won't integrate soundcloud](https://gitlab.gnome.org/World/lollypop/-/issues/1367)

## Rhytmbox

- [Website](https://help.gnome.org/users/rhythmbox/stable/index.html)
- [Gitlab](https://gitlab.gnome.org/GNOME/rhythmbox)
- Soundcloud search, but no bookmarks/favorites
