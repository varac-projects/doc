# Alsa

- Manpage: `man alsactl`
- Config file: `/var/lib/alsa/asound.state`
- Devices: [What Do ALSA Devices Like hw0,0 Mean?](https://www.baeldung.com/linux/alsa-devices-names)

List available soundcards:

```sh
aplay -l
cat /proc/asound/cards
```

Play on device:

```sh
aplay --device="hw:0,0" /usr/share/sounds/alsa/Front_Left.wav
```

- [Speaker test](https://www.mythtv.org/wiki/Using_ALSA%27s_speaker-test_utility)

## amixer

- `amixer` is the cli, `alsamixer` the ncurses frontent

Saves current mixer settings to `/var/lib/alsa/asound.state`, which will
get restored on reboot:

```sh
sudo alsactl store
```

Show controls:

```sh
amixer scontrols
```
