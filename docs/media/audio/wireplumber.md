# Wireplumber

* [Arch wiki: wireplumber](https://wiki.archlinux.org/title/WirePlumber)
* [Wireplumber docs](https://pipewire.pages.freedesktop.org/wireplumber/index.html)
  * [Bluetooth docs](https://pipewire.pages.freedesktop.org/wireplumber/configuration/bluetooth.html)

## Installation

Install the wireplumber package. It will conflict with other PipeWire Session Managers and make sure they are uninstalled.

    sudo pacman -S wireplumber

## Configuration

* Root config dir: `~/.config/wireplumber`
  * Bluetooth config sub dir: `bluetooth.lua.d`
* State: `/home/varac/.local/state/wireplumber/default-routes`

Restart user wireplumber service after config changes:

    systemctl --user restart wireplumber.service

## Usage

Get device statuses:

    $ wpctl status
    …
    Audio
    ├─ Devices:
    │      46. Soundcore Life P2 Mini              [bluez5]
    …

    $ wpctl inspect 46
    …

## Bluetooth devices

* Wireplumber has [bluetooth profile auto-switching enabled by default](https://wiki.archlinux.org/title/PipeWire#Automatic_profile_selection)
  It can automatically switch between HSP/HFP and A2DP profiles whenever an input stream is detected.

## Trobuleshooting

* [Debug logging](https://pipewire.pages.freedesktop.org/wireplumber/daemon-logging.html)

Enable debug logging:

    WIREPLUMBER_DEBUG=I wireplumber
    WIREPLUMBER_DEBUG=I wireplumber 2>&1 |grep bluetooth.lua.d
    WIREPLUMBER_DEBUG=I wireplumber 2>&1 |tee /tmp/wireplumber.log
