# ID3 tags

- [WIkipedia: ID3-Tag](https://de.wikipedia.org/wiki/ID3-Tag)
- [Wikipedia: ID3v1 Genres](https://de.wikipedia.org/wiki/Liste_der_ID3v1-Genres)
- [Ubuntuusers wiki: ID3-Tags](https://wiki.ubuntuusers.de/%C3%9Cberpr%C3%BCfung_MP3-Sammlung/#ID3-Tags)

## cli tools

### eyed3

- [docs](https://eyed3.readthedocs.io/en/latest/)

Show id3 tags with eyeD3:

```sh
eyeD3 002.mp3
```

Set track nr:

```sh
eyeD3 -n 2 002.mp3
```

### id3v2

Beware: Doesn't detect ID3 v2.4 tags in this track

Show id3 tags with id3v2

```sh
sudo apt-get install id3v2
```

### mp3info

Beware: `mp3info` only supports old id3 v1 !!

Show id3 tags with mp3info:

```sh
sudo apt install mp3info
```
