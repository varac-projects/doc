# Music

## Streaming platforms

- [Wikipedia: Comparison of music streaming services](https://en.wikipedia.org/wiki/Comparison_of_music_streaming_services)
- [Spotify](./spotify.md) incl. Spotify downloader
  - supported by [freyr-js](https://github.com/miraclx/freyr-js?tab=readme-ov-file#service-support)
- [Tidal](https://tidal.com/)
  - NOT supported by [freyr-js](https://github.com/miraclx/freyr-js/issues/33)
  - [Tidal-Media-Downloader](https://github.com/yaronzz/Tidal-Media-Downloader)
- [Nonoki](https://nonoki.com)
  - Free of charge
  - Couldn't find artist which is available on all above platforms

### Deezer

- [Website](https://www.deezer.com/us/)
- supported by [freyr-js](https://github.com/miraclx/freyr-js?tab=readme-ov-file#service-support)
- default downloader in [arr-scripts/lidarr](https://github.com/RandomNinjaAtk/arr-scripts/tree/main/lidarr)

#### deemix

- [deemix-js](https://gitlab.com/RemixDev/deemix-js)
- Unmaintained
- [deemix-docker](https://gitlab.com/Bockiii/deemix-docker)
- [deemix-py](https://gitlab.com/RemixDev/deemix-py)
- [truecharts/deemix](https://artifacthub.io/packages/helm/truecharts/deemix)
- [deemixrr](https://github.com/TheUltimateC0der/deemixrr)
- [Lidarr.Plugin.Deemix](https://github.com/ta264/Lidarr.Plugin.Deemix)
- [lidarr-on-steroids](https://github.com/youegraillot/lidarr-on-steroids)

## Stream rippers

- [streamrip](https://github.com/nathom/streamrip)
  - Qobuz, Tidal, Deezer and SoundCloud
  - Python
- [freyr-js](https://github.com/miraclx/freyr-js)
  - Spotify, Apple Music and Deezer
- [spotify-downloader](https://github.com/spotDL/spotify-downloader)
- [DownOnSpot](https://github.com/oSumAtrIX/DownOnSpot)
