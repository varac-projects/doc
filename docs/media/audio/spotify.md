# Spotify

[Arch Wiki](https://wiki.archlinux.org/index.php/spotify)

## Daemons

### spotifyd

- [GitHub](https://github.com/Spotifyd/spotifyd)
- [Blog](https://blog.katriel.co.uk/spotifyd)

Spotifyd will only work with Spotify Premium

Config: `~/.config/spotifyd/spotifyd.conf`

Build:

```sh
cd ~/projects/audio/spotify/spotifyd
cargo build --release --features "pulseaudio_backend"
```

## Clients

[Arch Wiki: third party clients](https://wiki.archlinux.org/index.php/spotify#Third-party_clients)

### spotify-tui

- Client for i.e. spotifyd
- Outdated
- [GitHub](https://github.com/Rigellute/spotify-tui)
- [Limitations](https://github.com/Rigellute/spotify-tui#limitations):

> If you want to play tracks, Spotify requires that you have a Premium account.

- Config: `~/.config/spotify-tui/client.yml`

### spotify-qt

- [snap doesn't support wayland](https://github.com/kraxarn/spotify-qt/issues/35)

## Etc

- [spotify-backup](https://gitlab.com/nathan.monfils/spotify-backup)
  saves a raw JSON list of all your saved tracks
- [rofi-spotify](https://github.com/AnySomebody1/rofi-spotify)
  python program to interact with Spotify via rofi

## Downloaders

### DownOnSpot

- Update: Shut down due to a cease and desist letter from Spotify.
- [GitHub](https://github.com/oSumAtrIX/DownOnSpot)
- Used [librespot](https://github.com/librespot-org/librespot)
- Config at `~/.config/down_on_spot/settings.json`

> Note: librespot only works with Spotify Premium. This will remain the case.
> We will not support any features to make librespot compatible with free accounts,
> such as limited skips and adverts.

Setup:

- Create a new application on the [Spotify developer dashboard](https://developer.spotify.com/dashboard)

Clone:

```sh
git clone https://github.com/oSumAtrIX/DownOnSpot ~/projects/audio/spotify/DownOnSpot
sudo apt install libmp3lame-dev
cd ~/projects/audio/spotify/DownOnSpot
```

Edit `Cargo.toml` according to [DownOnSpot#building](https://github.com/oSumAtrIX/DownOnSpot?tab=readme-ov-file#%EF%B8%8F-building),
then:

```sh
cargo build --release
ln -s ~/projects/audio/spotify/DownOnSpot/target/release/down_on_spot ~/bin/build/down_on_spot
```

### Config

- Config: `~/.config/down_on_spot/settings.json`

- Download dir: `~/down_on_spot`

- [Filename template variables](https://github.com/oSumAtrIX/DownOnSpot#template-variables)

Variables example:

- `%0disc%`: `01`
- `%0track%`: `04`
- `%album%`: `Soundtrack of Movie XYZ`
- `%albumArtist%`: `Various Artists`
- `%albumArtists%`: `Various Artists`
- `%artist%`: `Some artist`
- `%disc%`: `1`
- `%id%`: ?
- `%title%`: `Title`
- `%track%`: `4`

Examples of files created by following template:

```txt
%albumArtist% - %albumArtist% - %album% - %artist% - %disc% - %id% - %0track% - %title%
```

```txt
./Various Artists - Various Artists - Dein Song 2024 - Shayla Jolie -
  1 - 0YmfW205qCCXmMf0DP8e4i - 03 - Let You Go - Final Version.mp3
```

```txt
./LINA - LINA - Official - LINA - 1 - 5SdlE4nVM3zXmV3B5mMbxc - 08 - Idiot.mp3
```

### Usage

```sh
down_on_spot 'spotify:album:<SPOTIFY_ID>'
down_on_spot 'SPOTIFY_URL'
```

### Issues

- Doesn't well support support playlists
- Doesn't support release year template variable

### spotify-dl

[GitHub](https://github.com/SathyaBhat/spotify-dl)

> spotify-dl doesn't download anything from Spotify. It picks up the metadata
> from Spotify API and then uses yt-dlp to download the song.

From [this comment](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/1815#issuecomment-1146053318):

> I tried spotify_dl without success. Downloading is working well,
> but the content is not what I initially selected. I loaded some audiobooks
> and the title/story was the same, but different version / reader.
> I then tried to load a single track, and it doesn't load the exact content
> but is just looking for the same title...

### Stale: spotify-downloader

[GitHub](https://github.com/ritiek/spotify-downloader)

cmd: `spotdl`
