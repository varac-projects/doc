# Soundcloud

## Soundcloud Music Downloader

- [GitHub](https://github.com/flyingrub/scdl)

> The pip repository version of scdl is outdated you will have to install it directly from the github repo :

Install:

```sh
pipx install --spec git+https://github.com/flyingrub/scdl scdl
```
