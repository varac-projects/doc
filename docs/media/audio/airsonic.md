# Subsonic/Libresonic/Airsonic media server

## Complete rescan od media folder

see <https://github.com/airsonic/airsonic/issues/650#issuecomment-372479993>:
"Remove Media Folder/Scan/Clean Database/Add Media Folder/Rescan"

## Issues

- Can't import files with special characters (see <https://github.com/airsonic/airsonic/issues/716>).
  - See <https://github.com/airsonic/airsonic/issues/716#issuecomment-398209592>

Solution:

```sh
echo 'export LANG=en_US.UTF-8' >> /var/lib/tomcat8/bin/setenv.sh
```

## Run docker image locally

<https://github.com/anarcat/subsonic-docker-image/tree/airsonic>

```sh
cd ~/projects/audio/mediaserver/airsonic/subsonic-docker-image
docker build -t varac/debian-airsonic .

docker run --publish 127.0.0.1:8081:8080 --volume  "/home/varac/Music:/var/music:ro" varac/debian-airsonic -Dserver.address=127.0.0.1
```

Then browse <http://localhost:8081>
