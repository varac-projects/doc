# Pipewire

- [Website](https://pipewire.org/)
- [Docs](https://docs.pipewire.org/)
- [Wiki](https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/home)
- [Gitlab](https://gitlab.freedesktop.org/pipewire/pipewire)

## App support

### chromium

`chrome://flags/#enable-webrtc-pipewire-capturer` must be enabled

## Install

### Packages

```sh
sudo apt purge pulseaudio
sudo apt install pipewire
systemctl --user status pipewire
systemctl --user status pipewire.socket
```

## Usage

List output sinks:

```sh
pactl list sinks
```

## Debug

[Debug](https://github.com/lizthegrey/pipewire/blob/debian/0.3.5/INSTALL.md#running):

```sh
systemctl --user stop pipewire
env PIPEWIRE_DEBUG=5 pipewire
```

## Issues

- [NoiseTorch](https://github.com/lawl/NoiseTorch/issues/63) doesn't support
  pipewire
- [PulseEffects](https://github.com/wwmm/pulseeffects/issues/397) fails on
  pipewire

## pulseaudio replacement

- [Arch wiki: Replacing PulseAudio](https://wiki.archlinux.org/index.php/PipeWire#Replacing_PulseAudio)
- [PulseAudio replacement](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/INSTALL.md#pulseaudio-replacement)
- [PulseAudio Emulation](https://github.com/lizthegrey/pipewire/blob/debian/0.3.5/INSTALL.md#pulseaudio-emulation)

Needed to replace the 3 libraries in `/usr/lib/x86_64-linux-gnu/`:


```sh
ls -la /usr/lib/x86_64-linux-gnu/libpulse*
```

```sh
pactl info
ldd /usr/bin/pavucontrol | grep pulse
```

Play (pw-play only plays ogg, no mp3):

```sh
pw-play ~/Music/Michael\ Jackson/Number\ Ones/05\ Thriller.ogg
pw-pulse gst123 /usr/share/sounds/freedesktop/stereo/bell.oga
```

Working:

- [x] Native pipewire `pw-play /usr/share/tuxtype/themes/dansk/sounds/excuseme.wav`
- [x] gstreamer (`gst123 /usr/share/tuxtype/themes/dansk/sounds/excuseme.wav`)
- [ ] Alsa `aplay /usr/share/tuxtype/themes/dansk/sounds/excuseme.wav`
- [x] Pulseaudio `paplay /usr/share/tuxtype/themes/dansk/sounds/excuseme.wav`
- [ ] Fake-pulseaudio `pw-pulse paplay /usr/share/tuxtype/themes/dansk/sounds/excuseme.wav`
      segfault
