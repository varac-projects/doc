# Pulseaudio

## Usage

Play (paplay can't play `mp3`):

    paplay ~/Audio/examples/Thriller.ogg
    paplay /usr/share/sounds/alsa/Front_Center.wav

List all configured pulseaudio components (including soundcards):

    pactl list

Configure volume:

    pactl set-sink-volume 0 +5%
    pactl set-sink-volume 0 -5%
    pactl set-sink-mute   0 toggle
    pactl set-source-mute 1 toggle

## Issues

### Device/sink suspended

https://unix.stackexchange.com/a/171925

Disable `module-suspend-on-idle` in either `.config/pulse/default.pa` or
`/etc/pulse/default.pa`.

## Troubleshooting

https://wiki.archlinux.org/index.php/PulseAudio/Troubleshooting

Debug output:

    systemctl --user stop pulseaudio.socket pulseaudio.service
    pulseaudio -v

## Etc

* [How to auto-switch to new connected (i.e. bluetooth) sink](https://askubuntu.com/a/396166)
