# mpd

[Configuring MPD to run as a user service](https://help.ubuntu.com/community/MPD#Configuring_MPD_to_run_as_a_user_service)

* Config file:`/etc/mpd.conf`

Debugging:

Edit `/etc/mpd.conf` and change `log_level` to `verbose`, afterwards:

    sudo systemctl restart mpd.service

    tail -f /var/log/mpd/mpd.log

## mpc client

    mpc clear

Rescan whole library:

    mpc rescan

Add playlist to queue and start playing:

    mpc load playlist1
    mpc play

    mpc lsplaylists

Show playlist content:

    mpc playlist playlist1

    mpc ls
    mpc listall

## ncmpc

[ubuntuusers wiki](https://wiki.ubuntuusers.de/ncmpc/)

Ncurses based client

    sudo apt-get install ncmpc
