# Livecoding

## Tidal cycles

- [Tidal cycles](https://tidalcycles.org/)
- [tidal.nvim](https://github.com/robbielyman/tidal.nvim)
  neovim plugin for tidalcycles

## Web REPL

- [strudel REPL](https://strudel.cc/)
  - live coding platform to write dynamic music pieces in the browser
