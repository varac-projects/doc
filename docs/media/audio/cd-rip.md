# Rip CDs

[MusicBrainz Enabled Applications](https://musicbrainz.org/doc/MusicBrainz_Enabled_Applications)

## whipper

- [GitHub](https://github.com/whipper-team/whipper)
- Pros: accurate
- Cons: takes a long time, rips to flac

Usage:

```sh
whipper cd rip --unknown --cdr
```

Rips to `~/Audio/incoming/whipper`

## Sound juicer

- [Website](https://wiki.gnome.org/Apps/SoundJuicer)
- [GitHub](https://github.com/GNOME/sound-juicer)
- GUI app

## Discontinued apps

- [abcde](https://abcde.einval.com/wiki/)
  - Last commit 2021
  - Perl, not on GitHub
- `ripit`: can't find source code, no website, seems unmaintained
