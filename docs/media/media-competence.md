# Children / media competence

- [SCHAU HIN!](https://www.schau-hin.info/)
  Die Initiative „SCHAU HIN! Was Dein Kind mit Medien macht.“ hilft Familien bei der Medienerziehung.
- [smarter Start ab 14](https://www.smarterstartab14.de/)
  Elterninitiative für eine Smartphonefreie Kindheit

## Search engines

- [Die besten Kindersuchmaschinen](https://www.kaenguru-online.de/themen/medien/die-besten-kindersuchmaschinen)

## Screentime limit apps

- See [timelimit.md](../operating-systems/android/timelimit.md)
