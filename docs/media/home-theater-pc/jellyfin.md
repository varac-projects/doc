# Jellyfin

FOSS Emby fork.

## Hardware options

- [Selecting Appropriate Hardware](https://jellyfin.org/docs/general/administration/hardware-selection/)
  SBCs:

> Most Single Board Computers (SBC): Most SBCs (Including Raspberry Pis
> and especially the Pi 5) are too slow to provide a good Jellyfin experience
> since they often lack proper support for hardware acceleration.
> If You really want to run Jellyfin on an SBC, please look at models
> based on the following platforms:

- [Rockchip RK3588](https://www.cpubenchmark.net/cpu.php?cpu=Rockchip+RK3588&id=4906)
  - Multithread: `4398`, Single thread: `1477`
- Rockchip RK3588S
- Intel Core
- [Intel 12th gen N series](https://en.wikipedia.org/wiki/Alder_Lake#Alder_Lake-N)
  - i.e. casita server with [Intel N100](https://www.cpubenchmark.net/cpu.php?cpu=Intel+N100&id=5157)
    - Multithread Rating: `5505`, Single Thread Rating: `1943`
- compared to RPi 4b ([ARM Cortex-A72 4 Core 2016 MHz](https://www.cpubenchmark.net/cpu.php?cpu=ARM+Cortex-A72+4+Core+2016+MHz&id=6214))
  - Multithread Rating: `2192`, Single Thread Rating: `607`

## Parental control

- [Good overview of parental control feature requests](https://features.jellyfin.org/posts/999/tag-in-this-feature-tracker-for-features-relating-to-children-kids-parents-parental-controls)
- See [Television content rating system chaos](https://en.wikipedia.org/wiki/Television_content_rating_system)
- [FR: Parental Controls "Allow items with tags"](https://features.jellyfin.org/posts/235/parental-controls-allow-items-with-tags)
- [FR: Folder based access control](https://features.jellyfin.org/posts/90/folder-based-access-control)
- [FR: Who's watching ?](https://features.jellyfin.org/posts/493/users-within-users)
