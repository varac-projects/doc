# Home theater PC options

## HTPC operating systems

- [SteamOS](https://store.steampowered.com/steamos)
  - Still based on Debian 8 jessie (as of 2024-11)
- [Stremio](https://www.stremio.com/)
  - Successor of [PopcornTime](https://de.wikipedia.org/wiki/Popcorn_Time)
- [LineageOS 20 Android TV (Android 13) for Raspberry Pi 4](https://konstakang.com/devices/rpi4/LineageOS20-ATV/)

### Kodi based

- [LibreElec](https://libreelec.tv/)

Deprecated:

- [OpenElec](https://github.com/OpenELEC)

## Desktop env

### KDE plasma bigscreen

- [Website](https://plasma-bigscreen.org/de/)
  - Based on Wayland
  - Mycroft integrated
  - [Plasma Bigscreen Gitlab](https://invent.kde.org/plasma-bigscreen)
    - Development stalled
  - [Apps](https://store.kde.org/browse?cat=608&ord=latest/)
  - Couldn't get sound over HDMI working :/

#### Install

[Distributions offering Plasma Bigscreen](https://plasma-bigscreen.org/get/):

- [Debian package](https://packages.debian.org/stable/kde/plasma-bigscreen)
- [ManjaroARM](https://github.com/manjaro-arm/bigscreen/releases)
  - [Plasma Bigscreen first boot wizard bails on RPi 4](https://forum.manjaro.org/t/plasma-bigscreen-first-boot-wizard-bails-on-rpi-4/131448)
- [PostmarketOS](https://postmarketos.org/download/): Mostly for smartphones

### Deprecated

- [LinHES](http://www.linhes.org/)
  - Linux Home Entertainment System
  - Baed on MythTV
- [Geexbox](https://de.wikipedia.org/wiki/Geexbox)
