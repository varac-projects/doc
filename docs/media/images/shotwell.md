# Shotwell

## Issues

Moving tags out of parent tag group doesn't remove parent tag
Lets say we have a tag structure like this:

    A/B

When I move the B tag out of A into the tag root like this:

    A
    B

pictures with the B tag still remain to have the A tag set.
