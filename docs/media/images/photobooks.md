# Photo books

- [PDF Fotobuch und Erstellung mit Linux oder Mac – wer kann was! 🇩🇪](https://www.fotobuchmagazin.de/ratgeber-tipps/fotobuch-software-pdf-services-und-online-gestalter/)
  - Good Overview of different apps and online services, and which
    manufacturer handles PDF upload
- [Stiftung Warentest 2020-08 🇩🇪](https://www.test.de/Fotobuecher-im-Test-Wer-macht-die-schoensten-Bildbaende-4932482-5634116/)

## PDF creation

### Scribus

- [Ein Fotobuch mit Scribus frei erstellen 🇩🇪](https://www.fotobuchmagazin.de/fotobuchgestaltung/kapitel-5/ein-fotobuch-mit-scribus-frei-erstellen/)
- [PhotoBookTools-for-Scribus](https://github.com/RaffertyR/PhotoBookTools-for-Scribus)
  - "collection of scripts and tricks intended to create
    photo album pages in a fast and flexible way with Scribus"
  - [Instructions.pdf](https://raw.githubusercontent.com/RaffertyR/PhotoBookTools-for-Scribus/main/PhotoBookTools/Instructions.pdf)
  - Needs python packages `tk` and `pillow` installed (`sudo pacman -S tk python-pillow`)

## Manufactures with PDF support

### Saal digital

- [Lade dein PDF im Webshop hoch 🇩🇪](https://www.saal-digital.de/service/profibereich/lade-dein-pdf-im-webshop-hoch/)
- [Profibereich — Fotobücher 🇩🇪](https://www.saal-digital.de/fotobuch/profibereich/)
  - Layout and Dimensions, incl. Adobe InDesign template which can get
    imoprted into Scribus
- Minimum 26 pages
- Sale until mid-November

## Manufacturer software

## Pixum

- Best image print quality according to tests
- [AUR package: pixum-fotowelt](https://aur.archlinux.org/packages/pixum-fotowelt)
  - Installs and runs fine
  - Approx. 1.1 GB total space used
  - Conflicts with `cewe-fotowelt`

## CEWE, DM

- Cewe and DM apps are based on the same software
- Clunky perl installer, terminal-based installation
- Good ratings in print quality
- Shipping to DM shop possible
- [AUR package: cewe-fotowelt](https://aur.archlinux.org/packages/cewe-fotowelt)
  - Installs and runs fine
  - Approx. 1 GB total space used
- [AUR package: cewe-fotobuch](https://aur.archlinux.org/packages/cewe-fotobuch)
- [AUR package: dm-fotowelt](https://aur.archlinux.org/packages/dm-fotowelt)
  - Fails to install

## fotobuch.de designer-3

- good ratings for software UX
- One-click grafical .run installer
- medium rating for print quality
