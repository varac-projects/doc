# EXIF image/document metadata

- [Wikipedia: Exif](https://en.wikipedia.org/wiki/Exif)

## Tools

- Original [exiftool](https://exiftool.org/)
  - [GitHub](https://github.com/exiftool/exiftool)
  - Perl
  - Install:

```sh
sudo pacman -S perl-io-compress-brotli perl-image-exiftool
ln -s /usr/bin/vendor_perl/exiftool ~/bin/download/exiftool
```

- [exiftool-rs](https://github.com/jae1911/exiftool-rs)
  - Archived

## Usage

### Show exif metadata

```sh
exiftool DSC_0008.JPG
```

Output json:

```sh
exiftool -json DSC_0008.JPG
```

Show only timestamps:

```sh
exiftool -a -s -G1 -time:all resume/resume.pdf
```

### Modify PDF tags

from the [docs: PDF Tags](https://exiftool.org/TagNames/PDF.html):

> All metadata edits are reversible. While this would normally be considered an advantage,
> it is a potential security problem because old information is never actually deleted from the file.
> (However, after running ExifTool the old information may be removed permanently using the "qpdf" utility
> with this command: `qpdf --linearize in.pdf out.pdf`.)

### Copy all photos from Sony mobile

```sh
for i in $(exiftool -R -if '$Make eq "Sony"' \
  -p '$directory/$filename' ../07 2>/dev/null)
do cp $i /tmp/images; done
```

## Adjust capture date to match file modification date

Before:

```console
❯ exiftool -j /tmp/DSC_0008.JPG | grep -i date
  "FileModifyDate": "2019:12:11 20:38:39+01:00",
  "FileAccessDate": "2019:12:11 20:39:13+01:00",
  "FileInodeChangeDate": "2019:12:11 20:38:39+01:00",
  "ModifyDate": "2016:03:06 18:43:56",
  "DateTimeOriginal": "2016:03:06 18:43:56",
  "CreateDate": "2016:03:06 18:43:56",
  "DateDisplayFormat": "D/M/Y",
  "DateStampMode": "Off",
  "DateTimeDigitized": "2016:03:06 17:43:56Z",
  "SubSecCreateDate": "2016:03:06 18:43:56.00",
  "SubSecDateTimeOriginal": "2016:03:06 18:43:56.00",
  "SubSecModifyDate": "2016:03:06 18:43:56.00",
```

Modify:

```sh
exiftool -overwrite_original '-alldates<FileModifyDate' \
  '-DateTimeDigitized<FileModifyDate' '-FileModifyDate<FileModifyDate' *.JPG
```

After:

```console
❯ exiftool -j /tmp/DSC_0008.JPG | grep 2019
  "FileModifyDate": "2019:12:11 20:55:20+01:00",
  "FileAccessDate": "2019:12:11 20:55:22+01:00",
  "FileInodeChangeDate": "2019:12:11 20:55:20+01:00",
  "ModifyDate": "2019:12:11 20:54:46",
  "DateTimeOriginal": "2019:12:11 20:54:46",
  "CreateDate": "2019:12:11 20:54:46",
  "DateTimeDigitized": "2019:12:11 20:54:46+01:00",
  "SubSecCreateDate": "2019:12:11 20:54:46.00",
  "SubSecDateTimeOriginal": "2019:12:11 20:54:46.00",
  "SubSecModifyDate": "2019:12:11 20:54:46.00",
```
