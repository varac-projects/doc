# Ffmpeg

## Show video details

[Extracting video information of a video file using ffmpeg](https://www.oodlestechnologies.com/blogs/extracting-video-information-of-a-video-file-using-ffmpeg/)

```sh
ffmpeg -i video.mp4
```

## Downsample Video

- [How to Decrease Video Sizes Using FFmpeg](https://www.linuxjournal.com/content/how-decrease-video-sizes-using-ffmpeg-linux)
- [How to Downsample Video to a Lower Resolution](https://www.baeldung.com/linux/video-downsample-lower-resolution)

### by scale

```sh
ffmpeg -i in.mp4 -s 640x480 out.mp4
```

### by frame per second

```sh
ffmpeg -i big_buck_bunny.y4m fps-fps=30 big_buck_bunny.mp4
```

### to target size

```sh
~/bin/custom/ffmpeg_resize_to_target_size.sh
```

## Add Audio to video

[How to add a new audio (not mixing) into a video using ffmpeg?](http://stackoverflow.com/questions/11779490/ffmpeg-how-to-add-new-audio-not-mixing-in-video)

```sh
ffmpeg -i video.avi -i audio.mp3 -map 0 -map 1 -vcodec copy -shortest output_video.avi
```

## Remove Audio

```sh
ffmpeg -i infile.avi -an -c:v copy outfile.avi
```

(`-c:v copy` says to copy instead of using a codec.)

## Extract Audio from video

```sh
ffmpeg -i input-file-name-with-path output-filename.mp3
```

## Join mp4 video files

[Concat multiple (self-generated) videos using ffmpeg](http://stackoverflow.com/a/35619414)

```sh
ffmpeg -f concat -i "./list" -c copy out.mp4
```

## Cut mp4 into pieces

```sh
ffmpeg -i YourFile.mp4 -ss 00:10:25 -t 00:00:05 -acodec copy -vcodec copy Output.mp4
```

## Rotate Video

[Rotate 90 clockwise](https://stackoverflow.com/a/9570992/1565597):

```sh
ffmpeg -i in.mov -vf "transpose=1" out.mov
```

For the transpose parameter you can pass:

> 0 = 90CounterCLockwise and Vertical Flip (default)
> 1 = 90Clockwise
> 2 = 90CounterClockwise
> 3 = 90Clockwise and Vertical Flip

And to flip it horizontally [ffmpeg documentation](http://ffmpeg.org/ffmpeg-doc.html#TOC53*):

For example to horizontally flip the video in input with `ffmpeg`:

```sh
ffmpeg -i in.avi -vf "hflip" out.avi
```
