# yt-dlp

- [GitHub](https://github.com/yt-dlp/yt-dlp)
- [Supported sites](https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md)

## Web frontends

- [MeTube](https://github.com/alexta69/metube)
  - Config via env vars
  - [Browser extensions](https://github.com/alexta69/metube?tab=readme-ov-file#browser-extensions)
  - [Bookmarklets](https://github.com/alexta69/metube?tab=readme-ov-file#bookmarklet)
  - [Container images](https://github.com/alexta69/metube/pkgs/container/metube/versions?filters%5Bversion_type%5D=tagged)
- [yt-dlp-web-ui](https://github.com/marcopiovanello/yt-dlp-web-ui)
  - [Container images](https://github.com/marcopiovanello/yt-dlp-web-ui/pkgs/container/yt-dlp-web-ui)
    [not properly versioned](https://github.com/marcopiovanello/yt-dlp-web-ui/issues/200)
