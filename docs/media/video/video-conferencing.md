# Video conferencing

## Free Software

- [Galene](https://galene.org/)
- [Janus: the general purpose WebRTC server](https://janus.conf.meetecho.com/)
- Discontinued: [ion-sfu](https://github.com/pion/ion-sfu)

### Jitsi

- [jitsi-meet-electron](https://github.com/jitsi/jitsi-meet-electron/)
  - Jitsi Meet desktop application
- Config at `~/\.config/Jitsi\ Meet/`

#### Jitsi frontends

- [chatmosphere](https://chatmosphere.cc): informal video calls more fun and dynamic

### zoom

See also `../Audio/pipewire.md`

Force zoom to run as xwayland app:

```sh
env XDG_SESSION_TYPE=xcb zoom
```

#### Offical Deb

Download from `https://zoom.us/download?os=linux`

Install:

```sh
sudo dpkg -i zoom_amd64_5.4.6_56259.1207.deb
sudo apt --fix-broken install
```

- Logs at `~/.zoom/logs/zoom_stdout_stderr.log`
- No screensharing with wayland (only whitelisted desktop envs are supported)

#### Flatpak

- [Flatpak](https://flathub.org/apps/details/us.zoom.Zoom)
- No sign-in possible ?

#### Snap

- [snap](https://snapcraft.io/zoom-client) - no mic with pipewire
