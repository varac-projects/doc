# Webcam tools

- [Arch Wiki: Webcam setup](https://wiki.archlinux.org/title/webcam_setup)
- [TOP WEBCAM APPS FOR LINUX TO ENHANCE YOUR VIDEO EXPERIENCE](https://www.chicagovps.net/blog/top-webcam-apps-for-linux-to-enhance-your-video-experience)

## GUI

- [cameractrls](https://github.com/soyersoyer/cameractrls)

### Webcamoid

Webcamoid is a full featured and multiplatform webcam suite.

- [Website](https://webcamoid.github.io/)

### OBS Studio

- [Website](https://obsproject.com)
- [Video: OBS Studio as a Webcam](https://www.youtube.com/watch?v=U_bHqgOdZS8)

## cli

### v4l2-ctl

List all devices:

```sh
v4l2-ctl --list-devices
```

Show all values:

```sh
v4l2-ctl -l
```

Set brightness to absulte value:

```sh
v4l2-ctl -d /dev/video0 --set-ctrl=brightness=200
```
