# Video cards

## Video acceleration

    vainfo

### Video acceleration issues

    $ jitsi-meet
    libva error: /usr/lib/x86_64-linux-gnu/dri/i965_drv_video.so init failed

Fixed by overriding VA driver name (although performance doesn't improve):

    LIBVA_DRIVER_NAME=iHD jitsi-meet


## Open Hardware video cards

* [Asus GT710-4H-SL-2GD5](https://www.phoronix.com/scan.php?page=article&item=asus-50-gpu&num=1)
