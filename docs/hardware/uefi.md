# UEFI

- [Arch wiki: UEFI](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface)

## UEFI or bios mode ?

Did my system boot in EFI mode ?

```sh
ls /sys/firmware/efi/
```

Does my system booted with EFI secure boot ?

```sh
mokutil --sb-state
```

## Migrate existing system from Bios to UEFI

- [Switch Debian from legacy to UEFI boot mode](https://blog.getreu.net/projects/legacy-to-uefi-boot/)

## UEFI secure boot

- [Arch wiki: UEFI secure boot](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot)
- [Debian wiki: Secure boot](https://wiki.debian.org/SecureBoot)
