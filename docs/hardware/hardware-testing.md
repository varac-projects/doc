# Hardware testing / health

## Stress testing

### stress-ng

> Stress-ng will stress test a computer system in various selectable ways.
> It was designed to exercise various physical subsystems of a computer
> as well as the various operating system kernel interfaces.

- [Stress-ng](https://github.com/ColinIanKing/stress-ng)
- [Ubuntu reference page](https://wiki.ubuntu.com/Kernel/Reference/stress-ng)

Install:

```sh
apt install stress-ng
```

#### Usage

##### CPU

Simple matrix test:

```sh
stress-ng --matrix 0 -t 1m
```

Show thermal information if available:

```sh
stress-ng --cpu 0 --tz -t 1m
```

### anarcat/stressant

> Stressant is a simple yet complete stress-testing tool that forces a computer
> to perform a series of test using well-known Linux software in order to
> detect possible design or construction failures.
> Stressant builds on top of existing software and tries to cover
> most components of your system (currently disk, CPU and processor).

- [Gitlab](https://gitlab.com/anarcat/stressant)
- Included in GRML live cd

## memtest

- Boot `memtest` from [live cd](./disks/live-cds.md)
- or install it as debian package (`apt install memtest86+`), reboot and select
  it from the grub menu
- Run at least 1 whole **pass**
- [Website](https://memtest.org/)
- [GitHub](https://github.com/memtest86plus/memtest86plus/)
