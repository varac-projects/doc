# Fanless server options

- [Heise.de fanless barebores](<https://www.heise.de/preisvergleich/?cat=barepc&xf=2591_passiv+(l%FCfterlos)>)

## Celeron based

- [Raspberry Pi 4 B (Broadcom BCM2711) vs Intel Celeron J4105](https://www.cpu-monkey.com/de/compare_cpu-raspberry_pi_4_b_broadcom_bcm2711-vs-intel_celeron_j4105)

- [Futro S740](https://github.com/R3NE07/Futro-S740)
- [Dell Wyse 5070 Celeron](https://amso.eu/de/products/terminal-dell-wyse-5070-celeron-j4105-quad-1-5ghz-8gb-64gb-ssd-stromversorgung-171138)
