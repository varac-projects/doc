# Server Setup

## UEFI / Bios

- Secure boot can stay enabled
- Power-on after power failure
- Enable Bios password
  - Password store: `hardware/server/HOSTNAME/uefi`
  - Beware: Don't generate a too long password, in case it needs to be
    entered manually !
- Disable USB/Network boot

## Hardware

- Remove wifi card ?

## Hardware testing with live CD

- Boot `Grml` from [live cd](../disks/live-cds.md)

### Disk

- See also disk testing procedure ([Riseup disk test procedure](https://we.riseup.net/riseup+tech/disk-testing-procedure))
- Sanitize NVME
- Run nvme/ssd self tests

### CPU

- Use `stress-ng`, see [../hardware-testing.md](../hardware-testing.md)

## memtest

- Boot `memtest` from [live cd](../disks/live-cds.md)

## OS Installation

see [](../../operating-systems/debian/etc.md)

- Hostname: [List of Don Quixote characters](https://en.wikipedia.org/wiki/List_of_Don_Quixote_characters)

## Post installation steps

- Install `etckeeper` (`apt install etckeeper`) as first task right after
  installation
- Replace potential unsecure passwords from installation
  - root (Password store: `hardware/server/HOSTNAME/root`)
  - unprivileged user (Password store: `hardware/server/HOSTNAME/varac`)
  - LUKS encryption passphrase (Password store: `hardware/server/HOSTNAME/luks`)
    - `cryptsetup luksChangeKey /dev/nvme0n1p3`
- Configure sudo:
  - `apt install sudo`
  - `usermod -a -G sudo varac`

### Dropbear for remote LUKS unlocking

See [cryptsetup.md](../disks/crytpsetup.md)

### Grub password

- See `../disks/grub.md`
- Password store: `hardware/server/HOSTNAME/grub`
  - Beware: Don't generate a too long password, in case it needs to be
    entered manually !
