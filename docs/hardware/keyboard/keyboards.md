# Keyboards

## Logitech MX Mini

- [Review and configure the Logitech MX Keys for linux (Pop!\_OS)](https://github.com/spxak1/weywot/blob/main/guides/mxkeys_linux.md)
- [Factory reset MX mini](https://ernestojeh.com/factory-reset-mx-keys)
- Dimensions: 29,6 x 21 x 13,2 cm (W/D/H)
  - In comparism: [Apple Magic keyboard](https://www.apple.com/de/shop/product/MK2A3D/A/magic-keyboard-deutsch)
    - 27,9 x 11,5 x 1,1 cm (W/D/H)

## Logitech MX Mechanical Mini

- [Website](https://www.logitech.com/en-eu/products/keyboards/mx-mechanical.html)
- Dimensions: 43,3 x 13,1 x 2,6 cm (W/D/H)

### Logitech driver

- [logiops](https://github.com/PixlOne/logiops)

#### Issues

F1-F3 are used for switching Bolt devices and F4+F5 should dim the display
(doesn't work ?). Anyhow, to use F1-F5 as regular function keys, use `Fn+Esc`
to toggle between regular and Logitech mode.
Might be customizable with `solaar`

#### Solaar

[GitHub](https://github.com/pwr-Solaar/Solaar)
