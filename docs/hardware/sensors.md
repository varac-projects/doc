# Hardware sensors

## Battery

- [Arch wiki: Laptop/Battery state](https://wiki.archlinux.org/title/Laptop#Battery_state)

Show battery state percentage:

```sh
cat /sys/class/power_supply/BAT0/capacity
```

Alternativly, use [upower](https://archlinux.org/packages/?name=upower):

```sh
for BAT_PATH in $(upower -e | grep BAT); do upower -i "$BAT_PATH"; done
```

## lm-sensors

Setup:

```sh
sudo apt-get install lm-sensors
sudo sensors-detect --auto
sensors
```

## Fan speed control

- [Arch wiki: Fan speed control](https://wiki.archlinux.org/title/fan_speed_control)

```sh
apt install fancontrol
systemctl status fancontrol.service
```
