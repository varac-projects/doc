# Pinetime smartwatch

- [Wiki](https://wiki.pine64.org/wiki/PineTime)
- [Wiki: FAQ](https://wiki.pine64.org/wiki/PineTime_FAQ)
- No GPS!

## Firmware

### Infinitime

- [Github](https://github.com/InfiniTimeOrg/InfiniTime)
- [Updating](https://github.com/InfiniTimeOrg/InfiniTime/blob/develop/doc/gettingStarted/updating-software.md)

Issues:

- [Activity/Workout Tracking (different modes, on device](https://github.com/InfiniTimeOrg/InfiniTime/issues/1255)
- [Fitness data protocol](https://github.com/InfiniTimeOrg/InfiniTime/issues/749)
- [Steps history](https://github.com/InfiniTimeOrg/InfiniTime/issues/788)

## Pinetime apps

### Watchmate (Linux)

- PineTime smart watch companion app for Linux phone and desktop
- GUI, rust
- [Github](https://github.com/azymohliad/watchmate)
- Supports updating firmware and resources

### itd

- itd is a daemon that uses my infinitime library to interact with the PineTime
- [Gittea](https://gitea.arsenm.dev/Arsen6331/itd)
- Install daemion and cli tool with `paru -S itd-bin`
- Supports updating firmware and resources

#### itd-gui

- To install the [GUI](https://gitea.arsenm.dev/Arsen6331/itd#itgui),
  [LURE](https://gitea.arsenm.dev/Arsen6331/lure) is needed :/
  - [FR: Publish itd-gui AUR package](https://gitea.arsenm.dev/Arsen6331/itd/issues/52)

### Siglo (Linux)

- Stale, last commit 2022-07
- [Github](https://github.com/alexr4535/siglo)
- Linux GTK app
- Can update firmware

### Other pinetime firmwares

- <https://github.com/daniel-thompson/wasp-os#readme>
