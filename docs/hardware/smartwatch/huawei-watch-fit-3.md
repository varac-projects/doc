# Huawei Watch Fit 3

- [Kuketz-Blog: Gadgetbridge: Huawei Watch Fit 3 datenschutzfreundlich nutzen – Teil 3](https://www.kuketz-blog.de/gadgetbridge-huawei-watch-fit-3-datenschutzfreundlich-nutzen-teil-3/)
- [󰕧 The Quantified Scientist: Huawei Watch Fit 3: Almost an Apple Watch (Scientific Review)](https://www.youtube.com/watch?v=7l0JGcrhbNc)
- Model `SLO-B09`
- [󰖬 HarmonyOS version history](https://en.wikipedia.org/wiki/HarmonyOS_version_history)
  - Starting with version 5 it's called [HarmonyOS NEXT](https://en.wikipedia.org/wiki/HarmonyOS_NEXT)

## Setup with Gadgetbridge

- [Docs: Huawei Watch Fit 3](https://gadgetbridge.org/gadgets/wearables/huawei-honor/#device__huawei_watch_fit_3)
- [Docs: Huawei gadgets](https://gadgetbridge.org/basics/topics/huawei-honor/#supported-features)
- [Docs: Specifics for Huawei and Honor devices](https://gadgetbridge.org/internals/topics/huawei-honor-specifics/)
- Pairs directly with Gadgetbridge, no HUawei app is needed

Issues:

- No Firmware updates possible (Huawei health app needed)

## Firmware upgrade

- Only possible with original [Huawei health app](https://consumer.huawei.com/de/mobileservices/health/)
- Not in Play store, install from website
- Annoying: Need to register with Huawei ID
- After logging in with Huawei account, pair with device
- Upgrade Firmware
- Deinstall App
- Do a factory reset of the watch, in order to allow later pairing with Gadgetbridge
- Pair with Gadgetbridge
