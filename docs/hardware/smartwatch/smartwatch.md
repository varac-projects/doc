# Smartwatches / fitness trackers

- [Mozilla Privacy Not Included: Wearables](https://foundation.mozilla.org/en/privacynotincluded/categories/fitness-trackers/)
- [Top #5 Open Source Smartwatch That You Can Buy](https://diyusthad.com/2021/04/top-5-open-source-smartwatch.html)
- [The 11 Best Heart Rate Monitor Watches for 2023](https://www.healthline.com/nutrition/best-heart-rate-monitoring-watch)

## Software

### Gadgebridge (Android)

- [Website and Docs](https://gadgetbridge.org/)
- [Codeberg page](https://codeberg.org/Freeyourgadget/Gadgetbridge)
  - [Tags](https://codeberg.org/Freeyourgadget/Gadgetbridge/tags)
- [Data management](https://gadgetbridge.org/internals/development/data-management/#exporting-your-data)
  - [Exporting your data](https://gadgetbridge.org/internals/development/data-management/#exporting-your-data)

### Astroid OS

- [Website](https://asteroidos.org/)
- Open source Linux distribution that runs on many different smartwatches
- Slow development
- No good GPS support

## Hardware

### Huawei Watch Fit 3

See [./huawei-watch-fit-3.md](./huawei-watch-fit-3.md)

### Pinetime

See [./pinetime.md](./pinetime.md)

### Garmin

- Ranked well on [Mozilla Privacy Not Included: Wearables](https://foundation.mozilla.org/en/privacynotincluded/categories/fitness-trackers/)
- [Garmin connect](https://connect.garmin.com/) [seems to work fine on LineageOS
  without Google play services](https://www.reddit.com/r/LineageOS/comments/oug7v2/using_garmin_smartwatch_on_lineageos/)

### Samsung Watch 4

- WearOS
- [Samsung apps don't play well with LineagoOS without Google play services](https://www.reddit.com/r/LineageOS/comments/sf4f8m/galaxy_watch_4_and_lineage_os/)
  > To use the device with Google WearOS services, you would need Google Play
  > on the phone

### Bangle.js 2

- Bangle.js 2 is an open, hackable smartwatch
- [Website](https://www.espruino.com/Bangle.js2)
- [Berrybase](https://www.berrybase.de/detail/index/sArticle/9697/sCategory/2478)
- [Bangle.js Gadgetbridge](https://www.espruino.com/Gadgetbridge)
  - [Fdroid](https://f-droid.org/en/packages/com.espruino.gadgetbridge.banglejs/)
- [App Store](https://banglejs.com/apps)

#### Heart rate monitor

- [Heart rate monitor not accurate](https://forum.espruino.com/conversations/372681/)
- [Bangle JS 2 owners, how is the accuracy of the heart rate monitor?](https://www.reddit.com/r/Banglejs/comments/vgoo6z/bangle_js_2_owners_how_is_the_accuracy_of_the/)

#### GPS

Apps needed for GPS:

- AGPS
- [Recorder](https://github.com/espruino/BangleApps/tree/master/apps/recorder)

Other [GPS apps](https://banglejs.com/apps/?q=gps):

- [Openstreetmap](https://github.com/espruino/BangleApps/tree/master/apps/openstmap)
  - Map section needs to get uploaded for offline use,
    no way of downloading maps from within the app
