# Robotics

## LEGO

see [./lego.md](./lego.md)

## LEGO alternatives

- [Brickpi](https://www.dexterindustries.com/brickpi/)
  - [Linux Magazin: Lego-Roboter mit dem Raspi 3 steuern](https://www.linux-magazin.de/ausgaben/2018/08/brick-pi/)
