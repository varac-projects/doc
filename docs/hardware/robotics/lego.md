# LEGO Education robotics

- [Wikipedia: LEGO Education](https://de.wikipedia.org/wiki/LEGO_Education)
  - [Construction sets](https://en.wikipedia.org/wiki/Lego_Education#Construction_sets)

## LEGO Spike Prime

- [FAQ zu LEGO® Mindstorms® EV3 und SPIKE™ Prime](https://www.brickobotik.de/faq-zu-lego-mindstorms-ev3-und-spike-prime/)
- [Linux User: Mit Lego Education Spike spielerisch programmieren lernen](https://www.linux-community.de/ausgaben/linuxuser/2023/12/mit-lego-education-spike-spielerisch-programmieren-lernen/)
- [Test: SPIKE™ Prime auf dem Prüfstand (Teil 1 von 3)](https://www.brickobotik.de/test-spike-prime-auf-dem-pruefstand-teil-1-von-3/)
- [Spike Prime vs EV3](https://www.reddit.com/r/FLL/comments/1e07co9/spike_prime_vs_ev3/)

### Compability with Mindstorms

[Compability with Mindstorms](https://news.ycombinator.com/item?id=41072347):

> Spike Prime and Mindstorms use the same Hub just different colors.
> Spike Prime has a large motor, 2 small motors, a distance sensor, color sensor, and force sensor.
> The Mindstorms set had 4 small motors, a distance sensor, and color sensor.
> Spike Prime is Mindstorms for all intents and purposes.
> You can even get either the Spike Prime or Mindstorms app and use it with either Hub.

However, see [Can I use a MINDSTORMS Robot Inventor hub with SPIKE Prime?](https://www.antonsmindstorms.com/2024/07/04/can-i-use-a-mindstorms-robot-inventor-hub-with-spike-prime/)

> The MINDSTORMS hub has the same shape as the SPIKE hub, so you’d expect
> the firmware to be compatible. Until SPIKE2 — the previous major version —
> that was the case. But the current version, SPIKE3, stopped working.
> I was unable to install SPIKE3 on a MINDSTORMS hub. The SPIKE app tells me
> that ‘An error occurred’ as soon as I connect a MINDSTORMS hub.

### Hardware

Sets:

- [LEGO Education SPIKE Prime-Set 45678](https://www.lego.com/de-de/product/lego-education-spike-prime-set-45678)
  - three motors (1 large 2 medium) and
    sensors for distance, force and color,
    a controller brick based on an STM32F413 microcontroller[30]
    and 520+ Lego Technic elements
- [LEGO® Education SPIKE™ Essential-Set 45345](https://www.google.com/search?client=firefox-b-d&q=45345)
- [LEGO Education SPIKE Prime-Erweiterungsset 45681](https://www.lego.com/de-de/product/lego-education-spike-prime-expansion-set-45681)
  including [mounting plate](https://www.brickowl.de/catalog/lego-magenta-panel-11-x-15-with-pcb-holes-65817)
  for [Raspberry Pi Build HAT](https://www.raspberrypi.com/products/build-hat/)

### Software

#### Pybricks

- [Website](https://pybricks.com/)

> Pybricks runs on LEGO® BOOST, City, Technic, MINDSTORMS®, and SPIKE®.
> You can code using Windows, Mac, Linux, Chromebook, and Android.

- Scatch frontend only as paid SaaS
- [Installing Pybricks on LEGO MINDSTORMS EV3](https://pybricks.com/install/mindstorms-ev3/installation/)

> Pybricks for MINDSTORMS EV3 uses Pybricks version 2.0,
> while newer hubs like SPIKE, Technic, and MINDSTORMS Inventor
> use Pybricks 3.0 or higher.
> This means you can only code your EV3 with MicroPython in Visual Studio Code.
> The simpler online interface with block-based coding is not available for EV3.
> If you would like to see continued EV3 support, including block-based coding
> in our web app, please let us know on our forum. If we can find enough teachers
> and developers who want to get involved, perhaps we can make this happen.

#### Other

- [Host-hub communication for LEGO Spike Prime on Linux](https://libdoc.fh-zwickau.de/opus4/frontdoor/deliver/index/docId/15400/file/lego_spike_linux.pdf)
- [lego-spike-electron-app](https://github.com/joeyparrish/lego-spike-electron-app)
  - LEGO SPIKE web app in Electron
  - `lego-spike-git` AUR package

Outdated:

- [spikeprime-tools](https://github.com/sanjayseshan/spikeprime-tools)
  Last commit 2022-06
- [spike-prime](https://github.com/gpdaniels/spike-prime)
  Last commit 2023-08

### Learning resources

- [Prime lessions](https://primelessons.org)

## LEGO Boost

- 2017–present
- [Scratch wiki: LEGO Boost](https://de.scratch-wiki.info/wiki/Lego_Boost)

## Legacy products

### LEGO Mindstorms

- [Wikipedia: Lego Mindstorms](https://en.wikipedia.org/wiki/Lego_Mindstorms)
- 3 Generations
  - Robotics Invention System (launched in 1998)
  - Mindstorms NXT (launched in 2006)
  - Mindstorms EV3 (launched in 2013)
    - [LEGO Mindstorms EV3 31313 Set](https://www.lego.com/de-lu/product/lego-mindstorms-ev3-31313)
  - Mindstorms Robot Inventor (launched in 2020)
    - [Mindstorms Robot Inventor 51515](https://www.lego.com/en-de/product/robot-inventor-51515)
      - [Lego Mindstorms Robot Inventor](https://en.wikipedia.org/wiki/Lego_Mindstorms#Lego_Mindstorms_Robot_Inventor):
        "It has four medium motors from Spike Prime,
        two sensors (distance sensor and color/light sensor) also from Spike Prime,
        a Spike Prime hub with a six-axis gyroscope, an accelerometer,
        and support for controllers and phone control.
        It also has 902+ Lego Technic elements
        This set was discontinued in 2022 with Lego promising app support through 2024."

### Mindstorms software

- [ev3dev](https://www.ev3dev.org/)
  - "Debian Linux-based operating system that runs on several LEGO® MINDSTORMS
    compatible platforms including the LEGO® MINDSTORMS EV3 and Raspberry Pi-powered BrickPi."
