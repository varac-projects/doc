# Serial

## USB adapters

Allow access to `ttyUSB*`:

    sudo usermod -aG uucp varac
    newgrp uucp

Start minicom/screen/picocomm:

    minicom -D /dev/ttyUSB0 -b 1500000
