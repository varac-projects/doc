# Console keyboard setup

https://wiki.hetzner.de/index.php/CloudServer#Die_Tastaturbelegung_im_Konsolenfenster_scheint_falsch_zu_sein._Wie_kann_ich_das_beheben.3F

Requires a reboot:

    sudo dpkg-reconfigure keyboard-configuration

# Graphical keyboard setup

## Show keys presed

    xev

## i3

Defined in `~/.i3/config`:

- Super+home: `~/.screenlayout/home.sh`
- Super+end: `~/.screenlayout/laptop-only.sh`

Those execute: `~/bin/usb_keyboard_udev.sh`

This script will modify the keyboard by using `setxkbmap` and `xmodmap`.

## Show current setxkbmap layout

    setxkbmap  -query

## Possible options for setxkbmap

see `/usr/share/X11/xkb/rules/evdev.lst`

## Change keyboard layout

    setxkbmap us

## Special Characters

Compose key is configured with setxkbmap

    setxkbmap -option compose:rwin

Then type:

- Right Windows + Shift + " + a = ä
- Right Windows + Shift + " + Shift + a = Ä
  ...
- Right Windows + s +s = ß

## xmodmap

https://wiki.archlinux.org/index.php/xmodmap

### Show current map

    xmodmap -pke

### Re-map keys

see `~/.Xmodmap`, i.e.

    ! Thinkpad x230: PrtSc
    keycode 107 = Super_R

# Hardware related keyboard modding

[Standard US qwerty layout](https://en.wikipedia.org/wiki/QWERTY#/media/File:Qwerty.svg)

## Apple alu keyboard

https://wiki.archlinux.org/index.php/Apple_Keyboard

see `/etc/modprobe.d/hid_apple.conf`
