# Raspberry Pi

- [Performance tuning](http://raspberry.tips/raspberrypi-tutorials/raspberry-pi-performance-tuning-und-tweaks)

## Detect hardware model

```sh
dmesg |grep Raspberry
```

## Write image

```sh
sudo dd bs=4M if=2016-05-27-raspbian-jessie.img of=/dev/mmcblk0; sudo sync
```

### Raspberry pi imager

Install:

```sh
sudo pacman -S rpi-imager
```

Config is stored at `~/.config/Raspberry\ Pi/Imager.conf`

## Login

with user:pi, pw:raspberry

## enable ssh

```sh
systemctl start  ssh
systemctl enable ssh
```

### login via ssh

```sh
ssh-copy-id raspberry.local
ssh raspberry.local
```

## Change keyboard layout and locales

```sh
sudo dpkg-reconfigure keyboard-configuration
```

Generic 105 kb, US, no options

```sh
sudo dpkg-reconfigure locales
```

- en_US.UTF-8 UTF-8
- No default locale

## Change pw for pi user

```sh
tmux
passwd
```

## Only for GUI versions

### Disable Screensaver

[Disable screensaver](http://stackoverflow.com/a/31028089)

```sh
 sudo apt-get install xscreensaver
```

Go to preference ---> screensaver.
You will see a screen saver main menu. In mode : section,
select "disable screensaver" and close, reboot.

### Autostart

[How can I add new autostart programs in Lubuntu?](https://askubuntu.com/questions/81383/how-can-i-add-new-autostart-programs-in-lubuntu)

```sh
mkdir ~/.config/autostart
ln -s /usr/share/applications/firefox-esr.desktop ~/.config/autostart/firefox-esr.desktop
```

### Issues

#### Stream Webcam

```sh
./mjpg_streamer -i "./input_uvc.so -m 2  -f 6 -r VGA" -o "./output_http.so -w ./www"
```

## keyboard-setup

### for console

```sh
dpkg-reconfigure keyboard-configuration
setupcon
```

## WLAN

### ad-hoc

Use [connman](../../../network/connman.md)

### Permanent

#### raspi-config

Works with raspi-config as well.

#### Manual

[WPA-PSK and WPA2-PSK](https://wiki.debian.org/WiFi/HowToUse#WPA-PSK_and_WPA2-PSK)

```sh
chmod 0600 /etc/network/interfaces
wpa_passphrase Hangtheking PW
```

edit /etc/network/interfaces:

```sh
auto wlan0
iface wlan0 inet dhcp
  wpa-ssid myssid
  wpa-psk ...

ifdown wlan0
ifup   wlan0
```

## GPIO

[RPi2 GPIO Header Pins](https://static1.squarespace.com/static/5258a733e4b0804fa2549966/t/579ae69ae4fcb548820317f0/1469769378106/?format=600w)

```sh
echo 4 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio4/direction # set it as an output pin
echo 1 > /sys/class/gpio/gpio4/value # set the value to ON
echo 0 > /sys/class/gpio/gpio4/value # set the value to OFF
echo in > /sys/class/gpio/gpio4/direction # set it as input
cat /sys/class/gpio/gpio4/value # get the value
echo 4 > /sys/class/gpio/unexport # disables pin 4 and removes the gpio4 directory
```

## Video

[What is the difference between CEA and DMT?](https://raspberrypi.stackexchange.com/questions/7332/what-is-the-difference-between-cea-and-dmt)

Show preferred resolution:

```sh
/opt/vc/bin/tvservice -d /tmp/edid.dat; /opt/vc/bin/edidparser /tmp/edid.dat
```

### Set icon size and font for large screens

- [How to change system font and icon size](https://www.radishlogic.com/raspberry-pi/how-to-change-the-system-font-and-icon-size-of-raspberry-pi-desktop/)

Settings -> Appearance Settings -> Defaults -> For large screens

### Disable screen blanking

Use `raspi-config`:

- Display Options
  - Disable screen blankking

### Increase GPU mem

Use `raspi-config`:

- Performance options
  - GPU Mem: 256

### DRM

- Raspberry Pi OS 64bit now supports Widevine.
- [Installing Widevine DRM to the Raspberry Pi](https://pimylifeup.com/raspberry-pi-widevine/)

### Enable sound over HDMI

[How to enable sound on HDMI?](https://raspberrypi.stackexchange.com/a/32783)

Add this to `config.boot`:

```sh
hdmi_group=1
hdmi_drive=2
```

### No HDMI output after a while

[How do I force the Raspberry Pi to turn on HDMI?](https://raspberrypi.stackexchange.com/questions/2169/how-do-i-force-the-raspberry-pi-to-turn-on-hdmi)

Add these two lines to /boot/config.txt and reboot Raspbmc:

```sh
hdmi_force_hotplug=1
hdmi_drive=2
```

## config.txt docs

- [RPi Docs: The config.txt file](https://github.com/raspberrypi/documentation/blob/develop/documentation/asciidoc/computers/config_txt/what_is_config_txt.adoc)
