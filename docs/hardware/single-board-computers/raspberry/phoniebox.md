# Phoniebox

Raspberry Pi RFID-Jukebox

- [Website](http://phoniebox.de)
- [GitHub](https://github.com/MiczFlor/RPi-Jukebox-RFID)
- [Docs](https://github.com/MiczFlor/RPi-Jukebox-RFID/tree/future3/main/documentation)
- [Python backend re-implementation](https://github.com/laclaro/python-phoniebox)

## Similar Projects

- [home-assistant tags](https://www.home-assistant.io/blog/2020/09/15/home-assistant-tags/)
- [piCorePhonie](https://github.com/maahn/piCorePhonie)
- [MuPiBox](https://mupibox.de/)
- [TonUINO](https://www.voss.earth/tonuino): Arduino-based, no Spotify, only
  local mp3s
- [ESPuino](https://github.com/biologist79/ESPuino)
  - RFID-controlled musicplayer powered by ESP32

### Unmaintained

- [pimusicbox](https://github.com/pimusicbox/pimusicbox)
- [Sonos-Kids-Controller](https://github.com/Thyraz/Sonos-Kids-Controller)

## Hardware

- Raspberry Pi 3 Model B Plus Rev 1.3
- [Suptronics x400](https://wiki.geekworm.com/X400)
- MFRC522 RFID Reader (see below)

### Additional hardware options

- [Best battery solution in 2024 for Pi 3](https://github.com/MiczFlor/RPi-Jukebox-RFID/discussions/2270)

## Usage

Web-Interface:

- [Main phoniebox webinterface](http://phoniebox.local)
- Started by systemd service `nginx`
- Config at `/etc/nginx/sites-enabled/default`
  - Webapp root dir `/home/varac/RPi-Jukebox-RFID/src/webapp/build`
- Logs at
  - `/var/log/nginx/*.log`

Config / Locations:

- Root dir: `~/RPi-Jukebox-RFID`
- Audio files: `shared/audiofolders/`
- RFID to directory mappings: `shared/settings/cards.yaml`
- (auto generated) Playlists: `shared/playlists/`
- Logs: `shared/logs`

## Debugging

- Enable debug logging for different modules in `shared/settings/jukebox.yaml`
- `journalctl -f | grep -Eiv '(mpd|do-add-ip6-address|IPv6)'`

## Raspberry PI OS Installation

- Install Raspberry Pi OS _32bit_: [mopidy/spotify won't install/run on 64bit](https://discourse.mopidy.com/t/pyspotify-not-working-on-raspberry-pi-4-arm64-architecture/3885/4)
- npm packages need npm v14 -> at least `bookworm`
- see also `~/Howtos/hardware/raspberry/raspberry-pi-os.md`
- Enable SPI interface in `raspi-config`
- Configure x400 soundcard (`./expansion-boards/x400.md`)

## Known issues

- [future3/develop: known issues](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/develop/documentation/developers/known-issues.md)
- [Firefox: Webapp / websockets not working](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/1633)
  - Use Chromium until this is fixed

## Future3 installation

- [future3 docs](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/README.md)
  - [Installation](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/builders/installation.md)
  - [Configuration](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/builders/configuration.md)
  - [Feature status](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/developers/status.md)
- Installation logs are in `~/RPi-Jukebox-RFID/INSTALL*`

```sh
systemctl --user status jukebox-daemon.service
```

### Future3 upgrade

- [future3/main update guide](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/builders/update.md)
- [furute/develop update guide](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/develop/documentation/builders/update.md)
- FR: [Easy updating](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2304)

[Version upgrades](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2304#issuecomment-2247805245):

```sh
cd RPi-Jukebox-RFID
git pull
source .venv/bin/activate
python -m pip install --no-cache-dir -r requirements.txt
```

### Future3 issues

Solved:

- [🐛 | Repeat alias/action doesn't actually work](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2322)
  Solution: [remove the empty args: from the cards.yaml](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2322#issuecomment-2041381613)
- [PlayerMPD.prev/next crash at the start/end of the playlist](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2294)
  - Solution: Use `feature3/develop` until [Fix PlayerMPD.prev/next() when stopped](https://github.com/MiczFlor/RPi-Jukebox-RFID/commit/a9ab571ac90cce21b94fef705855b9598c941a30)
    is merged into `fetaure3/main`
- `RPi.GPIO` and `pi-` is broken in RPI OS bookworm, see
  - [bookworm/kernel 6.6 GPIO: Failed to add edge detection](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2313)
    - Solution: [Install rpi-lgpio](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2313#issuecomment-2124397117)
  - [Sharing tips for future3 on bookworm/kernel 6.6 and upstreaming fixes](https://github.com/MiczFlor/RPi-Jukebox-RFID/discussions/2295)
  - Solution: See [this comment](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/2313#issuecomment-2030579144)

Old / other:

- [Alphabetical playlist order instead of id3 tag track number based on future3](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/1851)
- [3 minute start-up time #1110](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/1110)

Feature requests:

## Future3 Usage

```bash
systemctl --user status mpd
systemctl --user status jukebox-daemon
```

Logs are at `~/RPi-Jukebox-RFID/shared/logs`

```sh
tail ~/RPi-Jukebox-RFID/shared/logs/app.log
```

## GPIO usage

- [GPIO Docs](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/builders/gpio.md)
- All possible InputDevices see [input_devices.py](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/src/jukebox/components/gpio/gpioz/core/input_devices.py)
- Settings: `shared/settings/gpio.yaml`

### GPIO Hardware

Both RFID-Reader and buttons are connected to GPIO bus.

Standard wiring as suggested in the [GPIO docs](https://github.com/MiczFlor/RPi-Jukebox-RFID/wiki/Using-GPIO-hardware-buttons#how-to-connect-the-buttons)

#### RFID

- Module: Neuftech Mifare RC522 IC Card RFID Module
- [MFRC522 RFID Reader](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/developers/rfid/mfrc522_spi.md)
  > MFRC522 RC522 can read/write RFID cards and tags built using ISO/IEC 14443 protocol.
  > This includes MIFARE-compatible RFID tags like MIFARE-mini, MIFARE-1K,
  > MIFARE-4K RFID tags, Key Fob, and NTAG RFID cards.
- Directly supported by RPi-Jukebox-RFID install script (`rc522`)
- [RFID Module Wiring](https://tutorials-raspberrypi.de/raspberry-pi-rfid-rc522-tueroeffner-nfc/)
- [Button wiring](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/259#issue-369742810)
  - Pin 1 (3.3V): RFID 3V (red)
  - Pin 9 (GND): RFID GND (black)
  - Pin 19 (GPIO10): RFID MOSI
  - Pin 21 (GPIO9): RFID MISO
  - Pin 22 (GPIO25): RFID RST (Blue)
  - Pin 23 (GPIO11): RFID SCKL
  - Pin 24 (GPIO8): RFID SDA

#### Buttons

- Pin 13 (GPIO27): Button play/pause toggle (yellow cable)

- Pin 15 (GPIO22): Button prev (green cable)

- Pin 16 (GPIO23): Butto next (brown cable)

- `RPi.GPIO` is broken in RPI OS bookworm, see [Sharing tips for future3 on bookworm/kernel 6.6 and upstreaming fixes](https://github.com/MiczFlor/RPi-Jukebox-RFID/discussions/2295)

- [Using GPIO hardware buttons](https://github.com/MiczFlor/RPi-Jukebox-RFID/wiki/Using-GPIO-hardware-buttons)

- [GPIO CONTROL](https://github.com/MiczFlor/RPi-Jukebox-RFID/tree/master/components/gpio_control)

- [pinout.xyz](https://pinout.xyz/): RPi pin documentation

### Button Wiring issues

Connecting a button to `Pin 40 (GPIO21)` while using the
[Suptronics x400 audio expansion board](https://wiki.geekworm.com/X400)
results in muted audio when pressing manually,
or when starting `scripts/gpio-buttons.py` with default pin layout.

### Spotify

- [Spotify does not work anymore](https://github.com/MiczFlor/RPi-Jukebox-RFID/issues/1815)
  - [Spotify-FAQ](https://github.com/MiczFlor/RPi-Jukebox-RFID/wiki/Spotify-FAQ)
  - Upstream mopidy-spotify issue: [Streaming does not work](https://github.com/mopidy/mopidy-spotify/issues/110)
  - Groundwork for v3: [Multi-Player support](https://github.com/MiczFlor/RPi-Jukebox-RFID/pull/2164)

## Development

[docs: developers](https://github.com/MiczFlor/RPi-Jukebox-RFID/blob/future3/main/documentation/developers/README.md)

```sh
cd ~/RPi-Jukebox-RFID
```

Configure `./shared/settings/jukebox.yaml`

Build:

```sh
docker compose -f docker/docker-compose.yml -f docker/docker-compose.linux.yml build
```

start:

```sh
docker-compose -f docker/docker-compose.yml -f docker/docker-compose.linux.yml up
```
