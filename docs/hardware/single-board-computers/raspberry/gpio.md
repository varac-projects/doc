# Raspberry PI GPIO

- [RPi docs: GPIO and the 40-pin header](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#gpio)
  - RPi A+/B+, Pi 2/3/4 pin numbers and GPIO numbers

## Libraries

- [A history of GPIO usage on Raspberry Pi devices, and current best practices, 2024-08](https://pip.raspberrypi.com/categories/685-whitepapers-app-notes-compliance-guides/documents/RP-006553-WP/A-history-of-GPIO-usage-on-Raspberry-Pi-devices-and-current-best-practices.pdf)

Legacy:

- [WiringPi](https://github.com/WiringPi/WiringPi)
  - "Due to the underlying hardware changes on Raspberry Pi 5,
    Wiring Pi does not yet work on this device. For this reason, it is
    not yet recommended for new projects. Wiring Pi is actively developed,
    so more recent versions may work on Raspberry Pi 5."
- [pigpio](https://github.com/joan2937/pigpio)
  - Last commit 2024
- [pigpiod](https://abyz.me.uk/rpi/pigpio/pigpiod.html)
  - A daemon/resident version of pigpio which accepts commands from pipe and socket interfaces
- [lgpio](https://pypi.org/project/lgpio/#description)
  - Last release 2024-03, no gti repo found
  - Same author as `pigpio` and `pigpiod`
- [RPi.GPIO](https://pypi.org/project/RPi.GPIO/#history)
  - Last release 2022
  - [Mock.GPIO](https://github.com/codenio/Mock.GPIO)
    Mock Library for RPI.GPIO python Library

Current libraries:

- libgpiod: see below
- [gpiozero](https://gpiozero.readthedocs.io/en/latest/)
  - [GitHub](https://github.com/gpiozero/gpiozero)
  - Last tag 2023-02
- `pinctrl` command
  - Display all the GPIO chips on a Raspberry Pi 5: `pinctrl get 26-42`

### libgpiod

- [libgpiod](https://libgpiod.readthedocs.io/en/latest/index.html)
- [GitHub](https://github.com/brgl/libgpiod/)
- [Configfs GPIO Simulator](https://www.kernel.org/doc/html/latest/admin-guide/gpio/gpio-sim.html)

> There are two versions of libgpiod, version 1 and version 2. At present,
> Raspberry Pi OS includes the version 1 library, so you should take that
> into account when using the various tools as their behaviour does change
> between versions.

#### Setup

> By default, libgpiod follows the Linux ethos of taking control of a peripheral,
> allowing the peripheral to be used, and, when the process ends,
> returning the peripheral to its previous state. This means that there is no
> guarantee that when a program to set a GPIO to a particular value exits,
> the value will remain as set. It may or may not be returned to the state
> from before the set program was run.
> In order to make the move from the persistent operations of the previous sysfs to
> libgpiod easier, the Raspberry Pi kernel modifies this behaviour
> so that the state of a GPIO IS retained when a program
> that has changed the value (e.g. gpioset, see below) exits.

This means, without the following modification a LED turned on with i.e.
`gpioset --mode time --sec 3 gpiochip0 17=1` will **not** reliably turn off
after the command exits.

Enable the automatic reversion behaviour of libgpiod by adding the following to the config.txt file:

```toml
dtparam=strict_gpiod
```

#### CLI usage

General info:

```sh
gpiodetect
gpioinfo
```

Find GPIO:

```sh
$ gpiofind STATUS_LED
gpiochip0 47
```

Get state of pin 21:

```sh
gpioget gpiochip0 21
```

Monitor state of pin:

```sh
gpiomon gpiochip0 21
```

Set GPIO3 high for three seconds, then return to default:

```sh
gpioset --mode time --sec 3 gpiochip0 3=1
```

#### Python bindings

- [gpiod](https://github.com/brgl/libgpiod/tree/master/bindings/python)
  - [examples](https://github.com/brgl/libgpiod/tree/master/bindings/python/examples)
- [Pypi: gpiod](https://pypi.org/project/gpiod/)
- [Debian package: gpiod](https://packages.debian.org/trixie/gpiod)
  - Pre-installed on Raspberry Pi OS

Tutorials:

- [Manage the GPIO lines in Python3 and C with libgpiod](https://www.acmesystems.it/libgpiod)
