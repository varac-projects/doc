# Mediaplayer setup

## Hardware

- RPi 4
- [hifiberry DAC+ Light](https://www.hifiberry.com/shop/boards/hifiberry-dac-light/)
  Sound/audio card hat

## Setup

- Flash raspberry pi OS using rpi-imager (use stored config settings for user,
  hostname, locale, wifi setup etc.)
- If not using rpi-imager configure manually:
  - Set hostname
    - Using hostnamectl (`~/Howtos/systemd/hostnamectl.md`)
    - or via Settings/Screen configuration menu entry
  - `sudo raspi-conifg`
    - Interface options
      - enable ssh

### Hifiberry

Not needed because the mediaplayer uses audio via HDMI

- [Hifiberry config docs](https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/)
- Add `dtoverlay=hifiberry-dac` to `/boot/config.txt`

### Etc

- Finish setup wizard

  - Display Options
    - Disable screen blankking
  - Performance options
    - GPU Mem: 256

- `apt update && apt upgrade`

- Run ansible

  ```bash
  cd ~/ansible/ varac
  ansible-playbook -l mediaplayer site.yml
  ```

- [Configure chromium browser for netflix etc](https://blog.vpetkov.net/2020/03/30/raspberry-pi-netflix-one-line-easy-install-along-with-hulu-amazon-prime-disney-plus-hbo-spotify-pandora-and-many-others/)

- reboot

Run `ansible-playbook` again (?)

## Additional tasks

- Turn off screen saver (Settings → Privacy → Screenlock)
- Bigger fonts (Settings → Accessability)

### Autostart in fullscreen mode

`firefox-fullscreen.sh`:

```sh
#!/bin/sh
firefox &
xdotool search --sync --onlyvisible --pid $! windowactivate key F11
```

`~/.config/autostart/firefox.desktop`:

```ini
[Desktop Entry]
Type=Application
Exec=/home/casita/firefox-fullscreen.sh
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=Firefox
Name=Firefox
Comment[en_US]=
Comment=
```
