# Ansible for Raspberry Pi

## Collections

- [oxivanisher/collection-raspberry_pi](https://github.com/oxivanisher/collection-raspberry_pi)
  - Contains multiple roles, i.e.
    - [oxivanisher/role-tmp_ramfs](https://github.com/oxivanisher/role-tmp_ramfs)
    - No `config.txt` config option
- [gjhenrique/rpi_stuff](https://github.com/gjhenrique/rpi_stuff)
  - No `config.txt` config option
- [expeditioneer/ansible-collection-raspberry-pi](https://github.com/expeditioneer/ansible-collection-raspberry-pi)
  Last commit 2023-02

## Roles

- [infothrill/ansible-role-rpi_boot_config](https://github.com/infothrill/ansible-role-rpi_boot_config)
- [geerlingguy/ansible-role-raspberry-pi](https://github.com/geerlingguy/ansible-role-raspberry-pi)
  - Configures `config.txt` and `rc.local`
- [gueguet.raspberry_pi_config](https://github.com/gueguet/raspberry_pi_config)
  No `config.txt` config option
