# Digital signage

- [elinux wiki: RPi Projects/Digital Signage](https://elinux.org/RPi_Projects/Digital_Signage)
- [Digital signage and kiosk with Ubuntu](../raspberry/digital-signage.md)

## Manual

- [Raspberry Pi - Raspbian Update auf Buster und Kioskmodus mit Chrome anpassen](https://itrig.de/index.php?/archives/2372-Raspberry-Pi-Raspbian-Update-auf-Buster-und-Kioskmodus-mit-Chrome-anpassen.html)

## Off-the-shelf

### PiSignage

- [Website](https://pisignage.com/)
- [Github](https://github.com/colloqi/piSignage)

Best solution so far

- Easy installation (flash, run, navigate to shown URL)
- Web-based configuration
- Can display grafana dashboards
- Can cycle through multiple URLs
- SSH enabled by default, user `pi` with pw `pi`
- No [configuration provisioning](https://github.com/colloqi/piSignage/issues/107)
- Logs are at `/home/pi/piSignagePro/*.log`

Issues:

- nameserver in `/etc/resolv.conf` is mananged by resolvconf (points to
  `127.0.0.1`), and doesn't use the DHCP-provided nameserver which breaks
  local DNS lookup. Manually setting it works though.

### Screenly

- [Website](https://anthias.screenly.io/)
- [Github, Images](https://github.com/screenly/screenly-ose/)

Very nice, just flash it and run it, it will show the URL to the management
web interface. Can cycle though different URLs. BUT:

- [Can't display grafana dashboards](https://github.com/Screenly/screenly-ose/issues/1293)

### Raspberry Digital Signage

- [Website](https://www.binaryemotions.com/digital-signage/raspberry-digital-signage/)
- Some features only available in paid image
- No source code available

### PiPresents KMS

- [Website](https://pipresents.wordpress.com/)
- No built images, installation based on raspbian

### FullpWebsiteageOS

- [Github](https://github.com/guysoft/FullPageOS)
- One URL conly can get set in `/boot/fullpageos.txt`.
- [Configuration of multiple cycle pages is horrible](https://github.com/guysoft/FullPageOS/issues/36#issuecomment-399247665)
  and does not even work.
- Comes with VNC preinstalled

## Stale / unmaintained

### Chillipie-Kiosk

- [Website](https://github.com/futurice/chilipie-kiosk)
- Still [no buster image](https://github.com/futurice/chilipie-kiosk/issues/61)
- [No URL provisioning](https://github.com/futurice/chilipie-kiosk/issues/38)
- cycling through tabs done by a cronjob
