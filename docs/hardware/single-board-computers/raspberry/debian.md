# Vanilla Debian on raspberry pi

* [Debian wiki: RaspberryPi](https://wiki.debian.org/RaspberryPi)
* [Debian wiki: RaspberryPiImages](https://wiki.debian.org/RaspberryPiImages)

## On a rock pro 64 board

* [ROCKPro64 Software Releases: Debian](https://wiki.pine64.org/wiki/ROCKPro64_Software_Releases#Debian)
