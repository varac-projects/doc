# Web kiosk

- [geerlingguy/pi-kiosk](https://github.com/geerlingguy/pi-kiosk)
- [FullPageOS](https://github.com/guysoft/FullPageOS)
- [Cage](https://github.com/cage-kiosk)

Proprietary:

- [porteus-kiosk](https://porteus-kiosk.org/)
