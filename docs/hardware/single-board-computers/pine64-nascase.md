# RockPro64

* [RockPro64 in Pine64 Shop](https://www.pine64.org/rockpro64)
* [RockPro64 Wiki page](https://wiki.pine64.org/wiki/ROCKPro64)
<https://wiki.pine64.org/index.php/ROCKPro64_Hardware_Accessory_Compatibility>

* no german retailer
* No real support, forum only
* The ROCKPro64 4GB board designated as LTS (long Term Supply) model, PINE64
  committed to supply at least for 5 years until year 2023 and beyond.

* [Pine64 forum: HOW TO - Connect the serial console](https://forum.pine64.org/showthread.php?tid=6387)

## Pine64 NASCase

<https://wiki.pine64.org/wiki/NASCase>
<https://pine64.com/product/rockpro64-metal-desktop-nas-casing/?v=0446c16e2e66>

Components:

* [ROCK64-4GB Single Board Computer](https://pine64.com/product/rock64-4gb-single-board-computer/?v=0446c16e2e66):
* [ROCKPro64 PCI-e to Dual SATA-II Interface Card](https://pine64.com/product/rockpro64-pci-e-to-dual-sata-ii-interface-card/?v=0446c16e2e66):
* [ROCKPro64 Metal Desktop/NAS Casing](https://pine64.com/product/rockpro64-metal-desktop-nas-casing/?v=0446c16e2e66):
  * ROCKPro64 SBC, Heatsink, Fan, and PCIe SATA adapter not included
  * Please use +12V 5A power supply due to HDDs power consumption
* <https://wiki.pine64.org/index.php/ROCKPro64_Hardware_Accessory_Compatibility>

## Software options

* [RockPro64 Spftware options](https://wiki.pine64.org/wiki/ROCKPro64_Software_Release)
  * [Pine64 A64(+) + Armbian Buster + Docker](https://www.acervera.com/blog/2020/04/pine64-a64-armbian-docker/index.html)
  * [DietPi](https://wiki.pine64.org/wiki/ROCKPro64_Software_Releases#DietPi)

### Official Debian

* [Pine64 wiki: Official Debian](https://wiki.pine64.org/wiki/ROCKPro64_Software_Release#Official_Debian)
  * [Pine64 forum: Official Debian support](https://forum.pine64.org/showthread.php?tid=9744&pid=93500#pid93500)
  * [Pine64 forum: Vanilla mainline Debian 11 (Bullseye) on the RockPro64](https://forum.pine64.org/showthread.php?tid=14939&page=3)
* [Debian wiki: FreedomBox Hardware Rock Pro64](https://wiki.debian.org/FreedomBox/Hardware/RockPro64)
* Minimal SDcard size: 8GB (4GB is *not* enough, the installer stalls during the
  installation)

#### U-Boot in SPI

I haven't found a way of booting Debian without flashing uboot into SPI flash.
[Here](https://forum.pine64.org/showthread.php?tid=9744&pid=119256#pid119256) is
someone who did, but it results in a more additional steps during
the Debian installation.
Check U-Boot version with serial connection. If it is too old, flash a recent u-boot to SPI.
My installation worked with u-boot v2021.04 from the [latest Sigmaris
u-boot fork release](https://github.com/sigmaris/u-boot/releases).
Instructions: [Flashing U Boot to SPI](https://github.com/sigmaris/u-boot/wiki/Flashing-U-Boot-to-SPI)

Flashing steps:

    cd ~/projects/hardware/pine-rockpro64/uboot
    wget https://github.com/sigmaris/u-boot/releases/download/v2021.04-rockpro64-ci/flash_spi.img.gz
    gunzip flash_spi.img.gz
    sudo dd if=./flash_spi.img of=/dev/sdd

Connect serial via USB and boot from sdcard, until `Wrote U-Boot to SPI Flash successfully.` appears.

#### Debian

Follow the [Pine64 wiki: Official Debian Installer](https://wiki.pine64.org/wiki/ROCKPro64_Software_Release#Debian)
install instructions (see also the [README.concatenateable_images](http://ftp.debian.org/debian/dists/bookworm/main/installer-arm64/current/images/netboot/SD-card-images/README.concatenateable_images)):

    cd ~/projects/hardware/pine-rockpro64/debian
    wget https://deb.debian.org/debian/dists/bookworm/main/installer-arm64/current/images/netboot/SD-card-images/firmware.rockpro64-rk3399.img.gz
    wget https://deb.debian.org/debian/dists/bookworm/main/installer-arm64/current/images/netboot/SD-card-images/partition.img.gz
    zcat firmware.rockpro64-rk3399.img.gz partition.img.gz > complete_image.img
    sudo dd if=complete_image.img of=/dev/mmcblkX bs=4M

**Make sure to no use (guided) encrypted LVM partitioning, since the installation boots,
but the USB keyboard is not recognized (as of 2023-08-14, Debian Bullseye).**

## Monitoring

Install `prometheus-node-exporter-collectors` which monitors md-raid:

    apt install prometheus-node-exporter-collectors smartmontools

Install md details cronjob:

    echo '*/5 * * * * root /usr/share/prometheus-node-exporter-collectors/md_info_detail.sh | sponge /var/lib/prometheus/node-exporter/md_info_detail.prom' > /etc/cron.d/prometheus_md_info_detail

## Issues

### ATA bus errors

Caused by a faulty SATA controller, fixed by switching to the
`JMicron Technology Corp. JMB58x AHCI SATA controller` !

Update:  From <https://forum.pine64.org/showthread.php?tid=6932&pid=56075#pid56075>:

> Like others, I switched to a Marvell-based SATA card, and the issues are gone even at 3Gbps or 6Gbps.
> I guess some ASM-based cards are crappy (jumpers related ?).
