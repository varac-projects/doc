# Raspberry Pi OS

* <https://www.raspberrypi.com/software/>

## RPi imager

* [Use RPi-imager to set hostname and more during installation](https://randomnerdtutorials.com/installing-raspbian-lite-enabling-and-connecting-with-ssh/)
* Config at `~/.config/Raspberry\ Pi/Imager.conf`
* Image cache at `/home/varac/.cache/Raspberry\ Pi/Imager`

## raspberry pi OS 64bit

* [Forum thread](https://www.raspberrypi.org/forums/viewtopic.php?f=117&t=275370)
* <https://downloads.raspberrypi.org/raspios_lite_arm64/images/>
* <https://github.com/raspberrypi/Raspberry-Pi-OS-64bit/issues>

## Enable ssh in headless mode

* <https://phoenixnap.com/kb/enable-ssh-raspberry-pi>

Mount `/boot` partition of sdcard and then:

     touch /run/media/varac/bootfs/ssh

## Dist upgrades

* [Bullseye to Bookworm](https://gnulinux.ch/pios-auf-bookworm-upgrade-ohne-neuinstallation-und-gewehr)
* [buster to bullseye](https://lernentec.com/post/how-to-upgrade-raspian-buster-to-bullseye/)
