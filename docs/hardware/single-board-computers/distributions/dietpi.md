# DietPi

* Runs fine on a 4GB Sdcard

## Upgrade to new Debian release

### To bookworm

* [Debian Bookworm has been released](https://dietpi.com/blog/?p=3128)

Scripted:

    sudo bash -c "$(curl -sSf 'https://raw.githubusercontent.com/MichaIng/DietPi/master/.meta/dietpi-bookworm-upgrade')"

Issues:

* On one system, `/boot/dietpi` was missing. Searching the web showed that
  other ppl encountered this bug as well. I didn't find an easy fix and reverted
  back to a plain Debian installation.

## Issues

### Unit systemd-logind.service is masked

[Why is systemd-logind.service masked?](https://dietpi.com/forum/t/why-is-systemd-logind-service-masked-problems-with-kodi/4783)

Solution: Edit ` /boot/dietpi.txt` and change `AUTO_UNMASK_LOGIND` to `1`
Than, after next boot:

    systemctl unmask systemd-logind.service
    systemctl enable systemd-logind.service
    systemctl start systemd-logind.service

### systemd-timesyncd is stopped


Solution: Edit ` /boot/dietpi.txt` and change `CONFIG_NTP_MODE` to `4`


##  Ansible

* Install `python3`
* [Install openssh instead of default dropbear](https://dietpi.com/docs/software/ssh/#openssh) [Install openssh instead of default dropbear](https://dietpi.com/docs/software/ssh/#openssh)
