# LVM

## Install lvm2

```sh
apt install lvm2
```

## Create lv

```sh
pvcreate /dev/sda99
vgcreate vg1 /dev/sda99
lvcreate -L 8gb -n NEWLV vg1
lvs

mkfs.ext4 -m 0 -L backupspace /dev/vg1/NEWLV
```

## Remove lv

```sh
lvremove seagate2tb_crypt_vg/oas-local-storage
```

## Sizes

Create new lv with 100% of free remaining vg disk space

```sh
lvcreate -l 100%FREE -n <lv-name> vg1
```

## Grow lv

See also `./crytpsetup.md` for growing encrypted LVM partions

Grow physical volume (eventually):

```sh
sudo pvresize /dev/sdx
```

Then:

```sh
sudo lvextend --size +40G ubuntu-vg/home
```

or extend with max available free space on vg:

```sh
sudo lvextend -l +100%FREE VolGroup00/LogVol00
```

After this:

```sh
sudo resize2fs /dev/mapper/ubuntu--vg-home
```

### grow lv to max

```sh
lvresize -l +100%FREE VolGroup00/LogVol00
```

## Shrink PV

Example: Disk was cloned to slightly smaller disk:

> WARNING: Device /dev/mapper/dapple has size of 468356272 sectors
> which is smaller than corresponding PV size of 487890944 sectors. Was device resized?

Show where segments were allocated:

```sh
pvs -v --segments /dev/mapper/dapple
```

Swap partition was at end of disk so we remove it before shrinking:

```sh
lvremove /dev/albatros-vg/swap_1
```

Now shrink to much less than actual disk size:

```sh
pvresize --setphysicalvolumesize 220g /dev/mapper/dapple
```

And then grow to max:

```sh
pvresize /dev/mapper/dapple
```

## Shrink logical volume

- Online shrinking is not supported
- [Offline shrinking using initramfs](https://serverfault.com/a/888830)

### Offline shrinking with live CD

Boot from live cd, then:

```sh
cryptsetup luksOpen /dev/sda3 sda3_crypt
lvchange -ay dapple-vg/root
```

#### Use `--resizefs` support in `lvreduce`

[Redhat training: Shrinking Logical Volumes](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/logical_volume_manager_administration/lv_reduce)

```sh
lvreduce --resizefs -L 64M vg00/lvol1
```

#### Manually check, shrink and grow filesystem

[How to Shrink an LVM Volume Safely on Linux](https://www.casesup.com/category/knowledgebase/howtos/how-to-shrink-an-lvm-volume-safely-on-linux)

```sh
e2fsck -f /dev/dapple-vg/root

# resize to ~90% of the desired size for safety
resize2fs /dev/dapple-vg/root 180G

lvreduce -L 200G /dev/dapple-vg/root

# Finally resize to the whole lv size
resize2fs /dev/dapple-vg/root
```

## Remove volume group mapping after removing physical disk

<https://serverfault.com/questions/421776/removing-vg-and-lv-after-physical-drive-has-been-removed>

```sh
umount /mnt
dmsetup status
dmsetup remove vg1-media
vgs
```

## Snapshots

### Create Snapshot

> Size: "The snapshot does not need the same amount of storage the origin has.
> In a typical scenario, 15-20% might be enough."

```sh
lvcreate -L4G -s -n dbbackup /dev/ops/databases
# create read-only snapshot (-pr)
VM=NAME; lvcreate -pr -L2400M -s -n $VM-root-dist-upgrade /dev/viper_vg0/$VM-root
```

### Revert to snapshot

```sh
`--merge`
```

> Merges a snapshot into its origin volume. To check if your kernel supports
> this feature, look for snapshot-merge in the output of dmsetup targets. If
> both the origin and snapshot volume are not open the merge will start
> immediately. Otherwise, the merge will start the first time either the origin
> or snapshot are activated and both are closed. Merging a snapshot into an
> origin that cannot be closed, for example a root filesystem, is deferred until
> the next time the origin volume is activated. When merging starts, the
> resulting logical volume will have the origin's name, minor number and UUID.
> While the merge is in progress, reads or writes to the origin appear as they
> were directed to the snapshot being merged. When the merge finishes, the
> merged snapshot is removed. Multiple snapshots may be specified on the
> commandline or a @tag may be used to specify multiple snapshots be merged to
> their respective origin. lvconvert --merge viper_vg0/mole-root-dist-upgrade
> Can't merge over open origin volume Merging of snapshot mole-root-dist-upgrade
> will start next activation.
> <http://www.thegoldfish.org/2011/09/reverting-to-a-previous-snapshot-using-linux-lvm/>

```sh
dmsetup remove viper_vg0-mole--rootp1
dmsetup remove viper_vg0-mole--rootp2
lvchange -an viper_vg0/mole-root
lvchange -ay viper_vg0/mole-root
virsh start mole
```

### Delete snapshot

```sh
$ lvremove viper_vg0/mole-root
Logical volume "mole-root-dist-upgrade" successfully removed
Do you really want to remove active logical volume mole-root? [y/n]: n
Logical volume mole-root not removed
```
