# SSDs / NVMEs

- [Solid state drive](https://wiki.archlinux.org/title/Solid_state_drive)

## NVME disks

[NVME drive](https://wiki.archlinux.org/index.php/Solid_state_drive/Memory_cell_clearing#NVMe_drive)

```sh
nvme list
nvme smart-log /dev/nvme0n1
nvme wdc help
```

### Format nvme disk

- [Using NVMe Command Line Tools to Check NVMe Flash Health](https://www.percona.com/blog/2017/02/09/using-nvme-command-line-tools-to-check-nvme-flash-health/)

### Sanitize / Secure-erase

[Arch wiki: Solid_state_drive/Memory_cell_clearing: NVMe_drive](https://wiki.archlinux.org/title/Solid_state_drive/Memory_cell_clearing#NVMe_drive)

Show available commands for the drive:

```sh
nvme id-ctrl /dev/nvme0 -H | grep -E 'Format |Crypto Erase|Sanitize'
```

Sanitize:

nvme sanitize-log /dev/nvme0

When the `Sanitize` command is not supported proceed with format:

```sh
nvme format /dev/nvme0nX -s 1
```

### NVME smart

Show `percent_used` (aka wear levelling):

```sh
sudo nvme smart-log /dev/nvme0n1 --output-format=json | jq .percent_used
```

## SSDs

- [Riseup disk testing procedure](https://we.riseup.net/riseup+tech/disk-testing-procedure)

### Discard / fstrim

- [Wait... is that how you are supposed to configure your SSD card?](https://current.workingdirectory.net/posts/2016/ssd-discard/)
- [SSD optimization](https://wiki.debian.org/SSDOptimization)
- [How to properly activate TRIM for your SSD on Linux: fstrim, lvm and dm-crypt](http://blog.neutrino.es/2013/howto-properly-activate-trim-for-your-ssd-on-linux-fstrim-lvm-and-dmcrypt/)

tldr:

- Discard is already added during installation in `/etc/crypttab`
- The system timer `fstrim.timer` is also enabled during installation
- Don't enable discard in `/etc/fstab` !
- Add `discard` option to luks devices in `/etc/crypttab`
- LVM has `issue_discards` already set by default
- A systemd timer + service (`fstrim.timer`) is enabled by default already
  `systemctl status fstrim.timer`

Show trimming results: `systemctl status fstrim.timer`

### Secure Erase

- [ata wiki: Secure erase](https://ata.wiki.kernel.org/index.php/ATA_Secure_Erase)

Make sure disk is not frozen:

```sh
sudo hdparm -I /dev/sdX | grep frozen
```

Set dummy user pw:

```sh
sudo hdparm --user-master u --security-set-pass 1234 /dev/sdX
```

Ensure security (pw) is enabled:

```sh
sudo hdparm -I /dev/sdX | grep -B3 enabled
```

Erase:

```sh
time sudo hdparm --user-master u --security-erase 1234 /dev/sdX
```

Takes up to ~12s for 500gb SSD.

- [Wiping an SSD with Parted Magic seemed too quick ?](https://security.stackexchange.com/a/183287)

Ensure security (pw) is _not_ enabled anymore:

```sh
sudo hdparm -I /dev/sdX | grep -B3 enabled
```
