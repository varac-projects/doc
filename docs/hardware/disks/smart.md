# smart

<https://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl>

    apt install smartmontools

General drive information:

    smartctl -i /dev/sda

Short all informations, including test results:

    smartctl -a /dev/sda

## Health assessment

<https://www.thomas-krenn.com/en/wiki/Analyzing_a_Faulty_Hard_Disk_using_Smartctl>

Show self-health-indicator:

    smartctl -H /dev/sda

Show most important details:

    smartctl -a /dev/sda | grep -iE '(error|uncorrect|pending|recovered|fail)' | grep -v '0$'

### Smart attributes explained

See also `~/projects/monitoring/prometheus/varac-alerts/custom-alerts/custom-alerts-general.yaml`

**Critical**:

* [Raw_Read_Error_Rate](https://kb.acronis.com/content/9101)
* [Current_Pending_Sector](https://kb.acronis.com/content/9133)

**Warning**:

* [Seek_Error_Rate](https://kb.acronis.com/content/9107)
* [UDMA_CRC_Error_Count](https://kb.acronis.com/content/9135)

**Informational**:

* [G-sense error rate](https://kb.acronis.com/content/9126)

## Tests

<https://www.thomas-krenn.com/de/wiki/SMART_Tests_mit_smartctl>

### Show duration of tests

    smartctl -c /dev/sdc

## Test in background mode

    smartctl -t <short|long|conveyance> /dev/sdc

### Test in foreground mode

Only useful if the drive is NOT used by any other process!
Also, it blocks access to smart log, so if possible, use the background
mode!

    smartctl -t <short|long|conveyance> -C /dev/sdc

### Show test results

    smartctl -l selftest /dev/sdb

### Issues with USB disks entering sleep mode

Tip: Poll test results in short intervals to prevent disk from sleeping

    watch smartctl -l selftest /dev/sdb

## NVME disks

* <https://www.smartmontools.org/wiki/NVMe_Support>
* <https://www.thomas-krenn.com/de/wiki/SMART_Werte_von_NVMe_SSDs_auslesen>

    apt install nvme-cli

    nvme list
    nvme --smart-log /dev/nvme0n1
    smartctl -a /dev/nvme0

## USB drives

<https://www.smartmontools.org/wiki/USB>

    smartctl -d sat -i /dev/sda

## Prometheus

* [smartctl_exporter](https://github.com/prometheus-community/smartctl_exporter)
  * [Unfortunatly no aarch64 release builds](https://github.com/prometheus-community/smartctl_exporter/issues/119)
