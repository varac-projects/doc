# Cryptsetup / LUKS

<https://wiki.ubuntuusers.de/LUKS/>

## Usage

### Mount existing device

Find luks partitions:

```sh
lsblk | highlight crypt

sudo cryptsetup luksOpen /dev/mmcblk0p1 backupkeys
sudo e2fsck -f -p /dev/mapper/backupkeys
sudo mount /dev/mapper/backupkeys /mnt
```

### Status of decrypted devices

List all open cryptsetup devices:

```sh
sudo dmsetup ls --target crypt
```

Get info about one particular device:

```sh
cryptsetup status backupkeys
```

### Unmount usbdisk with crypto-lvm

```sh
sudo umount /mnt/dest
sudo cryptsetup close backupkeys
```

## Setup

```sh
apt-get install cryptsetup lvm2 parted
export DISK_DEV='/dev/sdX'
export PART_DEV='/dev/sdX1'
export LUKS_NAME='luks_backup'
export VG_NAME='backup'
export LV_NAME='backup_personal'
```

Partition disk:

```sh
parted $DISK_DEV mklabel gpt --script
parted $DISK_DEV mkpart primary 0% 100% --script
```

### Setup encr. disk without lvm using passphrase

```sh
sudo cryptsetup luksFormat $PART_DEV
sudo cryptsetup luksOpen $PART_DEV $LUKS_NAME
```

### Setup luks with lvm and password

```sh
cryptsetup luksFormat $PART_DEV
cryptsetup open --type luks $PART_DEV $LUKS_NAME
cryptsetup status $LUKS_NAME

pvcreate /dev/mapper/$LUKS_NAME
vgcreate $VG_NAME /dev/mapper/$LUKS_NAME
lvcreate -L 400gb -n $LV_NAME $VG_NAME

mkfs.ext4 -m 0 -L $LV_NAME /dev/${VG_NAME}/${LV_NAME}
mount /dev/${VG_NAME}/${LV_NAME} /mnt
```

### Setup luks with lvm and Keyfile

<https://wiki.ubuntuusers.de/Archiv/LUKS/Schl%C3%BCsseldatei/>

```sh
export LUKSNAME='seagate-4tb'
export KEYNAME="${LUKSNAME}-luks.key"

mkdir /etc/luks
dd bs=1 count=4096 if=/dev/random of=/etc/luks/$KEYNAME

chmod 600 /etc/luks/*

cryptsetup luksFormat $PART_DEV /etc/luks/$KEYNAME
cryptsetup open --type luks $PART_DEV $LUKSNAME --key-file /etc/luks/$KEYNAME
cryptsetup status $LUKSNAME
```

Optionally, add a passphrase:

```sh
cryptsetup luksAddKey --key-file /etc/luks/$KEYNAME $PART_DEV
```

setup lvm and format lv

```sh
pvcreate /dev/mapper/$LUKSNAME
vgcreate ${LUKSNAME}-crypt /dev/mapper/$LUKSNAME
lvcreate -L 400gb -n media ${LUKSNAME}-crypt
```

or

```sh
lvcreate -l 100%FREE -n media seagate2tb_crypt_vg

mkfs.ext4 -m 0 -L usb1tb_media /dev/mapper/vg1-media
mount /dev/mapper/vg1-media /media/usb_crypt
...
umount /media/usb_crypt
cryptsetup luksClose crypt_sdX
```

### Mount at boot

<https://unix.stackexchange.com/questions/392284/using-a-single-passphrase-to-unlock-multiple-encrypted-disks-at-boot#392286>

```sh
echo 'sda_crypt  /dev/disk/by-uuid/UUID /etc/luks/usb1tb.key  luks' >> /etc/crypttab
update-initramfs -u -k all
```

### Change password

<https://www.maketecheasier.com/change-luks-encryption-passphrase/>
Enter password of any key slot, when some key slot can get opened this slot
will get its password changed.

```sh
cryptsetup luksChangeKey /dev/nvme0n1p3
```

### Add key

<http://www.howtoforge.com/automatically-unlock-luks-encrypted-drives-with-a-keyfile>

Show existing keyslots:

```sh
cryptsetup luksDump /dev/sda5
```

Add new password keyslot:

```sh
cryptsetup luksAddKey /dev/sda5
```

Provide keyfile when adding new password:

```sh
cryptsetup luksAddKey --key-file /etc/luks/usb1tb.key /dev/sda5
```

Add new keyfile keyslot:

```sh
cryptsetup luksAddKey /dev/sda5 backup.key
```

### Remove keyslot

Enter password of any _remaining_ key slot to make sure there's one slot left
with a known password:

```sh
cryptsetup luksKillSlot /dev/sda5 <slot-nr>
```

### Key derivation

<http://wiki.ubuntuusers.de/LUKS/Schl%C3%BCsselableitung>

```sh
export SRC_UUID='80312beb-9526-40df-8a6d-9a6824820a7a'  # sda5 laptop
export SRC_NAME='sda5_crypt'
export DST_UUID='bc2e555d-3f17-4264-850b-855a8fee0b93'  # /dev/sdb2, ultrabay
export DST_NAME='ultrabay_crypt'

/lib/cryptsetup/scripts/decrypt_derived $SRC_NAME > /etc/luks/decrypt_derived_${SRC_UUID}.key
chmod 600 /etc/luks/decrypt_derived_${SRC_UUID}.key
cryptsetup luksDump /dev/disk/by-uuid/$DST_UUID  # Keys before
cryptsetup luksAddKey /dev/disk/by-uuid/$DST_UUID /etc/luks/decrypt_derived_${SRC_UUID}.key
cryptsetup luksDump /dev/disk/by-uuid/$DST_UUID   # Keys afterwards
/lib/cryptsetup/scripts/decrypt_derived $SRC_NAME | \
  cryptsetup luksOpen /dev/disk/by-uuid/$DST_UUID $DST_NAME
```

#### in crypttab

```sh
echo "usbdisk_cryptbackup UUID=196fb147-f18e-4543-8860-b8b68466f0e2 \
  sda5_crypt luks,keyscript=/lib/cryptsetup/scripts/decrypt_derived" >> /etc/crypttab
```

## Cryptoroot mit Keyfile auf USB

- `/usr/share/doc/cryptsetup/README.initramfs.gz`
- Before: Do a backup of the initramfs !!!!!

## Find out the UUID of the usb device

```sh
blkid
...
/dev/sdb1: LABEL="KEYCHAIN" UUID="UUID" SEC_TYPE="ext2" TYPE="ext3"
```

## Generate Key on usb device

- put it in /root.key on the usbdrive

## Add key to luks container

```sh
cryptsetup luksDump /dev/sda3
cryptsetup luksAddKey /dev/sda3 /mnt/root.key
cryptsetup luksDump /dev/sda3

in /etc/crypttab:
# mapping_name  dev_to_be_decrypted                  location of keyfile
sda2_crypt UUID=acfe282b-c1ac-407f-9fab-6c3d19089b17 \
  /dev/disk/by-uuid/cb31f4fe-e0f3-4e98-a56d-3fbcfbd059f7:/root.key luks,keyscript=/lib/cryptsetup/scripts/passdev
update-initramfs -u
```

## Unlock luks key via ssh

### Setup dropbear

Beware: Dropbear can't handle `ed25519` keys at the moment (Debian 10, 2020-08).

```sh
apt install dropbear-initramfs
cp /home/varac/.ssh/authorized_keys /etc/dropbear/initramfs/
update-initramfs -u -k all
```

### Unlock

Use the `ssh_luks_unlock_HOSTNAME` alias, aliased i.e. to:

```sh
gopass show --password hardware/server/dapple/luks-system-pw \
  | ssh -T dapple-initramfs cryptroot-unlock
```

Manual:

In busybox, to unlock root partition, and maybe others like swap, run:

```sh
cryptroot-unlock
```

### Unlocking via WLAN

- [wifi-on-debian-initramfs](https://github.com/fangfufu/wifi-on-debian-initramfs)
  A bunch of files for enabling Wifi in Debian initramfs
- [Enable Wireless networks in Debian Initramfs](https://www.marcfargas.com/2017/12/enable-wireless-networks-in-debian-initramfs/)

### cryptreboot

> Convenient reboot for Linux systems with encrypted root partition.
>
> > Just type cryptreboot instead of reboot.
>
> It asks for a passphrase and reboots the system afterward,
> automatically unlocking the drive on startup using in-memory initramfs
> patching and kexec.
> Without explicit consent, no secrets are stored on disk, even temporarily.

- [Cryptreboot blog post](https://blog.pawelpokrywka.com/p/cryptreboot)
- [GitHub](https://github.com/phantom-node/cryptreboot)
- [Debian Request for package](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1085079)

### Scripting unlocking

- <https://cryptsetup-team.pages.debian.net/cryptsetup/README.Debian.html#remotely-unlock-encrypted-rootfs>
- [unlock-cryptroot](https://github.com/rhansen/unlock-cryptroot) is a wrapper
  script but you still need to enter the password manually
- [Using cryptsetup on the server](https://superuser.com/a/1558301)

tldr:

```sh
gopass show --password hardware/nucy/luks-pw | ssh -T nucy-initramfs cryptroot-unlock
```

## Auto-unlock with Mandos

[Mandos](https://www.recompile.se/mandos)

## cryptsetup issues

### out of memory

[cryptsetup luksOpen silently runs out of memory when run on a machine with 708M of RAM](https://gitlab.com/cryptsetup/cryptsetup/-/issues/488)

Also happened on a raspberry pi 3 Model B Rev 1.2 with 1gb RAM.

Run cryptsetup in verbose/debug mode:

```sh
sudo cryptsetup --debug open --type luks /dev/sda1 backuplvm
…
Command failed with code -3 (out of memory).
```

Solution: Convert luks key to "lower memory requirements (or use PBKDF2)"

Before:

```sh
❯ sudo cryptsetup luksDump /dev/sdb1
…
Keyslots:
  0: luks2
  Key:        512 bits
  Priority:   normal
  Cipher:     aes-xts-plain64
  Cipher key: 512 bits
  PBKDF:      argon2i
  Time cost:  6
  Memory:     1048576
  Threads:    4
…
```

Convery key to `pbkdf2`:

```sh
❯ sudo cryptsetup luksConvertKey /dev/sdb1
Enter passphrase for keyslot to be converted:
```

After:

```sh
❯ sudo cryptsetup luksDump /dev/sdb1
Keyslots:
  0: luks2
  Key:        512 bits
  Priority:   normal
  Cipher:     aes-xts-plain64
  Cipher key: 512 bits
  PBKDF:      pbkdf2
  Hash:       sha256
  Iterations: 1466538
```

## How to change the name an encrypted full-system partition is mapped to

<https://medium.com/opsops/how-to-rename-encrypted-root-volume-c6333cb72094>

1. `dmsetup rename /dev/mapper/sda3_crypt system_crypt`
1. Edit `/etc/crypttab`, replace name (and uuid, if needed). UUID can be found
   with blkid command.
1. `update-initramfs -k all -u`
1. `systemctl reboot`

## Unmount luks device with LVM volumes on it

<https://sleeplessbeastie.eu/2015/11/16/how-to-mount-encrypted-lvm-logical-volume/>

Deactivate all logical volumes on volume group:

```sh
lvs -S "lv_active=active"
lvchange -an seagate-4tb-crypt
```

Close LUKS device:

```sh
cryptsetup luksClose seagate_4tb_luks
```
