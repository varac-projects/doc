# Swap

* [What's the right amount of swap space for a modern Linux system?](https://opensource.com/article/19/2/swap-space-poll)

Calculate swap space needed, i.e. 1.5x24 GB RAM = 36 GB

    LANGUAGE=en ram_mb=$(free -m | grep '^Mem:' | sed 's/[^0-9]*//' | cut -d' ' -f1)

## Using a swapfile

    swapoff -a
    lvremove /dev/ubuntu-vg/swap_1
    # bs in GB (bytes * 1024³)
    dd conv=notrunc if=/dev/zero of=/var/lib/swapfile bs=1048576 count=$ram_mb
    mkswap /var/lib/swapfile
    chmod 600 /var/lib/swapfile

Then edit `/etc/fstab` and add the swapfile, afterwards:

    swapon -a
    swapon -s
