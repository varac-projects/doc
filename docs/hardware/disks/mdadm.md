# Raid with mdadm

- <https://raid.wiki.kernel.org/index.php/A_guide_to_mdadm>
- <https://wiki.ubuntuusers.de/Software-RAID/>
- <https://www.thomas-krenn.com/de/wiki/Software_RAID_mit_MDADM_verwalten>

## Install

By default, mdadm recommends a mail agent (defaulting to exim),
so if you don't need one don't install the recommends:

```sh
sudo apt install --no-install-recommends mdadm
```

## Show info

```sh
cat /proc/mdstat
mdadm --detail /dev/md0
```

## Create raid

<https://www.digitalocean.com/community/tutorials/how-to-create-raid-arrays-with-mdadm-on-ubuntu-16-04#creating-a-raid-1-array>

- [Use partitions, not whole disks!](https://unix.stackexchange.com/questions/320103/whats-the-difference-between-creating-mdadm-array-using-partitions-or-the-whole)

- Make sure both disks are excatly the same size (compare sectors with
  `fdisk -l -u sectors /dev/sdX`)

```sh
apt install mdadm

mdadm --create --verbose /dev/md0 --level=1 --raid-devices=2 /dev/sda1 /dev/sdb1
```

Show raid state:

```sh
cat /proc/mdstat
```

Sync will happen in background, `/dev/md0` is immediatly usable!

### Create degraded raid

Use case: sync old backup data to one disk before adding another disk to
complete the array.

<https://documentation.suse.com/sles/15-SP1/html/SLES-all/cha-raid-degraded.html>

Create a degraded array

```sh
mdadm --create /dev/md0 -l 1 -n 2 /dev/sdX1 missing
```

Then later add second disk:

```sh
mdadm /dev/md0 -a /dev/sdY1
```

## Setup raid1 with encrypted LVM

```sh
export RAID_DEV='/dev/md0'
export LUKS_NAME='cryptraid'
export VG_NAME=$LUKS_NAME
export LV_NAME='backup'
```

Partition disk:

```sh
parted /dev/sdX mklabel gpt --script
parted /dev/sdX mkpart primary 0% 100% --script

parted /dev/sdY mklabel gpt --script
parted /dev/sdY mkpart primary 0% 100% --script
```

Create raid1:

```sh
mdadm --create $RAID_DEV -l 1 -n 2 /dev/sdX1 /dev/sdY1
```

Luks-format it:

```sh
cryptsetup luksFormat $RAID_DEV
cryptsetup open --type luks $RAID_DEV $LUKS_NAME
cryptsetup status $LUKS_NAME
```

Create LVM:

```sh
pvcreate /dev/mapper/$LUKS_NAME
vgcreate $VG_NAME /dev/mapper/$LUKS_NAME
lvcreate -L 400gb -n $LV_NAME $VG_NAME

mkfs.ext4 -m 0 -L $LV_NAME /dev/${VG_NAME}/${LV_NAME}
mount /dev/${VG_NAME}/${LV_NAME} /mnt
```

## Delete raid1

```sh
mdadm --detail /dev/md3
mdadm --stop /dev/md3
mdadm --zero-superblock /dev/sda4
mdadm --zero-superblock /dev/sdb4
```

## Shrink root software raid

<http://www.webdesignblog.asia/operating-systems/linux-os/resizeshrink-raid1-filesystemvolumepartition-and-setup-a-lvm-on-the-free-disk-space-created/#sthash.gdUNQpuw.ohySozoz.dpbs>

```sh
root-part: md2 (sda3 + sdb3)

resize2fs -f /dev/md2 30G
```

13% mehr als 30G -> 34G
34*1024*1024

```sh
mdadm --grow /dev/md2 --size=35651584
```

15% mehr als 30G -> 35G

## Start inactive array

`md0 : inactive sda1[0](S) sdb1[2](S)`

```sh
mdadm --run /dev/md0
```

`md0: active (auto-read-only) raid1 sda1[0] sdb1[2](S)`

## Re-assemble existing raid

```sh
sudo mdadm --assemble --scan
```

## Rebuild

<https://www.thomas-krenn.com/de/wiki/Software_RAID_mit_MDADM_verwalten#Rebuild>

`md0 : active (auto-read-only) raid1 sdb1[0](S) sda1[2]`

In this case, sdb1 is unused, spare device. Remove it:

```sh
mdadm --manage /dev/md0 -r /dev/sdX1
```

Partition disk:

```sh
parted /dev/sdX mklabel gpt --script
parted /dev/sdX mkpart primary 0% 100% --script
```

Add disk to array:

```sh
mdadm --manage /dev/md0 -a /dev/sdX1
```
