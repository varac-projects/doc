# Disk images

## Mount a disk image with multiple partitions

* [StackExchange](https://superuser.com/a/803776)

With partx (already pre-installed by `util-linux` package):

    partx -a -v /home/varac/varac-pinenas.old.img
    ls -la /dev/loop0*
    mount /dev/loop0p1 /mnt
    …
    umount /mnt
    partx -d -v /dev/loop0

With `kpartx`:

    apt install kpartx
    …
