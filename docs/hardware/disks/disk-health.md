# Disk health

- Tools like `stress-ng` or `stressant` can also test disks, see [../hardware-testing.md](../hardware-testing.md)
- See also [./smart.md](./smart.md)

## Badblocks

- [Fix online badblocks reported by smart](https://www.smartmontools.org/wiki/BadBlockHowto)

### Read-Only Mode

```sh
badblocks -sv /dev/sdX # readonly
badblocks -svn -p 3 /dev/sdX # Safe read/write mode
fsck.ext3 -c /dev/sdX9  # readonly
fsck.ext3 -cc /dev/sdX9  # Safe read/write mode
```

### RW-Mode

Warning!

```sh
badblocks -t random -swv /dev/sdX ; mail -s "Badblocks finished with exit code $?" tostado
badblocks -b 4096 -p 3 -s -v -w /dev/hdX
badblocks -swv /dev/sdX
```

### Speed up badblocks

<https://superuser.com/a/798883>

```sh
time badblocks -svw -b 4096 -c 65536 -t 0 /dev/sdX
```

Takes ~10h for a 1 TB SATA drive.
