# Grub

## Add kernel parameters

- [Permanently Add a Kernel Boot Parameter](https://wiki.ubuntu.com/Kernel/KernelBootParameters#Permanently_Add_a_Kernel_Boot_Parameter)

Add parameter to `GRUB_CMDLINE_LINUX_DEFAULT` in `/etc/default/grub`, then:

```sh
sudo update-grub
```

## Grub rescue prompt

<https://phoenixnap.com/kb/grub-rescue>

## Security / Set grub password

- [Grub security](http://www.gnu.org/software/grub/manual/grub/grub.html#Security)

From [RedHat docs: Protecting GRUB with a password](https://access.redhat.com/documentation/de-de/red_hat_enterprise_linux/8/html/managing_monitoring_and_updating_the_kernel/assembly_protecting-grub-with-a-password_managing-monitoring-and-updating-the-kernel):

> You can protect GRUB with a password in two ways:
>
> Password is required for modifying menu entries but not for booting existing menu entries.
> Password is required for modifying menu entries as well as for booting existing menu entries.

### Setting password protection only for modifying menu entries

[Arch Wiki: Password protection of GRUB menu](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Password_protection_of_GRUB_menu)

Create hashed password:

```console
$ grub-mkpasswd-pbkdf2
Enter password:
Reenter password:
PBKDF2 hash of your password is grub.pbkdf2.sha512.10000.9A3AD8A...
```

Then add the following lines *at the end* of `/etc/grub.d/40_custom`:

```sh
set superusers="varac"
password_pbkdf2 varac grub.pbkdf2.sha512.10000.9A3AD8A...
```

This step alone will not only prevent from modifying menu entries,
but also prevents from booting any menu entry without authentication.
To allow users to boot the default menu entries (but disallow editing the
entries) add `/etc/grub.d/09_allow_unauthenticated_boot` with the following
content:

```sh
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.

# Allow booting any menu entry without authentication
# See https://wiki.archlinux.org/title/Talk:GRUB/Tips_and_tricks
menuentry_id_option="--unrestricted $menuentry_id_option"
```

Make this file executable:

```sh
chmod +x /etc/grub.d/09_allow_unauthenticated_boot
```

Unfortunatly, there is no better way of allowing unathenticated boot, the regarding
[grub upstream ticket](https://savannah.gnu.org/bugs/?func=detailitem&item_id=41801)
is still open.

Finally:

```sh
update-grub
```

## Install

```sh
grub-install --no-floppy --root-directory=/mnt/xyz /dev/sdX

disable plymouth/grafhical login: ro nosplash noplymouth INIT_VERBOSE=yes \
  init=/sbin/init -v
```

## USB-Boot mit Grub

- Usb-Disk rein, /dev/sdb1 on /media/usb/boot_vf

- Achtung: Keine Dateien löschen wg. Defragmentierung, nur nach old verschieben!

- Booten:

  qemu-system-x86_64 -enable-kvm -m 256 -usb /dev/sdb

- Ubuntu Installation schlägt fehl: <https://bugs.launchpad.net/ubuntu/+source/cdrom-detect/+bug/582427>

## Grub4DOS

- /media/usb_boot_vf/menu.lst
- Iso-Image nach /media/usb_boot_vf/boot/iso/grub4dos

## Grub2

- /media/usb_boot_vf/boot/grub
- Iso Image nach /media/usb_boot_vf/boot/iso

## Issues

- Can't persist `CLASS` entries in `/etc/grub.d/10_linux` - i.e. the
  `--unrestricted` entry on dapple. Each time the grub deb package gets upgraded
  the entry needs to get re-applied, followed by a `grub-update`
- Official [Grub issues](https://savannah.gnu.org/bugs/?group=grub)
