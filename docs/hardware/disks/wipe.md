# Wiping disks

## shred

Quick shed (seems to be much faster than a quick `wipe`):

```sh
time shred -v -n1 /dev/sda
```

~1,5h for a 230 GB disk attached over USB.

## Wipe

Gives an ETA

Quick wipe:

```sh
time wipe -q -Q 1 /dev/sda
```

ETA 2d for 230 GB disk attached over USB.
