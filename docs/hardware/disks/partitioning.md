# Disk partitioning

## parted

[Arch wiki: parted](https://wiki.archlinux.org/title/Parted)
[Parted Manual](https://www.gnu.org/software/parted/manual/parted.html)

    sudo parted --script /dev/sdX mklabel gpt

or for sdcards:

    sudo parted --script /dev/sdX mklabel msdos


    sudo parted --script /dev/sdX mkpart primary 0% 100%
    sudo mkfs.ext4 -m0 -L usbstick /dev/sdX1

or for vfat:

    sudo mkfs.vfat -n fotos /dev/mmcblk0p1

### Examples

Create a new single patition on i.e. a usb drive and format it with FAT32:

    sudo parted -s /dev/sdX -- mklabel msdos mkpart primary fat32 2048s -1s
    sudo mkfs.vfat -F 32 /dev/sdX1
