# MMC / sd-cards

## Check physical health

* [Linux check the physical health of a USB stick](https://www.cyberciti.biz/faq/linux-check-the-physical-health-of-a-usb-stick-flash-drive/)

Begin with badblocks:


    sudo badblocks -w -t 0 -s -o error.log /dev/sdX

Use [f3 tools](https://fight-flash-fraud.readthedocs.io/en/latest/):

    pamac install f3
    sudo f3probe --destructive --time-ops /dev/sdX

Format, write and read from filesystem:

    sudo mkfs.vfat /dev/sdX
    mount /dev/sdX /mnt
    f3write /mnt/
    f3read /mnt/

## mmc-utils

Doesn't compile on Manjaro-sway as of 2023-04:

    pamac install mmc-utils-git
