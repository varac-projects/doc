# Live CDs

## Ventoy

### Installation

#### Install Ventoy binary on Laptop

[Install](https://www.ventoy.net/en/doc_start.html)

Arch:

```sh
sudo pacman -S ventoy
```

From Github releases:

Can't install from git repo, need to download [latest release](https://github.com/ventoy/Ventoy/releases)

```sh
cd ~/projects/live-cds/ventoy-releases
wget https://github.com/ventoy/Ventoy/releases/download/v1.0.82/ventoy-1.0.82-linux.tar.gz
tar -xzf ventoy-1.0.82-linux.tar.gz
```

#### Install Ventoy on USB stick

```sh
sudo /opt/ventoy/Ventoy2Disk.sh -i /dev/sdX
```

#### Upgrade

From [Update Ventoy](https://www.ventoy.net/en/doc_start.html):

> It should be noted that the upgrade operation is safe, all the files in
> the first partition will be unchanged.
> Upgrade operation is in the same way with installation.
> Ventoy2Disk.sh will prompt you for update
> if the USB drive already installed with Ventoy.

```sh
sudo /opt/ventoy/Ventoy2Disk.sh -u  /dev/sda
```

#### Secure boot

[About Secure Boot in UEFI mode](https://www.ventoy.net/en/doc_secure.html):

> There are two methods: Enroll Key and Enroll Hash, use whichever one

Enroll Key:

- Error: Verification failed: `OK`
- Shim UEFI key management: `Press any key to perform MOK management`
- Perform MOK management: `Enroll key from disk`
  - `VTOYEFI`
  - `ENROLL_THIS_KEY_IN_MOKMANAGER.cer`
- Enroll MOK: `Continue`
- Enroll the key(s)?: `Yes`
- Reboot

#### Usage

Mount first partition (`/dev/sda1`) and simply copy iso images!

Test Ventoy boot: `sudo qemu-system-x86_64 -enable-kvm -m 256 -usb /dev/sda`

#### Plugins

- [Ventoy Boot Conf Replace Plugin](https://www.ventoy.net/en/plugin_bootconf_replace.html)
  Useful for pre-configuring kernel paramaters (i.e. `intel_idle.max_cstate=4`
  for the `lemur pro 9`)

### Yumi

- [YUMI – Multiboot USB Creator](https://www.pendrivelinux.com/yumi-multiboot-usb-creator/)
- Based on Ventoy

## Isos

### grml

- [Website](https://grml.org)
- [github development active](https://github.com/grml/)
- Based on Debian
- Con: Doesn't auto-connect to wired network using docking station
- GUI
  - Xorg

#### Custom settings when imaging usb disk

- [grml2usb](https://grml.org/grml2usb/)

Enable ssh:

Partition the usb-disk to have 1 primary partition (i.e. `sda1`)
Then:

```sh
sudo apt install grml2usb
PW="$(gopass show --password live-cds/ssh-password)"
sudo grml2usb --fat16 --bootoptions="ssh=$PW" \
  ~/Downloads/grml64-full_2020.06.iso /dev/sdX1
```

### Debian live

- [Debian live](https://www.debian.org/devel/debian-live/)
- [Live CD](https://www.debian.org/CD/live/)
- [Debian Live Manual](https://live-team.pages.debian.net/live-manual/html/live-manual/index.en.html)

#### live-build

[live-build examples](https://live-team.pages.debian.net/live-manual/html/live-manual/examples.en.html)

```sh
sudo apt install live-build
cd ~/projects/debian/live/ssh-live-cd

lb config

lb build 2>&1 | tee build.log
```

#### test-boot live image

```sh
kvm -cdrom live-image-i386.hybrid.iso
```

### SystemRescueCd

- [Website](https://www.system-rescue.org/)
- [Gitlab](https://gitlab.com/systemrescue/systemrescue-sources)
- Recent releases
- Based on Arch linux
- CLI
  - No quick way to configure bluetooth keyboards
- GUI
  - XFCE + Xorg

#### SSH

Allow ssh access:

```sh
iptables -I INPUT 1 -p tcp --dport 22 -j ACCEPT
```

Set root pw:

```sh
passwd
```

#### Autorun

- [Run your own scripts with autorun](https://www.system-rescue.org/manual/Run_your_own_scripts_with_autorun/)
- can't be added later because the USBstick is made of an `iso9660` immutable
  fs.

### Finnix

- [Website](https://www.finnix.org/)
- Last release 6 month ago
- based on debian-live

SSH access:

```sh
 passwd
 systemctl start ssh
```

SSH access:s:

```sh
 passwd
 systemctl start ssh
```

SSH access:s:

```sh
 passwd
 systemctl start ssh
```

SSH access:s:

```sh
 passwd
 systemctl start ssh
```

### Rescatux

- [Rescatux website](https://www.supergrubdisk.org/rescatux/)
- Recent release
- [Rescatux 0.74 released](https://www.supergrubdisk.org/2021/11/14/rescatux-0-74-released/)

### Outdated

- [Ultimate Boot CD](https://www.ultimatebootcd.com/news.html)
  Last release 2020
