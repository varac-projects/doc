# RaspiAudio

- [Raspiaudio](https://raspiaudio.com)
- [Discourse forum](https://forum.raspiaudio.com/)

## Muse Radio

- [Product page](https://raspiaudio.com/product/muse-radio/)
- [Docs](https://forum.raspiaudio.com/t/muse-radio-installation-documentation/1091)
- [MUSE RADIO installation documentation](https://forum.raspiaudio.com/t/muse-radio-installation-documentation/1091)
- Available firmware/apps:
  - Internet-Radio Demo app: [MuseRadio-DEMO](https://github.com/RASPIAUDIO/MuseRadio-DEMO)
  - Squeezelite
  - Homeassistant Voice Satellite (see below)

### Squeezelite on Muse Radio

- [Muse Radio Squeezelite](https://forum.raspiaudio.com/t/muse-radio-squeezelite/1099)

## Muse Luxe Speaker

- [Product page](https://raspiaudio.com/produit/esp-muse-luxe)
- [ESPMuse Lux FAQ](https://forum.raspiaudio.com/t/espmuse-luxe-faq/74)
- EspMuse is based on [Muse Proto](https://raspiaudio.com/produit/muse-proto),
  which is based on a [Espressif Esp32-WRover](https://www.espressif.com/en/products/modules)
- [Reset ESPMuse to AP mode](https://forum.raspiaudio.com/t/espmuse-with-squeezelite-esp32-doesnt-start-after-battery-was-drained/515/3?u=potl)
- [FAQ](https://forum.raspiaudio.com/t/espmuse-luxe-faq/74)
- Available firmware/apps:
  - Bluetooth
  - Internet radio
  - Squeezelite (see bellow)
  - Homeassistant Voice Satellite (see below)

### Squeezelite on MuseLuxe

- [Tutorial: MUSE LUXE speaker with Squeezlite (Logitech Media server)](https://forum.raspiaudio.com/t/muse-luxe-speaker-with-squeezlite-logitech-media-server/300)
- [RASPIAUDIO/squeezelite-MuseLuxe](https://github.com/RASPIAUDIO/squeezelite-MuseLuxe)

## Firmware / Apps

- [RASPIAUDIO Open Source App Store](https://raspiaudio.github.io/)

### Squeezelite

- This project is a [fork](https://github.com/RASPIAUDIO/squeezelite-esp32/) of the
  [sle118/Squeezelite-ESP32](https://github.com/sle118/squeezelite-esp32) project, customised for the MUSE Radio.
  `sle118/Squeezelite-ESP32` itself is a fork of [ralph-irving/Squeezelite](https://github.com/ralph-irving/squeezelite)
- For more details of see lyrion section in [media/audio/slimproto.md](../media/audio/slimproto.md)
- MuseRadio Squeezelite features:
  - AirPlay Support
  - Spotify Connect using [cspot](https://github.com/feelfreelinux/cspot)
  - Logitech Media Server Integration
  - Screen Support
  - Infrared Remote Control

#### Issues

- Using music-assistant the [display is only showing the IP](https://github.com/orgs/music-assistant/discussions/1305#discussioncomment-11631486)

### Home assistant voice satellite

- [Homeassistant Voice Satellite](https://github.com/tetele/muse-radio-voice-satellite)
  - This is an ESPHome configuration that turns the Raspiaudio Muse Radio into a Home Assistant voice satellite
  - [Forum: Muse Radio Home Assistant](https://forum.raspiaudio.com/t/muse-radio-home-assistant/1095)

### Flash firmware

#### Raspiaudio Open Source App Store

- [Raspiaudio Open Source App Store](https://raspiaudio.github.io/)
- Web based installer

#### Manual

- [Flash squeezelite firmware](https://forum.raspiaudio.com/t/espmuse-with-squeezelite-esp32-doesnt-start-after-battery-was-drained/515/5?u=potl)
- Turn off device
- Connect Laptop to Muse Luxe with Micro Usb cable

Then start the USB-serial console:

```console
$ screen /dev/ttyUSB0 115200
  nvs_erase_wifi_manager
  restart
```

- Turn on Muse Luxe

#### esptool

Follow [Multirooms with SQUEEZELITE with ESP32 MUSE PROTO](https://forum.raspiaudio.com/t/multirooms-with-squeezelite-with-esp32-muse-proto/291)

Install flash tool

```sh
sudo apt install esptool
```

Flash:

```sh
cd ~/projects/smart-home/esphome/espmuse-luxe
esptool -p /dev/ttyUSB0 write_flash 0x0 squeezeliteML.bin
```

## Troubleshooting

- [Forum: Lost after unboxing Espmuse Luxe](https://forum.raspiaudio.com/t/lost-after-unboxing-espmuse-luxe/481/5)

### Device "dead"

Symptoms:

- Green top LED doesn't turn on, device seems "dead"
- See also [EspMuse with squeezelite-esp32 doesn’t start after battery was drained](https://forum.raspiaudio.com/t/espmuse-with-squeezelite-esp32-doesnt-start-after-battery-was-drained/515)

Solutions:

- It might just be in Squeezelite recovery mode
  - Browse to the Squeezelite-ESP32 web interface
    - If you don't find the IP, connect to the USB-serial console to find the IP
      in the serial logs
  - Click at the bottom button `Exit recovery mode`
- If this doesn't help, try flashing the firmware again
