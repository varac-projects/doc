# Fritzbox

- [freetz-ng](https://github.com/Freetz-NG/freetz-ng)
  - firmware modification for ​AVM devices like FRITZ!Box

## APIs

- [AVM: schnittstellen](https://avm.de/service/schnittstellen/)
- [AVM TR-064 – First Steps](https://avm.de/fileadmin/user_upload/Global/Service/Schnittstellen/AVM_TR-064_first_steps.pdf)

### tr-064 SOAP API

- [fritzconnection](https://fritzconnection.readthedocs.io)
  - Python library to communicate with the AVM Fritz!Box by the TR-064 protocol,
    the AHA-HTTP-Interface and also provides call-monitoring.
- [a1fbox](https://github.com/bufemc/a1fbox)
  - Fritz!Box tool set, e.g. parsing the call monitor, adding phone entries to phonebook, auto-blocking calls etc.
  - Based on fritzconnection
- [Arduino-TR-064-SOAP-Library](https://github.com/Aypac/Arduino-TR-064-SOAP-Library)
  - Arduino library for the TR-064 protocol, most commonly used by the Fritz!Box router API
- [TR-064: x_contactSCPD](https://avm.de/fileadmin/user_upload/Global/Service/Schnittstellen/x_contactSCPD.pdf)
  - The service allows to configure contact features

Tools:

- [fritzbox-soap-json](https://github.com/ran-sama/fritzbox-soap-json)
  - Using Python 3 to send SOAP requests and get JSON formatted replies.
- [fritz_TR-064](https://github.com/sky321/fritz_TR-064)
  - Fritzbox tools for TR-064

Examples:

- [curl example](https://raw.githubusercontent.com/nicoh88/cron_fritzbox-reboot/refs/heads/master/cron_fritzbox-reboot.sh)
- "Get" [python example](https://github.com/ran-sama/fritzbox-soap-json/blob/master/soap-dsl1.py)

## Monitoring

- <https://labs.consol.de/monitoring/2017/03/08/prometheus-und-die-fritzbox.html>
- <https://github.com/mxschmitt/fritzbox_exporter>
- <https://hub.docker.com/r/mxschmitt/fritzbox_exporter>

  `GOBIN=/usr/local/bin go get github.com/mxschmitt/fritzbox_exporter/cmd/exporter`
