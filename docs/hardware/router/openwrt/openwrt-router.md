# OpenWRT router

## OpenWrt One

- [OpwnWRT wiki: OpenWrt One](https://openwrt.org/toh/openwrt/one)

## LTE Router

### Openwrt based

- [OpenWrt Table of Hardware: LTE Modem
  supported](https://openwrt.org/toh/views/toh_lte_modem_supported?dataflt%5BWLAN+5.0GHz*%7E%5D=ac&dataflt%5BSupported+Current+Rel*%7E%5D=22.03.5)

#### Gl Inet

- [Website](https://www.gl-inet.com/products/)
- [Github](https://github.com/gl-inet)

OpenWRT based routers

##### GL.iNet Spitz (GL-X750)

- 4G LTE OpenWrt Router, EMEA Version, EP06-E LTE Module, AC750 Dual-Band Wi-Fi, IoT Gateway, VPN Client and Server
- [Product page](https://www.gl-inet.com/products/gl-x750/)
- [OpenWRT wiki page](https://openwrt.org/toh/gl.inet/gl-x750)
- 16MB Flash, 128MB RAM
- ~180€
- 2 detachable LTE Antennas
- No external Wifi-Antenna, but DIY mounting possible

#### Teltonika

- Website currently offline

### Openwrt based, no official OpenWRT support

#### KuWFI

- [KuWFI shop: 4G LTE Router](https://www.kuwfi.shop/collections/4g-lte-router-2)
- **Not listed in OpenWrt TOH: LTE Modem support**

#### Tx-Team

- [Tx-Team LTE-Router IRS0200](https://www.directindustry.de/prod/tx-team-gmbh/product-212385-2209049.html)
- 2x LTE, 1x Wifi antenna
