# OpenWRT flashing

## TFTP

* [Flashing docs: TFTP](https://openwrt.org/docs/guide-user/installation/generic.flashing.tftp)

Install tftp package:

    sudo apt-get install tftpd-hpa tftp

Prevent tftp from auto-start:

    sudo systemctl stop tftpd-hpa.service
    sudo systemctl mask tftpd-hpa.service

Increase verbosity in `/etc/default/tftpd-hpa` by adding `-vvv` to `TFTP_OPTIONS`:

    TFTP_OPTIONS="--secure -vvv"

Copy firmware to `/srv/tftp` dir:

    sudo cp openwrt-ramips-mt76x8-tplink_tl-mr6400-v5-squashfs-tftp-recovery.bin /srv/tftp/tp_recovery.bin

    sudo systemctl unmask tftpd-hpa.service
    sudo systemctl start tftpd-hpa.service

Test:

    sudo journalctl -f -u tftpd-hpa.service

    tftp localhost
    tftp> get tp_recovery.bin
    Received 8166088 bytes in 0.2 seconds
    tftp> quit

    rm tp_recovery.bin

Prepare for flashing:

    sudo systemctl stop NetworkManager
    sudo ip addr add dev enx0050b6ebc45b 192.168.0.225/24

## After flashing

IP after flashing:

* `ssh root@192.168.1.1`
* Open [http://192.168.1.1](http://192.168.1.1)
