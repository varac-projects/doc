# OpenWRT

- [Which Firmware ?](https://wiki.openwrt.org/doc/howto/obtain.firmware.download#file_naming)

## Hardware considerations

- Min. 128 MB Storage/128 MB RAM to be able to use advanced Ansible collections like
  -[imp1sh/ansible_openwrt](https://github.com/imp1sh/ansible_openwrt)
- [64 MB Flash for Tailscale](https://openwrt.org/docs/guide-user/services/vpn/tailscale/start#installation_on_storage_constrained_devices)

## First setup

- Set root-pw
- Set IP, reboot
- [add ssh key](http://buffalo/cgi-bin/luci/admin/system/admin/sshkeys)
- Login in via ssh

Install packages:

```sh
opkg update
opkg install git tmux
```

- Check in `/etc`

```sh
cd /etc
git init .
git add .
git commit -m'Initial commit'
```

## Upgrade all packages

- [opkg update-all](https://adminswerk.de/openwrt-opkg-update-all/)
- [opkg docs](https://openwrt.org/docs/guide-user/additional-software/opkg)

> Since OpenWrt firmware stores the base system in a compressed read-only
> partition, any update to base system packages will be written in the
> read-write partition and therefore use more space than it would if it was just
> overwriting the older version in the compressed base system partition. It's
> recommended to check the available space in internal flash memory and the
> space requirements for updates of base system packages.

Run in tmux (!):

```sh
tmux
opkg list-upgradable | cut -f 1 -d ' ' | xargs -r opkg upgrade
reboot
```

## Add packages

### Basics

```sh
opkg install curl luci-app-ddns luci-app-upnp screen rsync
```

### openssh

```sh
opkg openssh-client openssh-sftp-server
```

### Wlan uplink

- [Connect to client Wi-Fi networkhttp://192.168.8.1/](https://openwrt.org/docs/guide-user/network/wifi/connect_client_wifi)
  - Network/Wireless
  - Scan
  - Join Network

## Storage

## Freeing space

- [FAQ: No space left on device](https://openwrt.org/faq/no_space_left_on_device)
- [How to list only packages that I have installed?](https://forum.openwrt.org/t/solved-opkg-how-to-list-only-packages-that-i-have-installed/11983/15)
  - [Script to show installed packages after flashing](https://gist.github.com/benok/10eec2efbe09070150ed2100d29dc743)
- [Recover from full /overlay](https://forum.openwrt.org/t/i-need-help-some-for-free-space/129674/3)

What helped after package installation broke and left half-installed packages:

- Removed leftovers from `/overlay/upper/usr/lib/`
- Reboot

### USB Storage

[Quick Start for Adding a USB drive](https://openwrt.org/docs/guide-user/storage/usb-drives-quickstart)

### Extroot

- [Extroot docs](https://openwrt.org/docs/guide-user/additional-software/extroot_configuration)

For SD-Card usage (yes, `kmod-usb-storage` needs to be installed to access a
sd-card, at least on the `GL.Inet X-750`):

```sh
opkg install kmod-mmc kmod-usb-storage
ls -al /sys/block/
ls -la ls -al /dev/sd*
parted -s /dev/sda -- mklabel gpt mkpart extroot 2048s -2048s
```

## UCI

[User guide: UCI](https://openwrt.org/docs/guide-user/base-system/uci)
