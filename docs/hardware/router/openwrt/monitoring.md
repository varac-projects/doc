# OpenWRT monitoring

## Prometheus metrics

- [OpenWrt and prometheus-node-exporter](https://grafana.com/blog/2021/02/09/how-i-monitor-my-openwrt-router-with-grafana-cloud-and-prometheus/)
- [Monitor OpenWrt nodes with Prometheus](https://www.cloudrocket.at/posts/monitor-openwrt-nodes-with-prometheus/)
- Install sizes:
  - [prometheus-node-exporter-lua](https://openwrt.org/packages/pkgdata/prometheus-node-exporter-lua):
    4Kb (116 Kb incl. dependencies)
  - All other `prometheus-node-exporter-lua-*` packages: 24Kb
  - Alltogether: 140Kb

Install:

```sh
opkg list|grep promet
opkg install \
  prometheus-node-exporter-lua \
  prometheus-node-exporter-lua-openwrt \
  prometheus-node-exporter-lua-wifi \
  prometheus-node-exporter-lua-wifi_stations
```

These packages provide extra metrics, although those
are not really intresting (`-nat_traffic` monitors traffic to all different
IP addresses i.e.):

```sh
opkg install \
  prometheus-node-exporter-lua-netstat \
  prometheus-node-exporter-lua-nat_traffic
```

Fix [duplicate `TYPE` definitions](https://github.com/openwrt/packages/issues/19508)
which break the upload of metrics to the pushgateway
until the [pull request](https://github.com/openwrt/packages/pull/24319)
arrives in the `prometheus-node-exporter-lua` package:

```sh
cp /usr/lib/lua/prometheus-collectors/netclass.lua /usr/lib/lua/prometheus-collectors/netclass.lua.bak
wget \
  -O /usr/lib/lua/prometheus-collectors/netclass.lua \
  https://raw.githubusercontent.com/openwrt/packages/master/utils/prometheus-node-exporter-lua/files/usr/lib/lua/prometheus-collectors/netclass.lua
```

Bind prometheus-node-exporter-lua to lan interface:

```sh
sed -i 's/loopback/lan/' /etc/config/prometheus-node-exporter-lua
service prometheus-node-exporter-lua restart
```

Query metrics:

```sh
curl 192.168.8.1:9100/metrics
```

### Grafana dashboard

- [openwrt grafana dashboard](https://github.com/try2codesecure/grafana_dashboards/tree/master/OpenWRT)

## Traffic monitoring

- [Openwrt wiki: Network monitoring](https://openwrt.org/docs/guide-user/services/network_monitoring/start)
- [Openwrt wiki: View historical bandwidth usage](https://openwrt.org/docs/guide-user/services/network_monitoring/bwmon#view_historical_bandwidth_usage)

### nlbwmon

- [nlbmon](https://github.com/jow-/nlbwmon)
- Simple conntrack/netlink based traffic accounting

Usage:

```sh
nlbw -c show

nlbw -c show -g mac,fam -o conn
```

### Other standalone tools

Actively maintained tools which allow historical view, which can get installed as opkg:

- [vnstat](https://humdi.net/vnstat/)
  - [Openwrt wiki: vnStat](https://openwrt.org/docs/guide-user/services/network_monitoring/vnstat)
  - Only overall traffic monitoring, no details about hosts/protocols
- [darkstat](https://github.com/emikulic/darkstat)
  - [Openwrt wiki page](https://openwrt.org/docs/guide-user/services/network_monitoring/darkstat)
  - Shows hosts
  > Darkstat shows the traffic in real time the traffic for different hosts
  > within your network, but it does not show the traffic profile of the
  > various host over time.

Luci integrated:

- [luci-app-statistics](https://openwrt.org/docs/guide-user/luci/luci_app_statistics)
  Based on `collectd` ?
- [luci-app-vnstat](https://openwrt.org/packages/pkgdata/luci-app-vnstat)

Unmaintained tools:

- [bmon](https://github.com/tgraf/bmon) (last commit 2020-02)
- [bwmon](https://github.com/stefankoegl/bwmon) (last commit 2012)
- [bandwithd](https://github.com/NethServer/bandwidthd) (last commit 2017)
  Also: can't monitor ipv6
- [wrtmwmon](https://github.com/pyrovski/wrtbwmon) last commit 2020-05
- [collectd](https://github.com/collectd/collectd) last release 2020
- [YaMon-v4](https://github.com/al-caughey/YAMon-v4) last commit 2020-06
- [iptraf-ng](https://github.com/iptraf-ng/iptraf-ng/) last commit 2020-12
- [cshark](https://github.com/cloudshark/cshark) last commit 2020-07

Unknown, no source found:

- [netifyd](https://openwrt.org/packages/pkgdata/netifyd): DPI server
