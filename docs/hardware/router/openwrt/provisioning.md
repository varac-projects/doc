# OpenWRT Provisioning / Infrastructure-as-code

## OpennWisp

* [Website](https://openwisp.org/)
* [Docs](https://openwisp.io/docs)
* OpenVPN and Wigeguard support, still no Tailscale

## Terraform

* [joneshf/openwrt](https://registry.terraform.io/providers/joneshf/openwrt/latest) provider
  * Currently the only Terraform OpenWrt provider
  * Mostly Network/DHCP/Wireless resources

## Ansible

* [Galaxy OpenWrt collections/roles](https://galaxy.ansible.com/search?deprecated=false&keywords=openwrt&order_by=-relevance&page=1)
  * General community collecttion modules
    * [community.general.opkg](https://docs.ansible.com/ansible/latest/collections/community/general/opkg_module.html)
    * [community.general.openwrt_init](https://docs.ansible.com/ansible/latest/collections/community/general/openwrt_init_module.html)
  * [imp1sh/ansible_openwrt](https://github.com/imp1sh/ansible_openwrt) collecttion
    * [Docs](https://wiki.junicast.de/en/junicast/docs/AnsibleOpenWrtCollection)
    * Actively maintained
    * Needs Python
    * Minimal requirements: 128 MB Storage, 128+ MB RAM (for Restic 512+ MB)
  * [gekmihesg/ansible-openwrt](https://github.com/gekmihesg/ansible-openwrt) role
    * Most popular role, development stalled since 2022-12
    * No Python needed
  * [NN708/ansible-openwrt](https://github.com/NN708/ansible-openwrt) collection
    * No Python needed
    * no opkg support
    * Few resources

Install python:

    opkg install python3-light
