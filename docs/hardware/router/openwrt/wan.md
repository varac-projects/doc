# OpenWRT WWAN / LTE modem

There are multiple modes for LTE modem operation:

* [QMI (NDIS) mode](https://openwrt.org/docs/guide-user/network/wan/wwan/ltedongle)
  * [qmi protocol](https://blogs.gnome.org/dcbw/2010/04/15/mobile-broadband-and-qualcomm-proprietary-protocols/)
    is a proprietary protocol by Qualcomm.
  * Used for GL.iNet Spitz x750
  * "Subjective, solution based on NDIS (QMI mode) works much stable. Faster reconnect. Easy to control and monitor."
* [NCM mode](https://openwrt.org/docs/guide-user/network/wan/wwan/ethernetoverusb_ncm)
* [RNDIS mode](https://openwrt.org/docs/guide-user/network/wan/wwan/ethernetoverusb_rndis)
* [AT command mode](https://openwrt.org/docs/guide-user/network/wan/wwan/3gdongle)

## QMI / NDIS modem setup

* [Router Preparation](https://openwrt.org/docs/guide-user/network/wan/wwan/ltedongle#router_preparation)
Install packages:

```sh
opkg update
opkg install usb-modeswitch kmod-mii kmod-usb-net kmod-usb-wdm kmod-usb-net-qmi-wwan uqmi luci-proto-qmi
reboot
```

[Test modem](https://openwrt.org/docs/guide-user/network/wan/wwan/ltedongle#interface_configuration):

```sh
ls -al /dev/cdc-wdm0
uqmi -d /dev/cdc-wdm0 --get-data-status
uqmi -d /dev/cdc-wdm0 --get-signal-info
```

QMI modem issues:

* [GH: uqmi: hangs on --get-pin-status](https://github.com/openwrt/openwrt/issues/10472#issuecomment-1523431151)
  * 2023-07-01: Installed [uqmi wrapper script](https://github.com/openwrt/openwrt/issues/10472#issuecomment-1523431151)
