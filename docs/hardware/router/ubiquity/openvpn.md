# OpenVPN with Ubiquity Edgerouter / EdgeOS

- Current CA in `/config/auth/demoCA`
- Openssl config at `/usr/lib/ssl/openssl.cnf`

Start:

```sh
sudo -i
cd /usr/lib/ssl/misc
```

## Increase validity days

- Edit `CA.pl` and increase:
  - `DAYS`
  - `CADAYS`
- Edit `../openssl.cnf` and increase:
  - `DAYS`

## Generate a root certificate / CA

```sh
./CA.pl -newca
```

Now enter the new CA pem password:

```sh
Enter PEM pass phrase:
```

__Don't__ add any `challenge password` !

```sh
cp demoCA/cacert.pem /config/auth
cp demoCA/private/cakey.pem /config/auth
```

## Generate the server certificate request

```sh
./CA.pl -newreq
```

- PEM password: `test` (will get renmoved later)
- Common name: `openvpn server cert`

## Sign the server certificate request

```sh
./CA.pl -sign
```

Remove the `server.key` password:

```sh
openssl rsa -in newkey.pem -out newkey-no-pw.pem
rm newkey.pem
mv newcert.pem /config/auth/server.pem
mv newkey-no-pw.pem /config/auth/server.key
```

## Generate, sign and move the certificate and key files for the first OpenVPN client

```sh
./CA.pl -newreq
```

- PEM password: `test` (will get renmoved later)
- Common name: i.e. `client cert varac`

```sh
./CA.pl -sign
```

Remove the client cert password and add read permission
for non-root users to be able to scp the files later:

```sh
openssl rsa -in newkey.pem -out newkey-no-pw.pem
chmod 644 newkey-no-pw.pem
rm newkey.pem newreq.pem
mv newcert.pem /config/auth/client1.pem
mv newkey-no-pw.pem /config/auth/client1.key
```

Repeat with other clients.

### Restart openvpn

In- or decrease the log level, which restarts the OpenVPN process:

```sh
configure
set interfaces openvpn vtun0 openvpn-option '--verb 7'
commit ; save
```

This will __not__ re-read the config, but restart all client connections:

```sh
reset openvpn interface vtun0
```

which is the same as

```sh
kill -USR1 $(cat /var/run/openvpn-vtun0.pid)
```
