# EdgeOS

- [Blog post explaining the evolution of Vyatta, VyOS and EdgeOS](https://blog.vyos.io/versions-mystery-revealed)
  - EdgeOS is a Fork from [Vyatta](https://en.wikipedia.org/wiki/Vyatta) 6.3.
    - [Ubiquity community wiki: EdgeOS API Guide](https://ubntwiki.com/products/software/edgeos/api)
  - Community development of Vyatta Core: [VyOS](https://vyos.io/)
    - [GitHub](https://github.com/vyos/vyos-1x)
    - [VyOS docs](https://docs.vyos.io/en/sagitta/)
- [Ubiquity community wiki: Overview of software products](https://ubntwiki.com/products/software?s%5B%5D=edgeos)
