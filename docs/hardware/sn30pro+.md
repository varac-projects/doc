# SN30 Pro+

* [Manual](https://download.8bitdo.com/Manual/Controller/SN30-Pro+/SN30_Pro+_Manual.pdf?0318)

Pairing mode:

* Press `Start` to turn on controller
* Press `Pair` for 3s to enter pairing mode
