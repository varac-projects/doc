# Hardware / VM inventory tools

## ansible-cmdb

- [Github](https://github.com/fboender/ansible-cmdb)
- [Docs](https://ansible-cmdb.readthedocs.io/en/latest/)

- [Offline devices](https://github.com/fboender/ansible-cmdb/issues/57)

## netbox

- [GitHub](https://github.com/netbox-community/netbox)
