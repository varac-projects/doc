# Hardware recycling

## Germany

- [Hardware for Future Leipzig](https://hardwareforfuture.de/)
- [Computertruhe e. V.](https://computertruhe.de/standorte/)
  - Different locations in Germany
- [Hey, Alter!](https://heyalter.com/)
  - Different locations in Germany
