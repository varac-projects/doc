# Thinkpad X1 Carbon

## X1 Carbon Gen 7 (private)

- Model 20QE - S5JY1S
- [Arch wiki Gen 7](<https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Carbon_(Gen_7)>)
- [ifixit: Lenovo ThinkPad X1 Carbon 7th Gen SSD Replacement](https://www.ifixit.com/Guide/Lenovo+ThinkPad+X1+Carbon+7th+Gen+SSD+Replacement/130841)

## X1 Carbon Gen 10 (work)

- [Arch wiki: Lenovo ThinkPad X1 Carbon (Gen 10)](<https://wiki.archlinux.org/title/Lenovo_ThinkPad_X1_Carbon_(Gen_10)>)
- [LVFS firmware updates](https://fwupd.org/lvfs/devices/com.lenovo.ThinkPadN3AETXXW.firmware)
- [The ThinkPad X1 Carbon Gen 10 as a Linux laptop](https://www.theregister.com/2023/03/10/thinkpad_x1c_g10_linux/)
  - [Hacker news](https://news.ycombinator.com/item?id=35324961)

### Webcam

Doesn't work yet. From the arch wiki page:

> Warning: Do not buy this laptop (says even Lenovo) with a 2.8k OLED screen
> because the only webcam available with that screen is the MIPI camera (listed
> by Lenovo as 'Computer Vision camera') which does not have open source drivers
> and will just not work in the coming few years.

- [Hans de Goede: IPU6 camera support status update](https://hansdegoede.dreamwidth.org/29233.html)
- [intel/ipu6-drivers: Fedora 38 on X1 carbon gen 10](https://github.com/intel/ipu6-drivers/issues/167)
- [Lenovo forum: Ubuntu/Mint on Thinkpad X1 Carbon Gen 10](https://forums.lenovo.com/t5/Ubuntu/Ubuntu-Mint-on-Thinkpad-X1-Carbon-Gen-10/m-p/5145526?page=5)
- [Ubuntu bug: Support Intel IPU6 MIPI camera on Alder Lake platforms](https://bugs.launchpad.net/ubuntu/+source/linux-firmware/+bug/1955383)
- [Ubuntu webcam issues for Dell Laptops](https://wiki.ubuntu.com/Dell), see
  [LP:#1978757 Cheese can't playback preview not working on 22.04](http://pad.lv/1978757)
- [Browser webcam test](https://webcamtests.com/)
- [Build with dkms](https://github.com/intel/ipu6-drivers/tree/iotg_ipu6#3-build-with-dkms)
- [Suspend/hibernate issue on Thinkpad x1 carbon gen10](https://github.com/intel/ipu6-drivers/issues/107)
- [archlinux-ipu6-webcam support issue](https://github.com/stefanpartheym/archlinux-ipu6-webcam/issues/21)

Works on kernel 6.5, but not on 6.6:
[Rebuilding the DKMS in Kernel 6.6.2 is problematic](https://github.com/stefanpartheym/archlinux-ipu6-webcam/issues/57)

### Installation from kernel v6.11 onwards

- [Please upstream modules into mainline kernel](https://github.com/intel/ipu6-drivers/issues/22)
  [IPU6 was enabled in the 6.11~rc4-1~exp1 release of the Linux kernel](https://github.com/intel/ipu6-drivers/issues/22#issuecomment-2323414050)
- [How to use the IPU6 webcam with kernel 6.10+?](https://bbs.archlinux.org/viewtopic.php?pid=2181619#p2181619)
  - [feed](https://bbs.archlinux.org/extern.php?action=feed&tid=297262&type=atom)
- [Fedora wiki: Changes/IPU6 Camera support](https://fedoraproject.org/wiki/Changes/IPU6_Camera_support)

From
[Debian bugtracker: linux-image-amd64: Please enable several CONFIG\_ options required for IPU6/MIPI cameras](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1074441#56)

- Linux kernel >= `6.11.1`
- libcamera >= `0.3.1-2`
  - "libcamera requires DMAHEAP, so beyond the settings mentioned above, one
    also has to enable DMABUF_HEAPS (and possibly DMABUF_HEAPS_SYSTEM options; I
    just set bot to y)"
- systemd > `v256`, or create the file `/etc/udev/rules.d/70-ipu6-psys.rules` as
  shown in the relevant
  [PR: rules: Add uaccess tag to /dev/udmabuf](https://github.com/systemd/systemd/pull/33738/files)
  Install packages shown in this
  [success tutorial](https://bbs.archlinux.org/viewtopic.php?pid=2204987#p2204987):

```sh
sudo pacman -S extra/gst-plugin-libcamera extra/pipewire-libcamera extra/libcamera-tools extra/libcamera-ipa extra/libcamera
pamac install intel-ivsc-firmware
```

Still with cheese:

```sh
Internal data stream error.: ../gstreamer/subprojects/gstreamer/libs/gst/base/gstbasesrc.c(3177): gst_base_src_loop (): /GstCameraBin:camerabin/GstWrapperCameraBinSrc:camera_source/GstBin:bin36/GstPipeWireSrc:pipewiresrc1:
streaming stopped, reason not-negotiated (-4)
```

#### Pre 6.11 install

```sh
sudo pacman -S linux65-headers

git clone https://github.com/stefanpartheym/archlinux-ipu6-webcam ~/work/webcam/archlinux-ipu6-webcam
cd ~/work/webcam/archlinux-ipu6-webcam
./install.sh
```

If it breaks compiling the `icamerasrc-git` AUR package dependency follow
[this fix](https://github.com/stefanpartheym/archlinux-ipu6-webcam/issues/18#issuecomment-1557720342):

```sh
git clone https://aur.archlinux.org/icamerasrc-git.git ~/work/webcam/icamerasrc-git
cd ~/work/webcam/icamerasrc-git
sed -i 's/branch=icamerasrc_slim_api/commit=17841ab6249aaa69bd9b3959262bf182dee74111/'\
  PKGBUILD
makepkg -sic
```

Then run the install script again:

```sh
cd ~/work/webcam/archlinux-ipu6-webcam
./install.sh
```

After successfull execution:

```sh
reboot
```

Test webcam with `./test.sh`

```sh
./test.sh
```

Uninstall:

```sh
sudo pacman -R dkms intel-ipu6-dkms-git-fix intel-ipu6ep-camera-bin \
  intel-ivsc-driver-dkms-git v4l2loopback-dkms-git-fix \
  intel-ipu6ep-camera-hal-git-fix icamerasrc-git v4l2loopback-dkms-git-fix \
  v4l2-relayd-fix intel-ipu6ep-camera-bin-fix intel-ipu6ep-camera-hal-git-fix
```
