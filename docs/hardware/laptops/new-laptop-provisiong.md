# Provsioning new Laptops

## Install manjaro-sway

- [arch.md](../../operating-systems/arch.md)

## Enable ssh for ansible run

```sh
sudo systemctl start sshd
```

From ansible provsioning machine:

```sh
ssh-copy-id cide
```

## Ansible

```sh
cd ~/ansible/varac
ansible -m ping cide.varac.net
ansible-playbook -l cide.varac.net site.yml
```

## Install tools

On Debian bookworm:

```sh
# etc
sudo apt install wipe unzip minisign zsh zoxide pre-commit fzf
# neovim
sudo apt install hunspell hunspell-de-de hunspell-en-us npm luarocks ripgrep fd-find python3-pip ripgrep python3-neovim
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
sudo apt install -t trixie golang # >1.19
sudo npm install -g neovim
mise use -g neovim starship
```

- Install [mise](../../coding/shell/mise.md)

## Password store

WIP: In transition to
[rbw/bitwarden](../../security/passwords/bitwarden/cli-client.md)

## dotfiles

Clone and setup dotfiles, see [chezmoi](../../etc/dotfiles/chezmoi.md), section
`New machine`

## Documentation

Clone [doc](https://0xacab.org/varac-projects/doc):

```sh
git clone git@0xacab.org:varac-projects/doc.git
```

## Nextcloud client

- nextcloud-client (and configure to sync `common` folder)

## Optional steps

### Deploy GPG key for git signing

On zancas:

```sh
gpg --armor --export-secret-keys varac@varac.net > /tmp/varac@varac.net-sec.asc
scp /tmp/varac@varac.net-sec.asc cide:
wipe /tmp/varac@varac.net-sec.asc
```

On new laptop:

```sh
gpg --import varac@varac.net-sec.asc
wipe varac@varac.net-sec.asc
```
