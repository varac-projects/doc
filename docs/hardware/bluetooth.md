# Bluetooth

- [Arch wiki: Bluetooth](https://wiki.archlinux.org/title/bluetooth)

## bluetoothctl

- [Arch wiki:  bluetoothctl](https://wiki.archlinux.org/index.php/bluetooth#Bluetoothctl)

Start bluetoothctl:

```sh
bluetoothctl
```

Cmds:

```sh
devices Paired
```

Re-add H800 headset:

```sh
disconnect 00:0D:44:D3:57:C1
untrust 00:0D:44:D3:57:C1
remove 00:0D:44:D3:57:C1

scan on
pair 00:0D:44:D3:57:C1
trust 00:0D:44:D3:57:C1
connect 00:0D:44:D3:57:C1

info 00:0D:44:D3:57:C1
exit
```

## Power status

- [Getting the Battery Status of a Bluetooth Device on Arch Linux](https://www.jvt.me/posts/2021/12/10/bluetooth-percent-linux/)
- [Enabling experimental features](https://wiki.archlinux.org/title/Bluetooth#Enabling_experimental_features)

## Hardware specifics

### Headsets

- [Arch wiki: Bluetooth headset](https://wiki.archlinux.org/index.php/Bluetooth_headset)
- [Switch between HSP/HFP and A2DP setting](https://wiki.archlinux.org/title/bluetooth_headset#Switch_between_HSP/HFP_and_A2DP_setting)

#### Philips Headset SHB 8850 NC

Press Bluetooth button a few seconds.

#### SHOKZ OpenRun Pro

- [Benutzerhandbuch](https://cdn.shopify.com/s/files/1/0474/1981/8141/files/OpenRun_Pro_User_Guide-German.pdf?v=1699598130)

### Jambox

Paring mode: Hold the power switch on the side of the Jambox in the up position for 3 seconds

### Philips Audio BT-Adapter

[Philips AEA2500](https://www.philips.de/c-p/AEA2500_12/nfc-bluetooth-hifi-adapter-mit-aptx-aac)

Power off -> Power off -> First minute the adapter will enter pairing mode

### Logitech MX vertical mouse

1. Turn your mouse over and switch it on. Find the switch on the bottom of your mouse and switch it to the ON position.
1. The LED should be blinking fast. If not, press and hold the Easy-Switch™ button (on bottom of mouse) for 3 seconds.

### Logitech MX Keys keyboard

- Model-Nr: `YR0084`
- Pair: Press F1/F2/F3 for a few seconds
- [Factory reset](https://ernestojeh.com/factory-reset-mx-keys):
  Press `esc O esc O esc B`
