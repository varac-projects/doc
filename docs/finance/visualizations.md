# Finance visualizations

* [Visualizing personal finances using D3js](https://aaronstacy.com/writings/visualizing-personal-finances/)
  * [D3JS](https://d3js.org/)
  * [Example Dashboard](https://aaronstacy.com/personal-finances-dashboard/)
