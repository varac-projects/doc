# Blockchain finance

## APIs

- [Blockcypher API service to query balances](https://blockcypher.github.io/documentation)

> BlockCypher runs the Main and Test Bitcoin, Litecoin and Dogecoin
> blockchains. The BlockCypher API is a simple, mostly RESTful
> JSON API, accessed over HTTP or HTTPS from the api.blockcypher.com domain
> BlockCypher APIs can be used up to these rate limits:
> Classic requests, up to 5 requests/sec and 600 requests/hr
> WebHooks and WebSockets, up to 600 requests/hr

After limit is reached you're blocked for ~1 hour.

## Wallettool

- [wallettool](https://github.com/akx/walletool)

> A list of addresses / private keys is printed.

See `~/projects/finance/blockchain/walletool`

## Bitcoin

## Segwit / Bech32 / P2SH

> This transaction could save 36% on fees by upgrading to native SegWit-Bech32
> or 26% by upgrading to SegWit-P2SH

Solution:

> Just make a new wallet, and it should default to be native segwit
> and then send all your funds from the old wallet to the new wallet.

### APIs / Monitoring services

### Alerts

- [cryptocurrencyalerting.com](https://cryptocurrencyalerting.com)
- [Fee alert](https://txfees.watch/)

#### Charts

- [BTC transaction fees](https://bitinfocharts.com/de/comparison/bitcoin-transactionfees.html#3y)
- [Mempool congestion chart](https://www.blockchain.com/explorer/charts/mempool-size)

### Wallets

#### Electrum

- [Website](https://electrum.org/)
- [Docs](https://electrum.readthedocs.io/en/latest/)
- [Unofficial guides for Electrum](https://bitcoinelectrum.com/)
- [Reddit](https://www.reddit.com/r/Electrum/)
- [Bitcointalk Electrum forum](https://bitcointalk.org/index.php?board=98.0)
- IRC: `#electrum` on Freenode
