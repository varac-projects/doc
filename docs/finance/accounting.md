# Accounting

## CLI

- `beancount.md`

## Web based tools

- [Actual](https://actualbudget.com/)
  - [Github](https://github.com/actualbudget)
- [Firefly 3](https://www.firefly-iii.org/)

## Coop accounting tools

### food coop accounting

- [cicer](https://0xacab.org/meskio/cicer)
  web based software to manage the stock, purchases and balance of a consumer association
