# Invoicing

## Invoice ninja

* [Website](https://www.invoiceninja.org/)
* [Docs](https://invoice-ninja.readthedocs.io/en/latest/index.html)
* [Helm chart](https://artifacthub.io/packages/helm/invoiceninja/invoiceninja)
* [Flux recipes](https://open.greenhost.net/xeruf/stackspout/-/tree/main/basic/apps/ninja)
* [F-droid app](https://f-droid.org/en/packages/com.invoiceninja.app/)
