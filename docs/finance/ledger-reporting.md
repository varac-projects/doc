# Ledger reporting and graphing

https://plaintextaccounting.org/#ui-web

## ledgerble

A ui for ledger-cli files

https://github.com/sbridges/ledgerble

UI, not scriptable

## ledger-web

https://vifon.github.io/ledger-web/
https://github.com/Vifon/ledger-web
[Demo](https://ledger-web.herokuapp.com/)

- Needs a postgres db
- Very limited functionality

## ledger-pyreport

https://yingtongli.me/git/ledger-pyreport/about/

UI, not scriptable

### Usage

    cd ~/projects/accounting/ledger/ledger-pyreport
    FLASK_APP=ledger_pyreport python3 -m flask run

- Not on github, no issues
- Doesn't seem to support hledger

  `Exception: ledger: Unknown flag: --args-only (use -h to see usage)`

- Breaks on non-defined accounts, i.e.:

  `Error: Unknown account 'Assets:Ayla:Genossenschaftsanteile:4.4'`
