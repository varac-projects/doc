# FinTS frontends

## python-fints

https://github.com/raphaelm/python-fints/
https://python-fints.readthedocs.io/en/latest/

## fints2ledger

https://github.com/MoritzR/fints2ledger

Upgrade: `pipx upgrade fints2ledger`
Source code at `~/projects/finance/fints/fints2ledger`
Config at `~/.config/fints2ledger/`

### Issues

- [fints2ledger can't deal with TANs at the moment](https://github.com/MoritzR/fints2ledger/issues/6)
  So we need to fetch the transactions only from the last 3 month.
- [Release 1.0.0](https://github.com/MoritzR/fints2ledger/releases/tag/1.0.0)
  [removed csv support](https://github.com/MoritzR/fints2ledger/issues/27)
  so it cannot get easily upgrade

### Usage

Pull transactions:

    fints2ledger_pull.sh

- csv file is at `~/.config/fints2ledger/transactions.csv`
- journal is at `~/.config/fints2ledger/fints2ledger.journal`
