# Beancount converters / importers

## Import transactions

- [Plaintextaccounting import overview](https://plaintextaccounting.org/#data-importconversion)
- [Awesome list of importers](https://github.com/siddhantgoel/awesome-beancount/blob/master/README.md#importers)
- [Beancount importing docs](https://beancount.github.io/docs/importing_external_data.html)
- [beanhub-import](https://github.com/LaunchPlatform/beanhub-import)
  cli tool
- [smart_importer](https://github.com/beancount/smart_importer):
  Augment Beancount importers with machine learning functionality.
- [beancount-importer](https://github.com/jbms/beancount-import):
  Web UI for semi-automatically importing external data into beancount

Stale:

- [beancount-importer](https://github.com/jamatute/beancount-importer):
  last commit 2018
- [csv2beancount](https://github.com/PaNaVTEC/csv2beancount): last commit 2017

## Converters

### ledger2beancount

- [ledger2beancount](https://github.com/beancount/ledger2beancount)
- [ledger2beancount docs](https://ledger2beancount.readthedocs.io)

As debian package:

```sh
sudo apt install ledger2beancount
ledger2beancount ~/.hledger.journal > ~/beancount/hledger-export.beancount
```

### Stale

- [ledger-to-beancount](https://github.com/glasserc/ledger-to-beancount): Last
  commit 2018
