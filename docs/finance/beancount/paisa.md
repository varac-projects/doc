# Paisa

- [Website](https://paisa.fyi/)
  - [Github](https://github.com/ananthakumaran/paisa)
- Install: `pamac install paisa-bin`
- [Configuration](https://paisa.fyi/reference/config/)

## Usage
