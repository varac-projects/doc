# Beancount

- [Website](https://beancount.github.io)
- [Awesome beancount](https://github.com/siddhantgoel/awesome-beancount)
- [Issues](https://github.com/beancount/beancount/issues)
- [Docs](https://beancount.github.io/docs/)
  - [Docs live on gdocs](http://furius.ca/beancount/doc/index)
- Debian packaged
- IRC: #beancount and #plaintextaccounting on Freenode
- Gitter: `#gitter_beancount=2Ffava:matrix.org`
- [Someone from Germany?](https://www.reddit.com/r/plaintextaccounting/comments/jzi6ci/someone_from_germany/)

Issues:

- How to track if i.e. recurring income is recieved on a regular base ?
- [no virtual postings](https://beancount.github.io/docs/a_comparison_of_beancount_and_ledger_hledger.html#transactions-must-balance)
- [no easy net profit total in income report](https://beancount.narkive.com/44lsYQnV/cumulative-sum-in-bean-query)
- [no settlement dates](https://beancount.github.io/docs/settlement_dates_in_beancount.html)

## Tools

- [favagtk](https://gitlab.gnome.org/johannesjh/favagtk/)
  - No Arch package

### Importers

- [Importers/Germany](https://github.com/siddhantgoel/awesome-beancount#germany)
  - [Beancount importer for Volksbank & GLS Bank](https://github.com/Fjanks/beancount-importer-volksbank)
- [beanborg](https://github.com/luciano-fiandesio/beanborg)
  Beanborg automatically imports financial transactions from external CSV files

## Neovim plugins

- [tree-sitter-beancount](https://github.com/polarmutex/tree-sitter-beancount)
- [polarmutex/beancount.nvim](https://github.com/polarmutex/beancount.nvim)
  Stil [broken by a recent commit to telescope](https://github.com/polarmutex/beancount.nvim/issues/1)
- [nathangrigg/vim-beancount](https://github.com/nathangrigg/vim-beancount)
  - VimScript
  - Stale, last commit 2023-01
  - Doesn't [Support non-ASCII characters in account names](https://github.com/nathangrigg/vim-beancount/issues/53)
  - Screws up formatting on save

### LSP

- [polarmutex/beancount-language-server](https://github.com/polarmutex/beancount-language-server)
- [beancount-language-server AUR package](https://aur.archlinux.org/packages/beancount-language-server#comment-944608)
  - old and lacking behind upstream releases
- [mason registry](https://github.com/mason-org/mason-registry/blob/main/packages/beancount-language-server/package.yaml)

Issues:

- [Completion issues: Only works if starting wit a lowercase letter](https://github.com/polarmutex/beancount-language-server/issues/477)

## Linters / Formatters

- [beancount-black](https://github.com/LaunchPlatform/beancount-black)

## Journal Syntax

- [Quickref](https://plaintextaccounting.org/quickref)
- [Syntax](https://fava.pythonanywhere.com/example-beancount-file/help/beancount_syntax)

## Plugins

- [autobean.narration: Narrations for Postings](https://github.com/SEIAROTg/autobean/tree/master/autobean/narration)

## Exporting metrics / Grafana

- [beancount google group: beancount > prometheus > grafana](https://groups.google.com/g/beancount/c/R3C9c-BPOGI/m/oiOBO1uBAgAJ?pli=1)

## bean-price

- [beanprice](https://github.com/beancount/beanprice)
- [YahooError: Status 401: Invalid Cookie](https://github.com/beancount/beanprice/issues/75)
  When querying yahoo you _need to pass a date_, see example below

Examples:

```sh
bean-price -d $(date -u +%Y-%m-%dT%H:%M:%S) -e EUR:yahoo/btc-eur
bean-price -e EUR:coinbase/btc-eur
```

[Prices from a Beancount Input File](https://beancount.github.io/docs/fetching_prices_in_beancount.html#prices-from-a-beancount-input-file):

```sh
bean-price --inactive personal.beancount
```

## Other tools

- [beangrep](https://github.com/zacchiro/beangrep)
  grep-like filter for Beancount

## Sorting transactions

### beanhub-cli

- [GitHub](https://github.com/LaunchPlatform/beanhub-cli)
- [Docs](https://beanhub-cli-docs.beanhub.io/)
- Comment preserving: Can sort a journal file without dropping comments
- Based on [beancount-black](https://github.com/LaunchPlatform/beancount-black)
  library (it's cli component is in maintenance mode and will get removed soon)
- No Arch/Manjaro package

Install:

```sh
pipx install beanhub-cli
```

Sort:

```sh
bh format accounts/personal/bargeld/bargeld.beancount

```

#### Issues

- Broken `stdin-mode`: [Write debug messages to stderr instead of stdout](https://github.com/LaunchPlatform/beanhub-cli/issues/7)

### Sorting with native beancount

**Beware**: All beancount-native methods will strip comments,
since the parser ignores comments.
There's currently no way of sorting transactions while preserving the comments
using native beancount.

Sort transactions in single file with `bean-report`:

```sh
bean-report input.beancount print >output.beancount
```

Sort transactions in multiple files:
Use [this script](https://groups.google.com/g/beancount/c/dWRPZlVFJak/m/_B5wE57pAgAJ)

## beancount v3

- `bean-query` migrated to [beanquery](https://github.com/beancount/beanquery)

Missing:

- Explain doesn't work: `EXPLAIN SELECT * FROM year = 2014;`
