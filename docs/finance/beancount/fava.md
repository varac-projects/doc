# Fava web interface

- [Website](https://beancount.github.io/fava)
- [Github](https://github.com/beancount/fava)
- [Demo site](https://fava.pythonanywhere.com/example-beancount-file/income_statement)
- [Docs/Help](https://fava.pythonanywhere.com/example-beancount-file/help/)

## Install

Install on Arch:

    pamac install fava

Install on Debian/Ubuntu:

    sudo apt install python3-fava

## Usage

    fava ~/beancount/personal.beancount

Then open `http://localhost:5000`.

Config: see `~/beancount/personal.beancount`

## Extensions

- [Fava Extensions](https://fava.pythonanywhere.com/example-beancount-file/help/extensions)

### fava-dashboards

- [Github](https://github.com/andreasgerstmayr/fava-dashboards)
  Custom Dashboards for Beancount in Fava

  Start with [default dashboard](https://github.com/andreasgerstmayr/fava-dashboards/blob/main/example/dashboards.yaml):

      wget https://raw.githubusercontent.com/andreasgerstmayr/fava-dashboards/main/example/dashboards.yaml ~/beancount/dashboards.yaml

### fava-review

- [Github](https://github.com/kubauk/fava-review)
  A Fava extension to help review transactions over a series of periods.

Install:

    pip install fava-review

Issues:

- When installed as pip package: [Could not build url for endpoint 'extension_report'](https://github.com/kubauk/fava-review/issues/13)
  - Solution: Downgrade fava to `1.24.4` until this issue gets fixed

Can't install [fava-review-git AUR package](https://aur.archlinux.org/packages/fava-review-git)
because [python-tables 3.9.1-3 AUR package](https://aur.archlinux.org/packages/python-tables)
fails to install:

    ~ $ pamac install  python-tables
    Warning: python-tables is only available from AUR
    Preparing...
    Cloning python-tables build files...
    Generating python-tables information...
    Checking python-tables dependencies...
    Resolving dependencies...
    Checking inter-conflicts...

    To build (1):
      python-tables  3.9.1-3    AUR


    Edit build files : [e]
    Apply transaction ? [e/y/N] y

    ==== AUTHENTICATING FOR org.manjaro.pamac.commit ====
    Authentication is required to install, update, or remove packages
    Authenticating as: Varac (varac)
    Password:
    ==== AUTHENTICATION COMPLETE ====

    Building python-tables...
    ==> Making package: python-tables 3.9.1-3 (Mo 20 Nov 2023 15:25:15 CET)
    ==> Checking runtime dependencies...
    ==> Checking buildtime dependencies...
    ==> Retrieving sources...
      -> Found tables-3.9.1.tar.gz
    ==> Validating source files with sha256sums...
        tables-3.9.1.tar.gz ... Passed
    ==> Removing existing $srcdir/ directory...
    ==> Extracting sources...
      -> Extracting tables-3.9.1.tar.gz with bsdtar
    ==> Removing existing $pkgdir/ directory...
    ==> Starting build()...
    * Getting build dependencies for wheel...
    * Using Python 3.11.5 (main, Sep  2 2023, 14:16:33) [GCC 13.2.1 20230801]
    * Found cython 3.0.5
    * USE_PKGCONFIG: True
    [11/20/23 15:25:16] WARNING  pyproject.toml does not contain a tool.setuptools_scm section
    setuptools.py:119
    * Found HDF5 headers at ``/usr/include``, library at ``/usr/lib64``.
    * Found LZO 2 headers at ``/usr/include``, library at ``/usr/lib64``.
    * Skipping detection of LZO 1 since LZO 2 has already been found.
    * Found bzip2 headers at ``/usr/include``, library at ``/usr/lib64``.
    * Found blosc headers at ``/usr/include``, library at ``/usr/lib64``.
    * Run 'blosc2_find_directories_hook'
    * Unable to find blosc2 wheel.
    * Found blosc2 headers at ``/usr/include``, library at ``/usr/lib64``.
    .. WARNING:: Unsupported Blosc2 version installed! Blosc2 2.10.4+ required.
    Found version 2.10.0.  Update it via `pip install blosc2 -U`.
    Compiling tables/utilsextension.pyx because it changed.
    [1/1] Cythonizing tables/utilsextension.pyx
    Compiling tables/hdf5extension.pyx because it changed.
    [1/1] Cythonizing tables/hdf5extension.pyx
    Compiling tables/tableextension.pyx because it changed.
    [1/1] Cythonizing tables/tableextension.pyx
    Compiling tables/linkextension.pyx because it changed.
    [1/1] Cythonizing tables/linkextension.pyx
    Compiling tables/_comp_lzo.pyx because it changed.
    [1/1] Cythonizing tables/_comp_lzo.pyx
    Compiling tables/_comp_bzip2.pyx because it changed.
    [1/1] Cythonizing tables/_comp_bzip2.pyx
    Compiling tables/lrucacheextension.pyx because it changed.
    [1/1] Cythonizing tables/lrucacheextension.pyx
    Compiling tables/indexesextension.pyx because it changed.
    [1/1] Cythonizing tables/indexesextension.pyx
    performance hint: tables/indexesextension.pyx:192:5:
    Exception check on 'swap_bytes' will always require the GIL to be acquired.
    Possible solutions:
      1. Declare the function as 'noexcept' if you control the definition and
         you're sure you don't want the function to raise exceptions.
      2. Use an 'int' return type on the function to allow an error code to be returned.
    performance hint: tables/indexesextension.pyx:212:5:
         Exception check on '_keysort' will always require the GIL to be acquired.
    ...

    .. ERROR:: Unable to find the blosc2 library.

    ERROR Backend subprocess exited when trying to invoke get_requires_for_build_wheel
    ==> ERROR: A failure occurred in build().
        Aborting...

With [blosc2-git 2.9.2.r27.g80464026-1 AUR package](https://aur.archlinux.org/packages/blosc2-git) I get this:

    ERROR Missing dependencies:
      blosc2>=2.2.8

## Filters

[Filters docs](https://fava.pythonanywhere.com/example-beancount-file/help/filters)

Exclude account (from i.e. the balance sheet):

    all(-account:"Income:Privat")
    http://localhost:5000/beancount/balance_sheet/?filter=all%28-account%3A%22Income%3APrivat

    http://localhost:5000/beancount/income_statement/?time=2021&account=Privat

## Issues

- [FR: Stacked graphs](https://github.com/beancount/fava/issues/716)
  - [Corresponding PR](https://github.com/beancount/fava/pull/1286)
