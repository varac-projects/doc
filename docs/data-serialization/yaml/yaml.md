# YAML

## Multiline

- [yaml-multiline.info](hhttps://yaml-multiline.info/)
- [Multiline explained on stackoverflow](https://stackoverflow.com/a/21699210)
- See also `~/projects/yaml/examples/multiline.yml`

## Parse yaml

## General issues

All cli parsers were, as of today (2019-10) not able to handle custom data types
like ansible-vault uses them. Here are the issues:

- [yq-python](https://github.com/kislyuk/yq/issues/70): Won't fix b/c of the
  nature of yq-python, which converts data to json and uses `jq` for parsing.
- [shyaml](https://github.com/0k/shyaml/issues/40)
- [yq-go](https://github.com/mikefarah/yq/issues/172) is using `go-yaml`, and
  the bug is still open in
  [go-yaml](https://github.com/go-yaml/yaml/issues/191).
- [yaml-cli](https://github.com/pandastrike/yaml-cli/issues/28) (Anyhow,
  yaml-cli stalled development, last commit 2016)

Example:

```sh
export YAML="/home/varac/projects/yaml/examples/ansible-vault-example.yml"
yq -y . < $YAML
yq -Y . < $YAML

dasel -f $YAML .
```

## Tools

## yq

For the two `yq` tools (go / python) see [yq.md](./yq.md)

## dasel

<https://github.com/TomWright/dasel>

## shyaml

<https://github.com/0k/shyaml>

Can only **parse** yaml.

### Install

```sh
pipsi install shyaml
```

or

```sh
wget https://raw.githubusercontent.com/0k/shyaml/master/shyaml \
  -O /usr/local/bin/shyaml && chmod a+x /usr/local/bin/shyaml
```

### query

show all values:

```sh
cd ~/leap/git/bitmask
cat hiera/local1.yaml| shyaml get-values
```

get particular value:

```sh
cat hiera/local1.yaml| shyaml get-value domain
cat hiera/local1.yaml| shyaml get-value sources.apt
cat hiera/local1.yaml| shyaml sources.platform.apt.basic
```

## Deprecated

## yaml-cli (Warning: Last commit 2016)

<https://github.com/pandastrike/yaml-cli>

## Validate / lint yaml

### yamllint

- [Docs](https://yamllint.readthedocs.io/en/stable/index.html)
- [Megalinter integration](https://megalinter.io/latest/descriptors/yaml_yamllint/)
  - Megalinter looks for a `.yamllint.yml` config file
- [pre-commit hook](https://yamllint.readthedocs.io/en/stable/integration.html#integration-with-pre-commit)

Install:

```sh
apt install yamllint
```

Usage:

```sh
yamllint hiera/local1.yaml
```

### v8r

> A command-line JSON, YAML and TOML validator that's on your wavelength v8r is
> a command-line validator that uses Schema Store to detect a suitable schema
> for your input files based on the filename.

- [GitHub](https://github.com/chris48s/v8r)
- Javascript
- [Docs](https://chris48s.github.io/v8r/)
  - [Configuration](https://chris48s.github.io/v8r/configuration/)
- [Megalinter integration](https://megalinter.io/latest/descriptors/json_v8r/)
- No way to exclude files from Megalinter runs ?

### Other linters

or

```sh
cat hiera/local1.yaml| shyaml get-values

cat .gitlab-ci.yml| ysh
```

## Format/fix yaml

```sh
prettier l.yml
prettier --write l.yml
```

## Sort yaml

<https://github.com/ddebin/yaml-sort>

```sh
npm install -g yaml-sort
```

## Yaml formaters

- [prettier](../../coding/quality/linting.md)
- [yamlfmt](https://github.com/google/yamlfmt)
  - [yamlfmt's recent slow development](https://github.com/google/yamlfmt/discussions/149)
- [yamlfix](https://github.com/lyz-code/yamlfix)
  - Project is in
    [Pull Request maintenance only](https://github.com/lyz-code/yamlfix/issues/272)
  - Bug:
    [Link with anchor gets split in comment](https://github.com/lyz-code/yamlfix/issues/70)
  - Doesn't [Crop long lines](https://github.com/lyz-code/yamlfix/issues/4)
  - Bug: Re-joins multiline string values even when resulting line is longer
    than the `YAMLFIX_LINE_LENGTH`

## Merging

<https://stackoverflow.com/questions/66694238/merging-two-yaml-documents-while-concatenating-arrays>

## Convert json to yaml

[Convert json to yaml](https://lzone.de/blog/Convert-JSON-to-YAML-in-Linux-bash)

```sh
yq eval -j resume.json > resume.yaml
```
