# yq

Beware: There are two `yq` tools, one in golang, one in python.
See below.

## yq (go)

- mikefarah/yq
- [GitHub](https://mikefarah.gitbook.io/yq)
- [Docs](https://mikefarah.gitbook.io/yq)
- [Debian RFP](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=908117)

## Installation

Get latest binary from GH:

```sh
wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 \
  -O /usr/local/bin/yq && chmod +x /usr/local/bin/yq
```

Brew:

```sh
brew install yq
```

or with snap (Be aware: <https://github.com/mikefarah/yq/#snap-notes>):

```sh
sudo snap install yq
```

Arch:

```sh
sudo pacman -S go-yq
```

## Usage

### Parse / read

Read single value:

```sh
yq '.internal.example.org' borgbackup_passphrase
yq '.internal.example.org' borgbackup_passphrase | ansible-vault decrypt
```

Read multiple values:

```sh
yq .
```

Match key in list:

```sh
gcloud beta billing accounts get-iam-policy 0E5... | \
  yq eval '.bindings[] | select(.role == "roles/billing.admin")'
```

Iterate over map and show language and files extensions [converted to csv](https://stackoverflow.com/a/70230843):

```sh
curl --no-progress-meter \
  https://raw.githubusercontent.com/github-linguist/linguist/refs/heads/main/lib/linguist/languages.yml\
  | yq 'to_entries | .[] | .key + ": " + (.value.extensions | @csv)'
```

### Modify

Update inplace (write new pw to file):

```sh
yq write -i internal.example.org borgbackup_passphrase NEWPW
echo NEWPW | yq write -i internal.example.org borgbackup_passphrase -
```

[Add to list:](https://mikefarah.gitbook.io/yq/v/v4.x/operators/add):

```sh
yq eval '.a.b += ["cow"]' sample.yml
```

[Delete key](https://mikefarah.gitbook.io/yq/operators/delete):

```sh
sops -d basic-auth.yaml | yq eval 'del(.metadata.creationTimestamp)' -
```

Merge multiple files:

```sh
yq ea '. as $item ireduce ({}; . * $item )' file1.yml file2.yml
```

Sort keys:

```sh
yq ea 'sort_keys(..)' helm-values-template.yml
```

### Create from scratch

[Creating yaml from scratch with multiple objects](https://mikefarah.gitbook.io/yq/operators/create-collect-into-object#creating-yaml-from-scratch-with-multiple-objects):

```console
 $ yq --null-input '(.secret.a = "a") | (.secret.b = "b")'
secret:
  a: a
  b: b
```

### Issues

- [Updates to one key will reformat/indent others](https://github.com/mikefarah/yq/issues/171)
- [List updates are wrongly indented](https://github.com/mikefarah/yq/issues/25)

## yq-python

- [GitHub](https://github.com/kislyuk/yq)
- [Docs](https://kislyuk.github.io/yq/)
- Debian package: `yq`

Converts yaml to json and uses `jq` for parsing.

- Can process `yaml`, `toml` **and** `xml`.
- Con only parse, not modify yaml

### Install

```sh
apt install yq
```

### kislyuk/yq Usage

Get single value:

```sh
yq .foo.bar input.yml
```

Parse whole yaml file and output YAML:

```sh
yq -y .services.kubelet.extra_args < /var/lib/OpenAppStack/rke/cluster.yml
```

### kislyuk/yq issues

- [literal blocks or folded blocks are replaced with a quoted multiline string](https://github.com/kislyuk/yq/issues/36)
