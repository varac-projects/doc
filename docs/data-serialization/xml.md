# XML

- [Creating XML Documents with Python](https://pymotw.com/2/xml/etree/ElementTree/create.html)

## CLI parsers

### python-yq/xq

- [GitHub](https://github.com/kislyuk/yq)
- [xq docs](https://kislyuk.github.io/yq/#xq)
- Depends on [jq](https://jqlang.github.io/jq/download/)
- Best XML parser so far
- Downside: Arch package `yq` conflicts with Arch package `go-yq`
  - Solution: Install locally in Python venv, or globally with `pipx install yq`

#### Usage

List all recently used files:

```sh
xq -r '.xbel.bookmark[]."@href"' ~/.local/share/recently-used.xbel
```

### sibprogrammer/xq

- [Github](https://github.com/sibprogrammer/xq)
- [AUR package](https://aur.archlinux.org/packages/xq)

Usage:

```sh
xq -x /user/@status

curl https://storage.googleapis.com/versatiles/ | xq
curl https://storage.googleapis.com/versatiles/ | xq -x '/ListBucketResult/Contents/Key'
```

### xmlstarlet

- [Website](https://xmlstar.sourceforge.net/doc/UG/xmlstarlet-ug.html)
- [Last commit](https://sourceforge.net/p/xmlstar/code/commit_browser): 2018
- Includes `xmllint`

```sh
sudo pacman -S xmlstarlet
```
