# Data-serialization tools

- [Comparison of data-serialization formats ](https://en.wikipedia.org/wiki/Comparison_of_data-serialization_formats)

## Dasel

<https://github.com/TomWright/dasel>

> Dasel (short for data-selector) allows you to query and modify data structures
> using selector strings. Comparable to jq / yq, but supports JSON, YAML, TOML,
> XML and CSV with zero runtime dependencies.

Install from github releases

Issues:

- No [Delete command](https://github.com/TomWright/dasel/discussions/64)
- [FR: Support HCL files](https://github.com/TomWright/dasel/issues/98)
