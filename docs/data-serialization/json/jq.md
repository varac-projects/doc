# jq

- [Website / Docs](https://jqlang.github.io/jq/)
- [Github](https://github.com/jqlang/jq)
- [Alternative jq implementations](https://github.com/jqlang/jq/wiki/Alternative-jq-implementations)

## Install

- Arch: `sudo pacman -S jq`
- Debian: `sudo apt install jq`

## Usage

- [jqplay.org](https://jqplay.org/): A playground for jq

### Parse / extract data

Suppress quotes from output (raw output):

```sh
jq -r '.ip_address' < ~/projects/json/aardwolf.json
```

List all sha:

```sh
jq '.[] | { sha }' < $JSON
```

List all keys:

<https://michaelheap.com/extract-keys-using-jq/>

```sh
jq '.paths | to_entries[].key' < ~/projects/work/greenhost/api-help.json
```

List all extension data:

```sh
export JSON=~/.mozilla/firefox/default/addons.json
jq '.addons[].id' < $JSON
jq '.addons[] | { name: .name, id: .id, type: .type }' < $JSON


grep 'extensions.xpiState' ~/.mozilla/firefox/default/prefs.js | \
  sed 's/^user_pref("extensions.xpiState", "//' | sed 's/");$//' | \
  sed 's/\\//g' | jq .
```

[Special characters (i.e. `dash`) in keys](https://github.com/jqlang/jq/issues/1224):

```sh
echo '{"p-name": "Dr Nic"}' | jq -r '.["p-name"]'
curl -s --netrc echo.oas.varac.net -L | jq '.request.headers["x-real-ip"]'
```

Filter objects based on the contents of a value:

```sh
jq '.letsencrypt.Certificates[].domain | select(.main | contains("www"))' < letsencrypt/acme.json
ip -j route list | jq '.[] | select(.protocol=="unspec")'
```

Filter all results which have not the status of `IGNORED` or `PASSED`:

```sh
jq '.results[] | select([.status] | inside(["IGNORED", "PASSED"]) | not)'
```

Remove key containng a value:

```sh
jq '.| del(.letsencrypt.Certificates[] | select(.domain.main == "www.moewe-altonah.de"))' letsencrypt/acme.json | \
  sponge letsencrypt/acme.json`
```

### Modify data

```sh
jq '.Statement[0].Resource = ["arn:aws:s3:::restic-zancas"]' \
  ~/projects/cloud/storage/minio/policy.readwrite.json
```

## Examples

### jq with prometheus

Alert count:

```sh
jq '.data.alerts | length' < ~/projects/monitoring/prometheus/example-alerts.json
```

Successful ?

```sh
jq 'contains({status: "success"})' < example-alerts.json
```

Filter out the `none` serverity alerts:

```sh
jq -c '.data.alerts[] | select(.labels.severity | contains("none") | not)' < example-alerts.json | jq .
```

and count them:

```sh
jq -c '[.data.alerts[] | select(.labels.severity | contains("none") | not )] | length' < example-alerts.json
```

## ndjson

`jq` can parse
[ndjson](https://en.wikipedia.org/wiki/JSON_streaming#Newline-delimited_JSON)

```sh
jq . < /tmp/renovate-log.ndjson
```

Compact lines (but wrap):

```sh
jq --compact-output . < /tmp/renovate-log.ndjson
```
