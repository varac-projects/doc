# Json

- [Working with JSON in Various Shells](https://blog.kellybrazil.com/2022/05/19/working-with-json-in-various-shells/)

## Tools

Use `~/projects/json/github-array.json` as example json

```sh
export JSON=~/projects/json/github-array.json
```

### jq

See [jq.md](./jq.md)

### jc

[Github](https://github.com/kellyjonbrazil/jc):

> CLI tool and python library that converts the output of popular command-line tools,
> file-types, and common strings to JSON, YAML, or Dictionaries.
> This allows piping of output to tools like jq and simplifying automation scripts

Install:

```sh
sudo pacman -S jc
```

Usage:

```sh
dig example.com | jc --dig
```

Convert INI file to json:

```sh
cat ~/.config/git/config | jc --ini
```

### dasel

- [GitHub](https://github.com/TomWright/dasel)
- [Docs](https://daseldocs.tomwright.me/)

```sh
dasel -f $JSON '.[0].author.id
```

From `stdin`:

```sh
echo '{ "name": "d" } ' | dasel -f - -r json '.name'
```

### gron

```sh
gron "https://api.github.com/repos/tomnomnom/gron/commits?per_page=1" | fgrep "commit.author"
gron "https://api.github.com/repos/tomnomnom/gron/commits?per_page=1" | fgrep "commit.author" | ungron

cat ~/leap/git/bitmask/provider.json | grep -v '//'| gron | grep contacts | ungron
```

### jgrep

<http://jgrep.org/>

not available via .deb, only via gem:

```sh
gem install jgrep

curl -s ipinfo.io | jgrep -s ip
```

### python

```sh
 echo '{"first_key": "value", "second_key": "value2"}' | \
   python -c 'import sys,json; print json.dumps({sys.argv[1]:json.load(sys.stdin)[sys.argv[1]]})' first_key
```

### json.sh

<https://github.com/dominictarr/JSON.sh>

### oq

<https://blacksmoke16.github.io/oq/>
<https://github.com/Blacksmoke16/oq>

> A performant, and portable jq wrapper to facilitate the consumption and output
> of formats other than JSON; using jq filters to transform the data.

## Linting json

### check-jsonschema

- [Github](https://github.com/python-jsonschema/check-jsonschema)
- [Docs](https://check-jsonschema.readthedocs.io/en/latest/)

#### Buildin schemas

- [Builtin schemas](https://check-jsonschema.readthedocs.io/en/latest/usage.html#builtin-schema-choices)
- see also [src/check_jsonschema/builtin_schemas](https://github.com/python-jsonschema/check-jsonschema/tree/main/src/check_jsonschema/builtin_schemas)
- The '--builtin-schema' flag supports the following schema names:
  - vendor.azure-pipelines
  - vendor.bamboo-spec
  - vendor.bitbucket-pipelines
  - vendor.buildkite
  - vendor.circle-ci
  - vendor.cloudbuild
  - vendor.dependabot
  - vendor.drone-ci
  - vendor.github-actions
  - vendor.github-workflows
  - vendor.gitlab-ci
  - vendor.readthedocs
  - vendor.renovate
  - vendor.taskfile
  - vendor.travis
  - vendor.woodpecker-ci
  - custom.github-workflows-require-timeout

Here the most interesting:

- `gitlab-ci` - Validate GitLab CI config against the schema provided by SchemaStore
- `github-actions`
- `github-workflows`
- `readthedocs`
- `renovate`

#### Install

```sh
sudo pacman -S check-jsonschema
```

#### cli usage

[Docs: CLI usage](https://check-jsonschema.readthedocs.io/en/latest/usage.html)

Builtin schema:

```sh
check-jsonschema --builtin-schema vendor.gitlab-ci .gitlab-ci.yml
```

Schema URL:

```sh
check-jsonschema -v \
  --schemafile https://raw.githubusercontent.com/goss-org/goss/master/docs/schema.yaml \
  goss.yaml
```

##### remote schemas

#### pre-commit usage

[Usage in pre-commit](https://check-jsonschema.readthedocs.io/en/latest/precommit_usage.html):

[.pre-commit-hooks.yaml](https://github.com/python-jsonschema/check-jsonschema/blob/main/.pre-commit-hooks.yaml)

```sh
repos:
  - repo: https://github.com/python-jsonschema/check-jsonschema
    rev: 0.23.2
    hooks:
      - id: check-gitlab-ci
```

### Different `jsonlint` linters

- [zaach/jsonlint](https://github.com/zaach/jsonlint): javascript, supported
  by null-ls
  `brew install jsonlint`
- [python3-demjson](https://github.com/dmeranda/demjson): Unmaintained, last commit 2015
- [Seldaek/jsonlint](https://github.com/Seldaek/jsonlint): php, unmaintained

## Json definitions / extensions

- [jsonc](https://github.com/komkom/jsonc): json like config with comments
- [hjson](https://hjson.github.io/)
  "Hjson is a syntax extension to JSON. It's NOT a proposal to replace JSON or
  to incorporate it into the JSON spec itself. It's intended to be used like a
  user interface for humans, to read and edit before passing the JSON data to the
  machine."
- [json5](https://json5.org/): JSON5 is an extension to the popular JSON file
  format that aims to be easier to write and maintain by hand
  (e.g. for config files).
  - [jq: json5 / hjson support?](https://github.com/jqlang/jq/issues/1607)
- [jsonl](https://jsonlines.org/): Newline delimited JSON
- [ndjson](http://ndjson.org/) Newline Delimited JSON

### Convert jsonc to json

```sh
sudo pacman -S python-json5

cat ~/.config/waybar/config.jsonc | pyjson5 --as-json | jq
```

### Convert jsonl to json

jsonl -> json:

```sh
jq -s '.' input.jsonl > output.json
```

json -> jsonl:

```sh
jq -c '.[]' input.json > output.jsonl
```

### Produce tables from json

see `./tables.md`
