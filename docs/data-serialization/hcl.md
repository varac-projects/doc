# HCL

- [hcledit](https://github.com/minamijoyo/hcledit)
  - AUR package: `hcledit`

Usage:

```sh
hcledit block list -f main.tofu
hcledit block get terraform -f main.tofu
hcledit attribute get terraform.required_version -f main.tofu
```

## Other tools

- [hcl](https://github.com/alecthomas/hcl)
- [hashicorp/hcldec](https://github.com/hashicorp/hcl/tree/main/cmd/hcldec)
  - Converts HCL to JSON
- [hcl2json](https://github.com/tmccombs/hcl2json)
- [python-hcl2](https://github.com/amplify-education/python-hcl2)
  - AUR package: `python-hcl2`
  - Python lib only, no cli tool
- [pyhcl](https://github.com/virtuald/pyhcl)
  - Does not support HCL2 (which Terraform or Opentofu use)
  - Last commit 2023
  - AUR package: `python-pyhcl`
