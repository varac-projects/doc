# CSV

See also [etc/tables.md](../etc/tables.md) how to convert CSV to tables

## CSV cli tools

- [miler comparism table](https://www.ict4g.net/adolfo/notes/data-analysis/tools-to-manipulate-csv.html)
  - Compares miller to datamash, xsv, csvkit, pivot, q

### csvkit

> A suite of utilities for converting to and working with CSV, the king of tabular file formats.

- [csvkit documentation](https://csvkit.readthedocs.io)
- [GitHub](https://github.com/wireservice/csvkit)

Install:

```sh
pamac install csvkit
```

Usage:

Convert csv to json:

```sh
csvjson data.csv > data.json
```

### Unmaintained

- [q](https://github.com/harelba/q)
  - Run SQL directly on CSV or TSV files
  - Last commit 2023-12
- [datamash](https://www.gnu.org/software/datamash/)
  - Last commit 2023-12
- [pivot](https://github.com/jgrahamc/pivot)
  - Last commit 2017
- [xsv](https://github.com/burntsushi/xsv)
  - A fast CSV command line toolkit written in Rust.
  - Last commit 2021
