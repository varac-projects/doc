# Image manipulation

## Make background transparent

Overview of different options: [Set transparent background](https://stackoverflow.com/questions/9155377/set-transparent-background-using-imagemagick-and-commandline-prompt)

Best solution so far: [transparent-background](https://github.com/plemeri/transparent-background):

```sh
cd ~/projects/images/transparent-background
transparent-background --source /tmp/orig.jpg --dest /tmp/orig-transparent.jpg
```

Using Imagemagick:

```sh
    convert -threshold 75% -fuzz 10% -transparent white orig.png orig-transparent.png
```

## Make transparent background white

```sh
    convert -background white 'Unterkonstruktion-Techn. Zeichnung.svg' Unterkonstruktion-Techn.-Zeichnung.png
```

## Imagemagick etc

### Convert svg to png

Try one of these:

```sh
    rsvg-convert /tmp/mermaid-diagram-20191130145632.svg > mermaid-diagram-20191130145632.png
    inkscape -z -e mermaid-diagram-20191130145632.png -w 1000 -h 1000 mermaid-diagram-20191130145632.svg
    convert mermaid-diagram-20191130145632.svg mermaid-diagram-20191130145632.png
```

### Text to image (for telefone nr)

```bash
convert -size 200x20 xc:transparent -font Helvetica -pointsize 16 \
  -draw "text 0,16 'Tel: +49 12 234556'" img/phone.png
```

## Inkscape

- [How to crop in Inkscape](http://goinkscape.com/how-to-crop-an-image-in-inkscape)
