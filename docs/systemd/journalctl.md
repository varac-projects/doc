# systemd journald

- [Journalctl cheat sheet with 10+ commands to filter systemd logs](https://www.golinuxcloud.com/view-logs-using-journalctl-filter-journald/)

## Usage

Verbose mode, shows the journald field names

    journalctl -o verbose

## Flush journal

Retain only the past two days:

    journalctl --vacuum-time=2d

Retain only the past 500 MB:

    journalctl --vacuum-size=500M

## Turn on persistant storage

According to https://www.golinuxcloud.com/enable-persistent-logging-in-systemd-journald/,
it's sufficient to only create the persistent journald stroage location with

    mkdir -p /var/log/journal

Existing journal entries from `/run/log/journal` will get moved over
immediately.

journal storage location: `/var/log/journal`

    journalctl --disk-usage

Clean up:

    sudo journalctl --vacuum-size=200M

    sudo journalctl --verify

Limit max size

     mkdir /etc/systemd/journald.conf.d
     vi /etc/systemd/journald.conf.d/size.conf
        [Journal]
        SystemMaxUse=500M
        SystemMaxFileSize=50M

## Log to journal

    echo 'hello' | systemd-cat -t someapp -p emerg
