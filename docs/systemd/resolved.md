# Systemd resolved

- [Arch wiki](https://wiki.archlinux.org/title/systemd-resolved)

## Usage

resolved status:

```sh
systemctl is-active systemd-resolved
systemctl status systemd-resolved
```

Current config/setup:

```sh
resolvectl dns
resolvectl domain
resolvectl status

resolvectl statistics
```

## Add DNS servers

```sh
vi /etc/systemd/resolved.conf
systemctl restart systemd-resolved
```

## Migrate to systemd-resolved

```sh
apt purge resolvconf openresolv
apt install systemd-resolved
systemctl enable systemd-resolved
systemctl start  systemd-resolved
systemctl status systemd-resolved
```

During installation of `systemd-resolved`, `/etc/resolv.conf` is converted
to a symlink to systemd's `/run/systemd/resolve/stub-resolv.conf`.
If `/etc/resolv.conf` is _not_ pointing to systemd's `stub-resolv.conf`:

```sh
ln -rsf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

## Flush DNS cache

```sh
resolvectl flush-caches
```

Query DNS:

```sh
resolvectl query ix.de
```

## Debug resolved

[Add a --verbose or --debug flag to resolvectl](https://github.com/systemd/systemd/issues/17330)

```sh
sudo systemctl stop systemd-resolved.service
sudo script -c 'SYSTEMD_LOG_LEVEL=debug /lib/systemd/systemd-resolved' /tmp/resolved.log
```

and on another terminal:

```sh
systemd-resolve ix.de
```

Or temporarily change the LOG_LEVEL of the running `systemd-resolved`:

```sh
LOGLEVEL=$(resolvectl log-level) && resolvectl log-level debug
resolvectl query example.org
resolvectl log-level ${LOGLEVEL}
```

## Which global DNS server is configured ?

There is still no [resolvectl status --json support](https://github.com/systemd/systemd/issues/33036):

```sh
resolvectl status |grep "  Current DNS Server:" | sed "s/.*: //"
```

## Force global DNS config

[How to use the same DNS for all connections](https://andrea.corbellini.name/2020/04/28/ubuntu-global-dns/)

Force global `resolved` config, i.e. to **always** encrypt DNS traffic for all
domains except those pushed by `NetworkManager`.

```sh
$ cat /etc/systemd/resolved.conf.d/dns-over-tls.conf
[Resolve]
# https://wiki.archlinux.org/title/systemd-resolved#DNS_over_TLS
# https://medium.com/@jawadalkassim/enable-dns-over-tls-in-linux-using-systemd-b03e44448c1c
DNS=1.1.1.1 1.0.0.1
FallbackDNS=8.8.8.8 8.8.4.4
Domains=~.
DNSOverTLS=opportunistic

$ systemctl daemon-reload
$ systemctl restart systemd-resolved.service
```

Now prevent [NetworkManager](https://wiki.archlinux.org/title/NetworkManager)
from pushing DNS options to `systemd-resolved`:

```sh
$ cat /etc/NetworkManager/conf.d/global-dns.conf
# https://andrea.corbellini.name/2020/04/28/ubuntu-global-dns/
[main]
# do not use the dhcp-provided dns servers, but rather use the global
# ones specified in /etc/systemd/resolved.conf
dns=none
systemd-resolved=false

$ systemctl restart NetworkManager.service
```

## Split DNS

- [Split DNS with systemd](https://www.bertrandbc.com/blog/split-dns-with-systemd/)
- [Understanding systemd-resolved, Split DNS, and VPN Configuration](https://blogs.gnome.org/mcatanzaro/2020/12/17/understanding-systemd-resolved-split-dns-and-vpn-configuration/)

## Issues

### resolved and /etc/network/interfaces

When `/etc/network/interfaces` is used instead of `networkd`,
resolved wrongly adds `DOMAINS` to the `search` attribute in `/etc/resolv.conf`:

```sh
$ cat /etc/resolv.conf
nameserver 127.0.0.53
options edns0 trust-ad
search DOMAINS
```

- [if-up.d/resolved: Fix hook to properly handle nameserver and search domains](https://salsa.debian.org/debian/ifupdown/-/merge_requests/20)
  Solution:

- Manually patching `/etc/network/if-up.d/resolved` with the patch from above MR
  (until `ifupdown` `v0.8.42` is released) solved it.

- or migrate to `networkd`

### No DNS servers configured

- Is `systemd-networkd` configured and enabeld ?
