# Systemd units

## Service units

Start service manually:

    systemctl --user start daily.service
    systemctl --user status daily.service

User units:

    systemctl --user status
    systemctl status user@1000 -l

## List units

List all failed user services:

    systemctl --user --no-legend --state=failed --plain list-units

### Instance units from template files

<https://www.digitalocean.com/community/tutorials/understanding-systemd-units-and-unit-files#creating-instance-units-from-template-unit-files>

* [Remove failed and wrong instance units](https://superuser.com/a/936976)

    `systemctl reset-failed`

### Service dependencies

<https://www.digitalocean.com/community/tutorials/systemd-essentials-working-with-services-units-and-the-journal#inspecting-units-and-unit-files>

To see the dependency tree of a unit (which units systemd will attempt to activate when starting the unit):

    systemctl list-dependencies NetworkManager-wait-online.service

This will show the dependent units, with target units recursively expanded.
To expand all dependent units recursively, pass the --all flag:

    systemctl list-dependencies --all NetworkManager-wait-online.service

To show the reverse dependency, i.e. which service depends on `NetworkManager-wait-online.service`:

    systemctl list-dependencies --reverse NetworkManager-wait-online.service

## Service watchdog

<https://www.medo64.com/2019/01/systemd-watchdog-for-any-service>

## Timers

<https://niels.kobschaetzki.net/blog/2015/11/11/creating-systemd-timers-instead-of-a-personal-crontab/>

    cd ~/.config/systemd/user
    vi test.timer
    vi test.service

    systemctl --user enable test.timer
    systemctl --user start  test.timer
    systemctl --user list-timers

## Changing user timers/services

    systemctl --user daemon-reload

## Time formats

    man systemd.time

## Unit overrides/drop-ins

Show all overrides system-wide:

    systemd-delta --type=extended

Show concatenated unit propoerties:

    systemctl cat autorandr
    systemctl show autorandr

### Add override/drop-in

#### Using systemctl edit

Use `systemctl edit` to automatically create a drop-in file:

    systemctl edit libvirtd.service

Then add i.e.

    [Unit]
    After=network-online.target

This will create `/etc/systemd/system/libvirtd.service.d/override.conf)`
with the same content, and reloads the systemd daemon to activate the changes.

#### Manually

Using the example of <https://github.com/prometheus-community/node-exporter-textfile-collector-scripts/issues/97#issuecomment-851957644>

    cd /etc/systemd/system
    mkdir prometheus-node-exporter-smartmon.service.d
    echo -e '[Service]\nEnvironment=LC_NUMERIC=C' > prometheus-node-exporter-smartmon.service.d/lc_numeric.conf
    systemctl daemon-reload
    systemctl status prometheus-node-exporter-smartmon
    systemctl restart prometheus-node-exporter-smartmon

<https://github.com/prometheus-community/node-exporter-textfile-collector-scripts/issues/97#issuecomment-851957644>

## Debug failing unit

    systemctl list-units --failed

## Start service when there's an actual network connection

Unfortunatly all targets seems to be very static,
I can't figure out how to do this dynamically on a Laptop
with ever changing connectivity.

<https://unix.stackexchange.com/questions/126009/cause-a-script-to-execute-after-networking-has-started>

## Remove systemd services

    systemctl stop [servicename]
    systemctl disable [servicename]
    rm /etc/systemd/system/[servicename]
    rm /etc/systemd/system/[servicename] # and symlinks that might be related
    rm /usr/lib/systemd/system/[servicename]
    rm /usr/lib/systemd/system/[servicename] # and symlinks that might be related
    systemctl daemon-reload
    systemctl reset-failed
