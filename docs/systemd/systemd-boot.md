# systemd-boot (sd-boot / gummiboot)

- [Arch linux doc](https://wiki.archlinux.org/title/systemd-boot)
- [Arch Linux – How to migrate from grub to systemd-boot in less than 2 minutes.](https://peterconfidential.com/grub-to-systemd-boot/)
- [Distro with systemd-boot by default](https://www.reddit.com/r/FindMeADistro/comments/10hrzdm/a_distro_with_systemdboot_by_default/)
- [Arch wiki: dm-crypt - encrypt hook - Using systemd-cryptsetup-generator](https://wiki.archlinux.org/title/dm-crypt/System_configuration#Using_systemd-cryptsetup-generator)

Install sd-boot:

    $ bootctl install
    Created "/efi/EFI/systemd".
    Created "/efi/loader".
    Created "/efi/loader/entries".
    Created "/efi/EFI/Linux".
    Copied "/usr/lib/systemd/boot/efi/systemd-bootx64.efi" to "/efi/EFI/systemd/systemd-bootx64.efi".
    Copied "/usr/lib/systemd/boot/efi/systemd-bootx64.efi" to "/efi/EFI/BOOT/BOOTX64.EFI".
    Random seed file /efi/loader/random-seed successfully written (32 bytes).
    Created EFI boot entry "Linux Boot Manager".

## Change default boot entry

Edit `/efi/loader/loader.conf`

## Automated kernel image management and boot loader entries

### kernel-install-for-dracut

- [kernel-install-for-dracut](https://gitlab.com/dalto.8/kernel-install-for-dracut)
- [kernel-install-for-dracut AUR package](https://aur.archlinux.org/packages/kernel-install-for-dracut):

### Kernelstub

> Kernelstub is a utility to automatically manage your OS's EFI System Partition
> (ESP). It makes it simple to copy the current kernel and initramfs image onto
> the ESP so that they are automatically probable by most EFI boot loaders as
> well as the EFI firmware itself.

- [Github](https://github.com/isantop/kernelstub)
- [What is Kernelstub](https://support.system76.com/articles/kernelstub/)
- Developed by System76, packaged in Pop-Os deb repo
- Not packaged in Arch, neither in AUR
- Sample template: `/etc/default/kernelstub.SAMPLE`

Copy current kernel and inintd to EFI partition:

    kernelstub -v

#### Install older kernel as default

    kernelstub -v -k /boot/vmlinuz-5.19.16-76051916-generic -i /boot/initrd.img-5.19.16-76051916-generic

Then copy the older kernel to i.e. `*-previous`:

    cp vmlinuz.efi vmlinuz-previous.efi
    cp initrd.img initrd.img-previous

Then override the default kernel + initrd entries:

    kernelstub -v

## Dracut

From [Arch wiki: Dracut](https://wiki.archlinux.org/title/Dracut);

> dracut creates an initial image used by the kernel for preloading the
> block device modules (such as IDE, SCSI or RAID) which are needed
> to access the root filesystem. Upon installing linux,
> you can choose between mkinitcpio and dracut.
> dracut is used by Fedora, RHEL, Gentoo, and Debian, among others.
> Arch uses mkinitcpio by default.

- [Dracut Github](https://github.com/dracutdevs/dracut)
  - [Dracut wiki](https://github.com/dracutdevs/dracut/wiki/)
- [Dracut man page](https://man.archlinux.org/man/dracut.8)
- [Dracut kernel cmdline man page](https://man7.org/linux/man-pages/man7/dracut.cmdline.7.html)

Manually create intiramfs images (not needed when using `kernel-install-for-dracut`):

    dracut --hostonly --no-hostonly-cmdline /efi/initramfs-6.1-x86_64-dracut.img
    dracut /efi/initramfs-6.1-x86_64-dracut-fallback.img

## Migrate from grub/mkinitcpio to sd-boot/dracut

see `./systemd-boot-migration.md`
