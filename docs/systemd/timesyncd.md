# systemd-timesyncd

<https://wiki.ubuntuusers.de/systemd/timesyncd/>

## Migrate to systemd-timesyncd

Purge old ntp implementations:

    sudo apt purge ntp sntp chrony openntpd

Install `systemd-timesyncd` which is its own package
since Debian bookworm:

    sudo apt install systemd-timesyncd

Check if timesyncd is already running and enabled:

    systemctl status systemd-timesyncd

If not, enable and start it:

    sudo systemctl enable systemd-timesyncd
    sudo systemctl start  systemd-timesyncd
    sudo systemctl status systemd-timesyncd

## Usage

    timedatectl status
