# systemd-analyse

https://wiki.archlinux.org/index.php/Improving_performance/Boot_process
https://wiki.ubuntuusers.de/systemd/systemd-analyze/

## Overview

    systemd-analyze
    systemd-analyze blame

Show blocking tree of daemons:

    systemd-analyze critical-chain

Graphs:

    systemd-analyze plot > /tmp/plot.svg
    eog /tmp/plot.svg

    systemd-analyze dot | dot -Tpng -o /tmp/systemd-dot.png


## Debug service

    systemctl status systemd-cryptsetup@sda_crypt.service
    systemctl list-units --state=failed


    systemctl --user list-dependencies nagstamon.service
