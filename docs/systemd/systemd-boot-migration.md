# Migrate from grub/mkinitcpio to sd-boot/dracut

Migration (2023-05) tested on:

* Manjaro-Sway 22.1.1, Iso from 2023-05-15
* Installed with `Hibernate` support and encrypted disk
* linux 6.1.26-1

## EFI system partition (ESP)

* [EFI system partition](https://wiki.archlinux.org/title/EFI_system_partition)

Mount ESP partiotion to `/efi`:

    umount /boot/efi
    mkdir /efi
    sed -i 's|boot/efi|efi|' /etc/fstab
    systemctl daemon-reload
    mount /efi

## Dracut configuration

Remove Grub and mkinitcpio:

    pacman -R grub mkinitcpio mkinitcpio-openswap

Install dracut and kernel-install-for-dracut:

    pacman -S dracut
    pamac install kernel-install-for-dracut

Configure kernel parameters in `/etc/kernel/cmdline`, read by
`kernel-install-for-dracut` when creating the initramfs.
Note: In order to unlock both swap and root partitions I needed to list both in
the kernel parameters.

    SWAP_UUID=$(blkid --label swap | sed 's/.*luks-//')
    ROOT_UUID=$(blkid |grep root | cut -d'"' -f 2)

Carfully double-check if those values match those from the current kernel
`cmdline`:

    cat /proc/cmdline|grep $ROOT_UUID
    cat /proc/cmdline|grep $SWAP_UUID

Then configure the new kernel cmdline used by dracut:

    cat /proc/cmdline > /etc/kernel/cmdline.grub.old
    echo "rd.luks.uuid=${ROOT_UUID} root=/dev/mapper/luks-${ROOT_UUID} apparmor=1 security=apparmor rd.luks.uuid=${SWAP_UUID} resume=/dev/mapper/luks-${SWAP_UUID} udev.log_priority=3" > /etc/kernel/cmdline
*Beware* compare the new cmdline with the old one so you don't miss anything
important (i.e. a `rootflags=subvol=@` for a btrfs root fs):

    diff /etc/kernel/cmdline.grub.old /etc/kernel/cmdline

Note: Dracut also reads `/etc/kernel/cmdline_fb` in case you need a different kernel
cmdline for your fallback entries.

Create dracut config:

    $ cat /etc/dracut.conf.d/custom.conf
    omit_dracutmodules+=" brltty cifs connman dash fcoe fcoe-uefi mksh busybox dbus-broker rngd multipath nbd network network-wicked nfs dmraid tpm2-tss iscsi nvmf biosdevname memstrack squash "
    add_dracutmodules+=" resume "
    # https://linuxreviews.org/Comparison_of_Compression_Algorithms
    # lzip: 23MB (slow), xz: 24MB, zstd: 25MB, gzip (default): 27MB
    compress="zstd"
    # enable automatic resume from swap

    $ echo "add_device+=\" /dev/disk/by-uuid/${SWAP_UUID} \"" >> /etc/dracut.conf.d/custom.conf

Crypttab and crypto_keyfile.bin: `/etc/crypttab` says root and swap partitions
shouldn't be included, so just comment them out.

Re-install current kernel to trigger auto-generation of loader entries:

    pacman -S linux61

## systemd-boot

* [Bootloader entries spec](https://uapi-group.org/specifications/specs/boot_loader_specification/#type-1-boot-loader-specification-entries)

Install sd-boot:

    bootctl install

Add the machine-id UID from `/efi/loader/entries/` as default to `/efi/loader/loader.conf`:

    MACHINE_ID=$(hostnamectl | grep 'Machine ID' | sed 's/.*: //')
    echo -e "default ${MACHINE_ID}*\ntimeout 5" > /efi/loader/loader.conf

Enable `systemd-boot-update.service` to auto-update systemd-boot in the EFI partition
after the next boot:

    systemctl enable systemd-boot-update.service

As a safety-net, copy the old (mkinitcpio) initramfs to the ESP and create a
manual loader entry in case the new setup doesn't boot as expected:

    mkdir /efi/backup
    cp /boot/initramfs-6.1-x86_64* /boot/intel-ucode.img /boot/vmlinuz-6.1-x86_64 /efi/backup/

Adjust `uuid`s and kernel versions:

    $ cat /efi/loader/entries/backup.conf
    title      Manjaro Linux Old initramfs (mkinitcpio)
    version    6.1.29-1-MANJARO
    options    rd.luks.uuid=4786099e-89f0-4055-aae3-77290c6dd9af root=/dev/mapper/luks-4786099e-89f0-4055-aae3-77290c6dd9af apparmor=1 security=apparmor rd.luks.uuid=be4d7aca-5382-44c6-b69e-6d279bff02bf resume=/dev/mapper/luks-be4d7aca-5382-44c6-b69e-6d279bff02bf udev.log_priority=3 systemd.machine_id=eac562054ed7484ab4637f1f9f7351a6
    linux      /backup/vmlinuz-6.1-x86_64
    initrd     /backup/initramfs-6.1-x86_64-fallback.img

Verify:

    bootctl is-installed
    bootctl list

Then: `reboot` !

It's worth testing the whole setup further by installing another kernel version.

But beware, Manjaro-Sway (and I guess Manjaro itself as well) still create an
EFI partition which is only `300 MB` big, which can hold max. 3 Kernel versions.
[Endeavour-OS](https://endeavouros.com/) defaults to a `1 GB` ESP partiotion.
It worth considering growing the ESP partition to prevent running out of space.
**Please check beforehand if you have enough space on your ESP (approx. 100 MB) !**

    pacman -S linux62

## Cleanup

Clenup unneeded entries in `/boot`:

    mkdir /boot/old
    mv /boot/grub /boot/initramfs* /boot/linux* /boot/vmlinuz* /boot/old
    rmdir /boot/efi
    pacman -R memtest86+ memtest86+-efi


Manjaro does install /crypto_keyfile.bin in the initramfs with
mkinitcpio which can automatically unlock *both* root and swap
luks devices. This is a potential security issue and
[here](https://forum.manjaro.org/t/why-does-the-installer-add-a-paswordless-key-to-luks-encrypted-partition/76551)
is the corresponding Manjaro forum thread.

Therefore let's remove the luks keyslots and later the keyfile itself:

    cryptsetup luksKillSlot /dev/nvme0n1p2 1 # root
    cryptsetup luksKillSlot /dev/nvme0n1p3 1 # swap

    rm /crypto_keyfile.bin

Clenup unneeded entries in `/efi/EFI`:

    mv /efi/EFI/boot /efi/EFI/Manjaro /boot/old

Remove `/efi/backup` and `/efi/loader/entries/backup.conf`:

    rm -rf /efi/backup /efi/loader/entries/backup.config

Remove old kernels (**careful !**):

    pacman -R linux61

## Appendix: Reference system for migration

* Endeavour-OS 3.56-1
* kernel 6.2.8-arch1-1
* Comes with sd-boot and dracut pre-installed

Packages installed:

    $ pacman -Q|grep -E '(dracut|grub|^linux|systemd|^efi)'
    dracut 056-2
    efibootmgr 18-2
    efitools 1.9.2-5
    efivar 38-3
    kernel-install-for-dracut 1.7-5
    linux 6.2.8.arch1-1
    linux-api-headers 6.1.9-1
    linux-firmware 20230210.bf4115c-1
    linux-firmware-whence 20230210.bf4115c-1
    linux-headers 6.2.8.arch1-1
    systemd 253.1-3
    systemd-libs 253.1-3
    systemd-sysvcompat 253.1-3

Content of ESP partition:

    $ find /efi
    /efi
    /efi/EFI
    /efi/EFI/systemd
    /efi/EFI/systemd/systemd-bootx64.efi
    /efi/EFI/BOOT
    /efi/EFI/BOOT/BOOTX64.EFI
    /efi/EFI/Linux
    /efi/loader
    /efi/loader/entries
    /efi/loader/entries/b9f1e342946d4521854ed8980492ae77-6.2.8-arch1-1.conf
    /efi/loader/entries/b9f1e342946d4521854ed8980492ae77-6.2.8-arch1-1-fallback.conf
    /efi/loader/loader.conf
    /efi/loader/random-seed
    /efi/loader/entries.srel
    /efi/b9f1e342946d4521854ed8980492ae77
    /efi/b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1
    /efi/b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/initrd
    /efi/b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/initrd-fallback
    /efi/b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/linux

Loader entries:

    $ cat /efi/loader/entries/b9f1e342946d4521854ed8980492ae77-6.2.8-arch1-1*
    title      EndeavourOS
    version    6.2.8-arch1-1
    machine-id b9f1e342946d4521854ed8980492ae77
    sort-key   endeavouros-6.2.8-arch1-1
    options    nvme_load=YES nowatchdog rw rd.luks.uuid=7f2e54a3-c8a6-48ad-87c0-27e181c90227 root=/dev/mapper/luks-7f2e54a3-c8a6-48ad-87c0-27e181c90227 rd.luks.uuid=8dfcba63-1b96-4d38-a808-a5ca2ec82761 resume=/dev/mapper/luks-8dfcba63-1b96-4d38-a808-a5ca2ec82761 systemd.machine_id=b9f1e342946d4521854ed8980492ae77
    linux      /b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/linux
    initrd     /b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/initrd

    title      EndeavourOS
    version    6.2.8-arch1-1-fallback
    machine-id b9f1e342946d4521854ed8980492ae77
    sort-key   endeavouros-6.2.8-arch1-1-fallback
    options    nvme_load=YES nowatchdog rw rd.luks.uuid=7f2e54a3-c8a6-48ad-87c0-27e181c90227 root=/dev/mapper/luks-7f2e54a3-c8a6-48ad-87c0-27e181c90227 rd.luks.uuid=8dfcba63-1b96-4d38-a808-a5ca2ec82761 resume=/dev/mapper/luks-8dfcba63-1b96-4d38-a808-a5ca2ec82761 systemd.machine_id=b9f1e342946d4521854ed8980492ae77
    linux      /b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/linux
    initrd     /b9f1e342946d4521854ed8980492ae77/6.2.8-arch1-1/initrd-fallback

Dracut config:

    cat /etc/dracut.conf.d/*
    # force installing /etc/crypttab even if hostonly="no"
    install_items+=" /etc/crypttab "
    # enable automatic resume from swap
    add_device+=" /dev/disk/by-uuid/8dfcba63-1b96-4d38-a808-a5ca2ec82761 "
    omit_dracutmodules+=" network cifs nfs brltty "
    compress="zstd"
    add_dracutmodules+=" resume "
