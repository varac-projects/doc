# Systemd-networkd

[Debian wiki: Systemd-netword](https://wiki.debian.org/SystemdNetworkd)

Show inferface status:

```sh
networkctl status -a
```

## Migrate from /etc/network/interfaces to systemd-networkd

```sh
sudo echo "# Disabled because of systemd-networkd" > /etc/network/interfaces
vi /etc/systemd/network/20-wired.network
```

DHCP example config:

```sh
[Match]
Name=enxb827eb54e69f

[Network]
DHCP=ipv4
Domains=local.example.org example.org
```

Static IP config:

```sh
[Match]
Name=eth0

[Network]
Address=10.27.64.131/29
Gateway=10.27.64.130
DNS=10.43.0.10
DNS=195.190.28.82
DNS=195.190.28.85
Domains=local.example.org example.org
```

Cleanup:

```sh
systemctl enable systemd-networkd
systemctl start systemd-networkd
systemctl status systemd-networkd
sudo apt purge dhcpcd5 isc-dhcp-client isc-dhcp-common network-manager
```
