# Hostnamectl

Show current hostname settings:

    hostnamectl

Permanently set a new hostname:

    hostnamectl set-hostname mediaplayer
