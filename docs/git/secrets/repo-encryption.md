# Git repo encryption

[Securely storing secrets in Git: transcrypt, git-crypt, git-secret, and SOPS](https://medium.com/@slimm609/securely-storing-secrets-in-git-542771d3ed8c)

## transcrypt

* [Github](https://github.com/elasticdog/transcrypt)
* Not Debian packaged

## git-secret

* [Website](https://git-secret.io/)
* [Github](https://github.com/sobolevn/git-secret)
* Debian packaged

## git-remote-gcrypt

* [Website](https://spwhitton.name/tech/code/git-remote-gcrypt/)
* [Github](https://github.com/spwhitton/git-remote-gcrypt)
* Still actively maintained
* Debian packaged
* Encrypts the *whole* repo, instead of single files like git-crypt

[Known issues](https://github.com/spwhitton/git-remote-gcrypt#known-issues):

`gcrypt.require-explicit-force-push`

    A longstanding bug is that every git push effectively has a --force.
    If this flag is set to true, git-remote-gcrypt will refuse to push, unless
    `--force` is passed, or refspecs are prefixed with `+`.

## git-crypt

* [Github](https://github.com/AGWA/git-crypt)
* Debian packaged
* Mixed plain/encrypted mode,
  specific files need to get whitelisted for encryption in `.gitattributes`.

Main issue:

All used keys must be trusted by (l)-signing them !
[Improve usability/documentation with untrusted GPG keys](https://github.com/AGWA/git-crypt/issues/23)

### Install

    apt install git-crypt

### Init

    git-crypt init
    # add Varac's key
    git-crypt add-gpg-user 0x54...
    ...

### Add files

    echo 'sshkeys/gitlab-runner_ssh filter=git-crypt diff=git-crypt' >> .gitattributes
    git add .gitattributes
    git check-attr -a sshkeys/gitlab-runner_ssh

Make sure to unlock repo before adding files:

    git add sshkeys/gitlab-runner_ssh
    git commit
    git-crypt status -e

Verify:

    git-crypt lock
    strings sshkeys/gitlab-runner_ssh
    git-crypt unlock

    git push
