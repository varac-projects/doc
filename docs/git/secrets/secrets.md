# Git secrets

Best practices:

* <https://departmentfortransport.github.io/ds-processes/Coding_standards/secrets.html>

## Scanning for secrets

* talisman
* git-secrets
* <https://github.com/zricethezav/gitleaks>

See `../pre-commit.md` for details
