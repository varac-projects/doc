# Reasing a branch which is based on another branch that got rebased

http://voidcanvas.com/how-to-rebase-a-branch-when-the-parent-is-rebased-with-another/

      git rebase --onto <new_commit_on_which_i_want_to_rebase> <old_commit_of_parent_from_which_i_rebased> <name_of _my_branch>
      git rebase --onto 38b4b02 032d9d6 improve_ci_further

or, if i already checked out the branch to rebase:

      git rebase --onto <new_commit_on_which_i_want_to_rebase> <old_commit_of_parent_from_which_i_rebased>
