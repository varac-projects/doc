# Git credentials helpers

- [Git book: Credential Storage](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)
- [Storing Git Credentials with Git Credential Helper](https://techexpertise.medium.com/storing-git-credentials-with-git-credential-helper-33d22a6b5ce7)

Cache daemon running in background:

```sh
git credential-cache--daemon /home/varac/.cache/git/credential/socket
```

## git-credential-oauth

- [GitHub](https://github.com/hickford/git-credential-oauth)
- Golang
- [How it works](https://github.com/hickford/git-credential-oauth?tab=readme-ov-file#how-it-works)

Install:

```sh
pamac install git-credential-oauth
```

Configure:

```sh
git config --global --unset-all credential.helper
git config --global --add credential.helper "cache --timeout 21600" # six hours
git config --global --add credential.helper oauth
```

### Custom Gitlab host

```sh
export GITLAB_URL=https://0xacab.org
git config --global credential.${GITLAB_URL}.oauthClientId $(gopass show --password mnt/ndr/token/0xacab.org/varac/application/git-credential-oauth/id
git config --global credential.${GITLAB_URL}.oauthScopes read_repository write_repository
git config --global credential.${GITLAB_URL}.oauthAuthURL /oauth/authorize
git config --global credential.${GITLAB_URL}.oauthTokenURL /oauth/token
```

### Test/Debug

```sh
export GIT_TRACE=1
echo url=https://gitlab.com | git credential fill
```

### Configure custom Gitlab hosts

[Custom GitLab instance support](https://github.com/hickford/git-credential-oauth/issues/18)

## git-credential-manager

- [GitHub](https://github.com/git-ecosystem/git-credential-manager)
- dot.net
- [AUR packages](https://aur.archlinux.org/packages?O=0&K=git-credential-manager)
  - [git-credential-manager](https://aur.archlinux.org/packages/git-credential-manager)
    - Installs ~460 MB dot.net dependencies
  - [git-credential-manager-core](https://aur.archlinux.org/packages/git-credential-manager-core)
    - Installs ~525 MB dot.net dependencies

## Gopasspw git-credential-gopass

- [Website](https://github.com/gopasspw/git-credential-gopass)
- **Warning: [Credentials might end up in the repo to be cloned](https://github.com/gopasspw/git-credential-gopass/issues/19)**

Install:

```sh
pamac install git-credential-gopass
git help -a | grep gopass
```

Setup:

```sh
git config --global credential.helper gopass
```

Setup for specific store/mount:

```sh
git config --global credential.helper "gopass --store=work"
```
