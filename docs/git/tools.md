# Tools around git

## lazygit

- [GitHub](https://github.com/jesseduffield/lazygit)
- [Docs](https://github.com/jesseduffield/lazygit/tree/master/docs)
  - [User config](https://github.com/jesseduffield/lazygit/blob/master/docs/Config.md)
  - [Custom Command Keybindings](https://github.com/jesseduffield/lazygit/blob/master/docs/Custom_Command_Keybindings.md)
- Config: `~/.config/lazygit/config.yml`

## gitui

- [gitui](https://github.com/extrawurst/gitui)
- Rust, faster than lazygit

Issues:

- [Panic when starting from subfolder](https://github.com/extrawurst/gitui/issues/2316)
  - experienced with 0.26.3-2

## degit

- <https://github.com/Rich-Harris/degit>

> When you run degit `some-user/some-repo`, it will find the latest commit
> on `https://github.com/some-user/some-repo` and download the associated tar file
> to `~/.degit/some-user/some-repo/commithash.tar.gz`
> if it doesn't already exist locally
