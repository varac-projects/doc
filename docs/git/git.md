# Git

## Config

### Global config

Anzeigen:

```sh
git config  --global --get-regexp user.*
```

Setzen:

```sh
git config --global user.name Varac
git config --global user.email varac@varac.net

git config --global init.defaultBranch main

git config --global log.date local
```

### Lokal config

```sh
git config --get user.name; git config --get user.email

git config user.name "varac"
git config user.email "varac@varac.net"
```

## Usage

### Commit messages, log

```sh
git log --follow -p file
```

Show log for specific remote branches:

```sh
git lg --remotes="origin/debian/*"
```

`--remotes` or `--branches` expect a glob, it no glob is found `/*` is used.
In order to see only one particular branch we need to fake a glob like this:

```sh
git lg --remotes="origin/debian/experimenta[l]-0.9"
```

Filter out Renovate Bot commits:

```sh
git log --author='^(?!Stackspin-renovate|Jon).*$' --perl-regexp
```

### Search git history for string

[Searching through history code](https://subscription.packtpub.com/book/programming/9781782168454/1/ch01lvl1sec16/searching-through-history-code)

```sh
git log -G"isOutdated" --oneline
```

### Change committer in git history

<http://stackoverflow.com/questions/750172/how-do-i-change-the-author-of-a-commit-in-git>

```sh
git filter-branch -f --env-filter "GIT_AUTHOR_NAME='varac'; GIT_AUTHOR_EMAIL='me@example.org';
  GIT_COMMITTER_NAME='varac'; GIT_COMMITTER_EMAIL='me@example.org';" HEAD
```

### Commiter im remote repository ändern

```sh
git filter-branch --env-filter \
  'if [ "$GIT_AUTHOR_EMAIL" = "me@example.org" ]; then \
  export GIT_AUTHOR_EMAIL="varac@varac.net"; fi'
rm -r .git/refs/original
git filter-branch --env-filter 'if [ "$GIT_AUTHOR_NAME" = "Varac" ]; then \
  export GIT_AUTHOR_NAME="nadir"; fi'
rm -r .git/refs/original
git filter-branch --env-filter 'if [ "$GIT_COMMITTER_NAME" = "Varac" ]; then \
  export GIT_COMMITTER_NAME="nadir"; fi'
rm -r .git/refs/original
git filter-branch --env-filter \
  'if [ "$GIT_COMMITTER_EMAIL" = "me@example.org" ]; then \
  export GIT_COMMITTER_EMAIL="varac@varac.net"; fi'

git push --force
```

## Remotes

### Filesystem remote

```sh
git clone file:////<host>/<share>/<path>
git clone file:////main/code
git clone file:////192.168.10.51/code
git clone ssh://puppet-old/etc/puppet
```

### Push to remote

```sh
git commit scripts/drupal-db-backup -m"+ drush views-export"
git push # zum origin:master repo

git checkout -b bitrigger-drupal-scripts bitrigger-drupal6/scripts
git branch -a

* bitrigger-drupal-scripts
  master
  bitrigger-drupal6/bitrigger-drupal6/master
  origin/devel
  origin/master

git cherry-pick 53036d4
git push bitrigger-drupal6 bitrigger-drupal-scripts:scripts
```

### Create new Reopository on server

```sh
cd /var/git
mkdir scripts-private.git
cd scripts-private.git
git --bare init
cd ..
chown git:git -R scripts-private.git/
```

On Client:

```sh
git clone git@bitrigger.de:scripts-private.git
```

### Remote Merge

```sh
git clone git@gitorious.org:puppet-modules/puppet-shorewall.git
cd puppet-shorewall/
git remote add immerda git://git.puppet.immerda.ch/module-shorewall.git
git remote -v
git remote update
git fetch immerda master
git log -p HEAD..FETCH_HEAD
git merge immerda/master
git push
```

### Fork

```sh
git remote rm origin
git remote add upstream git://git.puppet.immerda.ch/module-shorewall.git
git remote add origin git@gitorious.org:puppet-modules/puppet-shorewall.git
git push origin master
```

### Remote Pull/Merge

Use "their" diffs while merging:

```sh
git pull -s recursive -X theirs
```

### Choose different ssh identity for cloning

<https://superuser.com/a/912281>

Initial cloning:

```sh
GIT_SSH_COMMAND="ssh -i ~/.ssh/varac-nopw -F /dev/null" git clone git@0xacab.org:varac/varac-test.git
```

Subsequent cloning:

```sh
git config core.sshCommand "ssh -i ~/.ssh/varac-nopw -F /dev/null"
```

Show git repo root:

```sh
git rev-parse --show-toplevel
```

## Branches

### Delete a remote Branch

```sh
git push origin :0.7.0
```

or, if a tag has the same name as a branch:

```sh
git push origin :refs/heads/0.7.0
```

### Checkout and track remote branch

```sh
git checkout --track -b feature_someting  shared/master
```

### Create emtpy branch

[Create empty branch on GitHub](https://stackoverflow.com/a/34100189)

Warning !!!!! `rm -rf` deletes also files which are not listed in `.gitignore` !!!

```sh
git switch --orphan NEWBRANCH
git rm --cached -r .
rm -rf .
ls -la
git status
git commit --allow-empty -m"inital commit"
```

## Etc

- View unpushed commits: `git log origin/master..HEAD`

### Git Merge

- <http://stackoverflow.com/questions/501407/is-there-a-git-merge-dry-run-option>

### Undo last commit

Generic: <http://stackoverflow.com/questions/927358/git-undo-last-commit>

```sh
git reset --soft HEAD^
```

### Git Flow

- <http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/>

### git-checkout older revision of a file

```sh
git checkout a1b4c5de file/to/restore
```

### How to tell which local branch is tracking which remote branch in Git?

```sh
git remote show origin
```

### Get current branch name

```sh
git rev-parse --abbrev-ref HEAD
```

### Diff between two branches

```sh
git branch-diff master..v0.3

git branch-diff origin/develop..develop
```

### File Diff between two branches

```sh
 git difftool master:site_config/manifests/files.pp develop:site_config/manifests/files.pp
 git difftool master:check_p2pool.rb remotes/varac_codecoop/master:check_p2pool.rb
```

### Show old version of file

```sh
git show develop:Vagrantfile
git show 41ed3c80925658047381288e0cf71228c28c2f36:Vagrantfile
```

### show commits only present in particular branch

i.e. show commits only present in `icinga2_master` branch:

```sh
git log --no-merges icinga2_master ^master
```

### "Cherry-pick" commit from different git repo

```sh
git --git-dir=../<some_other_repo>/.git format-patch -k -1 \
  --stdout <commit SHA> | git am -3 -k
```

### Delete all merged branches

```sh
git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d
```
