# Git ignore

## gitignore.io

- [gitignore.io website](https://www.toptal.com/developers/gitignore)
- [github](https://github.com/toptal/gitignore.io)
- Collection of community .gitignore files

Usage:

```sh
gi node > .gitignore
```
