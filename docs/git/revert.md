# git revert

## Revert multiple commits:

https://stackoverflow.com/a/1470452

Note down latest HEAD of branch before D !

    $ git reset --hard A
    $ git reset --soft D # (or ORIG_HEAD or @{1} [previous location of HEAD]), all of which are D
    $ git commit
