# Gitlab

## Current, open issues

- [Exempt specific files from CODEOWNERS rules](https://gitlab.com/gitlab-org/gitlab/-/issues/41914)

## Create a project from cmd line

[Create a project with git push](https://0xacab.org/help/user/project/index.md#create-a-project-with-git-push):

```sh
git push --set-upstream git@0xacab.org:varac-projects/ulauncher-rbw.git main
```

## Add GIF to comment

[Expand external gifs on MR descriptions and comments](https://gitlab.com/gitlab-org/gitlab/-/issues/440250#note_1755569495):

```markdown
![gif](https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExNHl2cWtsdG1iNjk3bDVnem83angybTMydDdybGxlN2tkd2Z6YjN2ZyZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/3orieS4jfHJaKwkeli/giphy.gif)
```
