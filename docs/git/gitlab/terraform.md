# Gitlab Terraform integration

- [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
- [Infrastructure as Code with Terraform and GitLab](https://docs.gitlab.com/ee/user/infrastructure/iac/)

## Use curl to get the state file

Create Gitlab token with `api` scope and export it:

```sh
export GITLAB_TOKEN=...
```

Export gitlab project ID and terraform state file URL:

```sh
export TF_ADDRESS="https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/default"

curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" $TF_ADDRESS
```

## Issues

### error looking up module versions: 401 Unauthorized

Put token in `~/.terraformrc` like this:

```sh
credentials "gitlab.com" {
  token = "<myAPIkey>"
}
```

## Alternatives to managing gitlab with terraform

- [GitlabForm](https://gitlabform.github.io/gitlabform/)
- [Gitlab-Project-Configurator](https://gitlab.com/grouperenault/gitlab-project-configurator)
