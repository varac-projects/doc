# glab

Official gitlab cli client

- [Gitlab project](https://gitlab.com/gitlab-org/cli)
- [Docs](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html)
- [What about Lab?](https://github.com/profclems/glab#what-about-lab)

## Install

```sh
sudo pacman -S glab
```

### Configuration

Authentication:

> Get a GitLab personal access token with `api` and `write_repository`
> scopes

[Environment variables](https://gitlab.com/gitlab-org/cli#environment-variables)

```sh
export GITLAB_HOST=https://0xacab.org/
export GITLAB_TOKEN=$(gopass show --password token/0xacab.org/glab)
```

## Usage

### Merge requests

List all open MRs in group:

```sh
glab mr list --group 123..
```

### CI

```sh
glab ci trace
```

### Access tokens

- [Overview about different MRs to handle tokens](https://gitlab.com/gitlab-org/cli/-/merge_requests/1583#note_2073824411)

### Issues

- FR: [Add search subcommand](https://gitlab.com/gitlab-org/cli/-/issues/7404)
