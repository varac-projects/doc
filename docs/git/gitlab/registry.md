# GitLab Container Registry

https://docs.gitlab.com/ee/user/packages/container_registry/

    docker login -u varac -p XXX open.greenhost.net:4567

Push image to other tag:

    docker pull open.greenhost.net:4567/stackspin/dashboard/admin-frontend:0.1.1
    docker tag open.greenhost.net:4567/stackspin/dashboard/admin-frontend:0.1.1 open.greenhost.net:4567/stackspin/dashboard/dashboard:0.1.1
    docker push open.greenhost.net:4567/stackspin/dashboard/dashboard:0.1.1
