# Auto-update dependencies

## renovate-bot

see [bots/renovate.md](./bots/renovate.md)

## Dependabot

- [Website](https://dependabot.com)
- Cons: No flux helmRelease support
