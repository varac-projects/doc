# Gitlab pages


## 0xacab.org

- [Enabling GitLab pages?](https://0xacab.org/riseup/0xacab/-/issues/49)
- [Hosting a website with Hugo on 0xacab](https://roneo.org/en/hugo-gitlab-pages-riseup/)
- [Gitlab: use public Pages while keeping the Git repo private](https://roneo.org/en/gitlab-public-pages-private-repo/)
