# Gitlab CI

## Debug mode

Include this in the job definition inside `gitlab-ci.yml`:

```yaml
variables:
  CI_DEBUG_TRACE: "true"
```

## Lint .gitlab-ci.yaml

### Local only linters

These don't use the `/api/v4/ci/lint` API endpoint

#### check-jsonschema

see `../../json.md`

### Linters that use the Gitlab API

These use the `/api/v4/ci/lint` API endpoint. One major blocker:
[CI Lint API support for passing included files in working tree](https://gitlab.com/gitlab-org/gitlab/-/issues/349696)
is still open, until this gets solved we can't use the Gitlab CI API with local
includes. See also
[pre-commit check_yaml hook fails on gitlab-ci files](https://open.greenhost.net/stackspin/stackspin/-/issues/1460#note_55517)
for more details.

#### Pre-commit-gitlabci-lint

- [GitHub](https://github.com/emmeowzing/gitlabci-lint-pre-commit-hook)
- Last commit 2024-01
- Can deal with
  [includes](https://github.com/emmeowzing/gitlabci-lint-pre-commit-hook/issues/65)
- No Arch/Mise package

Install:

```sh
pipx install pre-commit-gitlabci-lint
```

[Setup](https://github.com/emmeowzing/gitlabci-lint-pre-commit-hook?tab=readme-ov-file#setup):

1. Create an access token with api scope.
2. Set access token value in an environment variable named `GITLAB_TOKEN` or
   `GITLABCI_LINT_TOKEN`.
3. Add the projectId for your gitlab project as a command line argument, or set
   it in the config file.
4. Adjust the
   [configuration](https://github.com/emmeowzing/gitlabci-lint-pre-commit-hook?tab=readme-ov-file#configuration)
   in `~/.config/.gitlabci-lint/config.toml`

Usage:

```sh
gitlabci-lint -p <project_id>
```

[pre-commit](https://github.com/emmeowzing/gitlabci-lint-pre-commit-hook?tab=readme-ov-file#pre-commit):

```yaml
repos:
  - repo: https://github.com/bjd2385/pre-commit-gitlabci-lint
    rev: v1.4.0
    hooks:
      - id: gitlabci-lint
      # args: [-b, 'https://custom.gitlab.host.com', '-p', '12345678']
```

#### Stale / not working

- [orobardet/gitlab-ci-linter](https://gitlab.com/orobardet/gitlab-ci-linter)
  - Last commit 2023-01
  - Go
  - Can be used with pre-commit
  - Issue:
    [gitlab-ci-linter prints "KO" with no details](https://gitlab.com/orobardet/gitlab-ci-linter/-/issues/25)
- [devopshq/gitlab-ci-linter](https://gitlab.com/devopshq/gitlab-ci-linter)
  - Python
  - Works as a
    [pre-commit hook](https://gitlab.com/devopshq/gitlab-ci-linter#pre-commit-hook)
  - Last commit 2023-09, worked fine with Gitlab API \< v16, but
    [broke with v16](https://gitlab.com/devopshq/gitlab-ci-linter/-/issues/4)
- [FalcoSuessgott/lint-gitlab-ci](https://github.com/FalcoSuessgott/lint-gitlab-ci)
  - Bash
  - Works as a pre-commit hook
  - Last commit 2023-09
- [kadrach/pre-commit-gitlabci-lint](https://github.com/kadrach/pre-commit-gitlabci-lint)
  - Python
  - Last commit 2021-02
- [smop/pre-commit-hooks](https://gitlab.com/smop/pre-commit-hooks/-/blob/master/pre_commit_hooks/check-gitlab-ci)
  - Bash
  - Last commit 2019

## Gitlab "Auto Devops"

[Auto Devops gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml)

## Teraform integration

- [Terraform integration in merge requests](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)
- <https://gitlab.com/gitlab-org/terraform-images>
- <https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml>

## Golang cacheing

<https://docs.gitlab.com/ee/ci/caching/#caching-go-dependencies>

## Run CI locally

- [gitlab-ci-local](https://github.com/firecow/gitlab-ci-local)
  - Run gitlab pipelines locally as shell executor or docker executor.

## Matrix builds

- [parallel:matrix docs](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix)
- [Efficient pipelines in GitLab CI/CD: Parallel Matrix Builds + !reference](https://dnsmichi.at/2021/09/23/efficient-pipelines-gitlab-ci-cd-parallel-matrix-builds-reference/)

Issues:

- FR:
  [dependencies for matrix artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/356172)
  - [Ugly workaround](https://gitlab.com/gitlab-org/gitlab/-/issues/356172#note_1335590043)

## Run job only if artifact exists

- [Run job only if artifact exists](https://stackoverflow.com/a/68269921)

## Build container images

- [Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
- [Kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)

### Buildah

- [Buildah](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#buildah-example)

Issues:

Fails to build with `vfs` driver on kubernets executor, see
[this failed job example](https://0xacab.org/varac-projects/varac-home-assistant/-/jobs/474967)

### Multi arch builds

- Possible with Docker-in-docker, but requires privileged mode
- Also possible with buildah, but also requires privileged mode
- [Kaniko can't do multi arch builds](https://github.com/GoogleContainerTools/kaniko#creating-multi-arch-container-manifests-using-kaniko-and-manifest-tool)

#### Podman / Buildah

In order to run Buildah in a custom gitlab-runner on Kubernetes, the scheduled
runner pods need to run in privileged mode, unfortunately.

First install prerequisites (also on K8s nodes which gitlab-runner schedule pods
on):

Debian:

```sh
apt install -y podman buildah qemu-user-static
```

Arch:

```sh
sudo pacman -S qemu-user-static qemu-user-static-binfmt
```

RedHat flavors:

```sh
sudo yum install -y podman buildah qemu-user-static
```

Then you can use the `--platform` parameter in Gitlab CI:

```sh
buildah build --platform linux/arm64/v8,linux/amd64 -t multiarch:latest
```
