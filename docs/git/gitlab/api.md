# API

[Docs: Rest API](https://docs.gitlab.com/ee/api/rest/)

## Authentication

Configure private token: <https://www.safaribooksonline.com/library/view/gitlab-cookbook/9781783986842/ch06s05.html>

Export `GITLAB_TOKEN` (already exported in when entering i.e. `~/oas/openappstack/`)
and API url:

```sh
export GITLAB_TOKEN='…'
export GITLAB_API_URL="https://gitlab.com/api/v4"
export
```

Alias curl cmd:

```sh
alias GITLAB_CURL="curl -s --header \"PRIVATE-TOKEN: $GITLAB_TOKEN\""
```

### Authentication scope

Show which user the token belongs to:

```sh
GITLAB_CURL "${GITLAB_API_URL}/user" | jq .
```

Show which groups a token has access to (and find out group IDs matching group names):

```sh
curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "https://gitlab.com/api/v4/groups" | jq '.[] | .full_path, .id'
```

What scopes do I need for which action ?

- Git (pull/push): `read_repository, write_repository`

## Projects

<https://docs.gitlab.com/ce/api/projects.html>

Extensive list of projects:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects" | jq .
```

Simple list of projects:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects?simple=true" | jq .
```

Show only projects that the current user is member of:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects?simple=truei&membership=true" | jq .
GITLAB_CURL "${GITLAB_API_URL}/projects?simple=true&membership=true" | jq '.[] | { id:.id, name:.name_with_namespace}'
```

Find out project id by project name:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects?simple=true&membership=true&search=infrastructure" | jq '.[] | { id:.id, name:.name_with_namespace}'
```

## Artifacts

<https://docs.gitlab.com/ee/api/job_artifacts.html>

Get list of jobs:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/6/jobs" > /tmp/jobs.json
```

Then use job id to download the artifact zip:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/6/jobs/68090/artifacts" --output /tmp/f
```

Or download artifact zip from *latest successful* pipeline (beware, if the last
pipeline doesn't contain the job this will fail, even if an earlier successful
pipeline succeeded with this job):

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/6/jobs/artifacts/master/download?job=setup-openappstack"
```

## Jobs

<https://docs.gitlab.com/ee/api/jobs.html>

Get a log file:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/50/jobs/129011/trace"
```

[List pipeline bridges / child pipelines](https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-bridges):

```sh
$ GITLAB_CURL "${GITLAB_API_URL}/projects/6/pipelines/14147/bridges" | \
    jq '.[] | select(.name=="install-stackspin") | .downstream_pipeline.id'
14149
```

[List jobs for a pipeline](https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs)

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/6/pipelines/14149/jobs"
GITLAB_CURL "${GITLAB_API_URL}/projects/6/pipelines/14149/jobs" | jq 'map({stage: .stage, name:.name})'
GITLAB_CURL "${GITLAB_API_URL}/projects/6/pipelines/14149/jobs" | jq '.[].name'
```

## Issues

<https://docs.gitlab.com/ce/api/issues.html>

**Beware of [pagination](https://docs.gitlab.com/ee/api/rest/#pagination),
max. 100 issues can get queried per page !**

Get all issues (max. 100):

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/4593807/issues?per_page=100"
```

Get all open issues (max. 100):

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/4593807/issues?state=opened&per_page=100" > issues.json
```

## CI variables

<https://docs.gitlab.com/ee/api/project_level_variables.html>

List variables:

```sh
GITLAB_CURL "${GITLAB_API_URL}/projects/6/variables"
```

Create variable:

```sh
GITLAB_CURL --request POST "${GITLAB_API_URL}/projects/6/variables" \
  --form "key=zzztest" --form "value=new value" --form "masked=true"
```

## URLs

Get single project (id):

```sh
projects/leap%2Fleap_cli
```

i.e. leap_cli: 241

Paginate:

```sh
0xacab.org/api/v3/projects?per_page=1
```

show builds for project

```sh
0xacab.org/api/v3/projects/129/builds?per_page=1
```

## Cleaning up

<https://docs.gitlab.com/ee/api/branches.html#delete-merged-branches>

## Repository files API

[Repository files API](https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository)

```sh
curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" 'https://gitlab.com/api/v4/projects/50527474/repository/files/gitlab%2Fci-configs%2Frepolinter.yaml/raw?ref=ops-422-repolinter'
```

or

```sh
curl "https://gitlab.com/api/v4/projects/50527474/repository/files/gitlab%2Fci-configs%2Frepolinter.yaml/raw?ref=ops-422-repolinter&private_token=$GITLAB_TOKEN"
```

## Terraform state

```sh
glab api projects/${GITLAB_PROJECT_ID}/terraform/state/default | jq -r .terraform_version
```
