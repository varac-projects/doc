# Gitlab bots

- [danger](https://github.com/danger/danger)
- [coolbeevip/gitlab-bot](https://github.com/coolbeevip/gitlab-bot)
  A Gitlab Review Bot Assistant which utilizes webhooks to automate certain tasks
- [renovate](./renovate.md)
