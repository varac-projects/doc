# renovate-bot

- [Github](https://github.com/renovatebot/renovate)
- [Installing and onboarding Renovate into repositories](https://docs.renovatebot.com/getting-started/installing-onboarding/)
- [Support](https://github.com/renovatebot/renovate/discussions)

## Gitlab

- [GitLab platform module](https://docs.renovatebot.com/modules/platform/gitlab)
- [Automated Dependency Updates for Gitlabci Include](https://docs.renovatebot.com/modules/manager/gitlabci-include/)
- [Automated dependency updates with Renovate Bot integrated into Gitlab CI](https://violoncello.ch/blog/2019/11/automated-dependency-updates-with-renovate-bot-integrated-into-gitlab-ci/)

### Renovate Gitlab howto

Follow the steps of the [Official gitlab-runner template](https://gitlab.com/renovate-bot/renovate-runner)

- Create a new project to host the runner
- Create a dedicated gitlab user (i.e. [varac-renovate](https://0xacab.org/varac-renovate))
  - As the new user, create a [GitLab Personal Access Token](https://docs.renovatebot.com/modules/platform/gitlab/#authentication)
    (scopes: `read_user`, `api` and `write_repository`),
    named i.d. `RENOVATE_TOKEN`
  - Add the new Gitlab PAT as CI variable `RENOVATE_TOKEN`
    to the new project
    (Environments: All, Visibility: Masked, Not protected, not expanded)
- Create a [Github token](https://docs.renovatebot.com/modules/platform/github/#authentication)
  with the `repo` scope and add it to the renovate user's CI/CD variables.
  "Fine-grained Personal Access Tokens do not support the GitHub GraphQL API
  and cannot be used with Renovate."
  This is important otherwise renovate might be rate-limited and MRs will
  fail.
  - Add the new GitHub token as a CI variable `GITHUB_COM_TOKEN`
    to the new project
    (Environments: All, Visibility: Masked, Not protected, not expanded)
- Add the new user as member to all projects/groups that should get renovates
  - Role: [Developer (to create MRs)](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions)
- Create a new main pipeline that includes this project's template
- Set up a schedule to run the pipeline regularly owned by the new renovate user

### Use renovate docker image locally

```sh
docker run --rm -it renovate/renovate bash
  export RENOVATE_REQUIRE_CONFIG=false RENOVATE_PLATFORM=gitlab \
    RENOVATE_ENDPOINT=https://open.greenhost.net/api/v4 \
    RENOVATE_LOG_FILE_LEVEL=debug RENOVATE_TOKEN=XXXX
  renovate --dry-run true --autodiscover=true --autodiscover-filter='stackspin/!(admin-frontend|admin-backend|user-panel|single-sign-on|local-path-provisioner|gitlab-k8s-executor-management|nextcloud)'
```

## Logging / Debug

Search debug log for errors:

```sh
grep -Ei '(erorr|denied|fatal|warn)' /tmp/renovate-log.ndjson | grep -Eiv '(warnings\":\[\]| 0 errors )
```

## Update flux2 helmReleases

<https://docs.renovatebot.com/modules/manager/flux/>

## Disable renovate per repo

Edit `renovate.json` and add `{ "enabled": false }`

## Datasources

### Helm

- [Helm datasource](https://docs.renovatebot.com/modules/datasource/helm/)
  - [helm datasource now failing with couldn't get index.yaml file](https://github.com/renovatebot/renovate/issues/16249)

### Repology

- [Repology datasource](https://docs.renovatebot.com/modules/datasource/repology/)

Issues with repology:

- The example in [Repology docs](https://docs.renovatebot.com/modules/datasource/repology/)
  show `musl-dev` but [Repology can't find `musl-dev`](https://repology.org/projects/?search=musl-dev)
  Same for `python3-dev` which is the reason this [MR](https://0xacab.org/varac-projects/varac-home-assistant/-/merge_requests/118)
  [build job fails](https://0xacab.org/varac-projects/varac-home-assistant/-/jobs/448509)

## Debug/Test configuration options

- [Github discussion: How do we debug/test configuration options](https://github.com/renovatebot/renovate/discussions/14215)
  - From this [commit](https://github.com/renovatebot/renovate/discussions/14215#discussioncomment-7485198):

> For me running `npx renovate --platform=local` helped me check
> configuration issues (wrongs keys) before going through the steps running the
> pipeline on GitLab.

### [Local platform](https://docs.renovatebot.com/modules/platform/local/)

> With the "local" platform you can perform dry runs of Renovate
> against the local file system. This can be handy when testing
> a new Renovate configuration for example.
