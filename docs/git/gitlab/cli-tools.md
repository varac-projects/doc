# Gitlab CLI clients

## glab

Official gitlab cli client

see [glab.md](./glab.md)

## python-gitlab

- [github](https://github.com/python-gitlab/python-gitlab)

- [docs](https://python-gitlab.readthedocs.io)

- 1.7k ⭐

- Ubuntu package lacks behind as expected!

  ln -s /usr/bin/python-gitlab ~/bin/gitlab

or:

```sh
pipx install 'python-gitlab[autocompletion]'
```

### CLI usage

<https://python-gitlab.readthedocs.io/en/stable/cli-objects.html>

Config: `~/.python-gitlab.cfg`

Show current user:

```sh
gitlab -g open.greenhost.net current-user get
```

Show project:

```sh
gitlab -g open.greenhost.net project get --id 6
```

Download latest artifacts:

```sh
gitlab -g open.greenhost.net project artifacts --id 6 --ref-name 555_prom_alerts --job setup-openappstack
```

List all master pipeline ids:

```sh
gitlab -g open.greenhost.net project-pipeline list --project-id 6 --ref master
```

Snipetts:

```sh
gitlab snippet list

e="$(cat bin/nextcloud_deck_export.py)"
gitlab snippet create --title nextcloud_deck_export.py --file-name nextcloud_deck_export.py \
  --visibility public --content "$e"
```

## lab

- [Website](https://zaquestion.github.io/lab/)
- [lab](https://github.com/zaquestion/lab)
- 1k ⭐
- golang

config at: `/home/varac/.config/lab.hcl`

### Usage

Show _all_ merged MRs: `lab mr list -s merged -a` Pretty-print MR list:
`lab mr list -s merged -a | awk '{first = $1; $1=""; print "*" $0, "(" first ")"}'`

## narkoz/gitlab

- [Website](https://narkoz.github.io/gitlab/)
- [GitHub](https://github.com/narkoz/gitlab)
- Ruby wrapper and CLI for the GitLab REST API

Usage:

```sh
gitlab projects '{per_page: 70}' | grep platf

gitlab merge_requests 128162 --json

gitlab merge_request 128162 230544 --json

gitlab create_merge_request 128195 \
  "Don't remove nfs client tools on vagrant" \
  "{source_branch: 'dont_remove_nfs_client_on_vagrant', \
    target_branch: 'develop', target_project_id: 128162}"
```

## Other clients

### Unmaintained cli clients (as of 2021-03)

- <https://gitlab.com/shank-utils/gitlab-cli> (last commit 2016-03)
- <https://github.com/lighttiger2505/lab> Last commit 2019-04

## Recursively clone all projects in group

- [gitlabber](https://github.com/ezbz/gitlabber)
- [gitlab-clone](https://github.com/LaserPhaser/gitlab-group-clone) Couldn't get
  it working
