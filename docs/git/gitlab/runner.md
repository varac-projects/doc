# Gitlab-runner

## Install gitlab-runner

[deb repo](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/docs/install/linux-repository.md)

    curl -L https://packages.gitlab.com/runner/gitlab-runner/gpgkey \
      2> /dev/null | sudo tee /etc/apt/trusted.gpg.d/gitlab.asc
    sudo echo -e "# 2021-01 no groovy repo yet \n" \
      "deb https://packages.gitlab.com/runner/gitlab-runner/ubuntu/ focal main" \
      > /etc/apt/sources.list.d/gitlab-runner.list
    sudo apt install gitlab-runner

Not recommended:

    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh \
      | sudo bash
    sudo apt install gitlab-runner

### Register

    gitlab-runner register

### Unregister

    gitlab-runner unregister --url=https://0xacab.org \
      --name="rpi-docker-runner" --token=XXX

### Run gitlab-runner as docker service

[docker](https://gitlab.com/gitlab-org/gitlab-runner/blob/master/docs/install/docker.md)

    docker run -d --name gitlab-runner --restart always \
      -v /home/varac/etc/gitlab-runner-varac:/etc/gitlab-runner gitlab/gitlab-runner:latest

### Register runner as docker service

    docker exec -it gitlab-runner gitlab-runner register

### Run in debug mode

    /usr/bin/gitlab-runner --debug run --working-directory /home/gitlab-runner \
      --config /etc/gitlab-runner/config.toml --service gitlab-runner \
      --user gitlab-runner

## Use docker caching locally

### Using cache for multiple stages

[Not possible atm](https://gitlab.com/gitlab-org/gitlab-runner/issues/2409)

## leap_platform

### Catalog test

    gitlab-runner exec docker --cache-dir=/cache/leap_platform --docker-volumes /tmp/gitlab-runner:/cache/leap_platform setup
    gitlab-runner exec docker --cache-dir=/cache/leap_platform --docker-volumes /tmp/gitlab-runner:/cache/leap_platform catalog

### Deploy test

    gitlab-runner exec docker --env AWS_ACCESS_KEY=$AWS_ACCESS_KEY --env AWS_SECRET_KEY=$AWS_SECRET_KEY \
      --env SSH_PRIVATE_KEY="$(cat ~/leap/git/pwstore/sshkeys/gitlab-runner_ssh)" \
      --cache-dir=/var/cache/gitlab-runner --docker-volumes ~/docker/cache/gitlab-runner:/var/cache/gitlab-runner deploy_test

### Benchmark

## Puppet Modules testing

    gitlab-runner exec docker --cache-dir=/var/cache/gitlab-runner \
      --docker-volumes ~/docker/cache/gitlab-runner/puppet-modules/apt:/var/cache/gitlab-runner rspec
