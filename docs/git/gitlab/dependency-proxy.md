# Gitlab dependency proxy

https://open.greenhost.net/help/user/packages/dependency_proxy/index

## Delete cache

https://open.greenhost.net/help/api/dependency_proxy.md#purge-the-dependency-proxy-for-a-group

    curl --request DELETE --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_API_URL/groups/8/dependency_proxy/cache"
