# Gitlab Kubernetes agent

## Register agent

```bash
docker run --pull=always --rm \
    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
    --agent-token=... \
    --kas-address=wss://0xacab.org/-/kubernetes-agent/ \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent | kubectl apply -f -
```

## Install

* <https://0xacab.org/help/user/clusters/agent/install/index>

Rough overview:

* Install k8s cluster
* Add custom flux repo
* Install agent chart
* Create kubernetes-agent-setup project with agent config

### Helm chart

* [GitLab agent Helm chart](https://artifacthub.io/packages/helm/gitlab/gitlab-agent)
* [gitlab agent helm chart source](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/build/deployment/gitlab-agent-chart)
