# Gitlab issues

## dotenv variables

- [Read and Import CI/CD Variables from .env file when pipeline executes](https://gitlab.com/gitlab-org/gitlab/-/issues/432120)
- [glenv](https://github.com/sn3d/glenv)
  - With direnv it will automatically export
    env variables directly from GitLab

## Merge requests

- [Allow to filters Merge Request by groups](https://gitlab.com/gitlab-org/gitlab/-/issues/18150)
- [Search on Merge Request dashboard for groups should allow to filter on projects and subgroup](https://gitlab.com/gitlab-org/gitlab/-/issues/355828)
