# Github

## Authentication

### Access token

#### Fine-grained personal access tokens

Required scopes:

- Git (read/write):
  - Repository access: All
  - Repository permissions:
    - Commit statuses: Read-only
    - Contents: Read and Write

#### Personal access tokens (classic)

Required scopes:

- Git (read/write): `repo, workflow`

## cli tool

<https://cli.github.com/>
<https://cli.github.com/manual/>

Setup:

```sh
sudo snap install gh && sudo snap connect gh:ssh-keys
```

Create personal access token (<https://github.com/settings/tokens/new>) with
the following scopes:

- repo\* (required)
- read:org (required)
- read:public_key
- read:repo_hook
- gist
- notifications
- read:user
- user:email
- write:discussion
- read:gpg_key

[Authenticate](https://cli.github.com/manual/gh_auth_login):

```sh
gh auth login
```

[Configure](https://cli.github.com/manual/gh_config):

```sh
gh config set git_protocol ssh

gh completion -s zsh > .zsh/completion/_gh
```

Usage:

```sh
gh gist list
```

## API

<https://developer.github.com/v3>

### Examples

Show all repos the token has access to in a organisation:

```sh
curl -s -H "Authorization: token $(gopass cad/www/github.com/dashboard-token)" \
  "https://api.github.com/orgs/leapcode/repos" | jq '.[].name'
```

Show user which the particular token authenticated with:

```sh
curl -s -H "Authorization: token $(gopass cad/www/github.com/dashboard-token)" "https://api.github.com/user"
```

### Fetch issues

```sh
curl -s -H 'Authorization: token 123' "https://api.github.com/pixelated/project-issues/issues\?page\=1\&per_page\=100"
```

#### Use 2-factor auth

Basic auth will fail

```sh
curl --netrc "https://api.github.com/pixelated/project-issues/issues\?page\=1\&per_page\=100"
{
  "message": "Must specify two-factor authentication OTP code.",
  "documentation_url": "https://developer.github.com/v3/auth#working-with-two-factor-authentication"
}
```

Generate 2fa code and then:

```sh
curl --netrc -H 'Authorization: token 123' https://api.github.com/authorizations
```

## Download / embed images from git repos

<https://stackoverflow.com/a/4605068>

```sh
https://raw.githubusercontent.com/user/repository/branch/filename
```

i.e.

```sh
wget https://raw.githubusercontent.com/splitbrain/dokuwiki/master/lib/images/smileys/fixme.gif -O /tmp/fixme.gif
```
