# Travis CI API

https://docs.travis-ci.com/api

## Examples

Hello world:

    curl -s -H "Authorization: token $(gopass cad/www/travis-ci.com/dashboard-token)" "https://api.travis-ci.org/"
    {"hello":"world"}%

Get builds:

    curl -s -H "Authorization: token $(gopass cad/www/travis-ci.com/dashboard-token)" "https://api.travis-ci.org/builds" | jq '.[].message'

Show user the token authenticates as:

    curl -s -H "Authorization: token $(gopass cad/www/travis-ci.com/dashboard-token)" "https://api.travis-ci.org/users"
