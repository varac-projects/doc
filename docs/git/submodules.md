# Git submodules

- [Git book](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
- [Anarcats comparism table about submodules, subtree etc](https://trac.torproject.org/projects/tor/ticket/30770)
- [Never use git submodules](https://diziet.dreamwidth.org/14666.html)

## Helper tools

- Stale and unmaintained: <https://github.com/kollerma/git-submodule-tools>

## Change submodule url

### Local

edit .gitmodules, change url

```sh
git submodule sync && git submodule update --init
git add -u
git commit -m"changes submodule url for…"
git push
```

### On every other checkout dir (i.e. puppet server)

```sh
git pull
git submodule sync && git submodule update --init
```

## Delete a submodule

<https://stackoverflow.com/a/1260982>

### local

```sh
git submodule deinit <path_to_submodule>
git rm <path_to_submodule>
git commit-m "Removed submodule "
rm -rf .git/modules/<path_to_submodule>
```

## on server checkout dir

```sh
rm -rf modules/dovecot
rm -rf .git/modules/modules/dovecot
vi .git/config   # remove submodule
```
