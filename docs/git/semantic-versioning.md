# Semantic versioning

## Conventional commits

- [Website](https://www.conventionalcommits.org/)
  - [Example commit history](https://github.com/conventional-commits/conventionalcommits.org/commits/master)
- [standard-version](https://github.com/conventional-changelog/standard-version)
  Automate versioning and CHANGELOG generation
- [conventional-commits-detector](https://github.com/conventional-changelog/conventional-commits-detector)
- [Commitizen](http://commitizen.github.io/cz-cli): Commit helper which lets
  you choose between standard tags

### Linting

- [commitlint](https://github.com/conventional-changelog/commitlint)

## Changelog management

- [vandamme: Changelogs Convention](https://github.com/tech-angels/vandamme/#changelogs-convention)
- [conventional-changelog](https://github.com/conventional-changelog/conventional-changelog)
- [conventional-changelog-cli](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/conventional-changelog-cli)
- [standard-changelog/](https://github.com/conventional-changelog/conventional-changelog/tree/master/packages/standard-changelog):
  An opinionated approach to CHANGELOG generation using angular commit conventions.

### keepachangelog

- [Website](https://keepachangelog.com)
- [GitHub](https://github.com/olivierlacan/keep-a-changelog/)
- **Not** based on Conventional commits
- [keepachangelog tools](https://github.com/olivierlacan/keep-a-changelog/issues/67)
- [Denote API breaking changes](https://github.com/olivierlacan/keep-a-changelog/issues/41)

### Automatic versioning / releasing

- [https://github.com/semantic-release/semantic-release](https://github.com/semantic-release/semantic-release):
  Fully automated version management and package publishing
- [semantic-pull-requests](https://github.com/zeke/semantic-pull-requests):
  Let the robots take care of the semantic versioning

### Tools

- [pm: Project Metadata Management](https://github.com/josehbez/pm) (Stale)
- [changelogger](https://github.com/churchtools/changelogger) (PHP)

### using git log

see also [What are some good ways to manage a changelog using Git?](https://stackoverflow.com/questions/3523534/what-are-some-good-ways-to-manage-a-changelog-using-git)

```sh
git log --oneline --decorate --no-merges 0.5.0..HEAD | cut -d' ' -f2- | sed 's/^/* /'
```

old:

```sh
git log --pretty=%s --first-parent  0.2.2..HEAD | sed 's/^/* /g'
```
