# Git subtree

* [git-subtree(1)](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt)

## Tutorials

* [Git – Subtree](https://www.geeksforgeeks.org/git-subtree/)
* [Atlassian docs](https://www.atlassian.com/git/tutorials/git-subtree)

## Usage

### Dotfiles example

Initial setup:

    cd ~
    dotfiles remote add lazyvim-starter https://github.com/LazyVim/starter
    dotfiles subtree add --prefix=.config/nvim/ lazyvim-starter main --squash

Pull in changes:

    cd ~
    dotfiles fetch lazyvim-starter
    dotfiles subtree pull --prefix=.config/nvim/ lazyvim-starter main --squash
