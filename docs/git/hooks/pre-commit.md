# pre-commit

- [Website](https://pre-commit.com)
- [GitHub](https://github.com/pre-commit/pre-commit)
- > 2k commits, 13k ⭐
- [Supported hooks](https://pre-commit.com/hooks.html)
- Cached hooks: `~/.cache/pre-commit`
- Custom hooks (see below): `~/projects/git/pre-commit/pre-commit-config-varac`

Install:

```sh
sudo apt install pre-commit
```

Usage:

```sh
pre-commit sample-config > .pre-commit-config.yaml
pre-commit install
pre-commit run --all-files
pre-commit run --all-files --show-diff-on-failure
```

Skip hooks:

```sh
SKIP=terraform_fmt,terraform_validate,terraform_tflint pre-commit ...
```

## General hooks

[Good documentation including expected stdin/parameters](https://www.digitalocean.com/community/tutorials/how-to-use-git-hooks-to-automate-development-and-deployment-tasks)

Global hooks activated by:

```sh
git config --global core.hooksPath ~/.config/git/hooks
```

### Filtering files with types

[Filtering files with types](https://pre-commit.com/index.html#filtering-files-with-types)

Identify file type discovered by pre-commit:

```sh
$ identify-cli setup.py
["file", "non-executable", "python", "text"]
```

## Issues

### Global hooks / include statement

- [Ability to include additional hooks](https://github.com/pre-commit/pre-commit/issues/2337)
- [Doesn't work well together with global hooks](https://github.com/pre-commit/pre-commit/issues/1298)
- [Global hook to detect pre-commit config per repo and run if present](https://github.com/pre-commit/pre-commit/issues/2233)
- [Metahooks / nested config](https://github.com/pre-commit/pre-commit/issues/731)
- [How can I configure pre-commit globally for all projects?](https://github.com/pre-commit/pre-commit/issues/450)
  - [A few workarounds listed by @asottile](https://github.com/pre-commit/pre-commit/issues/450#issuecomment-405616722)
- [Support global git hooks in git v2.9](https://github.com/pre-commit/pre-commit/issues/381)

Workarounds:

- [Using global pre-commit hook to prevent committing unwanted code](https://medium.com/@ripoche.b/using-global-pre-commit-hook-to-prevent-committing-unwanted-code-edbbf957ad12)
- [My own hack around](https://github.com/pre-commit/pre-commit/issues/450#issuecomment-441487301)

## Integrations / plugins / shared hooks

### Terraform

- When used in pre-commit, there's no indication about the file location
  [Output directory tflint is executed from](https://github.com/antonbabenko/pre-commit-terraform/issues/161)

### Ansible

```sh
cat ~/projects/git/pre-commit/custom/ansible.yaml >> .pre-commit-config.yaml
```

### Chef

<https://github.com/mattlqx/pre-commit-ruby>

### YAML

- [Feature: Add a param to check_json and check_yaml to support jsonschema](https://github.com/pre-commit/pre-commit-hooks/issues/71)
  closed :/

#### Docker

```sh
cat ~/projects/git/pre-commit/custom/docker.yaml >> .pre-commit-config.yaml
```

#### Kubernetes

Kubeval:

```sh
cat ~/projects/git/pre-commit/custom/kubeval.yaml >> .pre-commit-config.yaml
```

- [Using k8validate](https://blog.donbowman.ca/2020/04/11/pre-commit-hook-for-kubernetes-yaml)
  doesn't work

#### Gitlab Ci Linter

See [../gitlab/ci.md](../gitlab/ci.md)

### Check for secret material

#### gitleaks

- [gitleaks](https://github.com/zricethezav/gitleaks)
- [Diskussion about what passwords to protect](https://github.com/zricethezav/gitleaks/issues/778)
- [pre-commit-hook integration](https://github.com/gitleaks/gitleaks?tab=readme-ov-file#pre-commit)

Install:

```sh
sudo pacman -S gitleaks
```

Scan git history:

```sh
gitleaks detect -v
```

Ignore/allow false-positives:

- Add a `# gitleaks:allow` at the end of the line with an allowed false-positive
- For retroactive ignores from already committed false-positives add the
  fingerprint shown by `gitleaks detect -v` to a `.gitleaksignore`, see this
  repos
  [.gitleaksignore](https://0xacab.org/varac-projects/doc/-/raw/main/.gitleaksignore?ref_type=heads)
  as example

#### talisman

<https://github.com/thoughtworks/talisman/>

If you are sure you want to ignore detected secret material:

```sh
echo 'aarwolf.yaml # ignore:filecontent' >> .talismanignore
```

Issues:

- No way to
  [Permanent ignore for scoped file patterns](https://github.com/thoughtworks/talisman/issues/122)
  - see also no
    [Option to permanently ignore a file](https://github.com/thoughtworks/talisman/issues/103)

#### git-secrets

- Stale,
  [no new release including pre-commit support](https://github.com/awslabs/git-secrets/issues/182)
- Last tag 2019
- [Example pre-commit-hooks.yml](https://github.com/awslabs/git-secrets/blob/master/.pre-commit-hooks.yaml)
- Doesn't detect a `kubeconfig` file!

### Commit message linters

- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)

#### gitlint

[Website](https://jorisroovers.github.io/gitlint/)
[Config files](https://jorisroovers.com/gitlint/latest/configuration/#config-files)

Add a default `.gitlint` file:

```sh
gitlint generate-config
```

Ignore `body-is-missing` rule:

```sh
echo '[general]\nignore=body-is-missing' > .gitlint
```

Other (noteworthy) commit message linters:

- <https://marionebl.github.io/commitlint/#/> (nodejs)

#### commitlint

<https://github.com/conventional-changelog/commitlint>

#### gitmoji

<https://gitmoji.carloscuesta.me/>
<https://hackernoon.com/using-github-as-a-team-the-holy-grail-of-commit-messages-f5b10c925d62>

## Other tools

<https://github.com/zeke/semantic-pull-requests>: Stale, last commit 2021

## pre-commit in CI

- [Pre-Commit.ci : Integrating Pre-Commit into CI/CD](https://ns-rse.github.io/posts/pre-commit-ci/index.html)

### Container images

- [kiwicom/pre-commit](https://hub.docker.com/r/kiwicom/pre-commit/tags)
- [yesolutions/docker-pre-commit](https://gitlab.com/yesolutions/docker-pre-commit/container_registry/2722511):
  Last commit 2022-02

### Gitlab CI Templates

- [yesolutions/gitlab-ci-templates: pre-commit-autofix](https://gitlab.com/yesolutions/gitlab-ci-templates/-/blob/main/templates/pre-commit-autofix.yaml)
