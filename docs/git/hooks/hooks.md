# Git hooks

- [Prettier git hook options](https://prettier.io/docs/en/precommit.html)
  - Options include `lint-staged`, `husky`, `git-format-staged`

## Hook managers

- [husky](./husky.md)
- [pre-commit](./pre-commit.md)
- [lefthook](https://github.com/evilmartians/lefthook)
  - < 1k commits, 5k ⭐

## Hook runners

- [lint-staged](https://github.com/lint-staged/lint-staged)
  - Needs a hook manager to setup, defaults to `husky`
- [git-format-staged](https://github.com/hallettj/git-format-staged)
