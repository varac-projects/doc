# Husky

Git hook management

- [GitHub](https://github.com/typicode/husky/)
- Javascript
- 32k ⭐
- [Docs](https://typicode.github.io/husky/#/)

## Usage

See example husky integration at `~/kubernetes/flux-config/.husky/post-push`

Install:

```sh
pamac install nodejs-husky
```

If `pre-commit` was configured for this repo, move the pre-commit hook:

```sh
mv .git/hooks/pre-commit .husky
```

Add a hook, i.e.

```sh
mkdir .husky/
echo "task restart:all" > .husky/post-push
```
