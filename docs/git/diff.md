# Better git diffs

- [Use diff-highlight from git package](https://veronneau.org/a-better-git-diff.html)
- [Delta, a viewer for git and diff output](https://github.com/dandavison/delta)
- [GitHub style split diffs in your terminal](https://github.com/banga/git-split-diffs)
