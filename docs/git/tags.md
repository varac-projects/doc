# Git tags

List tags:

    git tag -l

List tags together with their tagging date:

    git tag -l --format='%(refname)   %(taggerdate)'

Create a signed tag:

    git tag -s 0.2.0 -m'Release 0.2.0'

Push tag:

    git push --tags

Re-tag / move tag:

    git tag -f -s 0.2.0 -m'Release 0.2.0'

Delete tag:
