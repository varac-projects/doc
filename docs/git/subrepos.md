# Clone a repo

    git subrepo clone https://github.com/pixelated/leap_platform.git leap_platform

## Reclone

if pull doesn't work:

    git subrepo clone -f -b master https://github.com/pixelated/leap_platform.git leap_platform

## Remove a subrepo

    export SUBREPO='modules/mysql'
    git subrepo -v clean --force $SUBREPO
    git remote rm "subrepo/$SUBREPO"
    rm -rf $SUBREPO
    git add $SUBREPO
    git commit -m"Removed subrepo $SUBREPO"

## Migrate from submodules

### Remove submodules

    for dir in $(git submodule | awk '{print $2}'); do git submodule deinit $dir; done
    rm -rf modules/*
    cp .gitmodules /tmp/.gitmodules.old
    rm -rf .git/modules/ .gitmodules
    git checkout modules
    rmdir modules/*

    awk 'BEGIN { RS = "[" ; FS = "\n" } { print $1, "\"", $3 }' /tmp/.gitmodules.old | cut -d'"' -f 2,4 | sed 's/"//' | sed 's/ *url *=//g' > /tmp/submodules
    while read line; do name=${line%% *}; url=${line#* }; git subrepo clone -v $url $name; done < /tmp/submodules
