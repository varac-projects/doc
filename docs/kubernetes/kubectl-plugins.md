# Plugins

## krew

- [krew plugin manager](https://github.com/kubernetes-sigs/krew/)
- [Available plugins](https://krew.sigs.k8s.io/plugins/)

Available krew cmds:

```sh
kubectl krew
```

Show krew version:

```sh
kubectl krew version
```

Upgrade krew (next to other plugins):

```sh
kubectl krew upgrade
```

Krew plugins home: `~/.krew/`

List all plugins:

```sh
kubectl plugin list
```

or

```sh
kubectl krew list
```

## Useful plugins

### Security

- Kubesec_scan
- [kubectl-doctor](https://github.com/emirozer/kubectl-doctor)
  - scan your currently targeted k8s cluster to see if there are anomalies or
    useful action points that it can report back to you.
  - Last commit 2022-02

### Secrets

- view_secret (see `./secrets.md`)
- No Plugin:
  [kubectl-no-mask AUR package](https://aur.archlinux.org/packages/kubectl-no-mask)
  kubectl patched to skip secret mask

### cert-manager

[Cert-manager kubectl login](https://cert-manager.io/docs/usage/kubectl-plugin/)

Install:

```sh
krew install cert-manager
```

Check API: `kubectl cert-manager check api`

Status of cert:

```sh
kc -n varac get cert
kubectl cert-manager -n varac status certificate www.varac.net-tls
```

Inspect TLS secret:

```sh
kc -n varac get secret | grep tls
kubectl cert-manager -n varac inspect secret www.varac.net-tls
```

### RBAC / Privileges

- [rakkess](https://github.com/corneliusweig/rakkess) Show an access matrix for
  k8s server resources
- [who-can](https://github.com/aquasecurity/kubectl-who-can)
- `kubectl auth can-i` is a builtin kubectrl cmd

### Node management

- kubectl-node-shell Exec into node via kubectl

### Volumes

- [kubectl-df-pv](https://github.com/yashbhutwala/kubectl-df-pv)
  - A kubectl plugin to see df for persistent volumes
  - Last commit 2021-07 :/
- kubectl-unused-volumes

### Resources

- kubectl-tree
- kubectl-split-yaml Split Kubernetes YAML output into one file per resource
- [kubectl-neat](https://github.com/itaysk/kubectl-neat) Clean up Kubernetes
  yaml and json output to make it readable
  - `kubectl-neat-diff` De-clutter your kubectl diff output using `kubectl-neat`
- kubectl-convert-bin allows you to convert manifests between different API
  versions

#### ketall / get-all

View all (really all) resources.

- [Ketall](https://github.com/corneliusweig/ketall)
- [Usage](https://github.com/corneliusweig/ketall/blob/master/doc/USAGE.md)

Innstall:

```sh
kubectl krew install get-all
```

Query:

```sh
kubectl get-all | grep -i prometheus-stack
```

[Delete all matching resources](https://github.com/corneliusweig/ketall/issues/146):

```sh
    kubectl get-all | grep -i prometheus-stack > /tmp/res
    grep ' oas ' /tmp/res | cut -f1 -d' ' | xargs kubectl -n oas delete
```

### Multi-cluster

- [kubectl-mc](https://github.com/jonnylangefeld/kubectl-mc) Run kubectl
  commands against multiple clusters at once Usaage examples

Show all PVCs:

```sh
kubectl mc -r 'unifi.*(dev|qa|test)' -n victoriametrics get pvc`
```

Show all controlplane versions:

```sh
kubectl mc -r 'unifi.*' version -o json | jq '.[].serverVersion.gitVersion'
```

Show all node versions:

```sh
kubectl mc -r 'unifi.*' get node
```
