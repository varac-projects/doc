# IAM options

- [Gluu blog: keycloak vs gluu](https://www.gluu.org/blog/gluu-versus-keycloak/)

# Keycloak

https://www.keycloak.org/
https://github.com/keycloak

OpenID Connect, OAuth 2.0, and SAML.

- Cons:
  - [No U2F](https://issues.jboss.org/browse/KEYCLOAK-6558), scheduled for 5.0
  - Jira issue tracker

# Gluu

https://www.gluu.org/
https://github.com/GluuFederation/gluu-docker

-
