# kind

- Helps you run Kubernetes clusters locally
  and in CI pipelines using Docker containers as "nodes".

- CNCF certified Kubernetes installer

- [kind website](https://kind.sigs.k8s.io)

- [kind releases](https://github.com/kubernetes-sigs/kind/releases)

Install: `brew install kind`

- [Available k8s version images tags](https://hub.docker.com/r/kindest/node/tags)

Start cluster without ingress-nginx:

```sh
time kind create cluster --image=vkindest/node:1.22.4 --config=/home/varac/kubernetes/kind/cluster-config-ingress.yml
docker ps
```

Start cluster with [ingress setup](https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx):

```sh
kind_ingress_create.sh
```

Delete cluster:

```sh
kind delete cluster
```
