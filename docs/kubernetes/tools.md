# Kubernetes tools

## kubewatch

Watch k8s events and trigger Handlers

[GitHub](https://github.com/bitnami-labs/kubewatch)

- [no matrix support so far](https://github.com/bitnami-labs/kubewatch/issues/245)

## Tools to try (from containerdays 2019)

Debug

- Kubectl debug page
- attach debugger: squash
- stern for logs !!

Deploy

- discontinued: forge
- sync files with your container: ksync
- kubefwd
- okteto
- telepresence

Build & Deploy

- [garden](https://docs.garden.io/)
- skaffold / tilt (better use tilt)

## kube-no-trouble / kubent

- [GitHub](https://github.com/doitintl/kube-no-trouble)

> Easily check your clusters for use of deprecated APIs

## Pluto

- [GitHub](https://github.com/FairwindsOps/pluto)
  Help discover deprecated apiVersions in Kubernetes
- [Docs](https://pluto.docs.fairwinds.com/)

Usage:

```sh
pluto detect-api-resources -owide 2>/dev/null
```
