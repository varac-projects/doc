# Prometheus-operator helm chart

<https://hub.helm.sh/charts/stable/prometheus-operator>
<https://github.com/helm/charts/tree/master/stable/prometheus-operator>

## Uninstall

```sh
kubectl delete crd \
  prometheuses.monitoring.coreos.com \
  prometheusrules.monitoring.coreos.com \
  servicemonitors.monitoring.coreos.com \
  podmonitors.monitoring.coreos.com \
  alertmanagers.monitoring.coreos.com
```
