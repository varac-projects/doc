# Kubernetes secrets

[Kubernetes docs: Secrets](https://kubernetes.io/docs/concepts/configuration/secret)

## Secrets management tools

### sealed-secrets

- [github](https://github.com/bitnami-labs/sealed-secrets)

- [flux with sealed-secrets](https://fluxcd.io/docs/guides/sealed-secrets)

- [helm chart](https://artifacthub.io/packages/helm/bitnami-labs/sealed-secrets)

- [helm chart source](https://github.com/bitnami-labs/sealed-secrets/tree/main/helm/sealed-secrets)

- [example ca.crt to test local encryption](https://raw.githubusercontent.com/181192/local-k8s-env/master/ca.crt)

- [Tutorial](https://www.arthurkoziel.com/encrypting-k8s-secrets-with-sealed-secrets/)

- cli-tool: `brew install kubeseal`

- Sealed Secrets decrypts the secret server-side, like sops

sealed-secrets vs. sops:

- sealed-secrets doesn't bloat the encrypted file with metadata footer

### Sops

see `~/docs/security/passwords/sops.md`, also for different SOPS operators

### helm-secrets

[helm-secrets](https://github.com/jkroepke/helm-secrets)

- Helm-secrets decrypts the secret client-side
  Therefor no support for helm-secrets in flux
- Backends: `Sops` or `Hashicorp Vault`

### Hashicorp Vault

- [Vault](https://www.vaultproject.io)
- No support for helm-secrets in flux
- [vault-secrets-operator](https://github.com/ricoberger/vault-secrets-operator)

### Other tools

- [kubernetes-external-secrets](https://github.com/external-secrets/kubernetes-external-secrets)
- [kubernetes-sigs/secrets-store-csi-driver](https://github.com/kubernetes-sigs/secrets-store-csi-driver)
- [Kamus](https://github.com/Soluto/kamus): last commit 2022
- [kapicorp/tesoro](https://github.com/kapicorp/tesoro)

## Etc

Generate kubertetes secret manifest from stdin:

```sh
echo -n bar | kubectl create secret generic mysecret --dry-run=client \
  --from-file=foo=/dev/stdin -o yaml
```

Generate ssh-private-key secret from file:

```sh
kubectl create secret generic ssh-private-key --dry-run=client -o yaml \
  --from-file=id_ed25519=/home/varac/.ssh/deploy-keys/tym-flow
```

## nginx/apache basic-auth

[ingress-nginx docs: Basic auth](https://kubernetes.github.io/ingress-nginx/examples/auth/basic)

```sh
    export BA_USER=tym-flow BA_PW=$(gopass show --password basic-auth/tym-flow) \
      NAME=tym-flow-staging-basic-auth NAMESPACE=tym-flow

    htpasswd -b -n "$BA_USER" "$BA_PW" | head -1 | kubectl -n $NAMESPACE \
      create secret generic --dry-run=client --from-file=auth=/dev/stdin \
      -o yaml $NAME | yq eval 'del(.metadata.creationTimestamp)' - > ${NAME}.yaml
```

Optionally encrypt secret with sops:

```sh
    sops -e -i --encrypted-regex '^(data)$' ${NAME}.yaml
```

## Decode secrets

### Kubectl secret plugins

#### kubectl-modify-secret

- <https://github.com/rajatjindal/kubectl-modify-secret>
- Can only modify secrets stored in a cluster, not local files.

Install:

```sh
kubectl krew install modify-secret
```

- [k9s plugin for kubectl-modify-secret](https://github.com/derailed/k9s/issues/1017#issuecomment-769005253)

#### view-secret kubectl plugin

<https://github.com/elsesiy/kubectl-view-secret>

Works nicely !!

> Decodes secrets. If there's only one key in the secret, it's printed.
> If there are multiple keys in the secret, it prints the keys and exits.
> In that case, specify the data key in the secret as another argument.

Install plugin:

```sh
kubectl krew install view-secret
```

View secrets:

```sh
kubectl view-secret foo
kubectl view-secret foo key.json

kubectl view-secret -n oas prometheus-settings values.yaml
```

## Unmaintained: ksd / kubernetes-secret-decode

<https://github.com/ashleyschuett/kubernetes-secret-decode>

[PR to include in krew plugin list](https://github.com/kubernetes-sigs/krew-index/pull/173)
was closed without being merged.

I can't get it to work :/

## manually with kubectl and jq

If you have a recent jq (>= 1.6.0 is needed):

```sh
kc -n oas get secret oas-test-prometheus-promet-prometheus-scrape-confg \
  -o json | jq '.data | map_values(@base64d)'
kubectl -n oas get secret monitoring-settings -o json | \
  jq '.data | map_values(@base64d)' | jq -r '."values.yaml"'
```

or use alias:

```sh
kc -n oas get secret oas-test-prometheus-promet-prometheus-scrape-confg \
  -o json | decode_secrets
```

Using jsonpath:

```sh
kc -n oas get secret oas-test-prometheus-promet-prometheus-scrape-confg \
  -o jsonpath="{.data.additional-scrape-configs\.yaml}" | base64 -d
```

(but I can't get this to work with dashes or slashed in the name of a secret)
