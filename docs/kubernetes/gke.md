# Google kubernetes engine

<https://console.cloud.google.com>

- [Trial options](https://console.cloud.google.com/freetrial/signup)

## Node images

<https://cloud.google.com/kubernetes-engine/docs/concepts/node-images#cos>

## gcloud

```
gcloud auth login
gcloud config set project level-pattern-290811
```

### Install a cluster with gke

<https://cloud.google.com/kubernetes-engine/docs/quickstart>

```
gcloud container clusters create cluster-name --num-nodes=1
```

### Configure kubectl for cluster

```
gcloud container clusters get-credentials oas-test1 --zone europe-west6-a --project level-pattern-290811
```

### Deploy hello world app

```
kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0
kubectl expose deployment hello-server --type LoadBalancer --port 80 --target-port 8080

kubectl get service hello-server
```

Note: You might need to wait several minutes before the Service's external IP address populates

```
curl http://34.65.49.245
```
