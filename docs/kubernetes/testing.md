# Kubernetes testing

[testing-kubernetes-infrastructure](https://www.inovex.de/blog/testing-kubernetes-infrastructure/)
[Run custom tests on cluster](https://open.greenhost.net/stackspin/stackspin/-/issues/715)

## Testing pyramid

### Static analysis

- kube-score
- kube-lint
- yamllint
- `kubectl apply -f <file> --dry-run --validate=true`

Dry-run:

- `kubectl apply -f <file> --server-dry-run`

### Unit tests

## Tools / frameworks

https://blog.flant.com/small-local-kubernetes-comparison/
https://thechief.io/c/editorial/k3d-vs-k3s-vs-kind-vs-microk8s-vs-minikube/

- minikube: see `./minikube.md`
  only run a single node in the local Kubernetes cluster
- Kind: see `./kind.md`
- [k3s](https://github.com/k3s-io/k3s)
  - [k3d](https://k3d.io) is a platform-agnostic, lightweight wrapper
    that runs K3s in a docker container.
- [microk8s](https://microk8s.io/): Created by Canonical,
  microK8S is a Kubernetes distribution designed to run
  fast, self-healing, and highly available Kubernetes clusters

### Sonobouy

- [sonobuoy](https://sonobuoy.io/)
- [docs](https://sonobuoy.io/docs)
- [Fast and Easy Sonobuoy Plugins for Custom Testing of Almost Anything](https://tanzu.vmware.com/content/blog/fast-and-easy-sonobuoy-plugins-for-custom-testing-of-almost-anything)

Usage:

```
cd ~/kubernetes/testing/sonobuoy
sonobuoy run --plugin hello-world.yaml --wait
outfile=$(sonobuoy retrieve) && tar -xf $outfile -C results && cat results/plugins/debug/results/*/out*
sonobuoy delete --wait
```

### Kubetest2, gomega & ginko

- [End-to-End Testing with kubetest2, ginko and gomega](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-testing/e2e-tests-kubetest2.md)
- [kubetest2](https://github.com/kubernetes-sigs/kubetest2)
  - [no user docs for kubetest2](https://github.com/kubernetes-sigs/kubetest2/issues/134)
- [gomega](https://github.com/onsi/gomega)
- [ginko](https://github.com/onsi/ginkgo)

### etc

- [Conformance Testing in Kubernetes](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-architecture/conformance-tests.md)
- [Testing and Kubernetes](https://mechanicalrock.github.io/2018/11/07/kubernetes-testing-e2e.html)
- [netassert](https://github.com/controlplaneio/netassert)
