# Prometheus alerts for Kubernetes

https://github.com/kubernetes-monitoring/kubernetes-mixin

## Setup

Install `jsonnet-bundler` and `jsonnet` following `../../coding/jsonnet.md`

## Produce alerts, rules and dashboards

```
cd ~/projects/monitoring/prometheus/kubernetes-mixin
jb install

make prometheus_alerts.yaml
make prometheus_rules.yaml
make dashboards_out


yq '.groups[].name' < prometheus_alerts.yaml
yq '.groups[0].rules[].alert' < prometheus_alerts.yaml
yq '.groups[0].rules' < prometheus_alerts.yaml
```

## Produce ansible template-safe output

```
sed -e '/{{/i {%- raw %}' -e'/{{/a {% endraw %}'  prometheus_alerts.yaml >> ~/oas/openappstack/ansible/roles/apps/templates/settings/prometheus.yaml
```
