# Kubernetes security

https://rancher.com/blog/2020/kubernetes-security-vulnerabilities/

https://geekflare.com/kubernetes-security-scanner/

## Test suites

- [kube-linter](https://github.com/stackrox/kube-linter/blob/main/.pre-commit-hooks.yaml)
- [datree](https://github.com/datreeio/datree)

### kube-bench

https://github.com/aquasecurity/kube-bench

On the node:

```
wget https://github.com/aquasecurity/kube-bench/releases/download/v0.5.0/kube-bench_0.5.0_linux_amd64.deb
apt install ./kube-bench_0.5.0_linux_amd64.deb
wget -O cfg/config.yaml https://raw.githubusercontent.com/aquasecurity/kube-bench/main/cfg/config.yaml
kube-bench
```

### kube-hunter

https://github.com/aquasecurity/kube-hunter

On the node:

```
pip3 install --user kube-hunter
~/.local/bin/kube-hunter
```

### kubectl-kubesec

https://github.com/controlplaneio/kubectl-kubesec

```
kubectl krew install kubesec-scan
kubectl kubesec-scan

kubectl kubesec-scan -n varac pod thelounge-57cc74b65f-m8sc4
```

## Etc

https://github.com/derailed/popeye
