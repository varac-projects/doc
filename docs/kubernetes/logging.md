# Kubernetes logging

https://kubernetes.io/docs/concepts/cluster-administration/logging/

# EFK: elasticsearch, fluent-bit and kibana

https://medium.com/@jbsazon/aggregated-kubernetes-container-logs-with-fluent-bit-elasticsearch-and-kibana-5a9708c5dd9a

## Fluent-bit

- https://docs.fluentbit.io/manual/installation/kubernetes
- [fluent-but helm chart](https://hub.helm.sh/charts/stable/fluent-bit)

## Elasticsearch

- [Official helm chart](https://github.com/elastic/helm-charts/tree/master/elasticsearch)
  ([stable/elasticsearch](https://github.com/helm/charts/tree/master/stable/elasticsearch) is deprecated).

Watch all cluster members come up.

```
kubectl get pods --namespace=logging -l app=elasticsearch-master -w
```

Test cluster health using Helm test.

```
helm test elasticsearch
```

## Kibana

- [Official elastic chart](https://hub.helm.sh/charts/elastic/kibana) -
  ([stable/kibana](https://hub.helm.sh/charts/stable/kibana) is deprecated)

# Fluentd

https://hub.helm.sh/charts/bitnami/fluentd

To verify that Fluentd has started, run:

```
kubectl get all -l "app.kubernetes.io/name=fluentd,app.kubernetes.io/instance=fluentd" --all-namespaces
```

Logs are captured on each node by the forwarder pods and then sent to the aggregator pods. By default, the aggregator pods send the logs to the standard output.
You can see all the logs by running this command:

```
kubectl -n logging logs -l "app.kubernetes.io/component=aggregator"
```

# Loki

see `../../logging/loki.md`
