# k3s

[Website](https://k3s.io/)
[k3s docs](https://docs.k3s.io/)

- [No debian package/repo so far](https://github.com/k3s-io/k3s/issues/630#issuecomment-949387521)
- AUR packages available

## Installation

### curl2bash

```
curl -sfL https://get.k3s.io | sh -
sudo k3s kubectl get node
```

[Uninstall](https://docs.k3s.io/installation/uninstall):

```
/usr/local/bin/k3s-uninstall.sh
```

### k3s wrappers / installers

- [k3sup](https://github.com/alexellis/k3sup)

## Usage

- [Manual upgrades](https://docs.k3s.io/upgrades/manual)

## Resource usage

- [K3s Resource Profiling](https://rancher.com/docs/k3s/latest/en/installation/installation-requirements/resource-profiling)
- [Profiling Lightweight Container Platforms:MicroK8s and K3s in Comparison to Kubernetes](http://ceur-ws.org/Vol-2839/paper11.pdf)
- [CPU and memory usage of k3s](https://github.com/k3s-io/k3s/issues/2278)

## k3s components

### networking

<https://rancher.com/docs/k3s/latest/en/networking/>

#### servicelb

- based on [klipper-lb](https://github.com/k3s-io/klipper-lb)
- [srvclb without traefik only starts lb pods on demand](https://github.com/k3s-io/k3s/issues/3196)

## k3os

- [Website](https://k3os.io)
- [Github](https://github.com/rancher/k3os)
- [k3os docs](https://rancher.com/docs/k3s/latest/en/)
- [The project is dead but ready to get into contributor mode](https://github.com/rancher/k3os/issues/846)

Uses:

- containerd
- crictl
- Flannel
- CoreDNS
- Host utilities (iptables, socat, etc)

Logs:

- /var/lib/rancher/k3s/agent/containerd/containerd.log
- /var/log/containers/
- /var/log/pods/
- /var/log/k3s-service.log

Storage: `/var/lib/rancher/k3s/storage/`

### vagrant

<https://app.vagrantup.com/boxes/search?utf8=%E2%9C%93&sort=downloads&provider=&q=k3os>

### Install with virt-install

<https://github.com/rancher/k3os/issues/133>

```
cd ~/kubernetes/os/k3os
wget https://github.com/rancher/k3os/releases/download/v0.9.0/k3os-amd64.iso
./install.sh
```

- login with user `rancher`
- sudo k3os install
- remove the ISO from the virtual machine and reboot

### Running

```
virsh start --console k3os
ssh k3os
```

- ssh only accepts public keys

Dont use traefik: <https://github.com/rancher/k3os/issues/69#issuecomment-570768225>

Procs after first install:

- /sbin/init -> /bin/busybox
- bpfilter_umh
- udevd
- acpid
- supervise-daemo

## k3d

k3s in docker: [k3d](https://github.com/rancher/k3d)

## K3s terraform module

<https://registry.terraform.io/modules/xunleii/k3s/module/latest>

## K3s ansible module

- <https://github.com/k3s-io/k3s-ansible>
