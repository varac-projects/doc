# Cluster health checks

https://kubernetes.io/docs/tasks/debug-application-cluster/debug-cluster/

```
kubectl cluster-info
kubectl get nodes
```

## API checks

https://kubernetes.io/docs/reference/using-api/health-checks/

```
curl -k https://localhost:6443/healthz
..
```

or

```
kubectl get --raw='/healthz?verbose'
kubectl get --raw='/readyz?verbose'
kubectl get --raw='/livez?verbose'
```
