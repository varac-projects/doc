# external-secrets

- [Website/Docs](https://external-secrets.io/)
- [Github](https://github.com/external-secrets/external-secrets)
- [Helm chart](https://artifacthub.io/packages/helm/external-secrets-operator/external-secrets)

[Trigger refresh](https://external-secrets.io/v0.8.1/introduction/faq/#can-i-manually-trigger-a-secret-refresh)

```
kubectl -n mastodon annotate es mastodon-sendgrid force-sync=$(date +%s) --overwrite
```
