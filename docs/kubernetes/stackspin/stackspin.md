# stackspin

## Create new cluster

<https://docs.stackspin.net/en/latest/installation/create_cluster.html>

```
export CLUSTER_NAME=varac-test2 CLUSTER_DOMAIN=stackspin.net
```

A. Create config for existing host:

```
python3 -m stackspin ${CLUSTER_NAME}.${CLUSTER_DOMAIN} create --ip-address 10.27.64.131 --create-hostname ${CLUSTER_NAME} --subdomain ${CLUSTER_DOMAIN}
```

B. Create config and droplet using the Greenhost API:

```
python3 -m stackspin ${CLUSTER_NAME}.${CLUSTER_DOMAIN} create --create-droplet --ssh-key-id 407 --create-domain-records --subdomain ${CLUSTER_NAME} ${CLUSTER_DOMAIN}
```

Install kubernetes cluster:

```
python -m stackspin ${CLUSTER_NAME}.${CLUSTER_DOMAIN} install
```

## Install stackspin

### Install kubernetes cluster

<https://docs.stackspin.net/en/v0.7/installation/install_oas.html>

Edit stackspin config file:

```
cd stackspin/stackspin
export CLUSTER_DIR=/home/varac/stackspin/stackspin/clusters/${CLUSTER_NAME}.${CLUSTER_DOMAIN}
export KUBECONFIG=$CLUSTER_DIR/kube_config_cluster.yml
cp install/.flux.env.example $CLUSTER_DIR/.flux.env
vi  $CLUSTER_DIR/.flux.env
```

Create secret/stackspin-cluster-variables:

```
cp install/kustomization.yaml $CLUSTER_DIR/
kubectl get namespace flux-system 2>/dev/null || kubectl create namespace flux-system
kubectl apply -k $CLUSTER_DIR
```

### Install / update stackspin

Install stackspin

```
./install/install-stackspin.sh
```

Install apps

```
./install/install-app.sh monitoring
...
```

Run testinfra tests:

<https://docs.stackspin.net/en/v0.7/troubleshooting.html#testinfra-tests>

```
cd test
export CLUSTER_DIR=/home/varac/stackspin/clusters/stackspin.varac.net
py.test -sv --ansible-inventory=${CLUSTER_DIR}/inventory.yml --hosts='ansible://*'
```

Run taiko tests:

<https://docs.stackspin.net/en/v0.7/troubleshooting.html#taiko-tests>

...

## Prometheus on stackspin.varac.net

Edit Custom prometheus config at
`~/stackspin/stackspin/clusters/stackspin.varac.net/group_vars/all/prometheus.yml`.

Redeploy prometheus config-secret afterwards:

```
cd ~/stackspin/stackspin
python3 -m stackspin stackspin.varac.net install --ansible-param='--tags=prometheus'
```

Secret `prometheus-settings` in namespace `stackspin` contains the new config:

```
kubectl view-secret -n stackspin prometheus-settings values.yaml
```

But the configmap `prometheus-server` didn't update, see the latest timestamp:

```
kubectl -n stackspin get cm prometheus-server -o jsonpath='{range .metadata.managedFields[*]}{.operation}{"\t"}{.time}{"\n"}{end}'
```

In order to update this configmap we need to force a helm upgrade for
prometheus. Because there are no git changes which the helm-operator can act
upon, there's no other way than to restart helm-operator pod:

```
kubectl -n stackspin rollout restart deployment flux-custom
```

# Restart helm-operator

```
kubectl -n stackspin rollout restart deployment helm-operator
```

# hydra / single-sign-on

- [.well-known/openid-configuration](https://sso.stackspin.greenhost.net/.well-known/openid-configuration) for client auto-configuration

# Prom queries

List of pods and container mem usage, sorted by pod:

```
prom_query -o json 'sort_desc(round(avg by (pod, container) (container_memory_working_set_bytes{container!=""})/1024/1024))' | jq -r '( .[] | "\(.metric.pod),\(.metric.container),\(.value[1])" )' | column -t | sort --field-separator=',' --key=2 | sed 's/^/[ ],/' | sed '1s/^/-,Pod,Container,Mem util\n/' | csvlook
```

## Locally template charts based on Stackspin defaults

```bash
yq '.data' ~/stackspin/stackspin/flux2/core/base/single-sign-on/kratos-values-configmap.yaml | tail -n +2 > /tmp/cm.yaml
helm -n test template -f /tmp/cm.yaml kratos-test ory/kratos
```
