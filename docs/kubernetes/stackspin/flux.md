## Stackspin flux

- Install/configure cluster with Ansible:

  python3 -m openappstack oas.varac.net install --install-kubernetes --no-install-openappstack

- Copy and edit `.flux.env`:

  export CLUSTER_DIR=~/oas/openappstack/clusters/oas.varac.net
  export KUBECONFIG=${CLUSTER_DIR}/kube_config_cluster.yml
  cp install/.flux.env.example ${CLUSTER_DIR}/.flux.env
  edit ${CLUSTER_DIR}/.flux.env

- Apply `secret/oas-cluser-variables` (generated from `.flux.env`)

  cp install/kustomization.yaml ${CLUSTER_DIR}
  kubectl apply -k ${CLUSTER_DIR}
  kubectl view-secret -n flux-system oas-cluster-variables
  kubectl view-secret -n flux-system oas-cluster-variables ip_address

- Initial OAS installation

  ./install/install-openappstack.sh

- Install apps:

  bash ./install/install-app.sh wekan

- Restore
