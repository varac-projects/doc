# Running Stackspin on AWS

## k3s installation

You can run OAS on an AWS EC2 node but you need to consider the following
limitations:

### Provide ansible with external IP addr

An AWS EC2 node by itself doesn't know it's public assigned IP addr.
Therefore you need to provide it in the installation/upgrade step like this:

```
python -m stackspin gl.varac.net install --ansible-param '-e ip_address=52.58.18.134'
```

### metallb

Because `metallb` doesn't work on AWS nodes, remmove the `--disable=metallb`
from the k3s startup parameters:

```
vi /etc/systemd/system/k3s.service
systemctl daemon-reload
systemctl restart k3s.service
```

## Stackspin installation

### metallb

Stackspin uses [metallb](https://metallb.universe.tf/) as load balancer, mostly
because we want the ingress controller [ingress-nginx](https://kubernetes.github.io/ingress-nginx)
to know about the external IP so it can get configured to [block or allow certain
IP ranges](https://open.greenhost.net/openappstack/openappstack/-/issues/660).
This works fine in certain environments like a plain VPS but
[metallb won't work on AWS or other cloud providers](https://metallb.universe.tf/installation/clouds).

The solution is to use [k3s integrated service load balancer](https://rancher.com/docs/k3s/latest/en/networking/#service-load-balancer)
instead of `metallb`. You can achieve this by overriding adding the following to
your `$CLUSTERDIR/group_vars/all/settings.yml` file:

```
k3s:
  version: 'v1.18.6+k3s1'
  server_args: '--disable traefik --disable local-storage'
```

!Attention! There's currently no easy way to disable/opt-out of metallb,
see <https://open.greenhost.net/stackspin/stackspin/-/issues/720> for more
details.
