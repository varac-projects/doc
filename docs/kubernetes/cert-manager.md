# Cert-manager

- [Docs](https://docs.cert-manager.io/en/latest/index.html)
  - [ACME](https://cert-manager.io/docs/configuration/acme)

## Helm chart

- [cert-manager chart](https://artifacthub.io/packages/helm/cert-manager/cert-manager)

### Uninstall helm chart

[Docs: Uninstall](https://docs.cert-manager.io/en/latest/tasks/uninstall/kubernetes.html)

```
helm delete --purge oas-test-cert-manager
kubectl delete namespace cert-manager
```

i.e. for 0.9.1:

```
kubectl delete -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.9/deploy/manifests/00-crds.yaml
```

Verify there's no CRDs left:

```
kc get crd --all-namespaces | grep -v calico
```

### Troubleshooting

- [Troubleshooting Issuing ACME Certificates](https://cert-manager.io/docs/faq/acme/)

Custom debug script:

```
~/bin/custom/k8s_debug_cert_manager.sh
```

[cmctl](https://cert-manager.io/docs/reference/cmctl/):

```
cmctl status certificate --namespace matrix matrix.varac.net
```

## Alternatives to letsencrypt

- [List of free ACME SSL providers](https://www.xf.is/2020/06/30/list-of-free-acme-ssl-providers/)

### ZeroSSL

Features:

- No rate limit
- [ZeroSSL pricing](https://zerossl.com/pricing/):
  3 90-Day Certificates for free, unlimited for 10$/month
- [ZeroSSL vs Let's Encrypt](https://zerossl.com/letsencrypt-alternative/)

Cert-manager + ZeroSSL resources:

- [Alternative ACME via cert-manager](https://medium.com/@markmcwhirter/alternative-acme-via-cert-manager-a9e9e7f105e0)
- [cert-manager with ZeroSSL](https://github.com/cert-manager/website/issues/583)
- [ZeroSSL ACME Automation](https://zerossl.com/features/acme/)

#### API

```
curl https://api.zerossl.com/certificates\?access_key=$zerossl_api_key
```

##### ZeroSSL issues

###### Reuse / recovery of ExternalAccountBinding based account #2882

See also [Fix ZeroSSL configuration](https://open.greenhost.net/stackspin/stackspin/-/issues/1059#note_33259)
and [Reuse / recovery of ExternalAccountBinding based account](https://github.com/cert-manager/cert-manager/issues/2882)

Solution: [Generate new EAB credentials per cluster](https://zerossl.com/documentation/api/generate-eab-credentials/)
