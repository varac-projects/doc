# Velero

- [Website](https://velero.io/)
- [Docs](https://velero.io/docs)
  - [Custom resource and controllers](https://velero.io/docs/main/file-system-backup/#custom-resource-and-controllers)
- [Helmchart](https://artifacthub.io/packages/helm/vmware-tanzu/velero)
- [Docker hub image](https://hub.docker.com/r/velero/velero)
- [Supported providers](https://velero.io/docs/main/supported-providers/)
- Volume snapshots possible
- No [native encryption at rest](https://github.com/vmware-tanzu/velero/issues/434)
- [3rd party controllers](https://velero.io/docs/main/file-system-backup/#3rd-party-controllers)
  - [velero-volume-controller](https://github.com/duyanghao/velero-volume-controller)
    - detects and adds relevant backup annotation to pods with volumes
    - Last commit 2022
  - [velero-pvc-watcher](https://github.com/bitsbeats/velero-pvc-watcher)
    - detects PVCs with no restic backup and exposes a prometheus metric
    - Last commit 2021

Needs [object store provider](https://velero.io/docs/main/supported-providers/)
for backup

## Limitations

- `hostPath` volumes are not supported,
  but the `local-path` volume type is supported.
- Volumes that are mounted with `subPath` are still backed up
  as a whole, not only the subPath

## Install

- [Installation options](https://velero.io/docs/v1.9/basic-install/)

Arch: `pamac install velero-bin`

## Usage

- [Stackspin velero docs](https://docs.stackspin.net/en/v0.8/system_administration/maintenance.html#backup)

Important: A root-level backup bucket needs to exist.
Create a bucket:

```sh
mc mb varac/velero
```

Manual backup:

```sh
velero create backup BACKUP_NAME --exclude-namespaces velero --wait
```

List backups:

```sh
velero get backups
kubectl get -A backups
```

Details of backup:

```sh
velero backup describe --details manual-test1
velero backup logs velero-daily-20240924153213
```

List `backupstoragelocations`:

```sh
kubectl get -A backupstoragelocations.velero.io
velero get backup-locations
```

List `backuprepositories`:

```sh
k get -A backuprepositories.velero.io
velero repo get
```

List `podvolumebackups`:

```sh
kubectl get -A podvolumebackups.velero.io
```

List podVolumes in backup set:

```sh
velero backup describe --details -o json manual3 | jq .status.backupVolumes.podVolumeBackups.podVolumeBackupsDetails
```

### Delete backups

[Docs: Deleting Backups](https://velero.io/docs/main/backup-reference/#deleting-backups)

Delete the backup custom resource only and will not delete any associated data from object/block storage:

```sh
kubectl delete backup <backupName> -n <veleroNamespace>
```

Delete the backup resource **including all data in object/block storage**:

```sh
velero backup delete <backupName>
```

Delete whole `backuprepository`:

```sh
kubectl -n velero delete backuprepositories.velero.io/monitoring-default-restic-tqx9l
```

## File System Backup

- [Docs: File System Backup](https://velero.io/docs/main/file-system-backup/)
- [local-path-provisioner](https://github.com/rancher/local-path-provisioner) lacks [VolumeSnapshot support](https://github.com/rancher/local-path-provisioner/issues/81)
- [Re-evaluate velero as backup solution](https://open.greenhost.net/stackspin/stackspin/-/issues/694)
- `hostPath volumes are not supported. Local persistent volumes are supported.`

Restic/Kopia repositorz password:

- No way to override restic password: [Enable users to set restic repo passwords](https://github.com/vmware-tanzu/velero/issues/1053)
- [Support changing existing backup repository password](https://github.com/vmware-tanzu/velero/issues/6537)

Solution:

- [Create repo password secret before installing velero](https://github.com/vmware-tanzu/velero/issues/1053#issuecomment-1356358483)

### Restic integration

- [Restic limitations](https://velero.io/docs/v1.13/file-system-backup/#limitations)

Issues:

- When files are altered/removed during backup Restic logs errors which lead
  to a `PartiallyFailed` backup job:
  [Error thrown for empty files during backup](https://github.com/vmware-tanzu/velero/issues/4183)

## Monitoring

- Velero comes with a native metrics exporter
- [Official Kubernetes/Tanzu/Velero dashboard](https://grafana.com/grafana/dashboards/16829-kubernetes-tanzu-velero/)
- Obsolete: [prometheus-velero-exporter](ihttps://github.com/technicityworks/helm-charts/tree/main/charts/prometheus-velero-exporter)
  - Related [Grafana dashboard: Velero Exporter Overview](https://grafana.com/grafana/dashboards/15469-kubernetes-addons-velero-stats/)
- [Example Prometheus Rule to monitor Velero seems bad](https://github.com/vmware-tanzu/helm-charts/issues/562)
