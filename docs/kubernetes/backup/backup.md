# Kubernetes Backup

## Velero

see [velero.md](./velero.md)

## stash

- [Website](https://stash.run/)
- [Github](https://github.com/stashed/stash)
- 👎 [Community edition limited to only 10 apps](https://stash.run/pricing)
- Non S3-Backends:
  - restic REST-Server
  - [Local backend](https://stash.run/docs/v2021.01.21/guides/latest/backends/local):
    > Local backend refers to a local path inside stash sidecar container.
    > Any Kubernetes supported persistent volume such as PersistentVolumeClaim,
    > HostPath, EmptyDir (for testing only), NFS, gcePersistentDisk etc. can be used as local backend.
- Still no [SFTP backend](https://github.com/stashed/stash/issues/1035)

## k8up

- [Website](https://k8up.io)
- [docker image](https://hub.docker.com/r/vshn/k8up)
- [Backends](https://docs.k8up.io/k8up/references/object-specifications.html#_backend):
  - Non-S3 backends:
    - No [SFTP backend](https://github.com/vshn/k8up/issues/66)
    - [local](https://docs.k8up.io/k8up/references/object-specifications.html#_local)
    - [rest](https://docs.k8up.io/k8up/references/object-specifications.html#_rest)

## Other backup tools

- [volsync](https://github.com/backube/volsync)
- [Kanister](https://kanister.io/)
  Open Source variant of [Kasten](https://www.kasten.io/)
- [FairwindsOps/gemini](https://github.com/FairwindsOps/gemini)
  Automated backups of PersistentVolumeClaims in Kubernetes using VolumeSnapshots
- [volsync](https://github.com/backube/volsync)

## Snapshot tools

- [Kubernetes 1.20: Kubernetes Volume Snapshot Moves to GA](https://kubernetes.io/blog/2020/12/10/kubernetes-1.20-volume-snapshot-moves-to-ga)

## Kubernetes state backup

- [katafygio](https://github.com/bpineau/katafygio): Dump, or continuously
  backup Kubernetes objets as yaml files in git
