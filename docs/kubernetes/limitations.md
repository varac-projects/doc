# Kubernetes limitations

## UDP Broadcasts

[metallb: Listen to UDP broadcast traffic](https://github.com/metallb/metallb/issues/344):

> Kubernetes's LoadBalancer logic (which MetalLB relies on) does not support
> forwarding broadcast traffic into pods, so this simply will not work in k8s,
> sorry. The only option you have is to run the pods with hostNetwork=true and
> try to wrangle something with that, but you won't get stable IPs with that setup.
