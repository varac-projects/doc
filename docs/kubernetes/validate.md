# Validating k8s resources

<https://learnk8s.io/validating-kubernetes-yaml>

## kubeconform

- [Github](https://github.com/yannh/kubeconform)
- As of 2020-11 only available as binary from github releases.
- Advantage over kubeval: Better support for CRDs

### CRD support

> Configurable list of remote, or local schemas locations, enabling validating
> Kubernetes custom resources (CRDs)

<https://github.com/instrumenta/kubeval/issues/47#issuecomment-929474854>:

> I have a repo with all my schemas <https://github.com/tarioch/k8s-schemas>
> Whenever I add any new CRDs to my cluster I update the schemas on a machine
> that has access to the cluster, see
> <https://github.com/tarioch/k8s-schemas/blob/master/update.sh> for special cases
> (e.g. in my case jaeger-operator) I get the CRDs not from the cluster but from
> another place This then get's checked in. Whenever I want to validate (e.g. on
> CI or in a pre-commit hook), I can just point it to that repository and
> validate

From <https://github.com/instrumenta/kubeval/issues/47#issuecomment-930450215>:

```sh
kubeconform -kubernetes-version 1.21.0 -strict -schema-location default \
  -schema-location \
  'https://raw.githubusercontent.com/tarioch/k8s-schemas/master/schemas/{{ .ResourceKind }}_{{ .ResourceAPIVersion }}.json'
```

### Issues

Couldn't get it working with CRDs like `helmRelease`.

## kubeval - old ?

<https://github.com/instrumenta/kubeval>
Last release 2020-04 (as of 2021-01)

As of 2020-09 only available as binary from github releases.

By itself, kubeval won't recognize CRDs like `helmrelease` etc
(see <https://github.com/instrumenta/kubeval/issues/47> or
<https://github.com/instrumenta/kubernetes-json-schema/issues/8>)

<https://kubernetesjsonschema.dev/>
<https://github.com/instrumenta/kubernetes-json-schema> comes to the rescue for linting
CRDs like `helmRelease`: (doesn't work anymore...)

```sh
kubeval --additional-schema-locations https://raw.githubusercontent.com/ams0/kubernetes-json-schema/master test-hr.yaml
```

Scan directory recursive:

```sh
kubeval --additional-schema-locations https://raw.githubusercontent.com/ams0/kubernetes-json-schema/master -i .pre-commit-config.yaml -d ~/kubernetes/flux-config
```

## kube-score

<https://github.com/zegl/kube-score>

- By itself, kubeval won't recognize CRDs like `helmrelease` etc, and I didn't
  find a workaround.
- Kube-score isn't designed to be extendable and you can't add or tweak policies.

Install:

```sh
kubectl krew install score
```

Usage:

```sh
kubectl score -vvvvvvv test-helmrelease.yaml
kube-score score -vvvvvvvvvv --exit-one-on-warning \
  ~/kubernetes/flux-config/oas.varac.net/varac/thelounge-pvc.yaml
```

Couldn't get it working:

```sh
$ kube-score score -vvvvvvvvvv --exit-one-on-warning \
  ~/kubernetes/flux-config/oas.varac.net/varac/thelounge-pvc.yaml
2020/11/09 16:35:42 Unknown datatype: /v1, Kind=PersistentVolumeClaim
```

## kubernetes-validate

<https://github.com/willthames/kubernetes-validate>

- Last commit 2020-04, no releases
- [pre-commit hook integration](https://github.com/Agilicus/pre-commit-hook-k8svalidate)
