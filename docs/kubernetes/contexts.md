# kubectl context switching apps

- [Setting kubectl context via env var](https://www.reddit.com/r/kubernetes/comments/135elqu/setting_kubectl_context_via_env_var/)

## kubie

- [Blog article](https://blog.sbstp.ca/introducing-kubie/)
- [GitHub](https://github.com/sbstp/kubie)
- Rust
- [Arch package](https://archlinux.org/packages/extra/x86_64/kubie/)
- Installable from [mise registry](https://github.com/mise-plugins/registry)
- Config at `~/.kube/kubie.yaml`
- Still no [zsh completion](https://github.com/sbstp/kubie/issues/34)
- [Hook support](https://github.com/sbstp/kubie/blob/master/README.md?plain=1#L166)
  for when a context is started and stopped
- Creates a new kubeconfig `/tmp/kubie-config-*.yaml`

### Direnv/mise support

- [Direnv support](https://github.com/sbstp/kubie/pull/125)
- ❌ kubie creates a [new shell](https://github.com/sbstp/kubie/issues/85)
  whenever a new context is started. Only in this case hooks are executed, not
  then using `kubie export` in the context of direnv/mise. This makes it not
  work properly with direnv/mise.

[Usage](https://github.com/sbstp/kubie?tab=readme-ov-file#usage):

```sh
kubie ctx
```

## kubeswitch

- [Introduction](https://danielfoehrkn.medium.com/the-case-of-kubeswitch-aff4b6a04ae7)
- [GitHub](https://github.com/danielfoehrKn/kubeswitch)
  - Very active and recent development
- [Install](https://github.com/danielfoehrKn/kubeswitch/blob/master/docs/installation.md)
  - [AUR package: kubeswitch-bin](https://aur.archlinux.org/packages/kubeswitch-bin)
- **NOT** installable from
  [mise registry](https://github.com/mise-plugins/registry)
- Supports multiple cloud provider authentication methods, i.e. the
  [GKE store](https://github.com/danielfoehrKn/kubeswitch/blob/master/docs/stores/gke/gke.md)
- Supports
  [hooks](https://github.com/danielfoehrKn/kubeswitch/blob/master/hooks/README.md)
  but hooks are only executed
  - before picking a context and/or on a regular schedule (every X hours i.e.)
  - [FR: Support post-picking hooks](https://github.com/danielfoehrKn/kubeswitch/issues/77)
    Unfortunatly still open
- [Direnv support](https://github.com/danielfoehrKn/kubeswitch/issues/76)

Usage:

Changes `KUBECONFIG` env var to i.e. `~/.kube/.switch_tmp/config.1692710140.tmp`

[Usage via env variable](https://github.com/danielfoehrKn/kubeswitch/issues/76#issuecomment-1830073364):

```sh
export KUBECONFIG=$(switcher $CONTEXT | sed 's/^__ //' | cut -d, -f1)
```

## kubesess

- [GitHub](https://github.com/Ramilito/kubesess)
- Rust, faster than `kubectx`
- **NOT** installable from
  [mise registry](https://github.com/mise-plugins/registry)
- No AUR package
- Allows setting namespace/context isolated per terminal (using `KUBECONFIG`) as
  well as global (using `~/.kube/config`)
- No hook support !

Changes `KUBECONFIG` env var to i.e.
`KUBECONFIG=~/.kube/kubesess/cache/mastodon-dev:/home/varac/.kube/config`

## kubectx

- [GitHub](https://github.com/ahmetb/kubectx)
- Slower than kubesess
- Warning: Changes context _globally_

Usage:

```sh
kubectx
```

## Use shell aliases

- Warning: Changes context _globally_

Set namespace:

```sh
alias kcd='kubectl config set-context $(kubectl config current-context) --namespace'
kcd gitlab-nextcloud
```

## kube-ps1

Kubernetes prompt info for bash and zsh

- [GitHub](https://github.com/jonmosco/kube-ps1)
