# Kubernetes configmaps

<https://kubernetes.io/docs/concepts/configuration/configmap/>

## Create config map from cli

```sh
echo -n bar | kubectl create cm test --dry-run=client --from-file=foo=/dev/stdin -o yaml
```

Generate from file:

```sh
kubectl create configmap known_hosts_0xacab --dry-run=client -o yaml --from-file=known_hosts=known_hosts_0xacab > known-hosts-0xacab-configmap.yaml
```

Add label:

```sh
echo -n bar | kubectl create cm test --dry-run=client --from-file=foo=/dev/stdin \
  -o yaml | kubectl label -f - --dry-run=client -o yaml \
  --local grafana_dashboard=1
```

## Parse data items

```sh
kubectl -n oas get cm prometheus-server -o yaml | yq -r '.data."prometheus.yml"' | yq . | less
```
