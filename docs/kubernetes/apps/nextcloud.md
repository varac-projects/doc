# Nextcloud on kubernetes

https://medium.com/faun/nextcloud-scale-out-using-kubernetes-93c9cac9e493

# Manual installation

```
export NEXTCLOUD_PASSWORD=$(pwgen -1) NEXTCLOUD_MARIADB_PASSWORD=$(pwgen -1) NEXTCLOUD_MARIADB_ROOT_PASSWORD=$(pwgen -1) ONLYOFFICE_JWT_SECRET=$(pwgen -1) ONLYOFFICE_POSTGRESQL_PASSWORD=$(pwgen -1) ONLYOFFICE_RABBITMQ_PASSWORD=$(pwgen -1)
helmfile -b /usr/local/bin/helm -e oas -f /var/lib/OpenAppStack/source/helmfiles/helmfile.d/20-nextcloud.yaml apply
```

# Uninstall

```
helmfile -b /usr/local/bin/helm -e oas -f /var/lib/OpenAppStack/source/helmfiles/helmfile.d/20-nextcloud.yaml destroy
```
