# SMTP charts

## cloudposse/postfix

https://hub.helm.sh/charts/cloudposse/postfix

Uses [cloudposse/postfix](https://hub.docker.com/r/cloudposse/postfix) docker
image: `ubuntu:14.04`, updated 2y ago, no builds.

## halkeye/postfix

https://hub.helm.sh/charts/halkeye/postfix
https://github.com/halkeye-helm-charts/postfix
Uses [applariat/tx-smtp-relay](https://hub.docker.com/r/applariat/tx-smtp-relay)
docker image: No Dockerfile, last Update 2016!

See also `~/Howtos/docker/smtp.md` for images
