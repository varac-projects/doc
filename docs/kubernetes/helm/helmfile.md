# Helmfile

- [Github](https://github.com/helmfile/helmfile)
- [Docs](https://helmfile.readthedocs.io/en/latest/)
- [Example helmfiles](https://github.com/cloudposse/helmfiles/tree/master/releases)

## Usage

- [Waiting to install release until another release is finished](https://github.com/roboll/helmfile/issues/107)
  Solution: Use lables

- Restart pod after configmap changes
  `kc -n media rollout restart deployment/hugo`

### Release odering

- [Needs doesn't work properly when helmfile is distributed among multiple files](https://github.com/roboll/helmfile/issues/2079)

### Issues

- [helmfile apply fails whenever chart defines custom resource instances while defining their CRDs](https://github.com/helmfile/helmfile/issues/763)
