# Helm chart testing

<https://helm.sh/docs/topics/chart_tests/>

See existing tests:

```
grep 'helm.sh/hook.*test' -ir ~/kubernetes/charts
```

## Example

```
cd /home/varac/kubernetes/charts/wekan/charts.git/wekan
```

Start kind cluster with ingress setup

```
kind_create.sh
```

Install:

```
helm install --set ingress.path=/ wekan .
```

Test:

```
helm test wekan
```

### "ct", chart-testing tool

<https://github.com/helm/chart-testing>

```
brew install chart-testing
```

Can also test a [local repo](https://github.com/helm/chart-testing#local-repo).

### Workflow
