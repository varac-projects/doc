# Helm documentation

## helm-docs

- <https://github.com/norwoodj/helm-docs>
- Much more commits, contributors etc than `frigate`

Install:

```
brew install norwoodj/tap/helm-docs
```

## Frigate

<https://frigate.readthedocs.io/en/latest>
<https://github.com/rapidsai/frigate>
