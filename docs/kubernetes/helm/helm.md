# Helm

- [Docs](https://helm.sh/docs/)

## Install helm binary

```sh
brew install helm
sudo snap install helm

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

## helm usage

### Add helm repo

```sh
helm repo add stable https://charts.helm.sh/stable
helm repo update
```

Add private chart repo

```sh
helm repo add --username varac --password $(gopass show --password \
  gitlab.com/varac/personal-access-token/cli-api) \
  mastodon https://gitlab.com/api/v4/projects/12345/packages/helm/stable
```

### Template

```sh
helm dependency update
helm template --validate .
```

Issues:

If `helm template` fails with i.e. `Error: chart requires kubeVersion: >=1.24.0-0 which is incompatible with Kubernetes v1.20.0`
and `kubectl` is the right version use the `--validate` or `--kube-version` flags:

```sh
helm template --kube-version 1.24.0 .
helm template --validate .
```

See also:

- [helm template defaults to --kube-version (Capabilities.KubeVersion) without mentioning it in the --help output](https://github.com/helm/helm/issues/13248)
- [Wrong cluster version detected (v1.20.0 when I'm running v1.21.2)](https://github.com/helm/helm/issues/10086)

### Etc

```sh
helm history kube-prometheus-stack -n oas
```

Uninstall all:

```sh
helm ls -a --all-namespaces | \
  awk 'NR > 1 { print  "-n "$2, $1}' | xargs -L1 helm delete
```

### Install charts

<https://helm.sh/docs/helm/helm_install/>

```sh
helm install mariadb stable/mariadb
helm install --name nc-test --set mariadb.enabled=true stable/nextcloud
```

Test/dry-run install

```sh
cd ~/work/git/charts/helm-work/charts/frontend
dasel -f ~/work/git/charts/workcloud-helm/environments/dev/ingress.yaml \
  'frontend'  > /tmp/val.yml
helm -n varac install --generate-name -f /tmp/val.yml \
  --set frontend.backend.url=http://foo.bar .
```

Install/upgrade local chart:

```sh
helm upgrade -n test --create-namespace \
  --set TOKEN=de personal-gitlab-exporter .
```

Only fetch and untar chart for inspection without adding
a persistent remote helm repo:

```sh
helm fetch --untar --repo https://charts.cloudposse.com/incubator/ \
  postfix --version 0.1.1
```

List chart versions:

```sh
helm repo add truecharts https://charts.truecharts.org/
helm repo update
helm search repo truecharts/ntfy --versions
```

Restart all failing releases:

```sh
helm delete --purge $(helm ls | grep FAILED | cut -d ' ' -f 1)
```

- [Automatically Roll Deployments When ConfigMaps or Secrets change](https://helm.sh/docs/howto/charts_tips_and_tricks/#automatically-roll-deployments)

## Plugins

### Git

- [GitHub](https://github.com/aslafy-z/helm-git)
- Helm git support is incredibly slow compared to proper helm packages

### Helmdiff

<https://github.com/databus23/helm-diff>

Install:

```sh
helm plugin install https://github.com/databus23/helm-diff --version master
```

## Wrappers

### helm_upgrade_logs

> Wrapper around helm and kubectl to allow easy debugging of a helm release

- [Github](https://github.com/SamuelGarrattIqa/helm_upgrade_logs)

## Use helm together with kustomize

- <https://kustomize.io/>

- <https://github.com/ContainerSolutions/helm-convert>

- <https://blog.argoproj.io/the-state-of-kubernetes-configuration-management-d8b06c1205>

## Chart development

Best practices:

- [REVIEW_GUIDELINES](https://github.com/helm/charts/blob/master/REVIEW_GUIDELINES.md)
- <https://docs.bitnami.com/tutorials/production-ready-charts/>

### Publish charts at own repo

```sh
cd ~/kubernetes/charts/varac/helmcharts
find . -maxdepth 1 -type d | egrep -v '^(.|./.git|./public)$' \
  | xargs -n 1 helm package -d public
helm repo index public --url https://charts.k.varac.net
```

### Publish helm chart repo at hub.helm.sh

- <https://github.com/helm/hub/blob/master/Repositories.md>

## OCI chart repositories

- [Use OCI-based registries](https://helm.sh/docs/topics/registries/)

### Install

```sh
helm install my-release oci://registry-1.docker.io/bitnamicharts/nginx --version 0.1.0
```

### List tags in OCI repo

- [helm itself still doesn't support searching for versions in OCI repos](https://github.com/helm/helm/issues/11000)

Workarounds:

With crane:

```sh
sudo pacman -S crane
crane ls tccr.io/truecharts/homer
```

With oras:

```sh
pamac install oras
oras repo tags tccr.io/truecharts/homer
```
