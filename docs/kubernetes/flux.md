# Flux CD

- [Website](https://fluxcd.io/)

## Other tools

- [flux-local](https://github.com/allenporter/flux-local)
  tools and libraries for managing a local flux gitops repository
  focused on validation steps to help improve quality of commits,
  PRs, and general local testing.

## Install

Flux cli tool:

- [AUR package](https://aur.archlinux.org/packages/flux-bin)
- [Github release binaries](https://github.com/fluxcd/flux2/releases)

### Terraform

- [terraform-provider-flux](https://github.com/fluxcd/terraform-provider-flux/issues/268)

Issues:

- [Support nodeSelector for scheduling](https://github.com/fluxcd/terraform-provider-flux/issues/268)

### Manual

Upgrade/install flux on cluster:

```sh
flux install --network-policy=false --watch-all-namespaces=true --namespace=flux-system
```

## Usage

Get all flux resources:

```sh
flux get all
```

### Reconcile all helm releases

```sh
flux get -A helmrelease --no-header | \
  awk -F' ' '{system("flux reconcile -n " $1 " helmrelease " $2)}'
```

or

```sh
kubectl get -A hr --template \
  '{{range .items}}{{.metadata.namespace}}/{{.metadata.name}}{{"\n"}}{{end}}' | \
  awk -F'/' '{system("flux reconcile -n " $1 " helmrelease " $2)}'
```

## CRDs

- [How to deal with operator that creates CRDs](https://github.com/fluxcd/flux2/discussions/1311)
  - [Example repo as POC](https://github.com/spolab/spolab-flux)

## Troubleshooting

### Test flux resource locally

Beware: Doesn't render `valuesFrom:` fields!

```sh
yq .spec.values media/transmission-helmrelease.yaml > /tmp/values.yml
helm -n test template -f /tmp/values.yml transmission truecharts/transmission
```

### upgrade retries exhausted

If you get `helm-controller reconciliation failed: upgrade retries exhausted`:

```sh
flux suspend helmrelease nextcloud -n oas-apps
flux resume helmrelease nextcloud -n oas-apps
```

### another operation (install/upgrade/rollback) is in progress

`Helm upgrade failed: another operation (install/upgrade/rollback) is in progress`

See <https://open.greenhost.net/stackspin/wordpress-helm/-/issues/119>

Solved by helm rollback and flux reconciliation of the helmRelease.
First list all helm releases in the namespace and watch for the `pending-install`
status ones:

```console
$ helm -n stackspin-apps ls --all
  NAME      NAMESPACE      REVISION UPDATED                                 STATUS          ...
  wordpress stackspin-apps 4        2022-04-19 13:57:09.108314695 +0000 UTC pending-install ...
```

Then do a helm rollback to the revision before (in this case `3`):

```sh
helm -n stackspin-apps rollback wordpress 3
```

### HelmChart 'flux-system/wireguard-wireguard-server' is not ready

Note: There are two `helmchart` resources:

- `helmcharts.helm.cattle.io` (default, when uses with `helmcharts`)

- `helmcharts.source.toolkit.fluxcd.io` (use this one!)

  kc get helmcharts.source.toolkit.fluxcd.io -A

### "Cannot delete kustomization when it is running health checks"

from [Cannot delete kustomization when it is running health checks](https://github.com/fluxcd/kustomize-controller/issues/487)

> The only workaround is to suspend the Kustomization, then restart the
> controller by killing its pod. When the controller starts it will no longer
> try to run the health check again since it receives the new object that's
> suspended.

### Use different branch for Stackspin

```sh
kc -n flux-system patch gitrepo stackspin -p "{\"spec:\": {\"ref\": {\"branch\": \"nextcloud_1030\" }}}"
```

## Patching flux resources outside helm options

- [Adding a sidecar container to helm chart's deployment](https://github.com/fluxcd/flux2/discussions/2088)

## Installing a helm chart from git

- [FluxCD — Deploy a Helm Chat from Git](https://medium.com/@damianmyerscough/fluxcd-deploy-a-helm-chat-from-git-bc517d619810)
