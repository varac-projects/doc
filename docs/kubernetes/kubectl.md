# kubectl

Install

```sh
sudo snap install kubectl --classic
```

## Completion

Prevent kubectl from trying to connect to cluster for completion:

```sh
kubectl completion bash --kubeconfig=/dev/null
```

## Useful Tools

- For Plugins see `./kubectl-plugins.md`
- [fubekctl](https://github.com/kubermatic/fubectl) Reduces repetitive
  interactions with kubectl
- [kubecolor](https://github.com/hidetatz/kubecolor) colorizes kubectl output
- [watch changes in ConfigMap and Secrets and then restart pods](https://github.com/stakater/reloader)

## Usage

### Use jsonpath to parse output

- [JSONPath Support](https://kubernetes.io/docs/reference/kubectl/jsonpath)

Show `volumeName` from pvc:

```sh
kubectl -n oas get pvc prometheus-0 -o=jsonpath='{.spec.volumeName}'
```

Show `.status.reason` from challenge:

```sh
kubectl -n oas-apps get challenge oas-rocketchat -o jsonpath='{.status.reason}'
```

[List All Container Images Running in a Cluster](https://kubernetes.io/docs/tasks/access-application-cluster/list-all-running-container-images/)

```sh
kubectl get pods --all-namespaces \
  -o jsonpath="{.items[*].spec.containers[*].image}" | uniq
```

List PVC name and selectedNode:

```sh
kubectl get pvc -A \
  -o jsonpath='{range .items[*]}{@.metadata.annotations.volume\.kubernetes\.io\/selected-node}{" "}{@.metadata.name}{"\n"}{end}' # <!-- markdownlint-disable-line -->
```

### Use field-selector to filter resource by certain keys

- [Field Selectors](https://kubernetes.io/docs/concepts/overview/working-with-objects/field-selectors/)
- [Kubernetes Label Selector And Field Selector](https://medium.com/mayadata/kubernetes-label-selector-and-field-selector-81d6967bb2f)
- [Doesn't work with complex keys](https://github.com/kubernetes/kubernetes/issues/49387)

Examples:

Show all PVCs not setup by flux:

```sh
kubectl get pvc -l kustomize.toolkit.fluxcd.io/namespace!=flux-system -A
```

Show all resources with `single-sign-on` velero backup label:

```sh
kubectl get all -A -l stackspin.net/backupSet=single-sign-on
```

Etc:

```sh
kubectl get -n kube-system pods -lname=tiller --field-selector=status.phase=Running
kc -n oas get pod kube-prometheus-stack-prometheus-node-exporter-72qz8 -o=jsonpath='{.spec.containers[].resources}'
```

Show events filtering for multiple fields:

```sh
kubectl -n stackspin-apps get events \
  --field-selector 'involvedObject.name=nextcloud,type!=Normal' -o yaml
```

### Rolling updates of an app/pod

- [Rolling Updates with Kubernetes Deployments](https://tachingchen.com/blog/kubernetes-rolling-update-with-deployment/)

  kubectl -n varac rollout restart deployment website kubectl -n varac rollout
  status deployment website

### Show all available APIs

[sting all resources in a namespace](https://stackoverflow.com/a/53016918):

```sh
kubectl api-resources --verbs=list --namespaced -o name
```

### Get all resources with their namespace

```sh
kubectl get -A hr --template '{{range .items}}{{.metadata.namespace}}/{{.metadata.name}}{{"\n"}}{{end}}'
```

### Show event timestamps instead of relative time

```sh
kubectl -n stackspin get events -o --no-headers=true \
  --field-selector type!=Normal \
  custom-columns=FirstSeen:.firstTimestamp,LastSeen:.lastTimestamp,Count:.count,\
  Component:.source.component,Object:.involvedObject.name,Type:.type,Reason:.reason,Message:.message
```

### Trigger rollout of new container

```sh
kubectl -n presentation patch deployment presentation -p \
  "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
```

### Set current namespace

```sh
alias kcd='kubectl config set-context $(kubectl config current-context) --namespace'
kcd nextcloud
```

### Get _all_ resources

`kubectl get all ...`

```sh
kubectl api-resources --verbs=list --namespaced -o name | \
  xargs -n 1 kubectl get --show-kind --ignore-not-found -n gitlab-nextcloud
```

See also `./kubectl-plugins.md` for the _ketall_ plugin

### Scale deployment

```sh
kubectl -n gitlab-nextcloud scale --replicas=0 deployment/nextcloud-test
```

## Dealing with multiple clusters/contexts

To use different contexts, set the `KUBECONFIG` env var like this:

```sh
KUBECONFIG=/home/varac/.kube/admin@hexacab.org.yml:/home/varac/.kube/admin@varac-oas.yml
```

Then you can use:

```sh
  kubectl config get-contexts
  kubectl config set-context varac-oas
```

### Context switching apps

see [./contexts.md](./contexts.md)
