# Minikube

- [Github](https://github.com/kubernetes/minikube)
- [Docs](https://minikube.sigs.k8s.io/docs/)

## Installation

Options:

- Brew: `brew install minikube`
- Snaps (<https://snapcraft.io/minikube>) are out of date.
- [Download binaries](https://minikube.sigs.k8s.io/docs/start/)

## Setup

Completion:

```
minikube completion zsh > ~/.zsh/completion/_minikube
```

### Choose driver

- [driver](https://minikube.sigs.k8s.io/docs/drivers/)
- Default driver is docker

#### kvm2

```
minikube config set vm-driver kvm2
```

## Usage

```
minikube start
minikube delete
```

### Persistent Volumes

```
/tmp/hostpath-provisioner/
```
