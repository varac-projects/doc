# Kubernetes ingress

## Get real-ip header

- [Traefik Kubernetes Ingress and X-Forwarded- Headers](https://medium.com/@_jonas/traefik-kubernetes-ingress-and-x-forwarded-headers-82194d319b0e)

Other resources, non of them worked for my setup though:

- [Make Traefik Forward Real Client IP](https://comphilip.wordpress.com/2021/05/23/k3s-thing-make-traefik-forward-real-client-ip/)
- [K8s docs: Create an External Load Balancer / Preserving the client source IP](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip)
- [Getting Real Client IP with k3s](https://github.com/k3s-io/k3s/discussions/2997)

### Traffic in front of the cluster ingress

- Haven't found a way to pass the real-ip header when using a TCP router with
  `TLS passthrough`
- [passHostHeader](https://doc.traefik.io/traefik/routing/services/#pass-host-header)
  is [only available for http services, not for tcp services](https://community.traefik.io/t/pass-host-header-for-tcp/3018), of course because
  > Header is a concept from HTTP protocol. TCP does not have a concept of an HTTP header
- Until then, services that require real-ip (i.e. Plausible) need to stay on an
  external node with static IP

## Exposing arbitrary TCP and UDP ports

`nginx-ingress` by default can only expose http+https ports.
You can use the `tcp` and `udp` [helm chart config options](https://github.com/helm/charts/tree/master/stable/nginx-ingress#configuration)
like this:

```
tcp:
  8080: "moewe/unifi-controller:8080"
```

or this:

```
 udp:
  10001: "moewe/unifi-discovery:10001"
  3478: "moewe/unifi-stun:3478"
```

**BUT** you can mix them, if will produce the following error when applying a
mixed TCP+UDP helmfile:

Error: Service "oas-test-proxy-nginx-ingress-controller" is invalid: spec.ports: Invalid value: …
cannot create an external load balancer with mix protocols

Related issues:

- https://github.com/helm/charts/issues/11268
- https://github.com/kubernetes/kubernetes/issues/23880

### Basic auth

- [Basic Authentication](https://kubernetes.github.io/ingress-nginx/examples/auth/basic/)
