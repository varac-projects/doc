# Ingress-nginx

- [Website](https://kubernetes.github.io/ingress-nginx/)
- [Github](https://github.com/kubernetes/ingress-nginx)
- [Using nginx-ingress controller to restrict access by IP (ip whitelisting) for a service deployed to a Kubernetes (AKS) cluster](https://medium.com/@maninder.bindra/using-nginx-ingress-controller-to-restrict-access-by-ip-ip-whitelisting-for-a-service-deployed-to-bd5c86dc66d6)
