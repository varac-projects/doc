# Postgresql on Kubernetes

- [Bitnami chart](https://github.com/bitnami/charts)

## Manually change the postgres user password

- [Modify the default administrator password](https://docs.bitnami.com/aws/infrastructure/postgresql/administration/change-reset-password/)

## Upgrade postgres major version

### Dump & restore

Preferred solution: [Dump and restore](https://www.postgresql.org/docs/current/backup.html)

- [How to properly handle major version upgrades?](https://github.com/bitnami/charts/issues/1798)
- [bitnami docs: Create and restore PostgreSQL backups](https://docs.bitnami.com/ibm/infrastructure/django/administration/backup-restore-postgresql/)
- [Dump and restore a postgresql database on kubernetes](https://www.adyxax.org/blog/2020/06/25/dump-and-restore-a-postgresql-database-on-kubernetes/)

Issues / considerations:

- Don't use text file format, but custom file format (`-Fc`),
  otherwise it could lead to text file issues like dos line endings and `\N`
  issues.
- Also, avoid piping from pod to local filesystem because of the above issue.

#### Manual dumps

Option 0: Use `pg_dumpall` like the automated [bitname postgresql backup cronjob](https://github.com/bitnami/charts/blob/main/bitnami/postgresql/values.yaml#L1133):
From inside the postgres container ()

```
export PGDUMP_DIR=/var/lib/postgresql/data
pg_dumpall --clean --if-exists --load-via-partition-root \
  --quote-all-identifiers --no-password \
  --file=${PGDUMP_DIR}/pg_dumpall-$(date '+%Y-%m-%d-%H-%M').pgdump"
```

Option 1: Dump directly on pg host:

```
export PG_PASSWORD=$(kubectl get secret -n plausible plausible-postgresql \
  -o jsonpath="{.data.postgres-password}" | base64 --decode)
echo $PG_PASSWORD
kubectl -n plausible exec -ti plausible-postgresql-0 -- pg_dump -Fc --host \
  plausible-postgresql -U postgres -d plausible -f /tmp/plausible.dump
kubectl -n plausible cp plausible-postgresql-0:/tmp/plausible.dump /tmp/plausible.dump
```

Option 2: Dump from dedicated pg client pod (WIP, still not working since the pod
terminates after pg_dump)

```
export PG_PASSWORD=$(kubectl get secret -n plausible plausible-postgresql \
  -o jsonpath="{.data.postgres-password}" | base64 --decode)
export PG_IMAGE=$(kubectl -n plausible get pod plausible-postgresql-0 \
  -o jsonpath="{.spec.containers[0].image}")
kubectl run db-postgresql-client --rm --tty -i --restart='Never' \
  --namespace plausible --image $PG_IMAGE --env="PGPASSWORD=$PG_PASSWORD" \
  --command -- \
  pg_dump -Fc --host plausible-postgresql -U postgres -d plausible -f /tmp/plausible.dump

kubectl -n plausible cp plausible-postgresql-0:/tmp/plausible.dump /tmp/plausible.dump
```

- Remove Helmrelease
- Remove Postgres PVC
- Upgrade helmrelease to new Postgres major version
- Scale down deployments/statefulsets (i.e. Plausible, Plausible-clickhouse )

#### Restore

```
kubectl -n plausible cp /tmp/plausible.dump plausible-postgresql-0:/tmp/plausible.dump

kubectl -n plausible exec -ti plausible-postgresql-0 -- \
  dropdb -U postgres plausible
kubectl -n plausible exec -ti plausible-postgresql-0 -- \
  createdb -U postgres plausible
kubectl -n plausible exec -ti plausible-postgresql-0 -- \
  pg_restore -U postgres -d plausible /tmp/plausible.dump
```

### pg_upgrade

- [Upgrade in-place using pg_upgrade](https://github.com/bitnami/charts/issues/8025#issuecomment-964906018)
