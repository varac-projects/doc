# Kubernetes troubleshooting

- [Troubleshooting Applications](https://kubernetes.io/docs/tasks/debug/debug-application/)
  - [Debug Running Pods](https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/)
- [Troubleshooting Clusters](https://kubernetes.io/docs/tasks/debug/debug-cluster/)

## Cluster components

```sh
kubectl get componentstatus
```

## Network

### netshoot

- For general netshoot docs see [../container/troubleshooting.md](../container/troubleshooting.md)
- [Netshoot with Kubernetes](https://github.com/nicolaka/netshoot?tab=readme-ov-file#netshoot-with-kubernetes)
- [K8s docs: Debug Running Pods](https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/)
- [k9s plugin](https://github.com/derailed/k9s/blob/master/plugins/debug-container.yaml)
- [kubectl plugin](https://github.com/nilic/kubectl-netshoot)

#### Usage

Run an ephemeral container in an existing pod:

```sh
kubectl -n headscale debug headscale-6646bf6ffd-tpzl7 -it --image=nicolaka/netshoot
```

Run an ephemeral container in an existing pod, and attach to process namespace of running container,
to access the process list i.e.:

```sh
kubectl -n headscale debug headscale-6646bf6ffd-tpzl7 -it --image=nicolaka/netshoot --target=headscale
```

Create dedicated namespace and run netshoot:

```sh
kubectl create namespace tmp
kubectl -n tmp run tmp-shell --rm -i --tty --image nicolaka/netshoot
kubectl -n tmp run netshoot-tmp --attach=true --rm -i --tty --image nicolaka/netshoot
```

Exec into already running netshoot container:

```sh
kubectl exec -it netshoot-tmp -- sh
```

## Delete evicted pods

After `DiskPressure` happened due to a full disk, hundreds of pods got evicted but still showed up after
`DiskPressure` recovered.
[Delete all evicted pods](https://github.com/kubernetes/kubernetes/issues/55051#issuecomment-342503382) with:

```sh
kubectl get pods --all-namespaces -ojson | jq -r '.items[] | \
  select(.status.reason!=null) | select(.status.reason | \
  contains("Evicted")) | .metadata.name + " " + .metadata.namespace' | \
  xargs -n2 -l bash -c 'kubectl delete pods $0 --namespace=$1'
```
