# Kubernetes events

- [Aggregate kubernetes events](https://open.greenhost.net/openappstack/openappstack/-/issues/706) with eventrouter
- Open k8s FR [Implement alternative storage destination (for events)](https://github.com/kubernetes/kubernetes/issues/19637)

Get events for specific pod💹

```
kubectl get event -w -n oas --field-selector involvedObject.name=prometheus-stack
```

## Eventrouter

- Migration is on it's way to [ openshift/eventrouter](https://github.com/openshift/eventrouter)

- Deprecated: [heptiolabs/eventrouter](https://github.com/heptiolabs/eventrouter)

- [Grafana recommends eventrouter](https://grafana.com/blog/2020/07/21/loki-tutorial-how-to-send-logs-from-eks-with-promtail-to-get-full-visibility-in-grafana/)

- It seems abondoned, but from what ppl say it's [still working fine](https://github.com/heptiolabs/eventrouter/issues/126#issuecomment-781289073).

- The chart is in the process of [beeing moved to the bitnami charts](https://github.com/bitnami/charts/pull/4698) (see also https://github.com/heptiolabs/eventrouter/issues/121).

## kubewatch

Watch k8s events and trigger Handlers

https://github.com/bitnami-labs/kubewatch

- Last commit 2020-07 (as of 2021-03)
- [no matrix support so far](https://github.com/bitnami-labs/kubewatch/issues/245)

## Other alternatives

- [heapster eventer](https://github.com/kubernetes-retired/heapster/tree/master/events) - deprecated
- [redhat openshift eventrouter](https://docs.okd.io/latest/logging/cluster-logging-eventrouter.html)
- [kube-eventer](https://github.com/AliyunContainerService/kube-eventer) - maintained, but [no stdout sink which can be used by promtail](https://github.com/AliyunContainerService/kube-eventer/issues/181)
