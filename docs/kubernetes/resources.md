# Kubernetes resources

## Node resources

<https://github.com/kubernetes/kubernetes/issues/17512>

```
kubectl describe nodes | grep 'Name:\|Allocated' -A 5 | grep 'Name\|memory'
```

## Pod requests and limits

<https://blog.kubecost.com/blog/requests-and-limits/#our-solution>

## Calculate resource usage of pods

Current Cpu/Mem usage:

```
$ kubectl -n mastodon top pod -l app.kubernetes.io/name=elasticsearch --sum=true
```

The equivalent prometheus query for the above kubectl cmd:

```
export MATCH='namespace="mastodon", pod=~".*elasticsearch.*"
```

- Be aware: `image!=""` for memory queries does make a difference !

| Query | CPU (mil cores) | Mem (Mb) |
|---------------- | --------------- | --------------- |
| Current metrics | `sum(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{$MATCH}) by (pod)*1024` | `sum(container_memory_working_set_bytes{$MATCH, image!=''}/1024^2) by (pod)` |
| Average resource usage over time | `sum(rate(container_cpu_usage_seconds_total{$MATCH}[1w])) by (pod)*1024` | `avg_over_time(sum(container_memory_working_set_bytes{namespace="mastodon", pod=~".*elasticsearch.*", image!=""}) by (pod)[1w:])/1024^2` |
| Total sum over time | `sum(rate(container_cpu_usage_seconds_total{$MATCH}[1w]))*1024` | `sum(avg_over_time(sum(container_memory_working_set_bytes{$MATCH})[1w:]))/1024^2` |
