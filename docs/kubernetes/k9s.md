# k9s

- [Website](https://k9scli.io/)

Install binary from GH:

```
cd /tmp && wget https://github.com/derailed/k9s/releases/download/v0.27.4/k9s_Linux_amd64.tar.gz \
  && tar -xzf k9s_Linux_amd64.tar.gz && mv k9s /usr/local/bin
```

Using brew:

```
brew install derailed/k9s/k9s
```

## Usage

- How to [modify secret without worrying about base64 encode/decode](https://github.com/derailed/k9s/issues/1017#issuecomment-1328330026)

## Plugins

- [Docs: Plugins](https://k9scli.io/topics/plugins/)
- [Community Custom Plugins](https://github.com/derailed/k9s/tree/master/plugins)
