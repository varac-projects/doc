# lens

<https://docs.k8slens.dev/main/getting-started/>

## Install

### snap

Beware: [snap is lacking a major version behind](https://github.com/lensapp/lens/issues/5093)

```
sudo snap install kontena-lens --classic
```
