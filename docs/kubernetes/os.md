# Container optimized OSes

- [Top Minimal Container Operating Systems for Kubernetes](https://computingforgeeks.com/minimal-container-operating-systems-for-kubernetes/)
- [A Guide to Linux Operating Systems for Kubernetes](https://thenewstack.io/a-guide-to-linux-operating-systems-for-kubernetes/)
- https://www.reddit.com/r/devops/comments/iz4zf9/is_there_an_open_source_container_optimized_os/

## Talos

- [Talos](https://www.talos.dev/)
  The Kubernetes Operating System
- No SSH, no console, only API
- [No auto-upgrade](https://www.talos.dev/v1.3/talos-guides/upgrading-talos/#talosctl-upgrade)
- [terraform-libvirtd-talos](https://codeberg.org/x33u/terraform-libvirtd-talos)
- Most popular k8s distribution besides k3s in the k8s-at-home community

## Flatcar

- [Flatcar website](https://flatcar-linux.org/)
- Kinvolk, the company behind Flatcar got [acquired by Microsoft](https://kinvolk.io/blog/2021/04/microsoft-acquires-kinvolk/)
- Flatcar Container Linux is a drop-in replacement for CoreOS Container Linux
- [Running Flatcar Container Linux on libvirt](https://flatcar-linux.org/docs/latest/installing/vms/libvirt/)
  - [Terraform and libvirt](https://flatcar-linux.org/docs/latest/installing/vms/libvirt/)
- [Terraform](https://flatcar-linux.org/docs/latest/provisioning/terraform/)

Features:

- [minimal amount of tools to run container workloads](https://www.flatcar.org/docs/latest/container-runtimes/): Docker, Kubernetes
  - Open feature request: [Flatcar Podman extension](https://github.com/flatcar/Flatcar/issues/112)
- Automated atomic updates
- Immutable filesystem
- OS image is immutable (/usr is a read-only partition and
  there’s no package manager to install packages)
- Flatcar uses the USR-A and USR-B update mechanism, first introduced by ChromeOS

## Photon OS

- [Website](https://vmware.github.io/photon/)
- By vmware
- [No qcow2 images](https://github.com/vmware/photon/wiki/Downloading-Photon-OS)

## Fedora CoreOS (FCOS)

[Fedora CoreOS](https://getfedora.org/en/coreos?stream=stable)
is the official successor to CoreOS Container Linux

- [Docs](https://docs.fedoraproject.org/en-US/fedora-coreos/)
- [Issues](https://github.com/coreos/fedora-coreos-tracker)

Features:

- automatically-updating

### Installation

FCOS reads and applies the configuration file with Ignition.

- [What is ignition ?](https://coreos.com/ignition/docs/latest/what-is-ignition.html)
  https://github.com/coreos/ignition

#### Install with libvirt

https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/#\_launching_with_qemu_or_libvirt

#### Ignition file

https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/

```
docker pull quay.io/coreos/fcct:release
docker run -i --rm quay.io/coreos/fcct:release --pretty --strict < varac.fcc > varac.ign
```

#### Install

```
cd ~/kubernetes/os/coreos
wget https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/31.20200210.3.0/x86_64/fedora-coreos-31.20200210.3.0-qemu.x86_64.qcow2.xz
unxz fedora-coreos-31.20200210.3.0-qemu.x86_64.qcow2.xz
./install.sh
```

#### Run

Questions:

- sudo ?

Running procs after boot

- systemd
  - init
  - systemd-journal
  - systemd-logind
- NetworkManager
- chronyd
- sssd (System Security Services Daemon)
  - sssd_be
  - sssd_nss
- dbus-broker-launch / dbus-broker
- sshd
- zincati (OS update daemon)
- polkitd
- dhclient
- agetty

## Kairos

- [Website](https://kairos.io/)

## BalenaOS

- [BalenaOS](https://www.balena.io/os)

## Out of scope

- [Google Container-Optimized OS](https://cloud.google.com/container-optimized-os/docs)
- [AWS Bottlerocket](https://aws.amazon.com/de/bottlerocket/)
  Open Source OS for Container Hosting from Amazon

## Deprecated / outdates OSes

### Flow Linux

- [Floe Linux](https://floelinux.github.io/)
  Floe is a lightweight Linux distribution made specifically to run Linux containers.
  It uses Tiny Core Linux, runs completely from RAM and is a ~25 MB download\*.

### Kutter OS

- [Kutter OS](https://kutter-os.github.io/)
  The aim is to make a minimal OS for running Kubernetes.

### boot2podman

- [boot2podman](https://boot2podman.github.io/)

### HypriotOS

- [HypriotOS](https://blog.hypriot.com/about/#hypriotos:6083a88ee3411b0d17ce02d738f69d47):
  make container technology a first class citizen on ARM and IoT devices

### k3os

- see `./k3os.md`
- [The project is dead but ready to get into contributor mode](https://github.com/rancher/k3os/issues/846)

### CoreOS Container Linux

from https://coreos.com/os/eol/:

> End-of-life announcement for CoreOS Container Linux
> On May 26, 2020, CoreOS Container Linux will reach its end of life and will no longer receive updates. We strongly recommend that users begin migrating their workloads to another operating system as soon as possible.

https://coreos.com/os/docs/latest/

From https://en.wikipedia.org/wiki/Container_Linux:

> Container Linux (formerly CoreOS Linux) is an open-source lightweight operating system based on the Linux kernel and designed for providing infrastructure to clustered deployments, while focusing on automation, ease of application deployment, security, reliability and scalability

### Rancher OS

- [Docs](https://rancher.com/docs/os/v1.x/)

> Note that RancherOS 1.x is currently in a maintain-only-as-essential mode, and it is no longer being actively maintained at a code level other than addressing critical or security fixes.

## Unrelated

- [kured](https://github.com/kubereboot/kured): Kubernetes Reboot Daemon
