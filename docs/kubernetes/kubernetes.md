# Docs

- Awesome lists: see `../awesome-container-lists.md`

Etc:

- [0xacab infrastructure/platform_wg wiki](https://0xacab.org/infrastructure/platform_wg/k8/wikis/home)
- [0xacab riseup/hexacab/kubernetes wiki](https://0xacab.org/riseup/hexacab/kubernetes/wikis/home)

## Dashboard

```
kubectl proxy
https://localhost:8001/ui
```

## Kubernetes and Puppet

```
https://puppet.com/blog/managing-kubernetes-configuration-puppet
```
