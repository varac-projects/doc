# CIFS storage on kubernetes

## kubernetes-csi / csi-driver-smb

<https://github.com/kubernetes-csi/csi-driver-smb>

- As of 2020-09: Actively maintained (22 contributors, recent releases, 34
  stars)
- [Helm chart](https://github.com/kubernetes-csi/csi-driver-smb/tree/master/charts)

### Test pod

<https://github.com/kubernetes-csi/csi-driver-smb/blob/master/deploy/example/e2e_usage.md>
<https://github.com/kubernetes-csi/csi-driver-smb/blob/master/docs/driver-parameters.md>

- Test if cifs share works: `smbclient -U oas2_nucy //varac-nas/oas2_nucy`
  (pw see `server/varac-nas/user/oas2_nucy`)
- `kubectl -n varac create secret generic test-smb-creds --from-literal username=k8s --from-literal password="..."`

## juliohm CIFS volume driver

<https://github.com/juliohm1978/kubernetes-cifs-volumedriver>

- As of 2021-07: Actively maintained (6 contributors, recent releases, 77 ⭐)

- No helm chart, installation seems a bit bit clunky:

- requires `cifs-utils` installed on the host

```sh
apt-get install -y cifs-utils jq
cd kubernetes-cifs-volumedriver/
kubectl apply -f install.yaml
```

Verify:

```sh
kubectl -n default exec -it juliohm-cifs-volumedriver-installer-m96zq ls /flexmnt/juliohm~cifs/cifs
```

Remove provisioning container:

```sh
kubectl delete -f install.yaml
```

## Deprecated drivers

### Azure CIFS/SMB FlexVolume driver for Kubernetes (Deprecated)

> WARNING: This driver is in maintenance mode. Please use SMB CSI driver instead.

- <https://github.com/Azure/kubernetes-volume-drivers/tree/master/flexvolume/smb>
- [helm chart](https://github.com/Azure/kubernetes-volume-drivers/tree/master/flexvolume/smb/helm)

### fstab/cifs Flexvolume Plugin for Kubernetes

<https://labs.consol.de/kubernetes/2018/05/11/cifs-flexvolume-kubernetes.html>
<https://github.com/fstab/cifs>

- As of 2020-09: last commit was 2019-12 (2 contributors, no recent releases, 80
  stars)
