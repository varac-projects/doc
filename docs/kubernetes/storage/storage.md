# Container storage deployed as containers

- [OpenEBS vs. Rook(Ceph) vs. Longhorn and others](https://vitobotta.com/2019/08/06/kubernetes-storage-openebs-rook-longhorn-storageos-robin-portworx/)
- [OpenEBS](https://www.openebs.io/)
- [rook-ceph](https://ceph.com/community/rook-automating-ceph-kubernetes/)

## List PVCs

Show node where local-storage PVC is located for a single PVC:

```sh
kubectl -n ci get pvc repocache -o=jsonpath='{.metadata.name} {.spec.volumeName} {.metadata.annotations.volume\.kubernetes\.io/selected-node}'
```

Show nodes where local-storage PVCs are located for a multiple PVCs, sorted by
node:

```sh
kubectl get pvc -A -o=jsonpath='{range .items[*]}{.metadata.name} {.metadata.annotations.volume\.kubernetes\.io/selected-node}{"\n"}{end}' | sort -k 2
```
