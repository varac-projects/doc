# NFS storage on kubernetes

- <https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-volumes-example-nfs-persistent-volume.html>
- <https://josebiro.medium.com/shared-nfs-and-subpaths-in-kubernetes-19ade234896b>

## nfs-client-provisioner

<https://www.padok.fr/en/blog/readwritemany-nfs-kubernetes>

Downside:

- [No user/pw authentication, only host access control](https://github.com/kubernetes-retired/external-storage/issues/1265)
  <https://unix.stackexchange.com/questions/341854/failed-to-pass-credentials-to-nfs-mount>

  or maybe it works passing `user=…,pass=…` as mount options (see <https://unix.stackexchange.com/a/494912>) ?
