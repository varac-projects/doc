# Local path provisioners

## Rancher local path provisioner

- [GitHub](https://github.com/rancher/local-path-provisioner)

### Issues

- [Not working for subPath](https://github.com/rancher/local-path-provisioner/issues/4)
  - [Suggestion to switch to `openebs/dynamic-localpv-provisioner`](https://github.com/rancher/local-path-provisioner/issues/4#issuecomment-1315596924)

## Other local path provisioners

- [openebs/dynamic-localpv-provisioner](https://github.com/openebs/dynamic-localpv-provisioner)
