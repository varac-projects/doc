# Access modes

<https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes>

## Set PV reclaim policy

### Manually

<https://kubernetes.io/docs/tasks/administer-cluster/change-pv-reclaim-policy/>

```
kubectl patch pv <your-pv-name> -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
```

### Programatically by using a custom storage class

<https://medium.com/faun/kubernetes-how-to-set-reclaimpolicy-for-persistentvolumeclaim-7eb7d002bb2e>

# Delete persistant volumes

```
kubectl -n media delete --wait=false pvc emby-config
kubectl -n media delete --wait=false pv  pvc-c70d703d-cb60-11e9-bf6b-00505600c5be
```

PVs will change to `Terminating` state and will wait for finalizers.
Remove all finalizers from the pv metadata:

```
kubectl patch -p '{"metadata":{"finalizers": []}}' --type=merge pv pvc-d8de8930-bae6-11e9-a969-00505600c5be pvc-ce18ae68-c8fe-11e9-8a83-00505600c5be
```
