# Kubernetes operators

https://operatorhub.io/what-is-an-operator
https://operatorhub.io/
https://www.operatorcon.io/

## Overview

https://github.com/leszko/build-your-operator

## Operator sdk

https://sdk.operatorframework.io/

- ansible, helm, go

## Operator frameworks

- [KOPF for python](https://github.com/nolar/kopf): Most popular framework

## Operator lifecycle manager (OLM)

https://github.com/operator-framework/operator-lifecycle-manager
https://operator-framework.github.io/olm-book/
