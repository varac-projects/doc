# Kubernetes networking

## CoreDNS

- [Config docs](https://coredns.io/manual/toc/#configuration)

- Config in configmap `kube-system/coredns`

## Traffic viewer

<https://getmizu.io/>

## Network debug pod

- [Github](https://github.com/wbitt/Network-MultiTool)

[How to use it in k8s](https://github.com/wbitt/Network-MultiTool#kubernetes-1):

Create single pod - without a deployment:

```
$ kubectl run multitool --image=wbitt/network-multitool
```

Create a deployment:

```
$ kubectl create deployment multitool --image=wbitt/network-multitool
```

Then:

```
$ kubectl exec -it multitool /bin/bash
```
