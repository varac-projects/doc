# Jinja

- [Website](https://jinja.palletsprojects.com/)
- [Docs: Templates](https://jinja.palletsprojects.com/en/3.1.x/templates/)

## CLI usage

## jinja2-cli

- [jinja2-cli](https://github.com/mattrobenolt/jinja2-cli)

### Install

```sh
brew install jinja2-cli
```

or

```sh
pipx install jinja2-cli
```

### Usage

#### Yaml

```sh
cd ~/projects/templating/jinja
jinja2 indentation.j2 data.yml
```

### Other jinja cli tools

- [j2cli](https://github.com/kolypto/j2cli): Last commit 2019
- [jinja-cli](https://github.com/cykerway/jinja-cli): Last commit 2021-12

## Ansible

[Playboooks templating](https://docs.ansible.com/ansible/latest/user_guide/playbooks_templating.html)

> Ansible uses Jinja2 templating to enable dynamic expressions and access to
> variables
