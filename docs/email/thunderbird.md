# Thunderbird

## Nvim support

### external-editor-revived

- [GitHub](https://github.com/Frederick888/external-editor-revived/)
- Rust, recent updates
- [Thunderbird addon-store](https://addons.thunderbird.net/en-US/thunderbird/addon/external-editor-revived/)
- [AUR package](https://aur.archlinux.org/packages/external-editor-revived)
  - Installs TB extension _and_ native messaging host
- [stevenxxiu/external_editor_revived_markdown](https://github.com/stevenxxiu/external_editor_revived_markdown)
  - Script to convert to and from Markdown when editing.

[Installation](https://github.com/Frederick888/external-editor-revived/wiki/Linux#installation):

```sh
pamac install external-editor-revived
external_editor_revived
mkdir -p "$HOME/.mozilla/native-messaging-hosts"
external-editor-revived | tee "$HOME/.mozilla/native-messaging-hosts/external_editor_revived.json"
```

Restart Thunderbird.

[Configuration](https://github.com/Frederick888/external-editor-revived/wiki/Linux#configuration):
Only needed if defaults (`nvim`, `kitty` terminal) need to get changed.

### Other external editor plugins

- [Firevim](https://github.com/glacambre/firenvim)
  - [Currently broken](https://github.com/glacambre/firenvim/issues/1263)

## Troubleshooting

### Thunderbird is not responding

- [Rebuild the Global Database](https://support.mozilla.org/en-US/kb/rebuilding-global-database)
- Remove all `*.msf` index files
