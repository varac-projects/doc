# Curl

<https://debian-administration.org/article/726/Performing_IMAP_queries_via_curl>

```sh
curl --url "imap://localhost:1984" \
  --user "user:pw" \
  --request "EXAMINE INBOX"
```
