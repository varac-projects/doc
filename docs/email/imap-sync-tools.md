# Imap sync tools

- [mbsync vs OfflineIMAP](https://anarc.at/blog/2021-11-21-mbsync-vs-offlineimap/)

## Isync

- [Website](https://isync.sourceforge.io/)
- Last [release](https://sourceforge.net/projects/isync/files/isync/) 2024-08
- [Arch wiki](https://wiki.archlinux.org/title/Isync)
- Alternative to offlineimap

## Offlineimap

- [Website](https://www.offlineimap.org/)
- [GitHub](https://github.com/OfflineIMAP/offlineimap3)
  - Python
  - Last commit 2024-08, last tag 2021

Sync all:

```sh
offlineimap_concurrent.sh
```

Sync one account:

```sh
offlineimap -a varac.net -u blinkenlights
```

Delete a folder:

```sh
offlineimap -a varac.net --delete-folder Sent.2023
rm -rf ~/mail/varac.net/Sent/2023
```
