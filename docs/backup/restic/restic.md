# Restic

- [Github](https://github.com/restic/restic)
- [Docs](https://restic.readthedocs.io)
- Dockerfile included in main repo
  - [restic docker image](https://hub.docker.com/r/restic/restic) 10M+ pulls,
  - [borg docker image](https://hub.docker.com/r/borgbackup/borg) 35 puls
- Still no native [Support backup/snapshot metrics](https://github.com/restic/restic/issues/1878)

<https://acha.ninja/blog/encrypted_backup_shootout/>
compares bupstash, restic, borg backup and plain old tar + gzip + GPG
across a series of simple benchmarks.

## Usage

Export `RESTIC_REPOSITORY` and `RESTIC_PASSWORD`,
or source `access-all.sh` if using the [ansible_role_restic](https://github.com/roles-ansible/ansible_role_restic):

    . /usr/local/bin/access-all.sh

Backup:

    restic--verbose backup --tag /

Disk usage (`gdu` doesn't support `--exclude*` flags):

    sudo ncdu --exclude-caches --exclude-kernfs -X /etc/backup.exclude  /

Show remote repo version:

    restic cat config

Show latest snapshot files:

    restic ls latest

Restore:

    restic restore latest --target /tmp/restore --include /home/varac/.config
    restic restore a2c6d1e2 --target /tmp/restore --include '*watson'

## Features

### Compression

Available from [v0.14.0](https://github.com/restic/restic/releases/tag/v0.14.0)

> To upgrade in place run migrate upgrade_repo_v2 followed by prune.
> See the documentation for more details. The migration checks the repository
> integrity and upgrades the repository format, but will not change any data.
> Afterwards, prune will rewrite the metadata to make use of compression.

### Metrics

- [Support backup/snapshot metrics](https://github.com/restic/restic/issues/1878)
- [Collect metrics from rest-server](https://gist.github.com/simonswine/494e27e9aa38647a7cb735cc3ccc9da0)
- [Restic grafana dashboards](https://grafana.com/grafana/dashboards?search=restic)
- [Restic Backups with systemd and Prometheus exporter](https://blog.cubieserver.de/2021/restic-backups-with-systemd-and-prometheus-exporter/)

## Ansible roles

- [do1jlr/restic](https://galaxy.ansible.com/do1jlr/restic)
  - By far the most popular module in the Galaxy
  - Activly maintained, recent release

## restic rest-server

<https://github.com/restic/rest-server>

- Exports prometheus metrics

### rest-server helm chart

- [varacs helmchart](https://0xacab.org/varac/helmcharts/-/tree/master/restic-rest-server)
- [original helm chart](https://github.com/edjmao/charts/blob/master/rest-server)
- [docker image](https://hub.docker.com/r/restic/rest-server)

Manual jobs:

<https://hub.docker.com/r/restic/rest-server>

Enter restic container and:

    create_user resticuser XXXX

Issues

- Using nginx-ingress with restic-rest-server didn't work, it crashed while
  backing up. That's why I'm now using the plain rest server and NodePort +
  TLS managed by the rest server.

## S3 / Minio backend

### Create new serviceaccount per device

    export SVCACC_NAME=restic-panza
    export BUCKET=$SVCACC_NAME
    jq ".Statement[0].Resource = [\"arn:aws:s3:::${BUCKET}*\"]" \
      ~/projects/cloud/storage/minio/policy.readwrite.json > /tmp/policy-new.json
    mc admin user svcacct add pinenas-pub-varac varac \
      --name $SVCACC_NAME \
      --description "Restic backup for $SVCACC_NAME" \
      --policy /tmp/policy-new.json \
      --access-key $SVCACC_NAME

Verify:

    mc admin user svcacct info pinenas-pub-varac $SVCACC_NAME
