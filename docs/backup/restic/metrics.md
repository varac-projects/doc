# Restic prometheus metrics

* [dermotduffy/restic-exporter](https://github.com/dermotduffy/restic-exporter)
* [Shell script, parses the jounal](https://blog.cubieserver.de/2021/restic-backups-with-systemd-and-prometheus-exporter/#reporting-failures)
  * Shell script, parses the jounal for restic plain output (no json)
  * see `~/projects/backup/restic/exporter/restic-log-exporter/`
  * exports quite a few useful metrics
  * textfile-exporter based
* [pinpox/restic-exporter](https://github.com/pinpox/restic-exporter)
  * Go
  * "real" exporter
* [j6s/restic-exporter](https://github.com/j6s/restic-exporter)
  * Go, only expxorts 1 metric (`restic_snapshot_timestamp`)
  * textfile-exporter based
* [varac-projects/ansible-role-autorestic autorestic-metrics.sh](https://0xacab.org/varac-projects/ansible-role-autorestic/-/blob/main/files/autorestic-metrics.sh)
