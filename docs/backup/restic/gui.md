# Restic GUIs

See also [../backup.md](../backup.md) (i.e. Deja-Dup)

* [resticguigx](https://gitlab.com/stormking/resticguigx)
* [restique](https://git.srcbox.net/stefan/restique)
* [restic-gui](https://github.com/lagasi/restic-gui), last commit 2022
* [restatic](https://github.com/Mebus/restatic) Vorta-fork, abandoned
* [restica](https://github.com/TheChiefMeat/Restica) unmaintained
* [relica](https://relicabackup.com/): Propriatary, Paid subscription service
* [jarg](https://github.com/rgwch/jarg): unmaintained
