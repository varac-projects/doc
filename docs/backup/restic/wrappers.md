# Restic client wrappers / helpers

* [resticpy](https://mtlynch.github.io/resticpy/)
  * Python client
  * [Last commit](https://github.com/mtlynch/resticpy) 2021-04

## Autorestic

* <https://github.com/cupcakearmy/autorestic>
* Go, 343 ⭐, 48 releases as of 2021-11
* Container images:
  * [cupcakearmy/autorestic](https://hub.docker.com/r/cupcakearmy/autorestic/tags)
    * maintained, proper tags
  * 3rd-party image [pascaliske/docker-autorestic](https://github.com/pascaliske/docker-autorestic)
    * Source up to date, maintained, but published image lags behind
* [autorestic systemd units](https://gitlab.com/py_crash/autorestic-systemd-units)
* [Quick start](https://autorestic.vercel.app/quick)

### Installation

[Installation](https://autorestic.vercel.app/installation)

    brew install autorestic

or download the binary from the
[latest release](https://github.com/cupcakearmy/autorestic/releases/latest), i.e.:

    wget -O /tmp/autorestic.bz2 https://github.com/cupcakearmy/autorestic/releases/download/v1.7.1/autorestic_1.7.1_linux_amd64.bz2
    bzip2 -d autorestic.bz2
    chmod +x autorestic
    sudo mv autorestic /usr/local/bin

Config file: `~/.autorestic.yml`

### Autorestic with ansible

* [autorestic docs: Ansible roles](https://autorestic.vercel.app/community)
* [varac-projects/ansible-role-autorestic](https://0xacab.org/varac-projects/ansible-role-autorestic)
  * Forked from `agate/autorestic-backup-ansible-role`
* [agate/autorestic-backup-ansible-role](https://code.agate.blue/agate/autorestic-backup-ansible-role)
  * [Blog post: autorestic with ansible](https://agate.blue/2019/12/03/a-quest-for-backups-a-journey-through-automation.html)
* [ItsNotGoodName/ansible-role-autorestic](https://github.com/ItsNotGoodName/ansible-role-autorestic)
  * Last commit 2022-05
  * No support for prune/check yet
  * Creates dedicated user
  * Installs restic from downloaded archive
  * uses cron
  * Uses `copy content` to deploy config file
  * Only 2 tags, no releases
* [agate/autorestic-backup-ansible-role](https://code.agate.blue/agate/autorestic-backup-ansible-role):
  * Last commmit 2022-01
  * Can't sign-in with gitlab or github, can't sign-up
  * No `global` section configurable in template
  * Uses cron
* [FuzzyMistborn/ansible-role-autorestic](https://github.com/FuzzyMistborn/ansible-role-autorestic)
  * Last commit 2021-12
  * Uses `copy content` to deploy config file
  * No cron/systemd timer
* [adsanz/ansible-restic-role](https://github.com/adsanz/ansible-restic-role)
  * Last commit Aug 2021

### Issues

* [Show native restic's output](https://github.com/cupcakearmy/autorestic/issues/147)
* Related: [Check and init command ignore YAML options](https://github.com/cupcakearmy/autorestic/issues/198)
* [InfluxDB metrics](https://github.com/cupcakearmy/autorestic/issues/104)
* [Allow backends to fail/be skipped](https://github.com/cupcakearmy/autorestic/discussions/185)

## Other wrappers

as of 2021-11:

* [runrestic](https://github.com/sinnwerkstatt/runrestic):
  * Python, 101 commits, 54 ⭐, 33 Releases, 6 contributors
  * support logging prometheus metrics with a textfile collector
  * [Hangs unresponsive if restic breaks](https://github.com/sinnwerkstatt/runrestic/issues/41)
* [crestic](https://github.com/nils-werner/crestic): Python, 35 ⭐, 7 releases,
  0 contributors, last commit 2021-08
* <https://github.com/mtlynch/resticpy>: python, 12 ⭐
* <https://github.com/peterrus/restic-wrapper>: shell, 15 commits
* <https://github.com/sephiroth1395/restic-PyBM>: 5 ⭐, python, 52 commits
* [rustic-rs/rustic](https://github.com/rustic-rs/rustic): Active, no init

### Unmaintained

* <https://github.com/maxkueng/restique>: Last commit 2021-02
* [bnavetta/rustic](https://github.com/bnavetta/rustic): Rust, 26 commits, 5 ⭐, 3 Releases, 0
  contributors, last commit 2020-08
* <https://github.com/wrohdewald/restaround>: Last commit 2019-12
* <https://github.com/OT-CONTAINER-KIT/restic>: Last commit 2020
* [resticpy](https://github.com/mtlynch/resticpy)

## Metrics

* [Support backup/snapshot metrics](https://github.com/restic/restic/issues/1878)
* [Collect metrics from rest-server](https://gist.github.com/simonswine/494e27e9aa38647a7cb735cc3ccc9da0)
* [Restic grafana dashboards](https://grafana.com/grafana/dashboards?search=restic)

## Clients as docker container

### Resticker

<https://github.com/djmaze/resticker>

* Active, recent release, 12 Contributors, 93 ⭐
* No [Prometheus metrics](https://github.com/djmaze/resticker/issues/26)
  * Workaround: <https://medium.com/@zoispag/collect-restic-metrics-when-using-resticker-cf862f87fc41>
* Official [docker image](https://hub.docker.com/r/mazzolino/restic/tags)
  active and multi-arch
* Issues
  * [restic repo initialised twice when starting up docker with both backup/pruneprune actually runs](https://github.com/djmaze/resticker/issues/48)
  * Often locked, and need to get unlocked

#### Resticker usage

    systemctl status docker-compose@resticker.service

Logs

    docker logs resticker_backup

List snapshots:

    docker exec resticker_backup restic snapshots

Unlock:

    docker exec resticker_prune restic unlock

##### Restore

List snapshots:

      docker exec resticker_backup restic snapshots

#### Stale docker wrappers

### restic-backup-docker

<https://github.com/lobaro/restic-backup-docker>

* Active, but last release 2020-05 (as of 2021-01)
* Official [docker image](https://hub.docker.com/r/lobaro/restic-backup-docker/tags)
* Cron shedule
* Doesn't have an option to start right after a `docker up`

### restic-robot

<https://github.com/Southclaws/restic-robot>

* Stale (last commit 2021-02 as of 2021-07), no release
* <https://hub.docker.com/r/southclaws/restic-robot>
* Cron shedule
* prometheus exporter `curl localhost:8080/metrics`

### Bivac

<https://camptocamp.github.io/bivac/>

* tried this in 2020, flaky. Might have gotten better
* Based on restic
* [Github](https://github.com/camptocamp/bivac/)
* Docker setup
* auto-backup of volumes
* handles db dumps
* [Exports prometheus metrics](https://github.com/camptocamp/bivac/wiki/Monitoring)
* [docker image](https://hub.docker.com/r/camptocamp/bivac)
* [bivac helm chart](https://hub.helm.sh/charts/camptocamp/bivac)

### Velero

* Supports exporting Prometheus metrics
* Only for kubernetes

## restic rest-server

<https://github.com/restic/rest-server>

* prometheus metrics

## rest-server helm chart

* [varacs helmchart](https://0xacab.org/varac/helmcharts/-/tree/master/restic-rest-server)
* [original helm chart](https://github.com/edjmao/charts/blob/master/rest-server)
* [docker image](https://hub.docker.com/r/restic/rest-server)

Manual jobs:

<https://hub.docker.com/r/restic/rest-server>

Enter restic container and:

    create_user resticuser XXXX

Issues

* Using nginx-ingress with restic-rest-server didn't work, it crashed while
  backing up. That's why I'm now using the plain rest server and NodePort +
  TLS managed by the rest server.
