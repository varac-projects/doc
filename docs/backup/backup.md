# Backup apps

- [npbackup](https://github.com/netinvent/npbackup)
  - based on Restic

## GUI apps

- [List of backup GUIs](https://wiki.ubuntuusers.de/Datensicherung/#Grafisch)
- [restic list of similar apps](https://github.com/restic/others)
- [Kopia](./kopia.md)
- [Timeshift](https://github.com/linuxmint/timeshift)
  System restore tool for Linux. Creates filesystem snapshots using rsync+hardlinks, or BTRFS snapshots

### Pika backup

- <https://flathub.org/apps/details/org.gnome.World.PikaBackup>
- <https://gitlab.gnome.org/World/pika-backup>
- Based on Borg backup
- [No S3 backend support](https://gitlab.gnome.org/World/pika-backup/-/issues/338),
  since Borg doesn't support any S3 backends neither

### Deja Dup

- <https://wiki.gnome.org/Apps/DejaDup>
- <https://gitlab.gnome.org/World/deja-dup>
- <https://wiki.ubiuntuusers.de/D%C3%A9j%C3%A0_Dup/>
- Traditionally based on [duplicity](https://duplicity.gitlab.io/),
  but from v4.3 on, it supports restic as backend
- Still, [no S3 restic support](https://gitlab.gnome.org/World/deja-dup/-/issues/389)

Debug deja-dup:

    DEJA_DUP_DEBUG=1 deja-dup

### Duplicacy

- UI is a paid product:/

### Duplicati

- [Github](https://github.com/duplicati/duplicati)
- [Much slower than Kopia](https://github.com/duplicati/duplicat)
