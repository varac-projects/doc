# Kopia backup

- [Website](https://kopia.io)
- [Kopia docs](https://kopia.io/docs/)
  - [Ransomware Protection](https://kopia.io/docs/advanced/ransomware-protection/#what-is-ransomware-and-why-do-we-need-extra-protections)
- [Kopia on GitHub](https://github.com/kopia/kopia)
- [Benchmarking Kopia: Architecture, Scale, and Performance](https://www.kasten.io/kubernetes/resources/blog/benchmarking-kopia-architecture-scale-and-performance)
- [Velero is moving from Restic to Kopia](https://github.com/vmware-tanzu/velero/issues/4538)
- [Kopia vs Duplicacy](https://www.reddit.com/r/Backup/comments/opu1ep/kopia_vs_duplicacy/)
- Config at `/.config/kopia/`
  - Default repository config at `/.config/kopia/repository.config`
  - The password to the repository is stored in operating-system specific credential storage
    (KeyRing on Linux). If the KeyRing is not available, the password
    will be stored base64-encoded in `~/.config/kopia/repository.config.kopia-password`

## Issues / PR

- [Provide an official systemd service & timer file](https://github.com/kopia/kopia/issues/2685)

## Install

[Installatio docs](https://kopia.io/docs/installation)

Kopia's apt repo supports also RPi OS (armv7l) as well as RockPro64 (aarch64)

```sh
curl -s https://kopia.io/signing-key | sudo gpg --dearmor -o /usr/share/keyrings/kopia-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/kopia-keyring.gpg] http://packages.kopia.io/apt/ stable main" | sudo tee /etc/apt/sources.list.d/kopia.list
sudo apt update
sudo apt install kopia-ui kopia
```

or:

UI: `flatpak install KopiaUI`
cli: `brew install kopia`

Arch: `pamac install kopia-bin`

## Usage

- [Getting started / cli](https://kopia.io/docs/getting-started/#kopia-cli)

### Multiple repo configs

Connect repo:

```sh
kopia repository connect s3 \
  --access-key=$(gopass show --password token/s3.k.varac.net/varac/velero/accesskey) \
  --secret-access-key=$(gopass show --password token/s3.k.varac.net/varac/velero/secretkey) \
  --bucket=velero --endpoint=s3.k.varac.net \
  --prefix=k.varac.net-kopia/kopia/vaultwarden/
```

The above cmd will create 2 config file in `~/.config/kopia/`:

- `repository.config`
- `repository.config.kopia-password`

Rename them so they are associated with their backup profile:

```sh
mv repository.config repository-velero-plausible.config
mv repository.config.kopia-password repository-velero-plausible.config.kopia-password
```

Then use the new config files:

```sh
export KOPIA_CONFIG_PATH=~/.config/kopia/repository-velero-plausible.config
kopia repository status -t -s
```

List snapshots:

```sh
kopia snapshot list
```

Show diff between snapshots:

```sh
kopia diff kb9a8420bf6b8ea280d6637ad1adbd4c5 ke2e07d38a8a902ad07eda5d2d0d3025d
```

List files in snapshot:

```sh
kopia ls -l kb9a8420bf6b8ea280d6637ad1adbd4c5
```

### Backup

#### Ignore files

- Kopia honors [CACHEDIR.TAG files](https://github.com/kopia/kopia/issues/564)

### Restore

> Mounting is currently the recommended way of restoring files/directories from snapshots.
> However, you can also use the kopia snapshot restore command to restore files/directories from snapshots.

```sh
kopia mount k2a873c512333066f449eea7b75c750ae /tmp/mnt
cp /tmp/mnt/… /tmp/restore
```

## Monitoring

- Prometheus [kopia-exporter](https://github.com/alvistar/kopia-exporter)
- the kopia-exporter is about to get merged into Kopia
- [FR: Prometheus metrics from kopia server](https://github.com/kopia/kopia/issues/609)
  - [PR: feat(server): snapshot metrics](https://github.com/kopia/kopia/pull/4100)

## Ansible

- [No ansible role in the Galaxy](https://galaxy.ansible.com/search?deprecated=false&keywords=kopia)
- [bashirsouid/AnsibleKopia](https://github.com/bashirsouid/AnsibleKopia)
- [devops-works/ansible-kopia](https://github.com/devops-works/ansible-kopia):
  Only supports GCS
