# Traefik

<https://www.digitalocean.com/community/tutorials/how-to-use-traefik-as-a-reverse-proxy-for-docker-containers-on-ubuntu-18-04>

## Generate admin pw

    htpasswd -nb admin 'PASSWORD'
    ansible-vault encrypt_string "HTPASSWD_ENCR_PW" --name traefik_admin_pw >> host_vars/foo

Show cert names:

    jq .letsencrypt.Certificates[].domain < letsencrypt/acme.json

## Dump certs

<https://github.com/ldez/traefik-certs-dumper>
<https://github.com/ldez/traefik-certs-dumper/blob/master/docs/docker-compose-traefik-v1.yml>

## Enforce cert-renewal

- [Manually trigger Traefik to generate letsencrypt certificate using API](https://github.com/traefik/traefik/issues/3652)
- [Enforce Certificate Renewal](https://github.com/traefik/traefik/issues/6418)

Options:

- Remove `acme.json` and restart traefik (beware of LE rate limiting by mass renewal all certs)
- Manually remove cert + key from `acme.json` and restart traefik
- Use <https://hub.docker.com/r/containous/acme-fixer>

## Http to https redirect

- [HTTP to HTTPS redirects with Traefik](https://jensknipper.de/blog/traefik-http-to-https-redirect/)
  Per domain, for a single application only or globally for all containers

## IP whitelist

- [Docs: Ip whitelist](https://doc.traefik.io/traefik/middlewares/http/ipwhitelist/#configuration-examples)
  - Only works in K3s with `forwardedHeaders.insecure` and `proxyProtocol.insecure`:
    [Traefik Kubernetes Ingress and X-Forwarded- Headers](https://medium.com/@_jonas/traefik-kubernetes-ingress-and-x-forwarded-headers-82194d319b0e)
