# Minishift

## Local minishift installation on laptop

The server is accessible via web console at:
    https://192.168.42.50:8443

You are logged in as:

    User:     developer
    Password: <any value>

To login as administrator:

    oc login -u system:admin

SSH Access:

    minishift ssh

## Install minishift

https://docs.openshift.org/latest/minishift/getting-started/installing.html

    minishift start

## Remote installation

    minishift start --public-hostname openshift.exmaple.org \
      --routing-suffix apps.openshift.exmaple.org \
      --cpus 4 --memory '8GB'--disk-size '100GB'

## Remote minishift installation without proper DNS

https://blog.chmouel.com/2017/06/09/deploying-minishift-on-a-remote-laptop/

    minishift start --public-hostname localhost \
      --routing-suffix 127.0.0.1.nip.io \
      --cpus 4 --disk-size 100 \
