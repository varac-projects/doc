# Upstream container images

## Commonly used

- [curlimages/curl](https://hub.docker.com/r/curlimages/curl)
  - Busybox: wget
  - No Bash
- [bash](https://hub.docker.com/_/bash)
  - Busybox: wget
  - No curl
- [debian:stable-slim](https://hub.docker.com/_/debian/tags)
  - Bash
  - No curl, no wget
