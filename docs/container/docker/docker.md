# Docker

## Install

### Debian

<https://docs.docker.com/engine/install/debian/>

    sudo -i
    apt remove docker docker-engine docker.io containerd runc
    apt install curl apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"> /etc/apt/sources.list.d/docker.list
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo tee /etc/apt/trusted.gpg.d/docker.asc
    apt update
    apt install docker-ce

## Proxy

* [HTTP/HTTPS proxy docs](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)

## Manage Docker as a non-root user

<https://docs.docker.com/engine/installation/linux/linux-postinstall/>

    sudo usermod -aG docker $USER
    newgrp docker

## Run docker

    docker info
    docker run hello-world

## Run image

run in foreground

    docker run -p 3128:8000 varac/squid_deb_proxy

run shell

    docker run -i -t --entrypoint /bin/bash varac/soledad

### Options

    -d    detach
    -P    Publish all exposed ports to random ports on the host interfaces.
          The default is false.
    -e CACHE_ANY=false  Set environment variables

## Examples

    docker run -d -p 5984:5984 --name couchdb couchdb

## Remove

. <http://stackoverflow.com/questions/17236796/how-to-remove-old-docker-io-containers>

## All stopped containers

<https://docs.docker.com/engine/reference/commandline/container_prune/>

    docker container prune

## All old containers, images, volumes etc

<https://docs.docker.com/engine/reference/commandline/system_prune/>

    docker system prune
    docker system prune --force

## all shutdown containers

    docker rm `docker ps --no-trunc -aq`

## all containers including running ones

    docker rm -f `docker ps --no-trunc -aq`

## all images

    docker rmi $(docker images -q)
    docker rmi $(docker images -q) --force

## based on certain image

    docker rm  $(docker ps -a | awk '/myimage:mytag/{print $1}')

## Run a new container from an image

    docker run -d -p 5984:5984 --name couchdb couchdb
    curl localhost:5984

with a bash:

    docker run -i -t useragent:upgrade_new_proxy /bin/bash

as proc:

    docker run -d -P \
      -v /home/varac/thoughtworks/git/useragent-docker/config:/root/pixelated \
      --publish=0.0.0.0:3333:3333 useragent:easy_install_pysqlcipher \
      /usr/bin/pixelated-user-agent --config /root/pixelated/pixelated.conf /bin/bash

## Test network access from within a container

    docker run -it debian sh -c 'ping -c 1 -W 2 1.1.1.1'

Test network resolution:

    docker run -it debian sh -c 'timeout 2 ping -c 1 ix.de'

## start a container and start bash

    docker run -i -t useragent:latest /bin/bash

## run cmd in existing container

    docker exec -it squid_deb_proxy /bin/bash

## Kill container

    docker kill couchdb

## Start existing container

    docker start couchdb

## Commit

    docker commit 79189911f3f6 apt-cacher-ng:latest

## List files in container

    docker export 4ec52b6023d2 | tar -tf -

## Show history of image build

    docker image history --no-trunc traefik:1.7.9 > /tmp/image_history

## docker ps

Include size of container:

    docker ps --size

## docker output formatting

* [Configure default format](https://container42.com/2016/03/27/docker-quicktip-7-psformat/)

[Output only specific fields](https://docs.docker.com/engine/reference/commandline/ps/#formatting):

    docker ps --format "table {{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Size}}\t{{.Names}}" \
      | cut -c-$(tput cols)

Or put the size at front:

    docker ps --format "table {{.Size}}\t{{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Names}}" \
      | cut -c-$(tput cols)

## docker inspect

<https://thispointer.com/how-to-get-ip-address-of-running-docker-container-from-host-using-inspect-command/>

List all running containers together with their IPs:

    docker inspect --format='{{.Name}} {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
      $(docker ps -q)

## Disk usage

Given this cluster disk usage:

    /dev/xvda1       30G   26G  4.1G  87% /

But `du` only schows 18G usage:

    root@master:~# du -sch /
    18G     /
    18G     total

The problem is that some deleted files from the overlay filesystem are not
completly freed, as shown with `lsof`:

    root@master:~# lsof -l | grep '(deleted)' | head
    dockerd    8768                       root   29w      REG              202,1 16151995496   21692420
      /var/lib/docker/containers/5e415309f9e1cd963e8aa535426cc7bdbb536214acd82a92611eb3b5cc697f23/5e415309f9e1cd963e8aa535426cc7bdbb536214acd82a92611eb3b5cc697f23-json.log
      (deleted)
    dockerd    8768  3759 dockerd         root   29w      REG              202,1 16151995496   21692420
      /var/lib/docker/containers/5e415309f9e1cd963e8aa535426cc7bdbb536214acd82a92611eb3b5cc697f23/5e415309f9e1cd963e8aa535426cc7bdbb536214acd82a92611eb3b5cc697f23-json.log
      (deleted)
    dockerd    8768  3761 dockerd         root   29w      REG

Those files were deleted from the container host from `/var/lib/…`.

For a real disk usage investigation you either need to restart
docker containers which hold up removed files or reboot the system.
