# How to get the IP of the docker host inside a container

Starting with v19.03.6, you can use query DNS for `host.docker.internal`:
* [Support host.docker.internal in dockerd on Linux](https://github.com/moby/moby/pull/40007/files)

`docker run` also support the `--add-host` flag to add extra host/ip
combinations which are then added to `/etc/hosts`, and `docker-compose` the
`add-host` key.

# DNS search domains

When DNS search domains are defined in `/etc/resolv.conf` (inside the container):

		/prometheus $ cat /etc/resolv.conf
		search local.moewe-altonah.de. moewe-altonah.de.
		nameserver 127.0.0.11
		options ndots:0

, a simple resolving of a docker container name like `cadvisor` wont work:

		/prometheus $ ping cadvisor
		ping: bad address 'cadvisor'

As seen in below debugging section it then tries to query the upstream DNS resolver
of the host for all search domains.

In this case, adding a dot at the end of the name solves the issue:



# Debugging DNS issues

[Embedded DNS not resolving hostname](https://github.com/docker/for-linux/issues/179#issuecomment-349864855)

* Put docker into [debug mode](https://docs.docker.com/config/daemon/#enable-debugging)
* Grep the journal for DNS resolving msgs:

	DNS resolution for `ping cadvisor` with enabled search domains:

    ```
    $ sudo journalctl -fu docker -n0 |grep -i resolve
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.061997081+01:00" level=debug msg="Name To resolve: cadvisor.local.moewe-altonah.de."                                      Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.062383101+01:00" level=debug msg="[resolver] query cadvisor.local.moewe-altonah.de. (AAAA) from 172.22.0.2:59229, forwarding to udp:10.27.192.1"
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.062871196+01:00" level=debug msg="[resolver] external DNS udp:10.27.192.1 responded with NXDOMAIN for \"cadvisor.local.moewe -altonah.de.\""
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.063286831+01:00" level=debug msg="Name To resolve: cadvisor.moewe-altonah.de."
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.063565747+01:00" level=debug msg="[resolver] query cadvisor.moewe-altonah.de. (AAAA) from 172.22.0.2:48598, forwarding to ud p:10.27.192.1"
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.063952506+01:00" level=debug msg="[resolver] external DNS udp:10.27.192.1 did not return any AAAA records for \"cadvisor.moe we-altonah.de.\""
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.064579138+01:00" level=debug msg="Name To resolve: cadvisor.local.moewe-altonah.de."
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.064830021+01:00" level=debug msg="[resolver] query cadvisor.local.moewe-altonah.de. (A) from 172.22.0.2:49788, forwarding to udp:10.27.192.1"
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.065222657+01:00" level=debug msg="[resolver] external DNS udp:10.27.192.1 responded with NXDOMAIN for \"cadvisor.local.moewe -altonah.de.\""
    Feb 06 11:30:20 brix dockerd[2198]: time="2020-02-06T11:30:20.065712077+01:00" level=debug msg="Name To resolve: cadvisor.moewe-altonah.de."
    Feb 06 11:30:23 brix dockerd[2198]: time="2020-02-06T11:30:23.103438885+01:00" level=debug msg="[resolver] lookup name cadvisor. present without IPv6 address"
		```

DNS resolution for `ping cadvisor.` with enabled search domains:

		```
		Feb 06 11:38:49 brix dockerd[2198]: time="2020-02-06T11:38:49.166102911+01:00" level=debug msg="Name To resolve: cadvisor."
		Feb 06 11:38:49 brix dockerd[2198]: time="2020-02-06T11:38:49.166228098+01:00" level=debug msg="[resolver] lookup name cadvisor. present without IPv6 address"
		Feb 06 11:38:49 brix dockerd[2198]: time="2020-02-06T11:38:49.166744828+01:00" level=debug msg="Name To resolve: cadvisor."
		Feb 06 11:38:49 brix dockerd[2198]: time="2020-02-06T11:38:49.166842977+01:00" level=debug msg="[resolver] lookup for cadvisor.: IP [172.22.0.6]"
		```
