# Docker smtp images

* [namshi/smtp](https://hub.docker.com/r/namshi/smtp):
  exim4 light & buster, last updated 2019-11, autobuilds, Pulls10M+
  no volumes: persistence ?
* [freinet/postfix-relay](https://hub.docker.com/r/freinet/postfix-relay):
  postfix, stretch, s6, last updated 2019-11

## Outdated

* [applariat/tx-smtp-relay](https://hub.docker.com/r/applariat/tx-smtp-relay):
  Used by [halkeye/postfix helm chart](https://hub.helm.sh/charts/halkeye/postfix)
  No Dockerfile found, last Update 2016!
* [cloudposse/postfix](https://hub.docker.com/r/cloudposse/postfix):
  `ubuntu:14.04`, updated 2y ago, no builds.
* [catatnight/postfix](https://hub.docker.com/r/catatnight/postfix):
  Updated 4 years ago, ubuntu:trusty
