# Docker compose

## Install

<https://docs.docker.com/compose/cli-command/#install-on-linux>

    apt install docker-compose-plugin

### OLD

### On arm

* [docker/compose image for armv7 / arm64v8](https://github.com/docker/compose/issues/6831#issuecomment-891374255)

## Disable autostart of docker-compose project

<https://stackoverflow.com/questions/41036273/disable-autostart-of-docker-compose-project>

When you find the compose.yml:

    docker-compose down

To permanently kill unknown containers:

    docker update --restart=no CONTAINER-ID
    docker kill CONTAINER-ID

# docker-compose with systemd service unit

<https://philipp-weissmann.de/docker-compose_mit_systemd/>
<https://github.com/docker/compose/issues/4266>

## Issues

* Containers can't start because a stale docker-proxy is allocating the port,
  even when no containers are running.
  Experienced with docker-compose `1.21.0-3` from buster, solved by upgrading
  to `1.25.0-1` from testing.

## Init container

* [Release notes 1.29 - initContainer information missing ](https://github.com/docker/docs/issues/12633)
* [init container example on stackoverflow](https://stackoverflow.com/a/70327319)
* [init container example in Github repo](https://github.com/zeitounator/compose-init-demo)
