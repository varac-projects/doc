# MariaDB in docker

https://hub.docker.com/_/mariadb

Enter cont

    docker run -it --network some-network --rm mariadb mysql -hsome-mariadb -uexample-user -p

Create dump:

    docker exec some-mariadb sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /some/path/on/your/host/all-databases.sql

Restore dump:

    docker exec -i some-mariadb sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < /some/path/on/your/host/all-databases.sql
    docker exec -i mariadb-drupal sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" drupal' < DB2027164-drupal-2019-09-18.sql
