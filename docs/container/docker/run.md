# Run a temp container and attach to bash

    $ docker run --rm -dit --name debian1 --network backend debian bash
    $ docker attach debian1
