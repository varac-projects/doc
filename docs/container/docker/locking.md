# Dockerfile locking tools

## tern

https://github.com/tern-tools/tern

## docker-lock

https://github.com/safe-waters/docker-lock

* Only locks images, not packages installed
