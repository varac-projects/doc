# Backup postgres in container

https://devopsheaven.com/postgresql/pg_dump/databases/docker/backup/2017/09/10/backup-postgresql-database-using-docker.html

    docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql

# Restore backup in container

    cat your_dump.sql | docker exec -i your-db-container psql -U postgres
