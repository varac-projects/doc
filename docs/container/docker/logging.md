# Docker logging options

See also `../kubernetes/logging.md` for kubernetes logging options,
including `loki`.

https://docs.docker.com/config/containers/logging/configure/

## Options to consider which come integrated with docker:

* [fluentd](https://docs.docker.com/config/containers/logging/fluentd/)
* [Graylog Extended Format logging driver (gelf)](https://docs.docker.com/config/containers/logging/gelf/):
  Suitable for graylog and logstash
* [journald](https://docs.docker.com/config/containers/logging/journald/):
  Log to systemd-journald. rsyslog can be configured to log journal msgs to a remote
  systlog server *without* blocking docker containers from starting.
* [syslog](https://docs.docker.com/config/containers/logging/syslog/)
  Log directly to a remote syslog server. Downside: Remote server must be up
  otherwise no container will start:

    `dockerd[767]: time="2020-03-09T13:57:02.874990220+01:00" level=error msg="Failed to start container d5884f62e5b01a43be49251eb7d8db6468d6d0b2f19775824213010741c08669: failed to initialize logging driver: dial tcp 10.27.192.52:1514: connect: connection refused"`

### fluentd

https://docs.fluentd.org/how-to-guides


### Logstash

integrates in the ELK stash (Elasticsearch, Logstash and Kibana)

### Greylog

> Graylog is a powerful, free, open-source log management and analysis tool that can be used for monitoring SSH logins and unusual activity to debugging applications. It is based on Java, Elasticsearch, and MongoDB and provides a beautiful web interface for centralized log management and log analysis.

* No native debian packages, but from the [graylog deb repo](https://docs.graylog.org/en/latest/pages/installation/operating_system_packages.html)
* Clients log to the greylog server using [rsyslog remote UDP](https://www.linode.com/docs/uptime/monitoring/how-to-install-and-configure-graylog2-on-debian-9/#access-graylog)
  [Sending syslog from Linux systems into Graylog](https://github.com/Graylog2/graylog-guide-syslog-linux)

> These instructions configure rsyslog and syslog-ng to send log messages unencrypted over the network. This is generally not recommended on public networks.

#### Clients authentication

seems to be a bit complicated to setup:

* [Using beats](http://docs.graylog.org/en/3.0/pages/secure/sec_graylog_beats.html)
* or [rsyslog over TLS](https://community.graylog.org/t/rsyslog-over-tls/263)


## Not integrated natively

### Loki

A lightweight approach when you already have grafana installed.

The [recommended way of using loki with plain docker](https://grafana.com/blog/2019/07/15/lokis-path-to-ga-docker-logging-driver-plugin-support-for-systemd/)
is the [loki docker-driver](https://github.com/grafana/loki/tree/master/cmd/docker-driver).
The docker-driver also has support for non-containerized application logs which
log directly to [systemd](https://grafana.com/blog/2019/07/15/lokis-path-to-ga-docker-logging-driver-plugin-support-for-systemd/#support-for-systemd).
??? Unfortunatly the `loki docker-driver` is not available for arm (Raspi). ???  (Really ???)

There's also the [Loki docker usage](https://github.com/grafana/loki/blob/master/production/README.md#run-locally-using-docker),
however it looks like you don't get service discovery with this approach.
There's also this approach: https://github.com/swarmstack/loki/tree.

#### Promtail

[Promtail will not start when running in docker on a Raspberry Pi](https://github.com/grafana/loki/issues/1575)

#### Clients authentication

Using the loku-url like this:

    `loki-url: "https://<user_id>:<password>@logs-us-west1.grafana.net/loki/api/v1/push"`
