# Awesome container lists

* [Extend list of comparable projects](https://open.greenhost.net/openappstack/openappstack/-/issues/846)
* https://github.com/veggiemonk/awesome-docker
* https://github.com/docker/awesome-compose
* https://github.com/awesome-selfhosted/awesome-selfhosted

## Kubernetes lists

* [ramitsurana's awesome k8s](https://ramitsurana.github.io/awesome-kubernetes)
  (lacking tools)
* [tomhuang12 awesome k8s](https://github.com/tomhuang12/awesome-k8s-resources)
* [RedHat Spain awesome k8s](https://redhatspain.com/kubernetes)
* [kubetools by collabnix](https://dockerlabs.collabnix.com/kubernetes/kubetools/)
* [awesome-home-kubernetes](https://github.com/k8s-at-home/awesome-home-kubernetes)
