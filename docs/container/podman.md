# Podman

- [Podman tutorial](https://github.com/containers/libpod/blob/master/docs/tutorials/podman_tutorial.md)
- [Gitlab CI with podman](https://tech.immerda.ch/2019/10/gitlab-ci-with-podman/)

## Installation

Install:

```sh
apt install podman
```

### Arch

- [Arch wiki: Podman](https://wiki.archlinux.org/title/Podman)
- [podman-compose package](https://archlinux.org/packages/extra/any/podman-compose/)
  - run docker-compose.yml using podman
- [podman-docker package](https://archlinux.org/packages/extra/x86_64/podman-docker/)
  Emulate Docker CLI using podman

Install:

```sh
pacman -S podman podman-docker
```

Disable docker emulation warning:

```sh
sudo mkdir -p /usr/etc/containers
sudo chgrp varac /usr/etc /usr/etc/containers
sudo touch /usr/etc/containers/nodocker
```

## Configuration

- `man containers.conf`
- Container engines read the
  - `/usr/share/containers/containers.conf`
  - `/etc/containers/containers.conf`
  - `/etc/containers/containers.conf.d/*.conf`
- When running in rootless mode:
  - `$HOME/.config/containers/containers.conf`
    `$HOME/.config/containers/containers.conf.d/.conf`

By default, podman logs to the journal.

## Usage

Start a container:

```sh
podman run --rm -ti docker://alpine sh
```

Import existing, self-built, non-published container images

```sh
podman pull docker-daemon:varac/test:5
```

Inspect:

```sh
podman inspect samba-server
```

Expose/publish ports:

```sh
podman run --rm --publish 9469:9469 billimek/prometheus-speedtest-exporter:sha-243c8eb
```

## Proxy settings

- [Podman as a Replacement for Docker Desktop](https://cpolizzi.github.io/podman/podman-as-a-replacement-for-docker-desktop.html#_corporate_network_considerations)

Mount custom CA into build container:

```sh
unset http_proxy https_proxy HTTP_PROXY HTTPS_PROXY REQUESTS_CA_BUNDLE \
  HTTP_PROXY_HOST HTTP_PROXY_PORT REQUESTS_CA_BUNDLE
podman build \
  -v /etc/ca-certificates/extracted/tls-ca-bundle.pem:/etc/ssl/certs/ca-certificates.crt \
  --env NODE_EXTRA_CA_CERTS=/etc/ssl/certs/ca-certificates.crt .
```

## Networking

Test network/DNS:

```sh
podman run --rm docker.io/alpine/curl -I ix.de
```

### inter-container networking

For inter-container communication all containers must be in a common Pod.

For internal DNS to work:

- Install these these packages: `apt install netavark aardvark-dns`
- Podman needs to be configured with `networkBackend: netavark`
  in `/etc/containers/containers.conf` (reboot afterwards)
  - `podman info -f json | jq '.host.networkBackend'`
- Container must be in a dedicated network with `dns_enabled`
  (see `podman network inspect NETWORK | jq '.[].dns_enabled'`)
  - The default `podman` network has `"dns_enabled": false` !!

#### host.containers.internal

The host can be reached by using the `host.containers.internal` mapping
in `/etc/hosts`, which **should** automatically get added:

```console
$ podman run --rm -it alpine grep host.containers.internal /etc/hosts
10.27.13.162 host.containers.internal host.docker.internal
```

In case it's not automatically added for any reason, manually add the entry
to `/etc/hosts`:

```sh
podman run --add-host=host.containers.internal:host-gateway --rm -it alpine cat /etc/hosts
```

or, if `host-gateway` cannot get determined on the host, pass the host IP:

```sh
podman run --add-host=host.containers.internal:10.10.10.1 --rm -it alpine cat /etc/hosts
```

### Iptables

The [podman debian package](https://packages.debian.org/bookworm/podman)
installs `ìptables` as a dependency of [containernetworking-plugins](https://packages.debian.org/bookworm/containernetworking-plugins)
which somehow get installed by installing podman.
There are multiple issues/PRs for [containernetworking-plugins](https://github.com/containernetworking/plugins/issues)
to not depend on iptables:

Closed:

- [add an option to disable iptables](https://github.com/containernetworking/plugins/issues/762)
- [WIP: firewall: add nftables backend](https://github.com/containernetworking/plugins/pull/462)
  - which got closed and is now implemented in [greenpau/cni-plugins](https://github.com/greenpau/cni-plugins)

### Authentication

- [podman login man page](https://docs.podman.io/en/latest/markdown/podman-login.1.html)
- Auth config at `${XDG_RUNTIME_DIR}/containers/auth.json`, i.e.
  `/run/user/1000/containers/auth.json`

## Ansible

- [containers.podman.podman_container module – Manage podman containers](https://docs.ansible.com/ansible/latest/collections/containers/podman/podman_container_module.html)
- [Ansible and Podman containers](https://redhatnordicssa.github.io/ansible-podman-containers-1)
- [podman-container-systemd](https://github.com/ikke-t/podman-container-systemd)
  creates systemd files and creates containers using podman

## Compose

- [podman-compose](https://github.com/containers/podman-compose)
  - Only implements a subset of the Compose spec
- [docker-compose](https://github.com/docker/compose)
  can also be used together with podman, see i.e.
  [Using Compose Files with Podman](https://docs.oracle.com/en/learn/podman-compose/index.html)
- [Podman Compose or Docker Compose: Which should you use in Podman ?](https://www.redhat.com/sysadmin/podman-compose-docker-compose)
