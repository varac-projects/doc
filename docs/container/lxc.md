# LXC

https://help.ubuntu.com/lts/serverguide/lxc.html

    sudo lxc-create -t download -n debian_jessie -- -d debian -r jessie -a amd64
    sudo lxc-start -n debian_jessie -d
    sudo lxc-attach --name debian_jessie

## Destroy

    sudo lxc-destroy --force --name packer-lxc

# Issues

dhclient can't get a lease, reporting `5 bad udp checksums in 5 packets`:

    ethtool -K br0 tx off

puppetized in `bitrigger/modules/site_lxc/files/lxc-net-start.sh`

see https://bugs.launchpad.net/ubuntu/+source/lxc/+bug/1254338
or https://github.com/fgrehm/vagrant-lxc/issues/153#issuecomment-26441273

# Images

    sudo  ls -al /var/cache/lxc

# Config for different containers

    /var/lib/lxc/
