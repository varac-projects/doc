# Devcontainers

- [Website](https://containers.dev/)

## devcontainers-cli

- Official client
- [Github](https://github.com/devcontainers/cli)

Install:

    pamac install devcontainer-cli

## Other cli tools

- [DevPod](https://devpod.sh/)
- Archived: [devc](https://github.com/nikaro/devc)
