# Docker images and registries

## Building

### Podman/Buildah

#### Build for a different/multiple architectures

- [Arch wiki: Podman/Foreign architectures](https://wiki.archlinux.org/title/Podman#Foreign_architectures)
- [Building Multi-Architecture Containers with Buildah](https://medium.com/oracledevs/building-multi-architecture-containers-with-buildah-44ed100ec3f3)
- [Podman blog: Working with container image manifest lists](https://podman.io/blogs/2021/10/11/multiarch)

Install qemu dependencies:

    sudo pacman -S qemu-user-static qemu-user-static-binfmt

Then build for i.e. `linux/arm64/v8` and `linux/amd64` archtecture:

    podman build --platform linux/arm64/v8 --platform linux/amd64 .

or with buildah:

    buildah build --jobs=2 --platform linux/arm64/v8,linux/amd64 --manifest multiarch:latest .

Validate local manifest:

    buildah manifest inspect multiarch:latest | jq '.manifests[].platform'

Then push:

    buildah manifest push --all multiarch:latest

Use [skopeo](https://github.com/containers/skopeo) to validate the remote manifest:

    export REMOTE_TAG=registry.gitlab.com/.../multiarch:latest
    skopeo inspect --raw docker://$REMOTE_TAG | jq '.manifests[].platform'

### Docker

from Dockerfile

    docker build -t varac/squid_deb_proxy .
    docker build --no-cache=true -t varac/soledad .

where varac/squid_deb_proxy is the name of the image that gets created

### Cross-compiling / builds

## Registry

List tags using podman:

    podman image search --list-tags --limit 999 grafana/loki

List tags using curl/docker.io API:

    curl https://registry.hub.docker.com/v2/repositories/leapcode/jessie/

[List tags for image](https://docs.docker.com/registry/spec/api/#listing-image-tags):

    curl -s https://registry.hub.docker.com/v2/repositories/leapcode/jessie/tags/ \
      | jq '."results"[]["name"]'


    image_name=python
    curl -s https://registry.hub.docker.com/v1/repositories/${image_name}/tags \
      | jq ".[].name" | sed -e "s/\"//g"

### Push images to registry

Use a personal access token to login (for Gitlab: `read_registry`, `write_registry` scope)

    docker login registry.0xacab.org
    docker push registry.0xacab.org/infrared/platform_wg/docker-tmate-server/docker-tmate-server:latest

### Private registries

- [Authenticate with the Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html)

> All of these authentication methods require the minimum scope:
> For read (pull) access, to be read_registry.
> For write (push) access, to bewrite_registry and read_registry.

Also, the `developer` role must be granted for [pull access](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions)

## Analyse image

    podman inspect registry.0xacab.org/varac-projects/varac-home-assistant:2023.10.5  |grep Architecture

### dive

<https://github.com/wagoodman/dive>

    dive debian:buster

## Show dockerfile / build history from image

<https://github.com/lukapeschke/dockerfile-from-image>

Useful when there's no published Dockerfile.

    docker run  --rm -v '/var/run/docker.sock:/var/run/docker.sock' lukapeschke/dfa d402b2607a26

## Image update monitoring tools

### Diun

[diun](https://crazymax.dev/diun/)

> Unlike WatchTower, Diun does not update containers but only handles notification via webhook or email.

- Small footprint
- [No prometheus metrics](https://github.com/crazy-max/diun/issues/201)

### Watchtower

- [Prometheus metrics on the way](https://github.com/containrrr/watchtower/issues/295)

### Deprecated

- <https://github.com/pyouroboros/ouroboros>
- Prom support
