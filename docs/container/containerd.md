# CRI / containerd

## containerd

* [containerd website](https://containerd.io/)
* [containerd github](https://github.com/containerd/containerd)
* [docs](https://github.com/containerd/containerd/tree/master/docs)

## cri-tools

* [cri-tools Github](https://github.com/kubernetes-sigs/cri-tools)

### crictl

* [crictl docs](https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md)
* [Debugging Kubernetes nodes with crictl](https://kubernetes.io/docs/tasks/debug/debug-cluster/crictl/)

### critest

* [Container Runtime Interface (CRI) Validation Testing](https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/validation.md)
