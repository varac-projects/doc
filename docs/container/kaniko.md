# Kaniko

- <https://github.com/GoogleContainerTools/kaniko>
- [Using kaniko](https://github.com/GoogleContainerTools/kaniko#using-kaniko)

## Issues

- [kaniko should only be run inside of a container](https://github.com/GoogleContainerTools/kaniko/issues/1542)

## Cross-compiling

_Beware_: Need to run on target architecture hosts

You can pass in docker args:

    kaniko --build-arg opts="CGO_ENABLED=0 GOARCH=amd64"

## Multi-arch images

Currently not supported, see [how to build multi-arch image using kaniko](https://github.com/GoogleContainerTools/kaniko/issues/786)
See also [Creating Multi-arch Container Manifests Using Kaniko and Manifest-tool](https://github.com/coder/kaniko#creating-multi-arch-container-manifests-using-kaniko-and-manifest-tool):

> While Kaniko itself currently does not support creating multi-arch manifests
> (contributions welcome), one can use tools such as manifest-tool to stitch
> multiple separate builds together into a single container manifest.
