# Container security

## Trivy

- [GitHub](https://github.com/aquasecurity/trivy)
- [Docs](https://trivy.dev/latest/docs/)

### Config

- [Configuration](https://trivy.dev/latest/docs/configuration/)
- Cli options can be exported as env vars (i.e. `--debug` -> `TRIVY_DEBUG=true trivy ...`)

### Usage

Show the different trivy component versions (core, Vulnerability DB, Check
Bundle) together with download times:

```sh
trivy --version
```

Scan local filesystem (i.e. to check a `Containerfile`, next to others):

```sh
trivy config .
```

Scan container image:

```sh
trivy image php:8.4.4-apache-bullseye
trivy image php:8.4.4-apache-bullseye | head -5  # Hide details
TRIVY_IGNORE_UNFIXED=true trivy image php:8.4.4-apache-bullseye # Only show fixed CVEs
```

### Ignore checks

[Docs: Filtering](https://aquasecurity.github.io/trivy/v0.56/docs/configuration/filtering/)

#### Ignore checks with a .trivyignore

```sh
$ cat .trivyignore
# Root file system is not read-only
# https://avd.aquasec.com/misconfig/kubernetes/general/avd-ksv-0014/
AVD-KSV-0014
```

#### Inline ignores

- i.e.: `#trivy:ignore:AVD-GCP-0051`
- Only work in certain files, i.e. OpenTofu files

### pre-commit hook

- [GitHub](https://github.com/mxab/pre-commit-trivy)

Usage:

```yaml
- repo: https://github.com/mxab/pre-commit-trivy.git
  rev: v0.12.0
  hooks:
    - id: trivyconfig-docker
      args:
        - Containerfile
```
