# Container trouble-shooting

## Network

### netshoot

> A network trouble-shooting swiss-army container

- [GitHub](https://github.com/nicolaka/netshoot)
- [No arm v7 builds (RPi v3)](https://github.com/nicolaka/netshoot/issues/119)

### Other network debug images

- `alpine` has busybox ping
- [busybox](https://hub.docker.com/_/busybox)
  - `ping: permission denied (are you root?)`
- [arunvelsriram/utils](https://hub.docker.com/r/arunvelsriram/utils)
  - No arm v7 builds (RPi v3)

### Outdated network debug images

- [gopher-net/dockerized-net-tools](https://github.com/gopher-net/dockerized-net-tools)
  - last commit 2018
- [Praqma/Network-MultiTool](https://github.com/Praqma/Network-MultiTool)
  - last commit 2022
