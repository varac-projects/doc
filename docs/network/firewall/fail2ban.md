# fail2ban

- [Website](https://www.fail2ban.org/wiki/index.php/Main_Page)


## Show status

    fail2ban-client status
    fail2ban-client status sshd

Unban IP:

    fail2ban-client set sshd unbanip 84.158.175.229
