# Iptables examples

[Linux Iptables allow or block ICMP ping request](https://www.cyberciti.biz/tips/linux-iptables-9-allow-icmp-ping.html)

Drop incoming pings:

    iptables -A INPUT -p icmp --icmp-type echo-request -j DROP

Remove rule:

[How to List and Delete iptables Firewall Rules](https://www.rosehosting.com/blog/how-to-list-and-delete-iptables-firewall-rules/)

    iptables -D INPUT -p icmp --icmp-type echo-request -j ACCEPT

## Log remaining packets

[How to Log Linux IPTables Firewall Dropped Packets to a Log File](https://www.thegeekstuff.com/2012/08/iptables-log-packets/)

i.e. Log All Dropped Input Packets

    iptables -N LOGGING
    iptables -A INPUT -j LOGGING
    iptables -A LOGGING -m limit --limit 2/min \
      -j LOG --log-prefix "IPTables-Dropped: " --log-level 4
    iptables -A LOGGING -j DROP

## Show all IPv4 NAT rules

    iptables -L -n -v -t nat
