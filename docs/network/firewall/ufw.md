# UFW

- [Ubuntu wiki: UncomplicatedFirewall](https://wiki.ubuntu.com/UncomplicatedFirewall)
- [Ubuntu docs: UFW](https://help.ubuntu.com/community/UFW#Services)
- [UbuntuFirewallSpec](https://wiki.ubuntu.com/UbuntuFirewallSpec#Rules)

## Config

- `/etc/ufw/`

## Usage

    ufw status
    ufw status verbose
    ufw show raw

List installed application profiles:

    ufw app list
    ufw app info WWW

Delete rule:

    ufw status numbered
    ufw delete 1

## Ansible

- [community.general.ufw module](https://docs.ansible.com/ansible/latest/collections/community/general/ufw_module.html)
  - Part of [community.general collection](https://galaxy.ansible.com/community/general)

> Note that as ufw manages its own state, simply removing
> a `rule=allow` task can leave those ports exposed.
> Therefore manually remove it:

    ufw delete reject 113/tcp
