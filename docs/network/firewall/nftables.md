# nftables

- [nftables wiki](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page)
- [Adoption](https://wiki.nftables.org/wiki-nftables/index.php/Adoption)
- [Debian wiki](https://wiki.debian.org/nftables)

> You should consider using a wrapper instead of writing your own firewalling scripts.
> It is recommended to run `firewalld`, which integrates pretty well into the system.

## Packages that still depend on iptables

- podman
- tailscale

## Usage

- [Quick reference-nftables in 10 minutes](https://wiki.nftables.org/wiki-nftables/index.php/Quick_reference-nftables_in_10_minutes)
- [Configuring tables](https://wiki.nftables.org/wiki-nftables/index.php/Configuring_tables)

List all tables:

    nft list tables

List rules in chain:

    nft list table ip filter

Flush all rules in table:

    nft flush table ip filter

## Migration

- [Moving from iptables to nftables](https://wiki.nftables.org/wiki-nftables/index.php/Moving_from_iptables_to_nftables)

## Kubernetes

Still no support for nftables, blocking OAS to use nftables:

- [kubernetes](https://github.com/kubernetes/kubernetes/issues/62720)
- [docker](https://github.com/moby/moby/issues/26824)
- from the [Calico 3.8.2 release notes](https://docs.projectcalico.org/v3.8/release-notes/):

  > calico can run on systems which use iptables in nft compatibility mode …

  OAS / rancher / k8s 14.3 use calico 3.4

## Ansible

Collections:

- [ansibleguy/collection_nftables](https://beta-galaxy.ansible.com/ui/repo/published/ansibleguy/nftables/)

  - [Github](https://github.com/ansibleguy/collection_nftables)
  - [Docs](https://wiki.junicast.de/de/junicast/docs/nftwall)
  - Recent commits, but no releases/tags

- [imp1sh/ansible_nftwallcollection](https://beta-galaxy.ansible.com/ui/repo/published/imp1sh/ansible_nftwallcollection/)
  - [Github](https://github.com/imp1sh/ansible_nftwallcollection)
  - Last commit 2021-10, last release 2021-03

(Deprecated) roles:

- [ipr-cnrs/nftables](https://github.com/imp1sh/ansible_nftwallcollection)
