# Firewalld

- [Website](https://firewalld.org/)

## Install

    apt install firewalld
    systemctl status firewalld.service

## Usage

    firewall-cmd --reload
    firewall-cmd --permanent --direct --get-all-rules
    firewall-cmd --direct --get-all-rules

### Limit outgoing connections

https://serverfault.com/questions/618164/block-outgoing-connections-on-rhel7-centos7-with-firewalld

Allow established connections:

    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 0 -m state --state ESTABLISHED,RELATED -j ACCEPT

Allow HTTP:

    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 1 -p tcp -m tcp --dport 80 -j ACCEPT

Allow HTTPS:

    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 1 -p tcp -m tcp --dport 443 -j ACCEPT

Allow for DNS queries:

    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 1 -p tcp -m tcp --dport 53 -j ACCEPT
    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 1 -p udp --dport 53 -j ACCEPT

Deny everything else:

    firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 2 -j DROP

## Ansible

- [ansible.posix.firewalld module](https://docs.ansible.com/ansible/latest/collections/ansible/posix/firewalld_module.html)
  - Part of [ansible/posix collection](https://galaxy.ansible.com/ansible/posix)

## Libvirt

- [Firewall and network filtering in libvirt](https://libvirt.org/firewall.html)
