# Network troubleshooting

## Tools for debug a slow internet connection

- `ping 1.1.1.1`: Packet loss ?
- [speedtest](./performance/performance.md)

## TCP: out of memory

Journal entries like `kernel: TCP: out of memory -- consider tuning tcp_mem`

The sysctl fix mentioned in [Medium article](https://medium.com/tier1app-com/tcp-outofmemory-consider-tuning-tcp-mem-d28bdf1c8dda)
didn't solve the issue. It was finally solved by upgrading the kernel
to v6.6 from Debian backports.
