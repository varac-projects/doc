# Autossh

- [Website](https://www.harding.motd.ca/autossh/)
- [Github](https://github.com/Autossh/autossh)
- [Arch wiki: Autossh](https://wiki.archlinux.org/title/OpenSSH#Autossh_-_automatically_restarts_SSH_sessions_and_tunnels)
- Last release 2023-

Install:

    sudo pacman -S autossh

Configure:

[Setting up autossh autostart with systemd](https://pesin.space/posts/2020-10-16-autossh-systemd/)
