# Nmap

## Usage

### Ping-scan live hosts (fast)

```sh
nmap -sP 192.168.178.1/24
```

### Scan hosts with grepable Output

```sh
nmap -sP -oG - 192.168.1.0/24
```

## Detect OS

```sh
sudo nmap -O 172.18.0.2
```

## Test individual ports

```sh
nmap -p 7946 860-high-node-cpptables-procs.ci.openappstack.net
```

### Test UDP port

<https://nmap.org/book/scan-methods-udp-scan.html>

```sh
sudo nmap -sUV -p 5060 sipgate.de
```

## Other tools

### Ndiff

- [Not included in arch nmap package](https://bugs.archlinux.org/task/69150)
- [Github](https://github.com/nmap/nmap/tree/master/ndiff)

Usage:

```sh
nmap -oX /tmp/before.xml 1.2.3.4/24
```

Then plug in device in question and scan after it recieved an IP via DHCP:

```sh
nmap -oX /tmp/after.xml 1.2.3.4/24
ndiff /tmp/before.xml /tmp/after.xml
```

### Nping

- [Nping manual](https://nmap.org/book/nping-man.html)

### Poor-mans nmap using ping

i.e. on hosts where no nmap is available:

```sh
for x in {1..254}; do (ping  -c 1 192.168.8.$x | grep "bytes from" | awk '{print $4}' &) done; sleep 1
```
