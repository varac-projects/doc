# Breitbandmessung.de

## Tools for automated measurements

### shneezin/breitbandmessung

- [shneezin/breitbandmessung-node](https://github.com/shneezin/breitbandmessung-node)
- [shneezin/breitbandmessung](https://github.com/shneezin/breitbandmessung)
- [What's the difference?](https://github.com/shneezin/breitbandmessung/issues/7)

> breitbandmessung-node is based on shiaky's project, to which I have made
> changes. The container is created with Debian or Alpine.
> With shiaky's repo, the container is terminated after a measurement.
> I have changed this in the fork.
> This project has several advantages. Selenium is used as browser, which i
> allows higher data rates. With this project, notifications are also possible
> via various channels. The measurements can be saved in a database with
> this project. However, it is only provided as a Debian container and not as
> Alpine Container.

- Both have `arm/v7` images !

### Other tools

- [shiaky/breitbandmessung](https://github.com/shiaky/breitbandmessung):
  Stale
- [breitbandmessung-docker](https://gitlab.fabianbees.de/fabianbees/breitbandmessung-docker):
  No public docker image
