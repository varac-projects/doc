# Speed / performance testing

for Prometheus speedtest exporters see `monitoring/prometheus/exporters/speedtest.md`

## speedtest-go

- [GitHub](https://github.com/showwin/speedtest-go)
- Active development

## Librespeed

- [go cli client](https://github.com/librespeed/speedtest-cli)
- Last release 2022-07
- see `../../monitoring/prometheus/exporters/speedtest.md` for speedtest exporter
- [k8s-at-home/librespeed webservice chart](https://artifacthub.io/packages/helm/k8s-at-home/librespeed)
  - [linuxserver/librespeed image](https://hub.docker.com/r/linuxserver/librespeed)
- [Server list](https://librespeed.org/backend-servers/servers.php)

### openwrt

On Laptop:

```bash
sudo pacman -S upx
cd /tmp
wget https://github.com/librespeed/speedtest-cli/releases/download/v1.0.10/librespeed-cli_1.0.10_linux_mips_softfloat.tar.gz
tar -xzf librespeed-cli_1.0.10_linux_mips_softfloat.tar.gz
# Uncompress binary, see https://github.com/librespeed/speedtest-cli/issues/44
upx -d librespeed-cli
scp librespeed-cli root@_gateway:/tmp
```

On router:

```bash
/tmp/librespeed-cli --simple
Ping:   40 ms   Jitter: 3 ms
Download rate:  5.37 Mbps
Upload rate:    5.21 Mbps
```

## speedtest-cli

> Command line interface for testing internet bandwidth using speedtest.net

see `../../monitoring/prometheus/exporters/speedtest.md` for speedtest exporter

- [GitHub](https://github.com/sivel/speedtest-cli)
- [Internetgeschwindigkeit mit dem Raspberry Pi messen](https://intux.de/2023/03/08/internetgeschwindigkeit-mit-dem-raspberry-pi-messen/)
- Last commit 2021
- won't work on mips/openwrt because of musl-libc ?

Share results:

```sh
speedtest-cli --share
```

## Using rsync

```bash
time dd if=/dev/random of=/tmp/rand.img bs=1M count=100
rsync --stats --partial --append  --progress -ah /tmp/rand.img server:
```
