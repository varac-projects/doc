# ipinfo.io

- [Web Interface](https://ipinfo.io/)

## API

- [API docs](https://ipinfo.io/developers)

Show external IP:

```sh
curl ipinfo.io | jq -r .ip
```

Query other IP:

```sh
curl ipinfo.io/8.8.8.8
```
