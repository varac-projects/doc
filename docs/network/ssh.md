# SSH

## Generate secure ed25519 key

<https://security.stackexchange.com/a/144044>

    ssh-keygen -a 100 -t ed25519 -f ./id_ed25519

## Disable pubkey auth

    ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no localhost

## Force certain identity file

    ssh -o IdentitiesOnly=yes -i id_ed25519 bitrigger -v

## Deploy pubkey

    ssh-copy-id -i keydir/work.pub server

## SSH into old boxes

<https://unix.stackexchange.com/a/340853>

When the remote host shows this:

    ssh 10.27.13.130
    Unable to negotiate with 10.27.13.130 port 22: no matching key exchange method
    found. Their offer:
    diffie-hellman-group14-sha1,diffie-hellman-group1-sha1,kexguess2@matt.ucc.asn.au

Use:

    ssh -oKexAlgorithms=+diffie-hellman-group1-sha1

## SSH-Agent

Start ssh-agent if none is running:

    eval $(ssh-agent)

Then add keys:

    ssh-add /root/.ssh/work_rsa

## GPG as ssh-agent

Key-DB: `~/.gnupg/sshcontrol`

Show keys added to gpg ssh-agent:

    ssh-add -E md5 -l


    echo 'KEYINFO --ssh-list --ssh-fpr' | gpg-connect-agent
    cat ~/.gnupg/sshcontrol

### Remove ssh-key from agent

<https://lists.gnupg.org/pipermail/gnupg-users/2016-August/056499.html>

    ssh-add -E md5 -l
    gpg-connect-agent
      KEYINFO --ssh-list --ssh-fpr
      DELETE_KEY <key_id>

## SSH-Tunnel

Create tunnel:

    ssh -L 8080:192.168.9.5:80 server

## Socks proxy tunnel

    ssh -D 1080 kermit

Then configure browser proxy settings as socks proxy: `localhost:1080`

## Show SSH Host key fingerprints

## in file

    ssh-keygen -lf /etc/ssh/ssh_known_hosts
    ssh-keygen -lf ~/.ssh/known_hosts -E md5 -F '[server1.example.org]:22'

    ssh-keygen -E sha256 -lf <(ssh-keyscan  -t ecdsa 10.1.1.12 2>/dev/null)
      256 SHA256:0N6VUnfoJyfkgaRCAkSOWOp0WB6/au5LbrQ0x9FUm44 10.1.1.12 (ECDSA)

## DSH

    dsh -cM -g bm.couch "grep '\[error\]' /opt/bigcouch/var/log/bigcouch.log|wc -l"

## endlessh - SSH tarpit

<https://nullprogram.com/blog/2019/03/22/>
<https://github.com/skeeto/endlessh>

    docker container run --rm --name endlessh -p 2222:2222 harshavardhanj/endlessh
    ssh -v localhost -p 2222
