# IPv6

* [book6: A Collaborative IPv6 Book](https://github.com/becarpenter/book6/blob/main/Contents.md)
* [IPv6 resources](https://gitlab.com/jenslink/ipv6-resources/-/blob/main/resources.md?ref_type=heads)
* [IPv6 Privacy_Extensions](https://wiki.ubuntuusers.de/IPv6/Privacy_Extensions)
* [Debian Handbuch IPv6](https://debian-handbook.info/browse/de-DE/stable/sect.ipv6.html)
* [Debian IPv6 startklar machen](https://www.maffert.net/debian-ipv6-startklar-machen-einrichten/)

## Test sites

* <http://test-ipv6.com/>
* <https://ipv6-test.com/>

## Link-local addr

<https://en.wikipedia.org/wiki/Link-local_address>

in IPv6, link-local addresses are assigned from the block `fe80::/10`,
and shown in `ip addr show` with a `scope link` flag.

## IPv6 addr discovery

    ip n
    ping6 -c4 ff02::1%$(ip r | awk '/def/{ print $5 }')
    ping6 ff02::1
    ping ff02::1%enx0050b6ebc45b

## Disable ipv6 completly

    echo 'net.ipv6.conf.all.disable_ipv6 = 1' > /etc/sysctl.d/70-disable-ipv6.conf
    sysctl -p -f /etc/sysctl.d/70-disable-ipv6.conf
