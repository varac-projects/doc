# Remote management tools

- [froscon2021 talk about rport and rustdesk](https://media.ccc.de/v/froscon2021-2667-komfortable_und_sichere_fernwartung_fur_alle_betriebssysteme_oss)
- [rport](https://docs.rport.io/)
- [rustdesk](https://rustdesk.com): Remote Desktop App
