# Traceroute tools

## Traditional traceoute

There are two traceroute projects:

- [traceroute.sourceforge.net](http://traceroute.sourceforge.net)
  - [Arch package: traceroute](https://archlinux.org/packages/extra/x86_64/traceroute/)
- [inet-traceroute](https://www.gnu.org/software/inetutils/)
  - No Arch/AUR package

## Other tools

## nexttrace

- [nexttrace](https://github.com/nxtrace/NTrace-core)
- visual route tracking CLI tool
- recent releases
- GUI: [opentrace](https://github.com/Archeb/opentrace)
  - cross-platform GUI wrapper for NextTrace
  - AUR package: `opentrace` or `opentrace-bin` (outdated)

```sh
nexttrace -g en -4 nmmn.de
```

### mtr

- [mtr](https://github.com/traviscross/mtr)
- [mtr085](https://github.com/yvs2014/mtr085)
  - Fork of mtr v0.85, with more IP details (Country, AS)
- [Traceroute mapper](https://stefansundin.github.io/traceroute-mapper/)
  - Map your traceroutes easily

### Deprecated

- [pingnoo](https://github.com/nedrysoft/pingnoo)
  - Last release 2021
- [asroute](https://github.com/stevenpack/asroute)
  - Last release 2020
- [dublin-traceroute](https://github.com/insomniacslk/dublin-traceroute)
  - NAT-aware multipath tracerouting tool
  - AUR package: `dublin-traceroute`
  - Last release 2017

## Traceroute destinations nearby

- `nmmn.de`
