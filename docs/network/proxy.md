# Network proxies

## Proxy autodiscovery

- [Web Proxy Auto-Discovery Protocol](https://en.wikipedia.org/wiki/Web_Proxy_Auto-Discovery_Protocol)

Show content of `wpad.dat`

```sh
❯ curl http://192.168.3.100/wpad.dat
  function FindProxyForURL(url, host)
  {
          if (shExpMatch (url, "*://127.0.0.1*"))
                  return "DIRECT";
          else if (shExpMatch (url, "*://localhost*"))
                  return "DIRECT";
          else if (shExpMatch (url, "*://192.168.*"))
                  return "DIRECT";
else
                  return "PROXY 192.168.3.100:8080";
  }
```
