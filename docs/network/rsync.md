# rsync

## Issues

- [No option to stop rsync on first error](https://github.com/WayneD/rsync/issues/71)

## Example options

- Use different ssh port: `-e 'ssh -p 1234'`
- Limit bandwidth: `--bwlimit=150`
- Show stats at the end: `--stats`
- Show progress: `--progress`
- Resume partially synced files from last run: `--partial --append`
- Archive options: `--numeric-ids -a`

## Clone a partition

```sh
rsync --stats --progress --numeric-ids -aAhHSP  /mnt/alt/ /mnt/neu/
```

### Control with -c (checksum)

```sh
rsync --stats --progress --numeric-ids -aAhHSPi -c  /mnt/alt/ /mnt/neu/
```
