# File transfer

## Magic wormhole

Install:

    sudo pacman -S magic-wormhole

[Docs: Example](https://magic-wormhole.readthedocs.io/en/latest/welcome.html#example)

    laptop1: wormhole send README.md
    laptop2: wormhole recieve
