# iwctl

Use [iwctl to connect to wifi](https://wiki.archlinux.org/title/iwd#Connect_to_a_network)
[Enable DHCP](https://wiki.archlinux.org/title/iwd#Enable_built-in_network_configuration)

```sh
echo "[General]\nEnableNetworkConfiguration=true" > /etc/iwd/main.conf
systemctl start iwd.service
```

Configure connection:

```sh
iwctl device device show
iwctl station wlan0 scan
iwctl station wlan0 get-networks
iwctl --passphrase $(gopass show --password moewe-privat/wlan/moewe/casita) \
  station wlan0 connect casita
```
