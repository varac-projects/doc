# nmcli

- [How to Connect to Wi-Fi Through the Linux Terminal With nmcli](https://www.makeuseof.com/connect-to-wifi-with-nmcli/)

## Connect to wifi

```sh
nmcli dev status
nmcli radio wifi
nmcli dev wifi list
```

Connect to unencrypted wifi:

```sh
nmcli dev wifi con hamburg.freifunk.net
```

Connect to WPA2 wifi:

```sh
nmcli dev wifi con SSID password PASSWORD name "Connection1"
```
