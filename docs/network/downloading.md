# aria2

Install:

    sudo apt install aria2

Download with max speed (https://askubuntu.com/a/507890):

    aria2c --file-allocation=none -c -x 10 -s 10 -d ~/Downloads/ URL
