# Network UPS tools (nut)

- [Network UPS Tools User Manual](https://networkupstools.org/docs/user-manual.chunked/index.html)
- [Debian wiki: nut](https://wiki.debian.org/nut)

## Install

Install the `nut` metapackage:

```sh
apt install nut
```

### Configure

Run `nut-scanner` to auto-fetch a valid config snippet for `/etc/nut/ups.conf`:

```sh
nut-scanner
```

Configure these files according to the Debian wiki tutorial:

- /etc/nut/nut.conf
- /etc/nut/ups.conf
- /etc/nut/upsd.users
- /etc/nut/upsmon.conf

Restart services:

```sh
systemctl restart nut-server.service
systemctl restart nut-monitor.service
```

Check the UPS:

```sh
upscmd -l eaton
upsc eaton
upsc eaton | grep -E '(ups.realpower|ups.load|ups.status|battery.charge|battery.runtime)'
```

## Ansible

- Collection: [vladgh.system/nut](https://galaxy.ansible.com/ui/repo/published/vladgh/system/content/role/nut/)
  - [GitHub](https://github.com/vladgh/ansible-collection-vladgh-system/tree/main/roles/nut)
  - Last role commit 2024-06
- [belgotux.nut_client](https://github.com/belgotux/ansible-role-nut-client)
  - Last commit 2024-01
- [colshine1.nutmon](https://github.com/colshine1/nutmon)
  - Last commit 2020
- [calvinbui.ansible_nut](https://github.com/calvinbui/ansible-nut)
  - Archived

## Prometheus

- [DRuggeri/nut_exporter](https://github.com/DRuggeri/nut_exporter)
  - Issue: [network_ups_tools_ups_status metric is missing when using NUT_EXPORTER_VARIABLES](https://github.com/DRuggeri/nut_exporter/issues/52)
- [HON95/prometheus-nut-exporter](https://github.com/HON95/prometheus-nut-exporter): Last commit 2022
