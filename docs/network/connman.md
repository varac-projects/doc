# ConnMan

- [Arch wiki: ConnMan](https://wiki.archlinux.org/title/ConnMan)
- Connection profiles are at `/var/lib/connman/`
- [Using iwd instead of wpa_supplicant](https://wiki.archlinux.org/title/ConnMan#Using_iwd_instead_of_wpa_supplicant)

## Configure Wifi

```sh
apt install connman
connmanctl technologies
connmanctl enable wifi
connmanctl scan wifi
connmanctl agent on
connmanctl connect wifi_dc85de828967_4d6568657272696e_managed_psk
```
