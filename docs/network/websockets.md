# Websockets

## wscat

* [wscat](https://github.com/websockets/wscat)

Usage:

    /home/varac/node_modules/.bin/wscat -c wss://websocket-echo.com

See <https://0xacab.org/riseup/0xacab/-/issues/120#note_367801>
for debugging the kubernetes agent server websockets setup on 0xacab.org
