# NFS

## Client

List available exports:

    showmount -e varac-nas

Mount an export:

    sudo mount -t nfs varac-nas:/Public /tmp/nfstest

NFS4:

    sudo mount.nfs4 varac-pinenas.local.moewe-altonah.de:/ /mnt
