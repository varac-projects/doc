# Samba / CIFS

## client

All netbios shares:

    nmblookup -S '*'

Only ip addresses:

    nmblookup '*'

Only samba shares:

    nmblookup __SAMBA__
    nmblookup -S __SAMBA__

### gvfs

<https://wiki.ubuntuusers.de/gvfs-mount>

. über nautilus mounten, dann:

    cd /run/user/1000/gvfs/smb-share:server=192.168.178.2,share=multimedia/

### benchmark

    rsync --progress --stats --human-readable --recursive \
      --remove-source-files ~/Videos/Traffic $SMB_TARGET

### smbclient

<http://wiki.ubuntuusers.de/Samba_Client_smbclient>

    apt install smbclient samba-common-bin

- Browse Network: `findsmb`
- `smbtree`

#### Browse shares

    smbclient -L //varac-pinenas/

#### Use specific share as user

    smbclient -U varac //rocinante/vpn-sync
    smbclient -U share%gibher //rocinante/vpn-sync

### Net Usershare

<https://wiki.ubuntuusers.de/Samba%20Server/net%20usershare/>

    net usershare info

### cifs

    sudo mount -t cifs -o <Optionen> //<Server>/Freigabe <Mountpunkt>
    sudo mount -t cifs -o credentials=~/.smbcredentials //192.168.1.100/Tausch /media/austausch

## Samba Server /LDAP

<http://redmine.bitrigger.de/redmine/projects/verikom-eidelstedt/wiki/Admin/edit>

### SambaUser Backends

### tdbsam

in smb.conf: `passdb backend = tdbsam` (default)

List users

    pdbedit -L

Add user

    adduser --shell /bin/false share
    pdbedit -a -u share

Create password entry for i.e. `servercontainers/samba`:

## Samba docker container

### servercontainers/samba

- [Docker hub](https://hub.docker.com/r/servercontainers/samba)
  - Last tag: 2019-06 :/
- [Github](https://hub.docker.com/r/servercontainers/samba)
  - ✅ arm64
  - smb.conf paratmeter support

### other smaba docker options

- <https://hub.docker.com/r/elswork/samba>
  - Proper tags
- [DeftWork/samba](https://github.com/DeftWork/samba)
  - ✅ arm64

#### dperson/samba

- Use by <https://artifacthub.io/packages/helm/k8s-at-home/samba>
- [Docker hub](https://hub.docker.com/r/dperson/samba)
  - No versioned tags
- [Github](https://github.com/dperson/samba) - last commit Jul 4, 2020
  - [Is this project dead?](https://github.com/dperson/samba/issues/375)

#### wener/samba

- Use by <https://artifacthub.io/packages/helm/wenerme/samba>
- [Image](https://hub.docker.com/r/wener/samba/tags)
  - No README.md, no linked github repo
