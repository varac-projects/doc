# Network-Manager

## nmcli

```sh
nmcli connection up VPN
nmcli connection down VPN
```

List available Wi-fi networks:

```sh
nmcli dev wifi
```

## Python lib

- [python-networkmanager](https://github.com/seveas/python-networkmanager)

## Hooks / NetworkManager-dispatcher

- [Arch wiki: Network services with NetworkManager dispatcher](https://wiki.archlinux.org/title/NetworkManager#Network_services_with_NetworkManager_dispatcher)

Ensure the service is running:

```sh
systemctl status NetworkManager-dispatcher.service
```

### Captive portals

- [Arch wiki: NetworkManager/Captive portals](https://wiki.archlinux.org/title/NetworkManager#Captive_portals)
- [Automated Login For Captive Portals in Linux](https://www.geeksforgeeks.org/automated-login-for-captive-portals-in-linux/)
