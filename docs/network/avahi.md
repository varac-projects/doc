# Avahi

[Ubuntu wiki](https://wiki.ubuntuusers.de/Avahi)

## Clients

### Show all devices/services discovered in local network

    avahi-browse -ar

### Avahi resolve

    $ avahi-resolve -4 --name EPSONE9FFDE.local
    EPSONE9FFDE.local 10.27.13.177

    $ avahi-resolve  --addr 10.27.13.177
    10.27.13.177  EPSONE9FFDE.casita.local.example.org

## Avahi server

    sudo apt-get install avahi-daemon

### Announce services

[Ubuntuusers wiki: Publishing](https://wiki.ubuntuusers.de/Avahi/#Publishing)

    cp /usr/share/doc/avahi-daemon/examples/ssh.service /etc/avahi/services/

### Avahi bonjour/mdns repeater between subnets

[bonjour/mdns repeater?](http://apple.stackexchange.com/a/132305)
