# Which switch port I'm connected to ?

```sh
tcpdump -nn -v -s 1500 -c 1 ‘ether[20:2] == 0x2000‘ -i <NIC-INTERFACE>
```
