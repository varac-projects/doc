# DNS

## DNS providers

DNS Providers with API integration:

- [letsencrypt go lib](https://go-acme.github.io/lego/dns/)
  - [LeGo DNS Providers](https://go-acme.github.io/lego/dns/)
- [Cert-manager native DNS provider integrations](https://cert-manager.io/docs/configuration/acme/dns01/#supported-dns01-providers)
  - [Cert-manager webhook supported DNS providers](https://cert-manager.io/docs/configuration/acme/dns01/#webhook)
    - [cert-manager-webhook-gandi](https://github.com/bwolf/cert-manager-webhook-gandi)
      With good instructions
- [Traefik Acme providers](https://doc.traefik.io/traefik/https/acme/#the-different-acme-challenges)
- [terraform DNS acme challenge](https://www.terraform.io/docs/providers/acme/dns_providers/index.html)

Options to consider:

- Gandi
- https://www.inwx.de
  - [Terraform Provider](https://registry.terraform.io/providers/inwx/inwx/1.3.0)
  - No API keys, API auth with user/password
  - OTP needed on each API call

## Hetzner

- [cert-manager-webhook-hetzner](https://github.com/vadimkim/cert-manager-webhook-hetzner)
  - Actively developed
  - Works !

## njal.la

- [cert-manager-webhook-njalla](https://github.com/balzanelli/cert-manager-webhook-njalla)
  - Couldn't make it work (`unable to check TXT record: code: 403, message: Permission denied.`)
  - Stale ?
- [Terraform njal.la providers](https://github.com/Sighery/terraform-provider-njalla)

Other tf provider options:

- <https://github.com/gidsi/terraform-provider-njalla>
  - [No provider documentation](https://registry.terraform.io/providers/gidsi/njalla/latest)
  - Only an [example in the README.md](https://registry.terraform.io/providers/gidsi/njalla/latest)

### njal.la dyndns

- [dyndns docs](https://njal.la/docs/ddns/)

  ❯ export TOKEN=$(gopass show --password token/njal.la/dyndns/varac.net)

Manual update from inside of webserver network:

    ❯ curl "https://njal.la/update/?h=varac.net&auto&k=$TOKEN"
    {"status": 200, "message": "record updated", "value": {"A": "93.221.19.99"}}

Update from outside:

Update:

    ❯ export IP=93.221.16.69
    ❯ curl "https://njal.la/update/?h=varac.net&auto&k=${TOKEN}&a=$IP"

Verify:

    host varac.net

## systemd-resolved

see [[systemd/resolved.md]] (also how to enable DNSSEC resolver)

## Privacy preserving DNS servers

- [Quad 9](https://www.quad9.net)
- [Digitale Gesellschaft](https://www.digitale-gesellschaft.ch/dns)
  DoT + DoH
- [dns.watch](https://dns.watch/)
- [digitalcourage](https://digitalcourage.de/support/zensurfreier-dns-server)
  > Update am 18. Februar 2020: Zur Zeit können wir bei unserem DoT-Dienst bei vielen Abfragen hintereinander Ausfälle/Hänger beobachten. Die Auflösung dauert dann länger oder bricht ab. Wir werden demnächst auf eine neue Software umstellen (und diesen Hinweis dann entfernen).
- [Cloudflare](https://1.1.1.1/help)

## DNS proxies with ad-blocking

- <https://github.com/0xERR0R/blocky>

  - <https://artifacthub.io/packages/helm/k8s-at-home/blocky>

- <https://pi-hole.net/>
  - <https://hub.helm.sh/charts?q=pi-hole>
- <https://adguard.com/de/adguard-home/overview.html>

## DNS encryption

- [Understanding DoT and DoH (DNS over TLS vs. DNS over HTTPS)](https://www.cloudns.net/blog/understanding-dot-and-doh-dns-over-tls-vs-dns-over-https/)

### DNS over HTTPS (DoH)

- [Add support for DNS-over-HTTPS to systemd-resolved](https://github.com/systemd/systemd/issues/8639)

### DNS over TLS (DoT)

- [Arch wiki: systemd-resolved with DoT](https://wiki.archlinux.org/title/systemd-resolved#DNS_over_TLS)

### DNS over Quic (DoQ)

- [DNS-over-QUIC wird zum offiziellen Standard](https://adguard-dns.io/de/blog/dns-over-quic-official-standard.html)

### DNSCrypt

- [Website](https://dnscrypt.info/)

### DNSSEC

- [Arch wiki: DNSSEC support in systemd-resolve](https://wiki.archlinux.org/title/systemd-resolved#DNSSEC)
- [DNSSEC Resolver Test](http://dnssec.vs.uni-due.de/)
- <https://dnsviz.net/d/systemli.org/dnssec/>

## Test DNS

    /usr/lib/nagios/plugins/check_dns -H varac-test.openappstack.net -a 213.108.108.134 -s 1.1.1.1

### dnsdiag tools

<https://dnsdiag.org/>

    sudo apt install dnsdiag

    dnsping -c 5 -s 10.27.13.1 varac.net

## SRV records

<https://www.pair.com/support/kb/what-is-an-srv-record/>
