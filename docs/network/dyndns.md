# DynDNS

## DynDNS clients

### ddns-updater

- [GitHub](https://github.com/qdm12/ddns-updater)
- Docker support
- Simple Web UI (read-only)
- Go
- See `~/projects/dns/ddns-updater/`

Issues:

- [Feature request: Record events log](https://github.com/qdm12/ddns-updater/issues/65)
  - Also about exposing Prometheus metrics

### Other clients

- [ddclient](https://github.com/ddclient/ddclient)
  Old, perl, no docker

## DynDNS providers

- [Njal.la ddns docs](https://njal.la/docs/ddns/)
- afraid.org
