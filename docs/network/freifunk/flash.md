# Freifunk flashing

## Flashing

- [Hamburg Freifunk Anleitung](https://hamburg.freifunk.net/anleitung)
- [Firmware images Hamburg-West](https://hamburg.freifunk.net/firmware/west)

### From OpenWRT

#### Password lost ?

- [Enter failsafe mode](https://wiki.openwrt.org/de/doc/howto/generic.failsafe)
- Settings LED blinks very fast

Reset to factory defaults:

```sh
firstboot -y
reboot -f
```

Then browse to <http://192.168.1.1>

#### Flash from OpenWRT

```sh
ssh 192.168.1.1
cd /tmp
wget http://firmware.pberg.freifunk.net/backfire/10.03/brcm-2.4/openwrt-brcm-2.4-squashfs.trx
wget http://www.bitrigger.de/pub/tmp/FW_WRT54GL_4.30.14.005_US_20091026.trx # original linksys firmware
dd bs=32 skip=1 if=openwrt-wrt54g-squashfs.bin of=openwrt-wrt-54g-squashfs.trx
mtd -r write openwrt-bcrm-2.4-squashfs.trx linux
```

### TFTP

- <https://wiki.freifunk-dresden.de/index.php/Tftp>

## Recover bricked TL-WR841N v9

- <https://crosp.net/blog/hardware/unbrick-tp-link-wifi-router-wr841nd-with-tftp-wireshark/>
- <https://forum.freifunk.net/t/router-zurueck-auf-original-firmware-flashen/3275>
- Download originalk firmware I.e. for a [TL-WR841N V9](https://www.tp-link.com/de/support/download/tl-wr841n/v9/)

Strip boot partition:

```sh
unzip TL-WR841N_V9_150310.zip
dd if=wr841nv9_wr841ndv9_en_3_16_9_up_boot\(150310\).bin \
  of=tplink_stripped.bin skip=257 bs=512
mv tplink_stripped.bin /home/varac/projects/freifunk/wr841nv9_tp_recovery.bin
```

Install tftpd-hpa:

```sh
sudo apt-get install tftpd-hpa
sudo systemctl stop tftpd-hpa.service
systemctl stop NetworkManager
```

Configure local IP addr to `192.168.0.66`
Watch network traffic:

```sh
sudo tcpdump -tni any host 192.168.0.66 | grep -v 'UDP, length'`
```

Start tftpf-hpa:

```sh
sudo in.tftpd -lL --secure --address 192.168.0.66:69 /home/varac/projects/freifunk
```

Then reboot router in recovery mode (turn off, hold reset button while powering it on)

Try multiple times, eventually it works!

## Freifunk (advanced) config mode

- <https://wiki.freifunk.net/Hamburg/Firmware#Weg_1:_Config_Mode>
- <https://pinneberg.freifunk.net/anleitungen/alles-zum-config-mode.html>
- Power on
- Plug in LAN cable
- Press QSS button (TP-Link models: `WPS/RESET`) until all LEDs blink
- Release button, device reboots
- Browse to <http://192.168.1.1>
- In the advanced config mode you can add ssh keys or set the root password.

## Re-configure from scratch

```sh
firstboot -y
reboot -f
```

## Setup after flashing

- LAN cable in LAN port 1
- Configure Laptop LAN connection with IP `192.168.1.2`
- Browse to <http://192.168.1.1/>
- Finish the config wizard
- After reboot the SSID `hamburg.freifunk.net` is available.
