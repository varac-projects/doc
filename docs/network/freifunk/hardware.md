# Freifunk Hardware

- [Openwrt wiki: Table of Hardware: Gluon supported](https://freifunk-nordhessen.de/technik/empfohlene-hardware-2020/)
- [FFHH Firmware: Supported devices](https://hamburg.freifunk.net/firmware)
- [Freifunk Nordhessen: Recommended hardware](https://freifunk-nordhessen.de/technik/empfohlene-hardware-2020/)
