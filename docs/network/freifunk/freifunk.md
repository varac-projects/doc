# Freifunk

- [Node start page](http://start.ffhh/)
- Local [status page](http://knoten.ffhh/) of the routers that I'm connected to

## Freifunk Hamburg

- [Freifunk Hamburg](https://hamburg.freifunk.net/)
  - [News](https://hamburg.freifunk.net/feed)
  - [Firmware: Supported devices](https://hamburg.freifunk.net/firmware)
- [Freifunk Wiki: Hamburg](https://wiki.freifunk.net/Hamburg)
  - [Hamburg/Richtfunknetz](https://wiki.freifunk.net/Hamburg/Richtfunknetz)
  - [Fux Richtfunk Overview](https://media.hamburg.freifunk.net/Richtfunknetz/Standorte/Altona-fux/0_Abstimmung/FUX-Abstimmung.pdf)
- [Github: freifunkhamburg](https://github.com/freifunkhamburg)
  - [Knotenkonfig](https://github.com/freifunkhamburg/Knotenkonfig)
- [FFHH Matrix channel](https://matrix.to/#/#ffhh:hackint.org)
- [Node administration](https://formular.hamburg.freifunk.net/#/)
  [Overview incl. Map](https://hamburg.freifunk.net/wo-wird-gefunkt)
  [Big map of Hamburg](https://map.hamburg.freifunk.net/)

## Background/management traffic

- Batman advanced consumes arounf 1 GB management traffic
  - Measured on a Freifunk node with LTE uplink. FFHH-Ost, the biggest subnet
  - Therefore not recommended in LTE setup

## IPv6 address discovery

Install `pamac install iputils-ping6-symlink` AUR package.

```sh
$ ping6 ff02::1%wlp3s0|head
64 bytes from fe80::1441:95ff:fe40:f7dc: icmp_seq=1 ttl=64 time=17.8 ms
64 bytes from fe80::6670:2ff:fe59:3b26: icmp_seq=1 ttl=64 time=35.9 ms (DUP!)
...
```

- First address is the own address
  t- Second address is the router address

## SSH

Connect:

```sh
ssh knoten.ffhh
```

or

```sh
ssh -6 root@fe80::6670:2ff:fe8a:c4a2%wlp3s0
```

## OpenWrt commands

Show switch config:

```sh
swconfig dev switch0 show
```

### Batman control tool

```sh
batctl o    # show all neighbor nodes, incl. nodes connected via vpn
```

Show only wlan neighbors on vpn-enabled node:

```sh
batctl o | grep -v mesh-vpn
```

Show only wlan neighbors on non-vpn node:

```sh
batctl o | grep -v ') 66:70:02:8a:c4:a2'
batctl tl   # show client mac addresses
batctl tg   # "
batctl gwl  # show gateways
batctl s    # stats
```

Show own MAC:

```sh
ifconfig |grep 66:70
```

Ping node with mac add:

```sh
batctl p 66:70:02:8a:c4:a2 i
```

Traceroute node with mac add:

```sh
batctl tr de:ad:be:ef:90:01
```

## Hardware

- <https://www.routerdefaults.org/tp-link/tl-wr841nd>
