# Ubiquity UniFi

- [Unify products](https://ui.com/eu/en/introduction)

## Hardware

- [Wifi](https://ui.com/eu/en/wifi)
  - [Outdoor](https://ui.com/eu/en/wifi/outdoor)
    - [U7 outdoor](https://techspecs.ui.com/unifi/wifi/u7-outdoor?s=eu)

## UniFi Network Server (formerly unifi controller)

- [Download UniFi Network Server](https://ui.com/download)
  - Below are the supported models
