# ethtool

- [ubuntu users wiki: ethtool](http://wiki.ubuntuusers.de/ethtool)

```sh
sudo ethtool -s eth0 speed 100 duplex full autoneg on
```
