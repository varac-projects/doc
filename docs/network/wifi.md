# Wifi

- [n vs ac speed](https://www.forbes.com/sites/gordonkelly/2014/12/30/802-11ac-vs-802-11n-wifi-whats-the-difference/?sh=2a4305213957)

## Laptop form factors

- [mini PCIe](https://www.assured-systems.com/uk/news/article/what-are-mini-pci-express-mpcie-cards/)
- [m2](https://en.wikipedia.org/wiki/M.2)

## Open source wifi drivers/devices

- [Purism Decides on Wireless Qualcomm Atheros 802.11n over 802.11ac](https://puri.sm/posts/purism-decides-qualcomm-atheros-802-11n/)
  - [librem-14 2021](https://puri.sm/products/librem-14/) still ships with Atheros 802.11n device
- [Comparison of open-source wireless drivers](https://en.wikipedia.org/wiki/Comparison_of_open-source_wireless_drivers)
- [wireless kernel wiki drivers page](https://wireless.wiki.kernel.org/en/users/drivers)
- [FSF RYF wireless devices](https://ryf.fsf.org/categories/wireless-adapters?sort_by=created&sort_order=DESC&vendor=All&page=0)

### Atheros

- [Wikipedia: Atheros](https://de.wikipedia.org/wiki/Qualcomm_Atheros)

### 802.11ac support

Seems there are still no ac/ax devices with pure open source drivers which
doesn't need propriatary firmware.

[ath10x kernel driver and supported devices](https://wireless.wiki.kernel.org/en/users/drivers/ath10k)
