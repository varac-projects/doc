# Remove libvirt bridge

```sh
virsh net-list
virsh net-destroy default
virsh net-undefine default

systemctl stop libvirtd.service libvirt-bin.service libvirt-guests.service
systemctl mask libvirtd.service libvirt-bin.service libvirt-guests.service
```
