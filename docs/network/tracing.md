# Network tracing

Show established outbound connections:

```sh
lsof -i -P | grep ESTABLISHED | awk '{split($9,s,">"); printf "%-10s %-4s %s\n", $1, $8, s[2]}'
```

## tcpflow

- [Monitoring HTTP Requests on a Network Interface in Real Time: tcpflow](https://www.baeldung.com/linux/monitoring-http-requests-network-interfaces#using-tcpflow)

Usage:

```sh
sudo tcpflow -p -c -i ens3 port 80
```

## httpry

- [Monitoring HTTP Requests on a Network Interface in Real Time: httpry](https://www.baeldung.com/linux/monitoring-http-requests-network-interfaces#using-httpry)

```sh
sudo httpry -i wlp0s20f3
sudo httpry -i wlp0s20f3 -m post
```

## tcpdump

[Show http headers](https://help.mulesoft.com/s/article/How-to-View-HTTP-Headers-From-Command-Line-in-Raw-Tcpdump-Command):

```sh
tcpdump -A -s 10240 'tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)' | \
  egrep --line-buffered "^........(GET |HTTP\/|POST |HEAD )|^[A-Za-z0-9-]+: " | \
  sed -r 's/^........(GET |HTTP\/|POST |HEAD )/\n\1/g'
```

## Other tools

- [Justniffer](https://onotelli.github.io/justniffer/)
  No Debian package
