# Bittorrent

## Web clients

- [What's your preferred web UI torrent client?](https://www.reddit.com/r/selfhosted/comments/uapgui/whats_your_preferred_web_ui_torrent_client/)

- [qbittorrent](https://github.com/qbittorrent/qBittorrent/)
  - 333 contributors, 26k stars
  - Uses [libtorrent](https://github.com/arvidn/libtorrent)
    - 132 contributors, pretty active
- [deluge](https://github.com/deluge-torrent/deluge)
  - 91 Contributors, 1.5k stars
  - Also uses libtorrent
  - Plugin system
- [rutorrent](https://github.com/Novik/ruTorrent)
  - 138 contributors, 2k Stars
  - Frontend to [rtorrent](https://github.com/rakshasa/rtorrent)
    - Last rtorrent release 2019
- [Transmission](./transmission.md)
