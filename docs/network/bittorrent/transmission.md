# Transmission

- [Website](https://transmissionbt.com/)
- [](https://github.com/transmission/transmission)
- [Transmission project is abandoned?](https://github.com/transmission/transmission/issues/7058)

## Remote user/pw

The default rpc-username and password is “transmission”

```sh
systemctl stop transmission-daemon.service
vi /etc/transmission-daemon/settings.json
systemctl start transmission-daemon.service
```

## ACL

```sh
chown -R debian-transmission:debian-transmission /media/Media/transmission-daemon/
```

## Transmission and Gluetun

- [Script to change transmission listening port](https://forum.transmissionbt.com/viewtopic.php?t=17359)

## Batch change destination location in resume files

- [transmission-batch-move](https://github.com/vlevit/transmission-batch-move)
