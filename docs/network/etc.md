# Networking etc

## Block network per process

```console
$ unshare -r -n ping 127.0.0.1
ping: connect: Network is unreachable
```
