# Curl

## Usage

### Be silent but show errors

```sh
curl --fail --silent --show-error https://localhost:9100
```

### Preview a site without proper name resolution

```sh
curl -kL --resolve tym-flow.de:443:52.58.18.134 https://tym-flow.de
```

## curl-impersonate

- [GitHub](https://github.com/lwthiker/curl-impersonate)
- A special build of curl that can impersonate Chrome & Firefox
- Useful for i.e. [markdown-link-check](https://github.com/tcort/markdown-link-check),
  when some websites return 200 in the browser, but 403 with standard curl

  - Although it doesn't always work

- [curl-impersonate-firefox AUR package](https://aur.archlinux.org/packages/curl-impersonate-firefox)
