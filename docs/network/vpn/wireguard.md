# Wireguard

- [ubuntuuser wiki: Wireguard](https://wiki.ubuntuusers.de/WireGuard)
- [OpenWRT guide](https://openwrt.org/docs/guide-user/services/vpn/wireguard)

## Hardware support

- [Fritzbox Wireguard blog post](https://avm.de/aktuelles/2022/wireguard-vpn-war-nie-so-einfach/)

## Setup

- [Medium tutorial](https://medium.com/@novysf/wireguard-server-client-with-roaming-ip-setup-498d708ebb7c)
- [IPv6 support](https://github.com/esphome/feature-requests/issues/1444)
  - [Home-assistant Wireguard plugin](https://github.com/hassio-addons/addon-wireguard)
  - [Home-assistant community wireguard plugin](https://community.home-assistant.io/t/home-assistant-community-add-on-wireguard/134662)

## Usage

Start:

```sh
wg-quick up wg0
```

## Network-manager integration

see `../network-manager.md`

### Server

```sh
sudo apt install wireguard wireguard-tools linux-headers-amd64
```

Generate keys

```sh
wg genkey | sudo tee /etc/wireguard/server_private.key | \
  wg pubkey | sudo tee /etc/wireguard/server_public.key
```

Server config:

```sh
[Interface]
Address = 10.11.12.1/24
ListenPort = 51820
PrivateKey = ...

[Peer]
PublicKey = ...
AllowedIPs = 10.11.12.2/32
```

### Client

```sh
[Interface]
Address = 10.11.12.2/24
DNS = 10.10.10.1
PrivateKey = ...

[Peer]
PublicKey = ...
AllowedIPs = 0.0.0.0/0
Endpoint = 12.34.56.78:51820
PersistentKeepalive = 25
```

## Run wireguard in a container

### Together with transmission

- [SebDanielsson/compose-transmission-wireguard)](https://github.com/SebDanielsson/compose-transmission-wireguard)
- [aerickson/docker-transmission-wireguard](https://github.com/aerickson/docker-transmission-wireguard):
  stale, last commit 2020-10

## Mullvad Wireguard

- [Mullvad server list](https://mullvad.net/en/servers)
- [Mullvad guide](https://www.mullvad.net/de/guides/running-wireguard-router)

### Mullvad config

- Key: ..
- Pub: ..
- IP: `10.99.86.112/32,fc00:bbbb:bbbb:bb01::5670/128`

## Wireguard ansbible roles

- [Wireguard roles in Galaxy](https://galaxy.ansible.com/search?deprecated=false&keywords=wireguard&order_by=-relevance&page=1&page_size=80)
- [Thulium-Drake/ansible-role-wireguard](https://github.com/Thulium-Drake/ansible-role-wireguard)
  - Best role so far from looking at it.
- [githubixx/ansible-role-wireguard](https://github.com/githubixx/ansible-role-wireguard)
  - Very popular
  - Big downside: [Removes config when ansible-playbook is run against a single endpoint](https://github.com/githubixx/ansible-role-wireguard/issues/79)
- [ansibleguy/infra_wireguard](https://github.com/ansibleguy/infra_wireguard)
  - Looks complex, generates keys and places them in files on the controller
- [lablabs/ansible-collection-wireguard](https://github.com/lablabs/ansible-collection-wireguard)
  - Debian 11 is supported but the system must use systemd-networkd networking
    service instead of Network Manager.
- [abelfodil/ansible_role_wireguard](https://galaxy.ansible.com/abelfodil/ansible_role_wireguard)
  - Wrong link to Github repo
- [mawalu/wireguard-private-networking](https://github.com/mawalu/wireguard-private-networking)
  - Stale !

## Wirgeguard gnome indicator

- [Wireguard indicator](https://github.com/atareao/wireguard-indicator)

## Wirguard overlays

- [Identity management for WireGuard](https://lwn.net/Articles/910766/)
- [Debating VPN options](https://anarc.at/blog/2022-10-28-vpn-considerations/)

### Other projects

- [NetBird](https://netbird.io)
  Fully open source, but no helm chart for server components
- [netmaker](https://www.netmaker.org): No helm charts
- [Firezone](https://www.firezone.dev/): No helm charts
