# Tailscale

- [Tailscale Website](https://tailscale.com)
- [GitHub](https://github.com/tailscale/tailscale)
- CLient open source, original control server component not,
  although there's
  - [Headscale](./headscale.md)
  - [ionscale](https://github.com/jsiebens/ionscale)

## Docs

- [How Tailscale works](https://tailscale.com/blog/how-tailscale-works/#encrypted-tcp-relays-derp)
- [Configuring Linux DNS](https://tailscale.com/kb/1188/linux-dns/)
- [Manage your Tailscale resources with Terraform](https://tailscale.com/blog/terraform/)
  - [tailscale terraform provider](https://registry.terraform.io/providers/tailscale/tailscale/latest)
- [Network access controls (ACLs)](https://tailscale.com/kb/1018/acls/)
- [Tailscale on Kubernetes](https://tailscale.com/kb/1185/kubernetes/)

## Install

- Client in Golang, packages in a [PPA](https://pkgs.tailscale.com/stable/)
- [Repology.org](https://repology.org/project/tailscale/versions)
- [tailscale snap](https://snapcraft.io/tailscale) is outdated
- [Download tailscale](https://tailscale.com/download/)

Arch:

```sh
sudo pacman -S tailscale
```

### Raspberry Pi

[Install Tailscale on Rasperry Pi](https://tailscale.com/download/linux/rpi-bullseye)

```sh
sudo apt-get install apt-transport-https
curl -fsSL https://pkgs.tailscale.com/stable/raspbian/$(lsb_release  -cs).noarmor.gpg \
  | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg > /dev/null
curl -fsSL https://pkgs.tailscale.com/stable/raspbian/$(lsb_release  -cs).tailscale-keyring.list \
  | sudo tee /etc/apt/sources.list.d/tailscale.list
sudo apt-get update
sudo apt-get install tailscale
```

## DNS

- Tailscale overrides `/etc/resolv.conf`, when the tunnel is up local name
  resolving doesn't work anymore.
  - [Why is resolv.conf being overwritten?](https://tailscale.com/kb/1235/resolv-conf/)
  - [Configuring Linux DNS](https://tailscale.com/kb/1188/linux-dns/)
  - [Interface-specific DNS not set if not overriding local nameserver](https://github.com/juanfont/headscale/issues/1547#issuecomment-1837947673)
- [Private DNS with MagicDNS](https://tailscale.com/blog/2021-09-private-dns-with-magicdns)

## Usage

- State file: `/var/lib/tailscale/tailscaled.state`

Parse state file content:

```sh
sudo jq -r ._profiles /var/lib/tailscale/tailscaled.state  | base64 -d | jq
sudo jq -r '."profile-e35a"' /var/lib/tailscale/tailscaled.state | base64 -d | jq
```

### Further usage

```sj
tailscale status
tailscale ip casita
```

## Issues

### Captive portals

- [Captive Wi-fi portal login didn't show up](https://github.com/tailscale/tailscale/issues/722)
  - [Better support captive portals in Hotels, Airports, etc](https://github.com/tailscale/tailscale/issues/1634)
- [FR: Flag health errors for self-signed or otherwise invalid certificates](https://github.com/tailscale/tailscale/issues/3198)
  - [MR: control/controlclient: cache control key](https://github.com/tailscale/tailscale/pull/7493)
- [Firefox Docs: Captive portal detection](http://detectportal.firefox.com/canonical.html)

#### How to fix Tailscale with captive portals

- Problem: No DNS resolution works because Tailscale is the default
  resolver (configured for DNS Domains: `~.`)
- Solution: Add `~.` to the `Additional search domains` of the configured
  Wifi connection in the Network Manager UI.
  This will route _all_ DNS queries directly over the wifi interface,
  circumventing the catch-all DNS resolution of Tailscale.
  **Beware**: This might have other side-effects !

## Ansible role

see [headscale.md](./headscale.md)

## Tailscale on Kubernetes

- [Tailscale on Kubernetes](https://tailscale.com/kb/1185/kubernetes)
- [Helm Charts repository](https://tailscale.com/kb/1236/kubernetes-operator#helm)
  - Contains currently only the helm chart for [Tailscale K8s operator](https://tailscale.com/kb/1236/kubernetes-operator)
- [K3s Tailscale integration](https://docs.k3s.io/installation/network-options#integration-with-the-tailscale-vpn-provider-experimental)

## nftables

- [Suppor nftables is still WIP](https://github.com/tailscale/tailscale/issues/391)
  - [nftables support just got merged](https://github.com/tailscale/tailscale/commit/80b912ace038ca5420059d62cbb7952949c7e3dd)

> This commit adds nftable rule injection for tailscaled.
> If tailscaled is started with envknob
> `TS_DEBUG_USE_NETLINK_NFTABLES = true`,
> the router will use nftables to manage firewall rules.

## Subnet router

- [Docs: Subnet routers](https://tailscale.com/kb/1019/subnets)
