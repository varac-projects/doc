# Headscale

- [Website](https://headscale.net/)
- [GitHub](https://github.com/juanfont/headscale)
  Open source control server software
- [Docs](https://headscale.net/stable/)
  - [ACLs use case example](https://headscale.net/stable/ref/acls/#acls-use-case-example)
  - [Setting custom DNS records](https://headscale.net/stable/ref/dns/#setting-custom-dns-records)
- [Helm charts](https://artifacthub.io/packages/search?ts_query_web=headscale&sort=relevance&page=1)
  - [gabe565 chart Artifacthub](https://artifacthub.io/packages/helm/gabe565/headscale)
  - [gabe565 chart GitHub](https://github.com/gabe565/charts/tree/main/charts/headscale)
- [Container images](https://github.com/juanfont/headscale/pkgs/container/headscale)
- [FR: Support for WireGuard only peers](https://github.com/juanfont/headscale/issues/1545)

## Headscale Web UI

- [Headscale docs: Web Interface options](https://headscale.net/stable/ref/integration/web-ui/)
- [Any WEB UI planned?](https://github.com/juanfont/headscale/issues/234)
- `gurucomputing/headscale-ui`: See below
- [GoodiesHQ/headscale-admin](https://github.com/GoodiesHQ/headscale-admin)
  - Last commit 2024-04
  - [Docker image](https://hub.docker.com/r/simcu/headscale-ui/tags)
- [Arispex/headscale-ui](https://github.com/Arispex/headscale-ui)
  - Last commit 2024-02
  - JS
  - Only few commits
- [ifargle/headscale-webui](https://github.com/ifargle/headscale-webui)
  - [State of project - Abandoned?](https://github.com/iFargle/headscale-webui/issues/132)
  - Stale, last commit 2023-05
- [cockpit-headscale](https://github.com/spotsnel/cockpit-headscale)
  - Stale, last commit 2023-07

### gurucomputing/headscale-ui

- [GitHub](https://github.com/gurucomputing/headscale-ui)
- [Container images](https://github.com/gurucomputing/headscale-ui/pkgs/container/headscale-ui)
- Bundles in both headscale helm charts
  - Configures ingress for `/web`
- Prometheus endpoint `127.0.0.1:9090/metrics`
  - No real useful metrics

#### Configure headscale-ui

Connect UI to headscale server:

```sh
kubectl -n headscale exec -it -c headscale headscale-6f4b745765-z9dpw -- \
  headscale apikeys create --expiration 3y
kubectl -n headscale exec -it -c headscale headscale-6f4b745765-z9dpw -- \
  headscale apikeys list
```

Server settings:

- Headscale API Key: `$(gopass show --password token/$HEADSCALE_DOMAIN/api-token)`

#### Headscale UI Usage

New client:

- Create a [pre-auth key](https://tailscale.com/kb/1085/auth-keys/)
- `Users` -> `Varac` -> `Preauth Keys +`

## Ionscale

- [GitHub](https://github.com/jsiebens/ionscale)
- Active, but lacking documentation
- No UI ([headscale-ui: ionscale support ?](https://github.com/gurucomputing/headscale-ui/issues/128))
- No helm chart so far

## Manual registration and login

If the Headscale user `varac` doesn't exist yet, create it (only once):

```sh
headscale user create varac
headscale user list
```

Request node registration from a device:

```sh
sudo tailscale up --login-server https://$HEADSCALE_DOMAIN
```

Then follow the link from the browser to register the node in headscale:

```sh
headscale nodes register --user varac --key nodekey:$NODEKEY
```

### Using a pre-auth key (deprecated)

Generate a preauth-key:

```sh
headscale --user $NODEUSER preauthkeys create --reusable --expiration 360d
```

Login using a preauth-key:

```sh
sudo tailscale up --login-server https://$HEADSCALE_DOMAIN \
  --authkey $(gopass show --password token/$HEADSCALE_DOMAIN/preauth-key/varac)
```

## Ansible role

- [artis3n-tailscale](https://github.com/artis3n/ansible-role-tailscale)
- State file on each machine: `~/.local/state/artis3n-tailscale`

### Bootstrap new machine

[Getting started: Using a preauthkey](https://headscale.net/stable/usage/getting-started/?h=pre#using-a-preauthkey)

Before configuring a device:

```sh
headscale user create varac
```

Create a preauthent key:

```sh
headscale --user varac preauthkeys create --reusable --expiration 3y
```

Or create a preauthkey using a tag:

```sh
headscale --user varac preauthkeys create --reusable --expiration 3y --tags tag:oas2
```

Then store token in Vaultwarden (`token/hs.k.varac.net/varac/preauth-key/ANSIBLE_INVENTORY_HOSTNAME`),
add the device to the `vpn` inventory group and run ansible:

```sh
ansible-playbook -l zancas -t tailscale site.yml
```

Finally, accept the additional routes in the headscale UI.
