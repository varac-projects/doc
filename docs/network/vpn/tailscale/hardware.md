# Tailscale on special hardware

## OpenWrt

- [Openwrt wiki: Tailscale user guide](https://openwrt.org/docs/guide-user/services/vpn/tailscale/start)
- Tailscale packages are huge and flash size >16 MB are needed!
  - [Openwrt wiki: Tailscale user guide / Installation on storage constrained devices](https://openwrt.org/docs/guide-user/services/vpn/tailscale/start#installation_on_storage_constrained_devices)
- [Gl-Inet router support table](https://docs.gl-inet.com/router/en/4/tutorials/tailscale/)
  - Currently, only one cellular router supports Tailscale,
    the expensive [GL-X3000 (Spitz AX)](https://store.gl-inet.com/collections/spitz-ax-gl-x3000/products/spitz-ax-gl-x3000-wi-fi-6-4g-lte-sim-openwrt)
  - There are no GL-Inet routers including a DSL modem

### openwrt-tailscale-enabler

- [GitHub](https://github.com/adyanth/openwrt-tailscale-enabler)
- Brings Tailscale to low powered OpenWRT devices
- Dynamically downloads Tailscale packages into RamFS to circumvent nvram
  size
- Auto-updates to latest Tailscale image on each reboot
- Runs on devices with [128 Mb RAM](https://github.com/adyanth/openwrt-tailscale-enabler/issues/15)
  - "Need to have at least 11+16 = ~27 MB of free space in /tmp
    (which is usually in RAM) to be able to use this."
- [Question about persistence](https://github.com/adyanth/openwrt-tailscale-enabler/issues/50)
- [Install](https://github.com/adyanth/openwrt-tailscale-enabler?tab=readme-ov-file#tailscale-on-openwrt)

[Uninstall](https://github.com/adyanth/openwrt-tailscale-enabler/issues/47#issuecomment-2105549118):

```sh
/etc/init.d/tailscale stop
rm /usr/bin/tailscale /usr/bin/tailscaled /etc/init.d/tailscale /tmp/tailscale*
rm -rf /var/lib/tailscale
reboot
```

Issues:

- Couldn't make it work reliably. DNS resolution didn't work, SSH to tailscale
  nodes neither (2024-06)

### OpenWrt issues

- [iptables-nft issue](https://github.com/tailscale/tailscale/issues/391)
- Solution: `opkg install iptables-nft`

## ESP

- [FR: ESP32 library](https://github.com/tailscale/tailscale/issues/5220)
