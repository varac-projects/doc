# VPNC (Cisco / IpSec)

* Also used in [Fritz boxes](https://avm.de/service/wissensdatenbank/dok/FRITZ-Box-7430/1471_VPN-zur-FRITZ-Box-unter-Linux-einrichten/)
* [NetworkManager-vpnc plugin](https://gitlab.gnome.org/GNOME/NetworkManager-vpnc/)
* [Unable to save VPN connection: "connection.interface-name must not be empty](https://gitlab.gnome.org/GNOME/NetworkManager-vpnc/-/issues/8)
