# VPN

## Test for dns leaks

see [DNS Is Leaking! Uses ISP DNS instead of VPN DNS!](https://github.com/haugene/docker-transmission-openvpn/issues/1455):

Run the following from the container:

    wget https://raw.githubusercontent.com/macvk/dnsleaktest/master/dnsleaktest.sh && chmod +x dnsleaktest.sh && ./dnsleaktest.sh

## VPN Containers

For Gluetun see [gluetun.md](./gluetun.md)

### transmisson-openvpn

- [transmisson-openvpn: Supported providers](https://haugene.github.io/docker-transmission-openvpn/supported-providers/#out-of-the-box_supported_providers)
- Stalled [Wireguard support](https://github.com/haugene/docker-transmission-openvpn/pull/2607)
