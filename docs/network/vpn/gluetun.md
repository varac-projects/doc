# Gluetun

- [Guthub](https://github.com/qdm12/gluetun)
- [Docs wiki](https://github.com/qdm12/gluetun-wiki/)
  - [Provider support](https://github.com/qdm12/gluetun-wiki/tree/main/setup/providers)

## OpenVPN and port forwarding

- [Protonvpn filtering by features](https://github.com/qdm12/gluetun-wiki/blob/main/setup/providers/protonvpn.md#optional-environment-variables)
  `PORT_FORWARD_ONLY`: To auto-pick a server which supports port-forwarding i.e.

## Wireguard and port forwarding

- [Discussion: Wireguard and port forwarding](https://github.com/qdm12/gluetun/discussions/1825)
- [Feature request: Support for Wireguard](https://github.com/qdm12/gluetun/issues/134)
  Overview issue which providers are supported using Wireguard
- [Port forwarding](https://github.com/qdm12/gluetun-wiki/blob/main/setup/options/port-forwarding.md)
- [VPN server port forwarding](https://github.com/qdm12/gluetun-wiki/blob/main/setup/advanced/vpn-port-forwarding.md#native-integrations)

### Gluetun provider support

List provider servers and if they are properly categorized:

```sh
podman run --rm qmcgaw/gluetun:latest format-servers -protonvpn
```

#### ProtonVPN

- [ProtonVPN](https://github.com/qdm12/gluetun-wiki/blob/main/setup/providers/protonvpn.md)
- Native wireguard and port-forwarding support

#### PrivateVPN

- [PrivateVPN]
- Servers are not tagged with any tags/categories
- Native port-forwarding support added after latest reelease ([v3.39.0](https://github.com/qdm12/gluetun/releases/tag/v3.39.0))
  in [feat(privatevpn): support natively port forwarding #2285](https://github.com/qdm12/gluetun/pull/2285)
  but [it's not working](https://github.com/qdm12/gluetun/pull/2285#issuecomment-2322640194)
- Wireguard mode needs to be configured via the [custom provider mode](https://github.com/qdm12/gluetun-wiki/blob/main/setup/providers/custom.md#wireguard)
