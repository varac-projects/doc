# Tinyproxy

- [Website](https://tinyproxy.github.io/)

Install:

    sudo pacman -S tinyproxy

Configure:

- Tinyproxy config at `/etc/tinyproxy/tinyproxy.conf`
- Add proxy env vars to shell config, i.e. `~/.zshrc`
- Configure dedicated applications which doesn't honor well proxy env vars
  - GnuPG: Add `http-proxy ` to:
    - `~/.gnupg/dirmngr.conf`
    - `/root/.gnupg/dirmngr.conf`
    - `/etc/pacman.d/gnupg/dirmngr.conf`
    - Still, gpg always will fail during WKD key lookups

Start:

    sudo systemctl enable tinyproxy.service
    sudo systemctl start tinyproxy.service

Usage:

Test it with:

    http_proxy=127.0.0.1:8888 curl example.com

Logs:

    sudo journalctl -u tinyproxy.service -f

## Issues

- [feature proposal: support proxy-auto-configuration scripts ("wpad.dat")](https://github.com/tinyproxy/tinyproxy/issues/224)

## Proxify apps which doesn't honor proxy env vars

Some apps doesn't support proxy configuration via env vars.
I.e. NodeJS apps which use `node-fetch`: [node-fetch does not work behind a proxy](https://github.com/node-fetch/node-fetch/issues/1770):

- i.e. repolinter

The solution is to use wrapper apps which redirect traffic to configured proxy,
i.e.:

- [proxychains-ng](https://github.com/rofl0r/proxychains-ng)
  - works well for i.e. repolinter
  - Configure it in `/etc/proxychains.conf`

Other possible solutions (not tried so far):

- [redsocks](https://github.com/darkk/redsocks)

## Podman

When using Podman or Docker the container needs to access
Tinyproxy on the host. In order to do that it must use different
env vars than the host. Add this to `/etc/containers/containers.conf.d/proxy.conf`:

    [containers]
    # Don't automatically pass proxy env vars to container
    http_proxy = false

    env = [
      "http_proxy=http://host.containers.internal:8888",
      "https_proxy=http://host.containers.internal:8888",
      "HTTP_PROXY=http://host.containers.internal:8888",
      "HTTPS_PROXY=http://host.containers.internal:8888",
      "no_proxy=localhost,.local,.svc,.svc.cluster.local,10.0.0.0/8",
      "HTTP_PROXY_HOST=host.containers.internal",
      "HTTP_PROXY_PORT=8888",
      "JAVA_OPTS=-Dhttp.proxyHost=host.containers.internal -Dhttp.proxyPort=8888 \
         -Dhttps.proxyHost=host.containers.internal -Dhttps.proxyPort=8888",
    ]

Tinyproxy also needs to be accessible from the container, so it should not bind
only to `localhost` as it is configured by default.
Comment the `Listen` parameter and add the (DHCP) IP which podman containers
use to connect (via `host.containers.internal`) in `/etc/tinyproxy/tinyproxy.conf`:

    # Listen 127.0.0.1
    Allow 10.27.13.162
