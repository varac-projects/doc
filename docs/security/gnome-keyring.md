# Gnome Keyring

- [Arch wiki](https://wiki.archlinux.org/title/GNOME/Keyring)
- [Seahorse](https://wiki.gnome.org/Apps/Seahorse): GUI

## CLI tools

### keyring (python)

- [GitHub](https://github.com/jaraco/keyring)
- AUR package: `python-keyring`

Add pw:

```sh
$ keyring set test-server username
Password for 'username' in 'test-server':
```

Get pw:

```sh
keyring get test-server username
```

### lssecret

[Gitlab](https://gitlab.com/GrantMoyer/lssecret)
List all secret items using libsecret (e.g. GNOME Keyring)

Install:

```sh
pamac install lssecret-git
```

Usage:

```sh
lssecret
```

### secret-tool

- [Secret Tool Explained](https://ict4g.net/adolfo/notes/admin/secret-tool.html)

Usage:

```sh
secret-tool lookup unique ssh-store:/home/varac/.ssh/id_ed25519
```

## Deprecated cli tools

- [libsecret-cli](https://github.com/Lyude/libsecret-cli)
  - Last commit 2021
- [Keychain](https://github.com/funtoo/keychain)
  - Last release 2018
