# Firware update daemon fwupd

* [User docs](https://fwupd.org/lvfs/docs/users)
* [Arch wiki](https://wiki.archlinux.org/index.php/Fwupd)

Install cli tool and UI:

    sudo pacman -S fwupd gnome-firmware
    sudo apt install fwupd

Refresh firmware database:

    sudo fwupdmgr refresh --force

    fwupdmgr get-devices

Include non-updateable devices (such as coreboot atm i.e.):

    fwupdmgr get-devices --show-all-devices

Show available updates:

    fwupdmgr get-updates

Upgrade:

    fwupdmgr update

Issues:

* [System Firmware update only possible in UEFI mode](https://github.com/merge/skulls/issues/164)

## Coreboot support

2019-11, fwupd 1.3.3-2: Coreboot detected with `fwupdmgr get-devices --show-all-devices`,
but device is shown as `non-updatable`.

[LVFS website search](https://fwupd.org/lvfs/search?value=coreboot) also doesn't
show more than one coreboot-supported device.

## WEB-IF search

* [Search fwupd database](https://fwupd.org/lvfs/search)
* [Search for coreboot](https://fwupd.org/lvfs/search?value=coreboot)

## GUI

* GNOME Software (`Updates` tab)
