# Hardware authentication device (security key/token)

- [Wikipedia: Security token](https://en.wikipedia.org/wiki/Security_token)

- [Unlocking LUKS2 volumes with TPM2, FIDO2, PKCS#11 Security Hardware](http://0pointer.net/blog/unlocking-luks2-volumes-with-tpm2-fido2-pkcs11-security-hardware-on-systemd-248.html)

## Questions / issues

- What if I forget my key at home ?
- What if I lose my key ?
  - Backup Fido keys ?
- What if my solokey get stolen ?

## Fido test website

- [Webauthn.io](https://webauthn.io/)
- [dongleauth.info](ttps://www.dongleauth.info)
  List of websites supporting OTP or WebAuthN

## Open Source / open hardware options

- [Nitrokey](https://www.nitrokey.com/products/nitrokeys)

## Solokeys

- [Website](https://solokeys.com/)
- [Technical docs](https://docs.solokeys.dev/)
- [Arch wiki: Solokey](https://wiki.archlinux.org/title/Solo)
- [GitHub: solo1-cli](https://github.com/solokeys/solo1-cli)
  - Last commit 2022-03
  - [AUR package: solo1](https://aur.archlinux.org/packages/solo1)
- [GitHub: solo2-cli](https://github.com/solokeys/solo2-cli)
  - [AUR package: solo2-cli](https://aur.archlinux.org/packages/solo2-cli)
    - [Fails to build at linking stage](https://github.com/briansmith/ring/issues/1444)
    - [Possible workaround](https://gitlab.archlinux.org/archlinux/packaging/packages/pacman/-/issues/20)

Install toolset:

```sh
pamac install solo1
```

## Usage

```sh
solo key credential ls
```

Update firmware:

```sh
solo key update
```

Test PIN:

```sh
solo key credential ls
```

Verify hardware and firmware:

```sh
solo key verify
```

## Other hardware options

- [token2](https://www.token2.com/home)
