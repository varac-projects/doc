# Malware / Anti-Virus

- [14 Best Free Bootable Antivirus Tools](https://www.lifewire.com/free-bootable-antivirus-tools-2625785)

## Dedicated live-CDs

- Avira Rescue System
  - Big 1.8G ISO
  - Didn't boot
- [ESET SysRescue Live](https://www.eset.com/de/support/sysrescue/)
  - [Wikipedia](https://de.wikipedia.org/wiki/ESET_NOD32_Antivirus)
  - `.img` or `.iso` downloadable
    - Choose `.iso`, `.img` didn't boot: [Undef symbol FAIL: x86_init_fpu](https://forum.eset.com/topic/28441-usb-sysrescue-drive-wont-boot)
  - 2022-09 download based on Ubuntu 18.04, LXDE
  - Virus DB update possible
  - Clean UI
- [Norton Bootable Recovery Tool](https://support.norton.com/sp/static/external/tools/nbrt.html)
  No version information available on download page

## LiveCDs with antivirus software

- [Using grml to scan for viruses](https://wiki.grml.org/doku.php?id=antivirus)

## No option

- Panda Cloud Cleaner Rescue ISO: Only Windows, not a live CD
- [Trend Micro Rescue Disk](https://www.trendmicro.com/de_de/forHome/products/free-tools/rescue-disk.html)
  Downloads an `.exe`
- [Windows Defender Offline](https://support.microsoft.com/en-us/windows/help-protect-my-pc-with-microsoft-defender-offline-9306d528-64bf-4668-5b80-ff533f183d6c)
  Downloads as an EXE file instead of directly as an ISO
- [Dr.Web LiveDisk](https://free.drweb.com/aid_admin/)
  - Russian anti-malware company Doctor Web
  - Recent version on website
- [Kaspersky Rescue Disk](https://www.lifewire.com/kaspersky-rescue-disk-review-2617884)
  - [Russian company](https://de.wikipedia.org/wiki/Kaspersky_Lab)

## Discontinued / outdated

- Anvi Rescue Disk
- AVG Rescue CD
- Comodo Rescue Disk
- Zillya! LiveCD
- VBA32 Rescue
- [F-Secure Rescue CD](https://community.f-secure.com/en/discussion/115362/does-f-secure-rescue-cd-is-still-alive)

## Payment required

- [Desinfec’t](https://de.wikipedia.org/wiki/Desinfec%E2%80%99t)
