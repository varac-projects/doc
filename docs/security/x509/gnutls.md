# GnuTLS / certtool

## Cert Validation with certtool

```sh
apt-get install gnutls-bin
```

## Download remote Cert

```sh
~/bin/custom/ssl-recv-cert.sh ix.de -p 443 > /tmp/cert.pem
```

## Show remote cert

```sh
gnutls-cli --print-cert example.com
```

Or with custom script:

```sh
~/bin/custom/ssl-show-cert.sh ix.de -p 443
```

## Print Info

```sh
certtool -i < /tmp/cert.pem
```

## Verify

### Proper CA

```sh
gnutls-cli ix.de -p 443 --print-cert < /dev/null | certtool --verify-chain
```

### Verify self-signed certs against a local CA

```sh
certtool --verify-chain < ./leap/providers/bitmask.net/keys/client/openvpn.pem < /home/varac/dev/projects/leap/git/bitmask/files/ca/client_ca.crt

gnutls-cli leap.se -p 443 --insecure --print-cert < /dev/null > /tmp/cert.pem
certtool --verify-chain < /tmp/cert.pem < /etc/ssl/certs/AddTrust_External_Root.pem
```
