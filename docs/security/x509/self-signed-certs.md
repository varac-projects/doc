# Create self-signed certs

<https://github.com/FiloSottile/mkcert>

## Selfsigned Cert

    openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -subj "/CN=$(hostname -f)" -keyout /tmp/key.pem -out /tmp/cert.pem

## Selfsigned Cert with SubjectAltNames

/etc/ssl/req.conf:

    [req]
    distinguished_name  = req_distinguished_name
    x509_extensions     = v3_req
    prompt              = no
    [req_distinguished_name]
    C           = XX
    ST          = YY
    L           = Somewhere
    O           = Project
    OU          = elk filebeat
    CN          = elk.org
    [v3_req]
    keyUsage           = keyEncipherment, dataEncipherment
    extendedKeyUsage   = serverAuth
    subjectAltName = @alt_names
    [alt_names]
    IP.1 = 213.73.97.130
    DNS.1 = some.domain


    openssl req -x509 -new -sha256 -config /etc/ssl/req.cnf -nodes -keyout /tmp/filebeat.pem -out /tmp/filebeat.crt

## Create

    cd /root/certs

wildcard, quick and dirty:

    export DOMAIN=example.org && openssl req --newkey rsa:2048  --subj /CN=**.\$DOMAIN  --nodes --keyout wildcard.\$DOMAIN.pem  --out wildcard.\$DOMAIN.crt

einzele domain, quick and dirty:

    export DOMAIN=www.example.org && openssl req --newkey rsa:2048  --subj /CN=\$DOMAIN --nodes  --keyout \$DOMAIN.pem --out \$DOMAIN.crt

besser: script von cacert: <http://svn.cacert.org/CAcert/Software/CSRGenerator/csr>

.crt auf cacert.org hochladen, mit class3-root-cert signieren lassen, und .crt downloaden
.crt in /etc/ssl/certs installieren
.pem in /etc/ssl private/ installieren
Done !
