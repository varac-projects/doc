# Cert tools

## testssl.sh

> command line tool which checks a server's service on any port for the support
> of TLS/SSL ciphers, protocols as well as some cryptographic flaws

- [GitHub](https://github.com/drwetter/testssl.sh)
- Can only check remote certs, not local ones from filesystem

Install:

```sh
sudo pacman -S testssl.sh
```

## check_ssl_cert

- [matteocorti/check_ssl_cert](https://github.com/matteocorti/check_ssl_cert)
  - A shell script (that can be used as a Nagios/Icinga plugin) to check an
    SSL/TLS connection

Install:

```sh
apt install --no-install-recommends -t testing monitoring-plugins-contrib
```

### check remote ssl cert

```sh
export PATH="/usr/lib/nagios/plugins:$PATH"

check_ssl_cert -H  mail.bitrigger.de -p 25 -P smtp
check_ssl_cert -H  mail.bitrigger.de -p 587 -P smtp

check_ssl_cert -H  mail.bitrigger.de -p 993 -P imaps --long-output fingerprint

check_ssl_cert  --H example.org --n example.org \
  --r /usr/share/ca-certificates/cacert.org/cacert.org.crt \
  --altnames --w 30  --c 14
check_ssl_cert --rootcert ~/Leap/git/leap_testprovider/provider/files/ca/ca.crt \
  --host couch1.rewire.co --cn couch1.rewire.co --altnames --p 6984
```

### Check local cert

```sh
check_ssl_cert -H localhost --file /tmp/www.example.org.crt
```

Using prometheus output:

```sh
check_ssl_cert -H localhost  --prometheus --file /tmp/www.example.org.crt
```

### SSL labs

<https://github.com/ssllabs/ssllabs-scan>

```sh
ssllabs-scan https://varac.net
```

## Custom scripts

```sh
ssl-recv-cert.sh www.example.org 443
```

## sslyze

[Github](https://github.com/nabla-c0d3/sslyze)

Install:

```sh
pip install sslyze
```

Usage:

```sh
sslyze repo.vmware.com:443
```

## Show cert with curl

```sh
curl -v https://example.com
```
