# Letsencrypt

* https://letsencrypt.org
* https://letsencrypt.org/docs/rate-limits/
* Show certificate issueing log: https://crt.sh/?q=varac.net

## Install

    apt install certbot

## systemd timer

Gets installed by the certbot debian package:

    systemctl status certbot.timer
    systemctl status certbot.service

## Fetch cert

### from staging CA, for test purposes

    certbot --test-cert --text --agree-tos certonly --standalone -d $(hostname -f)

### from production CA, beware of rate-limit !

    certbot --text --agree-tos certonly --standalone --email varac@varac.net -d $(hostname -f)

    find /etc/letsencrypt/live/

## Renew

    systemctl stop nginx
    certbot renew --dry-run
    certbot renew
    systemctl restart nginx postfix dovecot

### LEAP

    systemctl stop apache2
    certbot certonly --standalone --email admin@$(hostname -d) -d $(hostname -d) -d api.$(hostname -d) -d $(hostname -f) -d nicknym.$(hostname -d)
    systemctl start apache2

## Puppet integration

https://github.com/voxpupuli/puppet-letsencrypt

see `bitrigger/modules/site_letsencrypt/manifests/init.pp`

cronjob is installed in root's crontab

# Acme-tiny client

https://github.com/diafygi/acme-tiny/

Account key at:

    /home/letsencrypt/account.key

## Domain private key (if you haven't already)

    openssl genrsa 4096 > ~/domains/example.org/domain.key

## CSR for a single domain

    openssl req -new -sha256 -key ~/domains/example.org/domain.key -subj "/CN=example.org" > ~/domains/example.org/domain.csr

## Auto-renewal

## Using docker

https://realguess.net/2016/10/09/getting-let-s-encrypt-ssl-certificate-with-docker/

    docker pull certbot/certbot
    docker run --privileged -it --rm -p 443:443 -v /etc/letsencrypt:/etc/letsencrypt -v /var/log/letsencrypt:/var/log/letsencrypt certbot/certbot --non-interactive --text --agree-tos certonly --standalone --email varac@leap.se -d example.org
