# Clamav

- [Arch wiki](https://wiki.archlinux.org/title/ClamAV)

## Setup

Install `clamav` and other malware definition files (`clamav-unofficial-sigs` is
outdated):

```sh
pamac install clamav python-fangfrisch
```

Add user to `clamav` group:

```sh
sudo usermod -a -G clamav varac
newgrp clamav
```

Configure malware definition services:

```sh
sudo touch /var/log/clamav/freshclam.log
sudo chown clamav:clamav /var/log/clamav/freshclam.log
sudo freshclam
sudo systemctl enable clamav-freshclam.service
sudo systemctl start clamav-freshclam.service
```

Start clamav in daemon mode:

```sh
sudo systemctl enable clamav-daemon.service
sudo systemctl start clamav-daemon.service
```

## Additional malware definitions

These packages ship additional malware definition files:

- [fangfrisch](https://rseichter.github.io/fangfrisch)
- [clamav-unofficial-sigs](https://aur.archlinux.org/packages/python-fangfrisch) is outdated

### fangfrisch

- Config: `/etc/fangfrisch/fangfrisch.conf`
- db: `/var/lib/fangfrisch/db.sqlite`

Setup:

```sh
pamac install clamav python-fangfrisch
sudo -u clamav /usr/bin/fangfrisch --conf /etc/fangfrisch/fangfrisch.conf initdb
sudo systemctl enable fangfrisch.timer
sudo systemctl start fangfrisch.timer
```

## Test

```sh
$ curl https://secure.eicar.org/eicar.com.txt | clamscan -
...
stdin: Win.Test.EICAR_HDB-1 FOUND
```

## On-Access scanning

- [Clamav docs: On-Access Scanning](https://docs.clamav.net/manual/OnAccess.html)
- [Arch wiki: OnAccessScan](https://wiki.archlinux.org/title/ClamAV#OnAccessScan)
- Systemd service: `systemctl status clamav-clamonacc.service`
- Logs: `/var/log/clamav/clamonacc.log`
- Quarantine dir: `/root/quarantine`

### On-Access scanning setup

- FANOTIFY needs to be enabled in the kernel (which is enabled in
  default Arch linux/Manjaro kernels): `zgrep FANOTIFY /proc/config.gz`

First, edit the `/etc/clamav/clamd.conf` configuration file
by adding the following to the end of the file
(see config in the [Arch wiki: On-Access Scanning](https://wiki.archlinux.org/title/ClamAV#OnAccessScan)).
Don't forget to adjust all mount points.

Next, allow the clamav user to run notify-send as any user with custom environment variables via sudo:

```sh
sudo vi /etc/sudoers.d/clamav
```

and paste the following content:

```sh
# https://wiki.archlinux.org/title/ClamAV#OnAccessScan
clamav ALL = (ALL) NOPASSWD: SETENV: /usr/bin/notify-send
```

Edit `clamav-clamonacc.service`:

```sh
sudo systemctl edit clamav-clamonacc.service
```

and insert:

```txt
[Service]
ExecStart=
ExecStart=/usr/sbin/clamonacc -F --fdpass --log=/var/log/clamav/clamonacc.log
```

```sh
systemctl enable clamav-clamonacc.service
systemctl start clamav-clamonacc.service
```

### On-Access scanning test

```sh
wget https://secure.eicar.org/eicar.com.txt
cat eicar.com.txt
```

Issue: [VirusEvent does not trigger](https://github.com/Cisco-Talos/clamav/issues/1062)
