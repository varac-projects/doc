# Electronic identification

- [Arch wiki: Electronic identification](https://wiki.archlinux.org/title/Electronic_identification)
- [LinuxUser 01/2021: Kartenlesegeräte für den elektronischen Personalausweis](https://www.linux-community.de/ausgaben/linuxuser/2021/01/kartenlesegeraete-fuer-den-elektronischen-personalausweis/)

## Compatible RFID readers

- [Liste kompatibler USB-Kartenleser für die Nutzung der Online-Ausweisfunktion](https://www.ausweisapp.bund.de/usb-kartenleser)

## AusweisAPP2

- [AUR package: ausweisapp2](https://aur.archlinux.org/packages/ausweisapp2)
  - Installs `pcsclite` as dependency

Install:

````sh
gpg --keyserver keyserver.ubuntu.com --recv-keys 699BF3055B0A49224EFDE7C72D7479A531451088
pamac install ausweisapp2 ccid pcsc-tools

Test USB RFID card reader:

```sh
pcsc_scan
````

## Open eCard

- [Website](https://www.openecard.org/startseite/)
- [GitHub](https://github.com/ecsec/open-ecard)
