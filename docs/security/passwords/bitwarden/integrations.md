# Bitwarden integrations

## Ansible

- [Ansible bitwarden lookup](https://docs.ansible.com/ansible/latest/collections/community/general/bitwarden_lookup.html)
  - Horribly slow, because it calls the `bw` cli client
  - Failed lookups are not reported

Alternative: Use the [pipe lookup](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pipe_lookup.html)

Example:

```sh
pw: '{{ lookup(''ansible.builtin.pipe'', ''rbw get "server|casita|varac-pw"'' ) }}'
pw: '{{ lookup(''ansible.builtin.pipe'', ''rbw get "hs.k.varac.net|varac|preauth-key|{{ inventory_hostname }}" '' ) }}'

```

## Chezmoi

- [Bitwarden integration](https://www.chezmoi.io/user-guide/password-managers/bitwarden/)
  - Can use `rbw` as well

## Menu integrations

### bitwarden-menu

- [GitHub](https://github.com/firecat53/bitwarden-menu)
- Python, 3 contributors
- Recent release
- Supports Dmenu, Rofi, Wofi or Bemenu

#### Install

- AUR package `bitwarden-menu-git` fails with `No module named 'bwm.__main__'`
  - Same with `pip install --user bitwarden-menu`
- Use pipx to install: `pipx install bitwarden-menu`

#### Configure

- [docs: Configuration](https://github.com/firecat53/bitwarden-menu/blob/main/docs/configure.md)
- Config located at `~/.config/bwm/config.ini`

### Other menu integrations

#### Official bw client

- [mattydebie/bitwarden-rofi](https://github.com/mattydebie/bitwarden-rofi)
  - Shell, 15 contributors
  - Last commit/release 2023-04
  - AUR package: `bitwarden-rofi`

#### rbw

- [rofi-rbw](https://github.com/fdw/rofi-rbw)
  - Python, 13 contributors
  - Recent release
  - Can't search in collection names (due to `rbw`'s limitation)
  - Arch package: `rofi-rbw`
- [rofi-brbw](https://github.com/yusufaktepe/rofi-brbw)
  - Shell, no contributors
  - AUR package
- [rofi-menu](https://github.com/rbuchberger/rbw-menu)
  - Shell, no contributors
  - Last release 2023-09
  - AUR package

#### Other

- [bwutil](https://github.com/xPMo/bwutil?tab=readme-ov-file#bwutil)
  - fzf support
  - Last commit 2023-07
- [tessen FR: add support for bitwarden](https://github.com/ayushnix/tessen/issues/18)

### Stale

- [dmenu_bw](https://github.com/Sife-ops/dmenu_bw)
- [bitwarden-dmenu](https://github.com/andykais/bitwarden-dmenu)
- [bitwarden-pyro](https://github.com/mihalea/bitwarden-pyro)
