# Official Bitwarden cli client

- [GitHub](https://github.com/bitwarden/clients)
- [CLI docs](https://bitwarden.com/help/cli/)
- Very slow, see performance table below
  - `time bw --help` takes > 1 sec

## Install

[Generic](https://bitwarden.com/help/cli/#download-and-install):

```sh
cd /tmp/
wget 'https://vault.bitwarden.com/download/?app=cli&platform=linux' -O bw.zip
unzip bw.zip
mv bw /usr/local/bin
```

Arch / Manjaro:

```sh
sudo pacman -S bitwarden-cli
```

Debian:

```sh
apt update
apt-get install mise
mise use -g bitwarden
```

### Alpine container

- `bw` fails in `alpine:3.20.3`: lots of `Error relocating …: symbol not found`
  errors
- Related:
  [Error relocating /usr/lib/libcurl.so.4: SSL_get0_group_name: symbol not found](https://github.com/alpinelinux/docker-alpine/issues/383)

After installing `libstdc++` and `libgcc` alpine packages I can't get further
than:

```sh
Error relocating /root/.local/share/mise/installs/bitwarden/2024.10.0/bin/bw: fcntl64: symbol not found

```

Although there's a
[Alpine bitwarden client container image](https://gitlab.com/jitesoft/dockerfiles/bitwarden-client)
which seems to work.

## Configure

- config file at `~/.config/Bitwarden\ CLI/data.json`

Configure endpoint:

```sh
bw config server bw.example.com
```

Export env vars by adding them to `~/.config/shell/common/env_local`, i.e.
`BW_ORG`:

```sh
bw list organizations
```

### Multiple accounts

- [Log in to multiple accounts](https://bitwarden.com/help/cli/#log-in-to-multiple-accounts)

## Usage

### Login

> Logging in with email and password is recommended for interactive sessions

```sh
bw login
```

### Unlock

Interactively unlock safe:

```sh
bw unlock
```

Unlock safe using `BW_PASSWORD` env var:

```sh
export BW_PASSWORD="rbw get 'bitwarden|code.operations'"
bw unlock --passwordenv BW_PASSWORD
```

Export session env var:

```sh
export BW_SESSION="…"
```

### Etc

List personal items:

```sh
bw list items
```

Get item by id:

```sh
bw get item de08464b-b2c5-4564-8ed3-213110bc9a6c
```

### Collections

- [How do I add users to org-collections using cli ?](https://community.bitwarden.com/t/how-do-i-add-users-to-org-collections-using-cli/39474)
  - ""

> If you are trying to perform user management on your organization, you won’t
> be able to use the BW CLI. The CLI is designed to work with your vault
> contents, but user management or other organizational management tasks must be
> performed using the web vault interface, and cannot be accomplished using the
> Bitwarden app, browser extension, or CLI.

List org-collections:

```sh
bw list --pretty --organizationid $BW_ORG org-collections
```

Get org-collections template:

```sh
bw get template org-collection --pretty
```

## Backup / export

- The UI client can only export personal items, not organisation items.
- [Backup/export organisation vault](https://bitwarden.com/help/export-your-data/#tab-cli-2er90bORbbPnqOiFdfz6vW)
- None of the export methods export the personal folders where the items are
  stored.

### csv

**Important**: CSV-export doesn't always export all items, without any notice !!
I exported my test-vault (5 items) to json and csv: The json file contained all
entries, while the csv file only contained 2 !

CSV-Format:

```sh
collections,type,name,notes,fields,reprompt,login_uri,login_username,login_password,login_totp
```

```sh
bw export --organizationid $BW_ORG --output ~/backups/bitwarden/ --format csv
```

### Json

Json export contains much more details, like:

- `collections`
- `passwordHistory`
- `revisionDate`
- `creationDate`
- `deletedDate`
- `id`
- `organizationId`
- `folderId`
- `type` (integer: probably login, card, identity etc.)
- `favorite`
- `collectionIds`

```sh
bw export --organizationid $BW_ORG --output ~/backups/bitwarden/ --format json
```

## Issues

- [Renaming folder should rename all subfolders too](https://community.bitwarden.com/t/renaming-folder-should-rename-all-subfolders-too/3321)

## Other

- [Python wrapper for Bitwarden](https://github.com/jdhalbert/bitwarden_secrets_manager_python/)

## Performance

Comparism of time spent fetching a password:

| Tool   | milliseconds |
| ------ | ------------ |
| bw     | 3689         |
| rbw    | 11           |
| gopass | 26           |
