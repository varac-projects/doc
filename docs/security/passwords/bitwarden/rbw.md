# rbw

- [GitHub](https://github.com/doy/rbw)
- Rust
- Uses `rbw-agent` to cache password, so no hassle with session key env vars
- Limitation:
  [No support for collections](https://github.com/doy/rbw/issues/102)
- Config at `~/.config/rbw/`
- Local password cache at `/home/varac/.cache/rbw/`
- Agent runtime data and logs: `~/.local/share/rbw/`
- (d)menu integrations:
  - [rofi-rbw](https://github.com/fdw/rofi-rbw)
  - [rbw-menu](https://github.com/rbuchberger/rbw-menu)

## Install

Arch / Manjaro:

```sh
sudo pacman -S rbw
```

[Debian](https://github.com/doy/rbw?tab=readme-ov-file#debianubuntu):

```sh
$ wget https://git.tozt.net/rbw/releases/deb/rbw_1.12.1_amd64.deb
$ wget https://git.tozt.net/rbw/releases/deb/rbw_1.12.1_amd64.deb.minisig
$ minisign -V -x rbw_1.12.1_amd64.deb.minisig -P RWTM0AZ5RpROOfAIWx1HvYQ6pw1+FKwN6526UFTKNImP/Hz3ynCFst3r -m rbw_1.12.1_amd64.deb
Signature and comment signature verified
Trusted comment: timestamp:1722216304 file:rbw_1.12.1_amd64.deb hashed
$ sudo dpkg -i rbw_1.12.1_amd64.deb
$ rm rbw_1.12.1_amd64.deb*
```

## Configure

- [rbw: configuration](https://github.com/doy/rbw?tab=readme-ov-file#configuration)
- Config at `~/.config/rbw/config.json`

Configure with:

```sh
rbw config set base_url https://...
rbw config set email varac@...
```

## Usage

```sh
rbw get Test
```

Get only username:

```sh
rbw get Test --raw | jq .data.username
```

## Performance

For a comparism of time spent fetching a password see
[./cli-client.md](./cli-client.md)

## Issues

- [Support for organizations and collections when adding / getting a password](https://github.com/doy/rbw/issues/102)
- [Add --raw option for rbw list](https://github.com/doy/rbw/issues/107)
- [Clipboard does not work](https://github.com/doy/rbw/issues/214)
  - seems to only work on windows, since it depends on the
    [clipboard-win crate](https://crates.io/crates/clipboard-win)
  - Workaround: `rbw get "pw" | tr -d "\n" | wl-copy` see also
    [`rbw get` adds a newline](https://github.com/doy/rbw/issues/85)
