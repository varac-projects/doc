# Bitwarden

- [Website](https://bitwarden.com/)
  - [Bitwarden Passwordless.dev](https://bitwarden.com/products/passwordless/#passwordless-demo)
- [GitHub: Bitwarden Labs](https://github.com/bitwarden-labs)
- [Docs](https://bitwarden.com/help/)
  - [Confusion between folders and collections for organizations](https://www.reddit.com/r/Bitwarden/comments/1bv1jla/confusion_between_folders_and_collections_for/)
- [Switch between Bitwarden accounts quickly and easily](https://bitwarden.com/blog/account-switching-phased-rollout-for-bitwarden-clients/)
- [Share credentials across machines using chezmoi and bitwarden](https://medium.com/@josemrivera/share-credentials-across-machines-using-chezmoi-and-bitwarden-4069dcb6e367)
- [Understanding unlock vs. log in](https://bitwarden.com/help/unlock-with-pin/#understanding-unlock-vs-log-in)

## Issues

- [Is it possible to delete multiple folders and collections at once?](https://community.bitwarden.com/t/is-it-possible-to-delete-multiple-folders-and-collections-at-once/10573)
- [FR: Recursively delete folders](https://community.bitwarden.com/t/recursively-delete-folders/17545)

## Client

- For dmenu/rofi/wofi/tofi menu/selector see `./menu-integrations.md`

### Official GUI client

Config: `~/.config/Bitwarden/`

Install:

```sh
sudo pacman -S bitwarden
```

### Official cli client

see [cli-client](./cli-client.md)

### Browser extension

- [Password Manager Browser Extensions](https://bitwarden.com/help/getting-started-browserext/)
- [Extension activation steps](https://bitwarden.com/browser-start/)
  - [Chrome web store: Bitwarden Password Manager](https://chromewebstore.google.com/detail/bitwarden-password-manage/nngceckbapebfimnlniiiahkandclblb?hl=en-US&utm_source=ext_sidebar)
- Arch packages:
  - [AUR: bitwarden-chromium](https://aur.archlinux.org/packages/bitwarden-chromium)
    - Installs, but doesn't register as chrommium extension out of the box
  - [firefox-extension-bitwarden](https://aur.archlinux.org/packages/firefox-extension-bitwarden)

Install for firefox:

```sh
pamac install firefox-extension-bitwarden-bin
```

### Git credentials helper

- [Gist](https://gist.github.com/mikeboiko/58ab730afd65bca0a125bc12b6f4670d)

## Client libraries

### Python

- Official
  [Bitwarden python SDK](https://github.com/bitwarden/sdk/tree/main/languages/python)
  - Supports only management of
    - Secrets
    - Projects
    - Run arbitrary commands
- [corpusops/bitwardentools](https://github.com/corpusops/bitwardentools)
  - Bitwarden python api client and additional tools like for migrating from
    Vaultier to Bitwarden
  - ["bitwardentools dev is halted"](https://github.com/corpusops/bitwardentools/issues/14#issuecomment-2540002617)
  - Supports much more elements than the official SDK
  - [USAGE.md](https://github.com/corpusops/bitwardentools/blob/main/USAGE.md)
  - Supports [2fa](https://github.com/corpusops/bitwardentools/pull/18)
  - Still open:
    [Authentication by using API key](https://github.com/corpusops/bitwardentools/issues/14)
- [birlorg/bitwarden-cli](https://github.com/birlorg/bitwarden-cli/tree/trunk/python/bitwarden)
  - Last commit 2022
- [bitwarden_secrets_manager_python](https://github.com/jdhalbert/bitwarden_secrets_manager_python/tree/main)
  - Wrapper for bw cli
  - Only supports secret item management
- [s6r-bitwarden-cli](https://github.com/ScalizerOrg/s6r-bitwarden-cli)
  - Wrapper for bw cli
  - Search and get secrets, collections, organizations
- [bitwarden-labs/admin-scripts](https://github.com/bitwarden-labs/admin-scripts)
  - Bash, Powershell and Python script examples

## API

- [Bitwarden Public API](https://bitwarden.com/help/api/)
- [Vault Management API](https://bitwarden.com/help/vault-management-api/)
  accessible from the Bitwarden CLI using the `bw serve` command

## Import

- [Docs: Folders](https://bitwarden.com/help/folders/)
- [Importing passwords from other password managers](https://bitwarden.com/de-de/learning/importing-passwords-from-other-password-managers/)
- **Important**: Import is not idempotant, meaning
  [importing twice results in duplicated items and collections](https://bitwarden.com/help/import-faqs/#q-why-did-importing-create-duplicate-vault-items)
  - Prevent this with purging before importing "Individual vaults can be purged
    from the Settings → My account page. Organization vaults can be purged from
    the Organization Settings → Organization info page."
  - [FR: Duplicate removal tool/report (including merge)](https://community.bitwarden.com/t/duplicate-removal-tool-report-including-merge/648)

### Import to an organization

Using the admin UI:
[Import to an Organization](https://bitwarden.com/help/import-to-org/)

- `Admin Console` → `Settings` → `Import data`

Using the cli (there is not option to import all data into one collection, as
you can choose in the UI):

```sh
bw import --formats
bw import --organizationid $BW_ORG bitwardencsv file.csv
```

### From gopass

#### pass2bitwarden

- [GitHub](https://github.com/mtrovo/pass2bitwarden)
- Golang, last commit 2019

```sh
git clone https://github.com/mtrovo/pass2bitwarden ~/projects/security/passwords/bitwarden/pass2bitwarden
cd ~/projects/security/passwords/bitwarden/pass2bitwarden
```

Install deps, then:

```sh
go run ./main.go > ~/bitwarden-migration/gopass.csv
cd ~/bitwarden-migration
```

Convert from folders to
[collections](https://bitwarden.com/help/condition-bitwarden-import/#for-your-organization)
to import:

```sh
csvcut -C favorite,notes gopass.csv | sed '1 s|^folder,|collections,|; 1 s|fields|notes|' > gopass-collections.csv
```

Create a dedicated collection to import the items into (i.e. `varac`) Then
import using the `Import data` tool:

- Vault: Organization vault
- Collection: `varac`
- File format: `Bitwarden (CSV)`

#### pass2bw

- [GitHub](https://github.com/xPMo/bwutil?tab=readme-ov-file#pass2bw)
- Shell
- Last commit 2023

## Other tools

- [bitwarden-menu](https://github.com/firecat53/bitwarden-menu)
  - Config at `~/.config/bwm/`
