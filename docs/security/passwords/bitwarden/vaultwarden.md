# vaultwarden

Open source rust server reimplementation

- [GitHub](https://github.com/dani-garcia/vaultwarden)
- No SSO implemented

  - [Feature Requests](https://github.com/dani-garcia/vaultwarden/issues/246)
  - [Enable user SSO access using OAuth](https://github.com/dani-garcia/vaultwarden/issues/94)
  - [PR: SSO using OpenID Connect](https://github.com/dani-garcia/vaultwarden/pull/3899)

## Helm charts

- [Helm charts on artifacthub](https://artifacthub.io/packages/search?ts_query_web=vaultwarden)
- [gissilabs/vaultwarden](https://github.com/gissilabs/charts/tree/master/vaultwarden)
  - 14 contributors to the 2 charts
  - Recent updates
  - No dependencies
  - ✅ `existingclaim`
- [gabe565/vaultwarden](https://artifacthub.io/packages/helm/gabe565/vaultwarden)
  - Recent updates
  - 13 contributors to all charts
  - [Vaulvarden image updates by Renovatebot](https://github.com/gabe565/charts/pull/685)
  - Dependencies:
    - [bjw-s/common library chart](https://github.com/bjw-s/helm-charts/tree/main/charts/library/common)
    - [bitnami/postgresql](https://artifacthub.io/packages/helm/bitnami/postgresql)
  - ✅ `existingclaim`
- [guerzon/vaultwarden](https://artifacthub.io/packages/helm/vaultwarden/vaultwarden)
  - No `existingClaim`, but `keepPvc` setting, uses the
    [helm.sh/resource-policy: keep](https://helm.sh/docs/howto/charts_tips_and_tricks/#tell-helm-not-to-uninstall-a-resource)
    annotation
- [truecharts/vaultwarden](https://github.com/truecharts/public/tree/master/charts/premium/vaultwarden)
  - Postgres setup only possible with [CloudNativePG](https://cloudnative-pg.io/)
- [andrenarchy/vaultwarden](https://artifacthub.io/packages/helm/andrenarchy/vaultwarden)
  - Still based on long archived [k8s-at-home/common libracry chart](https://github.com/k8s-at-home/library-charts)

## Setup

- Enable admin portal
- [Hash admin token with argon2](https://github.com/dani-garcia/vaultwarden/wiki/Enabling-admin-page#using-argon2)
- Invite user from the admin portal
- Validate email, and login as invited user
- Create organisation
- Create collections

## Backup

- [Backing up your vault](https://github.com/dani-garcia/vaultwarden/wiki/Backing-up-your-vault)
- [vaultwarden-backup](https://gitlab.com/1O/vaultwarden-backup)
  A simple cron powered backup image for vaultwarden.
