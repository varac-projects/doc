# Keepass

## GUIs

### gnome-passwordsafe

https://gitlab.gnome.org/World/PasswordSafe

    sudo apt install gnome-passwordsafe

### keepassxc

Community-Project: https://keepassxc.org/

* Debian package a bit outdated
* snap package up to date
