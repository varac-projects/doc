# Passwords

## Password policies

[What's new - NIST Password Guidelines September 2024](https://www.oneadvanced.com/news-and-opinion/whats-new---nist-password-guidelines-september-2024/):

> Additionally, the complexity requirements have changed from
> requiring complexity to just focus on length.
> This is because of common practices like capitalising
> the first letter or adding a “1” or “!” to the end.

[Password policy recommendations for Microsoft 365 passwords](https://learn.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations?view=o365-worldwide#requiring-the-use-of-multiple-character-sets):

> Forcing your users to choose a combination of upper, lower, digits,
> special characters has a negative effect.
> Some complexity requirements even prevent users from using secure and
> memorable passwords, and force them into coming up with less secure and
> less memorable passwords.

## Generate passwords

### With basic shell utils

- [10 Ways to Generate a Random Password from the Linux Command Line](https://www.howtogeek.com/howto/30184/10-ways-to-generate-a-random-password-from-the-command-line/)

Easy, but beware this will raise a `SIGPIPE` error which can break a shell
script using `set -euo pipefail`:

```sh
< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c6
```

Safe way without running into a `SIGPIPE` error:

```sh
dd if=/dev/urandom bs=1 count=1000 2>/dev/null | tr -dc '[:alnum:]' | head -c32
```

## Password Strength Estimation

- [dropbox/zxcvbn](https://github.com/dropbox/zxcvbn): Original project,
  abandonned
- [dwolfhub/zxcvbn-python](https://github.com/dwolfhub/zxcvbn-python), Active
  python port of zxcvbn

Install:

```sh
sudo apt install python3-zxcvbn
```

### shell

```sh
❯ pwgen -1 24 | zxcvbn
{
  "password": "eiquausu0paerumaewahx4Oo", # gitleaks:allow
  "guesses": "1000000000000000000000001",
  "guesses_log10": 23.999999999999996,
  "sequence": [
    {
      "pattern": "bruteforce",
      "token": "eiquausu0paerumaewahx4Oo", # gitleaks:allow
      "i": 0,
      "j": 23,
      "guesses": 1000000000000000000000000,
      "guesses_log10": 23.999999999999996
    }
  ],
  "calc_time": "0:00:00.002316",
  "crack_times_seconds": {
    "online_throttling_100_per_hour": "36000000000000001998401480.33",
    "online_no_throttling_10_per_second": "100000000000000000000000.1",
    "offline_slow_hashing_1e4_per_second": "100000000000000000000.0001",
    "offline_fast_hashing_1e10_per_second": "100000000000000.0000000001"
  },
  "crack_times_display": {
    "online_throttling_100_per_hour": "centuries",
    "online_no_throttling_10_per_second": "centuries",
    "offline_slow_hashing_1e4_per_second": "centuries",
    "offline_fast_hashing_1e10_per_second": "centuries"
  },
  "score": 4,
  "feedback": {
    "warning": "",
    "suggestions": []
  }
}
```

### Python

```python
import zxcvbn
>>> zxcvbn.zxcvbn('1234')
  {'password': '1234', ...
```
