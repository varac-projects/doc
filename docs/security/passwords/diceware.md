# Diceware passwords

- Both `rbw` and the Bitwarden cli tool can generate diceware passwords,
  but only in english:

```sh
bw generate -p --words 12 --separator space
rbw generate --diceware 12
```

## ulif/diceware

- [GitHub](https://github.com/ulif/diceware)
- [AUR package: diceware](https://aur.archlinux.org/packages/diceware)
  - Last update 2022
- [Available wordlists (also packaged in AUR package)](https://github.com/ulif/diceware/tree/master/diceware/wordlists)

Install:

```sh
pamac install diceware
```

Usage:

```sh
diceware --wordlist de
diceware --wordlist de --delimiter " " --num 12
```

## Outdated

- [diceware-auto](https://github.com/dino-/diceware-auto)
  - Haskell, last commit 2019
  - AUR package: `diceware-auto`
- [diceware-gen](https://gitlab.com/potatodiet/diceware-gen)
  - Rust
  - AUR package: `diceware-gen`
  - Last commit 2020
