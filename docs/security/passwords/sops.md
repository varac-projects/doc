# Sops

- [sops](https://github.com/mozilla/sops)

Install: `brew install sops`

## Setup

Config at `~/.sops.yaml`

## Usage

### Kubernetes

So far no native [kubernetes secrets support in sops](https://github.com/getsops/sops/issues/401)

- [Workarounf with `creation_rules.unencrypted_regex`](https://github.com/getsops/sops/issues/401#issuecomment-2308432037)
- [sops-secrets-operator: Kubernetes SOPS secrets operator](https://github.com/isindir/sops-secrets-operator)
- [sops-operator: A Kubernetes operator for Mozilla SOPS](https://github.com/craftypath/sops-operator)

#### Edit kubernetes secrets in vim

```sh
sops prometheus-alertmanager-varac-secret.yaml
```

#### Using `stringdata:` without base64 encoding

#### Using `data:` and base64 encoding

- [christianrondeau/vim-base64](https://github.com/christianrondeau/vim-base64#in-a-visual-selection-v)
- `:set paste`
- mark visual (`v$`)
- convert base64 to ascii (`\btoa`)
- ...edit...
- mark visual (`v$`)
- convert ascii to base64 (`\atob`)

### Encrypt only part of the keys

see also [sops: Encrypting only parts of a file](https://github.com/mozilla/sops#49encrypting-only-parts-of-a-file)

```sh
sops -e -i --encrypted-regex '^(TOKEN)$' personal-gitlab-exporter.yaml
```

Then simply use `sops personal-gitlab-exporter.yaml` to edit further

### Generate sops secret from file

```sh
kubectl -n wireguard create secret generic wireguard-server-config \
  --dry-run=client \
  -o yaml --from-file=wg0.conf=wg0.conf | \
  sops -e --encrypted-regex '^(data|stringData)$' --input-type yaml \
  --output-type yaml /dev/stdin
```

### Generate empty sops encrypted k8s secret

```sh
cat ~/projects/kubernetes/secrets/empty-secret-stringdata.yml | \
  sops -e --encrypted-regex  '^(data|stringData)$' --input-type yaml \
  --output-type yaml /dev/stdin > $1
```

or use custom function:

```sh
    sops_create_k8s_secret /tmp/test.yml
```

## Wrapper tools

- [sops64](https://github.com/NoUseFreak/sops64): Sops wrapper that does
  base64 encoding and decoding for you. (Unmaintained)
