# Hashicorp vault

## Vault & Kubernetes

- https://learn.hashicorp.com/tutorials/vault/kubernetes-security-concerns
- https://github.com/ricoberger/vault-secrets-operator

## Usage

    export VAULT_ADDR='https://vault.example.org

    vault operator init -key-shares=3 -key-threshold=1
    vault unseal
    vault login $(gopass vault/root-token)

    vault status
