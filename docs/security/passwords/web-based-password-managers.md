# Web based password managers

- [Bitwarden](../passwords/bitwarden/bitwarden.md)
- [Psono](https://psono.com/) - Self hosted solution for teams, no helm charts
- [teampass](https://teampass.net) - no helm charts, unmaintained (last commit 2020-11)
- [nextcloud passwords](https://apps.nextcloud.com/apps/passwords)
  - server-side or client-side encryption possible ([bug](https://www.kuketz-blog.de/nextcloud-schwachstelle-in-passwords-app/))
  - [git repo](https://git.mdns.eu/nextcloud/passwords)
  - Android clients
    - [NC Passwords Mobile App](https://gitlab.com/joleaf/nc-passwords-app) by joleaf up to date
    - [Nextcloud Passwords Mobile App](https://github.com/daper/nextcloud-passwords-app) by daper: unmaintained,
      last commit 2020-08
  -
