# gopass

- [Website](https://www.gopass.pw/)
- [Github](https://github.com/gopasspw/gopass)
- [Docs/Features: Integrations](https://github.com/gopasspw/gopass/blob/master/docs/features.md#integrations)
- [Docs/Setup: dmenu / rofi support](https://github.com/gopasspw/gopass/blob/master/docs/setup.md#dmenu--rofi-support)

Compatible with [pass](https://www.passwordstore.org)

## Install

[Setup](https://github.com/gopasspw/gopass/blob/master/docs/setup.md#pre-installation-steps)

From Github releases:

- Install binary from [latest release](https://github.com/gopasspw/gopass/releases)

Arch linux:

```sh
pacman -S gnupg2 git rng-tools gopass
```

## Setup

- [Setup: Set up a GPG key pair](https://github.com/gopasspw/gopass/blob/master/docs/setup.md#set-up-a-gpg-key-pair)
- Gopass config: `~/.config/gopass/config`

```sh
gopass setup --crypto gpg --storage gitfs
```

### Domain aliases

- [Docs/Fetaures: Domain Aliases](https://github.com/gopasspw/gopass/blob/master/docs/features.md#domain-aliases)
- [Docs/Configuration](https://github.com/gopasspw/gopass/blob/master/docs/config.md)

Example:

```sh
domain-alias.<from>.insteadOf
```

## Usage

Adding a remote store:

```sh
gopass clone git@0xacab.org:varac/gopass-mobile.git ~/.local/share/gopass/stores/mobile
gopass mounts add mnt/mobile /home/varac/.local/share/gopass/stores/mobile
```

Initialize the store if the store is empty:

```sh
gopass init --store mobile --path /home/varac/.local/share/gopass/stores/mobile
```

Default mount location: `~/.local/share/gopass/stores/`

```sh
gopass mounts
gopass mounts add local /tmp/local
```

Reencrypt to new member:

> Running gopass recipients add or gopass recipients remove will automatically
> reencrypt the whole store Adding a recipient will automatically export his key
> to `.gpg-keys/<ID>`

-Caution\*: Don't use `--store` because in one case gopass started re-encrypting
my main store ! Without the `--store` option gopass asks which store to choose.

```sh
gopass recipients add <kedid>
```

## Andropid client (archived)

- [Password Store](https://github.com/android-password-store/Android-Password-Store)
  - Last release 2021
- [gopass for my Android](https://github.com/gopasspw/gopass/issues/571)

## Browser extensions

### gopass-bridge

- [Github](https://github.com/gopasspw/gopassbridge)

- Development stalled, last commit 2022

- Much better than `browserpass`, gopass-bridge can also handle `http basic auth`

Setup:

Install gopass-jsonapi from
[gopass-jsonapi releases](https://github.com/gopasspw/gopass-jsonapi/releases)

Or with pamac:

```sh
pamac install gopass-jsonapi
```

- Configure native messaging host for each browser: `gopass-jsonapi configure`
- [Install browser extension](https://github.com/gopasspw/gopassbridge#install-browser-extension)

### Browserpass

- [Browserpass extensions](https://github.com/browserpass/browserpass-extension)
- [browserpass-native](https://github.com/browserpass/browserpass-native)
  - Last release 2023-03
- Complicated to install

[Extension sideloading is not possible anymore since FF 74](https://blog.mozilla.org/addons/2019/10/31/firefox-to-discontinue-sideloaded-extensions/)

[Install browserpass native messaging host](https://github.com/browserpass/browserpass-native#install-manually):

```sh
export VERSION='3.0.7'
cd ~/projects/passwords/
wget https://github.com/browserpass/browserpass-native/releases/download/${VERSION}/browserpass-linux64-${VERSION}.tar.gz
wget https://github.com/browserpass/browserpass-native/releases/download/${VERSION}/browserpass-linux64-${VERSION}.tar.gz.asc
curl https://maximbaz.com/pgp_keys.asc | gpg --import
gpg --verify browserpass-linux64-${VERSION}.tar.gz

tar -xzf browserpass-linux64-${VERSION}.tar.gz
mv browserpass-linux64-${VERSION} browserpass-native-${VERSION}
cd browserpass-native-${VERSION}
echo '3.0.7' > .version
make BIN=browserpass-linux64 PREFIX=/usr/local configure
sudo make BIN=browserpass-linux64 PREFIX=/usr/local install
make PREFIX=/usr/local hosts-firefox-user
```

This installs (next to other files):

- `/usr/local/bin/browserpass-linux64`
- `/usr/local/lib/browserpass/hosts/chromium/com.github.browserpass.native.json`
- `/usr/local/lib/browserpass/hosts/firefox/com.github.browserpass.native.json`
- `/usr/local/lib/browserpass/policies/chromium/com.github.browserpass.native.json`

Install [browserpass browser extension](https://addons.mozilla.org/de/firefox/addon/browserpass-ce/?src=search)
from the ff app store

### General issues with browser extentions in snaps

> The native extension will not work in a Snapcraft package due to the
> aggressive sand-boxing preventing the gpg binary from getting required
> dependencies. This issue also effects the Browserpass binary

- [keepassxc-browser: Does not work in Chromium (Snap)](https://github.com/keepassxreboot/keepassxc-browser/issues/405)
- [browserpass: Support Chromium snap package](https://github.com/browserpass/browserpass-native/issues/82)
- [Chromium in snap beaks gnome-shell extension](https://bugs.launchpad.net/ubuntu/+source/chromium-browser/+bug/1741074)

General work-around: [Run chromium-snap without sandbox](https://github.com/keepassxreboot/keepassxc-browser/issues/405#issuecomment-704739349)

#### Browserpass Chromium

As of Ubuntu 19.10, chromium is only available as snap.
Snaps are sandboxed and self-contained, which breaks gpg usage.
The instructions below won't work :/, that's why I switched to firefox for the
time beeing.

[Install native messaging host](https://github.com/browserpass/browserpass-native#install-via-package-manager):

```sh
sudo apt install webext-browserpass
```

[Configure browser](https://github.com/browserpass/browserpass-native#configure-browsers):

```sh
cp /etc/chromium/native-messaging-hosts/com.github.browserpass.native.json ~/snap/chromium/current/.config/chromium/NativeMessagingHosts/
cp /usr/lib/browserpass/browserpass-native ~/snap/chromium/current/.config/chromium/NativeMessagingHosts
sed -i 's|/usr/lib/browserpass|/home/varac/snap/chromium/current/.config/chromium/NativeMessagingHosts|' com.github.browserpass.native.json
```

Use bundled extention from apt package:

```sh
`cp -a /usr/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/browserpass@maximbaz.com ~/snap/chromium/current/Downloads`

then, go to `Extensions` → `Install unpacked` and choose `~/snap/chromium/current/Downloads/browserpass@maximbaz.com`
```

### PassFF firefox extension

[Github](https://github.com/passff/passff)

## Git credentials helper

[Git Tools - Credential Storage](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage)
[git-credential-gopass](https://github.com/gopasspw/git-credential-gopass)

```sh
sudo -R pacman -S git-credential-gopass

git config --global credential.helper gopass
```
