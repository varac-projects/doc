# Metasploit

## Metasploitable 2 VM

- [Metasploitable 2](https://www.offsec.com/metasploit-unleashed/requirements/#how-to-update-kali)

Convert image:

    cd /tmp
    wget https://sourceforge.net/projects/metasploitable/files/Metasploitable2/metasploitable-linux-2.0.0.zip
    unzip metasploitable-linux-2.0.0.zip
    cd Metasploitable2-Linux/
    qemu-img convert -O qcow2 Metasploitable.vmdk metasploitable.qcow2
