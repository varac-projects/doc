# OpenID Connect

- Central identity provider (IDP), also calles OpenID Provider
- Client (or relying party)
- Identity Token includes personal information
  (`Claims`, i.e. name, role, email etc.)
  - Identity tokens are signed by the IDP, and verified by the client
  - JSON Web token (`JWT`)
  - Standard claims (`iss`, `sub`, `aud`, `email` etc.)
  - Custom claims (i.e. `roles`)

## OIDC flows

### Authorization code flow

- Most relevant
- Token is transferred via the `Backend channel` (CLient <-> IDP)
  - SAML uses the `Frontend channel` via Browser, which makes
    it less secure

## Scopes

- Defines the content of the ID token, i.e. which `claims` can an application get from the IDP ?
