# Apparmor

- [Debian wiki: AppArmor](https://wiki.debian.org/AppArmor)
- [Debian handbook](https://debian-handbook.info/browse/stable/sect.apparmor.html)
- [Ubuntusers wiki: AppArmor](https://wiki.ubuntuusers.de/AppArmor/)

From [AppArmor: HowToUse](https://wiki.debian.org/AppArmor/HowToUse):

> AppArmor profiles can be set to different modes:
> complain mode: violations to the policy will only be logged
> enforce mode: operations that violate the policy will be blocked.
> Note that deny rules in profiles are enforced/blocked even in complain mode.

## Usage

Install debugging tools (i.e. `aa-status`):

```sh
apt install apparmor-utils
```

Is Apparmor enabled ?

```sh
cat /sys/module/apparmor/parameters/enabled
```

Profile location:

```sh
ls -al /etc/apparmor.d/
```

List all loaded AppArmor profiles for applications and processes and detail their status (enforced, complain, unconfined):

```sh
sudo aa-status
```

List running executables which are currently confined by an AppArmor profile:

```sh
ps auxZ | grep -v '^unconfined'
```

List of processes with tcp or udp ports that do not have AppArmor profiles loaded:

```sh
sudo aa-unconfined
sudo aa-unconfined --paranoid
```

## Troubleshooting

```sh
journalctl -b |grep 'apparmor.*DENIED'
```

## Examples

### Bookworm upgrade: Cannot start libvirt domain

```sh
$ virsh start dapple-controlplane
error: Failed to start domain 'dapple-controlplane'
error: internal error: process exited while connecting to monitor:
2023-07-07T15:56:03.574545Z qemu-system-x86_64: -blockdev
{"driver":"file","filename":"/var/lib/libvirt/images/baseimage-dapple-controlplane",
  "node-name":"libvirt-3-storage","auto-read-only":true,"discard":"unmap"}:
Could not open '/var/lib/libvirt/images/baseimage-dapple-controlplane': Permission denied
```

From the journal:

```sh
$ journalctl -b |grep 'apparmor.*DENIED.*libvirt'
kernel: audit: type=1400 audit(1688745093.080:57): apparmor="DENIED" operation="open"
  profile="libvirt-a1c36ca3-03c2-4c73-83b1-683b12c214be"
  name="/var/local/lib/libvirt/pools/directory-pool/images/baseimage-dapple-controlplane"
  pid=2142 comm="qemu-system-x86" requested_mask="r" denied_mask="r" fsuid=64055 ouid=64055
```

Libvirt dynamically creates a new profiles for each new domain in `/etc/apparmor.d/libvirt`,
so setting the newly generated profile in `complain` mode (with `aa-complain /etc/apparmor.d/libvirt/libvirt-a1c36ca3-03c2-4c73-83b1-683b12c214be`
only helps for this particular domain.

Solution:

```sh
echo '/var/lib/libvirt/images/* rwk,' > /etc/apparmor.d/local/abstractions/libvirt-qemu
systemctl reload apparmor.service
```
