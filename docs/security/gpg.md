# GPG

- [Arch wiki](https://wiki.archlinux.org/title/GnuPG)

## Usage

```sh
export KEYID=4AFAAA882998AC891E7BDD4D5465E77E7876ED04 # gitleaks:allow
```

### Search Keys

`varac@varac.net` key:

```sh
gpg --keyserver gpg-keyserver.de --search-keys $KEYID
```

### Change passphrase

```sh
$ gpg --edit-key $KEYID
  passwd
  save
```

Reloads gpg-agent after changing the passphrase !

```sh
gpg-connect-agent reloadagent /bye
```

### Signatures

- [Making and verifying signatures](https://www.gnupg.org/gph/en/manual/x135.html)

#### Clearsign

```sh
gpg --clearsign text.txt
```

will result in a clearsigned text.txt.asc, consisting of data and Signature

#### Debian Style Sign

```sh
gpg -u varac--output SHA512SUMS.sign --detach-sign SHA512SUMS
gpg --verify SHA512SUMS.sign
```

### Verify

```sh
gpg --verify test.txt
```

### Test encrypt + decrypt

```sh
echo hi | gpg -e -r $KEYID |gpg -d
```

### Show Recipients

```sh
gpg --list-only passwords.gpg     # does not show own key
gpg --no-default-keyring --secret-keyring /dev/null -a --list-only \
  passwords.gpg
```

### Show whether key is locally signed

```sh
gpg --list-keys --list-options show-uid-validity bob
```

### Edit key

#### Renew / extend expiry date

```sh
export KEYID=$KEYID

gpg --edit-key $KEYID
  expire
  2y
  key 1  # This is for the encryption subkey
  expire
  2y
  save
gpg --list-keys $KEYID
```

Send pubkey to default keyserver and hkps pool:

```sh
gpg --send-keys $KEYID
gpg --keyserver hkps://keyserver.ubuntu.com --send-keys $KEYID
```

### Add identity

```sh
gpg --edit-key $KEYID
  adduid
  save
gpg --send-keys $KEYID
```

Trust shows up as "unknown", but after saving the key it's set to
ultimate.

### Change identity

- [GPG: Change email for key in PGP key servers](https://coderwall.com/p/tx_1-g/gpg-change-email-for-key-in-pgp-key-servers)

### Key transistion

```sh
export OLD_KEYID=...
```

i.e. from `dsa1024/$OLD_KEYID` to `rsa4096/$KEYID`

#### Sign new key with old key

```sh
gpg --default-key $OLD_KEYID --sign-key $KEYID
gpg --list-sigs $KEYID
gpg --send-keys $KEYID
```

#### Revoke old key

```sh
gpg --output varac@varac.net-$OLD_KEYID-revocation-certificate.asc --gen-revoke $OLD_KEYID
gpg --import varac@varac.net-$OLD_KEYID-revocation-certificate.asc
gpg --send-keys $OLD_KEYID
```

#### Publish Key transition statement

[Riseup docs: OpenPGP key transition](https://riseup.net/en/security/message-security/openpgp/key-transition)

### Show content of gpg key(ring)

```sh
gpg --list-packets ./goci/files/apt-keys.gpg
```

or

```sh
gpg ./goci/files/apt-keys.gpg
```

### Show only user-ids of containing keys

```sh
gpg --list-packets ./goci/files/apt-keys.gpg | grep ':user'
```

### Qrcodes

- [open-keychain wiki: QR Codes](https://github.com/open-keychain/open-keychain/wiki/QR-Codes)

qrencode fingerprint:

```sh
qrencode -t utf8 "OPENPGP4FPR:$KEYID"
qrencode -t utf8 "OPENPGP4FPR:$(gpg --fingerprint varac@varac.net | grep '^ ' | sed 's/.*= //; s/ //g')"
```

### Temp home

```sh
GNUPGHOME=$(mktemp -d $HOME/tmp/.gnupgXXXXXX)
export GNUPGHOME
...
```

### keys.openpgp.org

- [Uploading your key](https://keys.openpgp.org/about/usage#gnupg-upload)

### Send encrypted mail

```sh
echo "Encrypted to $KEYID at `date`" | \
  gpg -ea -r "$KEYID" --trust-model always -o - \
  > /tmp/encbody.asc && swaks -f varac@example.org \
  -t varac@example.org -s example.org \
  --h-Subject encrypted --body /tmp/encbody.asc'
```
