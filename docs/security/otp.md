# One time passwords (OTP)

## Android apps

### FreeOTP

- [Github](https://github.com/freeotp/freeotp-android)
  - Well maintained
- [F-Droid](https://f-droid.org/en/packages/org.fedorahosted.freeotp/)
  - Last release 2023-09
- [Docs: Backup](https://github.com/freeotp/freeotp-android/blob/dc3006385935aecad8e4d820e3387780cb726a17/BACKUP.md?plain=1)

#### Issues

- [Support "phonefactor" URIs from Microsoft Authenticator](https://github.com/freeotp/freeotp-android/issues/109)
  - It _is_ possible to use FreeOTP together with M$, see [this comment](https://github.com/freeotp/freeotp-android/issues/109#issuecomment-1668510631)

### FreeOPT+

- Enhanced fork of FreeOTP-Android providing a feature-rich 2FA authenticator
- [Github](https://github.com/helloworld1/FreeOTPPlus)
  - Last commit 2023-04
- [F-Droid](https://f-droid.org/en/packages/org.liberty.android.freeotpplus/)
  - Last release 2023-01
