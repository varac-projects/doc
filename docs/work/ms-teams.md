# M$ teams

- Video chat doesn't work with Firefox, only with Chromium

## Linux client

- [Microsoft Teams progressive web app now available on Linux](https://techcommunity.microsoft.com/blog/microsoftteamsblog/microsoft-teams-progressive-web-app-now-available-on-linux/3669846)
- [teams-for-linux](https://github.com/IsmaelMartinez/teams-for-linux)
  - [AUR package](https://aur.archlinux.org/packages/teams-for-linux)
  - Issues: Cannot login using 2fa with security key (2025-02)
    because of missing [electron WebAuthn FIDO/FIDO2 Support](https://github.com/electron/electron/issues/24573)
