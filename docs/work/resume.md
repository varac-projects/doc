# Resume / CV tools

## JSON Resume

> The open source initiative to create a JSON-based standard for resumes.
> For developers, by developers.

- [Website](https://jsonresume.org/)
- Other tools/[projects](https://jsonresume.org/projects/)
  - [Resumake web service](https://resumake.io/)
    - [GitHub](https://github.com/saadq/resumake.io)

### Themes

- [Themes](https://jsonresume.org/themes/)
- Available themes on [registry.jsonresume.org](https://registry.jsonresume.org/themes)
  - [registry package.json](https://github.com/jsonresume/jsonresume.org/blob/master/apps/registry/package.json)
  - FR to add [missing themes](https://github.com/jsonresume/jsonresume.org/issues/36)

Nice themes:

- [Kendall-bushmanov](https://github.com/Greg-Bush/jsonresume-theme-kendall-bushmanov)
  - Current theme
  - Up-to-date fork of original Kendall theme
- [Kendall](https://github.com/LinuxBozo/jsonresume-theme-kendall)
  - Last commit 2021-01
  - [Example Kendall resume](https://registry.jsonresume.org/thomasdavis?theme=kendall)

### Official resume-cli

- [resume-cli](https://github.com/jsonresume/resume-cli)
  - Last release 2022-10
  - NodeJS
  - [nodejs-resume-cli AUR package](https://aur.archlinux.org/packages/nodejs-resume-cli)

Serve HTML:

    ./node_modules/.bin/resume serve --resume resume.yaml --theme caffeine

PDF generation fails:

    ./node_modules/.bin/resume export --theme caffeine varac.pdf

#### Yaml support

- [Yaml support half-broken](https://github.com/jsonresume/resume-cli/issues/665)
  - Also based on [abandoned yaml-js](https://www.npmjs.com/package/yaml-js)
  - Open [PR to fix YAML support](https://github.com/jsonresume/resume-cli/pull/722)

Usage (PDF generation doesn't work):

    npm install jsonresume-theme-caffeine
    ./node_modules/.bin/resume export --resume resume.yaml --theme caffeine varac.pdf

### Other tools

- [goresume](https://github.com/nikaro/goresume)

  - Recent releases
  - AUR package
  - Only 3 [themes](https://github.com/nikaro/goresume#themes) so far, none of them fit well

- [Resumed](https://github.com/rbardini/resumed)
  - NodeJS
  - "Resumed is a complete reimplementation of resume-cli, using more modern
     technologies while dropping certain features, to remain small and focused."
  - Last commit 2023-01
  - [PDF export example](https://github.com/rbardini/resumed/tree/master/examples/with-pdf-export)
  - Can be used with standard `jsonresume-themes`
  - Works well, the best PDF export so far

### Deprecated tools

- [KissMyResume](https://github.com/karlitos/KissMyResume)
  - Stalled 2020
  - Uses puppeteer Headless Chrome Node API
  - Silently stops without producing a PDF using
    `./node_modules/.bin/kissmyresume build -v -f PDF -t node_modules/jsonresume-theme-kendall resume.json`
- [HackMyResume](https://github.com/hacksalot/HackMyResume)
  - Uses wkhtmltopdf to generate (not so great quality) PDF
  - [Stalled 2018](https://github.com/hacksalot/HackMyResume/issues/229)
- [resume-pycli](https://github.com/nikaro/resume-pycli)
  - Archived 2023 in favor of goresume
  - Uses wkhtmltopdf to generate (not so great quality) PDF
  - Themes: Ships [3 own themes](https://github.com/nikaro/resume-pycli/tree/main/resume_pycli/themes),
    not compatible with jsonresume themes.
    - Flat: Links are shown twice, ugly
    - Stackoverflow: Shows a `<br>` at the end of the work summary, and no icons
      at additional Gitlab links
    - Base: Unusable, only shows header

## Yaml resume

- [Yaml-resume website](https://yaml-resume.com)
- Only 2 themes !
- Last commit 2022-01
