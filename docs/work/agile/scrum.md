# Scrum

## Tools

### Issue estimation tools

- [hatjitsu](https://hatjitsu.toolforge.org/)
  - Distributed scrum planning poker for estimating agile projects.
- [Planning poker](https://planningpokeronline.com/)
- [komplexitaeter](https://app.komplexitaeter.de/)

### Etc

- [Excalidraw](https://app.excalidraw.com/)
