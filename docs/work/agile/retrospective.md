# Retrospectives

Retrosective 1o1s:

- <http://www.funretrospectives.com/>
- <https://www.atlassian.com/team-playbook/plays/retrospective>

Online retros:

- <https://app.funretrospectives.com>

## Introduction 5 mins

- Positive spirit of continuous improvement
- Not to blame anyone, but to improve as a team.
- Share whatever you think will help the team improve
- Listen with an open mind, and
  remember that everyone's experience is valid (even those you don't share).
- Don't make it personal, don't take it personally.

- [Prime directive](http://www.funretrospectives.com/prime-directive/):

> Regardless of what we discover, we understand and truly believe that
> everyone did the best job he or she could, given what was known at the
> time, his or her skills and abilities, the resources available,
> and the situation at hand.

## Retro questions

- Multiple possible questions:
  - What went well, What needs improvement
  - Liked, Learned, Lacked
  - Guess who likes it
  - Add, Remove, Recycle
  - Hopes & Concerns
- Always: Action items / Next steps column

## Timing

- first colum (3 mins)
- read first col
- fill both colum 2 + 3 (6 min)
- collective merge cards
- read cards
- vote 3 + 3 votes (3 mins)
