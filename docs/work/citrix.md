# Citrix Workspace App

- [Arch wiki: Citrix](https://wiki.archlinux.org/title/citrix)

## icaclient

- Config at `~/.ICAClient/`

Install:

```sh
pamac install icaclient
sudo mkdir /etc/icalicense
sudo chown varac:varac -R /etc/icalicense
```

Download `ica` file and start the client with:

```sh
/opt/Citrix/ICAClient/wfica.sh /tmp/Q29udHJvbGxlci5ORFIgRGVza3RvcCAyMDE5ICRTMTAyMC0xMDIz.ica
```

## Issues

- Copy&Paste does not work. Solution: Use [icaclient-beta](#icaclient-beta)

## icaclient-beta

- [icaclient-beta](https://aur.archlinux.org/packages/icaclient-beta)

Install:

```sh
pamac install icaclient-beta
```
