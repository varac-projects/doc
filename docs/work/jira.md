# Jira

- [Half-assed Markdown support](https://support.atlassian.com/jira-software-cloud/docs/markdown-and-keyboard-shortcuts/)

## cli tools

- [ankitpokhrel/jira-cli](https://github.com/ankitpokhrel/jira-cli)
  - [Introducing Jira CLI](https://medium.com/@ankitpokhrel/introducing-jira-cli-the-missing-command-line-tool-for-atlassian-jira-fe44982cc1de)

### Outdated

- [foxythemes/jira-cli](https://github.com/foxythemes/jira-cli)
  - Last commit 2019
