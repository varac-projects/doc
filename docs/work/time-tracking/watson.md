# Watson

* [Website](https://tailordev.github.io/Watson/)
* [Github](https://github.com/TailorDev/Watson.git)
* python, json db
* config at `~/.config/watson`
* Downsides:
  * Core development stalled: [Future of Watson: Community Collaboration?](https://github.com/TailorDev/Watson/issues/504)
  * json db file (`~/.config/watson/frames`) not human readable
  * [Lacking a nice stats visualization](https://github.com/TailorDev/Watson/issues/493)
    * [watson-viz](https://github.com/joelostblom/watson-viz): Needs
      [this PR](https://github.com/piccolomo/plotext/pull/164) applied
  * [FR: Add option to specify log/report timerange as calendar week, month or year](https://github.com/TailorDev/Watson/issues/494)
  * Still not possibele to [Get balance compared to nominal hours](https://github.com/TailorDev/Watson/issues/249)
  * [Add an optional (relative) time to the start and stop commands](https://github.com/TailorDev/Watson/issues/75)
  * [Add log messages to frames](https://github.com/TailorDev/Watson/pull/252) is still not merged

## Usage

Start:

    watson start OAS +#CC-83

Stop:

    watson stop

Add:

    watson add --from 10:15 --to 12:45 OAS

Generate monthly reports with custom script:

    watson-report-last-month.sh

Generate reports:

    export MONTH='2019-01'
    watson  report --from ${MONTH}-01 --to ${MONTH}-31 -p OAS > timesheet-report-${MONTH}.txt

Show log for given timeframe:

    watson log --from ${MONTH}-01 --to ${MONTH}-31 -p OAS`


## Issues

* [Report calculate wrong totals](https://github.com/TailorDev/Watson/issues/505)

## Watson graphs

* [watson-graphing](https://github.com/k4j8/watson-graphing)
