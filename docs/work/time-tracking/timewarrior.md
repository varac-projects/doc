# Timewarrior

- Written in C, extensible in Python
- [Website](https://timewarrior.net/)
- [Github](https://github.com/GothenburgBitFactory/timewarrior)
  - 2024-01: 41 Contributors, 3200 commits, recent release
- [Docs](https://timewarrior.net/docs/)
- [Tracking time on the command line with Taskwarrior and Timewarrior](https://jrisch.medium.com/tracking-time-with-taskwarrior-and-timewarrior-6759f3542276)
- [3rd party tools](https://timewarrior.net/tools/)
  - Waybar integrartions
    - [timewarrior-waybar](https://github.com/Krakazybik/timewarrior-waybar)
    - [Waybar](https://github.com/kohane27/Waybar-Clockify)
      Waybar-Clockify is a simple wrapper between clockify-cli and timewarrior
    - [Example Waybar integration](https://devpress.csdn.net/linux/62ebb99989d9027116a0f65a.html)
  - [twtools](https://github.com/fradeve/twtools)
  - [timew-report](https://github.com/lauft/timew-report)
  - [zsh completion](https://github.com/svenXY/timewarrior)
  - [TimeFX](https://github.com/amiroslaw/TimeFX)
    GUI application for presenting Timewarrior data on charts.
  - [toki](https://github.com/cheap-glitch/toki)
    Bash wrapper around the Timewarrior CLI that aims to improve its usability
  - [timew-fzf](https://github.com/oivvio/timew-fzf)

## Usage

- Database [files](https://timewarrior.net/docs/files/) are at `~/timewarrior/data/`

Start:

```sh
timew start project:work tag1
timew start 9:55 project:work tag1
```

Stop:

```sh
timew stop 14:45
```

[Track](https://timewarrior.net/docs/track/): Add closed interval

```sh
timew track :yesterday 'Training Course'
timew track 9am - 11am 'Staff Meeting'
timew track 2024-01-15T10:00 - 2024-01-15T16:09 project1
```

[Delete interval](https://timewarrior.net/reference/timew-delete.1/):

```sh
timew summary :week :ids
timew delete @1
```

[Continue interval](https://timewarrior.net/reference/timew-continue.1/):

```sh
timew continue @1 11:45
```

[Modify active/running interval](https://timewarrior.net/reference/timew-modify.1/):

```sh
timew modify start @3 2024-04-12T13:00
```

[Corrections](https://timewarrior.net/docs/corrections/)

Modify closed intervals:

```sh
timew shorten @2 10mins
timew lengthen @2 10mins
```

[Tags](https://timewarrior.net/docs/tags/):

```sh
timew tags
```

### Summaries

Show current, active task:

```sh
timew
```

[Summary](https://timewarrior.net/docs/summary/):

```sh
timew summary
timew summary :lastweek
```

Week view:

```sh
timew week
timew week :lastweek
```

## Extensions

[Extension API](https://timewarrior.net/docs/api/)

### timew-fzf

- [Github](https://github.com/oivvio/timew-fzf)

Install:

```sh
pamac install python-timew-report python-plumbum python-pyfzf
```
