# Time tracking tools

* [Arch wiki: Time trackers](https://wiki.archlinux.org/title/List_of_applications/Other#Time_trackers)
* [time-tracker projects on Github](https://github.com/topics/time-tracker)

## Manual time trackers

### Watson

See [watson.md](./watson.md)

### Timewarrior

See [timewarrior.md](./timewarrior.md)

### Kimai2

* [Website](https://www.kimai.org/)
* No Support for [OpenID Connect](https://github.com/kevinpapst/kimai2/issues/2469)
* [helm chart](https://artifacthub.io/packages/helm/kimai2tet/kimai-helmchart)
* limited [cli tool](https://github.com/infeeeee/kimai2-cmd)
  * Last release 2021
* [Android app im Play store](https://play.google.com/store/apps/details?id=de.hmrit.kimai2app)
  * 3€
* [Stale F-Droid app](https://f-droid.org/de/packages/de.live.gdev.timetracker/)

### super-productivity

* [Website](https://super-productivity.com/)
* [no cli tool](https://github.com/johannesjo/super-productivity/issues/173)
* [Android client](https://play.google.com/store/apps/details?id=com.superproductivity.superproductivity&hl=gsw)
  * Last version 2023-04
  * [Android client repo](https://github.com/johannesjo/super-productivity-android)

### Other tools

* [tiempo](https://tiempo.categulario.xyz/)
  * Rust
  * 2024-01: 465 commits, Last release 2023-08
* [Klog](https://github.com/jotaen/klog): go
  * 2024-01: 5 Contributors, 374 commits, recent release
* [tt](https://github.com/dribnif/tt) (python)
  * 2024-01: 3 Contributors, 54 commits, recent release
  * Adding a note is a second step
  * Human readable json db (`~/.tt-sheet.json`)

### Gui/Web only, no cli

* [InvoiceNinja](https://invoiceninja.com/projects/)
  A bit overkill for time tracking alone
* [Khronos](https://github.com/lainsce/khronos)
* [Furtherance](https://github.com/lakoliu/Furtherance)
* [Timetrack](https://gitlab.gnome.org/danigm/timetrack)
* [Kapow](https://gottcode.org/kapow/)
  * No Github repo found, Website hosted on Dreamhost :/
* [lasius](https://github.com/tegonal/lasius)
* [Furtherance](https://github.com/lakoliu/Furtherance)

### Stale / old / unmaintained

* [timetrace](https://github.com/dominikbraun/timetrace)
  * Last release 2022
* [doing](https://github.com/ttscoff/doing/)
  * Ruby/Go
  * Last release 2021
* [parti-time](https://github.com/JohannesFKnauf/parti-time)
  * Last release 2020
  * Blog article: [It's parti-time: Plain Text Time Tracking for Nerds](https://metamorphant.de/blog/posts/2019-11-27-parti-time-plain-text-time-tracking-for-nerds/)
  * Plaintext based, clojure
* [bartib](https://github.com/nikolassv/bartib)
  * Rust
  * Last release 2021
* [ctt](https://code.ungleich.ch/ungleich-public/ctt)
  * Last tag 2020
* [timetracking](https://github.com/hardliner66/timetracking/)
  * Last release 2021
* [utt](https://github.com/larose/utt)
  * Last tag 2021
* [Timetrap](https://github.com/samg/timetrap)
  * Last tag 2017
* [Zeit](https://github.com/mrusme/zeit)
  * Last release 2023-01
  * golang
  * 4 Contributors, recent releases
  * Waybar integration, default time tracker integrated in manjaro-sway
  * Export and stats option
  * Downsides:
    * db file (`~/.config/zeit/zeit.db`) not human readable, unknown
    format
    * [Lacks option to retroactively add tasks](https://github.com/mrusme/zeit/issues/29)
* [Tider](https://github.com/naspeh/tider)
* [hamster-cli](https://github.com/projecthamster/hamster-cli)
* [timed](https://pypi.org/project/timed)
* [t](https://stevelosh.com/projects/t/)
* [moro](https://github.com/getmoro/moro)

### ledger and time tracking

* [plaintextaccounting.org#time-tracking](https://plaintextaccounting.org/#time-tracking)
* [ledger-timetracking](https://github.com/anarcat/ledger-timetracking)
* [hledger timeclock](https://hledger.org/1.32/hledger.html#timeclock)

## Auto time trackers

* [Open Source WakaTime Alternatives](https://alternativeto.net/software/wakatime/?license=opensource)
* `./activitywatch.md`
* `./wakatime.md`
* [tockler](https://github.com/MayGo/tockler/)
  * [Still no wayland support ?](https://github.com/MayGo/tockler/issues/176)
* [Timenaut](https://timenaut.app/)
  * Saas

### Stale

* [latte](https://github.com/flakas/Latte)
