# ActivytyWatch

- [Website](https://activitywatch.net/)

> ActivityWatch is an app that automatically tracks how you spend time on your devices.
> It is open source, privacy-first, cross-platform, and a great alternative to services like
> RescueTime, ManicTime, and WakaTime.

- Local only: [FR: Syncing](https://github.com/ActivityWatch/activitywatch/issues/35)

## aw-server

- database: `~/.local/share/activitywatch/aw-server/peewee-sqlite.v2.db`
- [Config, logs, cache directories](https://docs.activitywatch.net/en/latest/directories.html#config-directory)
- [Activitywatch server Web UI](http://localhost:5600)

Install:

```sh
pamac install activitywatch-bin
```

Enable and start `aw-server-rust` as a systemd service:

```sh
ln -s /opt/activitywatch/aw-server-rust/aw-server.service ~/.config/systemd/user/aw-server.service
systemctl --user daemon-reload
systemctl --user enable aw-server
systemctl --user start aw-server
```

## aw-client

- [aw-client](https://github.com/ActivityWatch/aw-client)
- [docs](https://docs.activitywatch.net/en/latest/)
- [examples](https://docs.activitywatch.net/en/latest/examples.html#examples)

Install:

```sh
pamac install aw-client
```

## Watcher

- [Watchers](https://docs.activitywatch.net/en/latest/watchers.html)
- [ActivityWatch/aw-watcher-vim](https://github.com/ActivityWatch/aw-watcher-vim)
- [aw-watcher-table](https://github.com/Alwinator/aw-watcher-table)
  monitors whether you have set your height-adjustable table to sitting or standing

### aw-watcher-window-wayland

- [Wayland support](https://github.com/ActivityWatch/activitywatch/issues/92)
- [aw-watcher-window-wayland](https://github.com/ActivityWatch/aw-watcher-window-wayland/)

Install:

- [aur package](https://aur.archlinux.org/packages/aw-watcher-window-wayland-git)
  is outdated and has a conflict with `activitywatch-bin`, which is needed for
  the server component
  - Local patched AUR git package at `~/projects/timetracking/activitywatch/aw-watcher-window-wayland-git`
    including the `aw-watcher-window-wayland.service` service unit

Steps for patched AUR git package:

```sh
cd ~/projects/timetracking/activitywatch/aw-watcher-window-wayland-git
makepkg -i
pamac install openssl-1.1
systemctl --user enable aw-watcher-window-wayland.service
systemctl --user start aw-watcher-window-wayland.service
```

### aw-watcher-web

- [aw-watcher-web](https://github.com/ActivityWatch/aw-watcher-web)
- [Firefox addon has been removed from Mozilla site](https://github.com/orgs/ActivityWatch/discussions/818#discussioncomment-4017528)
  - Self-building makes no sense since "loading unsigned extensions is only
    possible in Firefox Nightly and Developer Edition"
