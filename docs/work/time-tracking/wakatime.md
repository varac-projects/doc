# Wakatime

- [Website](https://wakatime.com/)
- [Docs](https://wakatime.com/faq)
- [API Docs](https://wakatime.com/developers)

## wakatime-cli

- [GitHub](https://github.com/wakatime/wakatime-cli)
- Log: `~/.wakatime.log`
- Bolt DB: `~/.wakatime.bdb`
- Data dir: `~/.wakatime/)`

Configuration

- Config file: `~/.wakatime.cfg`
- [Config file options](https://github.com/wakatime/wakatime-cli/blob/develop/USAGE.md)

Install

```sh
sudo pacman -S wakatime
```

[Usage](https://github.com/wakatime/wakatime-cli/blob/develop/USAGE.md):

## Clients / Integrations

- [Wakatime integrations](https://wakatime.com/integrations)

Android:

- [Timeless android client](https://play.google.com/store/apps/details?id=com.gianlu.timeless&utm_source=wakatime_community)
- [Linitime](https://wakatime.com/apps/Linitime)

## Plugins

From [FAQ: Working offline](https://wakatime.com/faq#working-offline):

> When working offline with supported editors\* your coding activity is saved
> in a boltdb database at $HOME/.wakatime.bdb. The next time you use your IDE
> when online, your coding activity is synchronized to your WakaTime dashboard.
> All editor plugins using the wakatime-cli have support for offline coding.
>
> - The Chrome extension supports working offline using [IndexedDB](https://github.com/wakatime/browser-wakatime/pull/166).
> - The Brackets extension does not support working offline.

### neovim

- [Neovim integration](https://wakatime.com/neovim)
- [GitHub](https://github.com/wakatime/vim-wakatime)
- Vim plugin for automatic time tracking and metrics generated from your
  programming activity

### Wakatime browser plugin

> Chrome extension for automatic time tracking and metrics
> generated from your browsing activity.

- For both Firefox and Chrome
- [Github](https://github.com/wakatime/browser-wakatime)
- [Chrome](https://wakatime.com/chrome)
  - [Extenstion in chrome store](https://chrome.google.com/webstore/detail/wakatime/jnbbnacmeggbgdjgaoojpmhdlkkpblgi)
- [Firefox](https://wakatime.com/firefox)
- [Instructions for self-hosted backends like `wakapi`](https://github.com/muety/wakapi#browser-plugin-chrome--firefox)
- Issue: [Custom API URL ignored](https://github.com/wakatime/browser-wakatime/issues/296)

[Configure browser-wakatime](https://github.com/muety/wakapi#browser-plugin-chrome--firefox):

- API URL: `https://wakapi.dev/api/compat/wakatime/v1`

#### Build plugin

- The extension is going through a refactor, the new build instructions are in
  [DEVELOPMENT.md](https://github.com/wakatime/browser-wakatime/blob/master/DEVELOPMENT.md)

Install nvm and `@xarc/run-cli` (for `xrun`) (see `../../coding/node.md`):

Change URL in plugin:

```sh
git clone https://github.com/wakatime/browser-wakatime ~/projects/timetracking/wakatime/browser-wakatime
cd ~/projects/timetracking/wakatime/browser-wakatime
git checkout -b wakapi
grep wakatime.com -irl |grep -vE '(AUTHORS|README.md)' | xargs sed -i 's/wakatime.com/wakapi.k.varac.net/g'
```

Build:

```sh
nvm use 18.13.0
npm install
xrun build
```

- Build src files are located in the `dist/BROWSER` dirs
- Zipped extensions are located in the `dist/BROWSER/web-ext-artifacts` dirs
- Unfortunately, [unsigned extensions need a special Firefox edition (i.e. ESR, -devel, -nightly)](https://support.mozilla.org/en-US/kb/add-on-signing-in-firefox?as=u&utm_source=inproduct#w_what-are-my-options-if-i-want-to-use-an-unsigned-add-on-advanced-users)

### Other plugins

- [Terminal](https://wakatime.com/terminal)
  "Warning: Only use the terminal plugins if you don't work in a
  text editor or IDE. The normal editor plugins give much better coding stats."
- [Obsidian](https://wakatime.com/obsidian)
- [Community plugins](https://wakatime.com/community)

## Selfhosting server component

- [hakatime](https://github.com/mujx/hakatime)
  - Haskell
  - Last commit and release 2024-04

### wakapi

- [Wakapi](https://wakapi.dev/)
- [GitHub](https://github.com/muety/wakapi)
- [Container image](https://hub.docker.com/r/n1try/wakapi)
- [wakapi helm chart](https://artifacthub.io/packages/helm/qjoly/wakapi)
- current updates and releases
