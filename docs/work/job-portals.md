# Job portals

- [OpenSource Job Hub](https://opensourcejobhub.com/)
- [FOSS Jobs](https://www.fossjobs.net/)
- [Digital rights job board](https://www.digitalrights.community/job-board)

## APIs

- [Arbeitsagentur Jobsuche API](https://github.com/bundesAPI/jobsuche-api)

Get token (`client_id` and `client_secret` are from the jobsuche API project):

```sh
export client_id=c003a37f-024f-462a-b36d-b001be4cd24a # gitleaks:allow
export client_secret=32a39620-32b3-4307-9aa1-511e3d7f48a8 # gitleaks:allow
token=$(curl \
-d "client_id=${client_id}&client_secret=${client_secret}&grant_type=client_credentials" \
-X POST 'https://rest.arbeitsagentur.de/oauth/gettoken_cc' | \
grep -Eo '\[^"\]{500,}'|head -n 1)
```

Make request:

```sh
curl -m 60 -H "OAuthAccessToken: $token" 'https://rest.arbeitsagentur.de/jobboerse/jobsuche-service/pc/v4/jobs?angebotsart=1&wo=Hamburg&umkreis=10&arbeitszeit=tz;mj&page=1&size=25&pav=false'
```
