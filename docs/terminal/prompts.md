# Shell prompts

## Starship

* [Starship guide](https://starship.rs/guide)
* [Github](https://github.com/starship/starship)
* Rust
* No [Asynchronous Rendering](https://github.com/starship/starship/discussions/4532)

Install:

    sudo snap install starship

[Recommended Fonts](https://starship.rs/guide/#prerequisites) needs to get
installed in `~/.local/share/fonts`

Config file: `~/.config/starship.toml`
             (`~/snap/starship/current/.config/starship.toml` for snap)

Debug config:

    STARSHIP_LOG=debug starship prompt

or even:

    STARSHIP_LOG=trace starship prompt

Couldn't find a way to easily build a custom segment

## Spaceship

* [Website and docs](https://spaceship-prompt.sh/)
* [Github](https://github.com/spaceship-prompt/spaceship-prompt)
* Shell
* v4 introduced [asynchronous rendering](https://spaceship-prompt.sh/blog/2022-spaceship-v4/)

## Other

* Powerline: Slow!
* [zinc](https://github.com/robobenklein/zinc)
* [powerlevel10k](https://github.com/romkatv/powerlevel10k)
