# Pager

## Bat

- [GitHub](https://github.com/sharkdp/bat)
- Rust
- [Alternatives: comparison table](https://github.com/sharkdp/bat/blob/master/doc/alternatives.md)

### Config

Generate config file at `~/.config/bat/config`

```sh
bat --generate-config-file
```

### Themes

```sh
bat --list-themes
bat --theme="gruvbox-dark" foo.csv
```

### Syntax highlighting

```sh
bat --list-languages
bat --language=csv foo.txt
```

- [Adding new syntaxes / language definitions](https://github.com/sharkdp/bat?tab=readme-ov-file#adding-new-syntaxes--language-definitions)
- Based on the [syntect](https://github.com/trishume/syntect/) Rust library and
  [Sublime text editor packages](https://github.com/sublimehq/Packages)
- I.e. the
  [Markdown Sublime package](https://github.com/sublimehq/Packages/tree/master/Markdown)
  - Markdown code highlighting is not as good as Neovim + Treesitter

Missing syntax:

- [ndjson](https://github.com/sharkdp/bat/issues/3208)

## delta

- [Website](https://dandavison.github.io/delta/)
- [GitHub](https://github.com/dandavison/delta)
- Rust
- A syntax-highlighting pager for git, diff, and grep output
- [Comparisons with other tools (git, diff-so-fancy / diff-highlight, GitHub)](https://dandavison.github.io/delta/comparisons-with-other-tools.html)

## nvimpager

- [GitHub](https://github.com/lucc/nvimpager)
- Markdown code highlighting is really bad, not as good as Neovim + Treesitter
  maybe solved by
  [Add treesitter highlighting support in cat mode](https://github.com/lucc/nvimpager/pull/98)
