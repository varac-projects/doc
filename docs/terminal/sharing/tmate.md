# tmate

https://tmate.io/
https://tmate.io/#access_control
https://tmate.io/#named_sessions
https://github.com/tmate-io/tmate-ssh-server

## Usage

    tmate show-messages

    tmate list-clients

### Invite peers read-only

Show read-only URL (don't forget too clear screen before inviting):

    tmate show-messages

## self-hosted tmate-ssh-server

https://hub.docker.com/r/tmate/tmate-ssh-server
https://github.com/tmate-io/tmate-kube/

see also `~/projects/infrared/tmate/self-hosting-tmate-server.md`

### Limit access to server

* [Ebardie/authorized keys only](https://github.com/tmate-io/tmate-ssh-server/pull/93)
  brought back authorized_keys support again.
  There's no up to date [tmate-ssh-server container image](https://hub.docker.com/search?q=tmate-ssh-server).
