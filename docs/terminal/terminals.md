# Terminals

- [Wikipedia list of terminal emulators](https://en.wikipedia.org/wiki/List_of_terminal_emulators#X_Window_Terminals)
- [Anarcat: A look at terminal emulators](https://anarc.at/blog/2018-04-12-terminal-emulators-1/)
- [What are the best Wayland terminal emulators?](https://www.slant.co/topics/5372/~wayland-terminal-emulators)

## VTE based terminals

- Are wayland-natve
- [Gnome: VTE Terminal Widget Library](https://wiki.gnome.org/Apps/Terminal/VTE)

## Other terminals

- [Kitty](./kitty.md)
- [Wez](./wez.md)
  - [Support org.freedesktop.appearance.color-scheme to retreive system dark mode](https://github.com/wez/wezterm/issues/2258)
- [Foot](https://codeberg.org/dnkl/foot)
  - No [ligatutre support](https://codeberg.org/dnkl/foot/issues/57)
- [Tilix](https://github.com/gnunn1/tilix)
  - Cons: dconf, Double decorated window top
- Alacritty
- [Sakura](https://launchpad.net/sakura)
  - Cons: Issue tracker at launchpad, no gh mirror/issue tracker
- Terminology
  - Bad: binary config file :( `~/.config/terminology/config/standard/base.cfg`
- [zutty](https://tomscii.sig7.se/2020/12/A-totally-biased-comparison-of-Zutty)

## Font / ligatures support

- [FiraCode terminal compatibility list](https://github.com/tonsky/FiraCode?tab=readme-ov-file#terminal-compatibility-list)
- Ligature support:
  - Kitty
  - Wez

## Terminfo files

`libtinfo5` deb package provides terminfo files for the system.

These terminals are *not* supported by remote `libtinfo5`
debian stretch versions:

- termite
- st

These terminals are fully supported:

- urxvt
