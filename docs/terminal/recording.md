# Terminal recording

## asciinema

<https://asciinema.org/>

    apt install asciinema

    asciinema rec

### Edit

<https://github.com/cirocosta/asciinema-edit>

* Not packaged, download from GH releases
* [Wiki doc how to edit recordin](https://github.com/asciinema/asciinema-player/issues/78)

## svg-term-cli

* <https://github.com/marionebl/svg-term-cli>
* Can't find an image viewer for animated svgs :/

Install:

Needs `asciinema` installed.

    npm install -g svg-term-cli

Examples:

Use local json recording:

    wget https://github.com/conventional-changelog/commitlint/blob/master/docs/assets/commitlint.json -O /tmp/commitlint.json

Use remote asciicinema recording:

    svg-term --cast 113643 --out examples/parrot.svg --window --no-cursor --from=4500

## Other tools

* <http://angband.pl/termrec.html>
* <https://github.com/nbedos/termtosvg>
