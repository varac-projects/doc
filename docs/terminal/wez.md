# Wez terminal

- [GitHub](https://github.com/wez/wezterm)
- Rust
- Ligature support
- [Support org.freedesktop.appearance.color-scheme to retreive system dark mode](https://github.com/wez/wezterm/issues/2258)

Install:

```sh
sudo pacman -S wezterm
```
