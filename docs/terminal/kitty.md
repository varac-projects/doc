# Kitty

- [GitHub](https://github.com/kovidgoyal/kitty)
- Wayland native
- config text file
- Native image preview: `kitty +kitten icat file.jpg`

## Image support

For `mdcat` image rendering also install:

```sh
eget linebender/resvg
```

### Native image support

- [icat kitten](https://sw.kovidgoyal.net/kitty/kittens/icat/)
- [GitHub](https://github.com/kovidgoyal/kitty/tree/master/kittens/icat)
- Best results, much better than all `imgcat*` apps below

### imgcat

Different imgcat projects:

- [SilinMeng0510/imgcatr](https://github.com/SilinMeng0510/imgcatr)
  - Rust, 28 commits
  - last release 2024-01
  - AUR package: [imgcatr-git](https://aur.archlinux.org/packages/imgcatr-git)
  - Doesn't properly render
    - `rust-logo-128x128.png`: Very blurry
    - `rust-logo.svg`: Cannot display SVGs
- [eddieantonio/imgcat](https://github.com/eddieantonio/imgcat)
  - C, 283 commits
  - last release 2023-12
  - AUR package: [imgcat-git](https://aur.archlinux.org/packages/imgcat-git)
  - Doesn't properly render
    - `rust-logo-128x128.png`: Very blurry
    - `rust-logo.svg`: ✅
- [trashhalo/imgcat](https://github.com/trashhalo/imgcat)
  - Go
  - Last release 2020
  - AUR packages:
    - [imagcat](https://aur.archlinux.org/packages/imgcat)
    - [imagcat-bin](https://aur.archlinux.org/packages/imgcat)
  - Doesn't properly render
    - `rust-logo-128x128.png`: Very blurry
    - `rust-logo.svg`: Cannot display SVGs

Test:

```sh
cd ~/projects/markup/markdown/mdcat/sample/
imgcat /tmp/rust-logo.svg
```

## Configuration

### TERM

- [I get errors about the terminal being unknown or opening the terminal failing or functional keys like arrow keys don’t work?](https://sw.kovidgoyal.net/kitty/faq/#i-get-errors-about-the-terminal-being-unknown-or-opening-the-terminal-failing-or-functional-keys-like-arrow-keys-don-t-work)
- [Can we talk about "xterm-kitty"?](https://github.com/kovidgoyal/kitty/discussions/3873)
- [Please submit xterm-kitty terminfo to ncurses database](https://github.com/kovidgoyal/kitty/issues/879)

Solution for ssh:

- Use `kitten ssh myserver` which automatically installs the kitty terminfo
  on the remote server
- On every server you ssh into using kitty, install the i.e.
  Debian [kitty-terminfo](https://packages.debian.org/bullseye/kitty-terminfo) package

## Usage

### Key bindings

- [Open key bindings in new browser window](https://github.com/kovidgoyal/kitty/issues/2164#issuecomment-1016024263):
  `ctrl shift F6`
  (opens man page in browser)
- Edit configuration: `ctrl shift F2`
- [Search terminal scrollback](https://sw.kovidgoyal.net/kitty/#scrollback): `ctrl shift h`

## Themes

- [Kitty docs: Changing kitty colors](https://sw.kovidgoyal.net/kitty/kittens/themes/)
- [kovidgoyal/kitty-themes](https://github.com/kovidgoyal/kitty-themes)
- [catppuccin/kitty](https://github.com/catppuccin/kitty)
- Archived [gruvbox-material-kitty](https://github.com/selenebun/gruvbox-material-kitty) theme
- Archived [kitty-gruvbox-theme](https://github.com/wdomitrz/kitty-gruvbox-theme)
  - Outdated [AUR package](https://aur.archlinux.org/packages/kitty-gruvbox-theme-git)

Set theme for all Kitty instances:

```sh
kitten themes --reload-in=all Catppuccin-Mocha
kitten themes --reload-in=all Gruvbox\ Dark
```

Interactively select theme for current window:

```sh
kitten themes
```

## Issues

- "Phones home": [Remove update notifications](https://github.com/kovidgoyal/kitty/issues/2481)
  - [Set default update interval to 0](https://github.com/kovidgoyal/kitty/pull/3544)
