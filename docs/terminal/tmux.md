# Tmux

Tmux cheat sheet: <https://gist.github.com/MohamedAlaa/2961058>

- `~/projects/terminals/tmux`

## Plugins

- [Tmux Plugin Manager](https://github.com/tmux-plugins/tpm)
- [Tmux plugin list](https://github.com/tmux-plugins/list)

## Tmux issues

`open terminal failed: missing or unsuitable terminal: xterm-kitty`

```sh
export TERM=screen
```

## Script tmux window layout

### tmuxp

[tmuxp](https://tmuxp.git-pull.com/)

### Other

- [tmuxinator](https://github.com/tmuxinator/tmuxinator) (ruby)

## tmux like cluster-ssh

- [xpanes](https://github.com/greymd/tmux-xpanes)
- [intmux](https://github.com/dsummersl/intmux)
- [tmux-cssh](https://github.com/peikk0/tmux-cssh)
- [davidscholberg/tmux-cluster](https://github.com/davidscholberg/tmux-cluster) stalled at 2016
- [tcluster](https://github.com/ntnn/tcluster) stalled at 2016
