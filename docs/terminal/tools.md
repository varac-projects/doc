# Terminal tools

## hwatch

- [GitHub](https://github.com/blacknon/hwatch)

Install:

```sh
pamac install hwatch
```

Usage:

```sh
hwatch date
```

Output to `stdout` and only update when output changes:

```sh
hwatch -b date
```
