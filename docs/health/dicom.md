# DICOM

- [Medical imaging](https://en.wikipedia.org/wiki/Medical_imaging)
- [Ubuntuusers wiki: DICOM](https://wiki.ubuntuusers.de/DICOM/)

## Viewer

- [AlizaMS](https://github.com/AlizaMedicalImaging/AlizaMS)
  - C
  - Actively maintained
  - [AUR package](https://aur.archlinux.org/packages/alizams)
    - Lightweight
    - But compiles for ages
- [weasis](https://weasis.org/en/index.html)
  - [GitHub](https://github.com/nroduit/Weasis)
  - [AUR package](https://aur.archlinux.org/packages/weasis-bin)
  - Java
  - Actively maintained, up to date AUR package
- [dcm4che](https://www.dcm4che.org)
  - Actively maintained, Java
  - No AUR package
- [dicomscope](https://dicom.offis.de/dscope.php.en)
  - [AUR package](https://aur.archlinux.org/packages/dicomscope-bin)
    - last update 2024-03

## Unmaintained

- [ginkgocadx](https://github.com/gerddie/ginkgocadx)
  - archived in 2021
- [Aeskulap](http://aeskulap.nongnu.org/) (last release 2007)
- [FslView](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslView)
