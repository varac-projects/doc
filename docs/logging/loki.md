# Loki

- [Loki](https://grafana.com/oss/loki/)
- [Chart on helm hub](https://hub.helm.sh/charts/loki/loki)
- [Loki chart](https://github.com/grafana/loki/tree/master/production/helm)

## Loki alerting

How to alert when there's an error in the logs

- <https://grafana.com/docs/loki/latest/alerting/>
- <https://www.youtube.com/watch?v=GdgX46KwKqo>
- <https://grafana.com/docs/loki/latest/alerting/>
- <https://www.infracloud.io/blogs/grafana-loki-log-monitoring-alerting/>

## Configure grafana

<https://grafana.com/docs/grafana/latest/features/datasources/loki/>

URL: `http://loki-stack.logging.svc.cluster.local:3100`

Test query:

```sh
{job=~".*nextcloud.*"}
```

### Logcli

<https://grafana.com/docs/loki/latest/getting-started/logcli/>
<https://grafana.com/docs/loki/latest/logql/>

#### Install

```sh
sudo pacman -S logcli
```

or via GitHub releases:

```sh
eget grafana/loki
mv ~/bin/download/logcli-linux-amd64 ~/bin/download/logcli
```

#### Setup

```sh
kubectl -n stackspin port-forward pod/loki-0 3100
```

Optional: If loki is username/password protected
export the grafana/loki admin credentials:

```sh
export LOKI_USERNAME=admin
export LOKI_PASSWORD=...
```

#### Query exaples

```sh
logcli labels
logcli labels job
logcli labels filename
```

Show *all* logs:

From <https://stackoverflow.com/a/70904375>:

> Due to Loki’s design, all LogQL queries must contain a log stream selector.

```sh
logcli query '{job=~".+"}'
```

Search for regex:

```sh
logcli query '{job=~".+"} |~ "(error|warn)"'
```

Show log entries over time:

```sh
sum(rate({app="mastodon"}[5m])) by (pod)
```
