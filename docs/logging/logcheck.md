# Logcheck

https://wiki.koumbit.net/MonitoringService/SoftwareComparison#Logcheck

Run manually, pipe to file

    sudo -u logcheck sh -c '/usr/sbin/logcheck -o -S $(mktemp -d) > /tmp/logwatch.log'

Use different, new state directory

    sudo -u logcheck /usr/sbin/logcheck -o -S ${mktemp -d} > logwatch.eml

## Tune ignores

    zless /usr/share/doc/logcheck-database/README.logcheck-database.gz

Custom ignores are in `/etc/logcheck/ignore.d.workstation/custom-varac`.

i.e. Ignore ferm logs:

    Dec  9 18:18:59 rocinante kernel: [106230.394358] INPUT DENIED by ferm: IN=wlp3s0 OUT= MAC=8c:70:5a:d4:72:24:b8:27:eb:4e:b1:f9:08:00 SRC=192.168.178.34 DST=192.168.178.22 LEN=204 TOS=0x00 PREC=0x00 TTL=64 ID=24513 DF PROTO=UDP SPT=1900 DPT=49112 LEN=184
