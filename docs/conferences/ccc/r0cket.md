# R0cket

The r0ket is the badge for the Chaos Communication Camp 2011.

- [Camp 2011 wiki: R0cket](https://events.ccc.de/camp/2011/wiki/R0ket)
- [GitHub](https://github.com/r0ket/r0ket)
- [Recording from r0cket talk](https://media.ccc.de/v/cccamp11-4564-r0ket-en)
