# SIP

<https://en.wikipedia.org/wiki/List_of_SIP_software#Clients>
<https://wiki.ubuntuusers.de/Internet-Telefonie/>
<https://help.ubuntu.com/community/SoftPhone>
<https://help.ubuntu.com/community/VoIP>

## Sip clients

### Baresip

- cli client
- gtk client

### Empathy

<https://wiki.ubuntuusers.de/Empathy/>

for sip: `sudo apt-get install telepathy-sofiasip empathy`

Works !

Issues:

- can't call sipgate voicemail (`50000`)
- Doesn't search gnome contacts / carddav

### Linphone

<https://www.linphone.org>
<https://gitlab.linphone.org/BC/public/linphone-desktop>

gtk, available as app-image, debian packages outdated.
`segmentation fault` on calling (4.2.0-alpha)

### jitsi

Couldn't get `jitsi-desktop` to work, UI it stuck initializing.

### Gnome calls

<https://gitlab.gnome.org/GNOME/calls>

- [Basic SIP support](https://gitlab.gnome.org/GNOME/calls/-/milestones/1):
  still WIP (2021-11), will be part of Gnome 41
  <https://www.phoronix.com/scan.php?page=news_item&px=GNOME-41-Beta>

### Discontinued apps

- ekiga

### jami (ex ring, ex spfphone)

<https://jami.net/>
<https://git.jami.net/savoirfairelinux/jami-client-gnome>

Debian packaged

Issues:

- [No system contact lookup](https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/829),
  <https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/1191>
