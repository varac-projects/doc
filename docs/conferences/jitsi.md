# jitsi-meet-electron

- https://github.com/jitsi/jitsi-meet-electron

## Install

- [github release](https://github.com/jitsi/jitsi-meet-electron/releases)
  Installs `/opt/Jitsi Meet/jitsi-meet`
- [flatpak](https://flathub.org/apps/details/org.jitsi.jitsi-meet)

  `flatpak install org.jitsi.jitsi-meet`
