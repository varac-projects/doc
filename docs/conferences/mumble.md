# Mumble

## Instructions

### German

```
Bitte ladet euch Mumble auf euer Smartphone/ Gerät und verfahrt so:

Server einrichten:

1. Menüpunkt “Favourite Servers” auswählen
2. Oben rechts auf das “+” drücken
3. Die Felder wie folgt ausfüllen:

 - Description: wiki2.fux-eg.org
 - Address: wiki2.fux-eg.org
 - Port: Leerlassen oder 64738 eingeben
 - Username: dein Name
 - Password: leerlassen

4. Vermutlich kommt dann eine Meldung, dass das Zertifikat nicht überprüft werden konnte.
   Hier ausnahmsweise: „Zertifikat vertrauen“ auswählen.
5. o.g. Server auswählen und verbinden.

Um Störgeräusche zu vermeiden, stellt bitte „Push to Talk“ ein:

Push-to-Talk in der Desktop-App:
1. Einstellungen - blaues Rad oben - Audioeingabe - Übertagung - oberstes Auswahlfeld auf "Push-to-Talk" stellen - ok
2. Einstellungen - Tastenkürzel - Hinzufügen - Funktion: Push-to-Talk - Tastenkürzel: gewünschte Taste drücken (empfohlen wird STRG) - ok

Push-to-Talk auf dem Apfel-Telefon:
1. Vor Start des Gesprächs: Preferences - Transmission - Push-to-Talk auswählen (Häkchen)
2. Anruf starten
3. Den Mund unten drücken, um zu sprechen

Push-to-Talk auf dem Android-Telefon:
1. Auf die drei Striche oben links klicken - Einstellungen - Audio - Eingabemethode: Push-to-Talk auswählen
2. Anruf starten
3. Den Balken unten drücken, um zu sprechen
(Optional: Auf die drei Striche oben links klicken - Einstellungen - Audio - Push-to-Talk-Einstellungen bearbeiten (z.B. Gerätetaste zum Sprechen,..))


WICHTIG:

- Ladet euer Telefon vorher auf (Ladekabel mitbringen kann nicht schaden.)
- Ihr braucht einen Kopfhörer mit Mikrofon (z.B. den, der dem Smartphone zum Freisprechen beilag, Headset o.ä.)
- Kommt möglichst pünktlich oder, wenn ihr die App noch einrichten müsst oder unsicher seid, am besten 15 Minuten vorher!
```
