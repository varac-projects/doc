# Terraform

[Awesome terraform](https://github.com/shuaibiyy/awesome-terraform)
[Why terraform](https://blog.gruntwork.io/why-we-use-terraform-and-not-chef-puppet-ansible-saltstack-or-cloudformation-7989dad2865c)

## Install

[Docs: install/apt](https://www.terraform.io/docs/cli/install/apt.html)

```sh
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo tee /etc/apt/trusted.gpg.d/hashicorp.asc
sudo apt-add-repository \
  "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt install terraform
```

Completion:

[Docs: Tab completion](https://www.terraform.io/docs/commands/index.html#shell-tab-completion)

```sh
terraform -install-autocomplete
```

## Usage

```sh
terraform apply -auto-approve
```

Debug:

```sh
OS_DEBUG=1 TF_LOG=DEBUG terraform apply -auto-approve
```

### updating provider versions

```sh
find . -name versions.tf -exec sed -i 's/1.22.3/1.22.7/' {} \;
```

### Import

- [Docs: Import](https://opentofu.org/docs/cli/import/)

Import a resource when a provider upgrade would force a resource recreation:

```sh
tf state show module.ricote.libvirt_pool.default  # note the `id`
tf state rm module.ricote.libvirt_pool.default
tofu import module.ricote.libvirt_pool.default 09f0dc94-5984-4330-b1fe-f150050643d1
```

### Add provider versions to each submodule

```sh
git grep -l gitlabhq/gitlab | xargs -n 1 sed -i '/"gitlabhq\/gitlab"/a \      version = "16.3.0"'
```

### Initialize all

```sh
find . -type d -name .terraform -exec terraform -chdir={}/.. init -upgrade \
```

or

```sh
for i in infrastructure infrastructure/* global environments/*/* kubernetes kubernetes/* ;\
  do echo $i; terraform init -upgrade $i; done
```

### Remove all `.terraform` dirs

```sh
find . -type f -name .terraform.lock.hcl -exec rm {} \; && find . -type d -name .terraform -exec rm -rf {} \;
```

### Terraform state

```sh
tf state list
tf state show 'google_compute_instance.legacy_vm["ofts105-3"]'
```

## Style

- [Style](https://www.terraform.io/docs/configuration/style.html)
- [Syntax](https://www.terraform.io/docs/configuration/syntax.html)
- [Style best practices](https://www.terraform-best-practices.com/code-styling)
- [jonbrouse/terraform-style-guide](https://github.com/jonbrouse/terraform-style-guide/blob/master/README.md)

## Linting

- [terraform pre-commit hook](https://github.com/antonbabenko/pre-commit-terraform)
- [pre-commit-opentofu](https://github.com/tofuutils/pre-commit-opentofu)

- `terraform validate` isn't able to catch invalid resources like aws instance
  type, but `tflint` is i.e.

### tflint

- [Github](https://github.com/terraform-linters/tflint)
- [User guide](https://github.com/terraform-linters/tflint/tree/master/docs/user-guide)

Debug mode:

```sh
TFLINT_LOG=debug tflint
```

Issues:

- When used in pre-commit, there's no indication about the file location
  [Output directory tflint is executed from](https://github.com/antonbabenko/pre-commit-terraform/issues/161)
- Tflint searched for config files in the current directory, then for a global
  one (`~/.tflint.hcl`), but **not** in the root of the git repo i.e. When there
  is no global config file, the default config is used for tflint when executed
  in a subdir i.e., or even when used as `tflint --recursive --module`.

## Vim integration

- [vim-terraform plugin](https://github.com/hashivim/vim-terraform)
- Official [hashicorp/terraform-ls](https://github.com/hashicorp/terraform-ls)
  - OpenTofu support
    - [What is the future plans for an OpenTofu Language Server ?](https://github.com/orgs/opentofu/discussions/1247)
    - [Support OpenTofu in the VS Code Language Server](https://github.com/opentofu/opentofu/issues/970)
    - [OpenTofu fork](https://github.com/gamunu/opentofu-ls)
- [terraform-lsp](https://github.com/juliosueiras/terraform-lsp)

## Additional tools

- [terragrunt](https://terragrunt.gruntwork.io/)
- [tfsec](https://github.com/tfsec/tfsec) `brew install tfsec`
- [checkov](https://github.com/bridgecrewio/checkov) `pipx install checkov`
- `terraform-docs`
  - [OpenTofu support](https://github.com/terraform-docs/terraform-docs/issues/699)
- [tfupdate](https://github.com/minamijoyo/tfupdate):
  > Update version constraints in your Terraform configurations

## Gitblab CI & terraform

- [terraform.gitlab-ci.yml)](https://gitlab.com/gitops-demo/infra/templates/blob/master/terraform.gitlab-ci.yml)
