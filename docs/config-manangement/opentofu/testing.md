# Terraform testing

<https://terratest.gruntwork.io/docs/getting-started/introduction/>

- [Talk slides](https://www.slideshare.net/brikis98/how-to-test-infrastructure-code-automated-testing-for-terraform-kubernetes-docker-packer-and-more)

## Testing pyramid

### Static analysis

- conftest
- tf validate
- tflint

Dry-run:

- tf plan

## Unit tests

- terratest - works with terraform, docker, kubernetes, packer, servers, cloud
  APIs
- kitchen-terraform - works with terraform
- Inspec (no undeploy) - generic tool, cloud APIs
- serverspec (no undeploy) - generic tool
- goss (no undeploy) - generic tool

## Docs

<https://pkg.go.dev/github.com/gruntwork-io/terratest>

## Example

<https://github.com/gruntwork-io/infrastructure-as-code-testing-talk>
