# Terraform version managers

- Use case: Creating a TF state with v16 will result in a broken Gitlab
  CI (which uses TF v14 as default).
- [Awesome Opentofu: Environment managers](https://awesome-opentofu.com/#environment-managers)

## tenv

- [Website](https://tofuutils.github.io/tenv/)
- [GitHub](https://github.com/tofuutils/tenv)

[From the maintainer of tenv](https://github.com/tofuutils/tofuenv/issues/31#issuecomment-1887316628):

> Tenv is also designed not as wrapper, but as an in-depended binary program for managing OpenTofu as well as Terraform.

### Setup

```sh
pamac install cosign tenv-bin
```

Install a version:

```sh
tenv terraform install 1.8.4
tenv tofu install 1.8.4
```

Configure auto-installation:

```sh
echo TENV_AUTO_INSTALL=true >> ~/.zshrc
```

## tofuenv

- [GitHub](https://github.com/tofuutils/tofuenv)

## Tfswitch

- [Website](https://tfswitch.warrensbox.com/)
  - [GitHub](https://github.com/warrensbox/terraform-switcher)
- [OpenTF Support](https://github.com/warrensbox/terraform-switcher/issues/315)
- Much easier to start with than `tfenv`

### Install

Remove any global Terraform binary installations

```sh
sudo pacman -R terraform
```

Then install tfswitch

```sh
pamac install tfswitch-bin
```

### Usage

Tfswitch honors the `terraform.required_version` setting in i.e. `versions.tf`,
and switches to this version.
Use i.e. `required_version = "~> 1.4.0"` to require TF `v1.4.x`

```sh
tfswitch
```

## tfenv

- [GitHub](https://github.com/tfutils/tfenv)
  - [Future Improvement/Feature Request: OpenTF/OpenTofu](https://github.com/tfutils/tfenv/issues/409)
  - Last commit and release 2022: [Is tfenv dead?](https://github.com/tfutils/tfenv/issues/399)
  - Replaced by [tofuenv](https://github.com/tofuutils/tofuenv)

### Install and setup

```sh
pamac install tfenv
mkdir -p ~/.tfenv/bin
export TFENV_CONFIG_DIR=~/.tfenv
```

### Setup GPG key for signature verification

```sh
echo 'trust-tfenv: yes' > ${TFENV_CONFIG_DIR}/use-gpgv
sudo mkdir /opt/tfenv/share
gpg --search-key 374EC75B485913604A831CC7C820C6D5CD27AB87
gpg --armor --export 374EC75B485913604A831CC7C820C6D5CD27AB87 > /tmp/hashicorp-keys.pgp
sudo mv /tmp/hashicorp-keys.pgp /opt/tfenv/share/hashicorp-keys.pgp
```

### tfenv usage

```sh
tfenv install 1.6.0
```

### Issues

- Could not easily set up GPG verification
- Could not easily install versions as unprivileged user (`TFENV_CONFIG_DIR` and
  `TFENV_INSTALL_DIR` are ignored)
