# Chef ohai

https://docs.chef.io/attributes.html

## Write all node attributes to a file

    ohai > /root/ohai.json
    jq . < ohai.json

## Find OS codename

    jq .lsb.codename < ohai.json
