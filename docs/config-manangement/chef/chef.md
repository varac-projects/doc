# Chef

From chef v15 on, `chef-solo` is not included in the chef ruby gem.
Install the chef infra-client deb package to use chef-solo:

https://downloads.chef.io/products/infra-client
