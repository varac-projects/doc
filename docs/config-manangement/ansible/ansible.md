# Ansible

## Ansible galaxy

    ansible role list
    ansible-galaxy role info borgbase.borgbackup

Search on galaxy.ansible.com for roles:

    ansible role search borg

Install:

    ansible-galaxy role install jnv.debian-backports
    ansible-galaxy role install debops.docker

Install from `requirements.yml`

    ansible-galaxy install -r requirements.yml

## Ping all hosts

    ansible all -m ping

## Gather facts

[Gather facts](http://docs.ansible.com/ansible/latest/setup_module.html)

    ansible all -m setup > facts.json
    ansible all -m setup --tree ./facts
    ansible mumble.digital -m setup > facts.json

Disable fact gathering (i.e. for debugging / speeding up the run):

    export ANSIBLE_GATHERING=explicit

or:

    ANSIBLE_GATHERING=explicit ansible-playbook -l device.lan site.yml

## Deploy only to one host

    ansible-playbook -i 'dashboard.digital,' ssh.yml

## Ad-hoc commands

### Without inventory file

Beware the trailing comma !

    ansible all -i gl.varac.net, -a uptime

### Using an inventory file

<http://docs.ansible.com/ansible/latest/intro_adhoc.html>

    ansible bitrigger.de -m shell -a 'echo hi'

    ansible localhost -m debug -a 'msg="{{ traefik_admin_email | checksum }}"'

## Debugging

Don't remove target tmp files:

    export ANSIBLE_KEEP_REMOTE_FILES=1

List tasks that would be applied:

    ansible-playbook --list-tasks -l internal.digital site.yml

List tags that would be applied:

    ansible-playbook --list-tags -l internal.digital site.yml

## Using the debugger

<https://docs.ansible.com/ansible/2.5/user_guide/playbooks_debugger.html>
