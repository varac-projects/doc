# Ansible vault

[short tutorial on how to use Vault in your Ansible workflow](https://gist.github.com/tristanfisher/e5a306144a637dc739e7)

- Use sops: [`Community.Sops`](https://docs.ansible.com/ansible/latest/collections/community/sops/index.html)

## Include encrypted variables into plaintext variable files

<https://docs.ansible.com/ansible/2.5/user_guide/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml>

- [mikefarah/yq: Support ansible vault](https://github.com/mikefarah/yq/issues/172)

### Encrypt

```sh
ansible-vault encrypt_string --vault-id a_password_file 'foobar' --name 'the_secret'
```

Don't use quotes (`'"`) or acktickts (`) in passwords, this is asking for trouble in badly written scripts/software!

```sh
ansible-vault encrypt_string "$(pwgen -ys 24 -r "\`\"'" -1)" --name 'borgbackup_passphrase' >> host_vars/illapa.digital
```

To be on the safe side, don't use special chars at all:

```sh
ansible-vault encrypt_string "$(pwgen  24)" --name 'borgbackup_passphrase' >> host_vars/illapa.digital
```

### Decrypt

To view encrypted variables:

```sh
ansible host.name.tld -m debug -a 'var=mariadb_nextcloud_db_pw'
```
