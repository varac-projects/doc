# Combine filter

See also `~/ansible/examples/combine-dictionaries.yml`

Without `recursive`:

    ❯ ansible localhost -m debug -e '{"a": {"server": {"a": "a"}}}' -e '{"b": {"server": {"b": "b"}}}' -a 'msg={{ a | combine(b) }}'
    localhost | SUCCESS => {
        "msg": {
            "server": {
                "b": "b"
            }
        }
    }


With `recursive`:

    $ ansible localhost -m debug -e '{"a": {"server": {"a": "a"}}}' -e '{"b": {"server": {"b": "b"}}}' -a 'msg={{ a | combine(b, recursive=True) }}'
    localhost | SUCCESS => {
        "msg": {
            "server": {
                "a": "a",
                "b": "b"
            }
        }
    }
