# ansible-lint

- [GitHub](https://github.com/ansible/ansible-lint)
- [Docs](https://ansible.readthedocs.io/projects/lint/)

Usage:

Generate a text file '.ansible-lint-ignore' that ignores all found violations.
Each line contains filename and rule id separated by a space:

```sh
ansible-lint --generate-ignore
```

## pre-commit / CI

- [Ansible-lint docs: Pre-commit setup](https://ansible.readthedocs.io/projects/lint/configuring/#pre-commit-setup)
- [Installing and using Pre-commit with Ansible-lint](https://blog.42mate.com/installing-and-using-pre-commit-with-ansible-lint/)

Unfortunately, there is no way to [run only in new or changed
files](https://github.com/ansible/ansible-lint-action/discussions/49).
