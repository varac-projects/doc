# Docker with ansible

## community.docker.docker_compose

[community.docker.docker_compose](https://docs.ansible.com/ansible/latest/collections/community/docker/docker_compose_module.html)

* when using inline definitions, a `docker-compose.yml` will get created in a
  temp dir under `/tmp`, which isn't great for persistence


## Configure single containers with systemd

https://github.com/mhutter/ansible-docker-systemd-service

## Configure docker-compose with systemd


https://github.com/spigell/ansible-role-docker-compose-systemd
https://gist.github.com/Luzifer/7c54c8b0b61da450d10258f0abd3c917
