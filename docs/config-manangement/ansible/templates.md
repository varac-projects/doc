# Templates

## How to recursively copy templates

* [Closed FR](https://github.com/ansible/ansible-modules-core/issues/159) with good
  workarounds
* https://docs.ansible.com/ansible/latest/collections/community/general/filetree_lookup.html
  Doesn't allow exclusion of files
